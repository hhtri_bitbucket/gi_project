/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ctmlab.staticdata;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;

/**
 *
 * @author hhtri
 */
public class Region{
    public short id;
    public String name;
    public ArrayList<Integer> pos;
    public HashMap<Short, Zone> zones;

    public Region(){
        id = 0;
        pos = new ArrayList();
        zones = new HashMap();
        name = "";
    }   
    
    public Region(short id, int posX, int posY, String name){
        this.id = id;
        pos = new ArrayList();
        pos.add(posX);
        pos.add(posY);
        zones = new HashMap<>();
        this.name = name;
    }
    
    public void addZones(HashMap<Short, Zone> hZone){
        for(Entry<Short, Zone> e:hZone.entrySet()){
            Zone z = e.getValue();
            synchronized(zones){
                zones.put(e.getKey(), z);
            }
        }
    }
}
