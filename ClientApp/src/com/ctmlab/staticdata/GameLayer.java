/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ctmlab.staticdata;
import com.ctmlab.model.GameObjectMap;
import java.util.ArrayList;

/**
 *
 * @author hhtri
 */
public class GameLayer {
    public byte type;
    public byte minZoom;
    public byte maxZoom;
    public ArrayList<GameObjectMap> data;
}
