/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ctmlab.staticdata;

import com.ctmlab.enums.ENUM_RESOURCE_TYPE;
import com.google.common.io.LittleEndianDataInputStream;
import com.google.common.io.LittleEndianDataOutputStream;
import java.io.IOException;
import java.util.Map.Entry;
import java.util.TreeMap;

/**
 *
 * @author hhtri
 */
public class GameResources {

    public TreeMap<Byte, Long> data = new TreeMap<>();

    public GameResources() {
        for(byte t:ENUM_RESOURCE_TYPE.getKeys()){
            if(t>0) {
                data.put(t, 0L);
            }
        }
    }
    public GameResources copy(){
        GameResources rs = new GameResources();
        rs.setData(this);
        return rs;
    }

    public void setData(GameResources res){
        if(res==null){return;}
        res.data.entrySet().forEach((e) -> {
            data.put(e.getKey(), e.getValue());
        });
    }
    
    public void readData(LittleEndianDataInputStream is) throws IOException {
        for(byte t:ENUM_RESOURCE_TYPE.getKeys()){
            data.put(t, is.readLong());
        }
    }

    public void writeData(LittleEndianDataOutputStream os) throws IOException {
        for(byte t:ENUM_RESOURCE_TYPE.getKeys()){
            os.writeLong(data.get(t));
        }
    }

    public void writeDataEmpty(LittleEndianDataOutputStream os) throws IOException {
        for(byte t:ENUM_RESOURCE_TYPE.getKeys()){
            os.writeLong(0L);
        }
    }
}