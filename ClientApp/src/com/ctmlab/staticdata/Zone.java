/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ctmlab.staticdata;
import java.util.ArrayList;
/**
 *
 * @author hhtri
 */
public class Zone{
    
   
    public short id;
    public short id_r;
    public String name;
    public ArrayList<Integer> pos;
    public ArrayList<DecorLayer> layerDecor = new ArrayList<>();    

    public Zone(){        
        id = 0;
        pos = new ArrayList();
        layerDecor = new ArrayList<>();
    }
}