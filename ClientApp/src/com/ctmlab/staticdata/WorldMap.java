/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ctmlab.staticdata;

import com.ctmlab.game.data.ctmon.CTMONDataFieldType;
import com.ctmlab.model.GameObjectMap;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author hhtri
 */
public class WorldMap{
    public String name;
    public String version;
    public HashMap<String, ArrayList<Integer>> metadata;
    public HashMap<Short, Region> regions;
    
    @CTMONDataFieldType(ignore = true)
    public final HashMap<Short, GameObjectMap> lstTradeCenter = new HashMap();

    public WorldMap(){
        name = "";
        version = "0.0.0";
        metadata = new HashMap();
        regions = new HashMap<>();
    }
    
    public WorldMap copyStatic(){
        WorldMap m = new WorldMap();
        m.version = version;
        m.metadata = new HashMap();
        m.regions = new HashMap();
        
        for(Map.Entry<String, ArrayList<Integer>>e:metadata.entrySet()){
            ArrayList<Integer> ls = new ArrayList<>(e.getValue());
            m.metadata.put(e.getKey(), ls);
        }
        
        for(Region item:regions.values()){
            Region r = new Region(item.id, item.pos.get(0), item.pos.get(1), item.name);
            m.regions.put(item.id, r);
        }
        
        return m;
    }
    
    public Region getRegion(short id) {
        Region rs = regions.get(id);
        return rs;
    }

    public Zone getZone(short id) {
        Zone rs = null;
        for(Region r:regions.values()){
            if(r.zones.containsKey(id)){
                rs = r.zones.get(id);
            }
            if(rs!=null){
                break;
            }
        }
        return rs;
    }
}