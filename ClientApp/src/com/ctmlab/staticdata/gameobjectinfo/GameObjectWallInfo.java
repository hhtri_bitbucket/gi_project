/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ctmlab.staticdata.gameobjectinfo;

import com.ctmlab.enums.ENUM_UNIT_TYPE;
import com.ctmlab.manager.Utils;
import com.google.common.io.LittleEndianDataInputStream;
import com.google.common.io.LittleEndianDataOutputStream;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSyntaxException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author hhtri
 */
public class GameObjectWallInfo extends GameObjectInfo {

    public String version;
    public String name;
    public int maxLevel;
    public ArrayList<HashMap<Byte, Long>> info;
    public ArrayList<UpgradeInfo> upgrade_require;

    public GameObjectWallInfo() {
        super();
        mType = (byte) ENUM_UNIT_TYPE.HOME_WALL.getCode();
    }

    @Override
    public void setName(String name){
        this.name = name;
    }
    
    @Override
    public void setVersion(String version){
        this.version = version;
    }
    
    @Override
    public void setMaxLevel(int max_lvl){
        this.maxLevel = max_lvl;
    }
    
    @Override
    public void setInfo(HashMap<Byte, Long>[] values){
        if(info==null){info=new ArrayList<>();}
        for(HashMap<Byte, Long> h:values){
            info.add(h);
        }
    }
    
    @Override
    public void setReq(UpgradeInfo[] req){
        if(upgrade_require==null){
            upgrade_require=new ArrayList<>();
        }
        for(UpgradeInfo ui: req){
            upgrade_require.add(ui);
        }
    }

     @Override
    public void writeData(LittleEndianDataOutputStream os) throws IOException{
        super.writeData(os);
        Utils.i().writeBigString(os, version);
        Utils.i().writeBigString(os, name);
        os.writeInt(maxLevel);
        os.writeInt(info.size());
        for(HashMap<Byte, Long> h:info){
            os.writeInt(h.size());
            for(Map.Entry<Byte, Long> e:h.entrySet()){
                os.writeByte(e.getKey());
                os.writeLong(e.getValue());
            }
        }
        os.writeInt(upgrade_require.size());
        for(UpgradeInfo ui:upgrade_require){
            ui.writeData(os);
        }
    }
    
    @Override
    public void readData(LittleEndianDataInputStream is) throws IOException{
        super.readData(is);
        version = Utils.i().readBigString(is);
        name = Utils.i().readBigString(is);
        maxLevel = is.readInt();
        int len = is.readInt();
        int i = 0;
        info = new ArrayList<>();
        while(i++<len){
            HashMap<Byte, Long> h = new HashMap<>();
            int n = is.readInt();
            int j = 0;
            while(j++<n){
                h.put(is.readByte(), is.readLong());
            }
            info.add(h);
        }
        len = is.readInt();
        i = 0;
        upgrade_require = new ArrayList<>();
        while(i++<len){
            UpgradeInfo ui = new UpgradeInfo();
            ui.readData(is);
            upgrade_require.add(ui);
        }
    }
}
