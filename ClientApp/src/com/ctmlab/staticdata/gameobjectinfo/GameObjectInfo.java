/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ctmlab.staticdata.gameobjectinfo;

import com.ctmlab.enums.ENUM_UNIT_TYPE;
import com.google.common.io.LittleEndianDataInputStream;
import com.google.common.io.LittleEndianDataOutputStream;
import com.google.gson.JsonObject;
import java.io.IOException;
import java.util.HashMap;

/**
 *
 * @author hhtri
 */
public class GameObjectInfo {
    public byte mType;
    public short width;
    public short height;
    
    public GameObjectInfo(){
        width = height = 0;
    }
    
    public GameObjectInfo(byte mType){
        this.mType = mType;
        width = height = 0;
    }
    
    public void writeData(LittleEndianDataOutputStream os) throws IOException{
        os.writeByte(mType);
        os.writeShort(width);
        os.writeShort(height);
    }
    public void readData(LittleEndianDataInputStream is) throws IOException{
        width = is.readShort();
        height = is.readShort();    
    }
    
    public void readData(JsonObject obj) throws IOException{
        mType = obj.get("mType").getAsByte();
        width = obj.get("width").getAsShort();
        height = obj.get("height").getAsShort();
    }
    
    public static GameObjectInfo readDecorData(LittleEndianDataInputStream is) throws IOException{
        byte t = is.readByte();
        GameObjectInfo clazz = new GameObjectInfo(t);
        clazz.readData(is);
        return clazz;
    }
    
    public void setSize(short w, short h){
        width = w;
        height = h;
    }
    
    public void setName(String name){}
    
    public void setVersion(String version){}
    
    public void setMaxLevel(int max_lvl){}
    
    public void setInfo(HashMap<Byte, Long>[] values){}
    
    public void setReq(UpgradeInfo[] req){}
    
    public static GameObjectInfo readObjectData(LittleEndianDataInputStream is) throws IOException{
        byte t = is.readByte();
        ENUM_UNIT_TYPE unit = ENUM_UNIT_TYPE.get(t);
        GameObjectInfo clazz = null;
        switch (unit) {
            case HOME_OFFICE:{
                clazz = new GameObjectOfficeInfo();
                clazz.readData(is);
                break;
            }
            case HOME_EXPLORATION:{
                clazz = new GameObjectExplorationInfo();
                clazz.readData(is);
                break;
            }
            case HOME_TRANSPORTATION:{
                clazz = new GameObjectTransportationInfo();
                clazz.readData(is);
                break;
            }
            default: GameObjectInfo: {
                clazz = new GameObjectInfo(t);
                clazz.readData(is);
            }
        }
        return clazz;
    }    
}
