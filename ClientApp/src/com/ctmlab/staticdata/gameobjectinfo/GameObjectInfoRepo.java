/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ctmlab.staticdata.gameobjectinfo;

import com.ctmlab.manager.Utils;
import com.google.common.io.LittleEndianDataInputStream;
import java.io.IOException;
import java.util.TreeMap;

/**
 *
 * @author hhtri
 */
public class GameObjectInfoRepo {
    public String version;
    public TreeMap<Byte, GameObjectInfo> lstObjectMap;
    public TreeMap<Byte, GameObjectInfo> lstObjectDecor;
    
    
    public GameObjectInfoRepo(){
        version = "";
        lstObjectMap = new TreeMap<>();
        lstObjectDecor = new TreeMap<>();
    }
    
    public void readGameObjectInfo(LittleEndianDataInputStream is ) throws IOException{
        version = Utils.i().readBigString(is);            
        int len = is.readInt();
        int i = 0;

        lstObjectMap.clear();
        while(i++<len){
            byte key = is.readByte();
            GameObjectInfo clazz = GameObjectInfo.readObjectData(is);                
            if(clazz!=null){
                lstObjectMap.put(key, clazz);
            }
        }
        len = is.readInt();
        i = 0;
        lstObjectDecor.clear();
        while(i++<len){
            byte key = is.readByte();
            GameObjectInfo clazz = GameObjectInfo.readDecorData(is);       
            if(clazz!=null){
                lstObjectDecor.put(key, clazz);
            }
        }    
    }
}
