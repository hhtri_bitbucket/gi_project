/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ctmlab.staticdata;

import com.google.common.io.LittleEndianDataInputStream;
import com.google.common.io.LittleEndianDataOutputStream;
import java.io.IOException;

/**
 *
 * @author hhtri
 */
public interface DataStreamListener {
    public void writeData(LittleEndianDataOutputStream os) throws IOException;
    public void readData(LittleEndianDataInputStream is) throws IOException;
}
