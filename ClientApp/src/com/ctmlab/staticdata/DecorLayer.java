/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ctmlab.staticdata;

import com.ctmlab.model.GameObjectMap;
import com.google.common.io.LittleEndianDataInputStream;
import com.google.common.io.LittleEndianDataOutputStream;
import java.io.IOException;
import java.util.ArrayList;

/**
 *
 * @author hhtri
 * not using
 */
public class DecorLayer implements DataStreamListener{
    public byte type;
    public byte minZoom;
    public byte maxZoom;
    public ArrayList<GameObjectMap> data;
    
    public DecorLayer(){
        type = 0;
        minZoom = 1;
        maxZoom = 10;
        data = new ArrayList<>();
    }
    
    public DecorLayer(byte type){
        this.type = type;
        minZoom = 1;
        maxZoom = 10;
        data = new ArrayList<>();
    }
    
    public void addItem(GameObjectMap item){
        data.add(item);
    }

    @Override
    public void writeData(LittleEndianDataOutputStream os) throws IOException {
        os.writeByte(type);
        os.writeByte(minZoom);
        os.writeByte(maxZoom);
        os.writeInt(data.size());
        for(GameObjectMap o:data){
            os.writeShort(o.id_r);
            os.writeShort(o.id_z);
            os.writeByte(o.mType);
            os.writeShort(o.xPos);
            os.writeShort(o.yPos);
        }
    }

    @Override
    public void readData(LittleEndianDataInputStream is) throws IOException {
        type = is.readByte();
        minZoom = is.readByte();
        maxZoom = is.readByte();
        int len = is.readInt();
        int i = 0;
        data = new ArrayList<>();
        while(i<len){
            GameObjectMap obj = new GameObjectMap();
            obj.id_r = is.readShort();
            obj.id_z = is.readShort();
            obj.mType = is.readByte();
            obj.xPos = is.readShort();
            obj.yPos = is.readShort();
            data.add(obj);
            i++;
        }
    }
}
