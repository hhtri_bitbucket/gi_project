/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ctmlab.endpoint;

import java.nio.ByteBuffer;

/**
 *
 * @author hhtri
 */
public interface EndpointListener {
    
    public void handleOnOpen();
    public void handleOnMessage(ByteBuffer data);
    public void handleOnClosed();
    
}
