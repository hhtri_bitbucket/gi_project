/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ctmlab.endpoint;

import com.google.common.io.LittleEndianDataOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.URI;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.LinkedList;
import java.util.Queue;
import java.util.logging.Logger;
import javax.websocket.ClientEndpoint;
import javax.websocket.CloseReason;
import javax.websocket.ContainerProvider;
import javax.websocket.OnClose;
import javax.websocket.OnError;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.SendHandler;
import javax.websocket.SendResult;
import javax.websocket.Session;
import javax.websocket.WebSocketContainer;

/**
 *
 * @author hhtri
 */
@ClientEndpoint
public class WSClientEndpoint implements SendHandler {

    private static final Object LOCK = new Object();
    private final Queue<byte[]> data_queue = new LinkedList();
    private final Object SENDING_LOCK = new Object();
    private boolean sending = false;
    private URI endpointURI=null;
    private boolean isConnect = false;    
    private Session session = null;
    private EndpointListener listener = null;
    
    public WSClientEndpoint(URI _endpointURI){
        endpointURI = _endpointURI;
    }
    
    public void sendMessage(byte[] message) {
        addToBuffer(message);
    }
    
    public void addListener(EndpointListener l){
        listener = l;
    }
    
    private void addToBuffer(byte[] data) {
        if (session == null) {
            return;
        }
        synchronized (data_queue) {
            data_queue.add(data);
        }
        notifySend();
    }
    
    public void connect() {
        try {
            WebSocketContainer container = ContainerProvider.getWebSocketContainer();
            container.connectToServer(this, endpointURI);
            isConnect = true;
        } catch (Exception ex) {
            session = null;
            isConnect = false;
        }
    }
    
    public boolean isConnected(){return isConnect;}

    public void disconnect() {
        try {
            if (session != null) {
                session.close();
            }
        } catch (IOException ex) {
        }        
    }
    
    public void notifySend() {       
        ByteBuffer buffer;
        synchronized (data_queue) {
            if (data_queue.isEmpty()) {
                return;
            }
            synchronized (SENDING_LOCK) {
                if (sending) {
                    return;
                }
                sending = true;
            }
            buffer = ByteBuffer.wrap(data_queue.poll());
        }
        if (session.isOpen()) {
            try {
                synchronized (buffer) {
                    buffer.rewind();
                    session.getAsyncRemote().sendBinary(buffer, this);
                }
            } catch (java.lang.IllegalStateException ex) {
                try {session.close();}catch(IOException ex_close){}
            }
        }
    }

    @OnOpen
    public void onOpen(Session userSession) {
//        System.out.println("onOpen");
        session = userSession;        
//        System.out.println("Opened - " + session.getMaxBinaryMessageBufferSize());
        if(listener!=null){
            listener.handleOnOpen();
        }
    }

    @OnClose
    public void onClose(Session userSession, CloseReason reason) {
//        System.out.println("onClose - " + reason);
       if(listener!=null){
            listener.handleOnClosed();
        }
        isConnect = false;
        session = null;
    }
    
    @OnError
    public void onError(Throwable t) {
//        System.out.println("onError");
        
        StringWriter w = new StringWriter();
        t.printStackTrace(new PrintWriter(w));
        System.out.println(w.toString());
        
        isConnect = false;
        session = null;
    }

    
    @OnMessage
    public void onMessage(ByteBuffer buff) {
//        System.out.println("onMessage");
        buff.order(ByteOrder.LITTLE_ENDIAN);
        buff.rewind();        
        if(listener!=null){
            listener.handleOnMessage(buff);
        }
    }       

    @Override
    public void onResult(SendResult result) {
        synchronized (SENDING_LOCK) {sending = false;}
        if (result.isOK()) {
            notifySend();           
        } else{
            disconnect();
        }
    }

    private static void wait4TerminateSignal() {
        synchronized (LOCK) {
            try {LOCK.wait();} catch (InterruptedException e) {}
        }
    }
}
