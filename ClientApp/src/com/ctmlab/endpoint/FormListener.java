/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ctmlab.endpoint;

/**
 *
 * @author hhtri
 */
public interface FormListener {
    public void connected();
    public void receiveMsg(int type, int order, String data);
}
