/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ctmlab.endpoint;

import com.ctmlab.app.Form;
import com.ctmlab.enums.ENUM_OBJECT_MAP_TYPE;
import com.ctmlab.enums.ENUM_UNIT_TYPE;
import com.ctmlab.enums.GI_SERVER_CMD;
import com.ctmlab.manager.Utils;
import com.ctmlab.model.Exploration;
import com.ctmlab.model.GameMessage;
import com.ctmlab.model.GameObjectMap;
import com.ctmlab.model.Mine;
import com.ctmlab.model.OilMine;
import com.ctmlab.model.SocialChat;
import com.ctmlab.model.Truck;
import com.ctmlab.model.building.Building;
import com.ctmlab.reader.PlayerProfile;
import com.ctmlab.reader.SocialAlliance;
import com.ctmlab.reader.ZoneDetail;
import com.ctmlab.staticdata.GameResources;
import com.google.common.io.LittleEndianDataInputStream;
import com.google.common.io.LittleEndianDataOutputStream;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.URI;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Date;
import java.util.Map.Entry;
import java.util.Random;
import java.util.TreeMap;

/**
 *
 * @author hhtri
 */
public class WSClient extends Thread {


    private boolean mRunning = false;
    private WSClientEndpoint endpoint = null;
    private FormListener formListener = null;
    private boolean mConnected = false;
    private String userID = "";
    private String url = "ws://localhost:8084/goldinc/giservice";
    public PlayerProfile mProfile = null;
    private int curTab = -1; 
    
    private Gson gson = new Gson();
    private Gson pson = new GsonBuilder().serializeNulls().setPrettyPrinting().create();

    public WSClient() {
    }

    public WSClient(String user, String url) {
        this.url = url;
        userID = user;
    }
    
    public String getResourceText(){
        if(mProfile!=null){
            return pson.toJson(mProfile.privateInfo.resource);
        }
        return "";
    }
    
    public String getExplorationText(){
        if(mProfile!=null){
            return pson.toJson(mProfile.privateInfo.lstExploration);
        }
        return "";
    }
    
    public String getTruckText(){
        if(mProfile!=null){
            return pson.toJson(mProfile.privateInfo.lstTransportation);
        }
        return "";
    }

    public void startWS() {
        connect();
        start();
    }

    public void stopWS() {
        mRunning = false;
        interrupt();
    }

    public void setListener(FormListener l) {
        formListener = l;
    }

    public void connect() {
        endpoint = new WSClientEndpoint(URI.create(url));
//        endpoint = new WSClientEndpoint(URI.create("ws://localhost:8084/goldinc/giservice"));
//        endpoint = new WSClientEndpoint(URI.create("ws://test.ctmlab.com/goldinc/giservice"));
        endpoint.addListener(new EndpointListener() {
            @Override
            public void handleOnOpen() {
                mConnected = true;
                formListener.connected();
            }

            @Override
            public void handleOnMessage(ByteBuffer data) {
                try {
                    processData(data);
                } catch (IOException ex) {
                    
                }
            }

            @Override
            public void handleOnClosed() {
                formListener.receiveMsg(curTab, 0, "CLOSED");
            }
        });
        endpoint.connect();
    }

    public void signIn(String user, String pass) throws IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        try (LittleEndianDataOutputStream os = new LittleEndianDataOutputStream(baos)) {
            os.writeShort(GI_SERVER_CMD.REQ_SIGN_IN.getCode());
            Utils.i().writeBigString(os, user);
            Utils.i().writeBigString(os, pass);
        }
        curTab = Form.TAB_SIGN_IN;
        if (endpoint != null) {
            endpoint.sendMessage(baos.toByteArray());
        }
    }
    
    public void ping() throws IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        try (LittleEndianDataOutputStream os = new LittleEndianDataOutputStream(baos)) {
            os.writeShort(GI_SERVER_CMD.IDLE.getCode());
        }
        if (endpoint != null) {
            endpoint.sendMessage(baos.toByteArray());
        }
    }
    
    public void refine() throws IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        try (LittleEndianDataOutputStream os = new LittleEndianDataOutputStream(baos)) {
            os.writeShort(GI_SERVER_CMD.REQ_REFINE_OIL.getCode());
            os.writeInt(1);
        }
        curTab = Form.TAB_BUILDING;
        if (endpoint != null) {
            endpoint.sendMessage(baos.toByteArray());
        }
    }
    
    public void getZoneDetail(short id_z) throws IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        try (LittleEndianDataOutputStream os = new LittleEndianDataOutputStream(baos)) {
            os.writeShort(GI_SERVER_CMD.REQ_DATA_ZONE.getCode());
            os.writeShort(id_z);
        }
        curTab = Form.TAB_SIGN_IN;
        if (endpoint != null) {
            endpoint.sendMessage(baos.toByteArray());
        }
    }
    
    public void testMine(short zoneID, long id) throws IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        try (LittleEndianDataOutputStream os = new LittleEndianDataOutputStream(baos)) {
            os.writeShort(GI_SERVER_CMD.REQ_TEST_MINE.getCode());
            os.writeShort(zoneID);
            os.writeLong(id);
        }
        curTab = Form.TAB_SIGN_IN;
        if (endpoint != null) {
            endpoint.sendMessage(baos.toByteArray());
        }
    }
    
    public void pegMine(short zoneID, long id) throws IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        try (LittleEndianDataOutputStream os = new LittleEndianDataOutputStream(baos)) {
            os.writeShort(GI_SERVER_CMD.REQ_PEG_MINE.getCode());
            os.writeShort(zoneID);
            os.writeLong(id);
        }
        curTab = Form.TAB_SIGN_IN;
        if (endpoint != null) {
            endpoint.sendMessage(baos.toByteArray());
        }
    }
    
    public void setGameName(String name) throws IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        try (LittleEndianDataOutputStream os = new LittleEndianDataOutputStream(baos)) {
            os.writeShort(GI_SERVER_CMD.REQ_SET_NAME.getCode());
            Utils.i().writeBigString(os, name);
        }
        curTab = Form.TAB_SIGN_IN;
        if (endpoint != null) {
            endpoint.sendMessage(baos.toByteArray());
        }
    }
    
    public void updateWalletClient() throws IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        try (LittleEndianDataOutputStream os = new LittleEndianDataOutputStream(baos)) {
            os.writeShort(GI_SERVER_CMD.REQ_UPDATE_WALLET.getCode());
        }
        curTab = Form.TAB_SIGN_IN;
        if (endpoint != null) {
            endpoint.sendMessage(baos.toByteArray());
        }
    }

    public void sendMsgGame(String receiver, String subject, String content) throws IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        try (LittleEndianDataOutputStream os = new LittleEndianDataOutputStream(baos)) {
            os.writeShort(GI_SERVER_CMD.REQ_SEND_MESSAGE.getCode());
            Utils.i().writeBigString(os, receiver);
            Utils.i().writeBigString(os, subject);
            Utils.i().writeBigString(os, content);
        }
        curTab = Form.TAB_MSG;
        if (endpoint != null) {
            endpoint.sendMessage(baos.toByteArray());
        }
    }

    public void getMsgGame() throws IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        try (LittleEndianDataOutputStream os = new LittleEndianDataOutputStream(baos)) {
            os.writeShort(GI_SERVER_CMD.REQ_GET_MESSAGE.getCode());
        }
        curTab = Form.TAB_MSG;
        if (endpoint != null) {
            endpoint.sendMessage(baos.toByteArray());
        }
    }

    public void rmMsgGame(long id) throws IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        try (LittleEndianDataOutputStream os = new LittleEndianDataOutputStream(baos)) {
            os.writeShort(GI_SERVER_CMD.REQ_REMOVE_MESSAGE.getCode());
            os.writeLong(id);
        }
        curTab = Form.TAB_MSG;
        if (endpoint != null) {
            endpoint.sendMessage(baos.toByteArray());
        }
    }

    public void createAlliance(String name) throws IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        try (LittleEndianDataOutputStream os = new LittleEndianDataOutputStream(baos)) {
            os.writeShort(GI_SERVER_CMD.REQ_CREATE_ALLIANCE.getCode());
            Utils.i().writeBigString(os, name);
        }
        curTab = Form.TAB_CHAT;
        if (endpoint != null) {
            endpoint.sendMessage(baos.toByteArray());
        }
    }
    
    public void chatReion(short id_r, String message) throws IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        try (LittleEndianDataOutputStream os = new LittleEndianDataOutputStream(baos)) {
            os.writeShort(GI_SERVER_CMD.REQ_CHAT_REGION.getCode());
            os.writeShort(id_r);
            Utils.i().writeBigString(os, message);
        }
        curTab = Form.TAB_CHAT;
        if (endpoint != null) {
            endpoint.sendMessage(baos.toByteArray());
        }
    }
    
    public void buyingResource() throws IOException {  
        Random rd = new Random();
        int type = rd.nextInt(12) + 1;
        long amount = rd.nextInt(1000) + 1;
        TreeMap<Byte, Long> map = new TreeMap();
        map.put((byte)type, amount);
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        try (LittleEndianDataOutputStream os = new LittleEndianDataOutputStream(baos)) {
            os.writeShort(GI_SERVER_CMD.REQ_BUY_RESOURCE.getCode());
            os.writeInt(map.size());
            for(Entry<Byte, Long> e:map.entrySet()){
                os.writeByte(e.getKey());
                os.writeLong(e.getValue());
                System.out.println("buying => " + e.getKey()+"_"+e.getValue());
            }
        }
        curTab = Form.TAB_TRADE_CENTER;
        if (endpoint != null) {
            endpoint.sendMessage(baos.toByteArray());
        }
    }
    
    public void sellingResource() throws IOException {
        Random rd = new Random();
        int type = rd.nextInt(12) + 1;
        long amount = rd.nextInt(1000) + 1;
        TreeMap<Byte, Long> map = new TreeMap();
        map.put((byte)type, amount);
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        try (LittleEndianDataOutputStream os = new LittleEndianDataOutputStream(baos)) {
            os.writeShort(GI_SERVER_CMD.REQ_SELL_RESOURCE.getCode());
            os.writeInt(map.size());
            for(Entry<Byte, Long> e:map.entrySet()){
                os.writeByte(e.getKey());
                os.writeLong(e.getValue());
                System.out.println("selling => " + e.getKey()+"_"+e.getValue());
            }
        }
        curTab = Form.TAB_TRADE_CENTER;
        if (endpoint != null) {
            endpoint.sendMessage(baos.toByteArray());
        }
    }
    
    public void reqCashInfo() throws IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        try (LittleEndianDataOutputStream os = new LittleEndianDataOutputStream(baos)) {
            os.writeShort(GI_SERVER_CMD.REQ_TRADE_CASH_INFO.getCode());
        }
        curTab = Form.TAB_TRADE_CENTER;
        if (endpoint != null) {
            endpoint.sendMessage(baos.toByteArray());
        }
    }
    
    public void reqGetAllBuilding() throws IOException{
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        try (LittleEndianDataOutputStream os = new LittleEndianDataOutputStream(baos)) {
            os.writeShort(GI_SERVER_CMD.REQ_PLAYER_BUILDING.getCode());
            Utils.i().writeBigString(os, "snipertest001@gmail.com");
        }
        curTab = Form.TAB_BUILDING;
        if (endpoint != null) {
            endpoint.sendMessage(baos.toByteArray());
        }
    }
    
    public void reqUpgradeBuilding(byte type) throws IOException{
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        try (LittleEndianDataOutputStream os = new LittleEndianDataOutputStream(baos)) {
            os.writeShort(GI_SERVER_CMD.REQ_UPGRADE_BUILDING.getCode());
            os.writeByte(type);
        }
        curTab = Form.TAB_BUILDING;
        if (endpoint != null) {
            endpoint.sendMessage(baos.toByteArray());
        }
    }
    
    public void reqMovingToClaim(short zoneID, long mineID, ArrayList<Long> lstEquip, ArrayList<Long> lstWorker, byte type_speed) throws IOException{
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        try (LittleEndianDataOutputStream os = new LittleEndianDataOutputStream(baos)) {
            os.writeShort(GI_SERVER_CMD.REQ_TRANSPORT_TO_CLAIM.getCode());
            os.writeShort(zoneID);
            os.writeLong(mineID);
            os.writeInt(lstEquip.size());
            for(Long l:lstEquip){os.writeLong(l);}
            os.writeInt(lstWorker.size());
            for(Long l:lstWorker){os.writeLong(l);}
            os.writeByte(type_speed);
        }
        curTab = Form.TAB_TRADE_CENTER;
        if (endpoint != null) {
            endpoint.sendMessage(baos.toByteArray());
        }
    }
    
    public void reqMovingFuelToClaim(short zoneID, long mineID, int numberOfFuel, byte type_speed) throws IOException{
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        try (LittleEndianDataOutputStream os = new LittleEndianDataOutputStream(baos)) {
            os.writeShort(GI_SERVER_CMD.REQ_TRANSPORT_FUEL.getCode());
            os.writeShort(zoneID);
            os.writeLong(mineID);
            os.writeInt(numberOfFuel);
            os.writeByte(type_speed);
        }
        curTab = Form.TAB_BUILDING;
        if (endpoint != null) {
            endpoint.sendMessage(baos.toByteArray());
        }
    }
    
    public void attactOilMine(short zoneID, long mineID) throws IOException{
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        try (LittleEndianDataOutputStream os = new LittleEndianDataOutputStream(baos)) {
            os.writeShort(GI_SERVER_CMD.REQ_ATTACK_OIL_RIG.getCode());
            os.writeShort(zoneID);
            os.writeLong(mineID);
        }
        curTab = Form.TAB_TRADE_CENTER;
        if (endpoint != null) {
            endpoint.sendMessage(baos.toByteArray());
        }
    }

    private String readSignIn(LittleEndianDataInputStream is) throws IOException {
        byte rs = is.readByte();
        if (rs == 0) {
            mProfile = new PlayerProfile();
            mProfile.readData(is);
            userID = mProfile.userID;
//            return pson.toJson(mProfile);
            return mProfile.printLog(gson, pson);
        }
        return "SignIn is failed or duplicated SignIn";
    }

    private String readUpdateProfile(LittleEndianDataInputStream is) throws IOException {        
        PlayerProfile p = new PlayerProfile();
        p.readData(is);
        mProfile = p;        
        return pson.toJson(p);
    }
    
    private String readZoneDetail(LittleEndianDataInputStream is) throws IOException {    

            ZoneDetail zd = new ZoneDetail();
            zd.readData(is);    
        return pson.toJson(zd);
    }
    
    private String readTestMine(LittleEndianDataInputStream is) throws IOException {        
        
        StringBuilder sb = new StringBuilder();
        byte rs = is.readByte();
        sb.append(rs);
        Exploration ex = new Exploration();
        ex.readData(is);
        sb.append("\n").append(pson.toJson(ex));
        return sb.toString();
    }
    
    private String readPegMine(LittleEndianDataInputStream is) throws IOException {        
        
        StringBuilder sb = new StringBuilder();
        byte rs = is.readByte();
        if(rs==0){
            ENUM_OBJECT_MAP_TYPE type = ENUM_OBJECT_MAP_TYPE.get((int)is.readByte());
            GameObjectMap temp = null;
            switch(type){
                case MINE:{temp = new Mine();break;}
                case OIL_MINE:{temp = new OilMine();break;}
            }
            if(temp!=null){
                temp.readData(is);
                sb.append(pson.toJson(temp));
            }
            sb.append("\nPublic Profile\n");
            PlayerProfile p = new PlayerProfile();
            p.readData(is);
            sb.append(pson.toJson(p));
        }
        return sb.toString();
    }
    
    private String readMinFinfo(LittleEndianDataInputStream is) throws IOException {        
        
        StringBuilder sb = new StringBuilder();
        ENUM_OBJECT_MAP_TYPE type = ENUM_OBJECT_MAP_TYPE.get((int)is.readByte());
        GameObjectMap temp = null;
        switch(type){
            case MINE:{temp = new Mine();break;}
            case OIL_MINE:{temp = new OilMine();break;}
        }
        if(temp!=null){
            temp.readData(is);
            sb.append(pson.toJson(temp));
        }
        sb.append("\nPublic Profile\n");
        PlayerProfile p = new PlayerProfile();
        p.readData(is);
        sb.append(pson.toJson(p));
        return sb.toString();
    }
    
    private String readChatRegion(LittleEndianDataInputStream is) throws IOException {       
        
        StringBuilder sb = new StringBuilder();
        SocialChat sc = new SocialChat();
        sc.readData(is);
        sb.append(String.format("[%s:%s]:%s\n", sc.sender, sc.time, sc.data));
        return sb.toString();
    }
    
    private void processData(ByteBuffer data) throws IOException {
        byte[] a = new byte[data.remaining()];
        data.get(a);
        try (LittleEndianDataInputStream is = new LittleEndianDataInputStream(new ByteArrayInputStream(a))) {

            GI_SERVER_CMD cmd = GI_SERVER_CMD.fromID(is.readShort());
            switch (cmd) {
                case RES_SIGN_IN: {
                    String rs = readSignIn(is);
                    formListener.receiveMsg(curTab, 0, "RES_SIGN_IN: " + data.array().length + "\n" + rs);
                    break;
                }
                case RES_UPDATE_PROFILE: {
                    String rs = readUpdateProfile(is);
                    formListener.receiveMsg(curTab, 0, "RES_UPDATE_PROFILE: " + data.array().length + "\n" + rs);
                    break;
                }
                case RES_DATA_ZONE: {
                    String rs = readZoneDetail(is);
                    formListener.receiveMsg(curTab, 0, "RES_DATA_ZONE: " + data.array().length + "\n" + rs);
                    break;
                }
                case RES_TEST_MINE: {
                    String rs = readTestMine(is);
                    formListener.receiveMsg(curTab, 0, "RES_TEST_MINE: " + data.array().length + "\n" + rs);
                    break;
                }
                case RES_MINE_INFO: {
                    String rs = readMinFinfo(is);
                    formListener.receiveMsg(curTab, 0, "RES_MINE_INFO: " + data.array().length + "\n" + rs);
                    break;
                }
                case RES_PEG_MINE: {
                    String rs = readPegMine(is);
                    formListener.receiveMsg(curTab, 0, "RES_PEG_MINE: " + data.array().length + "\n" + rs);
                    break;
                }
                case RES_UPDATE_SPOT: {
                    String rs = readZoneDetail(is);
                    formListener.receiveMsg(curTab, 0, "RES_UPDATE_SPOT: " + data.array().length + "\n" + rs);
                    break;
                }
                case RES_UPDATE_WALLET: {
                    String rs = is.readBoolean()?"Update wallet is successful.":"Update wallet is failed.";
                    formListener.receiveMsg(curTab, 0, "RES_UPDATE_WALLET: " + data.array().length + "\n" + rs);
                    break;
                }
                case RES_SET_NAME: {
                    String rs = is.readBoolean()?"Setting name is successful.":"Setting name is failed.";
                    formListener.receiveMsg(curTab, 0, "RES_SET_NAME: " + data.array().length + "\n" + rs);
                    break;
                }
                case RES_SEND_MESSAGE: {                    
                    formListener.receiveMsg(curTab, 0, "RES_SEND_MESSAGE:\nResult is " + is.readByte());
                    break;
                }
                case RES_GET_MESSAGE: {
                    int len = is.readInt();
                    int i = 0;
                    ArrayList<GameMessage> ls = new ArrayList();
                    while (i++ < len) {
                        GameMessage gm = new GameMessage();
                        gm.readData(is);
                        ls.add(gm);
                    }
                    formListener.receiveMsg(curTab, 0, "RES_SEND_MESSAGE:\n" + pson.toJson(ls));
                    break;
                }
                case RES_CREATE_ALLIANCE: {
                    byte rs = is.readByte();
                    if (rs==0) {
                        SocialAlliance sa = new SocialAlliance();
                        sa.readData(is);
                        formListener.receiveMsg(curTab, 0, "RES_CREATE_ALLIANCE:\n" + pson.toJson(sa));
                    } else{
                        formListener.receiveMsg(curTab, 0, "RES_CREATE_ALLIANCE:\n" + rs);
                    }
                    break;
                }
                case RES_CHAT_REGION:{
                    String s = readChatRegion(is);
                    formListener.receiveMsg(curTab, 2, s);
                    break;
                }
                case RES_BUY_RESOURCE: {
                    byte rs = is.readByte();
                    if (rs==0) {
                        Truck t = new Truck();
                        t.readData(is);
                        
                        String s = "Buy resource success. Waiting " + (t.endTime-t.startTime)/1000 + "s transport cash.";
                        String text = "\n***********\n" + s;
                        formListener.receiveMsg(curTab, 0, "RES_BUY_RESOURCE:" + text);
                    } else{
                        formListener.receiveMsg(curTab, 0, "RES_BUY_RESOURCE:" + rs);
                    }
                    break;
                }
                case RES_SELL_RESOURCE: {
                    byte rs = is.readByte();
                    if (rs==0) {
                        Truck t = new Truck();
                        t.readData(is);
                        
                        String s = "Sell resource success. Waiting " + (t.endTime-t.startTime)/1000 + "s transport cash.";
                        
                        String text = "\n***********\n" + s;
                        formListener.receiveMsg(curTab, 0, "RES_SELL_RESOURCE:" + text);
                    } else{
                        String str = "\n***********\nResult - " + rs + "\n";
                        int len = is.readInt();
                        int i=0;
                        while(i++<len){
                            str += is.readByte() + " - " + is.readLong() + "\n";
                        } 
                        formListener.receiveMsg(curTab, 0, "RES_SELL_RESOURCE:" + str);
                    }
                    break;
                }
                case RES_TRANSPORT_TO_CLAIM: {
                    String log = "";
                    byte rs = is.readByte();
                    if(rs==0){
                        Truck t = new Truck();
                        t.readData(is);
                        log += String.format("Truck info:\n%s\ntime:%d", pson.toJson(t), (t.endTime-t.startTime));
                    } else{
                        log = String.format("[rs]-[%d]", rs);
                    }
                    formListener.receiveMsg(curTab, 0, "RES_TRANSPORT_TO_CLAIM: " + data.array().length + "\n" + log);
                    break;
                }
                case RES_PLAYER_BUILDING: {
                    String log = "";
                    int size = is.readInt();
                    int i = 0;
                    log += String.valueOf(size) + "\n";
                    while(i++<size){
                        log += gson.toJson(Building.readStatic(is)) + "\n";
                    }
                    formListener.receiveMsg(curTab, 0, "RES_PLAYER_BUILDING: " + data.array().length + "\n" + log);
                    break;
                }
                case RES_UPGRADE_BUILDING: {
                    String log = "";
                    byte rs = is.readByte();
                    if(rs==0){
                        log = String.format("%s", pson.toJson(Building.readStatic(is)));
                    } else{
                        log = String.format("[rs]-[%d]", rs);
                    }
                    formListener.receiveMsg(curTab, 0, "RES_UPGRADE_BUILDING: " + data.array().length + "\n" + log);
                    break;
                }
                case RES_REFINE_OIL:{
                    String log = "";
                    byte rs = is.readByte();
                    log = String.format("[rs]-[%d]", rs);
                    formListener.receiveMsg(curTab, 0, "RES_REFINE_OIL: " + data.array().length + "\n" + log);
                    break;
                }
                case RES_TRANSPORT_FUEL:{
                    String log = "";
                    byte rs = is.readByte();
                    if(rs==0){
                        Truck t = new Truck();
                        t.readData(is);
                        log += String.format("Truck info:\n%s\ntime:%d", pson.toJson(t), (t.endTime-t.startTime));
                    } else{
                        log = String.format("[rs]-[%d]", rs);
                    }
                    formListener.receiveMsg(curTab, 0, "RES_REFINE_OIL: " + data.array().length + "\n" + log);
                    break;
                }
                case RES_CLAIMS_INFO:{
                    String log = "";                    
                    int len = is.readInt();
                    int i = 0;
                    while(i++<len){
                        String temp = "";
                        temp += String.format("id:%d\n",is.readLong()); 
                        temp += String.format("fuel:%d\n",is.readLong()); 
                        int lenSpot = is.readInt();
                        temp += String.format("Number of Spot Dirt:%d\n",lenSpot); 
                        int j = 0;
                        long[] dataSpot = new long[lenSpot];
                        while(j<lenSpot){dataSpot[j++] = is.readLong();   }
                        temp += String.format("spot:%s\n", gson.toJson(dataSpot)); 
                        if(is.readBoolean()){
                            temp += String.format("dirt:%s\n", is.readLong()); 
                        }
                        if(is.readBoolean()){
                            GameResources res = new GameResources();
                            res.readData(is);
                            temp += String.format("res:%s\n", gson.toJson(res)); 
                        }
                        log += temp + "\n";
                    }
                    
                    formListener.receiveMsg(curTab, 0, "RES_CLAIMS_INFO: " + data.array().length + "\n" + log);
                    break;
                }
                case RES_ATTACK_OIL_RIG:{
                    String log = "";
                    byte rs = is.readByte();
                    if(rs==0){
                        Truck t = new Truck();
                        t.readData(is);
                        log += String.format("Truck info:\n%s\ntime:%d", pson.toJson(t), (t.endTime-t.startTime));
                    } else{
                        log = String.format("[rs]-[%d]", rs);
                    }
                    formListener.receiveMsg(curTab, 0, "RES_ATTACK_OIL_RIG: " + data.array().length + "\n" + log);
                    break;
                }
                case RES_OIL_RIG_INFO:{
                    String log = "Value: " + is.readLong();                   
                    formListener.receiveMsg(curTab, 0, "RES_OIL_RIG: " + data.array().length + "\n" + log);
                    break;
                }
                case RES_USER_RESOURCE:{
                    GameResources res = new GameResources();
                    res.readData(is);
                    String log = String.format("%s", pson.toJson(res));
                    formListener.receiveMsg(curTab, 0, "RES_USER_RESOURCE: " + data.array().length + "\n" + log);
                    break;
                }
                case RES_UPDATE_BUILDING: {
                    String log = String.format("%s", pson.toJson(Building.readStatic(is)));
                    formListener.receiveMsg(curTab, 0, "RES_UPDATE_BUILDING: " + data.array().length + "\n" + log);
                    break;
                }
            }
            if(cmd!=GI_SERVER_CMD.IDLE){
                String log = String.format("%s - client application recv: %d", Form.sdf.format(new Date()), data.array().length);
                System.out.println(log);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public void run() {
        mRunning = true;
        int count = 0;
        while (mRunning) {
            try {
                Thread.sleep(1000);
//                count++;
//                if(count==3){
//                    count = 0;
//                    try {
//                        ping();
//                    } catch (IOException ex) {
//
//                    }
//                }
                
            } catch (InterruptedException ex) {
            }
        }
        endpoint.disconnect();
        formListener.receiveMsg(curTab, 0, "Client is disconnected");
    }
}
