/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ctmlab.endpoint;

import com.ctmlab.app.Form;
import com.ctmlab.enums.GI_MICROSERVICE_CMD;
import com.ctmlab.game.data.ctmon.CTMON;
import com.ctmlab.manager.Utils;
import com.ctmlab.staticdata.Region;
import com.ctmlab.staticdata.WorldMap;
import com.ctmlab.staticdata.gameobjectinfo.GameObjectInfoRepo;
import com.google.common.io.LittleEndianDataInputStream;
import com.google.common.io.LittleEndianDataOutputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.URI;
import java.nio.ByteBuffer;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author hhtri
 */
public class WSClientService  extends Thread {


    private boolean mRunning = false;
    private WSClientEndpoint endpoint = null;
    private boolean mConnected = false;
//    private String url = "ws://test.ctmlab.com/ServiceStaticData/downloads";
    private String url = "ws://localhost:8080/ServiceStaticData/downloads";
    
    public WSClientService() {
    }

    public void startWS() {
        connect();
        start();
    }

    public void stopWS() {
        mRunning = false;
        interrupt();
    }
    public void connect() {
        endpoint = new WSClientEndpoint(URI.create(url));
        endpoint.addListener(new EndpointListener() {
            @Override
            public void handleOnOpen() {
                mConnected = true;
                try {
                    requestConnect();
                } catch (IOException ex) {
                    Logger.getLogger(WSClientService.class.getName()).log(Level.SEVERE, null, ex);
                }
            }

            @Override
            public void handleOnMessage(ByteBuffer data) {
                try {
                    processData(data);
                } catch (IOException ex) {
                    
                }
            }

            @Override
            public void handleOnClosed() {
                mConnected = false;
                mRunning = false;
            }
        });
        endpoint.connect();
    }   
    
    public void ping() throws IOException {
//        ByteArrayOutputStream baos = new ByteArrayOutputStream();
//        try (LittleEndianDataOutputStream os = new LittleEndianDataOutputStream(baos)) {
//            os.writeShort(GI_SERVER_CMD.IDLE.getCode());
//        }
//        if (endpoint != null) {
//            endpoint.sendMessage(baos.toByteArray());
//        }
    }
    
    public void requestConnect() throws IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        try (LittleEndianDataOutputStream os = new LittleEndianDataOutputStream(baos)) {
            os.writeShort(GI_MICROSERVICE_CMD.REQ_CONNECT.getCode());
        }
        if (endpoint != null) {
            endpoint.sendMessage(baos.toByteArray());
        }
    }
    
    private String responseConnect(LittleEndianDataInputStream is) throws IOException {
        StringBuilder sb = new StringBuilder();
        String worlmap = Utils.i().readBigString(is);
        String gameobjectinfo = Utils.i().readBigString(is);
        sb.append(String.format("[%s - %s]\n[%s]\n[%s]\n", "responseConnect", Utils.i().now(), worlmap, gameobjectinfo));
        return sb.toString();
    }
    
    public void requestGameObjectInfo() throws IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        try (LittleEndianDataOutputStream os = new LittleEndianDataOutputStream(baos)) {
            os.writeShort(GI_MICROSERVICE_CMD.REQ_GAMEOBJECT_INFO.getCode());
        }
        if (endpoint != null) {
            endpoint.sendMessage(baos.toByteArray());
        }
    }
    
    private String responseGameObjectInfo(LittleEndianDataInputStream is) throws IOException {
        StringBuilder sb = new StringBuilder();
        GameObjectInfoRepo rp = new GameObjectInfoRepo();
        int len = is.readInt();
        rp.readGameObjectInfo(is);
        sb.append(String.format("[%s - %s]\n%s\n", "responseGameObjectInfo", Utils.i().now(), Utils.i().gson.toJson(rp)));
        return sb.toString();
    }
        
    public void requestMapInfo() throws IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        try (LittleEndianDataOutputStream os = new LittleEndianDataOutputStream(baos)) {
            os.writeShort(GI_MICROSERVICE_CMD.REQ_MAP_INFO.getCode());
        }
        if (endpoint != null) {
            endpoint.sendMessage(baos.toByteArray());
        }
    }
    
    private String responseMapInfo(LittleEndianDataInputStream is) throws IOException {
        StringBuilder sb = new StringBuilder();
        WorldMap wm = CTMON.i().fromStream(is, WorldMap.class);
        sb.append(String.format("[%s - %s]\n%s\n", "responseMapInfo", Utils.i().now(), Utils.i().gson.toJson(wm)));
        return sb.toString();
    }
    
    public void requestMapInfoEx() throws IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        try (LittleEndianDataOutputStream os = new LittleEndianDataOutputStream(baos)) {
            os.writeShort(GI_MICROSERVICE_CMD.REQ_MAP_INFO_EXTRA.getCode());
        }
        if (endpoint != null) {
            endpoint.sendMessage(baos.toByteArray());
        }
    }
    
    private String responseMapInfoEx(LittleEndianDataInputStream is) throws IOException {
        StringBuilder sb = new StringBuilder();
        WorldMap wm = CTMON.i().fromStream(is, WorldMap.class);
        sb.append(String.format("[%s - %s]\n%s\n", "responseMapInfoEx", Utils.i().now(), Utils.i().gson.toJson(wm)));
        return sb.toString();
    }
    
    public void requestRegionData(short id) throws IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        try (LittleEndianDataOutputStream os = new LittleEndianDataOutputStream(baos)) {
            os.writeShort(GI_MICROSERVICE_CMD.REQ_REGION_DATA.getCode());
            os.writeShort(id);
        }
        if (endpoint != null) {
            endpoint.sendMessage(baos.toByteArray());
        }
    }
    
    private String responseRegionData(LittleEndianDataInputStream is) throws IOException {
        StringBuilder sb = new StringBuilder();
        Region r = CTMON.i().fromStream(is, Region.class);
        sb.append(String.format("[%s - %s]\n%s\n", "responseRegionData", Utils.i().now(), Utils.i().gson.toJson(r)));
        return sb.toString();
    }
    
    private void processData(ByteBuffer data) throws IOException {
        byte[] a = new byte[data.remaining()];
        data.get(a);
        try (LittleEndianDataInputStream is = new LittleEndianDataInputStream(new ByteArrayInputStream(a))) {

            GI_MICROSERVICE_CMD cmd = GI_MICROSERVICE_CMD.fromID(is.readShort());
            switch (cmd) {
                case RES_CONNECT: {
                    String rs = responseConnect(is);
                    System.out.println(rs);
                    requestGameObjectInfo();
//                    requestMapInfo();
//                    requestMapInfoEx();
//                    requestRegionData((short)0);
//                    requestRegionData((short)1);
//                    requestRegionData((short)2);
                    break;
                }
                case RES_GAMEOBJECT_INFO: {
                    String rs = responseGameObjectInfo(is);
                    System.out.println(rs);
                    break;
                }
                case RES_MAP_INFO: {
                    byte[] temp = new byte[is.readInt()];
                    is.read(temp);
                    System.out.println(temp.length);
                    break;
                }
                case RES_MAP_INFO_EXTRA: {
                    String rs = responseMapInfoEx(is);
                    System.out.println(rs);
                    break;
                }
                case RES_REGION_DATA: {
//                    String rs = responseRegionData(is);
//                    System.out.println(rs);
                    byte[] temp = new byte[is.readInt()];
                    is.read(temp);
                    System.out.println(temp.length);
                    break;
                }
            }
            if(cmd!=GI_MICROSERVICE_CMD.RES_PING){
                String log = String.format("%s - client application recv: %d", Form.sdf.format(new Date()), data.array().length);
                System.out.println(log);
            }
        } catch (Exception ex) {
        }
    }

    @Override
    public void run() {
        mRunning = true;
        int count = 0;
        while (mRunning) {
            try {
                Thread.sleep(1000);
                count++;
                if(count==3){
                    count = 0;
                    try {
                        ping();
                    } catch (IOException ex) {

                    }
                }
                
            } catch (InterruptedException ex) {
            }
        }
        endpoint.disconnect();
    }
}