/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ctmlab.enums;

import java.util.HashMap;

/**
 *
 * @author hhtri
 */
public enum ENUM_RESOURCE_TYPE {
    NULL(0),
    CASH(1),
    CREDIT(2),
    OIL(3),
    FUEL(4),
    COPPER(5),
    IRON(6),
    GOLD(7),
    SILVER(8),
    TOURAMINE(9),
    TUNGSTEN(10),
    TITANIUM(11),
    TOPAZ(12),
    SAPPHIRE(13),
    NEODYMIUM(14),
    RUBY(15),
    IRIDIUM(16),
    EMERALD(17),
    PLATINUM(18),
    PALLADIUM(19),
    CLEAR_DIAMOND(20),
    RHODIUM(21),
    PINK_DIAMOND(22),
    URANIUM(23),
    BLUE_DIAMOND(24),
    PLUTONIUM(25),
    ;
    private int code;
    private static final HashMap<Integer, ENUM_RESOURCE_TYPE> CODE_FIELD_TYPE = new HashMap();

    ENUM_RESOURCE_TYPE() {
        this.code = -1;
    }

    ENUM_RESOURCE_TYPE(int code) {
        this.code = code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public byte getCode() {
        return (byte) code;
    }

    public static ENUM_RESOURCE_TYPE get(int code) {
        ENUM_RESOURCE_TYPE rs = CODE_FIELD_TYPE.get(code);
        return rs == null ? NULL : rs;
    }

    public static ENUM_RESOURCE_TYPE get(String code) {
        ENUM_RESOURCE_TYPE rs = NULL;
        try{rs = ENUM_RESOURCE_TYPE.valueOf(code);} catch (Exception ex){}
        return rs == null ? NULL : rs;
    }

    public static String getString(int code) {
        ENUM_RESOURCE_TYPE rs = CODE_FIELD_TYPE.get(code);
        return rs == null ? NULL.toString().toLowerCase() : rs.toString().toLowerCase();
    }

    public static byte[] getKeys(){
        ENUM_RESOURCE_TYPE[] temp = ENUM_RESOURCE_TYPE.values();
        byte[] rs = new byte[temp.length-1];
        for(int i=1;i<temp.length; i++){
            rs[i-1] = temp[i].getCode();
        }
        return rs;
    }
    
    public static String[] getStringKeys(){
        ENUM_RESOURCE_TYPE[] temp = ENUM_RESOURCE_TYPE.values();
        String[] rs = new String[temp.length-1];
        for(int i=1;i<temp.length; i++){
            rs[i-1] = temp[i].toString();
        }
        return rs;
    }

    static {
        int i = 0;
        for (ENUM_RESOURCE_TYPE type : ENUM_RESOURCE_TYPE.values()) {
            if(type.getCode()!=-1){
                i = type.getCode();
            } else{
                type.setCode(++i);
            }
            CODE_FIELD_TYPE.put(type.code, type);
        }
    }
}