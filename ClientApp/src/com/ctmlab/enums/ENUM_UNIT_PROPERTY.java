/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ctmlab.enums;

import java.util.HashMap;

/**
 *
 * @author hhtri
 */
public enum ENUM_UNIT_PROPERTY {
    
    NULL(0),
    MAX_CLAIM(1),
    MAX_OIL_CLAIM(2),
    NETWORTH_VALUE(3),
    SPEED_LAND(4),
    SPEED_AIR(5),
    MAX_POWER(6),
    OIL_CAP(7),
    REFINE_SPEED(8),
    MAX_SPECIAL_SOLDIER(9),
    MAX_MERCENARY_SOLDIER(10),
    DPS(11),
    HEALTH(12),
    MAX_VEHICLE(14),
    AMMO_UNLOCK(14),
    VEHICLE_UNLOCK(15),
    RESOURCE_UNLOCK(16),
    OIL_PRODUCTION(17),
    GUN_UNLOCK(18),
    LAND_MAX_WEIGHT(19),
    AIR_MAX_WEIGHT(20),
    EXPLORE_SPEED(21),
    ;

    private int code;
    private static final HashMap<Integer, ENUM_UNIT_PROPERTY> CODE_FIELD_TYPE = new HashMap();
    private static final HashMap<String, ENUM_UNIT_PROPERTY> S_CODE_FIELD_TYPE = new HashMap();
        
    ENUM_UNIT_PROPERTY() {this.code = -1;}

    ENUM_UNIT_PROPERTY(int code) {this.code = code;}
    
    public byte getCode() {return (byte) code;}
    
    public void setCode(int c) {code = c;}

    public static byte getCode(int i) {
        ENUM_UNIT_PROPERTY rs = CODE_FIELD_TYPE.get(i);
        return (byte) (rs == null ? NULL.getCode() : rs.getCode());
    }

    public static byte getCode(String key) {
        try{
            ENUM_UNIT_PROPERTY rs = ENUM_UNIT_PROPERTY.valueOf(key);
            return rs.getCode();
        } catch(Exception ex){
            return NULL.getCode();
        }
        
    } 
    
    public static boolean isTime(int k){
        try{
            ENUM_UNIT_PROPERTY e = CODE_FIELD_TYPE.get(k);
            return (e==SPEED_AIR || e==SPEED_LAND || e==REFINE_SPEED || e==EXPLORE_SPEED);
        } catch(Exception ex){
            return false;
        }
        
    }
    
    static {
        int i = -2;
        for (ENUM_UNIT_PROPERTY type : ENUM_UNIT_PROPERTY.values()) {
            if(type.getCode()!=-1){
                i = type.getCode();
            } else{                
                type.setCode(++i);
            }            
            CODE_FIELD_TYPE.put(type.code, type);
            S_CODE_FIELD_TYPE.put(type.toString().toLowerCase(), type);
        }
    }
}
