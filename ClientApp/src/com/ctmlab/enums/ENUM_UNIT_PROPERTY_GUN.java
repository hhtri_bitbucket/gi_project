/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ctmlab.enums;

import java.util.HashMap;

/**
 *
 * @author hhtri
 */
public enum ENUM_UNIT_PROPERTY_GUN {
    
    NULL(0),
    MAX_CLAIM(1),
    MAX_OIL_CLAIM,
    NETWORTH_VALUE,
    SPEED_LAND,
    SPEED_AIR,
    MAX_POWER,
    OIL_CAP,
    REFINE_SPEED,
    MAX_SPECIAL_SOLDIER,
    MAX_MERCENARY_SOLDIER,
    DPS,
    HEALTH,
    MAX_VEHICLE,
    AMMO_UNLOCK,
    VEHICLE_UNLOCK,
    RESOURCE_UNLOCK,
    GUN_UNLOCK,
    LAND_MAX_WEIGHT,
    AIR_MAX_WEIGHT,
    ;

    private int code;
    private static final HashMap<Integer, ENUM_UNIT_PROPERTY_GUN> CODE_FIELD_TYPE = new HashMap();
        
    ENUM_UNIT_PROPERTY_GUN() {this.code = -1;}

    ENUM_UNIT_PROPERTY_GUN(int code) {this.code = code;}
    
    public byte getCode() {return (byte) code;}
    
    public void setCode(int c) {code = c;}

    public static byte getCode(int i) {
        ENUM_UNIT_PROPERTY_GUN rs = CODE_FIELD_TYPE.get(i);
        return (byte) (rs == null ? NULL.getCode() : rs.getCode());
    }
   
    

    public static byte getCode(String key) {
        try{
            ENUM_UNIT_PROPERTY_GUN rs = ENUM_UNIT_PROPERTY_GUN.valueOf(key);
            return rs.getCode();
        } catch(Exception ex){
            return NULL.getCode();
        }
        
    }    
    
    static {
        int i = -2;
        for (ENUM_UNIT_PROPERTY_GUN type : ENUM_UNIT_PROPERTY_GUN.values()) {
            if(type.getCode()!=-1){
                i = type.getCode();
            } else{                
                type.setCode(++i);
            }            
            CODE_FIELD_TYPE.put(type.code, type);
        }
    }
}
