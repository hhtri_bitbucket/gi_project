/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ctmlab.enums;

import java.util.HashMap;

/**
 *
 * @author hhtri
 */
public enum ENUM_MESSAGE {
    
    GAME_MSG(1),
    REPORT_TEST_MINE_MSG(2),
    INVITATION_MSG(3),
    JOIN_ALLIANCE_MSG(4),
    ;
    
    private final int code;
    private static final HashMap<Integer, ENUM_MESSAGE> CODE_FIELD_TYPE = new HashMap();

    ENUM_MESSAGE(int code) {
        this.code = code;
    }

    public byte getCode(){return (byte) code;}
    
    public static ENUM_MESSAGE get(int code) {
        return CODE_FIELD_TYPE.get(code);
    }

    static {
        for (ENUM_MESSAGE type : ENUM_MESSAGE.values()) {
            CODE_FIELD_TYPE.put(type.code, type);
        }
    }
}