/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ctmlab.enums;

import java.util.HashMap;

/**
 *
 * @author hhtri
 */
public enum ENUM_OBJECT_MAP_TYPE {
    
    NULL(0),
    HOME_BASE(1),
    MINE(2),
    OIL_MINE(3),
    TRADE_CENTER(4),
    ;
    
    private final int code;
    private static final HashMap<Integer, ENUM_OBJECT_MAP_TYPE> CODE_FIELD_TYPE = new HashMap();

    ENUM_OBJECT_MAP_TYPE(int code) {
        this.code = code;
    }

    public int getCode(){return code;}
    
    public static ENUM_OBJECT_MAP_TYPE get(int code) {
        return CODE_FIELD_TYPE.get(code);
    }

    static {
        for (ENUM_OBJECT_MAP_TYPE type : ENUM_OBJECT_MAP_TYPE.values()) {
            CODE_FIELD_TYPE.put(type.code, type);
        }
    }
}
