/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ctmlab.enums;

import java.util.HashMap;

/**
 *
 * @author hhtri
 */
public enum ENUM_UNIT_PROPERTY_VEHICLE {

    NULL(0),
    TANK(1),
    ATTACK_HELICOPTER,
    ADVANCE_TANK,
    ADVANCE_HELICOPTER,
    MISSILE_LAUNCHER,;

    private int code;
    private static final HashMap<Integer, ENUM_UNIT_PROPERTY_VEHICLE> CODE_FIELD_TYPE = new HashMap();

    ENUM_UNIT_PROPERTY_VEHICLE() {
        this.code = -1;
    }

    ENUM_UNIT_PROPERTY_VEHICLE(int code) {
        this.code = code;
    }

    public byte getCode() {
        return (byte) code;
    }

    public void setCode(int c) {
        code = c;
    }

    public static byte getCode(int i) {
        ENUM_UNIT_PROPERTY_VEHICLE rs = CODE_FIELD_TYPE.get(i);
        return (byte) (rs == null ? NULL.getCode() : rs.getCode());
    }

    public static byte getCode(String key) {
        try {
            ENUM_UNIT_PROPERTY_VEHICLE rs = ENUM_UNIT_PROPERTY_VEHICLE.valueOf(key);
            return rs.getCode();
        } catch (Exception ex) {
            return NULL.getCode();
        }

    }

    static {
        int i = -2;
        for (ENUM_UNIT_PROPERTY_VEHICLE type : ENUM_UNIT_PROPERTY_VEHICLE.values()) {
            if (type.getCode() != -1) {
                i = type.getCode();
            } else {
                type.setCode(++i);
            }
            CODE_FIELD_TYPE.put(type.code, type);
        }
    }
}
