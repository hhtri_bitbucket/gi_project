/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ctmlab.enums;

import java.util.HashMap;

/**
 *
 * @author hhtri
 */
public enum ENUM_UNIT_TYPE {
    
    NULL(0),
    HOME_BASE(1),
    MINE(2),
    OIL_RIG_BASIC(3),
    OIL_RIG_ADVANCE(4),
    OIL_RIG_ULTRA(5),
    TRADE_CENTER(6),
    EXPLORATION(7),
    TRANSPORTATION(8),
    
    HOME_OFFICE(10),
    HOME_REFINERY(11),
    HOME_BARRACK(12),
    HOME_DEFENSE(13),
    HOME_WEAPON(14),
    HOME_EXPLORATION(15),
    HOME_TRANSPORTATION(16),
    HOME_WALL(17),
    
    CLAIM_EXCAVATOR(20),
    CLAIM_DUMPTRUCK(21),
    CLAIM_WASH_PLANT(22),
    CLAIM_OIL_RIG(23),
    CLAIM_WORKER(24),
    ;
    
    private final int code;
    private static final HashMap<Integer, ENUM_UNIT_TYPE> CODE_FIELD_TYPE = new HashMap();

    ENUM_UNIT_TYPE(int code) {
        this.code = code;
    }

    public int getCode(){return code;}
    
    public static ENUM_UNIT_TYPE get(int code) {
        return CODE_FIELD_TYPE.get(code);
    }

    static {
        for (ENUM_UNIT_TYPE type : ENUM_UNIT_TYPE.values()) {
            CODE_FIELD_TYPE.put(type.code, type);
        }
    }
}
