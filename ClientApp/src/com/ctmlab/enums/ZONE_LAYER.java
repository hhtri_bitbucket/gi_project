/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ctmlab.enums;

import java.util.HashMap;

/**
 *
 * @author hhtri
 */
public enum ZONE_LAYER
{
    GROUND(1),
    WATER(2),
    SHORE(3),
    VISUAL(4),
    TREE(5),
    ;
    
    private final int code;
    private static final HashMap<Integer, ZONE_LAYER> CODE_FIELD_TYPE = new HashMap();

    ZONE_LAYER(int code) {
        this.code = code;
    }

    public byte getCode(){return (byte)code;}
    
    public static ZONE_LAYER get(int code) {
        return CODE_FIELD_TYPE.get(code);
    }

    static {
        for (ZONE_LAYER type : ZONE_LAYER.values()) {
            CODE_FIELD_TYPE.put(type.code, type);
        }
    }
}
