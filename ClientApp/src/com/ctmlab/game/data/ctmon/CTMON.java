/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ctmlab.game.data.ctmon;

//import com.ctmlab.game.goldincenum.CTMON_FieldType;
//import com.ctmlab.game.goldincenum.ZONE_LAYER;
//import com.ctmlab.util.Utils;
//import com.dp.db.DBDataPostgres;
//import com.dp.db.DataFieldType;
//import com.dp.db.Func;
import com.ctmlab.enums.CTMON_FieldType;
import com.ctmlab.enums.ZONE_LAYER;
import com.ctmlab.manager.Utils;
import com.google.common.io.LittleEndianDataInputStream;
import com.google.common.io.LittleEndianDataOutputStream;
import com.google.gson.JsonObject;
import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

/**
 *
 * @author hhtri
 */
public class CTMON {

    public CTMON() {
    }

    public static final Object LOCK = new Object();
    private static CTMON inst;

    public static CTMON i() {
        synchronized (LOCK) {
            if (inst == null) {
                inst = new CTMON();
            }
            return inst;
        }
    }

    private Object convert(Class<?> clazz, ArrayList ls) {
        Object result = null;
        if (clazz == ArrayList.class) {
            result = new ArrayList();
            ((ArrayList) result).addAll(ls);
        } else if (clazz == HashSet.class) {
            result = new HashSet();
            ((HashSet) result).addAll(ls);
        } else if (clazz == TreeSet.class) {
            result = new TreeSet();
            ((TreeSet) result).addAll(ls);
        }
        return result;
    }

    private Object convert(Class<?> clazz, HashMap h) {
        if (clazz == HashMap.class) {
            return h;
        } else if (clazz == TreeMap.class) {
            TreeMap t = new TreeMap();
            Set<Map.Entry<Object, Object>> set = (Set<Map.Entry<Object, Object>>) h.entrySet();
            for (Map.Entry<Object, Object> e : set) {
                t.put(e.getKey(), e.getValue());
            }
            return t;
        }
        return null;
    }

    private Object[] convertToList(Object o) {
        if (o == null) {
            return null;
        }
        Class clazz = o.getClass();
        if (clazz == ArrayList.class) {
            return ((ArrayList) o).toArray(new Object[0]);
        } else if (clazz == HashSet.class) {
            return ((HashSet) o).toArray(new Object[0]);
        } else if (clazz == TreeSet.class) {
            return ((TreeSet) o).toArray(new Object[0]);
        }
        return null;
    }

    private Set<Map.Entry<Object, Object>> convertToEntry(Object o) {
        if (o == null) {
            return null;
        }
        Class clazz = o.getClass();
        if (clazz == HashMap.class) {
            return (Set<Map.Entry<Object, Object>>) ((HashMap) o).entrySet();
        } else if (clazz == TreeMap.class) {
            return (Set<Map.Entry<Object, Object>>) ((TreeMap) o).entrySet();
        }
        return null;
    }
    
    private Field[] getListFields(Object c){return c.getClass().getDeclaredFields();}

    public void toStream(LittleEndianDataOutputStream os, Object o) {
        try {
            for(Field f : getListFields(o)) {
//                Utils.writeBigString(os, f.getName());
//                System.out.println(f.getName());
                CTMONDataFieldType fType = f.getAnnotation(CTMONDataFieldType.class);
                if(fType!=null && (fType.ignore())){
                    continue;
                }
                
                Utils.i().writeString(os, f.getName());
                Object value;
                try {
                    value = f.get(o);
                } catch (IllegalArgumentException | IllegalAccessException ex) {
                    value = null;
                }
//                System.out.println(Func.json(value));
                if (value == null) {
                    os.write(CTMON_FieldType.NULL.code);
                } else {
                    CTMON_FieldType type = CTMON_FieldType.get(f.getType());
                    os.write(type.code);
                    switch (type) {
                        case OBJECT:
                            toStream(os, value);
                            break;
                        case STRING:
                            Utils.i().writeBigString(os, (String) value);
                            break;
                        case BYTE:
                            os.writeByte((byte) value);
                            break;
                        case SHORT:
                            os.writeShort((short) value);
                            break;
                        case INTEGER:
                            os.writeInt((int) value);
                            break;
                        case LONG:
                            os.writeLong((long) value);
                            break;
                        case FLOAT:
                            os.writeFloat((float) value);
                            break;
                        case DOUBLE:
                            os.writeDouble((double) value);
                            break;
                        case BOOLEAN:
                            os.writeBoolean((boolean) value);
                            break;
                        case DATE:
                            os.writeLong(((Date) value).getTime());
                            break;
                        case BYTE_ARRAY:
                            byte[] b = (byte[]) value;
                            os.writeInt(b.length);
                            os.write(b);
                            break;
                        case BYTE_BUFFER:
                            byte[] bBuff = ((ByteBuffer) value).array();
                            os.writeInt(bBuff.length);
                            os.write(bBuff);
                            break;
                        case ARRAYLIST: {
                            Object[] l = convertToList(value);
                            ParameterizedType lsType = (ParameterizedType) f.getGenericType();
                            Class<?> tClazz = null;
                            try{
                                tClazz = (Class<?>) lsType.getActualTypeArguments()[0];
                            } catch(Exception ex){
                                ParameterizedType paramTypeImpl = (ParameterizedType) lsType.getActualTypeArguments()[0];
                                tClazz = (Class<?>) paramTypeImpl.getRawType();
                            }
                            CTMON_FieldType tType = CTMON_FieldType.get(tClazz);
                            os.write(tType.code);
                            os.writeInt(l.length);
                            for (Object obj : l) {
                                objectToStreamBase(os, tType, obj);
                            };
                            break;
                        }
                        case HASHMAP: {
                            Set<Map.Entry<Object, Object>> l = convertToEntry(value);
                            ParameterizedType lsType = (ParameterizedType) f.getGenericType();
                            CTMON_FieldType ftKey = CTMON_FieldType.get((Class<?>) lsType.getActualTypeArguments()[0]);
                            
                            Class<?> tClazz = null;
                            try{
                                tClazz = (Class<?>) lsType.getActualTypeArguments()[1];
                            } catch(Exception ex){
                                ParameterizedType paramTypeImpl = (ParameterizedType) lsType.getActualTypeArguments()[1];
                                tClazz = (Class<?>) paramTypeImpl.getRawType();
                            }
                            CTMON_FieldType ftValue = CTMON_FieldType.get(tClazz);
                            os.write(ftKey.code);
                            os.write(ftValue.code);
                            os.writeInt(l.size());
                            for (Map.Entry<Object, Object> e : l) {
                                objectToStreamBase(os, ftKey, e.getKey());
                                objectToStreamBase(os, ftValue, e.getValue());
                            }
                            break;
                        }
                        case ENUM: {
                            objectToStreamBase(os, type, value);
                            break;
                        }
                    }
                }
            }
            Utils.i().writeString(os, "");
        } catch (IOException | SecurityException ex) {
            ex.printStackTrace();
        }
    }

    private void listToStreamBase(LittleEndianDataOutputStream os, CTMON_FieldType type, Object value) {
        try {
            switch (type) {
                case ARRAYLIST: {
                    Object[] l = convertToList(value);
                    if (l.length > 0) {
                        Class<?> tClazz = l[0].getClass();
                        CTMON_FieldType tType = CTMON_FieldType.get(tClazz);
                        os.write(tType.code);
                        os.writeInt(l.length);
                        for (Object obj : l) {
                            objectToStreamBase(os, tType, obj);
                        };
                    } else {
                        os.write(CTMON_FieldType.NULL.code);
                        os.writeInt(0);
                    }
                    break;
                }
                case HASHMAP: {
                    Set<Map.Entry<Object, Object>> l = convertToEntry(value);
                    if (!l.isEmpty()) {
                        Map.Entry<Object, Object> typeTemp = null;
                        for (Map.Entry<Object, Object> e : l) {
                            if (typeTemp == null) {
                                typeTemp = e;
                                break;
                            }
                        }
                        CTMON_FieldType ftKey = CTMON_FieldType.get((Class<?>) typeTemp.getKey());
                        CTMON_FieldType ftValue = CTMON_FieldType.get((Class<?>) typeTemp.getValue());
                        os.write(ftKey.code);
                        os.write(ftValue.code);
                        os.writeInt(l.size());
                        for (Map.Entry<Object, Object> e : l) {
                            objectToStreamBase(os, ftKey, e.getKey());
                            objectToStreamBase(os, ftValue, e.getValue());
                        }
                    } else {
                        os.write(CTMON_FieldType.NULL.code);
                        os.write(CTMON_FieldType.NULL.code);
                        os.writeInt(0);
                    }
                    break;
                }
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    private void objectToStreamBase(LittleEndianDataOutputStream os, CTMON_FieldType type, Object o) {
        try {
            switch (type) {
                case OBJECT: {
                    toStream(os, o);
                    break;
                }
                case STRING: {
                    Utils.i().writeBigString(os, String.valueOf(o));
                    break;
                }
                case BYTE: {
                    os.writeByte((byte) o);
                    break;
                }
                case SHORT: {
                    os.writeShort((short) o);
                    break;
                }
                case INTEGER: {
                    os.writeInt((int) o);
                    break;
                }
                case LONG: {
                    os.writeLong((long) o);
                    break;
                }
                case FLOAT: {
                    os.writeFloat((float) o);
                    break;
                }
                case DOUBLE: {
                    os.writeDouble((double) o);
                    break;
                }
                case BOOLEAN: {
                    os.writeBoolean((boolean) o);
                    break;
                }
                case DATE: {
                    os.writeLong(((Date) o).getTime());
                    break;
                }
                case BYTE_ARRAY: {
                    byte[] b = (byte[]) o;
                    os.writeInt(b.length);
                    os.write(b);
                    break;
                }
                case BYTE_BUFFER: {
                    byte[] bBuff = ((ByteBuffer) o).array();
                    os.writeInt(bBuff.length);
                    os.write(bBuff);
                    break;
                }
                case ARRAYLIST: {
                    listToStreamBase(os, type, o);
                    break;
                }
                case HASHMAP: {
                    listToStreamBase(os, type, o);
                    break;
                }
                case ENUM: {
                    String name = o.getClass().getSimpleName();
                    switch (name) {
                        case "ZONE_LAYER": {
                            os.writeInt(((ZONE_LAYER) o).getCode());
                            break;
                        }
                    }
                    break;
                }
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    public <T> T fromStream(LittleEndianDataInputStream is, Class<T> clazz) {
        T t = null;
        try {
            if (clazz != null) {
                t = clazz.newInstance();
            }
            while (true) {
                Field f = null;
                String field = Utils.i().readString(is);
                if (field.isEmpty()) {
                    break;
                }
                if (clazz != null) {
                    try {
                        f = clazz.getField(field);
                        f.setAccessible(true);
                        CTMONDataFieldType fType = f.getAnnotation(CTMONDataFieldType.class);
                        if(fType!=null && (fType.ignore())){
                            continue;
                        }
                    } catch (NoSuchFieldException | SecurityException e) {
                    }
                }
                Object value = streamToObject(is, f);
                if (f != null) {
                    f.set(t, value);
                }
            }
        } catch (IOException | IllegalAccessException
                | IllegalArgumentException | InstantiationException ex) {
        }
        return t;
    }

    private Object streamToObject(LittleEndianDataInputStream is, Field f) {
        try {
            CTMON_FieldType type = CTMON_FieldType.get(is.read());
            switch (type) {
                case NULL:
                    return null;
                case OBJECT:
                    return fromStream(is, (f != null) ? f.getType() : null);
                case STRING:
                    return Utils.i().readBigString(is);
                case BYTE:
                    return is.readByte();
                case SHORT:
                    return is.readShort();
                case INTEGER:
                    return is.readInt();
                case LONG:
                    return is.readLong();
                case FLOAT:
                    return is.readFloat();
                case DOUBLE:
                    return is.readDouble();
                case BOOLEAN:
                    return is.readBoolean();
                case DATE:
                    return new Date(is.readLong());
                case BYTE_ARRAY:
                    int n = is.readInt();
                    if (n < 0) {
                        return false;
                    }
                    byte[] b = new byte[n];
                    is.read(b);
                    return b;
                case BYTE_BUFFER:
                    int lenBuff = is.readInt();
                    if (lenBuff < 0) {
                        return false;
                    }
                    byte[] bBuff = new byte[lenBuff];
                    is.read(bBuff);
                    return ByteBuffer.wrap(bBuff);
                case ARRAYLIST: {
                    CTMON_FieldType tItem = CTMON_FieldType.get(is.read());
                    int size = is.readInt();
                    Class<?> cType = null;
                    if (f != null) {
                        
//                        ParameterizedType lsType = (ParameterizedType) f.getGenericType();
//                        cTypeK = (Class<?>) lsType.getActualTypeArguments()[0];
                        
                        ParameterizedType lsType = (ParameterizedType) f.getGenericType();
//                        cType = (Class<?>) lsType.getActualTypeArguments()[0];
                        
                        try{
                            ParameterizedType paramTypeImpl = (ParameterizedType) lsType.getActualTypeArguments()[0];                        
                            cType = (Class<?>) paramTypeImpl.getRawType();
                        }catch(Exception ex){
                            cType = (Class<?>) lsType.getActualTypeArguments()[0];
                        }
                        
                    }
                    ArrayList l = new ArrayList();
                    int i = 0;
                    while (i < size) {
                        Object val = streamToObjectBase(is, tItem, cType);
                        if (val != null) {
                            l.add(val);
                        }
                        i++;
                    }
                    return f == null ? null : convert(f.getType(), l);
                }
                case HASHMAP: {
                    CTMON_FieldType tItemK = CTMON_FieldType.get(is.read());
                    CTMON_FieldType tItemV = CTMON_FieldType.get(is.read());
                    int size = is.readInt();
                    Class<?> cTypeK = null;
                    Class<?> cTypeV = null;
                    if (f != null) {
                        ParameterizedType lsType = (ParameterizedType) f.getGenericType();
                        cTypeK = (Class<?>) lsType.getActualTypeArguments()[0];
                        try{
                            ParameterizedType paramTypeImpl = (ParameterizedType) lsType.getActualTypeArguments()[1];                        
                            cTypeV = (Class<?>) paramTypeImpl.getRawType();
                        }catch(Exception ex){
                            cTypeV = (Class<?>) lsType.getActualTypeArguments()[1];
                        }
                    }
                    HashMap h = new HashMap();
                    int i = 0;
                    while (i < size) {
                        Object k = streamToObjectBase(is, tItemK, cTypeK);
                        Object v = streamToObjectBase(is, tItemV, cTypeV);
                        if (k != null) {
                            h.put(k, v);
                        }
                        i++;
                    }
                    return f == null ? null : convert(f.getType(), h);
                }
                case ENUM: {
                    return streamToObjectBase(is, type, f.getType());
                }
            }
            return null;
        } catch (IOException | IllegalArgumentException | SecurityException ex) {
            ex.printStackTrace();
        }
        return null;
    }

    private Object streamToListBase(LittleEndianDataInputStream is, CTMON_FieldType type, Class<?> clazz) {
        try {
            switch (type) {
                case NULL:
                    return null;
                case ARRAYLIST:{
                    CTMON_FieldType tItem = CTMON_FieldType.get(is.read());
                    int size = is.readInt();
                    Class<?> cType = tItem.getFieldTypeClass();
                    ArrayList l = new ArrayList();
                    int i = 0;
                    while (i < size) {
                        Object val = streamToObjectBase(is, tItem, cType);
                        if (val != null) {
                            l.add(val);
                        }
                        i++;
                    }
                    return convert(clazz, l);
                }
                case HASHMAP:
                    CTMON_FieldType tItemK = CTMON_FieldType.get(is.read());
                    CTMON_FieldType tItemV = CTMON_FieldType.get(is.read());
                    int size = is.readInt();
                    Class<?> cTypeK = tItemK.getFieldTypeClass();
                    Class<?> cTypeV = tItemV.getFieldTypeClass();
                    HashMap h = new HashMap();
                    int i = 0;
                    while (i < size) {
                        Object k = streamToObjectBase(is, tItemK, cTypeK);
                        Object v = streamToObjectBase(is, tItemV, cTypeV);
                        if (k != null) {
                            h.put(k, v);
                        }
                        i++;
                    }
                    return convert(clazz, h);              
            }
            return null;
        } catch (IOException | IllegalArgumentException | SecurityException ex) {
            ex.printStackTrace();
        }
        return null;
    }
    
    private Object streamToObjectBase(LittleEndianDataInputStream is, CTMON_FieldType type, Class<?> clazz) {
        try {
            switch (type) {
                case NULL:
                    return null;
                case OBJECT:
                    return fromStream(is, clazz);
                case STRING:
                    return Utils.i().readBigString(is);
                case BYTE:
                    return is.readByte();
                case SHORT:
                    return is.readShort();
                case INTEGER:
                    return is.readInt();
                case LONG:
                    return is.readLong();
                case FLOAT:
                    return is.readFloat();
                case DOUBLE:
                    return is.readDouble();
                case BOOLEAN:
                    return is.readBoolean();
                case DATE:
                    return new Date(is.readLong());
                case BYTE_ARRAY:{
                    int n = is.readInt();
                    if (n < 0) {
                        return false;
                    }
                    byte[] b = new byte[n];
                    is.read(b);
                    return b;
                }
                case BYTE_BUFFER:{
                    int lenBuff = is.readInt();
                    if (lenBuff < 0) {
                        return false;
                    }
                    byte[] bBuff = new byte[lenBuff];
                    is.read(bBuff);
                    return ByteBuffer.wrap(bBuff);
                }
                case ARRAYLIST:
                    return streamToListBase(is, type, clazz);
                case HASHMAP:
                    return streamToListBase(is, type, clazz);
                case ENUM: {
                    String name = clazz.getSimpleName();
                    switch (name) {
                        case "ENV_TYPE": {
                            return ZONE_LAYER.get(is.readInt());
                        }
                    }
                }
            }
            return null;
        } catch (IOException | IllegalArgumentException | SecurityException ex) {
            ex.printStackTrace();
        }
        return null;
    }

    public <T> T copy(Object o) {
        if (o == null) {
            return null;
        }
        T t = null;
        try {
            t = (T) o.getClass().newInstance();
            for (Field f : o.getClass().getFields()) {
                Object value;
                try {
                    value = f.get(o);
                } catch (IllegalArgumentException | IllegalAccessException ex) {
                    value = null;
                }
                f.set(t, value);
            }
        } catch (Exception ex) {
        }
        return t;
    }

//    private static <T> T newInstanceMWResponse(Class<T> clazz, Object... objs) {
//        if (clazz == null) {
//            return null;
//        }
//        T t = null;
//        try {
//            t = clazz.newInstance();
//            int i = 0;
//            for (Field f : clazz.getDeclaredFields()) {
//                Object value = objs[i++];
//                f.setAccessible(true);
//                f.set(t, value);
//            }
//        } catch (Exception ex) {
//        }
//        if (!Func.isLinuxServer()) {
//            System.out.println("----------response to client - " + Func.now() + "-----------");
//            System.out.println(Func.pson(t));
//            System.out.println("==========================================");
//        }
//        return t;
//    }
    public static <T> T fromJson(Class<T> clazz, JsonObject obj) {
        if (clazz == null) {
            return null;
        }
        T t = null;
        try {
            t = clazz.newInstance();
            Class cls = Class.forName(t.getClass().getName());
            Object[] arr = new Object[]{obj};
            String method = "fromJson";
            for (Method m : cls.getDeclaredMethods()) {
                if (m.getName().equals(method) && m.getParameterCount() == 1) {
                    m.invoke(t, arr);
                    break;
                }
            }
        } catch (ClassNotFoundException | IllegalAccessException
                | IllegalArgumentException | InstantiationException
                | SecurityException | InvocationTargetException ex) {
        }
        return t;
    }
}
