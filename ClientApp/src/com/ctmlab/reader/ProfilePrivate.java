/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ctmlab.reader;

import com.ctmlab.manager.Utils;
import com.ctmlab.model.Exploration;
import com.ctmlab.model.GameObjectMap;
import com.ctmlab.model.HomeBase;
import com.ctmlab.model.Mine;
import com.ctmlab.model.OilMine;
import com.ctmlab.model.Truck;
import com.ctmlab.model.Worker;
import com.ctmlab.model.building.Building;
import com.ctmlab.model.equipment.Equipment;
import com.ctmlab.staticdata.GameResources;
import com.google.common.io.LittleEndianDataInputStream;
import com.google.gson.Gson;
import java.io.IOException;
import java.util.ArrayList;

/**
 *
 * @author hhtri
 */
public class ProfilePrivate {
    public String secret = "";
    public GameResources resource = new GameResources();
    public long lastEditTime = 0;
    public GameObjectMap homeBase = null;
    public ArrayList<GameObjectMap> lstMine = new ArrayList();
    public ArrayList<GameObjectMap> lstOilMine = new ArrayList();
    public ArrayList<GameObjectMap> lstTestedMine = new ArrayList();
    public ArrayList<GameObjectMap> lstExploration = new ArrayList();
    public ArrayList<GameObjectMap> lstTransportation = new ArrayList();
    public ArrayList<Building> lstBuilding = new ArrayList();
    public ArrayList<Equipment> lstEquipment = new ArrayList();
    public ArrayList<Worker> lstWorker = new ArrayList();
    public boolean wallet = false;
    public String walletID = "";
    public boolean pattern = false;
    
    public void readData(LittleEndianDataInputStream is) throws IOException{
        secret = Utils.i().readBigString(is);
        resource.readData(is);
        lastEditTime = is.readLong();
        if(is.readBoolean()){
            homeBase = new HomeBase();
            homeBase.readData(is);
        }
        int len = is.readInt();
        int i = 0;
        lstMine.clear();
        while(i++<len){
            GameObjectMap o = new Mine();
            o.readData(is);
            lstMine.add(o);
        }
        len = is.readInt();
        i = 0;
        lstOilMine.clear();
        while(i++<len){
            GameObjectMap o = new OilMine();
            o.readData(is);
            lstOilMine.add(o);
        }
        len = is.readInt();
        i = 0;
        lstTestedMine.clear();
        while(i++<len){
            GameObjectMap o = new Mine();
            o.readData(is);
            lstTestedMine.add(o);
        }
        len = is.readInt();
        i = 0;
        lstExploration.clear();
        while(i++<len){
            GameObjectMap o = new Exploration();
            o.readData(is);
            lstExploration.add(o);
        }
        len = is.readInt();
        i = 0;
        lstTransportation.clear();
        while(i++<len){
            GameObjectMap o = new Truck();
            o.readData(is);
            lstTransportation.add(o);
        }
        len = is.readInt();
        i = 0;
        lstBuilding.clear();
        while(i++<len){
            lstBuilding.add(Building.readStatic(is));
        }
        len = is.readInt();
        i = 0;
        lstEquipment.clear();
        while(i++<len){
            lstEquipment.add(Equipment.readStatic(is));
        }
        len = is.readInt();
        i = 0;
        lstWorker.clear();
        while(i++<len){
            byte type= is.readByte();
            Worker w = new Worker();
            w.readData(is);
            lstWorker.add(w);
        }
        wallet = is.readBoolean();
        pattern = is.readBoolean(); 
        walletID = Utils.i().readBigString(is);
    }
    
    public String printLog(Gson json, Gson pson){
        StringBuilder sb = new StringBuilder();
        sb.append(String.format("secret:%s\n", secret));
        sb.append(String.format("resource:%s\n", json.toJson(resource)));
        sb.append(String.format("lastEditTime:%d\n", lastEditTime));
        sb.append(String.format("homeBase:%s\n", json.toJson(homeBase)));
        sb.append(String.format("lstMine:%s\n", json.toJson(lstMine)));
        sb.append(String.format("lstOilMine:%s\n", json.toJson(lstOilMine)));
        sb.append(String.format("lstTestedMine:%s\n", json.toJson(lstTestedMine)));
        sb.append(String.format("lstExploration:%s\n", json.toJson(lstExploration)));
        sb.append(String.format("lstTransportation:%s\n", json.toJson(lstTransportation)));
        sb.append(String.format("lstBuilding:%s\n", json.toJson(lstBuilding)));
        sb.append(String.format("lstEquipment:%s\n", json.toJson(lstEquipment)));
        sb.append(String.format("lstWorker:%s\n", json.toJson(lstWorker)));
        sb.append(String.format("wallet:%b\n", secret));
        sb.append(String.format("walletID:%s\n", walletID));
        sb.append(String.format("pattern:%b\n", pattern));
        return sb.toString();
    }
}
