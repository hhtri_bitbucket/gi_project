/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ctmlab.reader;

import com.ctmlab.manager.Utils;
import com.google.common.io.LittleEndianDataInputStream;
import com.google.gson.Gson;
import java.io.IOException;

/**
 *
 * @author hhtri
 */
public class PlayerProfile {
    public String userID = "";
    public String name = "";
    public String companyName = "";
    public int userLevel = 1;    
    public SocialAlliance alliance = null;
    public ProfilePrivate privateInfo = new ProfilePrivate();
    
    public PlayerProfile(){}
    
    public void readData(LittleEndianDataInputStream is) throws IOException{
        userID = Utils.i().readBigString(is);
        name = Utils.i().readBigString(is);
        companyName = Utils.i().readBigString(is);
        userLevel = is.readInt();
        if(is.readBoolean()){
            alliance = new SocialAlliance();
            alliance.readData(is);
        }
        if(is.readBoolean()){
            privateInfo = new ProfilePrivate();
            privateInfo.readData(is);
        }        
    }
    
    public String printLog(Gson json, Gson pson){
        StringBuilder sb = new StringBuilder();
        sb.append(String.format("userID:%s\n", userID));
        sb.append(String.format("name:%s\n", name));
        sb.append(String.format("companyName:%s\n", companyName));
        sb.append(String.format("userLevel:%d\n", userLevel));
        sb.append(String.format("alliance:%s\n", json.toJson(alliance)));
        sb.append(privateInfo.printLog(json, pson));
        return sb.toString();
    }
}
