/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ctmlab.reader;

import com.ctmlab.enums.ENUM_UNIT_TYPE;
import com.ctmlab.model.GameObjectMap;
import com.ctmlab.model.HomeBase;
import com.ctmlab.model.Mine;
import com.ctmlab.model.OilMine;
import com.ctmlab.model.TradeCenter;
import com.google.common.io.LittleEndianDataInputStream;
import com.google.common.io.LittleEndianDataOutputStream;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.io.IOException;
import java.util.ArrayList;

/**
 *
 * @author hhtri
 */
public class ZoneDetail {
    private final ArrayList<GameObjectMap> lstObj = new ArrayList();
    private final ArrayList<PlayerProfile> lstProfile = new ArrayList();
    public ZoneDetail(){}
    

    public void writeData(LittleEndianDataOutputStream os) throws IOException{

    }

    public void readData(LittleEndianDataInputStream is) throws IOException{
        int len = is.readInt();
        int i = 0;
        lstObj.clear();
        Gson pson = new GsonBuilder().serializeNulls().setPrettyPrinting().create(); 
        while(i++<len){
            ENUM_UNIT_TYPE type = ENUM_UNIT_TYPE.get((int)is.readByte());
            GameObjectMap temp = null;
            switch(type){
                case HOME_BASE:{
                    temp = new HomeBase();                    
                    break;
                }
                case MINE:{
                    temp = new Mine();
                    break;
                }
                case OIL_RIG_BASIC:{
                    temp = new OilMine();
                    break;
                }
                case OIL_RIG_ADVANCE:{
                    temp = new OilMine();
                    break;
                }
                case OIL_RIG_ULTRA:{
                    temp = new OilMine();
                    break;
                }
                case TRADE_CENTER:{
                    temp = new TradeCenter();
                    break;
                }
            }
            if(temp!=null){
                temp.readData(is);
                lstObj.add(temp);
            }
        }
        len = is.readInt();
        i=0;
        lstProfile.clear();
        while(i++<len){
            PlayerProfile p = new PlayerProfile();
            p.readData(is);
            lstProfile.add(p);
        }
    }
}
