/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ctmlab.reader;

import com.ctmlab.manager.Utils;
import com.google.common.io.LittleEndianDataInputStream;
import java.io.IOException;
import java.util.ArrayList;

/**
 *
 * @author hhtri
 */
public class SocialAlliance {
    public long id;
    public String name = "";
    public String leader = "";
    public ArrayList<String> coLeaders;
    public int numberOfMember;
    public int rank;
    public long created;

    private ArrayList<String> mMembers = null;

    public SocialAlliance() {
    }
    
    public void readData(LittleEndianDataInputStream is) throws IOException{
        
        id = is.readLong();
        name = Utils.i().readBigString(is);
        rank = is.readInt();
        leader = Utils.i().readBigString(is);
        int len = is.readInt();
        int i = 0;
        coLeaders = new ArrayList<>();
        while(i<len){
            coLeaders.add(Utils.i().readBigString(is));
            i++;
        }
        numberOfMember = is.readInt();
        if(is.readBoolean()){
            mMembers = new ArrayList();
            len = is.readInt();
            i=0;
            while(i<len){
                mMembers.add(Utils.i().readBigString(is));
                i++;
            }
        }
        
    }
    
    public String printData(){
        StringBuilder sb = new StringBuilder();
        sb.append("ID:" + id + "\n");
        sb.append("Name:" + name + "\n");
        sb.append("Rank:" + rank + "\n");
        sb.append("Leader:" + leader + "\n");
        for(String s:coLeaders){
            sb.append("Co-Leader:" + s + "\n");
        }
        sb.append("NumberOfMember:" + numberOfMember + "\n");
        for(String s:mMembers){
            sb.append("Members:" + s + "\n");
        }
        return sb.toString();
    }
    
    
    
}
