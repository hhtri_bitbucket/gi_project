package com.ctmlab.pathfinding;

/**
 * ************************************************
 * Copyright (C) 2014 Raptis Dimos <raptis.dimos@yahoo.gr>
 *
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 * ************************************************
 */
import com.google.gson.Gson;
import java.awt.Point;
import java.io.*;
import java.util.ArrayList;
import java.util.Scanner;

public class A_StarAlgorithm {

    public static void main(String[] args) throws InvalidLetterException, FileNotFoundException, IOException, HeapException {

        String filename = "/home/hhtri/Downloads/Pathfinding-master/Java/input_files/small_1.txt";

        InputHandler handler = new InputHandler();
        SquareGraph graph = handler.readMap(filename);
        long start = System.currentTimeMillis();
        ArrayList<Node> path = graph.executeAStar();
        System.out.println(System.currentTimeMillis() - start);
        if (path == null) {
            System.out.println("There is no path to target");
        } else {
            System.out.println("The total number of moves from distance to the target are : " + path.size());
            graph.printPath(path);
        }
//        test();
    }

    public static void test() {
        try {
            BufferedReader in = new BufferedReader(new FileReader("D:\\matrix.csv"));
            int len = Integer.parseInt(in.readLine());
            byte[][] matrix = new byte[len][len];
            for(int i=0;i<len;i++){
                for(int j=0;j<len;j++){
                    matrix[i][j] = 0;
                }
            }
            for(int i=0;i<len;i++){
                String[] a = in.readLine().split(",");
                int j = 0;
                for(String s:a){
                    matrix[i][j] = Byte.valueOf(s);
                    j++;
                }
            }
            InputHandler handler = new InputHandler();
            SquareGraph graph = handler.readMap(matrix, new Point(1,1), new Point(9,9));
            long start = System.currentTimeMillis();
            ArrayList<Node> path = graph.executeAStar();
            System.out.println(System.currentTimeMillis() - start);
            if (path == null) {
                System.out.println("There is no path to target");
            } else {
                System.out.println("The total number of moves from distance to the target are : " + path.size());
                graph.printPath(path);
            }
            graph = handler.readMap(matrix, new Point(1,1), new Point(1,9));
            path = graph.executeAStar();
            System.out.println(System.currentTimeMillis() - start);
            if (path == null) {
                System.out.println("There is no path to target");
            } else {
                System.out.println("The total number of moves from distance to the target are : " + path.size());
                graph.printPath(path);
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

}
