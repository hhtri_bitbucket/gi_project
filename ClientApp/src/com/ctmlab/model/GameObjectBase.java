/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ctmlab.model;

import com.ctmlab.staticdata.gameobjectinfo.GameObjectInfo;
import com.google.common.io.LittleEndianDataInputStream;
import com.google.common.io.LittleEndianDataOutputStream;
import java.io.IOException;

/**
 *
 * @author hhtri
 */
public class GameObjectBase {
    public long id; 
    public short id_r;
    public short id_z;
    public short xPos;
    public short yPos;
    
    public GameObjectInfo objInfo = null;
    
    public GameObjectBase(){
        id = 0;
        id_r = id_z = xPos = yPos = -1;
    }
    
    public void refInfo(){
        objInfo = null;
    }
    
    public void writeData(LittleEndianDataOutputStream os) throws IOException{
        os.writeLong(id);
        os.writeShort(id_r);
        os.writeShort(id_z);
        os.writeShort(xPos);
        os.writeShort(yPos);
    }
    
    public void readData(LittleEndianDataInputStream is) throws IOException{
        id = is.readLong();
        id_r = is.readShort();
        id_z = is.readShort();
        xPos = is.readShort();
        yPos = is.readShort();
    }
}
