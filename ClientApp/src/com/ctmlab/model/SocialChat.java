/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ctmlab.model;

import com.ctmlab.manager.Utils;
import com.google.common.io.LittleEndianDataInputStream;
import com.google.common.io.LittleEndianDataOutputStream;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Date;

/**
 *
 * @author hhtri
 */
public class SocialChat {
    public long id;    
    public long groupId;
    public String sender;
    public byte mType;
    public String data;
    public String time;
    
    public SocialChat(){}
    
    public SocialChat(byte type, long groupId, String sender, String data){
        this.mType = type;
        this.groupId = groupId;
        this.sender = sender;
        this.data = data;
    }
    
    public void writeData(LittleEndianDataOutputStream os) throws IOException{
        
        os.writeLong(id);
        os.writeLong(groupId);
        Utils.i().writeBigString(os, sender);        
        os.writeByte(mType);
        byte[] _data = data.getBytes(Utils.i().UTF_8);
        os.writeInt(_data.length);
        os.write(_data);        
        os.writeLong(System.currentTimeMillis());
    }
    
    public void readData(LittleEndianDataInputStream is) throws IOException{
        id = is.readLong();
        groupId = is.readLong();
        sender = Utils.i().readBigString(is);
        mType = is.readByte();
        byte[] _data = new byte[is.readInt()];
        is.read(_data);
        data = new String(_data, Utils.i().UTF_8);
        time = Utils.i().SDF.format(new Date(is.readLong()));
    }
}
