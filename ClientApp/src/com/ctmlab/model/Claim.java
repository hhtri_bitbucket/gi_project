/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ctmlab.model;

import com.ctmlab.staticdata.GameResources;
import com.google.common.io.LittleEndianDataInputStream;
import java.io.IOException;

/**
 *
 * @author hhtri
 */
public class Claim {
    public long id;     
    public String userID;
    public long mineID;    
    public long dirt; // mining and store (having rating)
    public long fuel; // from homebase to claim    
    public long[] spotDirt;
    
    public long[] lstEquipment;
    public long[] lstWorker;
    
    public Claim(){
        userID = "";
        mineID = -1;
        dirt = -1;
        fuel = -1;
        id = 0;
        lstEquipment = null;
        lstWorker = null;
    }
    
    public void readData(LittleEndianDataInputStream is) throws IOException {
        id = is.readLong();
        dirt = is.readLong();
        fuel = is.readLong();
        
        int len = is.readInt();
        int i = 0;
        lstEquipment = new long[len];
        while(i<len){lstEquipment[i++]=is.readLong();}
        
        len = is.readInt();
        i = 0;
        lstWorker = new long[len];
        while(i<len){lstWorker[i++]=is.readLong();}
        GameResources rs = new GameResources();
        rs.readData(is);
    }
}
