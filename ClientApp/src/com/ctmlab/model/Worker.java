/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ctmlab.model;

import com.google.common.io.LittleEndianDataInputStream;
import com.google.common.io.LittleEndianDataOutputStream;
import java.io.IOException;

/**
 *
 * @author hhtri
 */
public class Worker extends GameObjectMap{    
    public int level;
    public long startUpgradeTime;
    public long endUpgradeTime;  
    public long claimID = 0;
    public Worker(){}        
    public Worker(String userid){
        userID = userid;
        level = 1;
        startUpgradeTime = endUpgradeTime = 0;
    }       
    
    @Override
    public void writeData(LittleEndianDataOutputStream os) throws IOException{
        super.writeData(os);
        os.writeInt(level);
        os.writeLong(startUpgradeTime);
        os.writeLong(endUpgradeTime);
    }
    
    @Override
    public void readData(LittleEndianDataInputStream is) throws IOException{
        super.readData(is);
        level = is.readInt();
        startUpgradeTime = is.readLong();
        endUpgradeTime = is.readLong();
        claimID = is.readLong();
    }
}