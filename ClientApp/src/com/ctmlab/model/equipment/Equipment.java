/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ctmlab.model.equipment;

import com.ctmlab.enums.ENUM_UNIT_TYPE;
import com.ctmlab.manager.Utils;
import com.ctmlab.model.GameObjectMap;
import com.google.common.io.LittleEndianDataInputStream;
import java.io.IOException;

/**
 *
 * @author hhtri
 */
public class Equipment extends GameObjectMap{
            
    public long claimID = 0;
    public Equipment(){}    
        
    @Override
    public void readData(LittleEndianDataInputStream is) throws IOException{
        super.readData(is);
    }    
            
    public static Equipment readStatic(LittleEndianDataInputStream is) throws IOException{
        Equipment eq = null;
        ENUM_UNIT_TYPE type = ENUM_UNIT_TYPE.get(is.read());
        switch(type){
            case CLAIM_DUMPTRUCK:{
                eq = new EquipmentDumptruck();                
                break;
            }
            case CLAIM_EXCAVATOR:{
                eq = new EquipmentExcavator();                
                break;
            }
            case CLAIM_OIL_RIG:{
                eq = new EquipmentOilRig();                
                break;
            }
            case CLAIM_WASH_PLANT:{
                eq = new EquipmentWashPlant();                
                break;
            }
        }
        if(eq!=null){
            eq.readData(is);
        }
        return eq;
    }
}
