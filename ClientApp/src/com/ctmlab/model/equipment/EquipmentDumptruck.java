/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ctmlab.model.equipment;


import com.ctmlab.enums.ENUM_UNIT_TYPE;
import com.google.common.io.LittleEndianDataInputStream;
import com.google.common.io.LittleEndianDataOutputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

/**
 *
 * @author hhtri
 */
public class EquipmentDumptruck extends Equipment{
    
    public int level;
    public long startUpgradeTime;
    public long endUpgradeTime;
    
    public EquipmentDumptruck() {
        mType = (byte)ENUM_UNIT_TYPE.CLAIM_DUMPTRUCK.getCode();
        level = 1;
        startUpgradeTime = 0;
        endUpgradeTime = 0;
    }    
        
    @Override
    public void readData(LittleEndianDataInputStream is) throws IOException{
        super.readData(is);
        level = is.readInt();
        startUpgradeTime = is.readLong();
        endUpgradeTime = is.readLong();            
        claimID = is.readLong();            
    }
}