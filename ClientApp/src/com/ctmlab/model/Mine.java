/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ctmlab.model;

import com.ctmlab.staticdata.GameResources;
import com.google.common.io.LittleEndianDataInputStream;
import java.io.IOException;

/**
 *
 * @author hhtri
 */
public class Mine extends GameObjectMap{
//    public GameResources resource = new GameResources();
    public long price;
    public long test_price;      
    public long[] spotDirt;
    public Claim claim = null;
    
    public Mine(){super();}
    
    @Override
    public void readData(LittleEndianDataInputStream is) throws IOException{
        super.readData(is);
//        resource.readData(is);
        int len = is.readInt();
        if(len>0){
            int i = 0;
            spotDirt = new long[len];
            while(i<len){
                spotDirt[i] = is.readLong();
                i++;
            }
        }
        price = is.readLong();
        test_price = is.readLong();
        boolean flag = is.readBoolean();
        if(userID!=null&&!userID.isEmpty()&&flag){
            claim = new Claim();
            claim.userID = userID;
            claim.mineID = id;            
            claim.readData(is);
        }
    }
    
    
}
