/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ctmlab.model;

import com.google.common.io.LittleEndianDataInputStream;
import com.google.common.io.LittleEndianDataOutputStream;
import java.awt.Point;
import java.io.IOException;
import java.util.ArrayList;

/**
 *
 * @author hhtri
 */
public class Exploration extends GameObjectMap{
    
    public long startTime;
    public long endTime;
    public long destID;
    
    
    public ArrayList<Point> path = null;
    
    public Exploration(){}

    public void setTimes(long start, long end){
        startTime = start;
        endTime = end;
    }

    
    @Override
    public void writeData(LittleEndianDataOutputStream os) throws IOException{
        super.writeData(os);
        os.writeLong(startTime);
        os.writeLong(endTime);
        os.writeLong(destID);
    }
    
    @Override
    public void readData(LittleEndianDataInputStream is) throws IOException{
        super.readData(is);
        startTime = is.readLong();
        endTime = is.readLong();
        destID = is.readLong();
        if(is.readBoolean()){
            int len = is.readInt();
            int i = 0;
            path = new ArrayList<>();
            while(i++<len){
                path.add(new Point(is.readShort(), is.readShort()));
            }
        }
    }
}
