/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ctmlab.model;

import com.ctmlab.manager.Utils;
import com.google.common.io.LittleEndianDataInputStream;
import java.io.IOException;
import java.util.Date;

/**
 *
 * @author hhtri
 */
public class GameMessage {
    
    public long id;    
    public String sender;
    public String receiver;
    public String subject;
    public String content;
    public byte mType;
    public byte mState;
    public long time;
    public GameMessage(){}
    
    public GameMessage(String sender, String receiver, String sub, String content, byte type){
        this.sender = sender;
        this.receiver = receiver;
        this.subject = sub;
        this.content = content;
        mType = type;
        mState = 0;
        time = System.currentTimeMillis();
    }
       
    public void readData(LittleEndianDataInputStream is) throws IOException{
        mType = is.readByte();
        id = is.readLong();
        sender = Utils.i().readBigString(is);
        receiver = Utils.i().readBigString(is);
        subject = Utils.i().readBigString(is);
        content = Utils.i().readBigString(is);
        mState = is.readByte();
        time = is.readLong();
    }

    public String printData(){
        String format = "%d,%s,%s,%s,%d,%s";
        String out = String.format(format,
                id, sender, receiver, content, mState, Utils.i().SDF.format(new Date(time)));
        
        return out;
    }
}
