/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ctmlab.model;

import com.google.common.io.LittleEndianDataInputStream;
import com.google.common.io.LittleEndianDataOutputStream;
import java.awt.Point;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Map.Entry;
import java.util.TreeMap;

/**
 *
 * @author hhtri
 */
public class Truck extends GameObjectMap{
    public long startTime;
    public long endTime;
    public long srcID;
    public long destID;
    public int capacity;
    public int level;
    public TreeMap<Byte, Long> resContainer;
    public ArrayList<Point> path = null;
    public Truck(){
        startTime = endTime = destID = -1;
        capacity = 1000;
        level = 1;
        resContainer = new TreeMap();
    }   
    
    public void setTimes(long start, long end){
        startTime = start;
        endTime = end;
    }
    
    @Override
    public void writeData(LittleEndianDataOutputStream os) throws IOException{
        super.writeData(os);
        os.writeLong(startTime);
        os.writeLong(endTime);
        os.writeLong(srcID);
        os.writeLong(destID);
        os.writeInt(capacity);
        os.writeInt(level);
        os.writeInt(resContainer.size());
        for(Entry<Byte, Long> e:resContainer.entrySet()){
            os.writeByte(e.getKey());
            os.writeLong(e.getValue());
        }
    }
    
    @Override
    public void readData(LittleEndianDataInputStream is) throws IOException{
        super.readData(is);
        startTime = is.readLong();
        endTime = is.readLong();
        srcID = is.readLong();
        destID = is.readLong();
        capacity = is.readInt();
        level = is.readInt();
        int len = is.readInt();
        int i = 0;
        while(i++<len){
            resContainer.put(is.readByte(), is.readLong());
        }
        if(is.readBoolean()){
            len = is.readInt();
            i = 0;
            path = new ArrayList<>();
            while(i++<len){
                path.add(new Point(is.readShort(), is.readShort()));
            }
        }
    }
}
