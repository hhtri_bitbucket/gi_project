/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ctmlab.model.building;

import com.ctmlab.enums.ENUM_UNIT_TYPE;
import com.google.common.io.LittleEndianDataInputStream;
import java.io.IOException;

/**
 *
 * @author hhtri
 */
public class BuildingDefense extends Building{
    
    public int level;
    public long startUpgradeTime;
    public long endUpgradeTime;
    
    public BuildingDefense() {
        mType = (byte)ENUM_UNIT_TYPE.HOME_DEFENSE.getCode();
        level = 1;
        startUpgradeTime = 0;
        endUpgradeTime = 0;
    }
    
    @Override
    public void readData(LittleEndianDataInputStream is) throws IOException {
        super.readData(is);
        level = is.readInt();
        startUpgradeTime = is.readLong();
        endUpgradeTime = is.readLong();
    }
}
