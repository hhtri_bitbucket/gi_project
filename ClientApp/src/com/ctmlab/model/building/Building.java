/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ctmlab.model.building;
import com.ctmlab.enums.ENUM_UNIT_TYPE;
import com.ctmlab.manager.Utils;
import com.ctmlab.model.GameObjectMap;
import com.google.common.io.LittleEndianDataInputStream;
import java.io.IOException;

/**
 *
 * @author hhtri
 */
public class Building extends GameObjectMap{
    
    public Building(){
    }
    
    @Override
    public void readData(LittleEndianDataInputStream is) throws IOException{
        super.readData(is);
    }
    
    public static Building readStatic(LittleEndianDataInputStream is) throws IOException{
        ENUM_UNIT_TYPE type = ENUM_UNIT_TYPE.get(is.read());
        Building b = null;
        switch(type){
            case HOME_BARRACK:{
                b = new BuildingBarrack();
                break;
            }
            case HOME_DEFENSE:{
                b = new BuildingDefense();
                break;
            }
            case HOME_EXPLORATION:{
                b = new BuildingExploration();
                break;
            }
            case HOME_OFFICE:{
                b = new BuildingOfficer();
                break;
            }
            case HOME_REFINERY:{
                b = new BuildingRefinely();
                break;
            }
            case HOME_TRANSPORTATION:{
                b = new BuildingTransportation();
                break;
            }
            case HOME_WALL:{
                b = new BuildingWall();
                break;
            }
            case HOME_WEAPON:{
                b = new BuildingWeaponDepot();
                break;
            }
        }
        if(b!=null){
            b.readData(is);
        }
        return b;
    }
}
