/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ctmlab.model;

import com.ctmlab.manager.Utils;
import com.google.common.io.LittleEndianDataInputStream;
import com.google.common.io.LittleEndianDataOutputStream;
import com.google.gson.Gson;
import java.io.IOException;

/**
 *
 * @author hhtri
 */
public class GameObjectMap extends GameObjectBase{
    
    public String userID = "";
    public byte mType = 0;
    
    public GameObjectMap(){super();}
    
    public GameObjectMap(byte mType){
        super();
        this.mType = mType;
    }

    public void refClaim(Claim claim){}

    @Override
    public void readData(LittleEndianDataInputStream is) throws IOException{
        super.readData(is);
        userID = Utils.i().readBigString(is);
        mType = is.readByte();
    }
    
    public String printLog(Gson json, Gson pson){
        return "";
    }
}
