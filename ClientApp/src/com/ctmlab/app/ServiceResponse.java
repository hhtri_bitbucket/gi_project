/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ctmlab.app;

/**
 *
 * @author hhtri
 */
public class ServiceResponse {
    public int returnCode;
    public String message;
    public byte[] data;
    public ServiceResponse(){
        returnCode = -1;
        message = "";
        data = new byte[0];
    }
}
