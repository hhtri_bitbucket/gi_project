/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ctmlab.app;

import java.awt.Point;
import java.util.ArrayList;

/**
 *
 * @author hhtri
 */
public class TestMatrix {
    
    public class zone{
        Point[] items;
        Point pos;
        public zone(Point p){
            pos = p;
            items = new Point[9];
            int idx = 0;
            for(int i=0; i<3;i++){
                for(int j=0; j<3; j++){
                   items[idx++] = new Point(i, j);
                }
            }
        }
    }
    
    public static ArrayList<Point[][]> main = new ArrayList();
    
    public static Point[][] getBaseMatrix(int x, int y){
        Point[][] baseMatrix = new Point[3][3];
        for(int i=0; i<3;i++){
            for(int j=0; j<3; j++){
               baseMatrix[i][j] = new Point(x+i, y+j);
            }
        }
        return baseMatrix;
    }
    
    public static void genMatrix(){
        main = new ArrayList<>();
        for(int i=0;i<5;i++){
            for(int j=0; j<5; j++){
                main.add(getBaseMatrix(i*3, j*3));
            }
        }
    }
    
    public static void printMatrix(){
        for(Point[][] p:main){
            String line = "";
            for(int i=0;i<3;i++){
                String temp = "";
                for(int j=0;j<3;j++){
                    if(!temp.isEmpty()){temp+=";";}
                    temp += p[i][j].x+","+p[i][j].y;
                }
                line += temp + "\n";
            }
            System.out.println(line);
            System.out.println("*******************");            
        }
    }
    
    public static void main(String[] args) {
        genMatrix();
        printMatrix();
    }
}
