/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ctmlab.app;

import com.ctmlab.enums.ENUM_UNIT_PROPERTY;
import com.ctmlab.manager.Utils;
import com.ctmlab.staticdata.gameobjectinfo.GameObjectBarrackInfo;
import com.ctmlab.staticdata.gameobjectinfo.GameObjectDefenseInfo;
import com.ctmlab.staticdata.gameobjectinfo.GameObjectExplorationInfo;
import com.ctmlab.staticdata.gameobjectinfo.GameObjectInfo;
import com.ctmlab.staticdata.gameobjectinfo.GameObjectOfficeInfo;
import com.ctmlab.staticdata.gameobjectinfo.GameObjectRefineryInfo;
import com.ctmlab.staticdata.gameobjectinfo.GameObjectTransportationInfo;
import com.ctmlab.staticdata.gameobjectinfo.GameObjectWallInfo;
import com.ctmlab.staticdata.gameobjectinfo.GameObjectWeaponDepotInfo;
import com.ctmlab.staticdata.gameobjectinfo.UpgradeInfo;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;

/**
 *
 * @author hhtri
 */
public class CSVToJSON {

    public static Gson pson = new GsonBuilder().serializeNulls().setPrettyPrinting().create();
    public static Gson gson = new Gson();

    public static String PATH_FOLDER = "/home/hhtri/Desktop/workspace/netbean/external/8080/config/goldinc_csv_config";

    public static String[] FILENAMES = {
        "barracks", "defense", "exploration", "office",
        "protective", "refinery", "transportation", "weapon"
    };
    
    public static void convertWashingDirt(){
        String path = PATH_FOLDER + File.separator + "washing_dirt.csv";
        String dataString = Utils.i().readTextFile(path);
        String[] texts = dataString.split("\n");
        JsonArray arr = new JsonArray();
        for(String s:texts){
            System.out.println(s);
            String[] data = s.split(",");
            byte type = Byte.valueOf(data[0]);
            int rating = ((Double)(Double.valueOf(data[1])*100)).intValue();
            data = data[2].split("-");
            int aMin = ((Double)(Double.valueOf(data[0])*1000)).intValue();
            int aMax = ((Double)(Double.valueOf(data[1])*1000)).intValue();
            JsonObject obj = new JsonObject();
            obj.addProperty("type", type);
            obj.addProperty("rating", rating);
            obj.addProperty("amount_min", aMin);
            obj.addProperty("amount_max", aMax);
            arr.add(obj);
        }
        System.out.println(pson.toJson(arr));
    }
    
    public static void convertJSONGameObject(){
        for (String name : FILENAMES) {
            String path = PATH_FOLDER + File.separator + name + ".csv";
//            System.out.println(path);
            String dataString = Utils.i().readTextFile(path);
//            System.out.println(dataString);
            String[] texts = dataString.split("\n");
            int max_lvl = 0;
            HashMap<Byte, Long>[] info = null;
            UpgradeInfo[] upgrade_require = null;
            String[] temp = null;
            for (String it : texts) {
                String t = it.toLowerCase().trim();
                if (t.startsWith("max")) {
                    max_lvl = Integer.parseInt(t.split(",")[1]);
                    info = new HashMap[max_lvl];
                    upgrade_require = new UpgradeInfo[21];
                    for (int i = 0; i < max_lvl; i++) {
                        info[i] = new HashMap<>();
                        upgrade_require[i] = new UpgradeInfo();
                    }
                } else if (t.startsWith("info")) {
                    temp = t.split(",");
                    byte key = ENUM_UNIT_PROPERTY.getCode(Byte.valueOf(temp[0].split("-")[1]));
                    for (int i = 1; i < temp.length; i++) {
                        HashMap<Byte, Long> hash = info[i - 1];
                        if(ENUM_UNIT_PROPERTY.isTime(key)){
                            Double db = Double.valueOf(temp[i])*1000;
                            hash.put(key, db.longValue());
                        } else{
                            hash.put(key, Long.parseLong(temp[i]));
                        }
                    }
                } else if (t.startsWith("req")) {
                    temp = t.split(",");
                    String s = temp[0];
                    for (int i = 1; i < temp.length; i++) {
                        UpgradeInfo ui = upgrade_require[i - 1];
                        if (s.contains("time")) {
                            ui.timeUpgrade = 1000*Long.parseLong(temp[i]);
                        } else if (s.contains("resource")) {
                            ui.addResourceReq(Byte.parseByte(s.split("-")[2]), Long.parseLong(temp[i]));
                        } else if (s.contains("building")) {
                            ui.addBuildingReq(Byte.parseByte(s.split("-")[2]), Integer.parseInt(temp[i]));
                        }
                    }
                }
            }
            GameObjectInfo obj = null;
            switch (name) {
                case "office": {
                    obj = new GameObjectOfficeInfo();
                    obj.setName("office info");
                    break;
                }
                case "defense": {
                    obj = new GameObjectDefenseInfo();
                    obj.setName("defense info");
                    break;
                }
                case "barracks": {
                    obj = new GameObjectBarrackInfo();
                    obj.setName("barracks info");
                    break;
                }
                case "refinery": {
                    obj = new GameObjectRefineryInfo();
                    obj.setName("refinery info");
                    break;
                }
                case "exploration": {
                    obj = new GameObjectExplorationInfo();
                    obj.setName("exploration info");
                    break;
                }
                case "transportation": {
                    obj = new GameObjectTransportationInfo();
                    obj.setName("transportation info");
                    break;
                }
                case "weapon": {
                    obj = new GameObjectWeaponDepotInfo();
                    obj.setName("weapon info");
                    break;
                }
                case "protective": {
                    obj = new GameObjectWallInfo();
                    obj.setName("protective info");
                    break;
                }
            }
            if (obj != null) {
                obj.setVersion("1.0.1");
                obj.setMaxLevel(max_lvl);
                obj.setSize((short) 3, (short) 3);
                obj.setMaxLevel(max_lvl);
                obj.setInfo(info);
                obj.setReq(upgrade_require);
            }
            System.err.println(gson.toJson(obj));
            Utils.i().writeTextFile("/home/hhtri/Desktop/workspace/netbean/external/8080/config/output/" + name + ".json", pson.toJson(obj));
        }
    }

    public static void main(String[] args) {
        convertWashingDirt();
    }
}
