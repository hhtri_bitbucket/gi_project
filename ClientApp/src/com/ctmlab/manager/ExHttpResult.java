/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ctmlab.manager;

/**
 *
 * @author ccthien
 */
public class ExHttpResult {
    public String text = "";
    public int responseCode = 0;
    public String error = null;
    public byte []data = null;
    public boolean isDown() {
        if(responseCode==503) return true;
        return error!=null;
    }
}
