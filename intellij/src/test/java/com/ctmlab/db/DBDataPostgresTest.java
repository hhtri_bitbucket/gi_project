/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ctmlab.db;

import com.ctmlab.game.data.model.Transportation;
import com.dp.db.DB;
import com.dp.db.DBDataSQLInfo;
import com.dp.db.Func;
import com.opentable.db.postgres.embedded.EmbeddedPostgres;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import org.junit.Before;
import org.junit.BeforeClass;

/**
 *
 * @author hhtri
 */
public class DBDataPostgresTest {

    Transportation t = new Transportation();
    
    public void init() throws Exception {
        new Transportation().validateTable(DB.i().con());
    }

    public void viewSQLScript() throws SQLException {
        Transportation t = new Transportation();
        DBDataSQLInfo info = t.getSQLInfo();

        System.out.println(">>>Begin viewSQLScript test<<<");
        System.out.println(info.fieldCount);
        System.out.println(info.insertSQL);
        System.out.println(info.saveSQL);
        System.out.println(info.updateSQL);
        System.out.println(">>>End viewSQLScript test<<<");
    }

    public void testInsert() throws SQLException {
        System.out.println(">>>Begin testInsert test<<<");
        t = new Transportation();
        t.saveData();
        System.out.println(">>>End testInsert test<<<");
    }

    public void testSave() throws SQLException {
        System.out.println(">>>Begin testSave test<<<");
        t = new Transportation();
            t.saveData();
        System.out.println(">>>End testSave test<<<");
    }

    public void testDel() throws SQLException {
        System.out.println(">>>Begin testDel test<<<");

        System.out.println(">>>End testDel test<<<");
    }

    public void viewList() throws SQLException {
        System.out.println(">>>Begin viewList test<<<");
        ArrayList<Transportation> ls = DB.i().select(Transportation.class);
        System.out.println("Rows: " + ls.size());
        ls.forEach((tp) -> {
            tp.parseJson();
            System.out.println(Func.json(tp));
        });
        System.out.println(">>>End viewList test<<<");
    }
    
    public void testEmbeddedPostgres(){
        try (EmbeddedPostgres pg = EmbeddedPostgres.start();
                Connection c = pg.getPostgresDatabase().getConnection()) {
            Statement s = c.createStatement();
            ResultSet rs = s.executeQuery("SELECT 1");
            assertTrue(rs.next());
            assertEquals(1, rs.getInt(1));
            assertFalse(rs.next());
            System.out.print("231312312");
        } catch (Exception e) {
            System.out.print(e);
        }
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
//        DB.i(new DBPostgreSQL("localhost", "goldinc", "admin", "123postgres456!"));
    }

    @After
    public void tearDown() {
    }

    @org.junit.Test
    public void testSomeMethod() throws Exception {
//        init();
//        viewSQLScript();
//        viewList();
//        testInsert();
//        viewList();
//        testSave();
//        viewList();
//        testDel();
//        viewList();
    }
}
