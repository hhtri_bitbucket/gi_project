/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ctmlab.map;

import com.ctmlab.game.data.builder.GoldIncUserBuilder;
import com.ctmlab.game.data.model.GoldIncUser;
import com.ctmlab.game.data.model.building.Building;
import com.ctmlab.game.data.model.building.BuildingExploration;
import com.ctmlab.game.data.model.building.BuildingOfficer;
import com.ctmlab.game.data.model.building.BuildingTransportation;
import com.ctmlab.game.data.model.worker.Worker;
import com.ctmlab.game.data.pojo.ProfilePrivate;
import com.ctmlab.game.data.staticdata.Region;
import com.ctmlab.game.data.staticdata.Zone;
import com.ctmlab.game.data.staticdata.gameobjectinfo.GameObjectInfoRepo;
import com.ctmlab.game.data.staticdata.gameobjectinfo.GameObjectOfficeInfo;
import com.ctmlab.game.data.staticdata.gameobjectinfo.GameObjectWashingDirtInfo;
import com.ctmlab.game.data.staticdata.gameobjectinfo.GameObjectWeaponDepotInfo;
import com.ctmlab.game.goldincenum.ENUM_RESOURCE_TYPE;
import com.ctmlab.game.manager.GlobalConfig;
import com.ctmlab.game.manager.GoldIncMng;
import com.ctmlab.game.manager.StaticDataMng;
import com.ctmlab.game.network.manager.WSGameDefine;
import com.dp.db.DB;
import com.dp.db.DBPostgreSQL;
import com.dp.db.Func;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.*;

/**
 *
 * @author hhtri
 */
public class TestMap {

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {

    }

    @After
    public void tearDown() {
    }

    public void testMatrix(){
        int i = 0;
        int n = 10;
        while(i++<n){
            System.out.println(String.format("Measure %d", i));
            long start = System.currentTimeMillis();
            long end = 0;
            Region r = StaticDataMng.i().worldMap.getRegion((short)0);
            end = System.currentTimeMillis();
            System.out.println(String.format("Time get region is %d", (end-start)));

            Zone z1 = null;
            Zone z2 = null;
            start = System.currentTimeMillis();
            for(Zone z:r.zones.values()){
                if(z.x()==100 &&z.y()==300){
                    z1 = z;
                } else if(z.x()==700 &&z.y()==700){
                    z2 = z;
                } else if(z1!=null && z2!=null){
                    break;
                }
            }
            end = System.currentTimeMillis();
            System.out.println(String.format("Time get 2 zones random is %d", (end-start)));

            start = System.currentTimeMillis();
            byte[][] rs = r.getInnerMatrix(z1, z2);
            end = System.currentTimeMillis();
            System.out.println(String.format("Size of inner matrix is %d:%d", rs.length, rs[0].length));
            System.out.println(String.format("Time get inner matrix is %d", (end-start)));
            rs = null;
        }
    }

    public void testAddress(){
        GoldIncUser u = null;
        int id = 1;
        HashMap<String, GoldIncUser> hUsers = new HashMap();
        while(id<=3){
            u = new GoldIncUserBuilder()
                    .setUserID(String.format("snipertest00%d@gmail.com", id))
                    .setPassword("123")
                    .setCompanyName(String.format("company %d", id))
                    .build();
            System.out.println(u.password);
            hUsers.put(u.userID, u);
            id++;
        }

//        System.out.println("--------------------------");
//
//        for(GoldIncUser us:hUsers.values()){
//            System.out.println(us.hashCode());
//        }

        System.out.println("--------------------------");

        GoldIncUser[] temp = hUsers.values().toArray(new GoldIncUser[0]);
        for(GoldIncUser us:temp){
            us.password = 456 + "";
        }

        for(GoldIncUser us:hUsers.values()){
            System.out.println(us.password);
        }

//        System.out.println("--------------------------");
//
//        GoldIncUser[] temp1 = new GoldIncUser[3];
//        temp = hUsers.values().toArray(temp1);
//        for(GoldIncUser us:temp1){
//            System.out.println(us.hashCode());
//        }

    }

    private void rdResource() {
        Map.Entry<ENUM_RESOURCE_TYPE, GameObjectWashingDirtInfo.RatingInfo>[] temp;
        TreeMap<ENUM_RESOURCE_TYPE, GameObjectWashingDirtInfo.RatingInfo> info = StaticDataMng.i().getRepoGameObjectInfo().washingDirtInfo.getInfo();
        synchronized (info) {
            temp = info.entrySet().toArray(WSGameDefine.EMPTY_ENTRY);
        }
        TreeMap<ENUM_RESOURCE_TYPE, Long> result = new TreeMap();
        int i = 0;
        StringBuilder sb = new StringBuilder();
        while (i++ < 10) {
            for (Map.Entry<ENUM_RESOURCE_TYPE, GameObjectWashingDirtInfo.RatingInfo> e : temp) {
                GameObjectWashingDirtInfo.RatingInfo item = e.getValue();
                Long val = 0L;
                if (result.containsKey(e.getKey())) {
                    val = result.get(e.getKey());
                } else{
                    result.put(e.getKey(), val);
                }
                int ratting = GlobalConfig.GI_RANDOM.nextInt(101);

                if(ratting>=0 && ratting<=item.getRating()){
                    int tmp = GlobalConfig.rdInt(item.getAmountMin(), item.getAmountMax());
                    val += tmp;
                    sb.append(String.format("\n%s - [%s][rating][amount][value] - [%d <=> %d]- [%d-%d] - %d]",
                            "test", e.getKey().toString(), item.getRating(), ratting, item.getAmountMin(), item.getAmountMax(), tmp));
                } else{
                    sb.append(String.format("\n%s - [%s][rating][amount][value] - [%d <=> %d]- [%d-%d]]",
                            "test", e.getKey().toString(), item.getRating(), ratting, item.getAmountMin(), item.getAmountMax()));
                }
            }
            sb.append("\n\n*********************************************************\n");
        }
        System.out.println(sb);
    }
        
    @org.junit.Test
    public void testSomeMethod() throws Exception {
        GoldIncMng.i().initDefault(true);
//        testMatrix();
//        testAddress();
//        rdResource();
    }
}
