var __url_api__ = "ad_api.jsp";
function doGet(url, data, callback, callbackErr){
    return $.ajax({
        url: url,
        data: data,
        type: 'GET',
        success: function (res) {
            if(callback){
                callback(res);
            }
        },
        error:function(res){
            if(callbackErr){
                callbackErr(res);
            }
        }
    });
}

function doPost(url, data, callback, callbackErr){
    return $.ajax({
        url: url,
        data: data,
        type: 'POST',
//        contentType: 'application/json',
        success: function (res) {
            if(callback){
                callback(res);
            }
        },
        error:function(res){
            if(callbackErr){
                callbackErr(res);
            }
        }
    });
}

function doPut(url, data, callback, callbackErr){
    return $.ajax({
        url: url,
        data: data,
        type: 'PUT',
        contentType: 'application/json',
        success: function (res) {
            if(callback){
                callback(res);
            }
        },
        error:function(res){
            if(callbackErr){
                callbackErr(res);
            }
        }
    });
}

function doDelete(url, data, callback, callbackErr){
    return $.ajax({
        url: url,
        data: data,
        contentType: 'application/json',
        type: 'DELETE',
        success: function (res) {
            if(callback){
                callback(res);
            }
        },
        error:function(res){
            if(callbackErr){
                callbackErr(res);
            }
        }
    });
}

function onClickUserInfo(){
    window.location.href = "user_admin_info.jsp";
}

function onClickAdminMng(){
    window.location.href = "user_admin_mng.jsp";
}

function onClickUserMng(){
    window.location.href = "user_mng.jsp";
}

function onClickMapMng(){
    window.location.href = "matrix_zone.jsp";
}

function onClickCashConvert(){
    window.location.href = "cash_convert.jsp";
}

function onClickGameObjectInfo(){
    window.location.href = "gameobjectinfo.jsp";
}

function onClickbtnMsgTemplate(){
    window.location.href = "msg_template.jsp";
}