var HOTKEY_F1 = 112;
var HOTKEY_F2 = 113;
var HOTKEY_CONSOLE = 192;
var NULL = undefined;

function isFunction(o) {
    return( Object.prototype.toString.call( o ) === '[object Function]' );
}
function isObject(o) {
    return( Object.prototype.toString.call( o ) === '[object Object]' );
}
function isArray(o) {
    return( Object.prototype.toString.call( o ) === '[object Array]' );
}
function isNumber(v) {
    return (typeof v === "number");
}
function applyHotkey(func,keyCode) {
    if(!isNumber(keyCode)) keyCode = HOTKEY_CONSOLE;
    document.addEventListener('keyup', function(e) {
        if (e.ctrlKey) {
            if(e.keyCode === keyCode) {
                if(func) return func();
            }
        }
    }, false);	
}

function setCookie(c_name,value,exdays) {
    if(exdays===undefined) exdays = 20*365;
    var exdate=new Date();
    exdate.setDate(exdate.getDate() + exdays);
    var c_value=escape(value) + "; expires="+exdate.toUTCString();
    document.cookie=c_name + "=" + c_value;
}

function clearCookie(c_name) {
    var exdate=new Date(0);
    document.cookie=c_name + "=; expires="+exdate.toUTCString();
}

function getCookie(c_name) {
    var i,x,y,ARRcookies=document.cookie.split(";");
    for (i=0;i<ARRcookies.length;i++) {
        x=ARRcookies[i].substr(0,ARRcookies[i].indexOf("="));
        y=ARRcookies[i].substr(ARRcookies[i].indexOf("=")+1);
        x=x.replace(/^\s+|\s+$/g,"");
        if (x==c_name) {
            return unescape(y);
        }
    }
}
function wsURL(uri) {
    var loc = window.location, new_uri;
    if (loc.protocol === "https:") {
        new_uri = "wss:";
    } else {
        new_uri = "ws:";
    }
    new_uri += "//" + loc.host + "/" + loc.pathname.split("/")[1] + "/" + uri;
    return new_uri;
}
var query_string = null;
function getQueryString() {
    if(query_string===null) {
        query_string = {};
        var queryString = location.search.slice(1), re = /([^&=]+)=([^&]*)/g, m;
        while (m = re.exec(queryString)) {query_string[decodeURIComponent(m[1])] = decodeURIComponent(m[2]);}
    }
    return query_string;
}

//////////////////////////////////////////

function twoDecimal(num) {return parseFloat(Math.round(num * 100) / 100).toFixed(2);}
function _self() {return window.location.href;}

function isDefined(o) {
    return !(o === undefined);
}
function isString(o) {
    return (typeof o === "string");
}
function isInteger(str) {
	return /^\d+$/.test(str);
}
function isFloat(str) {
	return /^[+-]?\d+(\.\d+)?$/.test(str);
}
function _obj(id) {
    return document.getElementById(id);
}
function _html(o,hmtl) {
    if(isString(o)) o  = _obj(o);
    o.innerHTML = hmtl;
}
function _empty(o) {
    if(isString(o)) o  = _obj(o);
    o.innerHTML = "";
}
function _add(o,obj) {
    if(isString(o)) o  = _obj(o);
    o.appendChild(obj);
}
function _load(o,obj) {
    if(isString(o)) o  = _obj(o);
    o.innerHTML = "";
    o.appendChild(obj);
    return obj;
}
function _enable(o) {
    if(isString(o)) o  = _obj(o);
    o.disabled=false;
}
function _disable(o) {
    if(isString(o)) o  = _obj(o);
    o.disabled=true;
}
function _show(o) {
    if(isString(o)) o  = _obj(o);
    o.style.display="";
}
function _hide(o) {
    if(isString(o)) o  = _obj(o);
    o.style.display="none";
}
function _create(tag) {
    return document.createElement(tag);
}

function textToHTML(text)
{
    var charEncodings = {
        "\t": "&nbsp;&nbsp;&nbsp;&nbsp;",
        " ": "&nbsp;",
        "&": "&amp;",
        "<": "&lt;",
        ">": "&gt;",
        "\n": "<br />",
        "\r": "<br />"
    };
    var space = /[\t ]/;
    var noWidthSpace = "&#8203;";
    text = (text || "") + "";  // make sure it's a string;
    text = text.replace(/\r\n/g, "\n");  // avoid adding two <br /> tags
    var html = "";
    var lastChar = "";
    for (var i in text)
    {
        var char = text[i];
        var charCode = text.charCodeAt(i);
        if (space.test(char) && !space.test(lastChar) && space.test(text[i + 1] || ""))
        {
            html += noWidthSpace;
        }
        html += char in charEncodings ? charEncodings[char] :
        charCode > 127 ? "&#" + charCode + ";" : char;
        lastChar = char;
    }
    return html;
}
function createTable(parent) {
    var table = document.createElement("table");
    if(parent!==NULL) {_add(parent,table);}
    table.border = 1;
    table.createRow = function(pos) {
        var row = document.createElement("tr");
        row.createCell = function(value) {
            var cell = document.createElement("td");
            $(cell).html(textToHTML(value));
            this.appendChild(cell);
            cell.value = value;
            cell.getValue = function() {return this.value;};
            cell.setValue = function(v) {
                this.value=v;
                _html(this,v);
            };
            cell.setEditable = function(type,func) {
                switch(type) {
                    case "image":
                        var the_cell = this;
                        this.ondragover = function () { this.style.backgroundColor = '#FAEBD7'; return false; };
                        this.ondragend = function () { this.style.backgroundColor = 'white'; return false; };
                        this.ondragleave = this.ondragend;
                        this.ondrop = function (e) {
                            this.ondragend();
                            e = e || window.event;
                            if (e.preventDefault) { e.preventDefault(); }
                            var dt = e.dataTransfer;
                            var files = dt.files;
                            for (var i=0; i<files.length; i++) {
                                var file = files[i];
                                var reader = new FileReader();
                                reader.onloadend = function(e, file) {
                                    the_cell.loadImage(this.result);
                                };
                                reader.readAsDataURL(file);
                                
                                if(the_cell.uploadDone) {
                                    if(the_cell.uploadStart) {
                                        the_cell.uploadStart();
                                    }
                                    var formData = new FormData();
                                    formData.append("file", file);
                                    var url = 'do_upload.jsp';
                                    if(the_cell.extra) {
                                        for(var key in the_cell.extra) {
                                            if(key==='url') url = the_cell.extra[key];
                                            else formData.append(key, the_cell.extra[key]);
                                        }
                                    }
                                    $.ajax({
                                        url: url,
                                        data: formData,
                                        processData: false,
                                        contentType: false,
                                        type: 'POST',
                                        success: function(data){
                                            the_cell.uploadDone(data);
                                        }
                                    });
                                }
                            }
                            return false;                            
                        };
                        this.loadImage = function(data) {
                            this.img = document.createElement("img");
                            this.img.src = data;
                            this.img.onload = function() {
                                if(the_cell.onImageLoaded) the_cell.onImageLoaded(this);
                            };
                            this.getValue = function() {
                                if(isString(this.value) && this.value!=="") return this.value;
                                return this.img.src;
                            };
                            _load(this,this.img);
                        };
                        this.updateValue = function() {
                            if(this.value && this.value.length>0) {
                                this.loadImage(this.value);
                            }else{
                                _html(this,"Drop image here...");
                            }
                        }
                        this.updateValue();
                        
                        if(isObject(func)) {
                            cell.extra = {};
                            for(var key in func) {
                                if(!isFunction(func[key])) {
                                    cell.extra[key] = func[key];
                                }
                            }
                            cell.uploadDone = func.uploadDone;
                        }
                        break;
                    case "checkbox":
                        this.input = document.createElement("input");
                        this.input.type="checkbox";
                        this.input.checked = this.value;
                        this.getValue = function() {return this.input.checked;};
                        _load(this,this.input);
                        break;
                    case "button":
                        this.input = document.createElement("input");
                        this.input.type="button";
                        this.input.value = this.value;
                        this.input.cell = this;
                        if(func) this.input.onclick = func;
                        this.getValue = function() {return this.input.value;};
                        _load(this,this.input);
                        break;
                    case "textarea":
                        this.input = document.createElement("textarea");
                        this.input.value = this.value;
                        this.input.cell = this;
                        this.getValue = function() {return this.input.value;};
                        _load(this,this.input);
                        break;
                    case "select":
                        this.input = document.createElement("select");
                        for(var i=0;i<func.length;i++) {
                            var option = document.createElement("option");
                            option.text = func[i].display;
                            option.value = func[i].value;
                            this.input.add(option);
                        }
                        this.input.value = this.value;
                        this.input.cell = this;
                        this.getValue = function() {return this.input.value;};
                        _load(this,this.input);
                        break;
                    case "input":
                    default:
                        this.input = document.createElement("input");
                        this.input.value = this.value;
                        this.input.cell = this;
                        this.getValue = function() {return this.input.value;};
                        _load(this,this.input);
                }
                return this;
            };
            cell.row = this;
            cell.table = this.table;
            cell.show = function() {this.style.display="";return this;}
            cell.hide = function() {this.style.display="none";return this;}
            return cell;
        };
        row.createHeader = function(text) {
            var cell = document.createElement("th");
            cell.value = text;
            cell.getValue = function() {return this.value;}
            $(cell).text(text);
            this.appendChild(cell);
            cell.row = this;
            cell.show = function() {this.style.display="";return this;}
            cell.hide = function() {this.style.display="none";return this;}
            return cell;
        };
        if(isDefined(pos)) this.insertBefore(row, this.children[pos]);
        else this.appendChild(row);
        row.index = this.rows.length-1;
        row.table = this;
        return row;
    };
    table.getValue = function() {
        var values = [];
        for(var irow in this.rows) {
            if(isInteger(irow)) {
                var row = this.rows[irow];
                if(row.cells.length>0 && row.cells[0].tagName!=="TH") {
                    values[values.length] = [];
                    for(var icell in row.cells) {
                        if(isInteger(icell)) {
                            var cell = row.cells[icell];
                            values[values.length-1][icell]=cell.getValue();
                        }
                    }
                }
            }
        }
        return values;
    };
    table.getValueWithHeader = function() {
        var values = [];
        for(var irow in this.rows) {
            if(isInteger(irow)) {
                var row = this.rows[irow];
                if(row.cells.length>0) {
                    values[values.length] = [];
                    var cell_idx = 0;
                    for(var icell in row.cells) {
                        if(isInteger(icell)) {
                            var cell = row.cells[icell];
                            values[values.length-1][cell_idx]=cell.getValue();
                            if(cell.colSpan) cell_idx+=cell.colSpan;else cell_idx++;
                        }
                    }
                }
            }
        }
        return values;
    };
    table.setVisibleColumn = function(col,b) {
        for(var irow=0;irow<this.rows.length;irow++) {
            this.rows[irow].cells[col].style.display=(b)?"":"none";
        }
    };
    return table;
}
function createDataTable(data) {
    var table = createTable();
    table.data = data;
    for(var i=0;i<data.length;i++) {
        var row = table.createRow();
        var record = data[i];
        for(var key in record) {
            var value = record[key];
            var cell = row.createCell(value);
            if(isObject(value)) {
                cell.ctable = createTable();
                _load(cell,cell.ctable);
                for(var vkey in value) {
                    var crow = cell.ctable.createRow();
                    crow.createCell(vkey);
                    crow.createCell(value[vkey]);
                }
                cell.getValue = function() {
                    var data = {};
                    for(var i=0;i<this.ctable.rows.length;i++) {
                        var row = this.ctable.rows[i];
                        data[row.cells[0].getValue()]=row.cells[1].getValue();
                    }
                    return data;
                };
            }else{
            }
        }
    }
    table.getData = function() {
        for(var i=0;i<this.data.length;i++) {
            var record = this.data[i];
            var idx = 0;
            for(var key in record) {
                this.data[i][key] = this.rows[i].cells[idx++].getValue();
            }
        }
        return this.data;
    };
    return table;
}
function getGameMode(mode) {
    switch(mode) {
        case 0:return "Offline";
        case 1:return "Client";
        case 2:return "Server";
        default:return "Unknown";
    }
}
function formatTime(time) {
    var str ="ago";
    time /= 1000;if(Math.floor(time%60)>0) {str = Math.floor(time%60)+" second(s) "+str;}
    time /= 60;if(Math.floor(time%60)>0) {str = Math.floor(time%60)+" minute(s) "+str;}
    time /= 60;if(Math.floor(time%24)>0) {str = Math.floor(time%24)+" hour(s) "+str;}
    time /= 24;if(Math.floor(time%30)>0) {str = Math.floor(time%30)+" day(s) "+str;}
    time /= 30;if(Math.floor(time%12)>0) {str = Math.floor(time%12)+" month(s) "+str;}
    time /= 12;if(Math.floor(time)>0) str = Math.floor(time)+" year(s) "+str;
    if (str==='ago') str ='Just now';
    return str;
}

var MILI_SECOND = 1000;
var MILI_MINUTE = 60*MILI_SECOND;
var MILI_HOUR = 60*MILI_MINUTE;
var MILI_DAY = 24*MILI_HOUR;
var MILI_MONTH = 30*MILI_DAY;
var MILI_YEAR = 12*MILI_MONTH;
function shortTime(time) {
    if(time>MILI_YEAR) return Math.floor(time/MILI_YEAR)+"Y";
    if(time>MILI_MONTH) return Math.floor(time/MILI_MONTH)+"M";
    if(time>MILI_DAY) return Math.floor(time/MILI_DAY)+"D";
    if(time>MILI_HOUR) return Math.floor(time/MILI_HOUR)+"h";
    if(time>MILI_MINUTE) return Math.floor(time/MILI_MINUTE)+"m";
    if(time>MILI_SECOND) return Math.floor(time/MILI_SECOND)+"s";
    return "0s";
}

//////////////////////////////////////////
// replace \r\n --> \\n" +\n"
function addPopupStyle() {
    var style = document.createElement("style");
    style.innerHTML = (
            "	    .black_overlay{\n" +
            "	        position: fixed;\n" +
            "	        top: 0%;\n" +
            "	        left: 0%;\n" +
            "	        width: 100%;\n" +
            "	        height: 100%;\n" +
            "	        background-color: black;\n" +
            "	        z-index:1001;\n" +
            "	        -moz-opacity: 0.8;\n" +
            "	        opacity:.80;\n" +
            "	        filter: alpha(opacity=80);\n" +
            "	    }\n" +
            "	    .white_content {\n" +
            "	        position: fixed;\n" +
            "	        top: 10%;\n" +
            "	        left: 10%;\n" +
            "	        width: 80%;\n" +
            "	        height: 80%;\n" +
            "	        padding: 16px;\n" +
            "	        border: 16px solid orange;\n" +
            "	        background-color: white;\n" +
            "	        z-index:1002;\n" +
            "	        overflow: auto;\n" +
            "	    }\n"
    );
    document.head.appendChild(style);
    document.head.popupStyle = style;
}
function createPopupScreen() {
    if(!document.head.popupStyle) addPopupStyle();
    var div = $("<div style='display: none'></div>")[0];
    div.overlay = $("<div class='black_overlay'></div>")[0];
    div.content = $("<div class='white_content'></div>")[0];
    div.appendChild(div.overlay);
    div.appendChild(div.content);
    
    div.show = function() {this.style.display="";}
    div.hide = function() {this.style.display="none";}
    div.overlay.onclick=function() {this.parentNode.hide();};

    document.body.appendChild(div);
    return div;
}

var gErrorView = null;
function showError(data) {
    if(!gErrorView) {
        gErrorView = createPopupScreen();
        gErrorView.iframe = $('<iframe id="stderr" width="100%" height="100%"></iframe><br/>')[0];
        gErrorView.content.appendChild(gErrorView.iframe);
    }
    if(data.responseText) gErrorView.iframe.src = "data:text/html;charset=utf-8," + escape(data.responseText);
    else gErrorView.iframe.src = "data:text/plain;charset=utf-8," + JSON.stringify(data);
    gErrorView.show();
}
function showErrorHTML(html) {
    if(html.responseText) showError(html);
    else showError({responseText:html});
}

var gDebugText = null;
function showDebugTextArea() {
    if(!gDebugText) {
        gDebugText = $("<textarea rows=20 style='width:100%'></textarea>")[0];
        document.body.appendChild(gDebugText);
    }
    return gDebugText;
}
function showDebugHTML() {
    if(gDebugText) {showErrorHTML(gDebugText.value);}
}
function debug(obj) {
    var debug = $("#debug")[0];
    if(debug===NULL) {
        debug = $("<textarea id=\"debug\" style=\"width:100%;height:80vh;\"></textarea>")[0];
        $("body").prepend(debug);
    }
    debug.value+=((isObject(obj)||isArray(obj))?JSON.stringify(obj,null,4):obj)+"\n";
    debug.scrollTop = debug.scrollHeight;
}
