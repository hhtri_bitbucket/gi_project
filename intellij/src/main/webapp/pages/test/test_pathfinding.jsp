<%@page import="com.dp.db.Func"
%><%@page import="com.ctmlab.game.manager.StaticDataMng"
%><%@page import="com.ctmlab.game.data.staticdata.Zone"
%><%
    String id = request.getParameter("id");
    short zoneID = Short.parseShort(id == null ? "0" : id);
    Zone z = StaticDataMng.i().worldMap.getZone(zoneID);

    String start = request.getParameter("start");
    long startID = Long.parseLong(start == null ? "0" : start);

    String end = request.getParameter("end");
    long endID = Long.parseLong(start == null ? "0" : end);

    String s = z.getMatrixEx(startID, endID);
%>
<html lang="en">
<head>
    <title>Zone Test</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script>
        function btnOnClick(data){
            console.log(data);
        }
    </script>
</head>
<body>
<div id="content" style="overflow:scroll; height:100%; width: 4050px;">
    <%=s%>
</div>
</body>
</html>