<!DOCTYPE HTML>
<html>
    <head>
        <title>JSON Map Editor</title>
        <link href="../resources/editor/jsoneditor.css" rel="stylesheet" type="text/css">
        <script src="../resources/editor/jsoneditor.js"></script>
        <script src="../resources/jquery3_4_1.js"></script>
    </head>
    <body>
        <form id="myForm">
            <input type="hidden" id="json_map" value="" />                                
            <p><button type="button" id="getJSON" style="display:none;">Save JSON</button></p>
        </form>
        <div id="jsoneditor" style="width: 600px; height: 800px;"></div>
        <script>
            // create the editor
            var container = document.getElementById('jsoneditor');
            var options = {
                mode: 'tree',
                modes: ['code', 'tree'],
                onError: function (err) {
                    console.error(err);
                },
                onChange: function () {
//                    console.error(Date.now());
                    document.getElementById('getJSON').style.display = '';
                },
                indentation: 4,
                escapeUnicode: true
            };
            var editor = new JSONEditor(container, options);
            var json = #json;
            editor.set(json);

            // get json
            document.getElementById('getJSON').onclick = function () {             
                $.post(
                    "/goldinc/map/map_editor", 
                    {
                        action: "save_map",
                        json_map: JSON.stringify(editor.get())
                    },
                    function(data, status){
                        if("error" === status){
                            alert(status);
                        } else{
                            document.getElementById('getJSON').style.display = 'none';
                        }
                    }
                );
                
            };
        </script>
    </body>
</html>
