<%-- 
    Document   : change_password
    Created on : Sep 19, 2019, 10:30:45 AM
    Author     : hhtri
--%>
<%@page import="com.ctmlab.game.network.manager.WSGameDefine"%><%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Bootstrap Example</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
        <script>
            function showMsg(val) {
                alert(val);
                document.getElementById("myForm").style.display = "none";
            }
        </script>
    </head>
    <body>
        <div class="container">
            <form id="myForm" target="myframe" method="post" action="<%=(WSGameDefine.HOST + "/goldinc/service/change_passwd")%>">
                <input type="hidden" id="key" name="key" value="<%=request.getParameter("email")%>" />
                <div class="form-group">
                    <h2 class="col-sm-6 control-label">Change password form</h2>
                </div>
                <div class="form-group">                  
                    <div class="col-sm-4">
                        <input type="password" class="form-control" id="new_passwd" placeholder="Enter new password" name="new_passwd">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-4">
                        <input type="password" class="form-control" id="new_passwd" placeholder="Enter confirm new password" name="cfr_passwd">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-3 control-label">
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>                    
                </div>                
            </form>
            <iframe id="myframe" name="myframe" style="display: none;"></iframe>
        </div>
</body>
</html>

