<%
    if (session.getAttribute("user") == null) {
        response.sendRedirect("ad_login.jsp");
        return;
    }
%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <jsp:include page="_header_scripts.jsp" />
        <title>User Manager</title>
        <script type="text/javascript">
            var listUserID = [];
            var oTableInfo = null;
            var userSelected = null;

            function loadData(list) {
                list = JSON.parse(list);
                listUserID = [];
                for (i = 0; i < list.length; i++) {
                    listUserID[i] = {userID: list[i]};
                }
                if (oTableInfo === null || oTableInfo === undefined) {
                    oTableInfo = $('#list').DataTable({
                        "dom": 'rt<"dt-bottom"l><"clearfix">p',
                        "searching": true,
                        "language": {
                            "info": "&nbsp;&nbsp;&nbsp;<strong>_START_</strong> to <strong>_END_</strong> of <strong>_TOTAL_</strong>&nbsp;&nbsp;",
                            "infoEmpty": "0 to 0 of 0",
                            "lengthMenu": "_MENU_",
                            "infoFiltered": ""
                        },
                        "pagingType": "listbox",
                        iDisplayLength: 50,
                        resetPaging: true,
                        aaData: listUserID,
                        aoColumns: [{"mDataProp": "userID"}, ]
                    });
                    // Apply the search
                    oTableInfo.columns().every(function () {
                        var that = this;
                        $('input', this.footer()).on('keyup change', function () {
                            if (that.search() !== this.value) {
                                that.search(this.value).draw();
                            }
                        });
                    });
                } else {
                    table = $("#list").dataTable();
                    oSettings = table.fnSettings();
                    table.fnClearTable(this);

                    for (var i = 0; i < listUserID.length; i++) {
                        table.oApi._fnAddData(oSettings, listUserID[i]);
                    }
                    oSettings.aiDisplay = oSettings.aiDisplayMaster.slice();
                    table.fnDraw();
                }
            }
            
            function getProfile(userID) {
                userSelected = userID;
                $("#content_detail").show();
                var errorFunc = function (res) {};
                var successFunc = function (res) {
                    $("#logProfile").val(res)
                };
                doPost(__url_api__, {action: "info_user", id: userID}, successFunc, errorFunc)
            }

            function saveProfile() {
                var pass = $("#txtPass").val();
                if(pass===null || pass==="" || pass===undefined){
                    alert('Please input your password to save profile');
                    return;
                } else{
                    var data = JSON.parse($("#logProfile").val());
                    if(userSelected!==data.userID){
                        alert('Do not change userID in profile, please.');
                        return;
                    }
                }
                $("#content_detail").hide();
                var json={
                    action:"upd_resource",
                    json:$("#logProfile").val(),
                    pass:$("#txtPass").val()
                };
                var errorFunc = function (res) {
                     alert(res);
                };
                var successFunc = function (res) {
                    if(res==""){
                        $("#content_detail").hide();
                        userSelected = null;
                        oTableInfo.$('tr.selected').removeClass('selected');
                        cancelEditInfo();
                        alert("Save profile is successful.");
                    } else{
                        alert(res);
                    }
                };
                doPost(__url_api__, json, successFunc, errorFunc)
            }

            function initPage() {
                $('#list tfoot td').each(function () {
                    var title = $(this).text();
                    if (title === "HIDE_THIS") {
                        $(this).html('');
                    }
                });
                $('#list tbody').on('click', 'tr', function () {
                    if (!$(this).hasClass('selected')) {
                        oTableInfo.$('tr.selected').removeClass('selected');
                        $(this).addClass('selected');

                        var td = $(this).children()[0];
                        var key = $(td).html();
                        getProfile(key);
                    }
                });
                setInterval(function () {
                    $.get(_self());
                }, 900000);

                $("#content_detail").hide();
                var errorFunc = function (res) {};
                var successFunc = function (res) {
                    loadData(res)
                };
                doPost(__url_api__, {action: "list_user"}, successFunc, errorFunc);               
            }

            function cancelEditInfo() {
                $("#logProfile").val('');
                $("#txtPass").val('');
                $("#content_detail").hide();
            }

            function submitEditInfo() {
                saveProfile(); 
            }

            $(document).ready(initPage);
        </script>
    </head>
    <body>
        <jsp:include page="_header.jsp" />
        <br/>
        <section>
            <div class="row">
                <div class="form-horizontal">
                    <div class="col-md-2">
                        <table id="list" class="table table-striped table-bordered" cellspacing="0" width="100%">
                            <tfoot>
                                <tr>
                                    <td><input type="text" style="width:150px;" placeholder="Search..." /></td>
                                </tr>
                            </tfoot>
                            <thead>
                                <tr>
                                    <th>USERID</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                    <div id="content_detail" class="col-md-10">
                        <form>
                            <div class='form-group form-group-sm'>
                                <label class='col-sm-1 control-label' for='logProfile'>Profile</label>
                                <div class='col-sm-6'>
                                    <textarea class='form-control' rows="40" type='text' id='logProfile' value=''></textarea>
                                </div>
                            </div>
                            <div class='form-group form-group-sm'>
                                <label class='col-sm-1 control-label' for='txtPass'>Profile</label>
                                <div class='col-sm-6'>
                                    <input class='form-control' type='password' id='txtPass' value='' placeholder="Input password to save profile" />
                                </div>
                            </div>
                            <div class='form-group form-group-sm'>
                                <label class='col-sm-1 control-label' for=''></label>
                                <div class='col-sm-6'>
                                    <button type='button' class='btn btn-default' onclick="cancelEditInfo()">Cancel</button>
                                    <button type='button' class='btn btn-default' onclick='submitEditInfo()'>Submit</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>
    </body>
</html>
