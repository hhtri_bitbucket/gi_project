<link href="../resources/libs/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="../resources/libs/jquery.datatables/css/dataTables.bootstrap.css" rel="stylesheet" type="text/css"/>
<link href="../resources/libs/font-awesome/css/font-awesome.min.css" rel="stylesheet" />
<link href="../resources/css/styles.css" rel="stylesheet" />
<style type="text/css">
    thead, tfoot {
        display: table-header-group;
    }
    table.dataTable thead .sorting:after, 
    table.dataTable thead .sorting_asc:after, 
    table.dataTable thead .sorting_desc:after, 
    table.dataTable thead .sorting_asc_disabled:after, 
    table.dataTable thead .sorting_desc_disabled:after {
        bottom: 0;
    }
</style>
<script src="../resources/js/jquery3_4_1.js"></script>
<script src="../resources/js/ctmlab.js"></script>
<script src="../resources/js/bootstrap.min.js"></script>
<script src="../resources/libs/jquery.datatables/js/jquery.dataTables.min.js" type="text/javascript"></script>
<script src="../resources/libs/jquery.datatables/js/dataTables.bootstrap.min.js" type="text/javascript"></script>
<script src="../resources/js/select.js" type="text/javascript"></script>
<script src="../resources/js/manager.js" type="text/javascript"></script>