<%@page contentType="text/html" pageEncoding="UTF-8"%><%
    if (session.getAttribute("user") == null) {
        response.sendRedirect("ad_login.jsp");
        return;
    }
%><!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <jsp:include page="_header_scripts.jsp" />
        <title>Cash Convert Information</title>
        <script type="text/javascript">
            var lstCashConvert = [];
            var oTableInfo = null;
            var oTableEditingInfo = null;
            var userSelected = null;

            function loadDataEditing(list) {
                $('#divListShowing').hide();
                $('#divListEditing').hide();
                $('#btnEdit').hide();
                $('#btnCancelEdit').hide();
                $('#btnSave').hide();

                $('#divListEditing').show();
                $('#btnCancelEdit').show();
                $('#btnSave').show();

                if (oTableEditingInfo === null || oTableEditingInfo === undefined) {
                    oTableEditingInfo = $('#listEditing').DataTable({
                        "dom": 'rt<"dt-bottom"l><"clearfix">p',
                        "searching": true,
                        "language": {
                            "info": "&nbsp;&nbsp;&nbsp;<strong>_START_</strong> to <strong>_END_</strong> of <strong>_TOTAL_</strong>&nbsp;&nbsp;",
                            "infoEmpty": "0 to 0 of 0",
                            "lengthMenu": "_MENU_",
                            "infoFiltered": ""
                        },
                        "pagingType": "listbox",
                        iDisplayLength: 50,
                        resetPaging: true,
                        aaData: lstCashConvert,
                        aoColumns: [
                            {"mDataProp": "type", "render": function (data, type, row) {
                                    var html = "<input type='text' id='" + row.type + "_type' value='" + data + "' readonly />";
                                    return html;
                                }},
                            {"mDataProp": "name", "render": function (data, type, row) {
                                    var html = "<input type='text' id='" + row.type + "_name' value='" + data + "' readonly />";
                                    return html;
                                }},
                            {"mDataProp": "selling", "render": function (data, type, row) {
                                    var html = "<input type='text' id='" + row.type + "_selling' value='" + data + "' />";
                                    return html;
                                }},
                            {"mDataProp": "buying", "render": function (data, type, row) {
                                    var html = "<input type='text' id='" + row.type + "_buying' value='" + data + "' />";
                                    return html;
                                }}
                        ]
                    });
                } else {
                    table = $("#listEditing").dataTable();
                    oSettings = table.fnSettings();
                    table.fnClearTable(this);

                    for (var i = 0; i < lstCashConvert.length; i++) {
                        table.oApi._fnAddData(oSettings, lstCashConvert[i]);
                    }
                    oSettings.aiDisplay = oSettings.aiDisplayMaster.slice();
                    table.fnDraw();
                }
            }

            function loadData(list) {
                $('#divListShowing').hide();
                $('#divListEditing').hide();
                $('#btnEdit').hide();
                $('#btnCancelEdit').hide();
                $('#btnSave').hide();

                $('#divListShowing').show();
                $('#btnEdit').show();

                lstCashConvert = JSON.parse(list);
                if (oTableInfo === null || oTableInfo === undefined) {
                    oTableInfo = $('#list').DataTable({
                        "dom": 'rt<"dt-bottom"l><"clearfix">p',
                        "searching": true,
                        "language": {
                            "info": "&nbsp;&nbsp;&nbsp;<strong>_START_</strong> to <strong>_END_</strong> of <strong>_TOTAL_</strong>&nbsp;&nbsp;",
                            "infoEmpty": "0 to 0 of 0",
                            "lengthMenu": "_MENU_",
                            "infoFiltered": ""
                        },
                        "pagingType": "listbox",
                        iDisplayLength: 50,
                        resetPaging: true,
                        aaData: lstCashConvert,
                        aoColumns: [
                            {"mDataProp": "type"},
                            {"mDataProp": "name"},
                            {"mDataProp": "selling"},
                            {"mDataProp": "buying"}
                        ]
                    });
                    // Apply the search
                    oTableInfo.columns().every(function () {
                        var that = this;
                        $('input', this.footer()).on('keyup change', function () {
                            if (that.search() !== this.value) {
                                that.search(this.value).draw();
                            }
                        });
                    });
                } else {
                    table = $("#list").dataTable();
                    oSettings = table.fnSettings();
                    table.fnClearTable(this);

                    for (var i = 0; i < lstCashConvert.length; i++) {
                        table.oApi._fnAddData(oSettings, lstCashConvert[i]);
                    }
                    oSettings.aiDisplay = oSettings.aiDisplayMaster.slice();
                    table.fnDraw();
                }
            }

            function saveCashInfo() {
                var arr = [];
                for (var i = 0; i < lstCashConvert.length; i++) {
                    var temp = lstCashConvert[i];
                    arr.push({
                        type: temp.type,
                        name: temp.name,
                        selling: $('#' + temp.type + "_selling").val(),
                        buying: $('#' + temp.type + "_buying").val(),
                    });
                }
                var errorFunc = function (res) {};
                var successFunc = function (res) {
                    loadData(res);
                };
                doPost(__url_api__, {
                    action: "cash_converter_info_save",
                    data: JSON.stringify(arr)
                }, successFunc, errorFunc);
            }

            function cancelEdittingCashInfo() {
                $('#divListShowing').hide();
                $('#divListEditing').hide();
                $('#btnEdit').hide();
                $('#btnCancelEdit').hide();
                $('#btnSave').hide();

                $('#divListShowing').show();
                $('#btnEdit').show();
            }

            function editInfo() {
                loadDataEditing();
            }

            function initPage() {
                $('#list tfoot td').each(function () {
                    var title = $(this).text();
                    if (title === "HIDE_THIS") {
                        $(this).html('');
                    }
                });
                $('#list tbody').on('click', 'tr', function () {
                    if (!$(this).hasClass('selected')) {
                        oTableInfo.$('tr.selected').removeClass('selected');
                        $(this).addClass('selected');
                    }
                });
                setInterval(function () {
                    $.get(_self());
                }, 900000);

                var errorFunc = function (res) {};
                var successFunc = function (res) {
                    loadData(res)
                };
                doPost(__url_api__, {action: "cash_converter_json_info"}, successFunc, errorFunc);
            }

            $(document).ready(initPage);
        </script>
    </head>
    <body>
        <jsp:include page="_header.jsp" />
        <br/>
        <div>
            <input type="button" id="btnEdit" name="btnEdit" value="Edit" onclick="editInfo()" />
            <input type="button" id="btnCancelEdit" name="btnCancelEdit" value="Cancel Edit" onclick="cancelEdittingCashInfo()" />
            <input type="button" id="btnSave" name="btnSave" value="Save" onclick="saveCashInfo()" />
        </div> 
        <section>
            <div class="row">
                <div class="form-horizontal">

                    <div id="divListShowing" class="col-md-4" style="display: none;">
                        <table id="list" class="table table-striped table-bordered" cellspacing="0" width="100%">
                            <tfoot>
                                <tr>
                                    <td><input type="text" style="width:180px;" placeholder="Search..." /></td>
                                    <td><input type="text" style="width:180px;" placeholder="Search..." /></td>
                                    <td><input type="text" style="width:180px;" placeholder="Search..." /></td>
                                    <td><input type="text" style="width:180px;" placeholder="Search..." /></td>
                                </tr>
                            </tfoot>
                            <thead>
                                <tr>
                                    <th>ORDER</th>
                                    <th>NAME</th>
                                    <th>SELLING</th>
                                    <th>BUYING</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>                    
                    <div id="divListEditing" class="col-md-4">
                        <table id="listEditing" class="table table-striped table-bordered" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th>ORDER</th>
                                    <th>NAME</th>
                                    <th>SELLING</th>
                                    <th>BUYING</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>                    
                </div>
            </div>
        </section>
    </body>
</html>
