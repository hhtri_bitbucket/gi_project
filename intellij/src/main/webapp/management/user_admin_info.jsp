<%@page contentType="text/html" pageEncoding="UTF-8"%><%
    if (session.getAttribute("user") == null) {
        response.sendRedirect("ad_login.jsp");
        return;
    }
%>
<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Admin Information</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <jsp:include page="_header_scripts.jsp" />
        <script>
            function saveProfile() {
                var pass = $("#txtPass").val();
                if(pass===null || pass==="" || pass===undefined){
                    alert('Please input your password to save profile');
                    return;
                } else{
                    var data = JSON.parse($("#logProfile").val());
                    if(userSelected!==data.userID){
                        alert('Do not change userID in profile, please.');
                        return;
                    }
                }
                $("#content_detail").hide();
                var json={
                    action:"upd_resource",
                    json:$("#logProfile").val(),
                    pass:$("#txtPass").val()
                };
                var errorFunc = function (res) {
                     alert(res);
                };
                var successFunc = function (res) {
                    if(res==""){
                        $("#content_detail").hide();
                        userSelected = null;
                        oTableInfo.$('tr.selected').removeClass('selected');
                        cancelEditInfo();
                        alert("Save profile is successful.");
                    } else{
                        alert(res);
                    }
                };
                doPost(__url_api__, json, successFunc, errorFunc)
            }
            function loadData(json) {
                $("#content_detail").show();
                json = JSON.parse(json);
                $("#txtUsername").val(json.username);
                $("#txtPasswd").val(json.password);
                $("#txtCreated").val(new Date(json.created));
                $("#txtLastLogin").val(json.lastlogin>0?(new Date(json.lastlogin)):"");
                $("#txtCreatedby").val(json.createdby);
            }
            
            function signOut(){
                doPost(__url_api__, {action: "logout"}, function(res){
                    window.location.href = "ad_login.jsp";
                }, null);
            }
            
            function submitEditInfo(){
                 doPost(__url_api__, {
                     action: "upd_user_admin_passwd",
                     passwd: $("#txtPasswd").val(),
                     passwdOld: $("#txtPass").val(),
                 }, function(res){
                    if(res==="true"){
                        alert("Updating is successful.");
                    } else{
                        alert("Updating is failed.");
                    }
                }, null);
            }
            
            function initPage() {                
                setInterval(function () {
                    $.get(_self());
                }, 900000);

                $("#content_detail").hide();
                var errorFunc = function (res) {};
                var successFunc = function (res) {
                    loadData(res)
                };
                doPost(__url_api__, {action: "info_user_admin"}, successFunc, errorFunc);               
            }
            $(document).ready(initPage);
        </script>
    </head>
    <body>
        <jsp:include page="_header.jsp" />
        <section>
            <div class="row">
                <div class="form-horizontal">                    
                    <div id="content_detail" class="col-md-10">
                        <form>
                            <div class='form-group form-group-sm'>
                                <label class='col-sm-1 control-label' for='txtUsername'>Username</label>
                                <div class='col-sm-6'>
                                    <input class='form-control' type='text' id='txtUsername' value='' readonly/>
                                </div>
                            </div>
                            <div class='form-group form-group-sm'>
                                <label class='col-sm-1 control-label' for='txtPasswd'>Password</label>
                                <div class='col-sm-6'>
                                    <input class='form-control' type='text' id='txtPasswd' value='' />
                                </div>
                            </div>
                            <div class='form-group form-group-sm'>
                                <label class='col-sm-1 control-label' for='txtCreated'>Created</label>
                                <div class='col-sm-6'>
                                    <input class='form-control' type='text' id='txtCreated' value='' readonly/>
                                </div>
                            </div>
                            <div class='form-group form-group-sm'>
                                <label class='col-sm-1 control-label' for='txtLastLogin'>LastLogin</label>
                                <div class='col-sm-6'>
                                    <input class='form-control' type='text' id='txtLastLogin' value='' readonly/>
                                </div>
                            </div>
                            <div class='form-group form-group-sm'>
                                <label class='col-sm-1 control-label' for='txtCreatedby'>Created By</label>
                                <div class='col-sm-6'>
                                    <input class='form-control' type='text' id='txtCreatedby' value='' readonly/>
                                </div>
                            </div>
                            <div class='form-group form-group-sm'>
                                <label class='col-sm-1 control-label' for='txtPass'>Password Old</label>
                                <div class='col-sm-6'><input class='form-control' type='password' id='txtPass' value='' placeholder="Input your old password to confirm." /></div>
                            </div>
                            <div class='form-group form-group-sm'>
                                <label class='col-sm-1 control-label' for=''></label>
                                <div class='col-sm-6'>
                                    <button type='button' class='btn btn-default' onclick="signOut()">Log out</button>
                                    <button type='button' class='btn btn-default' onclick='submitEditInfo()'>Submit</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>
    </body>
</html>