<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Login Admin Page</title>
        <link href="../resources/css/login.css" rel="stylesheet" type="text/css"/>
        <link href="../resources/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <script src="../resources/js/jquery3_4_1.js" type="text/javascript"></script>
        <script src="../resources/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="../resources/js/manager.js" type="text/javascript"></script>
        
        <script type="text/javascript">
            var __url_api__ = "ad_api.jsp";
            
            function onSuccessEvent(type, result) {
                $('#loading').hide();
                window.location.href = "user_mng.jsp";
            }
            function onErrorEvent() {
                console.log("[admin_index.jsp]");
            }
            function toForgotForm(){
                $('#divLogin').hide();
                $('#loading').hide();
                $('#formForgotPwd').show();
            }
            function formForgotClick(){
                $('#formContent').hide();
                $('#loading').hide();
                $('#loading').show();
                var json = {
                    action:"forgot_pwd", 
                    email: $('#idForgotPwd').val()
                };
                var errorFunc = function(res){onErrorEvent("login", res);};
                var successFunc = function(res){onSuccessEvent("login", res);};
                doPost(__url_api__, json, successFunc, errorFunc);
            }
            function loginOnClick(){
                $('#formContent').hide();
                $('#loading').hide();
                $('#loading').show();
                var json = {
                    action:"login", 
                    username: $('#login').val(), 
                    password:$('#password').val()
                };
                var errorFunc = function(res){onErrorEvent("login", res);};
                var successFunc = function(res){onSuccessEvent("login", res);};
                doPost(__url_api__, json, successFunc, errorFunc);
            }            
        </script>
    </head>
    <body>
        <div class="wrapper fadeInDown">
            <div id="formContent">
                <!-- Tabs Titles -->
                <div id="divLogin">
                    <!-- Icon -->
                    <div><h2>Login</h2></div>
                    <!-- Login Form -->
                    <form id="formLogin" autocomplete="off">
                        <input type="text" id="login" name="login" placeholder="Input username" autocomplete="off">
                        <input type="password" id="password" name="password" placeholder="Input password" autocomplete="off">
                        <input type="button" onclick="loginOnClick()" value="Log In">
                    </form>
                    <!-- Remind Passowrd -->
                    <!--<div id="formFooter"><a class="underlineHover" onclick="toForgotForm()">Forgot Password?</a></div>-->
                </div>                                
                <!-- Forgot Form -->
<!--                <form id="formForgotPwd" style="display: none;" autocomplete="off">
                    <h2>Forgot password</h2>
                    <input type="text" id="idForgotPwd" name="idForgotPwd" placeholder="Your email" autocomplete="off">
                    <input type="button" onclick="formForgotClick()" value="Submit">
                </form>                    -->
            </div>
            <div id="loading" style="display: none;"><img id="loading_icon" src="../resources/js/spinningred.gif" width="50" height="50"/></div>
        </div>
        <input type="hidden" id="" value="" />
    </body>
</html>
