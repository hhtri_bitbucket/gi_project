<%@page import="com.ctmlab.game.goldincenum.ENUM_UNIT_TYPE"
%><%@page import="com.ctmlab.game.goldincenum.ENUM_DECOR_MAP_TYPE"
%><%@page import="com.google.gson.JsonObject"
%><%@page import="com.google.gson.JsonArray"
%><%@page import="com.ctmlab.game.manager.ServiceMng"
%><%@page import="com.ctmlab.game.data.staticdata.Zone"
%><%@page import="com.ctmlab.game.manager.StaticDataMng"
%><%@page import="com.dp.db.Func"
%><%@page import="com.ctmlab.game.manager.GoldIncAdminMng"
%><%!
    enum ACTION{
        login,
        logout,
        list_user,
        list_user_admin,
        list_gameobjectinfo,
        add_member_admin,
        info_user,
        info_user_admin,
        upd_user_admin_passwd,
        upd_resource,
        regen_static_map,
        cash_converter_json_info,
        cash_converter_info_save, 
        get_gameobject_info,
        save_gameobject_info,
    }
    public class LogAdmin{
        public String username = "";
        public String action = "";
        public String data = "";
        public String result = "";
        public long time = System.currentTimeMillis();
        public LogAdmin(){
        }
    }
%><%
    String action = request.getParameter("action");
    if(action!=null){
        LogAdmin la = new LogAdmin();
        la.action = action;
        response.setContentType(Func.MINE_TEXT);
        switch(ACTION.valueOf(action)){
            case logout:{
                String username = String.valueOf(session.getAttribute("username"));
                session.setAttribute("user", null);
                session.setAttribute("username", null);
                la.username = username;
                la.data = "";
                la.result = String.valueOf(true);
                break;
            }
            case login:{
                String username = request.getParameter("username");
                String password = request.getParameter("password");
                boolean result = GoldIncAdminMng.i().loginAdmin(username, password);
                if(result){
                    session.setAttribute("user", true);
                    session.setAttribute("username", username);
                    out.print("");
                } else{
                    out.print("Username or password is wrong!");
                }
                la.username = "";
                la.data = String.format("[username;password][%s;%s]", username, password);
                la.result = String.valueOf(result);
                break;
            }            
            case list_user:{
                String[] lstUser = GoldIncAdminMng.i().getListUser();
                String json = Func.json(lstUser);
                out.print(json);
                la.username = String.valueOf(session.getAttribute("username"));
                la.data = "";
                la.result = "";
                break;
            } 
            case list_user_admin:{
                String json = GoldIncAdminMng.i().getListUserAdmin(String.valueOf(session.getAttribute("username")));
                out.print(json);
                la.username = String.valueOf(session.getAttribute("username"));
                la.data = "";
                la.result = "";
                break;
            }
            case upd_user_admin_passwd:{
                String passwd = request.getParameter("passwd");
                String passwdOld = request.getParameter("passwdOld");
                String username = String.valueOf(session.getAttribute("username"));
                boolean flag = GoldIncAdminMng.i().updUserAdminInfo(username, passwd, passwdOld);
                out.print(GoldIncAdminMng.i().updUserAdminInfo(username, passwd, passwdOld));
                la.username = String.valueOf(session.getAttribute("username"));
                la.data = String.format("[newpass;oldpass][%s;%s]", passwd, passwdOld);
                la.result = String.valueOf(flag);
                break;
            } 
            case info_user:{
                String userID = request.getParameter("id");
                String json = Func.pson(GoldIncAdminMng.i().getUserInfo(userID));
                out.print(json);
                la.username = String.valueOf(session.getAttribute("username"));
                la.data = String.format("[userid][%s]", userID);
                la.result = String.valueOf(json);
                break;
            }
            case info_user_admin:{
                String json = GoldIncAdminMng.i().getUserAdmin(String.valueOf(session.getAttribute("username")));
                out.print(json);
                la.username = String.valueOf(session.getAttribute("username"));
                la.data = "";
                la.result = json;
                break;
            }
            case upd_resource:{
                String json = request.getParameter("json");
                String pass = request.getParameter("pass");
                String rs = GoldIncAdminMng.i().updUserInfo(json, String.valueOf(session.getAttribute("username")), pass);
                out.print(rs);
                
                la.username = String.valueOf(session.getAttribute("username"));
                la.data = String.format("[json;pass][%s;%s]", json, pass);
                la.result = rs;
                break;
            }
            case regen_static_map:{
                StaticDataMng.i().re_genWorldMap();
                Zone z = StaticDataMng.i().worldMap.getZone(Short.parseShort(request.getParameter("id")));
                String s = z.getMatrix();
                out.print(s);
                
                la.username = String.valueOf(session.getAttribute("username"));
                la.data = String.format("[zondid][%d]", Short.parseShort(request.getParameter("id")));
                la.result = s;
                break;
            }
            case cash_converter_json_info:{
                String json = Func.json(StaticDataMng.i().cashConvert.getJsonObject());
                out.print(json);
                la.username = String.valueOf(session.getAttribute("username"));
                la.data = "";
                la.result = json;
                break;
            }
            case cash_converter_info_save:{
                String json = request.getParameter("data");
                String msg = StaticDataMng.i().cashConvert.saveData(Func.gson.fromJson(json, JsonArray.class));
                out.println(msg.isEmpty()?json:msg);
                
                la.username = String.valueOf(session.getAttribute("username"));
                la.data = json;
                la.result = msg;
                break;
            }             
            case list_gameobjectinfo:{
                JsonObject obj = new JsonObject();
                obj.add("decor", ENUM_DECOR_MAP_TYPE.getListNames());
                obj.add("unit", ENUM_UNIT_TYPE.getListNames());
                String json = Func.json(obj);
                out.print(json);
                la.username = String.valueOf(session.getAttribute("username"));
                la.data = "";
                la.result = "";
                break;
            } 
            case get_gameobject_info:{
                String repoType = request.getParameter("repoType");
                String objType = request.getParameter("objType");
                String json = "";
                if("unit".equals(repoType)){
                    json = StaticDataMng.i().getRepoGameObjectInfo().getObjectUnit(objType.toLowerCase());
                } else if("decor".equals(repoType)){
                    json = StaticDataMng.i().getRepoGameObjectInfo().getObjectDecor(objType.toLowerCase());
                }
                out.print(json);
                la.username = String.valueOf(session.getAttribute("username"));
                la.data = String.format("[repoType:objType]-[%s:%s]", repoType, objType.toLowerCase());
                la.result = "";
                break;
            } 
            case save_gameobject_info:{
                String repoType = request.getParameter("repoType");
                String objType = request.getParameter("objType");
                String data = request.getParameter("data");
                String rs = "";
                if("unit".equals(repoType)){
                    rs = StaticDataMng.i().getRepoGameObjectInfo().setObjectUnit(objType, data);
                } else if("decor".equals(repoType)){
                    rs = StaticDataMng.i().getRepoGameObjectInfo().setObjectDecor(objType, data);
                }
                out.print(rs);
                la.username = String.valueOf(session.getAttribute("username"));
                la.data = String.format("[repoType:objType]-[%s:%s]", repoType, objType);
                la.result = "";
                break;
            } 
        }
        GoldIncAdminMng.i().writeLog(Func.json(la));
    }
%>