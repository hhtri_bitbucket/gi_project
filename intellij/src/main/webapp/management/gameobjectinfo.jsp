<%
    if (session.getAttribute("user") == null) {
        response.sendRedirect("ad_login.jsp");
        return;
    }
%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <jsp:include page="_header_scripts.jsp" />
        <title>Game Objects Information</title>
        <script src="../resources/js/template.js" type="text/javascript"></script>        
        <script type="text/javascript">
            var listData = {};
            var objContent = {};
            var oTableInfo = null;
            var objType = null;
            var repoType = null;

            function loadData(list) {
                list = JSON.parse(list);
                var keys = Object.keys(list);
                for (var i = 0; i < keys.length; i++) {
                    var key = keys[i];
                    var temp = [];
                    var ls = list[key];
                    for (var j = 0; j < ls.length; j++) {
                        temp[j] = {id: ls[j]};
                    }
                    listData[key] = temp;
                }
                repoType = "unit";
                if (oTableInfo === null || oTableInfo === undefined) {
                    oTableInfo = $('#list').DataTable({
                        "dom": 'rt<"dt-bottom"l><"clearfix">p',
                        "searching": false,
                        "language": {
                            "info": "&nbsp;&nbsp;&nbsp;<strong>_START_</strong> to <strong>_END_</strong> of <strong>_TOTAL_</strong>&nbsp;&nbsp;",
                            "infoEmpty": "0 to 0 of 0",
                            "lengthMenu": "_MENU_",
                            "infoFiltered": ""
                        },
                        "pagingType": "listbox",
                        iDisplayLength: 10,
                        resetPaging: true,
                        aaData: listData["unit"],
                        aoColumns: [{"mDataProp": "id"}, ]
                    });
                    // Apply the search
                    oTableInfo.columns().every(function () {
                        var that = this;
                        $('input', this.footer()).on('keyup change', function () {
                            if (that.search() !== this.value) {
                                that.search(this.value).draw();
                            }
                        });
                    });
                } else {
                    var lst = listData["unit"];
                    table = $("#list").dataTable();
                    oSettings = table.fnSettings();
                    table.fnClearTable(this);

                    for (var i = 0; i < lst.length; i++) {
                        table.oApi._fnAddData(oSettings, lst[i]);
                    }
                    oSettings.aiDisplay = oSettings.aiDisplayMaster.slice();
                    table.fnDraw();
                }
            }
            
            function renderInfo(info) {
                var htmlInfo = "<table id='tbInfo' class='table table-striped table-bordered' cellspacing='0' width='100%'>"
                htmlInfo += "    <thead>";
                htmlInfo += "        <tr>";
                htmlInfo += "            <th style='width:50%;'>NAME</th>";
                htmlInfo += "            <th style='width:25%;'>VALUE</th>";
                htmlInfo += "            <th style='width:25%;'></th>";
                htmlInfo += "        </tr>";
                htmlInfo += "    </thead>";
                htmlInfo += "    <tbody>";
                htmlInfo += "        #ct#";
                htmlInfo += "    </tbody>";
                htmlInfo += "</table>";

                var itemTemp = "<tr id='#idx'>";
                itemTemp += "    <th>#n</th>";
                itemTemp += "    <th>#v</th>";
                itemTemp += "    <th><button type='button' class='btn btn-default' onclick='rmRowTable(\"#id\")'>Remove</button></th>";
                itemTemp += "</tr>";

                var objInfo = info;
                var html = "";
                var keys = Object.keys(objInfo);
                for (var i = 0; i < keys.length; i++) {
                    var name = keys[i];
                    var value = objInfo[name];
                    var row = itemTemp;
                    row = row.replace("#idx", i);
                    row = row.replace("#n", name);
                    row = row.replace("#v", "<input type='text' value='"+value+"'/>");
                    row = row.replace("#id", "tbInfo_"+i);
                    html += row;
                }
                htmlInfo = htmlInfo.replace("#ct#", html);
                $('#divInfo').html(htmlInfo);
            }

            function renderUpgrade(upgradeRequire) {
                var htmlInfo = "<table id='tbUpgrade' class='table table-striped table-bordered' cellspacing='0' width='100%'>"
                htmlInfo += "    <thead>";
                htmlInfo += "        <tr>";                
                htmlInfo += "            <th>TYPE</th>";
                htmlInfo += "            <th>NAME</th>";
                htmlInfo += "            <th>VALUE</th>";
                htmlInfo += "        </tr>";
                htmlInfo += "    </thead>";
                htmlInfo += "    <tbody>";
                htmlInfo += "        #ct#";
                htmlInfo += "    </tbody>";
                htmlInfo += "</table>";

                var itemTemp = "<tr>";                
                itemTemp += "    <th>#t</th>";
                itemTemp += "    <th>#n</th>";
                itemTemp += "    <th>#v</th>";
                itemTemp += "</tr>";

                var html = "";
                var keyType = Object.keys(upgradeRequire);
                for(var i = 0;i<keyType.length;i++){
                    var objTemp = upgradeRequire[keyType[i]];                
                    var keys = Object.keys(objTemp);
                    if(keys.length===0){
                        var row = itemTemp;                    
                        row = row.replace("#t", keyType[i]);
                        row = row.replace("#n", keyType[i]);
                        row = row.replace("#v", upgradeRequire[keyType[i]]);
                        html += row;
                    } else{
                        for (var j = 0; j < keys.length; j++) {
                            var name = keys[j];
                            var value = objTemp[name];
                            var row = itemTemp;                    
                            row = row.replace("#t", keyType[i]);
                            row = row.replace("#n", name);
                            row = row.replace("#v", value);
                            html += row;
                        }
                    }
                }                
                htmlInfo = htmlInfo.replace("#ct#", html);
                $('#divUpgrade').html(htmlInfo);
            }

            function addRowTable(key){
                var arr = key.split("_");
            }
            
            function rmRowTable(key){
                var arr = key.split("_");
            }

            function txtSelectLevelOnclick() {
                var lv = $('#txtSelectLevel').val();
                if (lv !== '') {
                    lv = parseInt(lv, 10);
                    renderInfo(objContent.info[lv - 1]);
                    renderUpgrade(objContent.upgrade_require[lv - 1]);
                }
            }

            function renderGUI() {
                var numberOfLevel = objContent.info.length;
                console.log(numberOfLevel);
                var html = "<select id='txtSelectLevel' onclick='txtSelectLevelOnclick()'>";
                html += "<option value=''>Select level to editor</option>";
                for (var i = 1; i <= numberOfLevel; i++) {
                    html += "<option value='" + i + "'>Level " + i + "</option>";
                }
                html += "</select>";
                $('#divSelectLevel').html(html);


            }

            function getDetailJson(key) {
                objType = key;
                $("#content_detail").show();
                var errorFunc = function (res) {};
                var successFunc = function (res) {
                    objContent = JSON.parse(res);
                    renderGUI();
                };
                doPost(__url_api__, {action: "get_gameobject_info", repoType: repoType, objType: objType}, successFunc, errorFunc)
            }

            function saveProfile() {
                var pass = $("#txtPass").val();
                if (pass === null || pass === "" || pass === undefined) {
                    alert('Please input your password to save profile');
                    return;
                } else {
                    var data = JSON.parse($("#logProfile").val());
                    if (objType !== data.userID) {
                        alert('Do not change userID in profile, please.');
                        return;
                    }
                }
                $("#content_detail").hide();
                var json = {
                    action: "upd_resource",
                    json: $("#logProfile").val(),
                    pass: $("#txtPass").val()
                };
                var errorFunc = function (res) {
                    alert(res);
                };
                var successFunc = function (res) {
                    if (res == "") {
                        $("#content_detail").hide();
                        objType = null;
                        oTableInfo.$('tr.selected').removeClass('selected');
                        cancelEditInfo();
                        alert("Save profile is successful.");
                    } else {
                        alert(res);
                    }
                };
                doPost(__url_api__, json, successFunc, errorFunc)
            }

            function initPage() {
                $('#list tfoot td').each(function () {
                    var title = $(this).text();
                    if (title === "HIDE_THIS") {
                        $(this).html('');
                    }
                });
                $('#list tbody').on('click', 'tr', function () {
                    if (!$(this).hasClass('selected')) {
                        oTableInfo.$('tr.selected').removeClass('selected');
                        $(this).addClass('selected');

                        var td = $(this).children()[0];
                        var key = $(td).html();
                        getDetailJson(key);
                    }
                });
                setInterval(function () {
                    $.get(_self());
                }, 900000);

                $("#content_detail").hide();
                var errorFunc = function (res) {};
                var successFunc = function (res) {
                    loadData(res)
                };
                doPost(__url_api__, {action: "list_gameobjectinfo"}, successFunc, errorFunc);
            }

            function cancelEditInfo() {
                $("#logProfile").val('');
                $("#txtPass").val('');
                $("#content_detail").hide();
            }

            function submitEditInfo() {
                saveProfile();
            }

            function onClickbtnGameObjectInfo() {
                repoType = "unit";
                var lst = listData["unit"];
                table = $("#list").dataTable();
                oSettings = table.fnSettings();
                table.fnClearTable(this);

                for (var i = 0; i < lst.length; i++) {
                    table.oApi._fnAddData(oSettings, lst[i]);
                }
                oSettings.aiDisplay = oSettings.aiDisplayMaster.slice();
                table.fnDraw();
            }

            function onClickbtnGameObjectDecor() {
                repoType = "decor";
                var lst = listData["decor"];
                table = $("#list").dataTable();
                oSettings = table.fnSettings();
                table.fnClearTable(this);

                for (var i = 0; i < lst.length; i++) {
                    table.oApi._fnAddData(oSettings, lst[i]);
                }
                oSettings.aiDisplay = oSettings.aiDisplayMaster.slice();
                table.fnDraw();
            }

            $(document).ready(initPage);
        </script>
    </head>
    <body>
        <jsp:include page="_header.jsp" />
        <br/>
        <input type="button" id="btnGameObjectInfo" value="GameObjectInfo" onclick="onClickbtnGameObjectInfo()"/>
        <!--<input type="button" id="btnGameObjectDecor" value="GameObjectDecor" onclick="onClickbtnGameObjectDecor()"/>-->
        <br/>        
        <section>
            <div class="row">
                <div class="form-horizontal">
                    <div class="col-md-2">
                        <table id="list" class="table table-striped table-bordered" cellspacing="0" width="100%">
                            <tfoot>
                                <tr>
                                    <td><input type="text" style="width:150px;" placeholder="Search..." value=""/></td>
                                </tr>
                            </tfoot>
                            <thead>
                                <tr>
                                    <th>USERID</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                    <div id="content_detail" class="col-md-10">
                        <div class='form-group form-group-sm'>
                            <label class='col-sm-1 control-label' for='txtSelectLevel'>Level</label>
                            <div id="divSelectLevel" class="col-md-8">
                            </div>
                        </div>
                        <div class='form-group form-group-sm'>
                            <label class='col-sm-1 control-label' for='txtPass'>Attributes</label>
                            <div id="divTableInfo" class="col-md-8">
                                <button type='button' class='btn btn-default' onclick="cancelEditInfo()">Add Row</button>
                                <br/><br/>
                                <div id="divInfo">
                                </div>                                
                            </div>
                        </div>
                        <div class='form-group form-group-sm'>
                            <label class='col-sm-1 control-label' for='txtPass'>Upgrade requires</label>
                            <div id="divTableUpgrade" class="col-md-8">
                                <button type='button' class='btn btn-default' onclick="cancelEditInfo()">Add Row</button>
                                <br/><br/>
                                <div id="divUpgrade"></div>
                            </div>
                        </div>
                        <div class='form-group form-group-sm'>
                            <label class='col-sm-1 control-label' for='txtPass'>Profile</label>
                            <div class='col-sm-4'>
                                <input class='form-control' type='password' id='txtPass' value='' placeholder="Input password to save profile" />
                            </div>
                        </div>
                        <div class='form-group form-group-sm'>
                            <label class='col-sm-1 control-label' for=''></label>
                            <div class='col-sm-6'>
                                <button type='button' class='btn btn-default' onclick="cancelEditInfo()">Cancel</button>
                                <button type='button' class='btn btn-default' onclick='submitEditInfo()'>Submit</button>
                            </div>
                        </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>
    </body>
</html>
