/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.google.gson;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

/**
 *
 * @author ccthien
 */
public class StringCache {
	public static final HashMap<String,String> CACHE = new HashMap();
	public static final boolean enableCaching = true;
	
	public static String cache(String result) {
		synchronized(CACHE) {
			String cache = CACHE.get(result);
			return (cache == null) ? result : cache;
		}
	}
	
	public static String add(String s) {
		if (!enableCaching) {
			return s;
		}
		synchronized(CACHE) {
			String res = CACHE.get(s);
			if (res == null) {
				CACHE.put(s, s);
				return s;
			}
			return res;
		}
	}
	
	public static String[] add(String[] list) {
		if (!enableCaching) {
			return list;
		}
		if (list == null || list.length == 0) {
			return list;
		}
		int len = list.length;
		String[] res = new String[len];
		for (int i = 0; i < len; ++i) {
			res[i] = add(list[i]);
		}
		return res;
	}
	
	public static HashSet<String> add(HashSet<String> list) {
		if (!enableCaching) {
			return list;
		}
		if (list == null) {
			return null;
		}
		HashSet<String> res = new HashSet<>(list.size());
		for (String v : list) {
			res.add(add(v));
		}
		return res;
	}
	
	public static <T> HashMap<String, T> add(HashMap<String, T> map) {
		if (!enableCaching) {
			return map;
		}
		if (map == null) {
			return null;
		}
		HashMap<String, T> res = new HashMap<>(map.size());
		for (Map.Entry<String, T> e : map.entrySet()) {
			res.put(add(e.getKey()), e.getValue());
		}
		return res;
	}
	
	public static void clear() {
		synchronized(CACHE) {
			CACHE.clear();
		}
	}
}
