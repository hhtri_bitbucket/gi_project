/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dp.db;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author ccthien
 */
public class DBFile {
	public final File folder;
	public DBFile(File folder) {
		folder.mkdirs();
		this.folder = folder;
	}
	public final HashMap<String,File> files = new HashMap();
	public File getFile(String filename) {
		File file = files.get(filename);
		if(file==null) {
			file = new File(folder,filename);
			files.put(filename, file);
		}
		return file;
	}
	public void save(DBData data) {
		File file = getFile(Func.todayServer()+"."+data.table());
		synchronized(file) {
			try(FileOutputStream os = new FileOutputStream(file,true)) {
				data.save(new DataOutputStream(os));
			} catch (IOException ex) {
				Func.printlnLog("DBFile_err.log", ex);
			}
		}
	}
    public <T extends DBData> void select(Class<T> classOfT,DBDataListener<T> listener) {
		File file = getFile(classOfT.getSimpleName());
		synchronized(file) {
			try(FileInputStream fis = new FileInputStream(file)) {
				DataInputStream is = new DataInputStream(fis);
				while(true) {
					T data = classOfT.newInstance();
					if(data.load(is)) {listener.onItem(data);}else{break;}
				}
			} catch (EOFException ex) {
			} catch (IOException|InstantiationException|IllegalAccessException ex) {
				Func.printlnLog("DBFile_err.log", ex);
			}
		}
	}
}
