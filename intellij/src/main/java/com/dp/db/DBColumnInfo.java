/*
 * Copyright 2017 Chau Chi Thien
 */
package com.dp.db;

/**
 *
 * @author ccthien
 */
public class DBColumnInfo {
	public String name;
	public String type;
	public int size;

	public DBColumnInfo(String name, String type, int size) {
		this.name = name;
		this.type = type;
		this.size = size;
	}
	
}
