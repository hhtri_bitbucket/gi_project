/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.dp.db;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 *
 * @author ccthien
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface DataFieldType {
    public boolean autoIncreaseSequence() default true;
    public boolean autoIncrease() default false;
    public boolean usingMapSeq() default false;
    public boolean ignore() default false;
    public boolean notNull() default false;
    public boolean primaryKey() default false;
    public boolean uniqueKey() default false;
    public boolean isTimestamp() default false;
    public int size() default 0;
    public String childField() default "";
    
    public boolean isMap() default false;
    public Class mapKeyClass() default Object.class;
    public Class mapValueClass() default Object.class;
    
    
    public static final int SIZE_NORMAL = 0;
    public static final int SIZE_TINY = -1;
    public static final int SIZE_MEDIUM = -2;
    public static final int SIZE_LONG = -3;
}
