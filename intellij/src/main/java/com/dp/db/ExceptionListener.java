/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dp.db;

import java.sql.SQLException;

/**
 *
 * @author ccthien
 */
public interface ExceptionListener {

    void onException(Exception ex, Object data);
}
