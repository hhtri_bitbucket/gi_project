/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dp.db.data.js;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;
import java.util.Set;
import jdk.nashorn.api.scripting.JSObject;

/**
 *
 * @author ccthien
 */
public class JSArray extends ArrayList<Object> implements JSData {
	@Override public Object getSlot(int i) {return get(i);}
	@Override public boolean hasSlot(int i) {return i<size();}
	@Override public void setSlot(int i, Object o) {set(i, o);}
	@Override public String getClassName() {return getClass().getSimpleName();}
	@Override public boolean isFunction() {return false;}
	@Override public boolean isStrictFunction() {return false;}
	@Override public boolean isArray() {return true;}
	@Override public Collection<Object> values() {return this;}
	@Override public String toString(String space) {
		StringBuilder sb=new StringBuilder();
			sb.append(space).append("<"+getClassName()+">:\n");
		for(Object v:values()) {
			if(v instanceof JSData) {
				sb.append(((JSData)v).toString(space+"\t"));
			}else {
				sb.append(space+"\t").append("<"+v.getClass().getSimpleName()+">"+v).append("\n");
			}
		}
		return sb.toString();
	}


	@Override public Set<String> keySet() {
		throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
	}
	
	@Override
	public Object call(Object o, Object... os) {
		throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
	}

	@Override
	public Object newObject(Object... os) {
		throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
	}

	@Override
	public Object eval(String string) {
		throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
	}

	@Override
	public Object getMember(String string) {
		throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
	}

	@Override
	public boolean hasMember(String string) {
		throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
	}

	@Override
	public void removeMember(String string) {
		throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
	}

	@Override
	public void setMember(String string, Object o) {
		throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
	}

	@Override
	public boolean isInstance(Object o) {
		throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
	}

	@Override
	public boolean isInstanceOf(Object o) {
		throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
	}
	@Override public double toNumber() {
		throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
	}
}
