/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dp.db.data.js;

import com.dp.db.Func;
import java.util.Collection;
import java.util.HashMap;
import java.util.Set;

/**
 *
 * @author ccthien
 */
public class JSObj extends HashMap<String,Object> implements JSData {
	@Override public Set<String> keySet() {
		synchronized(this) {return super.keySet();}
	}

	@Override public Collection<Object> values() {
		synchronized(this) {return super.values();}
	}

	@Override
	public void removeMember(String string) {
		synchronized(this) {remove(string);}
	}

	@Override
	public void setMember(String string, Object o) {
		synchronized(this) {put(string,o);}
	}
	@Override public Object getMember(String string) {
		return get(string);
	}
	@Override public String toString() {
		return Func.pson(this);
	}
	@Override public boolean hasMember(String string) {return containsKey(string);}
	@Override public String getClassName() {return getClass().getSimpleName();}
	@Override public boolean isFunction() {return false;}
	@Override public boolean isStrictFunction() {return false;}
	@Override public boolean isArray() {return false;}
	@Override public String toString(String space) {
		StringBuilder sb=new StringBuilder();
		sb.append(space+"<").append(getClassName()).append(">:\n");
		for(Entry<String,Object> entry:entrySet()) {
			Object v = entry.getValue();
			sb.append(space+"\t").append(entry.getKey()).append(": ");
			if(v instanceof JSData) {
				sb.append("\n").append(((JSData)v).toString(space+"\t"));
			}else {
				sb.append(v==null?null:"<"+v.getClass().getSimpleName()+">"+v).append("\n");
			}
		}
		return sb.toString();
	}

	@Override
	public Object call(Object o, Object... os) {
		throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
	}

	@Override
	public Object newObject(Object... os) {
		throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
	}

	@Override
	public Object eval(String string) {
		throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
	}
	
	@Override
	public boolean isInstance(Object o) {
		throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
	}

	@Override
	public boolean isInstanceOf(Object o) {
		throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
	}

	@Override
	public Object getSlot(int i) {
		throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
	}

	@Override
	public boolean hasSlot(int i) {
		throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
	}

	@Override
	public void setSlot(int i, Object o) {
		throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
	}

	@Override
	public double toNumber() {
		throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
	}
}
