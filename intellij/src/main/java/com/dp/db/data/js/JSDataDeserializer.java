/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dp.db.data.js;

import com.google.gson.*;
import java.util.Map.*;
import java.lang.reflect.Type;

/**
 *
 * @author ccthien
 */
public class JSDataDeserializer implements JsonDeserializer<JSData> {
	@Override public JSData deserialize(JsonElement json, Type type, JsonDeserializationContext context) throws JsonParseException {
		Object o = deserializeObj(json);
		return (o instanceof JSData)?(JSData)deserializeObj(json):null;
	}
	public Object deserializeObj(JsonElement json) {
		if(json.isJsonObject()) {
			JSObj obj = new JSObj();
			JsonObject o = (JsonObject)json.getAsJsonObject();
			for(Entry<String,JsonElement> entry:o.entrySet()) {
				obj.put(entry.getKey(), deserializeObj(entry.getValue()));
			}
			return obj;
		}else if(json.isJsonArray()) {
			JSArray arr = new JSArray();
			JsonArray a = (JsonArray)json.getAsJsonArray();
			for(JsonElement e:a) {arr.add(deserializeObj(e));}
			return arr;
		}else if(json.isJsonPrimitive()) {
			JsonPrimitive p = json.getAsJsonPrimitive();
			if(p.isBoolean()) return p.getAsBoolean();
			else if(p.isString()) return p.getAsString();
			else if(p.isNumber()) {
				double v = p.getAsDouble();
				if ((v == Math.floor(v)) && !Double.isInfinite(v)) {
					return p.getAsLong();
				}
				return v;
			}
		}
	    return null;
	}
}