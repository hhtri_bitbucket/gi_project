/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.dp.db;

/**
 *
 * @author ccthien
 */
public class BaseDataInt extends BaseData {
    public int ID;
    public boolean saveOnChanged() {
        if (ID==0) return false;
        if(Changed) {
            Changed = false;
            DBOracle.i().save(table(), ID, Func.json(this));
        }
        return true;
    }
    public void delete() {DBOracle.i().delete(table(),ID);}
}
