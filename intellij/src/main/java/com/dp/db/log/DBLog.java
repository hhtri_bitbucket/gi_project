/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dp.db.log;

import com.dp.db.DBData;
import com.dp.db.DataFieldType;
import java.net.InetAddress;
import java.sql.Timestamp;
import java.util.HashMap;

/**
 *
 * @author ccthien
 */
public abstract class DBLog extends DBData {

    public static enum DBAction {
        SAVE(1), DELETE(2);
        public final int id;

        DBAction(int action) {
            this.id = action;
        }

        private static final HashMap<Integer, DBAction> MAP = new HashMap();

        static {
            for (DBAction a : DBAction.values()) {
                MAP.put(a.id, a);
            }
        }

        public static DBAction get(int action) {
            return MAP.get(action);
        }
    }
    public static String LOCALHOST = "";

    static {
        try {
            LOCALHOST = InetAddress.getLocalHost().toString();
        } catch (Exception ex) {
        }
    }

    @DataFieldType(primaryKey = true, autoIncrease = true)
    public long idx;
    public Timestamp time = new Timestamp(System.currentTimeMillis());
    public String table;
    @DataFieldType(size = DataFieldType.SIZE_LONG)
    public String data;
    public int action = DBAction.SAVE.id;
    public String host = LOCALHOST;

    public DBLog() {
    }
}
