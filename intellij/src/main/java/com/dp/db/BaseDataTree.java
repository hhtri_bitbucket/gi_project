/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.dp.db;

import java.util.ArrayList;
import java.util.TreeMap;

/**
 *
 * @author ccthien
 * @param <T>
 */
public class BaseDataTree<T extends BaseDataScoreInterface> {
    public static BaseDataScoreRecord record(BaseDataScoreInterface score) {
        BaseDataScoreRecord r = new BaseDataScoreRecord();
        r.score = score.getScore();
        r.time = score.getTime();
        return r;
    }
    public TreeMap<Long,BaseDataRank<T>>scores = new TreeMap<Long,BaseDataRank<T>>();
    public void remove(T t) {remove(record(t));}
    public void remove(BaseDataScoreRecord old) {
        BaseDataRank<T> rank = scores.get(old.score);
        if(rank!=null) {
            rank.map.remove(old.time);
            if(rank.map.size()==0) {
                scores.remove(rank.score);
            }
        }
    }
    public void update(T t) {update(t,null);}
    public void update(T t,BaseDataScoreRecord old) {
        if(old!=null) {
            remove(old);
        }
        BaseDataRank<T> rank = scores.get(t.getScore());
        if(rank==null) {
            rank = new BaseDataRank<T>(t.getScore());
            add(rank);
        }
        while(rank.map.get(t.getTime())!=null) t.increaseTime();
        rank.map.put(t.getTime(), t);
    }
    public void add(BaseDataRank<T> rank) {
        scores.put(rank.score, rank);
    }
    public ArrayList<T> all() {
        ArrayList<T>list = new ArrayList<T>();
        if(!scores.isEmpty()) {
            synchronized(scores) {
                Long cur = scores.lastKey();
                while(cur!=null) {
                    BaseDataRank<T> sr = scores.get(cur);
                    for(T user:sr.map.values()) {
                        list.add(user);
                    }
                    cur = scores.lowerKey(cur);
                }
            }
        }
        return list;
    }
    public ArrayList<T> top(int n) {
        ArrayList<T>list = new ArrayList<T>();
        if(!scores.isEmpty()) {
            synchronized(scores) {
                Long cur = scores.lastKey();
                while(cur!=null) {
                    BaseDataRank<T> sr = scores.get(cur);
                    for(T user:sr.map.values()) {
                        list.add(user);
                        if(--n==0) return list;
                    }
                    cur = scores.lowerKey(cur);
                }
            }
        }
        return list;
    }
    public ArrayList<T> top(long start_time,long end_time) {
        ArrayList<T>list = new ArrayList<T>();
        if(!scores.isEmpty()) {
            synchronized(scores) {
                Long cur = scores.lastKey();
                while(cur!=null) {
                    BaseDataRank<T> sr = scores.get(cur);
                    for(T user:sr.map.values()) {
                        if(user.getTime() >= start_time && user.getTime() < end_time) list.add(user);
                    }
                    cur = scores.lowerKey(cur);
                }
            }
        }
        return list;
    }
}
