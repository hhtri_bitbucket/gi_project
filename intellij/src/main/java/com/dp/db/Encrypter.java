/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dp.db;

import java.io.UnsupportedEncodingException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import org.apache.commons.codec.binary.Base64;

/**
 *
 * @author ccthien
 */
public class Encrypter {
    private static final String CIPHER = "AES/CBC/PKCS5Padding";
    private static final String PROVIDER = "SunJCE";
    private static final String PRIVATE_KEY = "ctmlab2008201804";
    private static final String IV = "AAAABBBBCCCCDDDD";
    private static final SecretKeySpec KEY = new SecretKeySpec(PRIVATE_KEY.getBytes(Func.UTF8), "AES");
    private static Cipher cipher_encrypt;
    private static Cipher cipher_decrypt;
	private static void init() {
        try {
            cipher_encrypt = Cipher.getInstance(CIPHER, PROVIDER);
            cipher_encrypt.init(Cipher.ENCRYPT_MODE, KEY, new IvParameterSpec(IV.getBytes(Func.UTF8)));
            cipher_decrypt = Cipher.getInstance(CIPHER, PROVIDER);
            cipher_decrypt.init(Cipher.DECRYPT_MODE, KEY, new IvParameterSpec(IV.getBytes(Func.UTF8)));            
        } catch (Exception ex) {}
	}
    static {init();}
	public static ExceptionListener exceptionListener = null;
	
    public static String getKey(){
        return Base64.encodeBase64String(KEY.getEncoded());
    }
    public static String encrypt(String text) {
        try {
            return Base64.encodeBase64String(cipher_encrypt.doFinal(text.getBytes(Func.UTF8)));
        } catch (BadPaddingException|IllegalBlockSizeException ex) {
			if(exceptionListener!=null)exceptionListener.onException(ex,text);
	        return null;
        }
    }
    public static String decrypt(String text) {
        try {
            return new String(cipher_decrypt.doFinal(Base64.decodeBase64(text)),Func.UTF8);
        } catch (BadPaddingException|IllegalBlockSizeException ex) {
			if(exceptionListener!=null)exceptionListener.onException(ex,text);
	        return null;
        }
    }
    public static String encryptData(byte []data) {
        try {
            return Base64.encodeBase64String(cipher_encrypt.doFinal(data));
        } catch (BadPaddingException|IllegalBlockSizeException ex) {
			if(exceptionListener!=null)exceptionListener.onException(ex,data);
	        return null;
        }
    }
    public static byte[] decryptData(String text) {
        try {
            return cipher_decrypt.doFinal(Base64.decodeBase64(text));
        } catch (BadPaddingException|IllegalBlockSizeException ex) {
			if(exceptionListener!=null)exceptionListener.onException(ex,text);
	        return null;
        }
    }
}
