/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dp.db;

import com.dp.db.log.DBLog;
import com.dp.db.log.DBLogManager;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintStream;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.lang3.StringUtils;

/**
 *
 * @author ccthien
 */
public abstract class DB {

    public static final int SQL_ERROR = -1024;

    /////////////////////////////////////////////////
    public final HashMap<String, Class> tableClass = new HashMap();

    public Class getTableClass(String table) {
        return tableClass.get(table);
    }

    protected DB() {
    }
    private static DB _inst = null;

    public static DB i() {
        return _inst;
    }

    public static void i(DB db) {
        _inst = db;
    }

    private DBLogManager log_manager = null;

    public void setLogManager(DBLogManager m) {
        log_manager = m;
    }

    protected Connection _con = null;
    public static ExceptionListener exceptionListener = null;

    public static void onException(Exception ex, Object data) {
        if (exceptionListener != null) {
            exceptionListener.onException(ex, data);
        }
    }
    
    public void onLog(String logname, Object data){}

    public String host;
    public String database;
    public String user;
    public String password;

    /////////////////////////////////////////////////
    public abstract String url();

    public String tableSQL(String table) {
        return table;
    }

    public String textDataType() {
        return "text";
    }

    public String intDataType() {
        return "int";
    }

    protected Connection getConnection() throws SQLException {
        return DriverManager.getConnection(url());
    }

    /////////////////////////////////////////////////
    public void reconnect() throws SQLException {
        if (_con != null && !_con.isClosed()) {
            _con.close();
        }
        _con = getConnection();
    }
    
    public Connection con() throws SQLException {
        if (_con == null || _con.isClosed()) {
            _con = getConnection();
        }
        return _con;
    }

    public int exec(String sql) throws SQLException {
        try (PreparedStatement st = con().prepareStatement(sql)) {
            return st.executeUpdate();
        }
    }

    public String scalar(String sql) throws SQLException {
        PreparedStatement st = con().prepareStatement(sql);
        ResultSet rs = st.executeQuery();
        if (rs.next()) {
            return rs.getString(1);
        }
        return null;
    }

    public int delete(Class clazz) {
        return delete(clazz.getSimpleName());
    }

    public int delete(String table) {
        int result = SQL_ERROR;
        try {
            PreparedStatement st = con().prepareStatement("DELETE FROM " + tableSQL(table));
            result = st.executeUpdate();
            st.close();
        } catch (SQLException ex) {
            onException(ex, "delete " + table);
        }
        return result;
    }

    public int drop(String table) {
        int result = SQL_ERROR;
        try {
            result = exec("DROP TABLE " + tableSQL(table));
        } catch (SQLException ex) {
            onException(ex, "drop " + table);
        }
        return result;
    }

    public void dropTable(DBData data) throws SQLException {
        drop(data.table());
    }

    public boolean loadAll(Class clazz, TreeMap map) {
        try {
            try (PreparedStatement st = con().prepareStatement("SELECT data FROM " + tableSQL(clazz.getSimpleName())); ResultSet rs = st.executeQuery()) {
                if (BaseDataInt.class.isAssignableFrom(clazz)) {
                    while (rs.next()) {
                        BaseDataInt data = (BaseDataInt) Func.gson.fromJson(rs.getString(1), clazz);
                        map.put(data.ID, data);
                    }
                } else {
                    while (rs.next()) {
                        BaseDataString data = (BaseDataString) Func.gson.fromJson(rs.getString(1), clazz);
                        map.put(data.ID, data);
                    }
                }
            }
        } catch (SQLException ex) {
            onException(ex, "loadAll " + clazz);
        }
        return true;
    }

    public ArrayList<String> allTables() {
        return new ArrayList();
    }

    public void export(String table, PrintStream os) {
        try {
            try (PreparedStatement st = con().prepareStatement("SELECT * from " + tableSQL(table)); ResultSet rs = st.executeQuery()) {
                ResultSetMetaData rsmd = rs.getMetaData();
                while (rs.next()) {
                    JsonObject obj = new JsonObject();
                    for (int i = 1; i <= rsmd.getColumnCount(); i++) {
                        int type = rsmd.getColumnType(i);
                        switch (type) {
                            case java.sql.Types.NUMERIC:
                                obj.addProperty(rsmd.getColumnName(i), rs.getInt(i));
                                break;
                            case java.sql.Types.CLOB:
                            case java.sql.Types.VARCHAR:
                                obj.addProperty(rsmd.getColumnName(i), rs.getString(i));
                                break;
                        }
                    }
                    os.println(Func.json(obj));
                }
            }
        } catch (SQLException ex) {
            onException(ex, "export " + table);
        }
    }

    public void exportAll(File folder) {
        if (!folder.exists()) {
            folder.mkdirs();
        }
        for (String table : DB.i().allTables()) {
            try {
                try (PrintStream out = new PrintStream(new File(folder, table + ".json"))) {
                    export(table, out);
                }
            } catch (FileNotFoundException ex) {
                Logger.getLogger(DB.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public boolean clear(Class clz) {
        try {
            exec("TRUNCATE TABLE " + tableSQL(clz.getSimpleName()));
        } catch (SQLException ex) {
            return false;
        }
        return true;
    }

    public boolean importTable(String folder, Class clz) {
        try {
            String table = clz.getSimpleName();
            Files.lines(Paths.get(folder + "/" + table + ".json")).forEach((line) -> {
                JsonObject obj = Func.gson.fromJson(line, JsonObject.class);
                ArrayList<String> cols = new ArrayList();
                ArrayList<String> questions = new ArrayList();
                ArrayList<JsonElement> values = new ArrayList();
                for (Entry<String, JsonElement> entry : obj.entrySet()) {
                    cols.add(entry.getKey());
                    questions.add("?");
                    values.add(entry.getValue());
                }
                String sql = "INSERT INTO " + tableSQL(table) + "(" + StringUtils.join(cols, ",") + ")VALUES(" + StringUtils.join(questions, ",") + ")";
                int id = values.get(0).getAsJsonPrimitive().getAsInt();
                try (PreparedStatement st = con().prepareStatement(sql)) {
                    for (int i = 0; i < values.size(); i++) {
                        JsonPrimitive v = values.get(i).getAsJsonPrimitive();
                        if (v.isNumber()) {
                            st.setInt(i + 1, v.getAsInt());
                        } else if (v.isString()) {
                            st.setString(i + 1, v.getAsString());
                        }
                    }
                    st.executeUpdate();
                    System.out.println(id + " --> Success");
                } catch (SQLException ex) {
                    System.out.println(id + " --> Failed " + ex);
                }
            });
        } catch (IOException ex) {
            Logger.getLogger(DB.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
        return true;
    }

    /////////////////////////////////////////////////////////////////////
    protected void beforeValidateTable(DBData data) throws SQLException {
        tableClass.put(data.table(), data.getClass());
    }

    public void validateTable(DBData data) throws SQLException {
        beforeValidateTable(data);
        data.validateTable(con());
    }

    public void logSave(DBData data) {
        if (log_manager != null && !(data instanceof DBLog)) {
            log_manager.add((DBData) data, DBLog.DBAction.SAVE.id);
        }
    }

    public void logDelete(DBData data) {
        if (log_manager != null && !(data instanceof DBLog)) {
            log_manager.add((DBData) data, DBLog.DBAction.DELETE.id);
        }
    }

    public long save(DBData data) throws SQLException {
        return data.save(con());
    }

    public int delete(DBData data) throws SQLException {
        try {
            DBDataSQLInfo uSQL = data.getSQLInfo();
            try (PreparedStatement st = con().prepareStatement("DELETE FROM " + tableSQL(data.table()) + " WHERE " + uSQL.whereKey)) {
                data.setKeys(st, 1);
                return st.executeUpdate();
            }
        } catch (IllegalArgumentException ex) {
            Logger.getLogger(DBData.class.getName()).log(Level.SEVERE, null, ex);
        }
        return -1;
    }

    public <T extends DBData> long lastAutoID(Class<T> clazz) {
        return 0;
    }

    public Map<String, DBColumnInfo> getColumnInfo(Class table) {
        return new HashMap();
    }

    /////////////////////////////////////////////////////////////////////
    // function for STRING key
    public void validate(String table, int id_size) {
        try {
            exec("CREATE TABLE " + tableSQL(table) + " (\n"
                    + "  id varchar(" + id_size + ") NOT NULL,\n"
                    + "  data " + textDataType() + " NOT NULL,\n"
                    + "  PRIMARY KEY (id)\n"
                    + ")");
        } catch (SQLException ex) {
            if (!ex.getMessage().contains("exist")) {
                onException(ex, "export " + table);
            }
        }
    }

    public void validate(Class clazz, int id_size) {
        validate(clazz.getSimpleName(), id_size);
    }

    public boolean validate(Class clazz, int id_size, TreeMap map) {
        validate(clazz, id_size);
        return loadAll(clazz, map);
    }

    public String get(String table, String id) {
        String result = null;
        try {
            try (PreparedStatement st = con().prepareStatement("SELECT data FROM " + tableSQL(table) + " WHERE id=?")) {
                st.setString(1, id);
                try (ResultSet rs = st.executeQuery()) {
                    if (rs.next()) {
                        result = rs.getString(1);
                    }
                }
            }
        } catch (SQLException ex) {
        }
        return result;
    }

    public boolean exist(String table, String id) {
        try {
            try (PreparedStatement st = con().prepareStatement("SELECT COUNT(*) FROM " + tableSQL(table) + " WHERE id=?")) {
                st.setString(1, id);
                try (ResultSet rs = st.executeQuery()) {
                    if (rs.next()) {
                        return rs.getInt(1) > 0;
                    }
                }
            }
        } catch (SQLException ex) {
            onException(ex, "exist " + table);
        }
        return false;
    }

    public int insert(String table, String id, String data) {
        try {
            int result;
            try (PreparedStatement st = con().prepareStatement("INSERT INTO " + tableSQL(table) + "(id,data)values(?,?)")) {
                st.setString(1, id);
                st.setString(2, data);
                result = st.executeUpdate();
            }
            return result;
        } catch (SQLException ex) {
            onException(ex, "insert " + table);
        }
        return SQL_ERROR;
    }

    public int update(String table, String id, String data) {
        try {
            int result;
            try (PreparedStatement st = con().prepareStatement("UPDATE " + tableSQL(table) + " SET data=? WHERE id=?")) {
                st.setString(1, data);
                st.setString(2, id);
                result = st.executeUpdate();
            }
            return result;
        } catch (SQLException ex) {
            onException(ex, "update " + table);
        }
        return SQL_ERROR;
    }
    
    public int executeUpdate(String sql, Object...objs) throws SQLException {
        try (PreparedStatement st = con().prepareStatement(sql)) {
            int i = 1;
            for(Object obj:objs){
                st.setObject(i++, obj);
            }
            return st.executeUpdate();
        }
    }

    public int delete(String table, String id) {
        int result = SQL_ERROR;
        try {
            try (PreparedStatement st = con().prepareStatement("DELETE FROM " + tableSQL(table) + " WHERE id=?")) {
                st.setString(1, id);
                result = st.executeUpdate();
            }
        } catch (SQLException ex) {
            onException(ex, "delete " + table);
        }
        return result;
    }

    public int save(String table, String id, String data) {
        if (exist(table, id)) {
            return update(table, id, data);
        }
        return insert(table, id, data);
    }

    /////////////////////////////////////////////////////////////////////
    // function for direct INT
    public int getInt(String table, String id) {
        int result = -1;
        try {
            try (PreparedStatement st = con().prepareStatement("SELECT data FROM " + tableSQL(table) + " WHERE id=?")) {
                st.setString(1, id);
                try (ResultSet rs = st.executeQuery()) {
                    if (rs.next()) {
                        result = Integer.parseInt(rs.getString(1));
                    }
                }
            }
        } catch (SQLException ex) {
        }
        return result;
    }

    public String getString(String table, String id) {
        String result = null;
        try {
            try (PreparedStatement st = con().prepareStatement("SELECT data FROM " + tableSQL(table) + " WHERE id=?")) {
                st.setString(1, id);
                try (ResultSet rs = st.executeQuery()) {
                    if (rs.next()) {
                        result = rs.getString(1);
                    }
                }
            }
        } catch (SQLException ex) {
        }
        return result;
    }

    public int save(String table, String id, int data) {
        if (exist(table, id)) {
            return update(table, id, String.valueOf(data));
        }
        return insert(table, id, String.valueOf(data));
    }

    /////////////////////////////////////////////////////////////////////
    // function for INT key
    public void validate(String table) {
        try {
            exec("CREATE TABLE " + tableSQL(table) + " (\n"
                    + "  id " + intDataType() + "(11) NOT NULL,\n"
                    + "  data " + textDataType() + " NOT NULL,\n"
                    + "  PRIMARY KEY (id)\n"
                    + ")");
        } catch (SQLException ex) {
            if (!ex.getMessage().contains("exists")) {
                onException(ex, "create " + table);
            }
        }
    }

    public void validate(Class clazz) {
        validate(clazz.getSimpleName());
    }

    public boolean validate(Class clazz, TreeMap map) {
        validate(clazz);
        return loadAll(clazz, map);
    }

    public boolean exist(String table, int id) {
        try {
            try (PreparedStatement st = con().prepareStatement("SELECT COUNT(*) FROM " + tableSQL(table) + " WHERE id=?")) {
                st.setInt(1, id);
                try (ResultSet rs = st.executeQuery()) {
                    if (rs.next()) {
                        return rs.getInt(1) > 0;
                    }
                }
            }
        } catch (SQLException ex) {
            onException(ex, "exist " + table);
        }
        return false;
    }

    public int insert(String table, int id, String data) {
        int result = SQL_ERROR;
        try {
            try (PreparedStatement st = con().prepareStatement("INSERT INTO " + tableSQL(table) + "(id,data) VALUES(?,?)")) {
                st.setInt(1, id);
                st.setString(2, data);
                result = st.executeUpdate();
            }
        } catch (SQLException ex) {
            onException(ex, "insert " + table);
        }
        return result;
    }

    public int update(String table, int id, String data) {
        int result = SQL_ERROR;
        try {
            try (PreparedStatement st = con().prepareStatement("UPDATE " + tableSQL(table) + " SET data=? WHERE id=?")) {
                st.setString(1, data);
                st.setInt(2, id);
                result = st.executeUpdate();
            }
        } catch (SQLException ex) {
            onException(ex, "update " + table);
        }
        return result;
    }

    public int save(String table, int id, String data) {
        if (exist(table, id)) {
            return update(table, id, data);
        }
        return insert(table, id, data);
    }

    public int delete(String table, int id) {
        int result = SQL_ERROR;
        try {
            try (PreparedStatement st = con().prepareStatement("DELETE FROM " + tableSQL(table) + " WHERE id=?")) {
                st.setInt(1, id);
                result = st.executeUpdate();
            }
        } catch (SQLException ex) {
            onException(ex, "delete " + table);
        }
        return result;
    }

    public static <T> Object getResultSetValue(ResultSet rs, int i, Type c) throws SQLException {
        String s = rs.getString(i);
        return Func.gson.fromJson(s, c);
    }

    public static <T> Object getResultSetValue(ResultSet rs, int i, Class<T> c) throws SQLException {
        if (c == Long.class || c == long.class) {
            return rs.getLong(i);
        } else if (c == Integer.class || c == int.class) {
            return rs.getInt(i);
        } else if (c == Double.class || c == double.class) {
            return rs.getDouble(i);
        } else if (c == Boolean.class || c == boolean.class) {
            return rs.getBoolean(i);
        } else if (c == java.util.Date.class || c == java.sql.Timestamp.class) {
            return rs.getTimestamp(i);
        } else if (c == StringBuilder.class) {
            return new StringBuilder(rs.getString(i));
        } else if (c == String.class) {
            return rs.getString(i);
        } else if (c == byte[].class) {
            return rs.getBytes(i);
        } else {
            String s = rs.getString(i);
            return Func.gson.fromJson(s, c);
        }
    }

    public void setValue(PreparedStatement st, int idx, Object value) throws SQLException {
        Class clazz = value.getClass();
        if (clazz == long.class || clazz == Long.class) {
            st.setLong(idx, (Long) value);
        } else if (clazz == int.class || clazz == Integer.class) {
            st.setInt(idx, (Integer) value);
        } else if (clazz == boolean.class || clazz == Boolean.class) {
            st.setBoolean(idx, (Boolean) value);
        } else if (clazz == double.class || clazz == Double.class) {
            st.setDouble(idx, (Double) value);
        } else if (clazz == String.class || clazz == StringBuilder.class) {
            st.setString(idx, value.toString());
        } else if (clazz == byte[].class) {
            st.setBytes(idx, (byte[]) value);
        } else if (clazz == java.util.Date.class) {
            st.setDate(idx, new java.sql.Date(((java.util.Date) value).getTime()));
        } else {
            st.setString(idx, Func.json(value));
        }
    }

    public HashSet<String> getStructure(Class clazz) {
        try {
            DBDataSQLInfo info = ((DBData) clazz.newInstance()).getSQLInfo();
            return new HashSet(info.nameList);
        } catch (InstantiationException | IllegalAccessException ex) {
            if (exceptionListener != null) {
                exceptionListener.onException(ex, clazz);
            }
        }
        return null;
    }

    protected static class ListParameterizedType implements ParameterizedType {

        private final Type type;

        protected ListParameterizedType(Type type) {
            this.type = type;
        }

        @Override
        public Type[] getActualTypeArguments() {
            return new Type[]{type};
        }

        @Override
        public Type getRawType() {
            return ArrayList.class;
        }

        @Override
        public Type getOwnerType() {
            return null;
        }
    }

    public <T extends DBData> int delete(Class<T> classOfT, String where, Object[] params) throws SQLException {
        try (PreparedStatement st = con().prepareStatement("DELETE FROM " + tableSQL(classOfT.getSimpleName()) + " " + where)) {
            if (params != null) {
                int idx = 1;
                for (Object param : params) {
                    setValue(st, idx++, param);
                }
            }
            return st.executeUpdate();
        }
    }

    public <T extends DBData> ArrayList<T> select(Class<T> classOfT) throws SQLException {
        return select(classOfT, "", null, null, null, null);
    }

    public <T extends DBData> void select(Class<T> classOfT, DBDataListener<T> listener) throws SQLException {
        select(classOfT, "", null, null, listener, null);
    }

    public <T extends DBData> ArrayList<T> select(Class<T> classOfT, String where, Object[] params) throws SQLException {
        return select(classOfT, where, null, null, null, params);
    }

    public <T extends DBData> void select(Class<T> classOfT, String where, Object[] params, DBDataListener<T> listener) throws SQLException {
        select(classOfT, where, null, null, listener, params);
    }

    public <T extends DBData> void select(Class<T> classOfT, String where, DBDataListener<T> listener) throws SQLException {
        select(classOfT, where, null, null, listener, null);
    }

    public <T extends DBData> void select(Class<T> classOfT, String where, String[] cols, DBDataListener<T> listener, Object[] params) throws SQLException {
        select(classOfT, where, new HashSet(Arrays.asList(cols)), null, listener, params);
    }

    public <T extends DBData> void selectIgnoreColumn(Class<T> classOfT, String where, Object[] params, String[] cols, DBDataListener<T> listener) throws SQLException {
        HashSet<String> hash = getStructure(classOfT);
        hash.removeAll(Arrays.asList(cols));
        select(classOfT, where, hash, null, listener, params);
    }

    public <T extends DBData> ArrayList<T> selectIgnoreColumn(Class<T> classOfT, String where, Object[] params, String[] cols) throws SQLException {
        HashSet<String> hash = getStructure(classOfT);
        hash.removeAll(Arrays.asList(cols));
        return select(classOfT, where, hash, null, null, params);
    }

    public <T extends DBData> void select(Class<T> classOfT, Map ref, DBDataListener<T> listener) throws SQLException {
        select(classOfT, "", null, ref, listener, null);
    }

    public <T extends DBData> ArrayList<T> selectIgnoreColumn(Class<T> classOfT, String where, String[] cols) throws SQLException {
        HashSet<String> hash = getStructure(classOfT);
        for (String c : cols) {
            hash.remove(c);
        }
        return select(classOfT, where, hash, null, null, null);
    }

    protected String quoteChar() {
        return "`";
    }

    public String quote(String name) {
        return quoteChar() + name + quoteChar();
    }

    public <T extends DBData> ArrayList<T> select(Class<T> classOfT, String where, HashSet<String> cols, Map ref, DBDataListener<T> listener, Object[] params) throws SQLException {
        ArrayList<T> list = null;
        if (listener == null) {
            list = new ArrayList();
        }

        String sql;
        if (cols == null) {
            sql = ("SELECT * FROM " + tableSQL(classOfT.getSimpleName().toLowerCase()) + " " + where);
        } else {
            StringBuilder sb = new StringBuilder();
            cols.stream().forEach((s) -> {
                if (sb.length() > 0) {
                    sb.append(",");
                }
                sb.append(quote(s));
            });
            sql = ("SELECT " + sb + " FROM " + tableSQL(classOfT.getSimpleName()) + " " + where);
        }
        try (PreparedStatement st = con().prepareStatement(sql)) {
            if (params != null) {
                int idx = 1;
                for (Object param : params) {
                    setValue(st, idx++, param);
                }
            }
            try (ResultSet rs = st.executeQuery()) {
                ResultSetMetaData metaData = rs.getMetaData();

                HashMap<String, Integer> columns = new HashMap();
                HashMap<Integer, java.lang.reflect.Field> fields = new HashMap();
                int count = metaData.getColumnCount(); //number of column
                for (int i = 1; i <= count; i++) {
                    columns.put(metaData.getColumnLabel(i), i);
                }

                java.lang.reflect.Field mapField = null;
                DataFieldType mapType = null;
                Class clazz = classOfT;
                while (clazz != DBData.class) {
                    for (java.lang.reflect.Field f : clazz.getDeclaredFields()) {
                        f.setAccessible(true);
                        if (java.lang.reflect.Modifier.isStatic(f.getModifiers())) {
                            continue;
                        }
                        DataFieldType type = f.getAnnotation(DataFieldType.class);
                        if (type != null) {
                            if (type.isMap()) {
                                mapField = f;
                                mapType = type;
                                continue;
                            }
                        }
                        Integer i = columns.get(f.getName());
                        if (i != null) {
                            fields.put(i, f);
                            columns.remove(f.getName());
                        }
                    }
                    clazz = clazz.getSuperclass();
                }

                while (rs.next()) {
                    try {
                        T t = classOfT.newInstance();
                        for (Integer i : fields.keySet()) {
                            java.lang.reflect.Field f = fields.get(i);
                            DataFieldType type = f.getAnnotation(DataFieldType.class);
                            if (type != null && !type.childField().isEmpty() && ref != null) {
                                try {
                                    f.set(t, ref.get(getResultSetValue(rs, i, f.getType().getField(type.childField()).getType())));
                                } catch (NoSuchFieldException ex) {
                                    Func.printlnLog(this.getClass().getSimpleName() + "_err.log", ex);
                                }
                            } else {
                                if (f.getType() == ArrayList.class) {
                                    Type the_type = new ListParameterizedType(type.mapValueClass());
                                    f.set(t, getResultSetValue(rs, i, the_type));
                                } else {
                                    f.set(t, getResultSetValue(rs, i, f.getType()));
                                }
                            }
                        }
                        if (mapField != null) {
                            if (mapField.getType() == TreeMap.class) {
                                TreeMap map = (TreeMap) mapField.get(t);

                                for (String key : columns.keySet()) {
                                    if (mapType.mapKeyClass().isEnum()) {
                                        map.put(Enum.valueOf(mapType.mapKeyClass(), key), getResultSetValue(rs, columns.get(key), mapType.mapValueClass()));
                                    } else {
                                        map.put(key, getResultSetValue(rs, columns.get(key), mapType.mapValueClass()));
                                    }
                                }
                            }
                        }
                        if (listener == null) {
                            list.add(t);
                        } else {
                            listener.onItem(t);
                        }
                    } catch (InstantiationException | IllegalAccessException ex) {
                        Logger.getLogger(DBData.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
        }

        return list;
    }

    public void close() {
        try {
            if (_con != null && !_con.isClosed()) {
                _con.close();
            }
        } catch (SQLException ex) {
            onException(ex, "DB_close_error");
        }
    }
}
