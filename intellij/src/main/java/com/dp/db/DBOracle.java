/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.dp.db;

import java.lang.reflect.Field;
import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

/**
 *
 * @author ccthien
 */
public class DBOracle extends DB {
    public DBOracle(String _host,String _database,String _user,String _password) {
        try {
            Class.forName("oracle.jdbc.driver.OracleDriver");
        } catch (ClassNotFoundException e) {
            System.out.println("Where is your Oracle JDBC Driver?");
        }
        host = _host;
        database = _database;
        user = _user;
        password = _password;
    }
    
    @Override public ArrayList<String> allTables() {
        ArrayList<String> list = new ArrayList();
        try{
            try (PreparedStatement st = con().prepareStatement("SELECT table_name FROM user_tables"); ResultSet rs = st.executeQuery()) {
                while(rs.next()) {
                    list.add(rs.getString(1));
                }
            }
        }catch(SQLException ex){onException(ex,"allTables");}
        return list;
    }
    
    @Override public String url() {return "jdbc:oracle:thin:@"+host+":1521/"+database;}
    @Override public String tableSQL(String table) {return "\""+table+"\"";}
    @Override public String textDataType() {return "NCLOB";}
    @Override public String intDataType() {return "number";}
    @Override protected Connection getConnection() throws SQLException {return DriverManager.getConnection(url(),user,password);}

	/////////////////////////////////////////////////////////
	
	private static String dbType(DBDataField f) {
		return dbType(f.type,f.clazz,f.size);
	}
	private static String dbType(DataFieldType type,Class clazz,int size) {
        if(type!=null && !type.childField().isEmpty()) {
            try {
                java.lang.reflect.Field field = clazz.getField(type.childField());
                clazz = field.getType();
                type = field.getAnnotation(DataFieldType.class);
                size = (type==null)?0:type.size();
            }catch(NoSuchFieldException e) {
                onException(e, type);
            }
        }
        if(clazz==long.class || clazz==Long.class) {
            if(type!=null && type.autoIncrease()) {
                return("NUMBER(24)");
            }else{
                return("NUMBER(24)");
            }
        }else if(clazz==int.class || clazz==Integer.class) {
            if(type!=null && type.autoIncrease()) {
                return("NUMBER(10)");
            }else{
                return("NUMBER(10)");
            }
        }else if(clazz==boolean.class || clazz==Boolean.class) {
            return("NUMBER(3)");
        }else if(clazz==double.class || clazz==Double.class) {
            return("FLOAT(126)");
        }else if(clazz==java.util.Date.class || clazz==java.sql.Timestamp.class) {
			return "TIMESTAMP";
        }else if(clazz==byte[].class) {
			return "BLOB";
        }else{
            if(size>0) {
				return("NVARCHAR2("+size+")");
			} else {
				return "NCLOB";
            }
        }
    }
    private static String toSqlTableType(DataFieldType type,Class clazz,String name,int size) {
        StringBuilder sb = new StringBuilder();
        sb.append("\t\""+name+"\" "+dbType(type, clazz, size));
        if(type!=null && type.isTimestamp()) {
            sb.append(" default CURRENT_TIMESTAMP");
        }
        sb.append(",\n");
        return sb.toString();
    }
	@Override public void dropTable(DBData data) throws SQLException {
		exec("DROP TABLE \""+data.table()+"\"");
        Class clazz = data.getClass();
        while(clazz != DBData.class) {
            for (java.lang.reflect.Field f : clazz.getDeclaredFields()) {
                if(java.lang.reflect.Modifier.isStatic(f.getModifiers())) continue;
                try {
                    DataFieldType type = f.getAnnotation(DataFieldType.class);
                    if(type!=null) {
						if(type.autoIncrease()) {
							exec("DROP SEQUENCE \""+data.table()+"_"+f.getName()+"_SEQ\"");
						}
                    }
                }catch(Exception e) {onException(e, data);}
            }
            clazz = clazz.getSuperclass();
        }
	}
    public ArrayList<String> getSqlTable(DBData data) {
		ArrayList<String> list = new ArrayList();
        StringBuilder sb = new StringBuilder();
        StringBuilder primary_key = new StringBuilder();
        sb.append("CREATE TABLE \""+data.table()+"\" (\n");
        Class clazz = data.getClass();
        while(clazz != DBData.class) {
            for (java.lang.reflect.Field f : clazz.getDeclaredFields()) {
                if(java.lang.reflect.Modifier.isStatic(f.getModifiers())) continue;
                try {
                    int size = 0;
                    DataFieldType type = f.getAnnotation(DataFieldType.class);
                    if(type!=null) {
                        if(type.ignore()) continue;
                        size = type.size();
                        if(type.primaryKey()) {
                            if(primary_key.length()>0) primary_key.append(",");
                            primary_key.append("\""+f.getName()+"\"");
                        }
						if(type.autoIncrease()) {
							list.add("CREATE SEQUENCE \""+data.table()+"_"+f.getName()+
									"_SEQ\" MINVALUE 1 MAXVALUE 999999999999999999999999 INCREMENT BY 1 START WITH 1 "+
									(type.autoIncreaseSequence()?"NOCACHE ORDER":"NOORDER")+
									" NOCYCLE");
						}
                    }
                    sb.append(toSqlTableType(type,f.getType(),f.getName(),size));
                }catch(Exception e) {onException(e, data);}
            }
            clazz = clazz.getSuperclass();
        }
        sb.append("\tPRIMARY KEY ("+primary_key+")\n");
        sb.append(")");
		list.add(sb.toString());
        return list;
    }
	@Override public void validateTable(DBData data) throws SQLException {
		beforeValidateTable(data);
		ArrayList<DBDataField> fields = data.getDataFields(); // this will do some init, do not remove
		Map<String,DBColumnInfo> map = getColumnInfo(data.getClass());
		if(map.isEmpty()) {
			for(String sql:getSqlTable(data)) {
				exec(sql);
			}
		}else{
			for(String sql:fields.stream()
				.filter(f -> !map.containsKey(f.name))
				.map(f -> "ALTER TABLE "+tableSQL(data.table())+" ADD "+tableSQL(f.name)+" "+dbType(f))
				.collect(Collectors.toList())
			) {
				exec(sql);
			}
		}
	}
	@Override public Map<String,DBColumnInfo> getColumnInfo(Class table) {
		HashMap map = new HashMap();
		String sql = "select COLUMN_NAME,DATA_TYPE,DATA_LENGTH from user_tab_columns where table_name = '"+table.getSimpleName()+"' order by column_id";
        try (PreparedStatement st = con().prepareStatement(sql);ResultSet rs = st.executeQuery()) {
			while(rs.next()) {
				DBColumnInfo inf = new DBColumnInfo(rs.getString(1), rs.getString(2), rs.getInt(3));
				map.put(inf.name,inf);
			}
		} catch (SQLException ex) {
			onException(ex, sql);
			return map;
		}
		return map;
	}
	
	
	public long nextSeq(DBData data) {
		Field field = DBData.getAutoField(data.getClass());
		String sql = "SELECT \""+data.table()+"_"+field.getName()+"_SEQ\".NEXTVAL FROM DUAL";
        try (PreparedStatement st = con().prepareStatement(sql);ResultSet rs = st.executeQuery()) {
			if(!rs.next()) return -1;
			return rs.getLong(1);
		} catch (SQLException ex) {
			onException(ex, data);
			return -1;
		}
	}
	@Override
	public<T extends DBData> long lastAutoID(Class<T> clazz) {
		Field field = DBData.getAutoField(clazz);
		String sql = "SELECT last_number FROM user_sequences WHERE sequence_name = '"+clazz.getSimpleName()+"_"+field.getName()+"_SEQ'";
        try (PreparedStatement st = con().prepareStatement(sql);ResultSet rs = st.executeQuery()) {
			if(!rs.next()) return -1;
			return rs.getLong(1)-1;
		} catch (SQLException ex) {
			onException(ex, clazz.getSimpleName());
			return -1;
		}
	} 
    public void setValue(oracle.jdbc.OraclePreparedStatement st,int idx,java.lang.reflect.Field f,Object value) throws SQLException {
        DataFieldType type = f.getAnnotation(DataFieldType.class);
        if(type!=null && !type.childField().isEmpty()) {
            try {
                f = f.getType().getField(type.childField());
                value = f.get(value);
            } catch (NoSuchFieldException | IllegalArgumentException | IllegalAccessException ex) {
                Logger.getLogger(DBData.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        Class clazz = f.getType();
        if(clazz==long.class || clazz==Long.class) {
            st.setLong(idx, (Long)value);
        }else if(clazz==int.class || clazz==Integer.class) {
            st.setInt(idx, (Integer)value);
        }else if(clazz==boolean.class || clazz==Boolean.class) {
            st.setBoolean(idx, (Boolean)value);
        }else if(clazz==double.class || clazz==Double.class) {
            st.setDouble(idx, (Double)value);
        }else if(clazz==String.class || clazz==StringBuilder.class) {
			st.setFormOfUse(idx, oracle.jdbc.OraclePreparedStatement.FORM_NCHAR);
            st.setString(idx, value==null?"":value.toString());
        }else if(clazz==byte[].class) {
            st.setBytes(idx, (byte[])value);
        }else if(clazz==java.util.Date.class) {
			long time=(value==null)?System.currentTimeMillis():((java.util.Date)value).getTime();
            st.setTimestamp(idx, new java.sql.Timestamp(time));
        }else if(clazz==java.sql.Timestamp.class) {
            st.setTimestamp(idx, (java.sql.Timestamp)value);
        }else{
			st.setFormOfUse(idx, oracle.jdbc.OraclePreparedStatement.FORM_NCHAR);
            st.setString(idx, Func.json(value));
        }
    }
	public boolean exists(DBData data) throws SQLException {
        DBDataSQLInfo uSQL = data.getSQLInfo(this.getClass());
		String sql = "SELECT count(1) FROM "+tableSQL(data.table())+" WHERE "+uSQL.whereKey;
        try (oracle.jdbc.OraclePreparedStatement st = (oracle.jdbc.OraclePreparedStatement)con().prepareStatement(sql)) {
            int idx = 1;
			for(Field f:uSQL.keys) {
				data.setValue(st, idx++, f, f.get(data));
			}
			ResultSet r = st.executeQuery();
			if(r.next()) return r.getInt(1)>0;
		}catch(IllegalAccessException ex){return false;}
		return false;
	}
	@Override public long save(DBData data) throws SQLException {
        DBDataSQLInfo uSQL = data.getSQLInfo(this.getClass());
        long result = -1;
		if(exists(data)) {
			try (oracle.jdbc.OraclePreparedStatement st = (oracle.jdbc.OraclePreparedStatement)con().prepareStatement(uSQL.updateSQL)) {
				int idx = 1;
				for(Field f:uSQL.updateFields) {
					Object value=f.get(data);
					setValue(st, idx++, f, value);
				}
				for(Field f:uSQL.keys) {
					Object value=f.get(data);
					setValue(st, idx++, f, value);
				}
				result = st.executeUpdate();
			}catch(IllegalAccessException ex) {onException(ex, data);}
		}else{
			try (oracle.jdbc.OraclePreparedStatement st = (oracle.jdbc.OraclePreparedStatement)con().prepareStatement(uSQL.insertSQL)) {
				int idx = 1;
				if(uSQL.autoField!=null) {
					try{result = (long)uSQL.autoField.get(data);}catch(IllegalAccessException ex){result = 0;}
					if(result<1) {
						result = nextSeq(data);
						uSQL.autoField.set(data, result);
					}
				}
				for(Field f:uSQL.insertFields) {
					Object value=f.get(data);
					setValue(st, idx++, f, value);
				}
				result = st.executeUpdate();
			}catch(IllegalAccessException ex) {onException(ex, data);}
		}
        return result;
    }
	@Override public int exec(String sql) throws SQLException {
		try (PreparedStatement st = con().prepareStatement(sql)) {
			return st.executeUpdate();
		}catch(java.sql.SQLSyntaxErrorException ex) {
			if(
				ex.getErrorCode()!=955 && // table exist
				ex.getErrorCode()!=942 && // table not exist
				ex.getErrorCode()!=2289 // seq not exist
			) {throw ex;}
		}
		return -1;
    }
	@Override protected String quoteChar() {return "\"";}
}
