/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.dp.db;

import java.util.*;

/**
 *
 * @author ccthien
 */
public class BaseDataRank<T extends BaseDataScoreInterface> {
    public TreeMap<Long,T>map=new TreeMap<Long,T>();
    public long score;
    public BaseDataRank(long _score) {score = _score;}
}
