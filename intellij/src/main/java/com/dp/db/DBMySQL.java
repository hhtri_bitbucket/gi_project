/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.dp.db;

/**
 *
 * @author ccthien
 */
public class DBMySQL extends DB {
    public DBMySQL(String _host,String _database,String _user,String _password) {
        try {
            Class.forName("com.mysql.jdbc.Driver");
        }catch(ClassNotFoundException ex) {
            System.out.println("Where is your MySQL JDBC Driver?");
        }
        host = _host;
        database = _database;
        user = _user;
        password = _password;
    }
    
    @Override
    public String url() {return "jdbc:mysql://"+host+":3306/"+database+"?user="+user+"&password="+password+"&useUnicode=true&characterEncoding=UTF-8";}
}
