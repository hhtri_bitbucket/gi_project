/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dp.db;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

/**
 *
 * @author ccthien
 */
public class DBDataSQLInfo {

    public int fieldCount = 0;
    public String saveSQL = null;
    public String insertSQL = null;
    public String whereKey = null;
    public String updateSQL = null;

    public java.lang.reflect.Field autoField = null;
    public final HashMap<String, java.lang.reflect.Field> fields = new HashMap();
    public final ArrayList<String> nameList = new ArrayList();
    public final ArrayList<java.lang.reflect.Field> keys = new ArrayList();
    public final ArrayList<java.lang.reflect.Field> updateFields = new ArrayList();
    public final ArrayList<java.lang.reflect.Field> insertFields = new ArrayList();
    public final HashSet<String> hkeys = new HashSet();
}
