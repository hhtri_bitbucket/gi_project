/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.dp.db;

/**
 *
 * @author ccthien
 */
public interface BaseDataScoreInterface {
    public long getScore();
    public long getTime();
    public void increaseTime();
}
