/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.dp.db;

/**
 *
 * @author ccthien
 */
public class DBDataField {
    DataFieldType type;
    Class clazz;
    String name;
    int size;
    boolean isPrimaryKey;
    public DBDataField(DataFieldType _type,Class _clazz,String _name,int _size,boolean key) {
        type = _type;
        clazz = _clazz;
        name = _name;
        size = _size;
        isPrimaryKey = key;
    }
}
