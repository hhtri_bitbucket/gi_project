/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ctmlab.util;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.Enumeration;
import java.util.List;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.io.IOUtils;

/**
 *
 * @author ccthien
 */
public class FileUploadManager {

    public static final int MEMORY_THRESHOLD = 1024 * 1024 * 100;
    public static final int MAX_FILE_SIZE = 1024 * 1024 * 100;
    public static final int MAX_REQUEST_SIZE = 1024 * 1024 * 100;

    private static FileUploadManager inst = null;

    public static FileUploadManager i() {
        if (inst == null) {
            inst = new FileUploadManager();
        }
        return inst;
    }

    DiskFileItemFactory factory;
    ServletFileUpload upload;
    public static final String UTF8 = "UTF-8";

    private FileUploadManager() {
        factory = new DiskFileItemFactory();
        factory.setSizeThreshold(MEMORY_THRESHOLD);
        factory.setRepository(new File(System.getProperty("java.io.tmpdir")));

        upload = new ServletFileUpload(factory);
        upload.setHeaderEncoding(UTF8);
        upload.setFileSizeMax(MAX_FILE_SIZE);
        upload.setSizeMax(MAX_REQUEST_SIZE);
    }

    public List<FileItem> parseRequest(javax.servlet.http.HttpServletRequest request) throws FileUploadException {
        return upload.parseRequest(request);
    }

    public FileUploadRequest getRequest(javax.servlet.http.HttpServletRequest request) {
        Object o = request.getAttribute("FileUploadRequest");
        if (o != null && o instanceof FileUploadRequest) {
            return (FileUploadRequest) o;
        }
        FileUploadRequest req = new FileUploadRequest();
        request.setAttribute("FileUploadRequest", req);

        req.path = request.getServletPath();
        req.url = request.getRequestURL().toString();
        if (ServletFileUpload.isMultipartContent(request)) {
            try {
                for (FileItem item : upload.parseRequest(request)) {
                    if (item.isFormField()) {
                        try {
                            req.fields.put(item.getFieldName(), item.getString(UTF8));
                        } catch (UnsupportedEncodingException ex) {
                        }
                    } else {
                        req.files.put(item.getFieldName(), item);
                    }
                }
            } catch (FileUploadException ex) {
            }
        } else {
            Enumeration<String> e = request.getParameterNames();
            if(e.hasMoreElements()){
                while (e.hasMoreElements()) {
                    String key = e.nextElement();
                    req.fields.put(key, request.getParameter(key));
                }
            } else{
                try {
                    req.data = IOUtils.toString(request.getInputStream(), UTF8);
                } catch (IOException ex) {
                }   
            }
        }
        return req;
    }
}
