/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ctmlab.util;

import com.dp.db.Func;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

/**
 *
 * @author hp-omen
 */
public class SendEmailUtil {

    private static final Object LOCK = new Object();
    private static SendEmailUtil sInst = null;

    public static SendEmailUtil i() {
        synchronized (LOCK) {
            if (sInst == null) {
                sInst = new SendEmailUtil();
            }
            return sInst;
        }
    }

    private static final String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@" + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
    private final Pattern pattern = Pattern.compile(EMAIL_PATTERN);;
    
    private javax.mail.Session mailSession = null;
    private final String mMailFrom = "servicectmlab2019@gmail.com";
    private final String mMailFromPass = "t123456789!";
//    private String mMailTo = "";
    private final String port = "587";
    private final String mContent = "Hi #email,\n\n#link_verify \n\nGoodluck";
    private final String mSubject = "GoldInc registry account";
    private final String mSenderName = "GoldInc Service";
    
    private SendEmailUtil() {
    }
    
    public boolean isValid(String email){
        if(email==null){return false;}
        Matcher matcher = pattern.matcher(email);
	return matcher.matches();
    }
    
    public String getContentTemplate(){
        return mContent;
    }
    
    private void getJavaMailSession() {
        if(mailSession!=null){return;}
        final String FROM = mMailFrom;
        final String PASSWORD = mMailFromPass;
        Properties props = new Properties();
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.port", port);

        mailSession = javax.mail.Session.getInstance(props, new javax.mail.Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(FROM, PASSWORD);
            }
        });
    }

    public String sendEmail(String receivers, String content) {
        getJavaMailSession();
        String SUBJECT = mSubject;
        String CONTENT = content;
        String TO = receivers; // gmail1,gmail2...
        try {
            Message message = new MimeMessage(mailSession);
            message.setFrom(new InternetAddress(mMailFrom, mSenderName));
            message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(TO));
            message.setSubject(SUBJECT);
            message.setText(CONTENT);
            Transport.send(message);
            return "";
        } catch (Exception ex) {
            // print out log
            return Func.toString(ex);
        }
    }
}
