package com.ctmlab.game.manager;

import com.ctmlab.util.Utils;
import com.dp.db.Func;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import java.io.File;
import java.util.Random;

public class GlobalConfig {

    static {

    }

    // config port server
    // with the port, it will have the folder that contains all of files for loading server.
    public static final int PORT = 8080;

    // <editor-fold defaultstate="collapsed" desc="LOG FOLDER">
    public static final String prefix = isOnlineServer()? ("/mnt/external/") : ("/home/hhtri/Desktop/workspace/netbean/external/" + PORT + "/");
    // log text
    public static final String pathLogText = prefix + "log_text";
    // log bin
    public static final String pathLogBin = prefix + "log_bin";
    // bak
    public static final String pathBackup = prefix + "backup";
    // config
    public static final String pathConfig = prefix + "config";

    public static final Random GI_RANDOM = new Random();

    public static void initFolders(){
        File f = new File(pathLogText);
        if(!f.exists()){f.mkdirs();}
        f = new File(pathLogBin);
        if(!f.exists()){f.mkdirs();}
        f = new File(pathBackup);
        if(!f.exists()){f.mkdirs();}
        f = new File(pathConfig);
        if(!f.exists()){f.mkdirs();}
    }

    public static int rdInt(int min, int max){
        int rs = GI_RANDOM.nextInt(max);
        while(!(rs>min && rs<max)){
            rs = GI_RANDOM.nextInt(max);
        }
        return rs;
    }

    public static long rdLong(Long min, Long max){
        long rs = GI_RANDOM.nextInt(max.intValue());
        int maxVal = max.intValue() + 1;
        while(!(rs>min && rs<max)){
            rs = GI_RANDOM.nextInt(maxVal);
        }
        return rs;
    }

    public static boolean isOnlineServer(){
//        return false;
        return isOnlineServerTest();
    }

    public static boolean isOnlineServerTest(){
        if(Func.isLinuxServer()){
            String cmd = "uname -n";
            String rs = Func.exec(cmd);
            return !rs.contains("hhtri");
        } else{
            return false;
        }
    }

    public static void initConfigEmail(){
        JsonObject obj = new JsonObject();
        obj.addProperty("email", "servicectmlab2019@gmail.com");
        obj.addProperty("sender_name", "GoldInc Service");
        obj.addProperty("passwd", "t123456789!");
        obj.addProperty("port", 587);
        obj.addProperty("subject", "GoldInc registry account");
        obj.addProperty("content", "Hi #email,\n #link_verify \n Goodluck");
        String json = Func.pson(obj);
        String path = pathConfig + File.separator + "send_email.json";
        Utils.i().writeTextFile(path, json);
    }

    public static JsonObject getJsonObject(String json){
        try{
            return Func.gson.fromJson(json, JsonObject.class);
        } catch (Exception ex){
            return null;
        }
    }

    public static JsonArray getJsonArray(String json){
        try{
            return Func.gson.fromJson(json, JsonArray.class);
        } catch (Exception ex){
            return null;
        }
    }

    // </editor-fold>


}
