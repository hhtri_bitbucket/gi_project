/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ctmlab.game.manager;

import com.ctmlab.game.data.builder.GIUserAdminBuilder;
import com.ctmlab.game.data.model.GIUserAdmin;
import com.ctmlab.game.data.model.GoldIncUser;
import com.ctmlab.game.hadoop.LogManager;
import com.ctmlab.game.network.manager.WSGameDefine;
import com.ctmlab.util.Utils;
import com.dp.db.DB;
import com.dp.db.Func;
import com.google.gson.JsonObject;
import java.io.File;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
/**
 *
 * @author hhtri
 */
public class GoldIncAdminMng {
    public static final Object LOCK = new Object();
    private static GoldIncAdminMng inst;

    public static GoldIncAdminMng i() {
        synchronized (LOCK) {
            if (inst == null) {
                inst = new GoldIncAdminMng();
            }
            return inst;
        }
    }
    
    public final HashMap<String, GIUserAdmin> hashUserAdmin = new HashMap();
    
    private GoldIncAdminMng(){
        if(hashUserAdmin.isEmpty()){
            loadDBData();
            if(hashUserAdmin.isEmpty()){
                try {
                    GIUserAdmin ad = new GIUserAdminBuilder()
                            .setUsername("admin")
                            .setPassword(Utils.i().sha256("123"))
                            .setCreatedBy("")
                            .buildAndSave();
                    hashUserAdmin.put(ad.username, ad);
                    ad = new GIUserAdminBuilder()
                            .setUsername("admin1")
                            .setPassword(Utils.i().sha256("123"))
                            .setCreatedBy("admin")
                            .buildAndSave();
                    hashUserAdmin.put(ad.username, ad);
                    ad = new GIUserAdminBuilder()
                            .setUsername("admin2")
                            .setPassword(Utils.i().sha256("123"))
                            .setCreatedBy("admin")
                            .buildAndSave();
                    hashUserAdmin.put(ad.username, ad);
                    ad = new GIUserAdminBuilder()
                            .setUsername("admin3")
                            .setPassword(Utils.i().sha256("123"))
                            .setCreatedBy("admin")
                            .buildAndSave();
                    hashUserAdmin.put(ad.username, ad);
                } catch (SQLException ex) {}
            }
        }
    }
    
    private void loadDBData(){
        ArrayList<GIUserAdmin> lst;
        try {
            lst = DB.i().select(GIUserAdmin.class);
            lst.forEach((a) -> {
                hashUserAdmin.put(a.username, a);
            });
        } catch (SQLException ex) {
        }
    }
    
    public boolean loginAdmin(String username, String password){
        GIUserAdmin u = hashUserAdmin.get(username);
        if(u==null){return false;}
        boolean flag = (u.password.equals(Utils.i().sha256(password)));        
        if(!flag){return false;}
        u.lastlogin=System.currentTimeMillis();
        try{u.save(DB.i().con());}catch(SQLException ex){}
        return flag;
    }
    
    public String getUserAdmin(String username){
        return Func.pson(hashUserAdmin.get(username));        
    }
    
    public String getListUserAdmin(String username){
        if("admin".equals(username)){
            return Func.json(hashUserAdmin.values());
        } else{
            GIUserAdmin ad = hashUserAdmin.get(username);
            if(ad==null){
                return "[]";
            } else{
                return Func.json(new Object[]{ad});
            }
        }
    }
    
    public String[] getListUser(){
        return GoldIncUserMng.i().getUserIDs();
    }
    
    public JsonObject getUserInfo(String userID){
        JsonObject obj = new JsonObject();
        GoldIncUser user = GoldIncUserMng.i().getUser(userID);
        if(user!=null){
            user.getUserInfoAdmin(obj);
        }
        return obj;
    }
    
    public boolean updUserAdminInfo(String username, String newPass, String oldPass){
        GIUserAdmin u = hashUserAdmin.get(username);
        if(u==null){return false;}
        boolean flag = (u.password.equals(Utils.i().sha256(oldPass)));
        if(!flag){return false;}
        u.password=Utils.i().sha256(newPass);
        try{u.save(DB.i().con());}catch(SQLException ex){}
        return flag;
    }
    
    public String updUserInfo(String json, String username, String pass){
        try{
            JsonObject obj = Func.gson.fromJson(json, JsonObject.class);
            GoldIncUser user = GoldIncUserMng.i().getUser(obj.get("userID").getAsString());
            GIUserAdmin u = hashUserAdmin.get(username);
            if(user!=null){
                if(u.password.equals(Utils.i().sha256(pass))){
                    user.setUserInfoAdmin(obj);
                } else{
                    return "Password is wrong!";
                }
            } 
            return "";
        } catch(Exception ex){
            return Func.toString(ex);
        }
    }
    
    private File folder = new File(GlobalConfig.pathLogText + File.separator + "admin_log");
    public void writeLog(String data){
        if(!folder.exists()){folder.mkdirs();}
        LogManager.i().writeLog(folder, "logadmin.json", data);
    }
}
