/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ctmlab.game.manager;

import com.ctmlab.game.data.model.GameObjectMap;
import com.ctmlab.game.data.model.GoldIncUser;
import com.ctmlab.game.goldincenum.GI_SERVER_CMD;
import com.ctmlab.game.network.client.WSServiceClient;
import com.ctmlab.game.network.manager.WSGameDefine;
import com.ctmlab.game.network.manager.WSServiceServer;
import com.google.common.io.LittleEndianDataOutputStream;
import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Queue;

/**
 *
 * @author hhtri
 */
public class ZoneRepo {
    
    private static final Object LOCK = new Object();

    private final HashSet<String> mUserInZone = new HashSet<>();
    private final Queue<GameObjectMap> mQueue = new LinkedList();
    private short mID = -1;
    
    public ZoneRepo(short id) {
        mID = id;
    }

    public void addQueue(GameObjectMap obj) {
        synchronized (LOCK) {
            mQueue.add(obj);
            mUserInZone.add(obj.userID);
        }
    }
    
    public void addUserID(String id){
        synchronized(LOCK){
            mUserInZone.add(id);
        }
    }

    private byte[] getDataSpot() {
        synchronized(LOCK){
            if(mQueue.isEmpty()){return WSGameDefine.EMPTY_BYTES;}
        }
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        try (LittleEndianDataOutputStream os = new LittleEndianDataOutputStream(baos)) {
            HashSet<String> ls = new HashSet<>();
            synchronized(LOCK){
                os.writeShort(GI_SERVER_CMD.RES_UPDATE_SPOT.getCode());
                os.writeInt(mQueue.size());
                while(!mQueue.isEmpty()){
                    GameObjectMap o = mQueue.poll();
                    os.writeByte(o.mType);
                    o.writeData(os);
                    ls.add(o.userID);
                }
                os.writeInt(ls.size());
                for(String id:ls){
                    GoldIncUser u = GoldIncUserMng.i().getUser(id);
                    u.getPlayerProfile().getProfileBin(os, true);
                }
            }
            ls.clear();
        } catch (Exception ex) {
        }
        return baos.toByteArray();
    }

    public void sendToClients() {
        byte[] dataSend = getDataSpot();
        if(dataSend.length==0){
            return;
        }
        synchronized(LOCK){
            for(String id:mUserInZone){
                WSServiceClient client = WSServiceServer.i().getClientWithID(id);
                if(client!=null&&!client.getUserID().isEmpty()){
                    client.send(dataSend);
                }
            }
        }

    }

}
