/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ctmlab.game.manager;

import com.ctmlab.game.data.model.*;
import com.ctmlab.game.data.pojo.MineIndex;
import com.ctmlab.game.threads.ZoneManager;
import com.ctmlab.game.data.ctmon.CTMON;
import com.ctmlab.game.data.model.base.GameObjectBase;
import com.ctmlab.game.data.staticdata.CashConvert;
import com.ctmlab.game.data.staticdata.PointInt;
import com.ctmlab.game.data.staticdata.PointRiver;
import com.ctmlab.game.data.staticdata.gameobjectinfo.GameObjectInfoRepo;
import com.ctmlab.game.data.staticdata.Region;
import com.ctmlab.game.data.staticdata.WorldMap;
import com.ctmlab.game.data.staticdata.Zone;
import com.ctmlab.game.goldincenum.ENUM_UNIT_TYPE;
import com.ctmlab.game.hadoop.HadoopProvider;
import com.ctmlab.game.network.manager.WSGameDefine;
import com.ctmlab.game.threads.LoadingDataThread;
import com.ctmlab.util.Utils;
import com.dp.db.DB;
import com.dp.db.Func;
import com.google.common.io.LittleEndianDataInputStream;
import com.google.common.io.LittleEndianDataOutputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Random;

/**
 *
 * @author hhtri
 */
public class StaticDataMng {

    public static final Object LOCK = new Object();
    private static StaticDataMng inst;

    public static StaticDataMng i() {
        synchronized (LOCK) {
            if (inst == null) {
                inst = new StaticDataMng();
            }
            return inst;
        }
    }

    private final String mLogName = this.getClass().getSimpleName().toLowerCase();

    public WorldMap worldMap = new WorldMap();

    public WorldMap worldMapStatic = null;

    public CashConvert cashConvert = null;

    public HashMap<String, String> hashVersion = new HashMap();

    public GameObjectInfoRepo GOIRepo = null;

    private StaticDataMng() {
    }

    private void debug(String data) {
        HadoopProvider.i().writeLog(mLogName, Func.now() + "\n" + data);
    }

    /* start - using for version */
    // <editor-fold defaultstate="collapsed" desc="Hash version">
    public String setNewVersion(String key) {
        if(hashVersion.isEmpty()){
            try {
                loadHashVersion();
            }catch (Exception ex){}
        }
        String sTemp = hashVersion.get(key);

        String[] a = sTemp.split("\\.");
        int n1 = Func.parseInt(a[0], 1);
        int n2 = Func.parseInt(a[1], 0);
        int n3 = Func.parseInt(a[2], 1);
        n3++;
        if (n3 == 99) {
            n3 = 1;
            n2++;
            if (n2 == 9) {
                n2 = 0;
                n1++;
            }
        }
        String ver = String.format("%d.%d.%d", n1, n2, n3);
        synchronized (LOCK) {
            hashVersion.put(key, ver);
            Utils.i().writeTextFile(GlobalConfig.pathBackup + File.separator + "version.txt", Func.pson(hashVersion));
        }
        return ver;
    }

    // using for microservice function
    public boolean isNewConfig(HashMap<String, String> versionService) {
        if (versionService == null || versionService.isEmpty() || hashVersion.size() > versionService.size()) {
            return true;
        }
        boolean flag = false;
        for (String key : hashVersion.keySet()) {
            String main = hashVersion.get(key);
            String service = versionService.get(key);
            if (!main.equals(service)) {
                flag = true;
                break;
            }
        }
        return flag;
    }

    public void loadHashVersion() throws Exception{
        String path = GlobalConfig.pathBackup + File.separator + "version.txt";
        if (!(new File(path)).exists()) {
            hashVersion = new HashMap<>();
            hashVersion.put("worldmap", "1.0.1");
            hashVersion.put("gameobjectinfo", "1.0.1");
            Utils.i().writeTextFile(GlobalConfig.pathBackup + File.separator + "version.txt", Func.pson(hashVersion));
        } else {
            hashVersion = Func.gson.fromJson(Utils.i().readTextFile(path), HashMap.class);
        }
    }

    // </editor-fold>
    /* end - using for version */
 /* start - using for worldmap */
    // <editor-fold defaultstate="collapsed" desc="WorldMap">
    private void genStatic() {
        ArrayList<Integer> region_size = worldMap.metadata.get("region_size");
        ArrayList<Integer> zone_size = worldMap.metadata.get("zone_size");
        if (region_size == null || region_size.isEmpty()
                || zone_size == null || zone_size.isEmpty()) {
            return;
        }

        int rowRegion = 3;
        int colRegion = 3;

        int rowRegionZone = 10;
        int colRegionZone = 10;

        short idRegion = 0;
        short idZone = 0;
        for (int i = 0; i < rowRegion; i++) {
            for (int j = 0; j < colRegion; j++) {
                short regionID = idRegion++;
                int xPos = i * region_size.get(0);
                int yPos = j * region_size.get(1);
                Region region = new Region(regionID, xPos, yPos, "region_" + regionID);
                worldMap.regions.put(regionID, region);
            }
        }
        for (Region region : worldMap.regions.values()) {
            for (int i = 0; i < rowRegionZone; i++) {
                for (int j = 0; j < colRegionZone; j++) {
                    Zone z = new Zone(zone_size.get(0), zone_size.get(1));
                    short id = idZone++;
                    int xPos = i * zone_size.get(0);// + region.pos.get(0);
                    int yPos = j * zone_size.get(1);//+ region.pos.get(1);  
                    z.id = id;
                    z.name = "zone_" + id;
                    z.pos.add(xPos);
                    z.pos.add(yPos);
                    region.zones.put(id, z);

                    // gen Tree on zone
                    z.initOrderLayer();
                }
            }
        }
    }

    private void writeStaticMap() {
        try {
            String newVer = setNewVersion("worldmap");
            worldMap.version = newVer;
            worldMapStatic = worldMap.copyStatic();
            File f = null;
            String sRegion = "";
            for (Region r : worldMap.regions.values()) {
                String name = r.id + ".bin";
                f = new File(GlobalConfig.pathBackup + File.separator + name);
                writeRegion(r, f);
                if (!sRegion.isEmpty()) {
                    sRegion += ",";
                }
                sRegion += name;

            }
            String key = GlobalConfig.pathBackup + File.separator + "regions.txt";
            try (OutputStream os = new FileOutputStream(key)) {
                os.write(sRegion.getBytes(WSGameDefine.UTF_8));
                os.flush();
            }
            // write all map
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            LittleEndianDataOutputStream lsDos = new LittleEndianDataOutputStream(baos);
            worldMapStatic.writeData(lsDos);
            byte[] data = baos.toByteArray();
            baos.reset();

            f = new File(GlobalConfig.pathBackup + File.separator + "map.bin");
            if (!f.exists() && !f.getParentFile().exists()) {
                f.getParentFile().mkdirs();
            }

            try (OutputStream os = new FileOutputStream(f)) {
                os.write(data);
                os.flush();
            }
            cashConvert.saveData();
        } catch (IOException ex) {
        }
    }

    private void genMap() throws Exception {
        worldMap = new WorldMap();
        worldMap.name = "world map";
        worldMap.version = hashVersion.get("worldmap");
        ArrayList<Integer> regionSize = new ArrayList<>();
        ArrayList<Integer> zoneSize = new ArrayList<>();
        Collections.addAll(regionSize, 1000, 1000);
        Collections.addAll(zoneSize, 100, 100);
        worldMap.metadata.put("region_size", regionSize);
        worldMap.metadata.put("zone_size", zoneSize);

        genStatic();

        cashConvert = new CashConvert();
    }

    private void writeRegion(Region r, File f) {
        if (!f.exists() && !f.getParentFile().exists()) {
            f.getParentFile().mkdirs();
        }
        try (OutputStream os = new FileOutputStream(f)) {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            r.writeData(new LittleEndianDataOutputStream(os));
            byte[] data = baos.toByteArray();
            os.write(data);
            os.flush();
            baos.reset();
        } catch (Exception ex) {
        }
    }

    private void readMap() throws IOException {
        String prefix = GlobalConfig.pathBackup;
        byte[] buff = new byte[1024];
        int len = 0;
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        try (InputStream is = new FileInputStream(prefix + File.separator + "map.bin")) {
            while ((len = is.read(buff)) != -1) {
                baos.write(buff, 0, len);
            }
        }
        worldMap = new WorldMap(baos.toByteArray());
        baos.reset();
        try (InputStream is = new FileInputStream(new File(GlobalConfig.pathBackup + File.separator + "regions.txt"))) {
            while ((len = is.read(buff)) != -1) {
                baos.write(buff, 0, len);
            }
        }
        String sRegion = new String(baos.toByteArray(), WSGameDefine.UTF_8);
        for (String region : sRegion.split(",")) {
            baos.reset();
            try (InputStream is = new FileInputStream(new File(GlobalConfig.pathBackup + File.separator + region))) {
                while ((len = is.read(buff)) != -1) {
                    baos.write(buff, 0, len);
                }
            }
            Region regionTemp = new Region(baos.toByteArray());
            for (Region r : worldMap.regions.values()) {
                if (r.id == regionTemp.id) {
                    r.addZones(regionTemp.zones);
                    break;
                }
            }
        }
        worldMapStatic = worldMap.copyStatic();

        readCashConvert();
    }

    public void genMine(int numberInZone) {
        for (Region r : worldMap.regions.values()) {
            for (Zone z : r.zones.values()) {
                for (int i = 0; i < numberInZone; i++) {
                    GameObjectBase spot = z.genMine("", (byte) ENUM_UNIT_TYPE.MINE.getCode(), r.id);
                }
            }
        }
    }

    public void genOilMine(int numberInZone) {
        for (Region r : worldMap.regions.values()) {
            for (Zone z : r.zones.values()) {
                for (int i = 0; i < numberInZone; i++) {
                    GameObjectBase spot = z.genOilMine("", r.id);
                }
            }
        }

    }

    public void re_genWorldMap() {
        for (Region region : worldMap.regions.values()) {
            for (Zone z : region.zones.values()) {
                z.cleanupStaticObject();
                // gen Tree on zone
                z.genMountains(region.id, 50);
                z.genGround(region.id, 0);
                z.genWater(region.id, 0);
                z.genShore(region.id, 0);
                z.genTree(region.id, 50);
            }
        }
        writeStaticMap();
    }

    private Zone getZone(Region r, PointInt p) {
        Zone rs = null;
        for (Zone z : r.zones.values()) {
//            String s = String.format("[start, end, p]-[(%d, %d),(%d, %d),(%d, %d)]", z.x(), z.y(), z.x()+z.getWidth(), z.y()+z.getHeight(), p.x, p.y);
//            System.out.println(s);
            if (p.x >= z.x() && p.x < (z.x() + z.getWidth()) && p.y >= z.y() && p.y < (z.y() + z.getHeight())) {
                return z;
            }
        }
        return rs;
    }

    public void genInitMap(int n) {
        for (Region r : worldMap.regions.values()) {
            for (Zone z : r.zones.values()) {
                z.genTradeCenter("", (byte) ENUM_UNIT_TYPE.TRADE_CENTER.getCode(), r.id);
//                z.genRivers(r.id, 50);
                z.genMountains(r.id, 50);
                for (int i = 0; i < n/2; i++) {
                    z.genOilMine("", r.id);
                }
                for (int i = 0; i < n; i++) {
                    z.genMine("", (byte) ENUM_UNIT_TYPE.MINE.getCode(), r.id);
                }
                z.genTrees(r.id, 20);
                z.genGround(r.id, 0);
                z.genWater(r.id, 0);
                z.genShore(r.id, 0);
                z.genTree(z.id, 200);
                z.genBush(z.id, 30);
            }
        }

//        // one branch river
//        PointRiver node = null;
//        for (Region r : worldMap.regions.values()) {
//            Queue<PointRiver> queue = new LinkedList<>();
//            Zone z = null;
//            for (Zone i : r.zones.values()) {
//                if (i.y() == 900 && i.x()==500) {
//                    z = i;
//                    break;
//                }
//            }
//            PointInt p = new PointInt(50, 99);
//            System.out.println(String.format("[finding point:zone]-[(%d,%d):%d]", z.x() + 50, z.y() + 99, z.id));
//            System.out.println(String.format("[start zone:point]-[%d:(%d,%d)]", z.id, p.x, p.y));
////            int mode = -1;
//            int mode = 1;
//            ArrayList<PointRiver> rs = z.genRivers(r.id, p, mode, false);
//
//            if (!rs.isEmpty()) {                
//                System.out.println(String.format("[end zone:point]-[%d:(%d,%d)]", z.id, rs.get(0).outPos.x, rs.get(0).outPos.y));
//                PointRiver added = rs.get(0);
//                queue.add(added);
//            }
//            System.out.println("---------------------------------------------------------");
//            while (!queue.isEmpty()) {
//                node = queue.poll();
//                z = getZone(r, node.worldPos);                           
//                System.out.println(String.format("[finding point]-[(%d,%d)]", node.worldPos.x, node.worldPos.y));
//                if (z != null) {                           
//                    System.out.println(String.format("[ZONE]-[%d]", z.id));                    
//                    System.out.println(String.format("[start zone:point]-[%d:(%d,%d)]", z.id, node.startPos.x, node.startPos.y));
//                    rs = z.genRivers(r.id, node.startPos, mode, false);
//                    if (!rs.isEmpty()) {                        
//                        System.out.println(String.format("[end zone:point]-[%d:(%d,%d)]", z.id, rs.get(0).outPos.x, rs.get(0).outPos.y));
//                        PointRiver added = rs.get(0);
//                        queue.add(added);
//                    }
//                    System.out.println("---------------------------------------------------------");
//                } else{
//                    break;
//                }
//            }
//            break;
//        }
        
//        // two branch
//        PointRiver node = null;
//        for (Region r : worldMap.regions.values()) {
//            Queue<PointRiver> queue = new LinkedList<>();
//            Zone z = null;
//            for (Zone i : r.zones.values()) {
//                if (i.y() == 900 && i.x()==500) {
//                    z = i;
//                    break;
//                }
//            }
//            PointInt p = new PointInt(50, 99);
//            System.out.println(String.format("[finding point:zone]-[(%d,%d):%d]", z.x() + 50, z.y() + 99, z.id));
//            System.out.println(String.format("[start zone:point]-[%d:(%d,%d)]", z.id, p.x, p.y));
//            int mode = -1;
//            ArrayList<PointRiver> rs = z.genRivers(r.id, p, mode,true);
//
//            if (!rs.isEmpty()) {                
//                for(PointRiver pr:rs){
//                    queue.add(pr);
//                }
//            }
//            System.out.println("---------------------------------------------------------");
//            while (!queue.isEmpty()) {
//                node = queue.poll();
//                z = getZone(r, node.worldPos);                           
//                System.out.println(String.format("[finding point]-[(%d,%d)]", node.worldPos.x, node.worldPos.y));
//                if (z != null) {                           
//                    System.out.println(String.format("[ZONE]-[%d]", z.id));                    
//                    System.out.println(String.format("[start zone:point]-[%d:(%d,%d)]", z.id, node.startPos.x, node.startPos.y));
//                    rs = z.genRivers(r.id, node.startPos, mode, false);
//                    if (!rs.isEmpty()) {                        
//                        for(PointRiver pr:rs){
//                            queue.add(pr);
//                        }
//                    }
//                    System.out.println("---------------------------------------------------------");
//                } else{
//                    break;
//                }
//            }
//            break;
//        }

//        int rowRegionZone = 10;
//        int count = 0;
//        int countTest = 0;
//        Random rd = new Random();
//        PointInt pRoot = new PointInt(rd.nextInt(100), rd.nextInt(100));
//        for (Region r : worldMap.regions.values()) {  
//            Zone[] temp = r.zones.values().toArray(WSGameDefine.EMPTY_ZONE);
//            for(int i=temp.length-1; i>=0; i--){
//                Zone z = temp[i];
//                if(count==rowRegionZone){
//                    pRoot = new PointInt(pRoot.x, rd.nextInt(100));
//                    count = 0;
//                    countTest++;
//                    if(countTest == 2){
//                        break;
//                    }
//                }
//                count++;
//                z.genRivers(r.id, pRoot, false);
//            }
//            break;
//        }
        writeStaticMap();
    }

    public void addGameObjectItem(GameObjectMap item) {
        LoadingDataThread.i().setTimeLoading(this.getClass().getSimpleName(), System.currentTimeMillis());
//        item.refInfo();
//        Region r = worldMap.getRegion(item.id_r);
//        if (r != null) {
//            Zone z = r.getZone(item.id_z);
//            if (z != null) {
//                z.addGameObject(item);
//                // add info to player profile
//                if (!item.userID.isEmpty()) {
//                    GoldIncUser user = GoldIncUserMng.i().getUser(item.userID);
//                    if (user != null) {
//                        if (item instanceof HomeBase) {
//                            user.setHomeBase(item);
//                        } else if (item instanceof Mine) {
//                            user.addMineFromDB(item);
//                        }
//                        user.setPlayerProfile();
//                        ZoneManager.i().addUserInZone(item.id_z, item.userID);
//                    }
//                }
//            }
//        }
        item.refInfo();
        Zone z = worldMap.getZone(item.getIndex());
        if (z != null) {
            z.addGameObject(item);
            // add info to player profile
            if (!item.userID.isEmpty()) {
                GoldIncUser user = GoldIncUserMng.i().getUser(item.userID);
                if (user != null) {
                    if (item instanceof HomeBase) {user.setHomeBase(item);}
                    user.setPlayerProfile();
                    ZoneManager.i().addUserInZone(item.id_z, item.userID);
                }
            }
        }
    }

    public ArrayList<Integer> getRegionSize() {
        if (worldMap != null && worldMap.metadata != null) {
            worldMap.metadata.get("region_size");
        }
        return null;
    }

    public ArrayList<Integer> getZoneSize() {
        if (worldMap != null && worldMap.metadata != null) {
            return worldMap.metadata.get("zone_size");
        }
        return null;
    }

    public GameObjectMap getSpotRandom(String userID) throws Exception {
        Random rd = new Random();
        short idx = (short) rd.nextInt(worldMap.regions.size());
        Region r = worldMap.regions.get(idx);
        idx = (short) rd.nextInt(r.zones.size());
        Zone z = r.getRandomZone(idx);
        GameObjectMap spot = z.genHomeBase(userID, (byte) ENUM_UNIT_TYPE.HOME_BASE.getCode(), r.id);
        if (spot != null) {
            ServiceMng.i().resUpdateSpot(spot, userID);
        }
        return spot;
    }

    public void loadStaticMap() throws Exception {
        String path = GlobalConfig.pathBackup;
        if (!(new File(path + File.separator + "map.bin").exists())) {
            System.out.println("Generating New Object");
            try {
                String[] tables = new String[]{
                    "exploration", "gamemessage", "giuseradmin", "goldincuser",
                    "homebase", "building", "mine", "oilmine", "registryaccount", "socialalliance",
                    "socialchat", "tradecentermap", "transportation"
                };
                for (String table : tables) {
                    DB.i().exec(String.format("Delete from %s;", table));
                }
                initDefault();
                genInitMap(30);
                GoldIncUserMng.i().initDefault();
            } catch (IOException | SQLException ex) {
                debug(Func.toString(ex));
            }
        } else {
            System.out.println("Reading Old Object");
            readMap();
        }
    }

    public void loadClaimNotOwner(){
        MineIndex idx = new MineIndex();
        try {
            DB.i().select(Claim.class, (t) -> {
                if (t.userID == null || t.userID.isEmpty()) {
                    idx.id = t.mineID;
                    idx.id_z = t.id_z;
                    GameObjectMap o = worldMap.getItemMap(idx);
                    if (o != null) {
                        ((Mine) o).refClaim(t);
                    }
                }
            });
        } catch (Exception ex){}
    }

    public void loadDB() throws Exception {
        DB.i().select(Mine.class, (t) -> {
//            t.parse();
            addGameObjectItem(t);
        });
        DB.i().select(OilMine.class, (t) -> {
//            t.parse();
            addGameObjectItem(t);
        });
        DB.i().select(HomeBase.class, (t) -> {
            addGameObjectItem(t);
        });
        DB.i().select(TradeCenterMap.class, (t) -> {
            addGameObjectItem(t);
        });
    }

    // </editor-fold>
    /* end - using for worldmap */
 /* start - using for gameobjectinfo */
    // <editor-fold defaultstate="collapsed" desc="Gameobjectinfo">
    public GameObjectInfoRepo getRepoGameObjectInfo(){

        if(GOIRepo!=null){return GOIRepo;}
        GOIRepo = new GameObjectInfoRepo();
        try {
            String path = GlobalConfig.pathBackup + File.separator + "gameobjectinfo.bin";
            System.out.println(path);
            if (!(new File(path)).exists()) {
                GOIRepo.init(hashVersion.get("gameobjectinfo"));
            } else {
                GOIRepo.readGameObjectInfo(path);
            }
        } catch (Exception ex){

        }
        return GOIRepo;
    }

    public void readGameObjectInfo(String pathBin){
        getRepoGameObjectInfo().readGameObjectInfo(pathBin);
    }

    public void writeGameObjectInfo(String pathBin){
        getRepoGameObjectInfo().writeGameObjectInfo(pathBin);
    }

    // </editor-fold>
    /* end - using for gameobjectinfo */
 /* start - using for CashConvert */
    private void readCashConvert() {

        cashConvert = new CashConvert();

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        byte[] buff = new byte[1024];
        int len = 0;
        try (InputStream is = new FileInputStream(new File(GlobalConfig.pathBackup + File.separator + "cashconvert.bin"))) {
            while ((len = is.read(buff)) != -1) {
                baos.write(buff, 0, len);
            }
        } catch (Exception ex) {
            return;
        }
        byte[] data = baos.toByteArray();
        if (data.length > 0) {
            ByteArrayInputStream bais = new ByteArrayInputStream(data);
            try (LittleEndianDataInputStream ledis = new LittleEndianDataInputStream(bais)) {
                CashConvert _cashConvert = CTMON.i().fromStream(ledis, CashConvert.class);
                cashConvert.syncCashConvert(_cashConvert);
                cashConvert = _cashConvert;
                cashConvert.getDataStream();
            } catch (Exception ex) {
            }
        }
    }

    /* end - using for CashConvert */
    public void init(){
        try {
            loadHashVersion();
            getRepoGameObjectInfo();
            loadStaticMap();
            loadDB();
        } catch (Exception ex) {
            System.out.println(String.format("[Log in application - %s]: Exception in StaticDataMng:init....", Func.now()));
            debug(Func.toString(ex));
        }
    }

    /* start - using for testing */
    public void initDefault() throws IOException {
        try {
            loadHashVersion();
            setNewVersion("worldmap");
            setNewVersion("gameobjectinfo");
            Utils.i().writeTextFile(GlobalConfig.pathBackup + File.separator + "version.txt", Func.pson(hashVersion));
            getRepoGameObjectInfo().init(hashVersion.get("gameobjectinfo"));
            genMap();
        } catch (Exception ex) {
            debug(Func.toString(ex));
        }
    }

    public GameObjectMap getSpot(String userID, short regionID, short zoneID) throws Exception {
        Random rd = new Random();
        Region r = worldMap.regions.get(regionID);
        short idx = 0;
        Zone z = r.getRandomZone(idx);
        GameObjectMap spot = z.genHomeBase(userID, (byte) ENUM_UNIT_TYPE.HOME_BASE.getCode(), r.id);
        if (spot != null) {
//            System.out.println(String.format("%d,%d,%d,%d,%d",spot.id_r, spot.id_z, spot.id, spot.xPos, spot.yPos));
        }
        return spot;
    }

    /* end - using for testing */
}
