/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ctmlab.game.hadoop;

import com.ctmlab.game.data.ctmon.CTMON;
import com.ctmlab.game.goldincenum.CTMON_FieldType;
import com.ctmlab.game.network.manager.WSGameDefine;
import com.dp.db.DBData;
import com.dp.db.DBDataListener;
import com.dp.db.DBDataPostgres;
import com.dp.db.Func;
import com.google.common.io.LittleEndianDataInputStream;
import com.google.common.io.LittleEndianDataOutputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FSDataInputStream;
import org.apache.hadoop.fs.FSDataOutputStream;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;

/**
 *
 * @author hhtri
 */
public class HadoopProvider {
    public static final Object LOCK = new Object();
    public static final FSDataOutputStream[] EMPTY_OUTPUT = new FSDataOutputStream[0];
    private static HadoopProvider inst;

    public static HadoopProvider i() {
        synchronized (LOCK) {
            if (inst == null) {inst = new HadoopProvider();}
            return inst;
        }
    }
    
    protected Configuration mConf;
    protected FileSystem mFS;
    protected Short mRepNum = 1;
    private final String mLogName = this.getClass().getSimpleName().toLowerCase();
    private String mCurDate = Func.todayServer();
    private final HashMap<String, FSDataOutputStream> mHashOutput = new HashMap();
    private final String HD_FOLDER = "goldinc";
    private boolean mFlagConnected = false;
        
    private HadoopProvider(){}
    
    public void init() {
        init("hdfs://localhost:54310");
    }
    
    public void init(String url) {
        mConf = new Configuration();
        mConf.set("fs.defaultFS", url);
        try {
            if(mFS!=null){
                mFS.close();
                mFS = null;
            }
            mFS = FileSystem.get(mConf);
            mFlagConnected = true;
        } catch (Exception ex) {
            mFlagConnected = false;
            LogManager.i().writeLog(mLogName, Func.now()
                    + "\nMethod init(String url)\n" + Func.toString(ex));
        }
    }
    
    public boolean isConnected(){
        return mFlagConnected;
    }
    
    private void writeObjectBase(FSDataOutputStream os, Object o) throws IOException {
        CTMON_FieldType type = CTMON_FieldType.get(o);
        os.write(type.code);
        switch (type) {
            case BYTE: {
                os.writeByte((byte) o);
                break;
            }
            case BOOLEAN: {
                os.writeBoolean((boolean) o);
                break;
            }
            case SHORT: {
                os.writeShort((short) o);
                break;
            }
            case INTEGER: {
                os.writeInt((int) o);
                break;
            }
            case LONG: {
                os.writeLong((long) o);
                break;
            }
            case STRING: {
                byte[] data = String.valueOf(o).getBytes(WSGameDefine.UTF_8);
                os.writeInt(data.length);
                os.write(data);
                break;
            }
            case FLOAT: {
                os.writeFloat((float) o);
                break;
            }
            case DOUBLE: {
                os.writeDouble((double) o);
                break;
            }
            case DATE: {
                os.writeLong(((java.util.Date) o).getTime());
                break;
            }
            case BYTE_ARRAY: {
                byte[] data = (byte[]) o;
                os.writeInt(data.length);
                os.write(data);
                break;
            }
            case OBJECT: {
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                LittleEndianDataOutputStream ledos = new LittleEndianDataOutputStream(baos);
                ledos.writeUTF(o.getClass().getName());
                CTMON.i().toStream(ledos, o);
                byte[] dataCTMON = baos.toByteArray();
                os.writeInt(dataCTMON.length);
                os.write(dataCTMON);
                break;
            }
        }
        os.hflush();
    }
    
    private Object readObjectBase(FSDataInputStream is) throws Exception {
        CTMON_FieldType type = CTMON_FieldType.get(is.read());
        switch (type) {
            case BYTE: {
                return is.readByte();
            }
            case BOOLEAN: {
                return is.readBoolean();
            }
            case SHORT: {
                return is.readShort();
            }
            case INTEGER: {
                return is.readInt();
            }
            case LONG: {
                return is.readLong();
            }
            case STRING: {
                byte[] data = new byte[is.readInt()];
                is.read(data);
                return new String(data, WSGameDefine.UTF_8);
            }
            case FLOAT: {
                return is.readFloat();
            }
            case DOUBLE: {
                return is.readDouble();
            }
            case DATE: {
                return new Date(is.readLong());
            }
            case BYTE_ARRAY: {
                byte[] data = new byte[is.readInt()];
                is.read(data);
                return data;
            }
            case OBJECT: {
                byte[] data = new byte[is.readInt()];
                is.read(data);
                ByteArrayInputStream bais = new ByteArrayInputStream(data);
                LittleEndianDataInputStream ledis = new LittleEndianDataInputStream(bais);
                Class clz = Class.forName(ledis.readUTF());
                return CTMON.i().fromStream(ledis, clz);
            }
            default:{
                return null;
            }
        }
    }    
   
    public void closeAll(){
        
        FSDataOutputStream[] temp;
        synchronized(mHashOutput){
            temp = mHashOutput.values().toArray(EMPTY_OUTPUT);
        }
        
        for(FSDataOutputStream fs:temp){
            if(fs!=null){                
                try {
                    fs.close();
                } catch (IOException ex) {
                    LogManager.i().writeLog(mLogName, Func.now()
                    + "\nMethod closeAll()\n" + Func.toString(ex));
                }
            }
            fs = null;
        }
        
        synchronized(mHashOutput){
            mHashOutput.clear();
        }
    }
    
    public String listFiles(String hdfsPath) {
        try {
            StringBuilder sb = new StringBuilder();
            ArrayList<Path> lsFolder = new ArrayList();
            lsFolder.add(new Path(hdfsPath));
            FileStatus[] fileStatus = null;
            Path pathTemp = null;
            while (!lsFolder.isEmpty()) {
                pathTemp = lsFolder.get(0);
                lsFolder.remove(0);
                fileStatus = mFS.listStatus(pathTemp);
                for (FileStatus status : fileStatus) {
                    if(status.isDirectory()){
                        lsFolder.add(status.getPath());
                    } else{
                        if(sb.length()>0){sb.append("\n");}
                        sb.append(status.getPath().toString());
                    }
                }
            }
            return sb.toString();            
        } catch (IOException ex) {
            LogManager.i().writeLog(mLogName, Func.now()
                    + "\nMethod listFiles(String hdfsPath)\n" + Func.toString(ex));
            return "";
        }
    }
    
    public void writeDataDemo(String file, byte[] data){
        try {
            FileSystem fSys = mFS;
            String filePath = file;
            Path hdfsPath = new Path(filePath);
            FSDataOutputStream fos = mHashOutput.get(file);
            if(fos==null){
                if (fSys.exists(hdfsPath)) {
                    fos = fSys.append(hdfsPath);       
                } else {
                    //create file and write content to file
                    if(!fSys.exists(hdfsPath.getParent())){
                        fSys.mkdirs(hdfsPath.getParent());
                        fSys.setReplication(hdfsPath.getParent(), mRepNum);
                    }
                    fos = fSys.create(hdfsPath, mRepNum);
                }                
                mHashOutput.put(file, fos);
            }
            writeObjectBase(fos, data);
            
        } catch (IOException | IllegalArgumentException ex) {
            LogManager.i().writeLog(mLogName, Func.now()
                    + "\nMethod writeDataDemo(String file, byte[] data)\n" + Func.toString(ex));
        }
    }
    
    public void writeLog(String filename, Object data){
        
        if(data == null){
        }else if(!mFlagConnected){
            LogManager.i().writeLog(filename, String.valueOf(data));
        } else{
            try {
                FileSystem fSys = mFS;
                FSDataOutputStream fos = null;
                int retry = 5;
                do {
                    synchronized (LOCK) {
                        String strDate = Func.todayServer();
                        String filePath = HD_FOLDER + File.separator + Func.todayServer() + "_" + filename;                    
                        if (!mCurDate.equals(strDate)) {
                            //close all handle last day
                            closeAll();
                            mCurDate = strDate;
                        }
                        fos = mHashOutput.get(filePath);
                        if (fos == null) {
                            Path hdfsPath = new Path(filePath);
                            if (fSys.exists(hdfsPath)) {
                                fos = fSys.append(hdfsPath);
                            } else {
                                if(!fSys.exists(hdfsPath.getParent())){
                                    fSys.mkdirs(hdfsPath.getParent());
                                    fSys.setReplication(hdfsPath.getParent(), mRepNum);
                                }
                                fos = fSys.create(hdfsPath, mRepNum);
                            }
                            mHashOutput.put(filePath, fos);
                        }
                    }
                    synchronized (fos) {
                        writeObjectBase(fos, data);
                        break;
                    }
                } while (--retry > 0);
                
            } catch (IOException | IllegalArgumentException ex) {
                LogManager.i().writeLog(mLogName, Func.now()
                    + "\nwriteLog(String filename, Object data)\n"
                    + Func.toString(ex));
            }
        }
    }
    
    public ArrayList<Object> readLog(String filename, LogObjectListener listener){
        ArrayList<Object> rs = new ArrayList<>();
        Path path = new Path(filename);
        //create file and write content to file
        try (FSDataInputStream fis = mFS.open(path, mRepNum)){
            while(true){
                Object o = readObjectBase(fis);
                if(listener!=null){
                    listener.onLogObject(o);
                } else{
                    rs.add(o);  
                }
            }
        } catch (Exception ex) {
            LogManager.i().writeLog(mLogName, Func.now()
                    + "\nreadLog(String filename)\n"
                    + Func.toString(ex));
        }        
        return rs;
    }
    
    public byte[] readData(String file, LogObjectListener listener){
        byte[] rs = new byte[0];
        Path path = new Path(file);
        try {
            //create file and write content to file
            FSDataInputStream fis = mFS.open(path, mRepNum);
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            byte[] buff = new byte[1024];
            int len = 0;
            while ((len = fis.read(buff)) != -1) {
                baos.write(buff, 0, len);
            }
            fis.close();
            
            
            if(listener!=null){
                listener.onLogBytes(baos.toByteArray());
            } else{
                rs = baos.toByteArray();
            }
            baos.reset();
            baos = null;
            
        } catch (IOException ex) {
            LogManager.i().writeLog(mLogName, Func.now()
                    + "\nreadData(String file)\n"
                    + Func.toString(ex));
        }
        return rs;
    }
    
    public void deleteFile(String hdfsPath){
        if(mFlagConnected){
            Path path = new Path(hdfsPath);
            try {
                mFS.delete(path, true);
            } catch (IOException ex) {
                LogManager.i().writeLog(mLogName, Func.now()
                        + "\nreadData(String file)\n"
                        + Func.toString(ex));
            }
        }
    }
    
    public void save(DBDataPostgres data){
        if(data == null){}
        else if(!mFlagConnected){
            LogManager.i().saveHD(data);
        } else{
            try {
                FileSystem fSys = mFS;
                FSDataOutputStream fos = null;              
                int retry = 5;
                do {
                    Path hdfsPath = new Path(HD_FOLDER);
                    //create new folder main                        
                    if (!fSys.exists(hdfsPath)) {
                        fSys.mkdirs(hdfsPath);
                    }
                    synchronized (LOCK) {
                        String strDate = Func.todayServer();
                        String filePath = HD_FOLDER + File.separator + Func.todayServer() + "_" + data.table();                        
                        if (!mCurDate.equals(strDate)) {
                            //close all handle last day
                            closeAll();
                            mCurDate = strDate;
                        }
                        fos = mHashOutput.get(filePath);
                        if (fos == null) {
                            hdfsPath = new Path(filePath);
                            if (fSys.exists(hdfsPath)) {
                                fos = fSys.append(hdfsPath);
                            } else {
                                if(!fSys.exists(hdfsPath.getParent())){
                                    fSys.mkdirs(hdfsPath.getParent());
                                    fSys.setReplication(hdfsPath.getParent(), mRepNum);
                                }
                                fos = fSys.create(hdfsPath, mRepNum);
                            }
                            mHashOutput.put(filePath, fos);
                        }
                    }
                    if (fos != null) {
                        synchronized (fos) {
                            ByteArrayOutputStream baos = new ByteArrayOutputStream();
                            LittleEndianDataOutputStream os;
                            os = new LittleEndianDataOutputStream(baos);
                            CTMON.i().toStream(os, data);
                            os.flush();
                            byte[] dataCTMON = baos.toByteArray();
                            synchronized (fos) {
                                fos.writeInt(dataCTMON.length);
                                fos.write(dataCTMON);
                                fos.hflush();
                                break;
                            }
                        }
                    }
                } while (--retry > 0);                
            } catch (IOException | IllegalArgumentException ex) {
                LogManager.i().writeLog(mLogName, Func.now()+"\n"+Func.toString(ex));
                LogManager.i().saveHDErr(data);
            }
        }
    }
    
    public <T extends DBData> void select(Class<T> classOfT, long start, long end, DBDataListener<T> listener) {
        if(mFlagConnected){
            LogManager.i().select(classOfT, start, end, listener);
        }
        if(end<start){return;}
        long timeOfDay = 24*60*60*1000;
        long iTime = start;
        while(iTime<end){
            select(classOfT, iTime, listener);
            iTime+=timeOfDay;
        }                
    }
    
    public <T extends DBData> void select(Class<T> classOfT, long time, DBDataListener<T> listener) {

        if(!mFlagConnected){
            LogManager.i().select(classOfT, time, listener);
        }
        String pathFile = HD_FOLDER + File.separator + Func.getSDateUTC(time)
                + "_" + classOfT.getSimpleName().toLowerCase();
        Path path = new Path(pathFile);
        try (FSDataInputStream fis = mFS.open(path, mRepNum)){
            while (true) {
                int len = fis.readInt();
                byte[] dataCTMON = new byte[len];
                fis.read(dataCTMON, 0, len);

                ByteArrayInputStream bais = new ByteArrayInputStream(dataCTMON);
                LittleEndianDataInputStream ledis = new LittleEndianDataInputStream(bais);
                T data = CTMON.i().fromStream(ledis, classOfT);
                if(listener!=null){
                    listener.onItem(data);
                }
            }
        } catch (Exception ex) {
            LogManager.i().writeLog(mLogName, Func.now()
                    + "\n" + Func.toString(ex));
        }   
    }
}
