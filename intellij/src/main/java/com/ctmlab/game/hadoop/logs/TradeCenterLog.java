/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ctmlab.game.hadoop.logs;

import com.ctmlab.game.network.manager.WSGameDefine;
import com.ctmlab.util.Utils;
import com.google.common.io.LittleEndianDataInputStream;
import com.google.common.io.LittleEndianDataOutputStream;
import java.io.IOException;
import java.util.Map.Entry;
import java.util.TreeMap;

/**
 *
 * @author hhtri
 */
public class TradeCenterLog {
        
    public String userid = "";
    public short zoneid = -1;
    public byte action = -1;
    public TreeMap<Byte, Long> lstResources = new TreeMap<>();
    public byte resultTotal = WSGameDefine.ADD_RES_FAIL;
    public TreeMap<Byte,Long> lstResult = new TreeMap<>();
    
    public TradeCenterLog(){}
    
    public TradeCenterLog setUserid(String userid) {
        this.userid = userid;
        return this;
    }
    public TradeCenterLog setZoneID(short zoneid) {
        this.zoneid = zoneid;
        return this;
    }
    public TradeCenterLog setAction(byte action) {
        this.action = action;
        return this;
    }
    public TradeCenterLog setListResources(TreeMap<Byte, Long> lstResources) {
        this.lstResources = lstResources;
        return this;
    }
    public TradeCenterLog setResultTotal(byte resultTotal) {
        this.resultTotal = resultTotal;
        return this;
    }
    public TradeCenterLog setListResult(TreeMap<Byte,Long> lstResult) {
        this.lstResult = lstResult;
        return this;
    }
    
    public void writeData(LittleEndianDataOutputStream os) throws IOException{
        Utils.i().writeBigString(os, userid);
        os.writeShort(zoneid);
        os.writeByte(action);
        os.writeInt(lstResources.size());
        for(Entry<Byte, Long> e:lstResources.entrySet()){
            os.writeByte(e.getKey());
            os.writeLong(e.getValue());
        }
        os.writeByte(resultTotal);
        os.writeInt(lstResult.size());
        for(Entry<Byte, Long> e:lstResult.entrySet()){
            os.writeByte(e.getKey());
            os.writeLong(e.getValue());
        }
    }
    
    public void readData(LittleEndianDataInputStream is) throws IOException{
        userid = Utils.i().readBigString(is);
        zoneid = is.readShort();
        action = is.readByte();
        int len = is.readInt();
        int i = 0;
        lstResources = new TreeMap<>();
        while(i++<len){
            lstResources.put(is.readByte(), is.readLong());
        }
        resultTotal = is.readByte();
        len = is.readInt();
        i=0;
        lstResult = new TreeMap<>();
        while(i++<len){
            lstResult.put(is.readByte(), is.readLong());
        }
    }
}
