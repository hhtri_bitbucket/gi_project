/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ctmlab.game.hadoop.logs;

import com.ctmlab.game.data.pojo.TradeTransaction;
import com.ctmlab.game.hadoop.LogManager;
import com.google.common.io.LittleEndianDataOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

/**
 *
 * @author hhtri
 */
public class TradeCenterLogMng {
    public static final Object LOCK = new Object();
        
    private static TradeCenterLogMng inst;

    public static TradeCenterLogMng i() {
        synchronized (LOCK) {
            if (inst == null) {inst = new TradeCenterLogMng();}
            return inst;
        }
    }
    
    private final ByteArrayOutputStream baos = new ByteArrayOutputStream();
    
    private TradeCenterLogMng(){}
    
    public void writeLogBuy(String userid, short zoneid, TradeTransaction transaction){
        byte[] data = null;
        synchronized(LOCK){
            baos.reset();
            try(LittleEndianDataOutputStream os = new LittleEndianDataOutputStream(baos)){
                new TradeCenterLog()
                        .setUserid(userid)
                        .setZoneID(zoneid)
                        .setAction((byte)TradeTransaction.ACTION_BUY)
                        .setListResources(transaction.getLstTrade())
//                        .setResultTotal((byte)(transaction.getResource() !=null?WSGameDefine.ADD_RES_SUCCESS:WSGameDefine.ADD_RES_FAIL))
                        .setListResult(transaction.getLstResult())
                        .writeData(os);
                data = baos.toByteArray();
            } catch (IOException ex) {
                
            }
        }
        LogManager.i().writeLog(TradeCenterLog.class.getSimpleName().toLowerCase(), data);
    }
    
    public void writeLogSell(String userid, short zoneid, TradeTransaction transaction){
        byte[] data = null;
        synchronized(LOCK){
            baos.reset();
            try(LittleEndianDataOutputStream os = new LittleEndianDataOutputStream(baos)){
                new TradeCenterLog()
                        .setUserid(userid)
                        .setZoneID(zoneid)
                        .setAction((byte)TradeTransaction.ACTION_SELL)
                        .setListResources(transaction.getLstTrade())
//                        .setResultTotal((byte)(transaction.getResource() !=null?WSGameDefine.ADD_RES_SUCCESS:WSGameDefine.ADD_RES_FAIL))
                        .setListResult(transaction.getLstResult())
                        .writeData(os);
                data = baos.toByteArray();
            } catch (IOException ex) {
                
            }
        }
        LogManager.i().writeLog(TradeCenterLog.class.getSimpleName().toLowerCase(), data);
    }
}
