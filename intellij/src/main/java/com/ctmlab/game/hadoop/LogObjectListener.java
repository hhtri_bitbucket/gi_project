/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ctmlab.game.hadoop;

/**
 *
 * @author hhtri
 */
public interface LogObjectListener {
    void onLogObject(Object data);
    void onLogBytes(byte[] data);
}
