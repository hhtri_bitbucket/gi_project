/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ctmlab.game.hadoop;

import com.ctmlab.game.goldincenum.CTMON_FieldType;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonPrimitive;
import java.io.DataOutputStream;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.HashMap;

/**
 *
 * @author hhtri
 */
//public class HDJSObject extends HashMap<String, Object> {
//
//    protected static final Gson GSON_BUILDER = new GsonBuilder().registerTypeAdapter(HDJSObject.class, new JsonDeserializer<HDJSObject>() {
//        @Override
//        public HDJSObject deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
//            Object o = deserializeObject(json);
//            return (o instanceof HDJSObject) ? (HDJSObject) o : null;
//        }
//
//        public Object deserializeObject(JsonElement json) {
//            if (json.isJsonObject()) {
//                HDJSObject item = new HDJSObject();
//                ((JsonObject) json).entrySet().forEach((entry) -> {
//                    JsonElement v = entry.getValue();
//                    item.put(entry.getKey(), deserializeObject(v));
//                });
//                return item;
//            } else if (json.isJsonArray()) {
//                HDJSArray arr = new HDJSArray();
//                for (JsonElement e : json.getAsJsonArray()) {
////                    arr.add(deserializeObject(e));
//                }
//                return arr;
//            } else if (json.isJsonPrimitive()) {
//                JsonPrimitive jp = json.getAsJsonPrimitive();
//                if (jp.isString()) {
//                    return jp.getAsString();
//                } else if (jp.isBoolean()) {
//                    return jp.getAsBoolean();
//                } else if (jp.isNumber()) {
//                    Number n = jp.getAsNumber();
//                    Double d = n.doubleValue();
//                    if ((d == Math.floor(d)) && !Double.isInfinite(d)) {
//                        return n.longValue();
//                    } else {
//                        return d;
//                    }
//                }
//            }
//            return null;
//        }
//    }).create();
//    
//    public static String[] EMPTY_KEYS = new String[0];
//    public static Object[] EMPTY_VALUES = new Object[0];
//    public static Entry<String, Object>[] EMPTY_ENTRY = new Entry[0];
//    
//    public HDJSObject() {}
//    
//    public String[] safeKeys() {
//        synchronized (this) {
//            return super.keySet().toArray(EMPTY_KEYS);
//        }
//    }
//
//    public Object[] safeValues() {
//        synchronized (this) {
//            return super.values().toArray(EMPTY_VALUES);
//        }
//    }
//    
//    public Entry<String, Object>[] safeEntries() {
//        synchronized (this) {
//            return super.entrySet().toArray(EMPTY_ENTRY);
//        }
//    }
//
//    
//    public HDJSObject getObject(String json){
//        try{
//            HDJSObject item = GSON_BUILDER.fromJson(json, HDJSObject.class);
//            return item;
//        } catch(Exception ex){
//            
//        }
//        return null;
//    }
//    
////    NULL(null, null, 0),
////    OBJECT(null, null, 1),
////    STRING(String.class, StringBuilder.class, 2),
////    BYTE(byte.class, Byte.class, 3),
////    SHORT(short.class, Short.class, 4),
////    INTEGER(int.class, Integer.class, 5),
////    LONG(long.class, Long.class, 6),
////    FLOAT(float.class, Float.class, 7),
////    DOUBLE(double.class, Double.class, 8),
////    BOOLEAN(boolean.class, Boolean.class, 9),
////    DATE(Date.class, java.sql.Timestamp.class, 10),
////    BYTE_ARRAY(byte[].class, Byte[].class, 11),
////    ARRAYLIST(null, ArrayList.class, 12),
////    HASHMAP(null, HashMap.class, 13),
////    ENUM(null, null, 14),
////    BYTE_BUFFER(null, ByteBuffer.class, 15),;
//    
//    public void writeObject(Object o, DataOutputStream os) throws IOException {
//        CTMON_FieldType type = CTMON_FieldType.get(o);
//        switch (type) {
//            case OBJECT:{
//                break;
//            }
//        }
//            
//    }
//    
//    public void write(DataOutputStream os) throws IOException {
//        os.writeInt(size());
//        for (Entry<String, Object> entry : safeEntries()) {
//            writeObject(entry.getKey(), os);
//            writeObject(entry.getValue(), os);
//        }
//    }
//}
