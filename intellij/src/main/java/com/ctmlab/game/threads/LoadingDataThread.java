/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ctmlab.game.threads;

import com.ctmlab.game.manager.GoldIncMng;
import com.ctmlab.game.manager.GoldIncUserMng;
import com.ctmlab.game.manager.StaticDataMng;
import com.ctmlab.game.network.manager.WSGameDefine;
import java.util.HashMap;
import java.util.Map.Entry;

/**
 *
 * @author hhtri
 */
public class LoadingDataThread extends Thread {

    private static final Object LOCK = new Object();
    private static final Object LOCK_WAIT = new Object();
    private static LoadingDataThread inst;

    public static LoadingDataThread i() {
        synchronized (LOCK) {
            if (inst == null) {
                inst = new LoadingDataThread();
            }
            return inst;
        }
    }    
    
    private final HashMap<String, Long> mItems = new HashMap<>();
    private boolean mRunning = false;
    
    public int flagSyncCliaim = -1;
    
    public void setTimeLoading(String keyClass, long time){
        if(flagSyncCliaim==-1){
            flagSyncCliaim = 0;
        }
        synchronized(LOCK){
            mItems.put(keyClass, time);
        }
    }
    
    private void finish(String cls){
        switch(cls){
            case "GoldIncUserMng":{
                new Thread(()->{
                    GoldIncUserMng.i().syncUserInfo();
                    System.out.println("Synchronizing loaded data to clients.");
                }).start();
                break;
            }
            case "StaticDataMng":{
                new Thread(()->{
                    StaticDataMng.i().loadClaimNotOwner();
                    System.out.println("StaticDataMng loaded.");
                }).start();
                break;
            }
        }
        
    }
    
    private void checkFinishLoading(){
        if(mItems.isEmpty()){
            if(flagSyncCliaim==0){
                flagSyncCliaim = 1;
                new Thread(()->{
                    GoldIncUserMng.i().refClaimToMine();
                    System.out.println("Referrence data to all mine.");
                }).start(); 
            }
            return;
        }
        long time = System.currentTimeMillis();
        Entry<String, Long>[] temp;
        synchronized(LOCK){
            temp = mItems.entrySet().toArray(WSGameDefine.EMPTY_ENTRY);
        }
        for(Entry<String, Long> e:temp){
            if((time-e.getValue())>5000){
                finish(e.getKey());
                mItems.remove(e.getKey());
            }
        }
    }
    
    public void startMng() {        
        GoldIncMng.i().init();
        start();
    }

    public void stopMng() {
        mRunning = false;
        interrupt();
    }
    
    private void mSleep(int time) {
        try {
            synchronized (LOCK_WAIT) {
                LOCK_WAIT.wait(time);
            }
        } catch (InterruptedException ex) {
        }
    }

    @Override
    public void run() {
        mRunning = true;
        while (mRunning) {
            mSleep(1000);
            checkFinishLoading();                                                              
        }
    }
}