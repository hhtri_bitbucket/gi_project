/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ctmlab.game.threads;

import com.ctmlab.game.manager.GlobalConfig;
import com.ctmlab.game.network.client.WSServiceClient;
import com.ctmlab.game.network.manager.WSGameDefine;
import com.ctmlab.game.network.manager.WSServiceServer;

/**
 *
 * @author hhtri
 */
public class WSCheckPing extends Thread {

    private WSCheckPing() {}
    public static WSCheckPing _inst = null;
    private static final Object LOCK = new Object();
    private static final Object LOCK_WAIT = new Object();
    private static final long EXPIRE_TIME = 10000;

    public static WSCheckPing i() {
        synchronized (LOCK) {
            if (_inst == null) {
                _inst = new WSCheckPing();
            }
            return _inst;
        }
    }

    private boolean isRunning = false;
//
    public void startCheck() {
        start();
    }

    public void stopCheck() {
        isRunning = false;
        interrupt();
    }
    
    public void mSleep(int time){
        try{
            synchronized(LOCK_WAIT){
                LOCK_WAIT.wait(time);
            }
        } catch(InterruptedException ex){}
    }
    
    public void check(){
        if(!GlobalConfig.isOnlineServer()){return;}
        WSServiceClient[] ls = WSServiceServer.i().getListClient();
        long curTime = System.currentTimeMillis();
        for(WSServiceClient client:ls){
            long time = curTime - client.getLastPing();
            if(time<0 || (time>EXPIRE_TIME)){
//                 WSServiceServer.i().disconnect(client.getId());
            }
        }
    }
    
    @Override
    public void run() {
        isRunning = true;
        while(isRunning){
            if(GlobalConfig.isOnlineServer()){check();}
            mSleep(1000);
        }
    }

}
