/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ctmlab.game.threads;

import com.ctmlab.game.data.model.GameObjectMap;
import java.util.HashMap;

/**
 *
 * @author hhtri
 */
public class ZoneManager extends Thread{    
    private static final Object LOCK = new Object();
    private static final Object LOCK_WAIT = new Object();    
    private static ZoneManager inst;
    public static ZoneManager i() {
        synchronized (LOCK) {
            if (inst == null) {inst = new ZoneManager();}
            return inst;
        }
    }        
    private final ZoneRepo[] EMPTY_ZONE_REPO = new ZoneRepo[0];
    private final HashMap<Short, ZoneRepo> mListZoneRepo = new HashMap();
    private boolean mRunning = false;     
    private ZoneManager(){}
    
    private void mSleep(int time){
        try{
            synchronized(LOCK_WAIT){
                LOCK_WAIT.wait(time);
            }
        } catch(InterruptedException ex){}
    }
    
    public void check(){
        synchronized(LOCK){
            for(ZoneRepo z:mListZoneRepo.values()){
                z.sendToClients();
            }
        }
    }
    
    public void addUserInZone(short zoneID, String userID){
        ZoneRepo zp = mListZoneRepo.get(zoneID);
        if(zp==null){
            zp = new ZoneRepo(zoneID);
            synchronized(LOCK){
                mListZoneRepo.put(zoneID, zp);
            }
        }
        zp.addUserID(userID);
    }
    
    public void updateSpot(GameObjectMap obj){
        short zoneID = obj.id_z;
        ZoneRepo zp = mListZoneRepo.get(zoneID);
        if(zp==null){
            zp = new ZoneRepo(zoneID);
            synchronized(LOCK){
                mListZoneRepo.put(zoneID, zp);
            }
        }
        zp.addQueue(obj);
    }
    
    public void startZoneMng(){
        start();
    }
    
    public void stopZoneMng(){
        mRunning = false;
        interrupt();
    }
    
    @Override
    public void run(){
        mRunning = true;
        while(mRunning){
            check();
            mSleep(3000);
        }
    }
    
}