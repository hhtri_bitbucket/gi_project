/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ctmlab.game.network.manager;

import com.ctmlab.game.data.model.GameObjectMap;
import com.ctmlab.game.data.model.GoldIncUser;
import com.ctmlab.game.data.model.SocialAlliance;
import com.ctmlab.game.data.model.worker.Worker;
import com.ctmlab.game.data.model.building.Building;
import com.ctmlab.game.data.model.equipment.Equipment;
import com.ctmlab.game.data.staticdata.Zone;
import com.ctmlab.game.manager.GlobalConfig;
import com.ctmlab.game.network.client.WSMicroServiceClient;
import com.ctmlab.game.network.client.WSServiceClientChatting;
import com.ctmlab.game.network.client.WSServiceClient;
import com.ctmlab.util.Utils;
import com.dp.db.Func;
import com.google.gson.JsonObject;
import java.io.File;
import java.nio.ByteOrder;
import java.nio.charset.Charset;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map.Entry;

/**
 *
 * @author hhtri
 */
public class WSGameDefine {
    
    public static final byte[] EMPTY_BYTES = new byte[0];
    public static final Entry[] EMPTY_ENTRY = new Entry[0];
    public static final String[] EMPTY_STRING = new String[0];
    public static final Object[] EMPTY_OBJECT = new Object[0];
    public static final GoldIncUser[] EMPTY_USER = new GoldIncUser[0];
    public static final SocialAlliance[] EMPTY_USER_GROUP = new SocialAlliance[0];
    public static final WSServiceClient[] EMPTY_WS_CLIENT = new WSServiceClient[0];
    public static final WSMicroServiceClient[] EMPTY_MICROS_CLIENT = new WSMicroServiceClient[0];
    public static final WSServiceClientChatting[] EMPTY_CHAT_WORKER = new WSServiceClientChatting[0];
    public static final GameObjectMap[] EMPTY_GAMEOBJECT_MAP = new GameObjectMap[0];
    public static final Building[] EMPTY_BUILDING = new Building[0];
    public static final Equipment[] EMPTY_EQUIPMENT = new Equipment[0];
    public static final Worker[] EMPTY_WORKER = new Worker[0];
    public static final Zone[] EMPTY_ZONE = new Zone[0];
    
    public static final Charset UTF_8 = Charset.forName("UTF-8");
    public static final ByteOrder BYTE_ORDER = ByteOrder.LITTLE_ENDIAN;
    public static final long MILISEC_IN_DAY = 24*60*60*1000;
    public static final int NUMBET_OF_FRAME = 512*1024;
    public static final int WS_BUFFER_SIZE = 65536;
    public static final int MAP_INITIAL_CAPACITY = 512;
    
    public static final int ALLIANCE_COLEADER_LIMIT = 2;
    public static final int UNREAD_STATE_MSG = 0;
    public static final int READ_STATE_MSG = 1;

    public static SimpleDateFormat sSimpleDay = new SimpleDateFormat("yyyyMMdd");
    public static SimpleDateFormat sSimpleTimeDay = new SimpleDateFormat("yyyyMMddHHmmss");    

    public static String getFullCurrentTime(){
        return sSimpleTimeDay.format(new Date());
    }

    // <editor-fold defaultstate="collapsed" desc="HOST">
    
//    public static String HOST = "http://192.168.9.22:8084";
    public static String HOST = GlobalConfig.isOnlineServer() ? "http://test.ctmlab.com:8080":"http://192.168.9.22:8084";
    
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Define return value [ProfilePrivate - addResource]">
    
    public static final int ADD_RES_INVALID = -1;
    public static final int ADD_RES_SUCCESS = 0;
    public static final int ADD_RES_FAIL = 1;
    public static final int ADD_RES_MISSING_CONFIG = 2;
    
    // </editor-fold>
}
