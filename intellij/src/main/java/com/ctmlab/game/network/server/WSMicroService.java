/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ctmlab.game.network.server;

import com.ctmlab.game.hadoop.HadoopProvider;
import com.ctmlab.game.network.client.WSMicroServiceClient;
import com.ctmlab.game.network.manager.WSServiceServer;
import com.dp.db.Func;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import javax.websocket.OnClose;
import javax.websocket.OnError;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;

/**
 *
 * @author hhtri
 */
@ServerEndpoint("/microservice")
public class WSMicroService {
    private final String logName = "microservice";
    private final StringBuilder logData = new StringBuilder();
    private WSMicroServiceClient client = null;
    
    private void debug(String data){
        HadoopProvider.i().writeLog(logName, data);
    }
    
    @OnOpen
    public void onOpen(Session peer) {  
        client = new WSMicroServiceClient();
        client.init(peer);
        logData.append(Func.now()).append("\n")
                .append(client.getId())
                .append(" - Opened (Playing): (").append(peer.getId())
                .append(")\nBufferSize: ")
                .append(peer.getMaxBinaryMessageBufferSize());
        debug(logData.toString());
        logData.setLength(0);
    }

    @OnClose
    public void onClose(Session peer) {
        if (client != null) {        
            
            WSServiceServer.i().rmClient(client);  
            
            logData.append(Func.now()).append("\n")
                    .append(client.getId())
                    .append(" - Closed (Playing): (").append(peer.getId())
                    .append(")");
            debug(logData.toString());
            logData.setLength(0);
            
            client = null;
        }
    }

    @OnError
    public void onError(Throwable t) {  
        if(client!=null){
            logData.append(Func.now()).append("\n")
                    .append(client.getId()).append(" - onError\n")
                    .append(Func.toString(t));
            debug(logData.toString());
            logData.setLength(0);
        }
    }

    @OnMessage
    public void onMessage(ByteBuffer data) {
        if (client != null) {
            data.order(ByteOrder.LITTLE_ENDIAN);
            try {
                client.onMessage(data);
            } catch (IOException ex) {
                logData.append(Func.now()).append("\n")
                    .append(client.getId())
                    .append(" - onMessage by ByteBuffer has Exception\n")
                    .append(Func.toString(ex));
                debug(logData.toString());
                logData.setLength(0);
            }
        }
    }

    @OnMessage
    public void onMessage(String message) {
        if (client != null) {
            try {
                client.onMessage(message);
            } catch (IOException ex) {
                logData.append(Func.now())
                    .append("\n")
                    .append(client.getId())
                    .append(" - onMessage by String has Exception\n")
                    .append(Func.toString(ex));
                debug(logData.toString());
                logData.setLength(0);
            }
        }
    }
}
