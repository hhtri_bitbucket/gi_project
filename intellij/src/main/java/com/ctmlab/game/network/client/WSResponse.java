/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ctmlab.game.network.client;

import com.ctmlab.game.network.manager.WSGameDefine;
import com.google.common.io.LittleEndianDataOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;

/**
 *
 * @author hhtri
 */
public class WSResponse {
    public short cmd;
    public byte[] data;
    public WSResponse(){
        cmd = -1;
        data = WSGameDefine.EMPTY_BYTES;
    }
    
    public byte[] getBytes(){
        try{
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            LittleEndianDataOutputStream os = new LittleEndianDataOutputStream(baos);
            os.writeShort(cmd);
            os.writeInt(data.length);
            os.write(data);
            return baos.toByteArray();
        } catch(IOException ex){
            return WSGameDefine.EMPTY_BYTES;
        }
    }
    
    public boolean isOutOfBufferSize(){
        int count = 4 + 4 + 4 + data.length;        
        return count>WSGameDefine.WS_BUFFER_SIZE;
    }
    
    public ArrayList<byte[]> splitData(){
        ArrayList<byte[]> rs = new ArrayList<>();
        
        return rs;
    }
}
