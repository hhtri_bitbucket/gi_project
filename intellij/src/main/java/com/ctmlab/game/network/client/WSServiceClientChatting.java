/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ctmlab.game.network.client;

import com.ctmlab.game.hadoop.LogManager;
import com.ctmlab.game.goldincenum.GI_SERVER_CMD;
import com.ctmlab.game.network.manager.WSServiceChattingMng;
import com.dp.db.Func;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.LinkedList;
import java.util.Queue;
import javax.websocket.SendHandler;
import javax.websocket.SendResult;
import javax.websocket.Session;

/**
 *
 * @author hhtri
 */
public class WSServiceClientChatting implements SendHandler {
    
    // <editor-fold defaultstate="collapsed" desc="Variances in class">    
    
    // LOCK to synchronize
    private final Object SENDING_LOCK = new Object();
    // flag uses for synchronizing    
    private boolean mSending = false;
    // last time connect. Using for check timeout session
    private long mLastPing = 0;
    // keeping session when has a connection 
    private Session mSession = null;
    // id return after add worker to manager (WSServiceMng)
    private int mId = -1;
    // buffer data (bytebuffer) to support for sending data to client 
    private final Queue<ByteBuffer> mDataQueue = new LinkedList();    
    
    // log name
    private final String mLogName = this.getClass().getSimpleName();
    // stringbuilder using for debug log
    private final StringBuilder mLogBuilder = new StringBuilder();
    
    public long teamID = -1;
    public int idInGlobal = -1;
    public int idInHost = -1;
    public long lastIdxMsgGroup = -1;
    public long lastIdxMsgGlobal = -1;
    
    
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="Public Methods">
    
    public void init(Session _session){
        mSession = _session;
//        WSServiceMng.i().addClient(this);
        mLastPing = System.currentTimeMillis();
    }    
    
    public void notifySend() {       
        ByteBuffer buffer;
        synchronized (mDataQueue) {
            if (mDataQueue.isEmpty()) {
                return;
            }
            synchronized (SENDING_LOCK) {
                if (mSending) {
                    return;
                }
                mSending = true;
            }
            buffer = mDataQueue.poll();
        }
        if (mSession.isOpen()) {
            try {
                synchronized (buffer) {
                    buffer.rewind();
                    mSession.getAsyncRemote().sendBinary(buffer, this);
                }
            } catch (java.lang.IllegalStateException ex) {
                try {mSession.close();}catch(IOException ex_close){}
            }
        }
    }
    
    // method send: 
    public void send(ByteBuffer buffer) {
        synchronized (mDataQueue) {
            mDataQueue.add(buffer);
        }
        notifySend();
    }

    // method disconnect: close connection and remove from manager RAM.
    // Just manager call this methods
    public void disconnect() {
        if (mSession != null) {
            try {
                mSession.close();
            } catch (IOException ex) {
            }
        }
    }
    
    // <editor-fold defaultstate="collapsed" desc="Recv Methods">
    public void onMessage(ByteBuffer data) throws IOException {
        GI_SERVER_CMD CMD = GI_SERVER_CMD.fromID(data.getShort());
        switch (CMD) {
            case IDLE:{
                // server check
                mLastPing = System.currentTimeMillis();
                break;
            }
            default:{
                parseMsg(CMD, data);
                break;
            }
        }
    }
    
    private void parseMsg(GI_SERVER_CMD cmd, ByteBuffer data){
        switch(cmd){
//            case MSG_CONNECT:{
//                teamID = data.getLong();                        
//                // add global
//                WSServiceChattingMng.i().addClient(this);                
//                // has team => add team                
//                // else => dont do anything     
//                if(teamID!=-1){
//                    WSServiceChattingMng.i().addClientToTeam(teamID, this);
//                }
//                
//                break;
//            }
//            case MSG_TEAM:{
//                // add to team with teamID
//                teamID = data.getLong();
//                WSServiceChattingMng.i().nodifyTeam(teamID, data);
//                break;
//            }
//            case MSG_GLOBAL:{
//                // add to global and send all
//                WSServiceChattingMng.i().nodifyGlobal(data);
//                break;
//            }
//            case MSG_OLD_TEAM:{
//                // need time (long)
//                teamID = data.getLong();
//                break;
//            }
//            case MSG_OLD_GLOBAL:{
//                // need time (long)
//                break;
//            }
        }
    }
    
    public void onMessage(String data) throws IOException {
        synchronized(mLogBuilder){
            mLogBuilder.append(Func.now()).append(" - Recv String\n").append(data);
            debug(mLogBuilder.toString());
            mLogBuilder.setLength(0);
        }
    }    
    // </editor-fold>
    
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="Private Methods">
    
    private void debug(String data){
        LogManager.i().writeLog(mLogName, data);
    }
    
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="Refactor Methods">
    
    /**
     * @return the lastPing
     */
    public long getLastPing() {
        return mLastPing;
    }

    /**
     * @param lastPing the lastPing to set
     */
    public void setLastPing(long lastPing) {
        this.mLastPing = lastPing;
    }

    /**
     * @return the session
     */
    public Session getSession() {
        return mSession;
    }

    /**
     * @param session the session to set
     */
    public void setSession(Session session) {
        this.mSession = session;
    }

    /**
     * @return the id
     */
    public int getId() {
        return mId;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.mId = id;
    }
    
    // </editor-fold>      
    
    // <editor-fold defaultstate="collapsed" desc="Override Methods">
    
    /**
    * Method insure that the data is sent completely to client-side
    * @author hhtri
    * @param result
    */
    @Override
    public void onResult(SendResult result) {
//        synchronized (SENDING_LOCK) {
//            sending = false;
//        }
//        if (result.isOK()) {
//        } else {
//            Throwable ex = result.getException();
//            if (ex != null) {
//                ex.printStackTrace();
//            }
//        }
//        notifySend();
    }
    
    // </editor-fold>
    
}
