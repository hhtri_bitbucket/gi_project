/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ctmlab.game.network.client.frames;

import java.io.ByteArrayOutputStream;
import java.nio.ByteBuffer;

/**
 *
 * @author hhtri
 */
public class FrameItem {
    public int len;
    public int lenRead;
    private ByteArrayOutputStream baos;
    public FrameItem() {
        len = 0;
        lenRead = -1;
        baos = new ByteArrayOutputStream();        
    }
    
    public byte[] getData(){
        return baos.toByteArray();
    }
    
    public boolean readData(ByteBuffer buff){        
        if(len==-1){
            len = buff.getInt();
        }
        int remainLen = len - lenRead;
        byte[] temp = new byte[(buff.remaining()>remainLen)?remainLen:buff.remaining()];
        buff.get(temp);
        lenRead+=temp.length;
        baos.write(temp, 0, temp.length);
        return len==lenRead;
    }
}
