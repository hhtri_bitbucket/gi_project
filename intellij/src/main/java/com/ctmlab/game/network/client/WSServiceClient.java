/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ctmlab.game.network.client;

import com.ctmlab.game.goldincenum.ENUM_MESSAGE;
import com.ctmlab.game.goldincenum.ENUM_UNIT_PROPERTY;
import com.ctmlab.game.hadoop.LogManager;
import com.ctmlab.game.goldincenum.GI_SERVER_CMD;
import com.ctmlab.game.manager.ServiceMng;
import com.ctmlab.game.network.manager.WSGameDefine;
import com.ctmlab.game.network.manager.WSServiceServer;
import com.ctmlab.util.Utils;
import com.dp.db.Func;
import com.google.gson.JsonObject;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.Queue;
import java.util.TreeMap;
import javax.websocket.SendHandler;
import javax.websocket.SendResult;
import javax.websocket.Session;

/**
 *
 * @author hhtri
 */
public class WSServiceClient implements SendHandler {
    
    // <editor-fold defaultstate="collapsed" desc="Variances in class">    
    
    // LOCK to synchronize
    private final Object SENDING_LOCK = new Object();
    // flag uses for synchronizing    
    private boolean mSending = false;
    // last time connect. Using for check timeout session
    private long mLastPing = 0;
    // keeping session when has a connection 
    private Session mSession = null;
    // id return after add worker to manager (WSServiceMng)
    private int mId = -1;
    // buffer data (bytebuffer) to support for sending data to client 
    private final Queue<byte[]> mDataQueue = new LinkedList(); 
    // id user signin (success)
    private String userID = "";
    
    // log name
    private String mLogName = this.getClass().getSimpleName().toLowerCase();
    // stringbuilder using for debug log
    private final StringBuilder mLogBuilder = new StringBuilder();
    
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="Public Methods">
    
    public String getInfoSocket(){
        JsonObject obj = new JsonObject();
        obj.addProperty("ID", mId+"");
        obj.addProperty("UserID", userID);
        obj.addProperty("LastPing", WSGameDefine.sSimpleTimeDay.format(new Date(getLastPing())));
        return Func.pson(obj);
    }
    
    public void init(Session _session){
        
        mSession = _session;
        WSServiceServer.i().addClient(this);
        mLastPing = System.currentTimeMillis();
    }    
    
    public void notifySend() {       
        ByteBuffer buffer;
        synchronized (mDataQueue) {
            if (mDataQueue.isEmpty()) {
                return;
            }
            synchronized (SENDING_LOCK) {
                if (mSending) {
                    return;
                }
                mSending = true;
            }
            buffer = ByteBuffer.wrap(mDataQueue.poll());
        }
        if (mSession.isOpen()) {
            try {
                synchronized (buffer) {
                    buffer.rewind();
                    mSession.getAsyncRemote().sendBinary(buffer, this);
                }
            } catch (java.lang.IllegalStateException ex) {
                try {mSession.close();}catch(IOException ex_close){}
            }
        }
    }
    
    // method send: 
    public void send(byte[] buffer) {     
        synchronized (mDataQueue) {
            mDataQueue.add(buffer);
        }
        notifySend();
    }

    // method disconnect: close connection and remove from manager RAM.
    // Just manager call this methods
    public void disconnect() {
        if (mSession != null) {
            try {
                mSession.close();
            } catch (IOException ex) {
            }
        }
    }
    
    // <editor-fold defaultstate="collapsed" desc="Recv Methods">   
    public void onMessage(ByteBuffer data) throws IOException {
        GI_SERVER_CMD CMD = GI_SERVER_CMD.fromID((int)data.getShort());
        mLogBuilder.append(String.format("Time: %s\nCMD: %s\nData remain: %d", WSGameDefine.sSimpleTimeDay.format(new Date()), CMD.toString(), data.remaining()));
        ServiceMng mng = ServiceMng.i();
        long time = System.currentTimeMillis();
        switch (CMD) {
            case REQ_DATA_ZONE:{
                short zoneID = data.getShort();
                mLogBuilder.append("\nzone_id:").append(zoneID);
                mng.getZoneDetail(this, mLogBuilder, zoneID);
                break;
            }
            case REQ_MINE_INFO:{
                short zone_id = data.getShort();
                long mine_id = data.getLong();
                mLogBuilder.append("\nzone_id:").append(zone_id).append("\nmine_id:").append(mine_id);
                mng.getMineInfo(this, mLogBuilder, zone_id, mine_id);
                break;
            }
            case REQ_TEST_MINE:{
                short zone_id = data.getShort();
                long mine_id = data.getLong();
                mLogBuilder.append("\nzone_id:").append(zone_id).append("\nmine_id:").append(mine_id);
                mng.payTestMine(this, mLogBuilder, zone_id, mine_id);
                break;
            }
            case REQ_PEG_MINE:{
                short zone_id = data.getShort();
                long mine_id = data.getLong();
                mLogBuilder.append("\nzone_id:").append(zone_id).append("\nmine_id:").append(mine_id);
                mng.payMine(this, mLogBuilder, zone_id, mine_id);
                break;
            }
            case REQ_UPDATE_WALLET:{
                mng.updateWalletClient(this, mLogBuilder);
                break;
            }
            case REQ_BUY_RESOURCE:{
                TreeMap<Byte, Long> buys = new TreeMap();
                int len = data.getInt();
                int i = 0;
                while(i++<len){buys.put(data.get(), data.getLong());}
                mng.buyResource(this, mLogBuilder, buys);
                break;
            }
            case REQ_SELL_RESOURCE:{
                TreeMap<Byte, Long> sells = new TreeMap();
                int len = data.getInt();
                int i = 0;
                while(i++<len){sells.put(data.get(), data.getLong());}
                byte type = ENUM_UNIT_PROPERTY.SPEED_LAND.getCode();
                if(data.remaining()==1){
                    type = data.get();
                }
                mng.sellResource(this, mLogBuilder, sells, type);
                break;
            }
            case REQ_SIGN_IN:{
                // <editor-fold defaultstate="collapsed" desc="Recv Methods">
                String accID = Utils.i().readBigString(data);
                String passwd = Utils.i().readBigString(data);
                mLogBuilder.append(String.format("\nemail:%s\npasswd:%s", accID, passwd));
                if(mng.signIn(this, mLogBuilder, accID, passwd)==0){
                    userID = accID;
                    WSServiceServer.i().addClientWithID(mId, userID);
                    mLogName = userID + "_" + this.getClass().getSimpleName().toLowerCase();
                }
                break;
            }
            case REQ_USER_RESOURCE:{
                String accID = Utils.i().readBigString(data);
                mLogBuilder.append(String.format("\nUserID: %s\nOther: %s", userID, accID));
                mng.getUserResource(this, mLogBuilder, userID);
                break;
            }
            case REQ_SET_NAME:{
                String name = Utils.i().readBigString(data);
                mLogBuilder.append("\nname:").append(name);
                mng.setNameClient(this, mLogBuilder, name);
                break;
            }
            case REQ_TRADE_CASH_INFO:{
                mng.getTradeCashInfo(this, mLogBuilder);
                break;
            }
            case REQ_SEND_MESSAGE:{
                String sender = userID;
                String receiver = Utils.i().readBigString(data);
                String subject = Utils.i().readBigString(data);
                String content = Utils.i().readBigString(data);
                mLogBuilder.append("\nsender:").append(sender)
                        .append("\nreceiver:").append(receiver)
                        .append("\ncontent:").append(content);

                mng.sendMsgGame(this, mLogBuilder, sender, receiver, subject, content, ENUM_MESSAGE.GAME_MSG.getCode());
                break;
            }
            case REQ_GET_MESSAGE:{
                mng.getMsgGame(this, mLogBuilder);
                break;
            }
            case REQ_SET_STATE_MESSAGE:{
                long id = data.getLong();
                mLogBuilder.append("\nid:").append(id);
                mng.setReadStateMsg(this, mLogBuilder, id);
                break;
            }
            case REQ_REMOVE_MESSAGE:{
                long id = data.getLong();
                mLogBuilder.append("\nid:").append(id);
                mng.rmMsgGame(this, mLogBuilder, id);
                break;
            }
            case REQ_CREATE_ALLIANCE:{
                String name = Utils.i().readBigString(data);
                mng.createAlliance(this, name);
                break;
            }
            case REQ_INVITE_ALLIANCE:{
                String receiver = Utils.i().readBigString(data);
                String subject = Utils.i().readBigString(data);
                String content = Utils.i().readBigString(data);
                mng.inviteAlliance(this, mLogBuilder, receiver, subject, content);
                break;
            }
            case REQ_JOIN_ALLIANCE:{
                String name = Utils.i().readBigString(data);
                mng.createAlliance(this, name);
                break;
            }
            case REQ_ADD_MEMBER:{
                String name = Utils.i().readBigString(data);
                mng.createAlliance(this, name);
                break;
            }
            case REQ_CHAT_REGION:{
                short id_r = data.getShort();
                String msg = Utils.i().readBigString(data);
                mng.chatRegion(this, msg, id_r);
                break;
            }
            case REQ_PLAYER_BUILDING:{
                String oUserID = Utils.i().readBigString(data);
                mng.playerBuilding(this, mLogBuilder, oUserID);
                break;
            }
            case REQ_UPGRADE_BUILDING:{
                byte type = data.get();
                mng.upgradeBuilding(this, mLogBuilder, type);
                break;
            }
            case REQ_TRANSPORT_TO_CLAIM:{
                short zoneID = data.getShort();
                long mineID = data.getLong();
                ArrayList<Long> lstEquip = new ArrayList();
                ArrayList<Long> lstWorker = new ArrayList();
                int len = data.getInt();
                int i = 0;
                while(i++<len){lstEquip.add(data.getLong());}
                len = data.getInt();
                i = 0;
                while(i++<len){lstWorker.add(data.getLong());}
                mng.transportToClaim(this, mLogBuilder, zoneID, mineID, lstEquip, lstWorker, data.get());
                break;
            }
            case REQ_REFINE_OIL:{
                int number = data.getInt();
                mng.refineFuel(this, mLogBuilder, number);
                break;
            }
            case REQ_TRANSPORT_FUEL:{
                short zoneID = data.getShort();
                long mineID = data.getLong();
                int numberOfFuel = data.getInt();
                byte type= data.get();
                mng.transportFuelToClaim(this, mLogBuilder, zoneID, mineID, numberOfFuel, type);
                break;
            }
            case REQ_ATTACK_OIL_RIG:{
                short zone_id = data.getShort();
                long mine_id = data.getLong();
                mLogBuilder.append("\nzone_id:").append(zone_id).append("\nmine_id:").append(mine_id);
                mng.attackOilRig(this, mLogBuilder, zone_id, mine_id);
                break;
            }
            case REQ_AUTO_DIGGING:{
                mng.testAutoDigging(this, mLogBuilder);
                break;
            }
            case REQ_AUTO_WASHING:{
                mng.testAutoWashing(this, mLogBuilder);
                break;
            }
            case IDLE:{
                mng.resPing(this, time);
                break;
            }
            default:{
                data.rewind();
                System.out.println(Func.now() + ": " + Func.json(data.array()));
                break;
            }
        }
        if(CMD!=GI_SERVER_CMD.IDLE){
            synchronized (mLogBuilder) {
                debug(mLogBuilder.toString());
            }
        }
        mLogBuilder.setLength(0);
        mLastPing = time;
    }
    
    public void onMessage(String data) throws IOException {
        synchronized(mLogBuilder){
            mLogBuilder.append(Func.now()).append(" - Recv String\n").append(data);
            debug(mLogBuilder.toString());
            mLogBuilder.setLength(0);
        }
    }    
    // </editor-fold>
    
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="Private Methods">
    
    // If data is a big data, return length of data else return 0
//    private int isBigFrame(byte[] data) {
//        if (lenBigFrame > 0) {
//            return 0;
//        }
//        ByteBuffer b = ByteBuffer.wrap(data);
//        int len = b.getInt();
//         //frame big
//         //trường hợp này cứ lấy gói tin và gửi, khi nào lastLen==lenBigFrame thì ghép frame tiếp
//        return len > WSGameDefine.WS_BUFFER_SIZE ? len : 0;
//    }
    
//    private void notifySendAdvance() {
//        synchronized (mDataQueue) {
//            if (mDataQueue.isEmpty()) {
//                return;
//            }
//            synchronized (SENDING_LOCK) {
//                if (mSending) {
//                    return;
//                }
//                mSending = true;
//            }
//        }
//        byte[] t;
//        int lenFrameSend = 0;
//        synchronized (data_queue) {
//            t = (byte[]) data_queue.poll();
//        }
//        if (mSession.isOpen()) {
//            try {
//                synchronized (buffer) {
//                    buffer.rewind();
//                    mSession.getAsyncRemote().sendBinary(buffer, this);
//                }
//            } catch (java.lang.IllegalStateException ex) {
//                try {mSession.close();}catch(IOException ex_close){}
//            }
//        }
//    }
    
    private void debug(String data){
        LogManager.i().writeLog(mLogName, data);
    }
    
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="Refactor Methods">
    
    /**
     * @return the lastPing
     */
    public long getLastPing() {
        return mLastPing;
    }

    /**
     * @param lastPing the lastPing to set
     */
    public void setLastPing(long lastPing) {
        this.mLastPing = lastPing;
    }

    /**
     * @return the session
     */
    public Session getSession() {
        return mSession;
    }

    /**
     * @param session the session to set
     */
    public void setSession(Session session) {
        this.mSession = session;
    }

    /**
     * @return the id
     */
    public int getId() {
        return mId;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.mId = id;
    }
    
    public String getUserID() {
        return userID;
    }
    
    // </editor-fold>      
    
    // <editor-fold defaultstate="collapsed" desc="Override Methods">
    
    /**
    * Method insure that the data is sent completely to client-side
    * @author hhtri
    * @param result
    */
    @Override
    public void onResult(SendResult result) {
        synchronized (SENDING_LOCK) {
            mSending = false;
        }
        if (result.isOK()) {
        } else {
            Throwable ex = result.getException();
            if (ex != null) {
                debug(Func.toString(ex));
            }
        }
        notifySend();
    }
    
    // </editor-fold>
    
}
