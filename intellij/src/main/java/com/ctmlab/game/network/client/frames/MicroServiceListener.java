/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ctmlab.game.network.client.frames;

import java.util.ArrayList;

/**
 *
 * @author hhtri
 */
public interface MicroServiceListener {
    public void recvFrame(ArrayList<byte[]> data);
    public void sendFrame(ArrayList<byte[]> data);
}
