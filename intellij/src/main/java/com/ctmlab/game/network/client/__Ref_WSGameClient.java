/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ctmlab.game.network.client;

import com.ctmlab.game.hadoop.LogManager;
import com.ctmlab.game.network.buffer.WSGameMsgBuffer;
import com.ctmlab.game.network.server.WSGameServer;
import com.ctmlab.util.Utils;
import com.google.common.io.LittleEndianDataOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import javax.websocket.SendHandler;
import javax.websocket.SendResult;
import javax.websocket.Session;

/**
 *
 * @author hhtri
 */
public class __Ref_WSGameClient implements SendHandler {

//    private final String logName = this.getClass().getSimpleName().toLowerCase();
//
//    private int id = -1;
//
//    public int getSessionID() {
//        return id;
//    }
//    private String account_id = "";
//
//    public String getAccountID() {
//        return account_id;
//    }
//    private String name = "";
//    public short id_in_host = 0;
//    public long lastPing = 0;
//    private Session session = null;
//    private WSTeamGroupChatting host = null;
//    
//    private boolean isPlayingGame = false;
//    public final GameFrameClient gameFrame = new GameFrameClient(0L);
//
//    private final Queue<ByteBuffer> data_queue = new LinkedList();
//    private final Object SENDING_LOCK = new Object();
//    private boolean sending = false;
//
//    public boolean isMain() {
//        return id_in_host == WSTeamGroupChatting.MAIN_ID;
//    }
//
//    public String getParameter(String param) {
//        List<String> list = session.getRequestParameterMap().get(param);
//        if (list != null && list.size() > 0) {
//            return list.get(0);
//        }
//        return null;
//    }
//
//    public void init(Session _session) {
//        session = _session;
//        id = WSGameServer.i().genClientID();
//        WSGameServer.i().addClient(this);
//        lastPing = System.currentTimeMillis();
//    }
//    
//    public void clientPlayingGame(long gameObjectID){
//        isPlayingGame = true;
//        gameFrame.setmGameObjectID(gameObjectID);
//    }
//
//    public void notifySend() {        
//        if(isPlayingGame){
//            if (sending) {return;}
//            WSGameFrameBuffer.i().getBuffer(gameFrame);
//            if (session.isOpen()) {
//                try {
//                    synchronized (gameFrame) {
//                        gameFrame.getmBuffer().rewind();
//                        session.getAsyncRemote().sendBinary(gameFrame.getmBuffer(), this);
//                    }
//                } catch (java.lang.IllegalStateException ex) {
//                    host.remove(this);
//                    try {session.close();}catch(IOException ex_close){}
//                }
//            }
//        } else{        
//            ByteBuffer buffer;
//            synchronized (data_queue) {
//                if (data_queue.isEmpty()) {
//                    return;
//                }
//                synchronized (SENDING_LOCK) {
//                    if (sending) {
//                        return;
//                    }
//                    sending = true;
//                }
//                buffer = data_queue.poll();
//            }
//            if (session.isOpen()) {
//                try {
//                    synchronized (buffer) {
//                        buffer.rewind();
//                        session.getAsyncRemote().sendBinary(buffer, this);
//                    }
//                } catch (java.lang.IllegalStateException ex) {
//                    host.remove(this);
//                    try {session.close();}catch(IOException ex_close){}
//                }
//            }
//        }
//    }
//
//    public void send(ByteBuffer buffer) {
//        synchronized (data_queue) {
//            data_queue.add(buffer);
//        }
//        notifySend();
//    }
//
//    public void disconnect() {
//        if (session != null) {
//            try {
//                session.close();
//            } catch (IOException ex) {
//            }
//        }
//    }
//
//    private void hostMsg(ByteBuffer data) throws IOException {        
//        if (isMain()) {
//            host.onMessage(data);
//        } else if (host.main != null) {
//            // server forward host client
//            host.main.send(data);
//        }
//    }
//
//    private void clientMsg(MWNetworkClientCMD CMD, ByteBuffer data) throws IOException {
//        switch (CMD) {
//            case Null:
//                break;
//            case REQ_SIGN_IN:
//                onReceiveSIGNIN(data);
//                break;
//            case REQ_LIST_HOST:
//                onReceiveGETLISTHOST(data);
//                break;
//            case REQ_CREATE_HOST:
//                onReceiveCREATEHOST(data);
//                break;
//            case REQ_JOIN_HOST:
//                onReceiveJOINHOST(data);
//                break;
//        }
//        lastPing = System.currentTimeMillis();
//    }
//
//    public void onMessage(ByteBuffer data) throws IOException {
//        MWNetworkClientCMD CMD = MWNetworkClientCMD.fromID(data.getShort());
//        switch (CMD) {
//            case IDLE:{
//                // server check
//                lastPing = System.currentTimeMillis();
//                break;
//            }
//            default:{
//                if (host != null) {
//                    data.rewind();
//                    hostMsg(data);
//                } else {
//                    clientMsg(CMD, data);
//                }
//                break;
//            }
//        }
//    }
//
    @Override
    public void onResult(SendResult result) {
//        synchronized (SENDING_LOCK) {
//            sending = false;
//        }
//        if (result.isOK()) {
//        } else {
//            Throwable ex = result.getException();
//            if (ex != null) {
//                ex.printStackTrace();
//            }
//        }
//        notifySend();
    }
//
//    private void onReceiveSIGNIN(ByteBuffer data) throws IOException {
//        // read player name and player id
//        if (data.capacity() < 10) {
//            return;
//        }
//        account_id = Utils.readBigString(data);
//        name = Utils.readBigString(data);
//        String secret = Utils.readBigString(data);
//        System.out.println(id + " - " + secret);
//        // send player id back to client
//        ByteArrayOutputStream baos = new ByteArrayOutputStream();
//        LittleEndianDataOutputStream outStream = new LittleEndianDataOutputStream(baos);
//
//        writeNetworkCMD(outStream, MWNetworkClientCMD.RES_SIGN_IN);
//        Utils.writeBigString(outStream, account_id);
//        Utils.writeBigString(outStream, getName());
//
//        byte[] rs = baos.toByteArray();
//        ByteBuffer buffer = ByteBuffer.wrap(rs);
//        send(buffer);
//    }
//
//    private void onReceiveGETLISTHOST(ByteBuffer data) throws IOException {
//        WSTeamGroupChatting[] rs = WSGameServer.i().getListHost();
//        // send list host back to client
//        ByteArrayOutputStream baos = new ByteArrayOutputStream();
//        LittleEndianDataOutputStream outStream = new LittleEndianDataOutputStream(baos);
//
//        writeNetworkCMD(outStream, MWNetworkClientCMD.RES_LIST_HOST);
//        outStream.writeInt(rs.length);
//        for (int i = 0; i < rs.length; i++) {
//            WSTeamGroupChatting host = rs[i];
//            outStream.writeInt(host.id);
//            outStream.writeInt(host.Map_ID());
//            Utils.writeBigString(outStream, host.getName());
//            outStream.writeInt(host.playerCount());
//            outStream.writeInt(host.Max_Player());
//        }
//
//        ByteBuffer buffer = ByteBuffer.wrap(baos.toByteArray());
//        send(buffer);
//    }
//
//    private void onReceiveCREATEHOST(ByteBuffer data) throws IOException {
//        int map_id = data.getInt();
//        int max_player = data.getInt();
//        host = WSGameServer.i().createHost(map_id, max_player);
//        short client_idx = host.add(this);
//        ByteArrayOutputStream baos = new ByteArrayOutputStream();
//        LittleEndianDataOutputStream outStream = new LittleEndianDataOutputStream(baos);
//
//        writeNetworkCMD(outStream, MWNetworkClientCMD.RES_CREATE_HOST);
//        outStream.writeInt(host.id);
//        outStream.writeInt(host.Map_ID());
//        outStream.writeInt(host.Max_Player());
//        outStream.writeShort(host.main.id_in_host);
//        outStream.writeShort(client_idx);
//
//        ByteBuffer buffer = ByteBuffer.wrap(baos.toByteArray());
//        send(buffer);
//
//        WSGameServer.i().listHostToSingleClient();
//    }
//
//    private void onReceiveJOINHOST(ByteBuffer data) throws IOException {
//        int host_id = data.getShort();
//        host = WSGameServer.i().getHost(host_id);
//
//        if (host != null) {
//            short client_idx = host.add(this);
//        } else {
//            // host not found
//        }
//    }
//
//    private void writeNetworkCMD(LittleEndianDataOutputStream dos, MWNetworkClientCMD cmd) throws IOException {
//        dos.writeShort(cmd.getID());
//    }
//
//    public WSTeamGroupChatting getHost() {
//        return host;
//    }
//
//    public void setHost(WSTeamGroupChatting _host) {
//        host = _host;
//    }
//
//    /**
//     * @return the name
//     */
//    public String getName() {
//        return name;
//    }
}
