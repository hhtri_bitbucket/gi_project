/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ctmlab.game.network.buffer;

import com.ctmlab.game.data.model.SocialChat;
import com.ctmlab.game.data.pojo.DataChat;
import java.util.SortedMap;
import java.util.TreeMap;

/**
 *
 * @author hhtri
 */
public class MessageInDay{    

    public static final Object LOCK = new Object();
    
    private final TreeMap<Long, SocialChat> mBuffer = new TreeMap();
    private long time = -1L;

    public MessageInDay(){
        time = -1L;
    }
        
    public DataChat getMsgObj(long t){
        SocialChat tmp = mBuffer.get(t);
        if(tmp==null){return null;}
        return new DataChat(tmp);
    }
    
    public DataChat[] getMsgPojo(long start, long end){
        if(start<end){return null;}
        DataChat[] result = null;
        SortedMap<Long, SocialChat> map = null;
        synchronized(mBuffer){
            map = mBuffer.subMap(start, end);
            
        }
        if(map!=null && !map.isEmpty()){
            result = new DataChat[map.size()];
            int i = 0;
            for(SocialChat item:map.values()){
                result[i++] = new DataChat(item);
            }
        }
        return result;
    }
    
    public void addItem(SocialChat data){
        synchronized(mBuffer){
            mBuffer.put(data.time, data);
        }
        if(time==-1){
            time = data.time;
        }
    }
    
    public void reset(){
        time =  -1L;
        mBuffer.clear();
    }
}
