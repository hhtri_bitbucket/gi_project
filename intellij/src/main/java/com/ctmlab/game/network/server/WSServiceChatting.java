/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ctmlab.game.network.server;

import com.ctmlab.game.hadoop.LogManager;
import com.ctmlab.game.network.client.WSServiceClient;
import com.ctmlab.game.network.client.WSServiceClientChatting;
import com.ctmlab.game.network.manager.WSServiceServer;
import com.dp.db.Func;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import javax.websocket.OnClose;
import javax.websocket.OnError;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;

/**
 *
 * @author hhtri
 */
@ServerEndpoint("/giservicechatting")
public class WSServiceChatting {
    
    // <editor-fold defaultstate="collapsed" desc="Variances">
    
    private final String logName = "giservice";
    private final StringBuilder logData = new StringBuilder();
    private WSServiceClientChatting client = null;
    
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="Private Methods">
    
    private void debug(String data){
        LogManager.i().writeLog(logName, data);
    }
    
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="Override Methods">
    @OnOpen
    public void onOpen(Session peer) {        
        client = new WSServiceClientChatting();
        client.init(peer);
        logData.append(Func.now()).append("\n")
                .append(client.getId())
                .append(" - Opened (Playing): (").append(peer.getId())
                .append(")\nBufferSize: ")
                .append(peer.getMaxBinaryMessageBufferSize());
        debug(logData.toString());
        logData.setLength(0);
    }

    @OnClose
    public void onClose(Session peer) {
        if (client != null) {        
            
//            WSServiceMng.i().rmClient(client);  
            
            logData.append(Func.now()).append("\n")
                    .append(client.getId())
                    .append(" - Closed (Playing): (").append(peer.getId())
                    .append(")");
            debug(logData.toString());
            logData.setLength(0);
            
            client = null;
        }
    }

    @OnError
    public void onError(Throwable t) {        
        logData.append(Func.now()).append("\n")
                .append(client.getId()).append(" - onError\n")
                .append(Func.toString(t));
        debug(logData.toString());
        logData.setLength(0);
    }

    @OnMessage
    public void onMessage(ByteBuffer data) {
        if (client != null) {
            data.order(ByteOrder.LITTLE_ENDIAN);
            try {
                data.rewind();
                client.onMessage(data);
            } catch (IOException ex) {
                logData.append(Func.now()).append("\n")
                    .append(client.getId())
                    .append(" - onMessage by ByteBuffer has Exception\n")
                    .append(Func.toString(ex));
                debug(logData.toString());
                logData.setLength(0);
            }
        }
    }

    @OnMessage
    public void onMessage(String message) {
        if (client != null) {
            try {
                client.onMessage(message);
            } catch (IOException ex) {
                logData.append(Func.now())
                    .append("\n")
                    .append(client.getId())
                    .append(" - onMessage by String has Exception\n")
                    .append(Func.toString(ex));
                debug(logData.toString());
                logData.setLength(0);
            }
        }
    }
    
    // </editor-fold>
    
}
