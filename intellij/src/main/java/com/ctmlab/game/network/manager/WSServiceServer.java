/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ctmlab.game.network.manager;

import com.ctmlab.game.data.model.GoldIncUser;
import com.ctmlab.game.manager.GoldIncUserMng;
import com.ctmlab.game.network.client.WSMicroServiceClient;
import java.util.concurrent.atomic.AtomicInteger;
import com.ctmlab.game.network.client.WSServiceClient;
import java.util.HashMap;

/**
 *
 * @author hhtri
 */
public class WSServiceServer {
    
    // <editor-fold defaultstate="collapsed" desc="Variances in class">    
    
    private final AtomicInteger aClientID = new AtomicInteger(1); 
    private final AtomicInteger aClientID1 = new AtomicInteger(1); 
    private static final Object LOCK = new Object(); 
//    private final String mLogName = this.getClass().getSimpleName().toLowerCase();
    
    public static WSServiceServer _inst = null;  
    public final HashMap<Integer, WSServiceClient> iClients = new HashMap();
    public final HashMap<String, WSServiceClient> sClients = new HashMap();
    
    public final HashMap<Integer, WSMicroServiceClient> iMicroSClients = new HashMap();
    
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="Singleton Methods">
    
    private WSServiceServer() {}
    
    public static WSServiceServer i(){synchronized(LOCK){if(_inst==null){_inst = new WSServiceServer();}return _inst;}}
    
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="Private Methods">
       
    
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="Public Methods">
    
    public WSServiceClient[] getListClient() {
        WSServiceClient[] temp;
        synchronized(iClients){temp = iClients.values().toArray(WSGameDefine.EMPTY_WS_CLIENT);}
        return temp;
    }
    
    public WSMicroServiceClient[] getListMicroSClient() {
        WSMicroServiceClient[] temp;
        synchronized(iMicroSClients){temp = iMicroSClients.values().toArray(WSGameDefine.EMPTY_MICROS_CLIENT);}
        return temp;
    }
    
    public void addClient(WSServiceClient client){
        if(client!=null){
            int id = aClientID.getAndIncrement();
            client.setId(id);
            synchronized(iClients){iClients.put(id, client);}
        }
    }
    public void addMicroSClient(WSMicroServiceClient client){
        if(client!=null){
            int id = aClientID1.getAndIncrement();
            client.setId(id);
            synchronized(iMicroSClients){iMicroSClients.put(id, client);}
        }
    }
    
    public void addClientWithID(int id, String userID){
        WSServiceClient client = iClients.get(id);
        if(client!=null){
            sClients.put(userID, client);
        }
    }
    
    public void rmClient(int id){
        synchronized(iClients){iClients.remove(id);}
    }
    
    public void rmClient(String userID){
        synchronized(sClients){sClients.remove(userID);}
    }
    
    public void rmClient(WSServiceClient client){
        if(client!=null){
            GoldIncUser user = GoldIncUserMng.i().getUser(client.getUserID());
            if(user!=null){
                user.isLogin = false;
                user.curZoneID = -1;
                rmClient(client.getId());
                rmClient(client.getUserID());     
            } else{
                rmClient(client.getId());
            }
        }
    }
    
    public void rmClient(WSMicroServiceClient client){
        if(client!=null){
            iMicroSClients.remove(client.getId());
        }
    }
    
    public void disconnect(int id){
        WSServiceClient client = iClients.get(id);
        if(client!=null){
            client.disconnect();
        }
    }
    
    public boolean containsClient(String userID){
        return sClients.containsKey(userID);
    }
    
    public WSServiceClient getClientWithID(String userID){
        return sClients.get(userID);
    }
    
    public void sendDataWithID(String userID, byte[] data){
        WSServiceClient cl =  sClients.get(userID);
        GoldIncUser u = GoldIncUserMng.i().getUser(userID);
        if(u!=null&&u.isLogin){
            cl.send(data);
        }
    }
    
    public void sendAllClient(byte[] data){
        for(WSServiceClient c:getListClient()){
            GoldIncUser u = GoldIncUserMng.i().getUser(c.getUserID());
            if(u!=null&&u.isLogin){
                c.send(data);
            }
        }
    }
        
    // </editor-fold>
    
}
