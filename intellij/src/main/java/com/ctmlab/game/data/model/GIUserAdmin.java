/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ctmlab.game.data.model;

import com.dp.db.DBDataPostgres;
import com.dp.db.DataFieldType;

/**
 *
 * @author hhtri
 */
public class GIUserAdmin extends DBDataPostgres{
    @DataFieldType(primaryKey = true)
    public String username;
    public String password;
    public long created;
    public long lastlogin;
    public String createdby;
    public GIUserAdmin(){
        username = password = createdby = "";
        created = lastlogin = -1;
    }
    public GIUserAdmin(String username, String password, String createdby){
        this.username = username;
        this.password = password;
        this.createdby = createdby;
        created = System.currentTimeMillis();
        lastlogin = -1;
    }
}
