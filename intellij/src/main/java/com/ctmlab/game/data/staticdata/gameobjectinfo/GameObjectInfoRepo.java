/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ctmlab.game.data.staticdata.gameobjectinfo;

import com.ctmlab.game.goldincenum.ENUM_DECOR_MAP_TYPE;
import com.ctmlab.game.goldincenum.ENUM_UNIT_TYPE;
import com.ctmlab.game.manager.GlobalConfig;
import com.ctmlab.game.manager.StaticDataMng;
import com.ctmlab.util.Utils;
import com.dp.db.Func;
import com.google.common.io.LittleEndianDataInputStream;
import com.google.common.io.LittleEndianDataOutputStream;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.TreeMap;

/**
 *
 * @author hhtri
 */
public class GameObjectInfoRepo {

    public String version;
    public TreeMap<Byte, GameObjectInfo> lstObjectMap;
    public TreeMap<Byte, GameObjectInfo> lstObjectDecor;

    public GameobjectDiggingDirtInfo diggingInfo = null;
    public GameObjectWashingDirtInfo washingDirtInfo = null;

    public GameObjectInfoRepo() {
        version = "";
        lstObjectMap = new TreeMap<>();
        lstObjectDecor = new TreeMap<>();
        diggingInfo = new GameobjectDiggingDirtInfo();
        washingDirtInfo = new GameObjectWashingDirtInfo();
    }

    public GameObjectInfo getDecorInfo(byte t) {
        return lstObjectDecor.get(t);
    }

    public GameObjectInfo getGameOjectInfo(byte t) {
        return lstObjectMap.get(t);
    }

    public void init(String v) {
        try {
            String json = Utils.i().readTextFile(GlobalConfig.pathConfig + File.separator + "gameobjectinfo.json");
            JsonObject objAll = Func.gson.fromJson(json, JsonObject.class);
            if (objAll.has("version") && objAll.has("lstObjectMap") && objAll.has("lstObjectDecor")) {
                version = v;
                JsonObject obj = objAll.get("lstObjectMap").getAsJsonObject();
                for (Entry<String, JsonElement> e : obj.entrySet()) {
                    byte key_type = Byte.valueOf(e.getKey());
                    JsonObject temp = e.getValue().getAsJsonObject();
                    GameObjectInfo clazz = new GameObjectInfo();
                    clazz.readData(temp);
                    lstObjectMap.put(key_type, clazz);
                }
                HashMap<ENUM_UNIT_TYPE, String> hashFile = new HashMap<>();
                hashFile.put(ENUM_UNIT_TYPE.HOME_BARRACK, "barracks.json");
                hashFile.put(ENUM_UNIT_TYPE.HOME_DEFENSE, "defense.json");
                hashFile.put(ENUM_UNIT_TYPE.HOME_EXPLORATION, "exploration.json");
                hashFile.put(ENUM_UNIT_TYPE.HOME_OFFICE, "office.json");
                hashFile.put(ENUM_UNIT_TYPE.HOME_REFINERY, "refinery.json");
                hashFile.put(ENUM_UNIT_TYPE.HOME_TRANSPORTATION, "transportation.json");
                hashFile.put(ENUM_UNIT_TYPE.HOME_WALL, "protective.json");
                hashFile.put(ENUM_UNIT_TYPE.HOME_WEAPON, "weapon.json");
                hashFile.put(ENUM_UNIT_TYPE.OIL_RIG_BASIC, "oil_rig_basic.json");
                hashFile.put(ENUM_UNIT_TYPE.OIL_RIG_ADVANCE, "oil_rig_advance.json");
                hashFile.put(ENUM_UNIT_TYPE.OIL_RIG_ULTRA, "oil_rig_ultra.json");

                for(Entry<ENUM_UNIT_TYPE, String> e:hashFile.entrySet()){
                    json = Utils.i().readTextFile(GlobalConfig.pathConfig + File.separator + e.getValue());
                    ENUM_UNIT_TYPE eType = e.getKey();
                    lstObjectMap.put(eType.getCode(), GameObjectInfo.initFromJson(eType, json));
                }

//                // barrack
//                json = Utils.i().readTextFile(GlobalConfig.pathConfig + File.separator + "barracks.json");
//                lstObjectMap.put(ENUM_UNIT_TYPE.HOME_BARRACK.getCode(), GameObjectInfo.initFromJson(ENUM_UNIT_TYPE.HOME_BARRACK, json));
//                // defense
//                json = Utils.i().readTextFile(GlobalConfig.pathConfig + File.separator + "defense.json");
//                lstObjectMap.put(ENUM_UNIT_TYPE.HOME_DEFENSE.getCode(), GameObjectInfo.initFromJson(ENUM_UNIT_TYPE.HOME_DEFENSE, json));
//                // exploration
//                json = Utils.i().readTextFile(GlobalConfig.pathConfig + File.separator + "exploration.json");
//                lstObjectMap.put(ENUM_UNIT_TYPE.HOME_EXPLORATION.getCode(), GameObjectInfo.initFromJson(ENUM_UNIT_TYPE.HOME_EXPLORATION, json));
//                // office
//                json= Utils.i().readTextFile(GlobalConfig.pathConfig + File.separator + "office.json");
//                lstObjectMap.put(ENUM_UNIT_TYPE.HOME_OFFICE.getCode(), GameObjectInfo.initFromJson(ENUM_UNIT_TYPE.HOME_OFFICE, json));
//                // refinely
//                json = Utils.i().readTextFile(GlobalConfig.pathConfig + File.separator + "refinery.json");
//                lstObjectMap.put(ENUM_UNIT_TYPE.HOME_REFINERY.getCode(), GameObjectInfo.initFromJson(ENUM_UNIT_TYPE.HOME_REFINERY, json));
//                // transportation
//                json = Utils.i().readTextFile(GlobalConfig.pathConfig + File.separator + "transportation.json");
//                lstObjectMap.put(ENUM_UNIT_TYPE.HOME_TRANSPORTATION.getCode(), GameObjectInfo.initFromJson(ENUM_UNIT_TYPE.HOME_TRANSPORTATION, json));
//                // wall
//                json = Utils.i().readTextFile(GlobalConfig.pathConfig + File.separator + "protective.json");
//                lstObjectMap.put(ENUM_UNIT_TYPE.HOME_WALL.getCode(), GameObjectInfo.initFromJson(ENUM_UNIT_TYPE.HOME_WALL, json));
//                // weapon
//                json = Utils.i().readTextFile(GlobalConfig.pathConfig + File.separator + "weapon.json");
//                lstObjectMap.put(ENUM_UNIT_TYPE.HOME_WEAPON.getCode(), GameObjectInfo.initFromJson(ENUM_UNIT_TYPE.HOME_WEAPON, json));
//                // oil_rig_basic
//                json = Utils.i().readTextFile(GlobalConfig.pathConfig + File.separator + "oil_rig_basic.json");
//                lstObjectMap.put(ENUM_UNIT_TYPE.OIL_RIG_BASIC.getCode(), GameObjectInfo.initFromJson(ENUM_UNIT_TYPE.OIL_RIG_BASIC, json));
//                // oil_rig_advance
//                json = Utils.i().readTextFile(GlobalConfig.pathConfig + File.separator + "oil_rig_advance.json");
//                lstObjectMap.put(ENUM_UNIT_TYPE.OIL_RIG_ADVANCE.getCode(), GameObjectInfo.initFromJson(ENUM_UNIT_TYPE.OIL_RIG_ADVANCE, json));
//                // oil_rig_ultra
//                json = Utils.i().readTextFile(GlobalConfig.pathConfig + File.separator + "oil_rig_ultra.json");
//                lstObjectMap.put(ENUM_UNIT_TYPE.OIL_RIG_ULTRA.getCode(), GameObjectInfo.initFromJson(ENUM_UNIT_TYPE.OIL_RIG_ULTRA, json));

                obj = objAll.get("lstObjectDecor").getAsJsonObject();
                for (Entry<String, JsonElement> e : obj.entrySet()) {
                    byte key_type = Byte.valueOf(e.getKey());
                    JsonObject temp = e.getValue().getAsJsonObject();
                    GameObjectInfo clazz = new GameObjectInfo();
                    clazz.readData(temp);
                    clazz.mType = key_type;
                    lstObjectDecor.put(key_type, clazz);
                }
            }
            String path = GlobalConfig.pathBackup + File.separator + "gameobjectinfo.bin";
            writeGameObjectInfo(path);
            path = GlobalConfig.pathBackup + File.separator + "gameobjectinfo.json";
            Utils.i().writeTextFile(path, Func.json(this));
        } catch (Exception ex) {
            System.out.println(Func.toString(ex));
        }
    }

    public void readGameObjectInfo(String pathBin) {
        byte[] data = Utils.i().readFile(pathBin);
        ByteArrayInputStream bais = new ByteArrayInputStream(data);
        try (LittleEndianDataInputStream is = new LittleEndianDataInputStream(bais)) {
            version = Utils.i().readBigString(is);
            int len = is.readInt();
            int i = 0;

            lstObjectMap.clear();
            while (i++ < len) {
                byte key = is.readByte();
                GameObjectInfo clazz = GameObjectInfo.readObjectData(is);
                if (clazz != null) {
                    lstObjectMap.put(key, clazz);
                }
            }
            len = is.readInt();
            i = 0;
            lstObjectDecor.clear();
            while (i++ < len) {
                byte key = is.readByte();
                GameObjectInfo clazz = GameObjectInfo.readDecorData(is);
                if (clazz != null) {
                    lstObjectDecor.put(key, clazz);
                }
            }
        } catch (Exception ex) {
            System.out.println(Func.toString(ex));
        }
    }

    public void writeGameObjectInfo(String pathBin) {
        version = StaticDataMng.i().setNewVersion("gameobjectinfo");
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        try (LittleEndianDataOutputStream os = new LittleEndianDataOutputStream(baos)) {
            Utils.i().writeBigString(os, version);
            os.writeInt(lstObjectMap.size());
            for (Entry<Byte, GameObjectInfo> e : lstObjectMap.entrySet()) {
                // order in enum ENUM_UNIT_TYPE
                os.writeByte(e.getKey());
                // type extend from GameObjectInfo
                e.getValue().writeData(os);
            }
            os.writeInt(lstObjectDecor.size());
            for (Entry<Byte, GameObjectInfo> e : lstObjectDecor.entrySet()) {
                // order in enum DECOR_MAP_TYPE
                os.writeByte(e.getKey());
                // type extend from GameObjectInfo
                e.getValue().writeData(os);
            }
        } catch (Exception ex) {
        }
        Utils.i().writeBinFile(pathBin, baos.toByteArray());
    }

    // using for editor
    // object map - building in homebase, claim
    public String getObjectUnit(String type){
        String rs = "";
        byte key = (byte)ENUM_UNIT_TYPE.get(type);
        if(lstObjectMap.containsKey(key)){
            rs = lstObjectMap.get(key).toJson();
        }
        return rs;
    }
    public String setObjectUnit(String type, String json){
        String rs = "";
        byte key = (byte)ENUM_UNIT_TYPE.get(type);
        if(lstObjectMap.containsKey(key)){
            rs = lstObjectMap.get(key).fromJson(json);
        }
        return rs;
    }
    // object map - decor unit (static)
    public String getObjectDecor(String type){
        String rs = "";
        byte key = (byte)ENUM_DECOR_MAP_TYPE.get(type);
        if(lstObjectDecor.containsKey(key)){
            rs = lstObjectDecor.get(key).toJson();
        }
        return rs;
    }
    public String setObjectDecor(String type, String json){
        String rs = "";
        byte key = (byte)ENUM_DECOR_MAP_TYPE.get(type);
        if(lstObjectDecor.containsKey(key)){
            rs = lstObjectDecor.get(key).fromJson(json);
        }
        return rs;
    }
    
    // using for testing
    public byte[] getBytes() {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        try (LittleEndianDataOutputStream os = new LittleEndianDataOutputStream(baos)) {
            Utils.i().writeBigString(os, version);
            os.writeInt(lstObjectMap.size());
            for (Entry<Byte, GameObjectInfo> e : lstObjectMap.entrySet()) {
                os.writeByte(e.getKey());
                e.getValue().writeData(os);
            }
            os.writeInt(lstObjectDecor.size());
            for (Entry<Byte, GameObjectInfo> e : lstObjectDecor.entrySet()) {
                os.writeByte(e.getKey());
                e.getValue().writeData(os);
            }
        } catch (Exception ex) {
        }
        return baos.toByteArray();
    }
}
