/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ctmlab.game.data.pojo;

import com.ctmlab.game.data.staticdata.PointShort;

import java.util.ArrayList;
import java.util.TreeMap;

/**
 *
 * @author hhtri
 */
public class TradeTransaction {
    
    public static final int ACTION_BUY = 1;
    public static final int ACTION_SELL = 2;
    public static final int ACTION_TRANS_TO_CLAIM = 3;
    public static final int ACTION_TRANS_FUEL = 4;
    public static final int ACTION_TRANS_TO_OILMINE = 5;

    private TreeMap<Byte, Long> lstTrade;
    private ArrayList<Long> lstEquipment;
    private ArrayList<Long> lstWorker;
    private TreeMap<Byte, Long> lstResult;
    private ArrayList<PointShort> path;
    private boolean canTransport;
    private int typeTranstport;
    private long truckID = -1;
    
    public TradeTransaction(){
        setLstTrade(new TreeMap());
        setLstResult(new TreeMap());
        setLstEquipment(new ArrayList());
        setLstWorker(new ArrayList());
        setPath(new ArrayList());
        setCanTransport(false);
        setTypeTranstport(-1);
        setTruckID(-1);
    }
    
    public void unload(){
       setCanTransport(false);
       getLstTrade().clear();
       getLstResult().clear();
       getLstEquipment().clear();
       getLstWorker().clear();
       getPath().clear();
    }

    public TreeMap<Byte, Long> getLstTrade() {
        return lstTrade;
    }

    public void setLstTrade(TreeMap<Byte, Long> lstTrade) {
        this.lstTrade = lstTrade;
    }

    public ArrayList<Long> getLstEquipment() {
        return lstEquipment;
    }

    public void setLstEquipment(ArrayList<Long> lstEquipment) {
        this.lstEquipment = lstEquipment;
    }

    public ArrayList<Long> getLstWorker() {
        return lstWorker;
    }

    public void setLstWorker(ArrayList<Long> lstWorker) {
        this.lstWorker = lstWorker;
    }

    public TreeMap<Byte, Long> getLstResult() {
        return lstResult;
    }

    public void setLstResult(TreeMap<Byte, Long> lstResult) {
        this.lstResult = lstResult;
    }

    public ArrayList<PointShort> getPath() {
        return path;
    }

    public void setPath(ArrayList<PointShort> path) {
        this.path = path;
    }

    public boolean isCanTransport() {
        return canTransport;
    }

    public void setCanTransport(boolean canTransport) {
        this.canTransport = canTransport;
    }

    public int getTypeTranstport() {
        return typeTranstport;
    }

    public void setTypeTranstport(int typeTranstport) {
        this.typeTranstport = typeTranstport;
    }

    public long getTruckID() {
        return truckID;
    }

    public void setTruckID(long truckID) {
        this.truckID = truckID;
    }
}
