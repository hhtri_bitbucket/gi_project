/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ctmlab.game.data.staticdata;

import com.ctmlab.game.data.ctmon.CTMONDataFieldType;
import com.ctmlab.game.data.model.*;
import com.ctmlab.game.data.pojo.GameResources;
import com.ctmlab.game.data.staticdata.gameobjectinfo.OilRigBasicInfo;
import com.ctmlab.game.data.staticdata.gameobjectinfo.GameObjectInfo;
import com.ctmlab.game.goldincenum.ENUM_DECOR_MAP_TYPE;
import com.ctmlab.game.goldincenum.ENUM_MATRIX_TYPE;
import com.ctmlab.game.goldincenum.ENUM_UNIT_TYPE;
import com.ctmlab.game.goldincenum.ZONE_LAYER;
import com.ctmlab.game.manager.GlobalConfig;
import com.ctmlab.game.manager.StaticDataMng;
import com.ctmlab.game.network.manager.WSGameDefine;
import com.ctmlab.game.pathfinding.astartalgorithm.A_StarAlgorithm;
import com.ctmlab.util.Utils;
import com.dp.db.Func;
import com.google.common.io.LittleEndianDataInputStream;
import com.google.common.io.LittleEndianDataOutputStream;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.annotations.ExcluseJson;

import java.awt.*;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Random;

/**
 *
 * @author hhtri
 */
public class Zone implements DataStreamListener {

    @CTMONDataFieldType(ignore = true)
    private final Object LOCK = new Object();

    public short id;
    public short id_r;
    public String name;
    public ArrayList<Integer> pos;
    public ArrayList<DecorLayer> layerDecor = new ArrayList<>();
    @CTMONDataFieldType(ignore = true)
    public ArrayList<GameObjectMap> layerGameObject = new ArrayList<>();

    @CTMONDataFieldType(ignore = true)
    @ExcluseJson(scope = ExcluseJson.ALL)
    private byte[][] matrix = null;
    @CTMONDataFieldType(ignore = true)
    @ExcluseJson(scope = ExcluseJson.ALL)
    private int mWidth = 0;
    @CTMONDataFieldType(ignore = true)
    @ExcluseJson(scope = ExcluseJson.ALL)
    private int mHeight = 0;

    private HashMap<String, Long> hashTest = new HashMap();

    public Zone() {
        id = 0;
        pos = new ArrayList();
        layerDecor = new ArrayList<>();
    }

    public Zone(int w, int h) {
        id = 0;
        pos = new ArrayList();
        layerDecor = new ArrayList<>();
        matrix = new byte[w][h];
        for (int i = 0; i < w; i++) {
            for (int j = 0; j < h; j++) {
                matrix[i][j] = ENUM_MATRIX_TYPE.NONE.getCode();
            }
        }
        mWidth = w;
        mHeight = h;
    }

    public byte[][] getRawMatrix(){
        byte[][] tMatrix = new byte[mWidth][mHeight];
        byte[][] realMatrix = matrix.clone();
        for(int i=0;i<mWidth;i++){
            for(int j=0;j<mHeight;j++){
                tMatrix[i][j] = realMatrix[i][j];
            }
        }
        return tMatrix;
    }

    public byte[][] getRawMatrix(short x, short y, GameObjectMap obj){
        byte[][] rs = getRawMatrix();
        obj.refInfo();
        GameObjectInfo info = obj.objInfo;
        int r = info.width;
        int c = info.height;
        int w = mWidth;
        int h = mHeight;
        if (r == 0 || c == 0) {
            rs[x][y] = -1;
            return rs;
        }
        for (int i = 0; i < r; i++) {
            int _x = x + i;
            for (int j = 0; j < c; j++) {
                int _y = y + j;
                boolean flag = ((_x >= 0) && (_x < w) && (_y >= 0) && (_y < h));
                if (flag) {
                    rs[_x][_y] = -1;
                }
            }
        }
        return rs;
    }

    public int getWidth(){return mWidth;}

    public int getHeight(){return mHeight;}

    public int x(){
        Region r = StaticDataMng.i().worldMap.getRegion(id_r);
        return pos.get(0) + r.pos.get(0);
    }

    public int y(){
        Region r = StaticDataMng.i().worldMap.getRegion(id_r);
        return pos.get(1) + r.pos.get(1);
    }

    private GameObjectInfo getDecorInfo(byte type) {
        return StaticDataMng.i().getRepoGameObjectInfo().getDecorInfo(type);
    }

    private GameObjectInfo getGameOjectInfo(byte type) {
        return StaticDataMng.i().getRepoGameObjectInfo().getGameOjectInfo(type);
    }

    private short getShort(Random rd, int max) {
        short rs = 0;
        rs = (short) rd.nextInt(max);
        while (!(rs >= 0 && rs % 3 == 0)) {
            rs = (short) rd.nextInt(max);
        }
        return rs;
    }

    private void updateMatrix(int x, int y, int w, int h, byte state, int r, int c) {
        if (r == 0 || c == 0) {
            matrix[x][y] = state;
            return;
        }
        for (int i = 0; i < r; i++) {
            int _x = x + i;
            for (int j = 0; j < c; j++) {
                int _y = y + j;
                boolean flag = ((_x >= 0) && (_x < w) && (_y >= 0) && (_y < h));
                if (flag) {
                    matrix[_x][_y] = state;
                }
            }
        }
    }

    private int isMountainAvailable(int x, int y, int r, int c) {
        for (int i = 0; i < r; i++) {
            int _x = x + i;
            if (_x < mWidth) {
                for (int j = 0; j < c; j++) {
                    int _y = y + j;
                    if (_y < mHeight) {
                        if (matrix[_x][_y] == ENUM_MATRIX_TYPE.TREE.getCode()
                                || matrix[_x][_y] == ENUM_MATRIX_TYPE.HOME_BASE.getCode()
                                || matrix[_x][_y] == ENUM_MATRIX_TYPE.TRADE_CENTER.getCode()
                                || matrix[_x][_y] == ENUM_MATRIX_TYPE.OIL_MINE.getCode()
                                || matrix[_x][_y] == ENUM_MATRIX_TYPE.MINE.getCode()) {
                            return 1;
                        }
                    }
                }
            }
        }
        return 0;
    }

    private int isTreesAvailable(int x, int y, int r, int c) {
        for (int i = 0; i < r; i++) {
            int _x = x + i;
            if (_x < mWidth) {
                for (int j = 0; j < c; j++) {
                    int _y = y + j;
                    if (_y < mHeight) {
                        if (matrix[_x][_y] == ENUM_MATRIX_TYPE.MOUNTAIN.getCode()
                                || matrix[_x][_y] == ENUM_MATRIX_TYPE.HOME_BASE.getCode()
                                || matrix[_x][_y] == ENUM_MATRIX_TYPE.TRADE_CENTER.getCode()
                                || matrix[_x][_y] == ENUM_MATRIX_TYPE.OIL_MINE.getCode()
                                || matrix[_x][_y] == ENUM_MATRIX_TYPE.MINE.getCode()) {
                            return 1;
                        }
                    }
                }
            }
        }
        return 0;
    }

    private int isPosAvailable(int x, int y) {
        if (!(matrix[x][y] == ENUM_MATRIX_TYPE.NONE.getCode())) {
            return 1;
        }
        return 0;
    }

    private int isHomeBasePosAvailable(int x, int y, int r, int c) {
        if ((x + r) >= mWidth || (y + c) >= mHeight) {
            return 1;
        }
        byte none = ENUM_MATRIX_TYPE.NONE.getCode();
        for (int i = 0; i < r; i++) {
            int _x = x + i;
            for (int j = 0; j < c; j++) {
                int _y = y + j;
                if (_x < mWidth && _y < mHeight && matrix[_x][_y] != none) {
                    return 1;
                }
            }
        }
        return 0;
    }

    public void cleanupStaticObject() {
        int w = mWidth;
        int h = mHeight;
        for (int i = 0; i < w; i++) {
            for (int j = 0; j < h; j++) {
                matrix[i][j] = ENUM_MATRIX_TYPE.NONE.getCode();
            }
        }
        layerDecor.forEach((dl) -> {
            dl.data.clear();
        });
        for (GameObjectMap obj : layerGameObject) {
            byte gameobject = -1;
            if (obj instanceof TradeCenterMap) {gameobject = ENUM_MATRIX_TYPE.TRADE_CENTER.getCode();}
            else if (obj instanceof HomeBase) {gameobject = ENUM_MATRIX_TYPE.HOME_BASE.getCode();}
            else if (obj instanceof OilMine) {gameobject = ENUM_MATRIX_TYPE.OIL_MINE.getCode();}
            else {gameobject = ENUM_MATRIX_TYPE.MINE.getCode();}
            GameObjectInfo info = getDecorInfo(obj.mType);
            updateMatrix(obj.xPos + info.width / 2, obj.yPos + info.height / 2, mWidth, mHeight, gameobject, info.width, info.height);
            info = getDecorInfo((byte) 26);
            updateMatrix(obj.xPos + info.width / 2, obj.yPos + info.height / 2, mWidth, mHeight, ENUM_MATRIX_TYPE.GROUND.getCode(), info.width, info.height);
        }
    }

    public String getMatrix() {
        HashMap<Byte, String> hash = new HashMap();
        hash.put(ENUM_MATRIX_TYPE.NONE.getCode(), "#000000");
        hash.put(ENUM_MATRIX_TYPE.HOME_BASE.getCode(), "#ff0000");
        hash.put(ENUM_MATRIX_TYPE.MINE.getCode(), "#ff0000");
        hash.put(ENUM_MATRIX_TYPE.OIL_MINE.getCode(), "#ff0000");
        hash.put(ENUM_MATRIX_TYPE.TRADE_CENTER.getCode(), "#D1D710");
        hash.put(ENUM_MATRIX_TYPE.TREE.getCode(), "#00ff00");
        hash.put(ENUM_MATRIX_TYPE.MOUNTAIN.getCode(), "#AB9D39");
        hash.put(ENUM_MATRIX_TYPE.GROUND.getCode(), "#D77A10");
        hash.put(ENUM_MATRIX_TYPE.WATER.getCode(), "#5588ee");

        StringBuilder sb = new StringBuilder();
        StringBuilder sbLine = new StringBuilder();
        String btnTemp = "<button type='button' id='#id' onclick='btnOnClick(\"#id\")' "
                + "style='width:25px;height:25px;font-size:8px;text-align:center;"
                + "background-color: #c;font-weight: 900;'>#v</button>";
//        String btnTemp = "<button type='button' id='#id' onclick='btnOnClick(\"#id\")' "
//                + "style='width:40px;height:40px;font-size:8px;text-align:center;"
//                + "background-color: #c;font-weight: 900;'>#v</button>";

        for (int i = 0; i < mWidth; i++) {
//        for (int i = mWidth-1; i >=0; i--) {
            String s = "";
            for (int j = 0; j < mHeight; j++) {
//            for (int j = mHeight-1; j >=0; j--) {
                String temp = btnTemp;
                byte b = matrix[i][j];
                String c = hash.get(b);
                if (c == null) {
                    c = "#F9A896";
                }
                temp = temp.replace("#c", c);
                temp = temp.replace("#v", b + "");
//                temp = temp.replace("#v", String.format("(%d,%d)",i, j) + "");
                temp = temp.replaceAll("#id", i + "_" + j);
                sbLine.append(temp);
            }
            sb.append(sbLine.toString()).append("</br>");
            sbLine.setLength(0);
        }
        return sb.toString();
    }

    public String getMatrixEx(long startID, long endID) {
        byte[][] tMatrix = getRawMatrix();
        GameObjectMap start = null;
        GameObjectMap end = null;
        if(startID!=endID&&startID!=0&&endID!=0){
            for(GameObjectMap o:layerGameObject){
                if(o.id==startID){
                    start = o;
                } else if(o.id==endID){
                    end = o;
                } else if(start!=null&&end!=null){
                    break;
                }
            }
        } else{
            Random rd = new Random();
            start = layerGameObject.get(rd.nextInt(layerGameObject.size()));
            end = layerGameObject.get(rd.nextInt(layerGameObject.size()));
        }

        Point from = new Point(start.xPos, start.yPos);
        Point to = new Point(end.xPos, end.yPos);

        ArrayList<PointInt> rs = A_StarAlgorithm.findPathEx(tMatrix, from, to);
        rs.forEach((p) -> {tMatrix[p.x][p.y] = 20;});

        HashMap<Byte, String> hash = new HashMap();
        hash.put(ENUM_MATRIX_TYPE.NONE.getCode(), "#000000");
        hash.put(ENUM_MATRIX_TYPE.HOME_BASE.getCode(), "#ff0000");
        hash.put(ENUM_MATRIX_TYPE.MINE.getCode(), "#ff0000");
        hash.put(ENUM_MATRIX_TYPE.OIL_MINE.getCode(), "#ff0000");
        hash.put(ENUM_MATRIX_TYPE.TRADE_CENTER.getCode(), "#D1D710");
        hash.put(ENUM_MATRIX_TYPE.TREE.getCode(), "#00ff00");
        hash.put(ENUM_MATRIX_TYPE.MOUNTAIN.getCode(), "#AB9D39");
        hash.put(ENUM_MATRIX_TYPE.GROUND.getCode(), "#D77A10");
        hash.put(ENUM_MATRIX_TYPE.WATER.getCode(), "#5588ee");
        hash.put((byte)20, "#ffff00");

        StringBuilder sb = new StringBuilder();
        StringBuilder sbLine = new StringBuilder();
        String btnTemp = "<button type='button' id='#id' onclick='btnOnClick(\"#id\")' "
                + "style='width:25px;height:25px;font-size:8px;text-align:center;"
                + "background-color: #c;font-weight: 900;'>#v</button>";

        for (int i = 0; i < mWidth; i++) {
            String s = "";
            for (int j = 0; j < mHeight; j++) {
                String temp = btnTemp;
                byte b = tMatrix[i][j];
                String c = hash.get(b);
                if (c == null) {c = "#F9A896";}
                temp = temp.replace("#c", c);
                String sData = hashTest.containsKey(i + "_" + j)?(hashTest.get(i + "_" + j) + ""):(i + "_" + j);
                if(hashTest.containsKey(i + "_" + j)){
                    temp = temp.replace("#v", "-");
                } else{
                    temp = temp.replace("#v", b + "");
                }

                temp = temp.replaceAll("#id", sData);
                sbLine.append(temp);
            }
            sb.append(sbLine.toString()).append("</br>");
            sbLine.setLength(0);
        }
        return sb.toString();
    }

    public void syncListPosExisted(int w, int h) {
        matrix = new byte[w][h];
        for (int i = 0; i < w; i++) {
            for (int j = 0; j < h; j++) {
                matrix[i][j] = ENUM_MATRIX_TYPE.NONE.getCode();
            }
        }
        mWidth = w;
        mHeight = h;
        DecorLayer layer = getLayerDecorBy((byte) ZONE_LAYER.TREE.getCode());
        GameObjectInfo info = null;
        for (GameObjectMap obj : layer.data) {
            info = getDecorInfo(obj.mType);
            ENUM_MATRIX_TYPE state = (obj.mType > 6) ? ENUM_MATRIX_TYPE.TREE : ENUM_MATRIX_TYPE.MOUNTAIN;
            updateMatrix(obj.xPos, obj.yPos, w, h, state.getCode(), info.width, info.height);
            if(obj.mType<7){
                hashTest.put(obj.xPos+"_"+obj.yPos, (long)obj.mType);
            }
            obj.refInfo();
        }
        layer = getLayerDecorBy((byte) ZONE_LAYER.GROUND.getCode());
        for (GameObjectMap obj : layer.data) {
            info = getDecorInfo(obj.mType);
            updateMatrix(obj.xPos, obj.yPos, w, h, ENUM_MATRIX_TYPE.GROUND.getCode(), info.width, info.height);
            obj.refInfo();
        }
        layer = getLayerDecorBy((byte) ZONE_LAYER.SHORE.getCode());
        for (GameObjectMap obj : layer.data) {
            info = getDecorInfo(obj.mType);
            updateMatrix(obj.xPos, obj.yPos, w, h, ENUM_MATRIX_TYPE.SHORE.getCode(), info.width, info.height);
            obj.refInfo();
        }
        layer = getLayerDecorBy((byte) ZONE_LAYER.WATER.getCode());
        for (GameObjectMap obj : layer.data) {
            updateMatrix(obj.xPos, obj.yPos, w, h, ENUM_MATRIX_TYPE.WATER.getCode(), 1, 1);
            obj.refInfo();
        }
    }

    // static
    public void genTrees(short id_r, int len) {
        DecorLayer layper = getLayerDecorBy((byte) ZONE_LAYER.TREE.getCode());
        Random rd = new Random();
        short x = -1;
        short y = -1;
        int i = 0;
        while (i++ < len) {
            x = getShort(rd, mWidth);
            y = getShort(rd, mHeight);
            byte typeTrees = (byte) (ENUM_DECOR_MAP_TYPE.beginTrees() + rd.nextInt(6));
            GameObjectInfo info = getDecorInfo(typeTrees);
            while (isTreesAvailable(x, y, info.width, info.height) == 1) {
                x = (short) rd.nextInt(mWidth);
                y = (short) rd.nextInt(mHeight);
            }
            GameObjectMap obj = new GameObjectMap();
            obj.id_r = id_r;
            obj.id_z = id;
            obj.mType = typeTrees;
            obj.xPos = x;
            obj.yPos = y;
            obj.refInfo();
            layper.addItem(obj);
            updateMatrix(x, y, mWidth, mHeight, ENUM_MATRIX_TYPE.TREE.getCode(), info.width, info.height);
            hashTest.put(x+"_"+y, (long)typeTrees);
        }
    }

    public void genMountains(short id_r, int len) {
        DecorLayer layper = getLayerDecorBy((byte) ZONE_LAYER.TREE.getCode());
        DecorLayer layperG = getLayerDecorBy((byte) ZONE_LAYER.GROUND.getCode());
        int i = 0;
        Random rd = new Random();
        short x = -1;
        short y = -1;
        while (i++ < len) {
            x = getShort(rd, mWidth);
            y = getShort(rd, mHeight);
            byte typeMountain = (byte) rd.nextInt(7);
            GameObjectInfo info = getDecorInfo(typeMountain);
            while (isMountainAvailable(x, y, info.width, info.height) == 1) {
                x = (short) rd.nextInt(mWidth);
                y = (short) rd.nextInt(mHeight);
            }
            GameObjectMap obj = new GameObjectMap();
            obj.id_r = id_r;
            obj.id_z = id;
            obj.mType = typeMountain;
            obj.xPos = x;
            obj.yPos = y;
            obj.refInfo();
            layper.addItem(obj);
            updateMatrix(x, y, mWidth, mHeight, ENUM_MATRIX_TYPE.MOUNTAIN.getCode(), info.width, info.height);
            hashTest.put(x+"_"+y, (long)typeMountain);

            GameObjectMap objg = new GameObjectMap();
            objg.id_r = id_r;
            objg.id_z = id;
            objg.mType = (byte) 26;
            objg.xPos = (short) (x + info.width / 2);
            objg.yPos = (short) (y + info.height / 2);
            objg.refInfo();
            layperG.addItem(objg);
            info = getDecorInfo(objg.mType);
            updateMatrix(x, y, mWidth, mHeight, ENUM_MATRIX_TYPE.GROUND.getCode(), info.width, info.height);
        }
    }

    private class ItemRiver {
        int step;
        int root;
        int id;
        int x;
        int y;
        int mode;
        public ItemRiver(int x, int y, int idRoot, int id, int step, int mode) {
            this.x = x;
            this.y = y;
            this.id = id;
            this.root = idRoot;
            this.step = step;
            this.mode = mode;
        }
        public int _x() {return x;}
        public int _y() {return y;}
        public int _id() {return id;}
        public int _root() {return id;}
        public int _step() {return step;}
        public int _mode() {return mode;}
        public String print() {return String.format("[Step, root, id, x, y, mode] - [%d, %d, %d, %d, %d, %d]", step, root, id, x, y, mode);}
    }

    public ArrayList<PointRiver> genRivers(short id_r, PointInt root, int mode, boolean hasBranch) {

        ArrayList<PointRiver> rs = new ArrayList<>();

        DecorLayer layper = getLayerDecorBy((byte) ZONE_LAYER.WATER.getCode());
        Random rd = new Random();
        JsonObject data = Func.gson.fromJson(Utils.i().readTextFile("/home/hhtri/Desktop/workspace/netbean/external/config/rivers.json"), JsonObject.class);
        JsonArray lstRivers = data.getAsJsonArray("lstRiver");

        int x = root.x;
        int y = root.y;
        int _id = 37;

        if(!hasBranch){
            if(mode==-1){
                while((_id==37||_id==40)){_id = 36 + rd.nextInt(6);}
            } else{
                while(_id==37){_id = 36 + rd.nextInt(6);}
            }
        } else{
            mode = 0;
        }

        Queue<ItemRiver> queue = new LinkedList<>();
        int step = 0;
        queue.add(new ItemRiver(x, y, -1, _id, step++, mode));
        while (!queue.isEmpty()) {
            ItemRiver ir = queue.poll();
            System.out.println(ir.print());

            GameObjectMap obj = new GameObjectMap();
            obj.id_r = id_r;
            obj.id_z = this.id;
            obj.mType = (byte) ir._id();
            obj.xPos = (short) ir._x();
            obj.yPos = (short) ir._y();
            obj.refInfo();
            layper.addItem(obj);
            updateMatrix(ir._x(), ir._y(), mWidth, mHeight, ENUM_MATRIX_TYPE.WATER.getCode(), 1, 1);
            JsonArray listConnectPoint = null;
            JsonArray arr = null;
            for (JsonElement e : lstRivers) {
                JsonObject o = e.getAsJsonObject();
                if (ir._id() == o.get("id").getAsInt()) {
                    arr = o.getAsJsonArray("connectPoint");
                    break;
                }
            }
            if (arr != null) {
                if(hasBranch){
                    for(int i=0;i<arr.size();i++){

                    }
                } else{
                    listConnectPoint = arr.get(0).getAsJsonArray();
                    JsonObject newObj = null;
                    int n = listConnectPoint.size();
                    while(true){
                        int idx = rd.nextInt(n);
                        newObj = listConnectPoint.get(idx).getAsJsonObject();
                        if(newObj.get("idRiver").getAsInt()==37){continue;}
                        else{break;}
                    }
//                    ArrayList<JsonObject> aNegative = new ArrayList<>();
//                    ArrayList<JsonObject> aPositive = new ArrayList<>();
//                    for(int i=0; i<listConnectPoint.size(); i++){
//                        JsonObject newObj = listConnectPoint.get(i).getAsJsonObject();
//                        if(newObj.get("idRiver").getAsInt()==37){continue;}
//                        if(ir._mode()==-1 && newObj.get("idRiver").getAsInt()==40){continue;}
//                        int xMode = newObj.get("relatedPos").getAsJsonArray().get(0).getAsInt();
//                        if(xMode>=0){aPositive.add(newObj);}
//                        else{aNegative.add(newObj);}
//                    }
//                    JsonObject newObj = null;
////                    System.out.println("+++++++++++++++++" + ir.id);
//                    if(ir._mode()==-1){
//                        newObj = aNegative.get(rd.nextInt(aNegative.size()));
//                    } else if(ir._mode()==1){
//                        newObj = aPositive.get(rd.nextInt(aPositive.size()));
//                    }
                    JsonArray relatedPos = newObj.get("relatedPos").getAsJsonArray();
                    x = ir._x() + relatedPos.get(0).getAsInt();
                    y = ir._y() + relatedPos.get(1).getAsInt();
                    if (x >= 0 && x < mHeight && y >= 0 && y < mWidth) {
                        queue.add(new ItemRiver(x, y, ir._id(), newObj.get("idRiver").getAsInt(), step++, ir._mode()));
                    } else{
                        // endpoint this branch
                        PointInt pOut = new PointInt(x, y);
                        PointInt pW = new PointInt();
                        PointInt start = new PointInt();
                        if(y>0){
                            // pos world
                            pW.x = x() + x;
                            pW.y = y() + y;
                            // pos start with new zone
                            if(x<0){
//                              // left
                                start.x = 100 + x;
                            } else if(x>=100){
                                // right
                                start.x= x - 100;
                            }
                            start.y = y;
                        } else{
                            // next zone follow vertical
                            // pos world
                            pW.x = x() + x;
                            pW.y = y() + root.y + y - 100;
                            // pos start with new zone
                            start.x = x;
                            if(x>=100){
                                start.x=x-100;
                            }
                            start.y = 100 + y;
                        }
                        rs.add(new PointRiver(root, start, pOut, pW));
                    }
                }
            }
        }
        return rs;
    }

    public void genTree(short id_r, int len) {
        DecorLayer layper = getLayerDecorBy((byte) ZONE_LAYER.TREE.getCode());
        int i = 0;
        Random rd = new Random();
        short x = -1;
        short y = -1;
        i = 0;
        GameObjectInfo info = null;
        while (i++ < len) {
            x = getShort(rd, mWidth);
            y = getShort(rd, mHeight);
            while (isPosAvailable(x, y) == 1) {
                x = getShort(rd, mWidth);
                y = getShort(rd, mHeight);
            }
            GameObjectMap obj = new GameObjectMap();
            obj.id_r = id_r;
            obj.id_z = id;
            obj.mType = (byte) (rd.nextInt(12) + ENUM_DECOR_MAP_TYPE.beginTree());
            obj.xPos = x;
            obj.yPos = y;
            layper.addItem(obj);
            obj.refInfo();
            info = getDecorInfo(obj.mType);
            updateMatrix(x, y, mWidth, mHeight, ENUM_MATRIX_TYPE.TREE.getCode(), info.width, info.height);
        }

    }

    public void genBush(short id_r, int len) {
        DecorLayer layper = getLayerDecorBy((byte) ZONE_LAYER.TREE.getCode());
        int i = 0;
        Random rd = new Random();
        short x = -1;
        short y = -1;
        i = 0;
        GameObjectInfo info = null;
        while (i++ < len) {
            x = getShort(rd, mWidth);
            y = getShort(rd, mHeight);
            while (isPosAvailable(x, y) == 1) {
                x = getShort(rd, mWidth);
                y = getShort(rd, mHeight);
            }
            GameObjectMap obj = new GameObjectMap();
            obj.id_r = id_r;
            obj.id_z = id;
            obj.mType = (byte) (rd.nextInt(3) + ENUM_DECOR_MAP_TYPE.beginBush());
            obj.xPos = x;
            obj.yPos = y;
            layper.addItem(obj);
            obj.refInfo();
            info = getDecorInfo(obj.mType);
            updateMatrix(x, y, mWidth, mHeight, ENUM_MATRIX_TYPE.TREE.getCode(), info.width, info.height);
            hashTest.put(x+"_"+y, (long)obj.mType);
        }
    }

    public void genGround(short id_r, int len) {
        DecorLayer layper = getLayerDecorBy(ZONE_LAYER.GROUND.getCode());
    }

    public void genWater(short id_r, int len) {
        DecorLayer layper = getLayerDecorBy(ZONE_LAYER.WATER.getCode());
    }

    public void genShore(short id_r, int len) {
        DecorLayer layper = getLayerDecorBy(ZONE_LAYER.SHORE.getCode());
    }

    public void genVisual(short id_r, int len) {
        DecorLayer layper = getLayerDecorBy(ZONE_LAYER.VISUAL.getCode());
    }

    public void initOrderLayer() {
        layerDecor.clear();
        layerDecor.add(new DecorLayer(ZONE_LAYER.GROUND.getCode()));
        layerDecor.add(new DecorLayer(ZONE_LAYER.WATER.getCode()));
        layerDecor.add(new DecorLayer(ZONE_LAYER.SHORE.getCode()));
        layerDecor.add(new DecorLayer(ZONE_LAYER.VISUAL.getCode()));
        layerDecor.add(new DecorLayer(ZONE_LAYER.TREE.getCode()));
    }

    // end generator static
    // game
    public GameObjectMap genHomeBase(String userID, byte type, short id_r) {
        Random rd = GlobalConfig.GI_RANDOM;
        short x = getShort(rd, mWidth);
        short y = getShort(rd, mHeight);
        GameObjectInfo info = getGameOjectInfo(type);
        while (isHomeBasePosAvailable(x, y, info.width, info.height) == 1) {
            x = getShort(rd, mWidth);
            y = getShort(rd, mHeight);
        }
        updateMatrix(x, y, mWidth, mHeight, ENUM_MATRIX_TYPE.HOME_BASE.getCode(), info.width, info.height);
        HomeBase obj = new HomeBase();
        obj.setBaseInfo(id_r, id, x, y, type, userID);
        obj.refInfo();

        DecorLayer layperG = getLayerDecorBy((byte) ZONE_LAYER.GROUND.getCode());
        GameObjectMap objg = new GameObjectMap();
        objg.setBaseInfo(id_r, id, (short) (x + info.width / 2), (short) (y + info.height / 2), ENUM_DECOR_MAP_TYPE.GROUND_DECOR_8.getCode(), "");
        layperG.addItem(objg);
        objg.refInfo();
        info = getDecorInfo(objg.mType);
        updateMatrix(x, y, mWidth, mHeight, ENUM_MATRIX_TYPE.GROUND.getCode(), info.width, info.height);

        // save
        if (obj.saveData() > 0) {
            layerGameObject.add(obj);
            hashTest.put(x+"_"+y, obj.id);
            return obj;
        } else {
            return null;
        }
    }

    public GameObjectMap genMine(String userID, byte type, short id_r) {
        Random rd = GlobalConfig.GI_RANDOM;
        short x = getShort(rd, mWidth);
        short y = getShort(rd, mHeight);

        GameObjectInfo info = getGameOjectInfo(type);
        while (isHomeBasePosAvailable(x, y, info.width, info.height) == 1) {
            x = getShort(rd, mWidth);
            y = getShort(rd, mHeight);
        }
        updateMatrix(x, y, mWidth, mHeight, ENUM_MATRIX_TYPE.MINE.getCode(), info.width, info.height);

        Mine obj = null;
//        GameResources rs = new GameResources();
//        rs.genRandomMineData();

        obj = new Mine();
        obj.setBaseInfo(id_r, id, x, y, type, userID);
        obj.setPrices(500, 10000);
//        obj.setResource(rs);
        obj.refInfo();

        DecorLayer layperG = getLayerDecorBy((byte) ZONE_LAYER.GROUND.getCode());
        GameObjectMap objg = new GameObjectMap();
        objg.setBaseInfo(id_r,id, (short) (x + info.width / 2), (short) (y + info.height / 2), ENUM_DECOR_MAP_TYPE.GROUND_DECOR_8.getCode(), "");
        objg.refInfo();
        layperG.addItem(objg);
        info = getDecorInfo(objg.mType);
        updateMatrix(x, y, mWidth, mHeight, ENUM_MATRIX_TYPE.GROUND.getCode(), info.width, info.height);
        // save
        if (obj.saveData() > 0) {
            layerGameObject.add(obj);
            hashTest.put(x+"_"+y, obj.id);

            // add ref object in world map
            Claim mCliam = new Claim();
            mCliam.mineID = obj.id;
            mCliam.id_z = id;
            mCliam.userID = "";
            mCliam.saveData();
            // ref claim in mine
            obj.refClaim(mCliam);
            return obj;
        } else {
            return null;
        }
    }

    public GameObjectMap genOilMine(String userID, short id_r) {
        Random rd = GlobalConfig.GI_RANDOM;
        short x = getShort(rd, mWidth);
        short y = getShort(rd, mHeight);

        byte type = -1;
        int rate = rd.nextInt(100);
        if(rate>49){
            type = ENUM_UNIT_TYPE.OIL_RIG_BASIC.getCode();
        } else if(rate>19){
            type = ENUM_UNIT_TYPE.OIL_RIG_ADVANCE.getCode();
        } else{
            type = ENUM_UNIT_TYPE.OIL_RIG_ULTRA.getCode();
        }

        GameObjectInfo info = StaticDataMng.i().getRepoGameObjectInfo().lstObjectMap.get(type);
        int _width = info.width;
        int _height = info.height;

        while (isHomeBasePosAvailable(x, y, _width, _height) == 1) {
            x = getShort(rd, mWidth);
            y = getShort(rd, mHeight);
        }
        updateMatrix(x, y, mWidth, mHeight, ENUM_MATRIX_TYPE.OIL_MINE.getCode(), _width, _height);

        OilMine obj = new OilMine();
        obj.setBaseInfo(id_r, id, x, y, type, userID);
        obj.refInfo();

        DecorLayer layperG = getLayerDecorBy((byte) ZONE_LAYER.GROUND.getCode());
        GameObjectMap objg = new GameObjectMap();
        objg.setBaseInfo(id_r, id, (short) (x + _width/2), (short) (y + _height/2), ENUM_DECOR_MAP_TYPE.GROUND_DECOR_8.getCode(), "");
        objg.refInfo();
        layperG.addItem(objg);
        info = getDecorInfo(objg.mType);
        updateMatrix(x, y, mWidth, mHeight, ENUM_MATRIX_TYPE.GROUND.getCode(), info.width, info.height);
        // save
        if (obj.saveData() > 0) {
            layerGameObject.add(obj);
            hashTest.put(x+"_"+y, obj.id);
            return obj;
        } else {
            return null;
        }
    }

    public GameObjectMap genTradeCenter(String userID, byte type, short id_r) {
        Random rd = GlobalConfig.GI_RANDOM;
        short x = (short) (mWidth / 2);
        short y = (short) (mHeight / 2);

        GameObjectInfo info = getGameOjectInfo(type);

        updateMatrix(x, y, mWidth, mHeight, ENUM_MATRIX_TYPE.TRADE_CENTER.getCode(), info.width, info.height);

        TradeCenterMap obj = new TradeCenterMap();
        obj.setBaseInfo(id_r, id, x, y, type, userID);
        obj.refInfo();

        DecorLayer layperG = getLayerDecorBy((byte) ZONE_LAYER.GROUND.getCode());
        GameObjectMap objg = new GameObjectMap();
        objg.setBaseInfo(id_r, id, (short) (x + info.width / 2), (short) (y + info.height / 2), ENUM_DECOR_MAP_TYPE.GROUND_DECOR_8.getCode(), userID);
        objg.refInfo();
        layperG.addItem(objg);
        info = getDecorInfo(objg.mType);
        updateMatrix(x, y, mWidth, mHeight, ENUM_MATRIX_TYPE.GROUND.getCode(), info.width, info.height);
        // save
        if (obj.saveData() > 0) {
            layerGameObject.add(obj);
            StaticDataMng.i().worldMap.refTradeCenter(id, obj);
            return obj;
        } else {
            return null;
        }
    }

    // end generator game object
    public DecorLayer getLayerDecorBy(byte type) {
        DecorLayer rs = null;
        for (DecorLayer l : layerDecor) {
            if (l.type == type) {
                rs = l;
                break;
            }
        }
        if (rs == null) {
            rs = new DecorLayer(type);
            layerDecor.add(rs);
        }
        return rs;
    }

    public GameObjectMap[] getGameObjects() {
        GameObjectMap[] temp;
        synchronized (this) {
            temp = layerGameObject.toArray(WSGameDefine.EMPTY_GAMEOBJECT_MAP);
        }
        return temp;
    }

    public ArrayList<String> getListUserID() {
        ArrayList<String> rs = new ArrayList<>();
        GameObjectMap[] temp;
        GameObjectMap[] lsEmpty = WSGameDefine.EMPTY_GAMEOBJECT_MAP;
        synchronized (this) {
            temp = layerGameObject.toArray(lsEmpty);
        }
        for (GameObjectMap g : temp) {
            if (!g.userID.isEmpty()) {
                rs.add(g.userID);
            }
        }
        return rs;
    }

    public void addGameObject(GameObjectMap obj) {
        synchronized (LOCK) {
            layerGameObject.add(obj);
            hashTest.put(obj.xPos+"_"+obj.yPos, obj.id);
        }
        GameObjectInfo info = getGameOjectInfo(obj.mType);
        ENUM_MATRIX_TYPE type = ENUM_MATRIX_TYPE.NONE;
        int w = info.width;
        int h = info.height;
        if (obj instanceof HomeBase) {
            type = ENUM_MATRIX_TYPE.HOME_BASE;
        } else if (obj instanceof OilMine) {
            type = ENUM_MATRIX_TYPE.OIL_MINE;
        } else if (obj instanceof Mine) {
            type = ENUM_MATRIX_TYPE.MINE;
        } else if (obj instanceof TradeCenterMap) {
            type = ENUM_MATRIX_TYPE.TRADE_CENTER;
            StaticDataMng.i().worldMap.refTradeCenter(id, obj);
        }
        updateMatrix(obj.xPos, obj.yPos, mWidth, mHeight, type.getCode(), w, h);

        int x = obj.xPos + info.width / 2;
        int y = obj.yPos + info.height / 2;
        info = getDecorInfo((byte) ENUM_DECOR_MAP_TYPE.GROUND_DECOR_8.getCode());
        updateMatrix(x, y, mWidth, mHeight, ENUM_MATRIX_TYPE.GROUND.getCode(), info.width, info.height);
    }

    public GameObjectMap getObjectMap(long id) {
        GameObjectMap[] temp = getGameObjects();
        for (GameObjectMap it : temp) {
            if (id == it.id) {
                return it;
            }
        }
        return null;
    }

    public boolean hasGameObjects() {
        boolean rs = true;
        synchronized (LOCK) {
            if (layerGameObject == null) {
                return false;
            }
            rs = layerGameObject.size() > 0;
        }
        return rs;
    }

    public boolean checkAvailableMine(long id) {
        synchronized (LOCK) {
            for (GameObjectMap it : layerGameObject) {
                if (id == it.id && !it.userID.isEmpty()) {
                    return true;
                }
            }
        }
        return false;
    }

    @Override
    public void writeData(LittleEndianDataOutputStream os) throws IOException {
        os.writeShort(id);
        os.writeShort(id_r);
        Utils.i().writeBigString(os, name);
        os.writeInt(pos.size());
        for (int p : pos) {
            os.writeInt(p);
        }
        os.writeInt(layerDecor.size());
        for (DecorLayer dl : layerDecor) {
            dl.writeData(os);
        }
    }

    @Override
    public void readData(LittleEndianDataInputStream is) throws IOException {
        id = is.readShort();
        id_r = is.readShort();
        name = Utils.i().readBigString(is);
        int len = is.readInt();
        int i = 0;
        pos = new ArrayList<>();
        while (i++ < len) {
            pos.add(is.readInt());
        }
        len = is.readInt();
        i = 0;
        layerDecor = new ArrayList<>();
        while (i++ < len) {
            DecorLayer dl = new DecorLayer();
            dl.readData(is);
            layerDecor.add(dl);
        }
    }

    @Override
    public long calDataOnRAM() {
        long rs = 2 * 2 + name.length() + (pos.size()) * 4;
        for (DecorLayer dl : layerDecor) {
            rs += dl.calDataOnRAM();
        }
        for (GameObjectMap o : layerGameObject) {
            rs += o.capacityOnRAM();
        }
        return rs;
    }

}