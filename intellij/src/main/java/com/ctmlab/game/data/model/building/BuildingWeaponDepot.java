/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ctmlab.game.data.model.building;

import com.ctmlab.game.data.staticdata.gameobjectinfo.GameObjectWeaponDepotInfo;
import com.ctmlab.game.data.staticdata.gameobjectinfo.UpgradeInfo;
import com.ctmlab.game.goldincenum.ENUM_UNIT_TYPE;
import com.google.common.io.LittleEndianDataInputStream;
import com.google.common.io.LittleEndianDataOutputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

/**
 *
 * @author hhtri
 */
public class BuildingWeaponDepot extends Building{
    public int level;
    public long startUpgradeTime;
    public long endUpgradeTime;
    
    public BuildingWeaponDepot() {
        mType = (byte)ENUM_UNIT_TYPE.HOME_WEAPON.getCode();
        level = 1;
        startUpgradeTime = 0;
        endUpgradeTime = 0;
    }
    
    public BuildingWeaponDepot(String user, short idRegion, short idZone, short x, short y) {
        
        id_r = idRegion;
        id_z = idZone;
        xPos = x;
        yPos = y;
        
        userID = user;        
        mType = (byte)ENUM_UNIT_TYPE.HOME_WEAPON.getCode();
        
        level = 1;
        startUpgradeTime = 0;
        endUpgradeTime = 0;
    }
    
    @Override
    public void setTimeUpgrade(long start, long end){
        startUpgradeTime = start;
        endUpgradeTime = end;
    }
    
    @Override
    public int getLevel(){        
        return level;
    }
    
    @Override
    public void setLevel(int lv){
        level = lv;
        startUpgradeTime = endUpgradeTime = 0;
        try {
            writeData();
        } catch (IOException ex) {
        }
    }        
    
    @Override
    public HashMap<Byte, Long> getGOInfoByLevel(){
        super.getGameObjectInfo();
        GameObjectWeaponDepotInfo oInfo = (GameObjectWeaponDepotInfo)objInfo;
        if(oInfo.info.size()<level){return null;}
        return oInfo.info.get(level-1);
    }
    
    @Override
    public ArrayList<HashMap<Byte, Long>> getConfigGameObjectInfo(){
        super.getGameObjectInfo();
        GameObjectWeaponDepotInfo oInfo = (GameObjectWeaponDepotInfo)objInfo;
        return oInfo.info;
    }
        
    @Override
    public ArrayList<UpgradeInfo> getConfigGameObjectUpgradeInfo(){
        super.getGameObjectInfo();
        GameObjectWeaponDepotInfo oInfo = (GameObjectWeaponDepotInfo)objInfo;
        return oInfo.upgrade_require;
    }
        
    @Override
    public void readData() throws IOException{
        if(data!=null){
            try(LittleEndianDataInputStream is = new LittleEndianDataInputStream(new ByteArrayInputStream(data))){
                level = is.readInt();
                startUpgradeTime = is.readLong();
                endUpgradeTime = is.readLong();
            }
        }
            
    }
    
    @Override
    public void writeData() throws IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        try(LittleEndianDataOutputStream os = new LittleEndianDataOutputStream(baos)){
            os.writeInt(level);
            os.writeLong(startUpgradeTime);
            os.writeLong(endUpgradeTime);
        } catch(Exception ex){}
        data = baos.toByteArray();
        super.saveData();
    }
    
    // response to client
    @Override
    public void writeData(LittleEndianDataOutputStream os) throws IOException {
        super.writeData(os);
        os.writeInt(level);
        os.writeLong(startUpgradeTime);
        os.writeLong(endUpgradeTime);
    }
    
    @Override
    public void writeDataOther(LittleEndianDataOutputStream os) throws IOException{
        super.writeData(os);
        os.writeInt(level);
        os.writeLong(0L);
        os.writeLong(0L);
    }
}