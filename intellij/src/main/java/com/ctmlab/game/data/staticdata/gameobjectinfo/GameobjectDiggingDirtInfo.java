package com.ctmlab.game.data.staticdata.gameobjectinfo;

public class GameobjectDiggingDirtInfo {

    private String version;
    private String name;
    private String desc;
    private int numberOfSpot;
    private long timeOfYard;
    private long minYard;
    private long maxYard;

    public GameobjectDiggingDirtInfo(){
        setVersion("1.0.1");
        setName("the dirts");
        setDesc("digging of the dirt in the claim");
        setNumberOfSpot(9);
        setTimeOfYard(1000);
        setMinYard(10000L);
        setMaxYard(100000L);
    }

    // <editor-fold defaultstate="collapsed" desc="Method (Gets and Sets)">

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public int getNumberOfSpot() {
        return numberOfSpot;
    }

    public void setNumberOfSpot(int numberOfSpot) {
        this.numberOfSpot = numberOfSpot;
    }

    public long getTimeOfYard() {
        return timeOfYard;
    }

    public void setTimeOfYard(long timeOfYard) {
        this.timeOfYard = timeOfYard;
    }

    public long getMinYard() {return minYard;}

    public void setMinYard(long minYard) {
        this.minYard = minYard;
    }

    public long getMaxYard() {
        return maxYard;
    }

    public void setMaxYard(long maxYard) {
        this.maxYard = maxYard;
    }

    // </editor-fold>
}
