/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ctmlab.game.data.pojo;

import com.ctmlab.game.goldincenum.ENUM_RESOURCE_TYPE;
import com.ctmlab.game.network.manager.WSGameDefine;
import com.ctmlab.game.threads.GameStateThread;
import com.google.common.io.LittleEndianDataInputStream;
import com.google.common.io.LittleEndianDataOutputStream;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import java.io.IOException;
import java.util.Map.Entry;
import java.util.Random;
import java.util.TreeMap;

/**
 *
 * @author hhtri
 */
public class GameResources {

    public TreeMap<Byte, Long> data = new TreeMap<>();

    public GameResources() {
        for(byte t:ENUM_RESOURCE_TYPE.getKeys()){
            if(t>0) {
                data.put(t, 0L);
            }
        }
    }
    public GameResources copy(){
        GameResources rs = new GameResources();
        rs.setData(this);
        return rs;
    }

    public void setData(GameResources res){
        if(res==null){return;}
        for(Entry<Byte, Long> e:res.data.entrySet()){
            data.put(e.getKey(), e.getValue());
        }
    }

    public long getValue(ENUM_RESOURCE_TYPE orderEnum){
        byte key = orderEnum.getCode();
        if(!data.containsKey(key)){return 0;}
        return data.get(key);
    }

    public void setValue(ENUM_RESOURCE_TYPE orderEnum, long val){
        byte key = orderEnum.getCode();
        if(!data.containsKey(key)){return ;}
        data.put(key, val);
    }

    public boolean decreaseValue(ENUM_RESOURCE_TYPE orderEnum, long value){
        byte key = orderEnum.getCode();
        if(!data.containsKey(key)){return false;}
        else if(data.get(key)<value){return false;}
        long temp = data.get(key)-value;
        data.put(key, temp);
        return true;
    }

    public boolean increaseValue(ENUM_RESOURCE_TYPE orderEnum, long value){
        byte key = orderEnum.getCode();
        if(!data.containsKey(key)){return false;}
        long temp = data.get(key)+value;
        data.put(key, temp);
        return true;
    }

    private boolean setResourceValue(byte type, long value, boolean isBuying) {
        return isBuying?increaseValue(ENUM_RESOURCE_TYPE.get(type), value):decreaseValue(ENUM_RESOURCE_TYPE.get(type), value);
    }

    private boolean enoughResourceValue(byte type, long value) {
        if(!data.containsKey(type)){return false;}
        long curValue = data.get(type);
        return curValue>value;
    }

    public boolean checkResForUpgrade(TreeMap<Byte, Long> values){
        boolean flag = true;
        for(Entry<Byte, Long> e:values.entrySet()){
            if(!enoughResourceValue(e.getKey(), e.getValue())){
                flag = false;
                break;
            }
        }

        return flag;
    }

    public void decreaseResource(TreeMap<Byte, Long> values){
        for(Entry<Byte, Long> e:values.entrySet()){
            decreaseValue(ENUM_RESOURCE_TYPE.get(e.getKey()), e.getValue());
        }
    }

    public void increaseResource(TreeMap<Byte, Long> values){
        for(Entry<Byte, Long> e:values.entrySet()){
            increaseValue(ENUM_RESOURCE_TYPE.get(e.getKey()), e.getValue());
        }
    }

    public int addResource(boolean isBuying, byte type, long amount, long price) {

        long tPrice = price * amount;
        long cash = data.get(ENUM_RESOURCE_TYPE.CASH.getCode());
        if (amount < 0 || price < 0) {
            return WSGameDefine.ADD_RES_INVALID;
        } else if (isBuying && (tPrice < 0 || (tPrice >= 0 && cash >= 0 && tPrice > cash))) {
            return WSGameDefine.ADD_RES_FAIL;
        }

        // isBuying==true => -cash, +amount
        // else => +cash, -amount
        boolean flag = setResourceValue(type, amount, isBuying);
//        if (flag) {
//            cash += isBuying ? (-1 * tPrice):tPrice;
//        }
        return flag ? WSGameDefine.ADD_RES_SUCCESS : WSGameDefine.ADD_RES_FAIL;
    }

    public void setResourceAdmin(JsonObject obj) throws Exception {
        for(Entry<String, JsonElement> e: obj.entrySet()){
            String key = e.getKey();
            long value = e.getValue().getAsLong();
            data.put(ENUM_RESOURCE_TYPE.get(key).getCode(), value);
        }
    }

    public JsonObject getJsonObjectAdmin() {
        JsonObject obj = new JsonObject();
        for(Entry<Byte, Long> e:data.entrySet()){
            String key = ENUM_RESOURCE_TYPE.get(e.getKey()).toString();
            obj.addProperty(key, e.getValue());
        }
        return obj;
    }

    public void genDataTest() {
        Random rd = new Random();
        int maxValue = 100000;
        for(ENUM_RESOURCE_TYPE t:ENUM_RESOURCE_TYPE.values()){
            long value = rd.nextInt(maxValue);
            data.put(t.getCode(), value);
        }
    }

    public void genRandomMineData() {
        Random rd = new Random();
        int maxValue = 100000;
        for(ENUM_RESOURCE_TYPE t:ENUM_RESOURCE_TYPE.values()){
            long value = rd.nextInt(maxValue);
            data.put(t.getCode(), value);
        }
    }

    public void genRandomOilMineData() {
        Random rd = new Random();
        int maxValue = 100000;
        for(ENUM_RESOURCE_TYPE t:ENUM_RESOURCE_TYPE.values()){
            long value = rd.nextInt(maxValue);
            data.put(t.getCode(), value);
        }
    }

    public void readData(LittleEndianDataInputStream is) throws IOException {
        for(byte t:ENUM_RESOURCE_TYPE.getKeys()){
            data.put(t, is.readLong());
        }
    }

    public void writeData(LittleEndianDataOutputStream os) throws IOException {
        for(byte t:ENUM_RESOURCE_TYPE.getKeys()){
            os.writeLong(data.get(t));
        }
    }

    public void writeData(LittleEndianDataOutputStream os, int order) throws IOException {
        for(byte t:ENUM_RESOURCE_TYPE.getKeys()){
            os.writeLong((t<order)?data.get(t):0L);
        }
    }

    public void writeDataEmpty(LittleEndianDataOutputStream os) throws IOException {
        for(byte t:ENUM_RESOURCE_TYPE.getKeys()){
            os.writeLong(0L);
        }
    }
}