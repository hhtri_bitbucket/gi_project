/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ctmlab.game.data.staticdata;

/**
 *
 * @author hhtri
 */
public class PointInt {
    public int x;
    public int y;
    public PointInt(){
        x = y = -1;
    }
    public PointInt(int x, int y){
        this.x = x;
        this.y = y;
    }    
    
    public void reset(){
        x = y = -1;
    }
    
    public void setValue(int x, int y){
        this.x = x;
        this.y = y;
    }
}
