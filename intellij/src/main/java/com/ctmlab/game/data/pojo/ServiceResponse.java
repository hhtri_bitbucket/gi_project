/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ctmlab.game.data.pojo;

import com.ctmlab.game.data.ctmon.CTMON;
import com.ctmlab.game.network.manager.WSGameDefine;
import com.google.common.io.LittleEndianDataOutputStream;
import java.io.ByteArrayOutputStream;

/**
 *
 * @author hhtri
 */
public class ServiceResponse {
    public int returnCode;
    public String message;
    public byte[] data;
    public ServiceResponse(){
        returnCode = -1;
        message = "";
        data = WSGameDefine.EMPTY_BYTES;
    }
    
    public byte[] getBytes(){
        try{
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            LittleEndianDataOutputStream os = new LittleEndianDataOutputStream(baos);
            CTMON.i().toStream(os, this);
            return baos.toByteArray();
        } catch(Exception ex){
            return WSGameDefine.EMPTY_BYTES;
        }
    }

    /**
     * @return the returnCode
     */
    public int getReturnCode() {
        return returnCode;
    }

    /**
     * @param returnCode the returnCode to set
     */
    public void setReturnCode(int returnCode) {
        this.returnCode = returnCode;
    }

    /**
     * @return the message
     */
    public String getMessage() {
        return message;
    }

    /**
     * @param message the message to set
     */
    public void setMessage(String message) {
        this.message = message;
    }

    /**
     * @return the data
     */
    public byte[] getData() {
        return data;
    }

    /**
     * @param data the data to set
     */
    public void setData(byte[] data) {
        this.data = data;
    }
}
