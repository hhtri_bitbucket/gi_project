/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ctmlab.game.data.staticdata;

/**
 *
 * @author hhtri
 */
public class PointShort {
    public short x;
    public short y;
    public PointShort(){
        x = y = -1;
    }
    
    public PointShort(short x, short y){
        this.x = x;
        this.y = y;
    }    
    public PointShort(int x, int y){
        this.x = (short)x;
        this.y = (short)y;
    }    
    
    public void reset(){
        x = y = -1;
    }
    
    public void setValue(short x, short y){
        this.x = x;
        this.y = y;
    }
}
