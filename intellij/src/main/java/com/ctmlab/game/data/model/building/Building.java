/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ctmlab.game.data.model.building;

import com.ctmlab.game.data.model.GameObjectMap;
import com.ctmlab.game.data.staticdata.gameobjectinfo.GameObjectInfo;
import com.ctmlab.game.data.staticdata.gameobjectinfo.UpgradeInfo;
import com.ctmlab.game.goldincenum.ENUM_UNIT_TYPE;
import com.dp.db.DB;
import com.google.common.io.LittleEndianDataOutputStream;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;

/**
 *
 * @author hhtri
 */
public class Building extends GameObjectMap{
    
    public byte[] data;
    
    public Building(){
    }
    
    public byte getType(){return mType;}
    
    public int getLevel(){return 0;}
    
    public void setLevel(int lv){}
    
    public String getUserID(){return userID;}
    
    public void setUserID(String u){userID=u;}
    
    public void setTimeUpgrade(long start, long end){}
    
    public HashMap<Byte, Long> getGOInfoByLevel(){return null;}
    
    public GameObjectInfo getGameObjectInfo(){
        super.refInfo();
        return objInfo;
    }
    
    public ArrayList<HashMap<Byte, Long>> getConfigGameObjectInfo(){return null;}
        
    public ArrayList<UpgradeInfo> getConfigGameObjectUpgradeInfo(){return null;}
    
    public Building parseData(){
        Building b = null;
        ENUM_UNIT_TYPE type = ENUM_UNIT_TYPE.get(mType);
        switch(type){
            case HOME_BARRACK:{
                b = new BuildingBarrack();                
                break;
            }
            case HOME_DEFENSE:{
                b = new BuildingDefense();                
                break;
            }
            case HOME_EXPLORATION:{
                b = new BuildingExploration();                
                break;
            }
            case HOME_OFFICE:{
                b = new BuildingOfficer();                
                break;
            }
            case HOME_REFINERY:{
                b = new BuildingRefinery();                
                break;
            }
            case HOME_TRANSPORTATION:{
                b = new BuildingTransportation();                
                break;
            }
            case HOME_WALL:{
                b = new BuildingWall();                
                break;
            }
            case HOME_WEAPON:{
                b = new BuildingWeaponDepot();                
                break;
            }
        }
        if(b!=null){
            b.id = this.id;
            b.id_r = this.id_r;
            b.id_z = this.id_z;
            b.xPos = this.xPos;
            b.yPos = this.yPos;
            b.mType = this.mType;
            b.userID = this.userID;
            b.data = this.data;
            try{
                b.readData();
                b.getGameObjectInfo();
            } catch(IOException ex){}
        }
        return b;
    }
    
    public void readData() throws IOException{}
    
    public void writeData() throws IOException {}
    
    @Override
    public void writeData(LittleEndianDataOutputStream os) throws IOException{
        super.writeData(os);
    }
    
    public void writeDataOther(LittleEndianDataOutputStream os) throws IOException{
        super.writeData(os);
    }
    
    @Override
    public int saveData(){
        try{
            return saveEx(DB.i().con(), Building.class);
        } catch(SQLException ex){return -2;}
    }
    
//    public void writeDataResponse(LittleEndianDataOutputStream os) throws IOException {
//        super.writeData(os);
//    }
}
