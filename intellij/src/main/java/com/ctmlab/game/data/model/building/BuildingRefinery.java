/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ctmlab.game.data.model.building;

import com.ctmlab.game.data.staticdata.gameobjectinfo.GameObjectRefineryInfo;
import com.ctmlab.game.data.staticdata.gameobjectinfo.UpgradeInfo;
import com.ctmlab.game.goldincenum.ENUM_UNIT_TYPE;
import com.google.common.io.LittleEndianDataInputStream;
import com.google.common.io.LittleEndianDataOutputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

/**
 *
 * @author hhtri
 */
public class BuildingRefinery extends Building{
    
    public int level;
    public long startUpgradeTime;
    public long endUpgradeTime;
    public long startTimeRefinery;
    public long endTimeRefinery;
    
    public BuildingRefinery() {
        mType = (byte)ENUM_UNIT_TYPE.HOME_REFINERY.getCode();
        level = 1;
        startUpgradeTime = 0;
        endUpgradeTime = 0;
        startTimeRefinery = 0;
        endTimeRefinery = 0;
    }
    
    public BuildingRefinery(String user, short idRegion, short idZone, short x, short y) {
        
        id_r = idRegion;
        id_z = idZone;
        xPos = x;
        yPos = y;
        
        userID = user;        
        mType = (byte)ENUM_UNIT_TYPE.HOME_REFINERY.getCode();
        
        level = 1;
        startUpgradeTime = 0;
        endUpgradeTime = 0;
        startTimeRefinery = 0;
        endTimeRefinery = 0;
    }
    
    @Override
    public void setTimeUpgrade(long start, long end){
        startUpgradeTime = start;
        endUpgradeTime = end;
    }
    
    
    public void setTimeRefine(long start, long end){
        startTimeRefinery = start;
        endTimeRefinery = end;
    }
    
    public void finishRefineOil(){
        startTimeRefinery = endTimeRefinery = 0;
        try {
            writeData();
        } catch (IOException ex) {
        }
    }
    
    @Override
    public int getLevel(){        
        return level;
    }
    
    @Override
    public void setLevel(int lv){
        level = lv;
        startUpgradeTime = endUpgradeTime = 0;
        try {
            writeData();
        } catch (IOException ex) {
        }
    }        
    
    @Override
    public HashMap<Byte, Long> getGOInfoByLevel(){
        super.getGameObjectInfo();
        GameObjectRefineryInfo oInfo = (GameObjectRefineryInfo)objInfo;
        if(oInfo.info.size()<level){return null;}
        return oInfo.info.get(level-1);
    }
    
    @Override
    public ArrayList<HashMap<Byte, Long>> getConfigGameObjectInfo(){
        super.getGameObjectInfo();
        GameObjectRefineryInfo oInfo = (GameObjectRefineryInfo)objInfo;
        return oInfo.info;
    }
        
    @Override
    public ArrayList<UpgradeInfo> getConfigGameObjectUpgradeInfo(){
        super.getGameObjectInfo();
        GameObjectRefineryInfo oInfo = (GameObjectRefineryInfo)objInfo;
        return oInfo.upgrade_require;
    }
        
    @Override
    public void readData() throws IOException{
        if(data!=null){
            try(LittleEndianDataInputStream is = new LittleEndianDataInputStream(new ByteArrayInputStream(data))){
                level = is.readInt();
                startUpgradeTime = is.readLong();
                endUpgradeTime = is.readLong();
                startTimeRefinery = is.readLong();
                endTimeRefinery = is.readLong();
            }
        }
            
    }
    
    @Override
    public void writeData() throws IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        try(LittleEndianDataOutputStream os = new LittleEndianDataOutputStream(baos)){
            os.writeInt(level);
            os.writeLong(startUpgradeTime);
            os.writeLong(endUpgradeTime);
            os.writeLong(startTimeRefinery);
            os.writeLong(endTimeRefinery);
        } catch(Exception ex){}
        data = baos.toByteArray();
        super.saveData();
    }
    
    // response to client
    @Override
    public void writeData(LittleEndianDataOutputStream os) throws IOException {
        super.writeData(os);
        os.writeInt(level);
        os.writeLong(startUpgradeTime);
        os.writeLong(endUpgradeTime);
        os.writeLong(startTimeRefinery);
        os.writeLong(endTimeRefinery);
    }
    
    @Override
    public void writeDataOther(LittleEndianDataOutputStream os) throws IOException{
        super.writeData(os);
        os.writeInt(level);
        os.writeLong(0L);
        os.writeLong(0L);
        os.writeLong(0L);
        os.writeLong(0L);
    }
}
