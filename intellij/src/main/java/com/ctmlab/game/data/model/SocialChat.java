/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ctmlab.game.data.model;

import com.ctmlab.game.network.manager.WSGameDefine;
import com.ctmlab.util.Utils;
import com.dp.db.DBDataPostgres;
import com.dp.db.DataFieldType;
import com.google.common.io.LittleEndianDataOutputStream;
import java.io.IOException;

/**
 *
 * @author hhtri
 */
public class SocialChat extends DBDataPostgres{
    @DataFieldType(autoIncrease = true, primaryKey = true)
    public long id;
    
    public long groupId;
    public String sender;
//    public String receiver;
    public byte mType;
    public byte[] data;
    public long time;
    
    public SocialChat(){id = 0;}
    
    public SocialChat(byte type, long groupId, String sender,  byte[] data){
        this.mType = type;
        this.groupId = groupId;
        this.sender = sender;
        this.data = data;
        this.time=System.currentTimeMillis();
        id = 0;
    }
    
    public void writeData(LittleEndianDataOutputStream os) throws IOException{
        
        os.writeLong(id);
        os.writeLong(groupId);
        Utils.i().writeBigString(os, sender);        
        os.writeByte(mType);
        os.writeInt(data.length);
        os.write(data);        
        os.writeLong(time);
    }
}
