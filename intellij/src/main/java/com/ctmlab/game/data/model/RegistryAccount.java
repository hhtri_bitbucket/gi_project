/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ctmlab.game.data.model;

import com.dp.db.DBDataPostgres;
import com.dp.db.DataFieldType;

/**
 *
 * @author hhtri
 */
public class RegistryAccount extends DBDataPostgres{
    
    @DataFieldType(size = 128, primaryKey = true)
    public String id;
    public String email;
    public String password;
    public String companyName;
    public long timeReg;
    
    public RegistryAccount(){
        id = "";
        email = "";
        password = "";
        companyName = "";
        timeReg = 0L;
    }
    
    public RegistryAccount(String id, String email, String password, String companyName){
        this.id = id;
        this.email = email;
        this.password = password;
        this.companyName = companyName;
        timeReg = System.currentTimeMillis();
    }
}
