package com.ctmlab.game.data.staticdata.gameobjectinfo;

import com.ctmlab.game.goldincenum.ENUM_RESOURCE_TYPE;
import com.ctmlab.game.manager.GlobalConfig;
import com.ctmlab.util.Utils;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import java.io.File;
import java.util.TreeMap;

public class GameObjectWashingDirtInfo {

    public class RatingInfo{
        private ENUM_RESOURCE_TYPE type;
        private int rating;
        private int amountMin;
        private int amountMax;
        public RatingInfo(ENUM_RESOURCE_TYPE t, int r, int aMin, int aMax){
            rating = r;
            amountMin = aMin;
            amountMax = aMax;
            type = t;
        }
        public int getRating(){return rating;}
        public int getAmountMin(){return amountMin;}
        public int getAmountMax(){return amountMax;}
        public ENUM_RESOURCE_TYPE getType(){return type;}
    }

    private long mTimeOfWashing;
    private TreeMap<ENUM_RESOURCE_TYPE, RatingInfo> mInfo = null;

    public GameObjectWashingDirtInfo(){readData();}

    private void readData(){
        String path = GlobalConfig.pathBackup + File.separator + "washing_dirt.json";
        JsonObject objInfo = GlobalConfig.getJsonObject(Utils.i().readTextFile(path));
        if(objInfo!=null){
            mTimeOfWashing = objInfo.get("time_washing").getAsInt();
            mInfo = new TreeMap<>();
            JsonArray arr = objInfo.get("info").getAsJsonArray();
            for(int i=0; i<arr.size(); i++){
                JsonObject obj = arr.get(i).getAsJsonObject();
                ENUM_RESOURCE_TYPE type = ENUM_RESOURCE_TYPE.get(obj.get("type").getAsInt());
                int rating = obj.get("rating").getAsInt();
                int amountMin = obj.get("amount_min").getAsInt();
                int amountMax = obj.get("amount_max").getAsInt();
                mInfo.put(type, new RatingInfo(type, rating, amountMin, amountMax));
            }
        }
    }

    public long getTimeWashing(){
        return mTimeOfWashing;
    }

    public TreeMap<ENUM_RESOURCE_TYPE, RatingInfo> getInfo(){
        if(mInfo==null){readData();}
        return mInfo;
    }
}
