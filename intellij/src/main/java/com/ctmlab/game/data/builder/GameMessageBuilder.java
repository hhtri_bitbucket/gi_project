/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ctmlab.game.data.builder;

import com.ctmlab.game.data.model.GameMessage;
import com.ctmlab.game.data.model.base.GameMessageBase;
import com.dp.db.DB;
import java.sql.SQLException;

/**
 *
 * @author hhtri
 */
public class GameMessageBuilder {
    private String sender;
    private String receiver;
    private String subject;
    private String content;
    private byte type;

    public GameMessageBuilder setSender(String sender) {
        this.sender = sender;
        return this;
    }

    public GameMessageBuilder setReceiver(String receiver) {
        this.receiver = receiver;
        return this;
    }

    public GameMessageBuilder setSubject(String subject) {
        this.subject = subject;
        return this;
    }

    public GameMessageBuilder setContent(String content) {
        this.content = content;
        return this;
    }

    public GameMessageBuilder setType(byte mType) {
        this.type = mType;
        return this;
    }
    
    public GameMessageBase build(){
        GameMessage rs = new GameMessage(sender, receiver, subject, content, type);
        return rs;
    }
    
    public GameMessageBase buildAndSave() throws SQLException{
        GameMessageBase ra = build();
        if(ra.save(DB.i().con())<0){return null;}
        return ra;
    }
}
