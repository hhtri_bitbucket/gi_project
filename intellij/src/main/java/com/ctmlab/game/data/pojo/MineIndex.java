/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ctmlab.game.data.pojo;

import com.google.common.io.LittleEndianDataInputStream;
import com.google.common.io.LittleEndianDataOutputStream;
import java.io.IOException;

/**
 *
 * @author hhtri
 */
public class MineIndex {
    public short id_r;
    public short id_z;
    public long id;
    
    public MineIndex(){}
    
    public MineIndex(short id_r, short id_z, long id){
        this.id_r = id_r;
        this.id_z = id_z;
        this.id = id;
    }
    
    public boolean isEqualIndex(MineIndex idx){
        return ((id_r==idx.id_r) && (id_z==idx.id_z) && (id==idx.id))
                || ((id_z==idx.id_z) && (id==idx.id));
    }
    
    public String getIdxCode(){
        String rs = id_r + "_" + id_z + "_" + id;
        rs = rs.replaceAll("-", "_");
        return rs;
    }
    
    public void writeData(LittleEndianDataOutputStream os) throws IOException{
        os.writeShort(id_r);
        os.writeShort(id_z);
        os.writeLong(id);
    }
    
    public void readData(LittleEndianDataInputStream is) throws IOException{
        id_r = is.readShort();
        id_z = is.readShort();
        id = is.readLong();
    }
}
