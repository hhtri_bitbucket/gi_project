/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ctmlab.game.data.builder;

import com.ctmlab.game.data.model.SocialChat;
import com.dp.db.DB;
import java.sql.SQLException;

/**
 *
 * @author hhtri
 */
public class SocialChatBuilder {
    public long groupId;
    public String sender;
    public byte mType;
    public byte[] data;
    public SocialChatBuilder(){}

    public SocialChatBuilder setGroupID(long groupId) {
        this.groupId = groupId;
        return this;
    }
    public SocialChatBuilder setSender(String sender) {
        this.sender = sender;
        return this;
    }
    public SocialChatBuilder setType(byte mType) {
        this.mType = mType;
        return this;
    }
    public SocialChatBuilder setData(byte[] data) {
        this.data = data;
        return this;
    }
    public SocialChat build(){
        SocialChat sc = new SocialChat(mType, groupId, sender, data);
        sc.time = System.currentTimeMillis();
        return sc;
    }
    
    public SocialChat buildAndSave() throws SQLException{
        SocialChat u = build();
        if(u.save(DB.i().con())<0){return null;}
        return u;
    }
}
