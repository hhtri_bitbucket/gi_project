/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ctmlab.game.data.model;

import com.ctmlab.game.data.ctmon.CTMON;
import com.ctmlab.game.data.ctmon.CTMONDataFieldType;
import com.ctmlab.game.data.pojo.GameResources;
import com.ctmlab.game.data.staticdata.Zone;
import com.ctmlab.game.manager.StaticDataMng;
import com.ctmlab.game.network.manager.WSGameDefine;
import com.dp.db.DataFieldType;
import com.google.common.io.LittleEndianDataInputStream;
import com.google.common.io.LittleEndianDataOutputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;

/**
 *
 * @author hhtri
 */
public class Mine extends GameObjectMap{
//    @CTMONDataFieldType(ignore = true)
//    public byte[] resource = WSGameDefine.EMPTY_BYTES;
    
    // resources
//    @DataFieldType(ignore = true)
//    public GameResources mResource = new GameResources();

    public long price;
    public long test_price;

    @DataFieldType(ignore = true)
    public long[] spotDirt;
    @DataFieldType(ignore = true)
    public Claim claim = null;
    
    public Mine(){
        super();
    }
    
    @Override
    public void refClaim(Claim claim){
        if(claim.mineID<=0){
            claim.mineID = id;
        }
        this.claim = claim;
        spotDirt = claim.spotDirt;
    }
    
    @Override
    public Claim getClaim(){
        return this.claim;
    }

    @Override
    public void setBaseInfo(short id_r, short id_z, short x, short y, byte type, String userID){
        super.setBaseInfo(id_r, id_z, x, y, type, userID);
        test_price = 0;
        price = 0;
    }

    public void setPrices(long test_price, long price){
        this.test_price = test_price;
        this.price = price;
    }

//    public GameResources parse() {
//        mResource = new GameResources();
//        if (resource != null && resource.length>0) {
//            mResource = CTMON.i().fromStream(new LittleEndianDataInputStream(new ByteArrayInputStream(resource)), GameResources.class);
//        }
//        return mResource;
//    }
    
//    public GameResources getResource() {
//        return mResource;
//    }
        
//    public void setResource() {setResource(mResource);}
    
//    public void setResource(GameResources res) {
//        ByteArrayOutputStream baos = new ByteArrayOutputStream();
//        LittleEndianDataOutputStream os = new LittleEndianDataOutputStream(baos);
//        CTMON.i().toStream(os, res);
//        this.resource = baos.toByteArray();
//        this.mResource = res;
//        baos.reset();
//    }

    public void writeDataWithOrder(LittleEndianDataOutputStream os, int orderResource) throws IOException{
        if(orderResource==-1){
            writeData(os);
        }
        super.writeData(os);
//        mResource.writeData(os, orderResource);
        if(spotDirt!=null){
            os.writeInt(spotDirt.length);
            for(long l:spotDirt){
                os.writeLong(l);
            }
        } else{
            os.writeInt(0);
        }
        os.writeLong(price);
        os.writeLong(test_price);
        if(claim!=null && !userID.isEmpty()){
            claim.writeData(os);
        }
    }
    
    @Override
    public void writeData(LittleEndianDataOutputStream os) throws IOException{
        super.writeData(os);
//        mResource.writeData(os);
        if(spotDirt!=null){
            os.writeInt(spotDirt.length);
            for(long l:spotDirt){
                os.writeLong(l);
            }
        } else{
            os.writeInt(0);
        }
        os.writeLong(price);
        os.writeLong(test_price);
        if(claim!=null && !userID.isEmpty()){
            os.writeBoolean(true);
            claim.writeData(os);
        } else{
            os.writeBoolean(false);
        }
    }
    
    @Override
    public void readData(LittleEndianDataInputStream is) throws IOException{
//        super.readData(is);
//        mResource.readData(is);
//        price = is.readLong();
//        test_price = is.readLong();
    }
    
    public void writeDataEmptyMine(LittleEndianDataOutputStream os) throws IOException{
        super.writeData(os);
//        mResource.writeDataEmpty(os);
        os.writeInt(0);
        os.writeLong(price);
        os.writeLong(test_price);
        os.writeBoolean(false);
    }
    
    @Override
    public ArrayList<Integer> getRealLocation(){
        Zone z = StaticDataMng.i().worldMap.getZone(id_z);
        if(z!=null){
            ArrayList<Integer> rs = new ArrayList<>();
            rs.add(z.pos.get(0) + xPos);
            rs.add(z.pos.get(1) + yPos);
            return rs;
        }
        return null;
    }

    @Override
    public void updateState(long time){
        if(claim!=null){
//            claim.startAutoDigging();
            claim.updateState(time);
        }
    }
}