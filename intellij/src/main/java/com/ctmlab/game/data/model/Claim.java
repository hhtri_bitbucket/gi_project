/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ctmlab.game.data.model;

import com.ctmlab.game.data.model.worker.Worker;
import com.ctmlab.game.data.model.equipment.Equipment;
import com.ctmlab.game.data.pojo.GameResources;
import com.ctmlab.game.data.staticdata.gameobjectinfo.GameObjectWashingDirtInfo.RatingInfo;
import com.ctmlab.game.data.staticdata.gameobjectinfo.GameobjectDiggingDirtInfo;
import com.ctmlab.game.goldincenum.ENUM_RESOURCE_TYPE;
import com.ctmlab.game.goldincenum.ENUM_UNIT_TYPE;
import com.ctmlab.game.manager.GlobalConfig;
import com.ctmlab.game.manager.StaticDataMng;
import com.ctmlab.game.network.manager.WSGameDefine;
import com.dp.db.DB;
import com.dp.db.DBDataPostgres;
import com.dp.db.DataFieldType;
import com.dp.db.Func;
import com.google.common.io.LittleEndianDataInputStream;
import com.google.common.io.LittleEndianDataOutputStream;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Map;
import java.util.TreeMap;

/**
 * @author hhtri
 */
public class Claim extends DBDataPostgres {
    @DataFieldType(primaryKey = true, notNull = true, autoIncrease = true, usingMapSeq = true)
    public long id;
    public String userID;
    public short id_z;
    public long mineID;
    public long dirt; // mining and store (having rating) - response to client
    public long fuel; // from homebase to claim - response to client
    public long[] spotDirt;

    public long startTimeDigging; // set this var is System.current when There is enough equips and workers to mining.
    public long curTimeDigging;
    public boolean isDigging = false;
    public long startTimeWashing; // set this var is System.current when There is enough equips and workers to mining.
    public long curTimeWashing;
    public boolean isWashing = false;
    // the data is saved the list "id" equips, workers in the claim
    public byte[] data;

    // resources after the claim washed.
    // noted: Washing must update
    @DataFieldType(ignore = true)
    public GameResources mResource = new GameResources();

    @DataFieldType(ignore = true)
    public Equipment[] lstEquipment;
    @DataFieldType(ignore = true)
    public Worker[] lstWorker;
    @DataFieldType(ignore = true)
    public int curIndexDirt = -1;
    @DataFieldType(ignore = true)
    public boolean working = false;

    public Claim() {
        userID = "";
        mineID = -1;
        dirt = 0;
        fuel = 0;
        id = 0;
        lstEquipment = new Equipment[]{null, null};
        lstWorker = new Worker[]{null, null};
        data = WSGameDefine.EMPTY_BYTES;
        genDirtAllSpot();
    }

    private void genDirtAllSpot() {
        GameobjectDiggingDirtInfo info = StaticDataMng.i().getRepoGameObjectInfo().diggingInfo;
        spotDirt = new long[info.getNumberOfSpot()];
        int i = 0;
        while (i < spotDirt.length) {
            spotDirt[i++] = GlobalConfig.rdLong(info.getMinYard(), info.getMaxYard());
        }
    }

    public void flagToDigging(boolean flag){
        isDigging = flag;
        saveData();
    }

    public void flagToWashing(boolean flag){
        isWashing = flag;
        saveData();
    }

    public void addFuel(long val) {
        if (fuel == -1) {
            fuel = val;
        } else {
            fuel += val;
        }
    }

    public void responseState(LittleEndianDataOutputStream os) throws IOException {
        os.writeLong(id);
        os.writeLong(fuel);
        os.writeInt(spotDirt.length);
        for(long l:spotDirt){
            os.writeLong(l);
        }
        os.writeBoolean(isDigging);
        if(isDigging){
            os.writeLong(dirt);
        }
        os.writeBoolean(isWashing);
        if(isWashing){
            mResource.writeData(os);
        }
    }

//    public void reset(){
//        for(Equipment e:lstEquipment){
//            if(e!=null){
//                e.inClaim = false;
//                e.saveData();
//            }
//        }
//        for(Worker w:lstWorker){
//            if(w!=null){
//                w.inClaim = false;
//                w.saveData();
//            }
//        }
//    }

    // <editor-fold defaultstate="collapsed" desc="Method for save data, set workers, equips">

    public boolean isFullWorker() {
        if (lstWorker == null) {
            lstWorker = new Worker[]{null, null};
            return false;
        }
        boolean rs = true;
        for (Worker w : lstWorker) {
            if (w == null) {
                rs = false;
                break;
            }
        }
        return rs;
    }

    public ArrayList<Byte> lsEquipTypeEmpty() {
        ArrayList<Byte> rs = new ArrayList();
        rs.add((byte) ENUM_UNIT_TYPE.CLAIM_DUMPTRUCK.getCode());
        rs.add((byte) ENUM_UNIT_TYPE.CLAIM_EXCAVATOR.getCode());
        if (lstEquipment == null) {
            lstEquipment = new Equipment[]{null, null};
        }
        for(int i=rs.size()-1; i>=0; i--){
            if(lstEquipment[i]!=null){
                rs.remove(i);
            }
        }
        return rs;
    }

    private void putEquipment(ArrayList<Equipment> lstEquipData) {
        ArrayList<Byte> rs = lsEquipTypeEmpty();
        int len = lstEquipment.length;
        if (!rs.isEmpty()) {
            for (int i = 0; i < len; i++) {
                if (lstEquipment[i] == null) {
                    for (Equipment e : lstEquipData) {
                        if (e.getClaimID()==id) {
                            e.setClaimID(id);
                            lstEquipment[i] = e;
                            e.saveData(); // save 'inClaim' var to Database
                            break;
                        }
                    }
                }
            }
        }
    }

    private void putWorker(ArrayList<Worker> lstWorkerData) {
        if (lstWorker == null) {
            lstWorker = new Worker[]{null, null};
        }
        int len = lstWorker.length;
        if (!isFullWorker()) {
            for (int i = 0; i < len; i++) {
                if (lstWorker[i] == null) {
                    for (Worker w : lstWorkerData) {
                        if (w.claimID==id) {
                            lstWorker[i] = w;
                            w.saveData(); // save 'inClaim' var to Database
                            break;
                        }
                    }
                }
            }
        }
    }

    public void setEquipAndWorkerData(ArrayList<Equipment> lstEquipData, ArrayList<Worker> lstWorker) {
        putEquipment(lstEquipData);
        putWorker(lstWorker);
        saveData();
    }

    // calling in profile private (loading data from 'synReference()' method)
    public void refData(ArrayList<Equipment> lstEquip, ArrayList<Worker> lstW) {
        ByteArrayInputStream bais = new ByteArrayInputStream(data);
        try (LittleEndianDataInputStream is = new LittleEndianDataInputStream(bais)) {
            int len = is.readInt();
            if (len == lstEquipment.length) {
                for (int i = 0; i < len; i++) {
                    long id = is.readLong();
                    if (id > 0) {
                        for (Equipment e : lstEquip) {
                            if (e.id == id) {
                                lstEquipment[i] = e;
                                break;
                            }
                        }
                    }
                }
            }
            len = is.readInt();
            if (len == lstWorker.length) {
                for (int i = 0; i < len; i++) {
                    long id = is.readLong();
                    if (id > 0) {
                        for (Worker w : lstW) {
                            if (w.id == id) {
                                lstWorker[i] = w;
                                break;
                            }
                        }
                    }
                }
            }
            mResource = new GameResources();
            mResource.readData(is);
        } catch (Exception ex) {
        }
    }

    public void saveData() {
        try {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            try (LittleEndianDataOutputStream os = new LittleEndianDataOutputStream(baos)) {
                if (lstEquipment != null) {
                    os.writeInt(lstEquipment.length);
                    for (Equipment e : lstEquipment) {
                        os.writeLong(e == null ? 0L : e.id);
                    }
                }
                if (lstWorker != null) {
                    os.writeInt(lstWorker.length);
                    for (Worker w : lstWorker) {
                        os.writeLong(w == null ? 0L : w.id);
                    }
                }
                mResource.writeData(os);
                data = baos.toByteArray();
            }
            this.save(DB.i().con());
        } catch (IOException | SQLException ex) {
        }
    }

    public void writeData(LittleEndianDataOutputStream os) throws IOException {
        os.writeLong(id);
        os.writeLong(dirt);
        os.writeLong(fuel);
        // equip ref
        os.writeInt(lstEquipment.length);
        for (Equipment e : lstEquipment) {
            os.writeLong(e == null ? 0L : e.id);
        }
        // workers ref
        os.writeInt(lstWorker.length);
        for (Worker w : lstWorker) {
            os.writeLong(w == null ? 0L : w.id);
        }
        mResource.writeData(os);
    }

    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Method for digging the dirt when there is enough (equipments and workers)">

    private int getIndexDirtAvailable(){
        if(curIndexDirt==spotDirt.length){
            return -1;
        } else {
            int i = (curIndexDirt==-1) ? 0 : curIndexDirt;
            while(i<spotDirt.length && spotDirt[i]<=0){
                i++;
            }
            curIndexDirt = (i==spotDirt.length)?-1:i;
            return curIndexDirt;
        }
    }

    private boolean isEmptyDirt(){
        int i = 0;
        while (i < spotDirt.length) {
            if(spotDirt[i]>0){return false;}
            i++;
        }
        return true;
    }

    private boolean _isDigging(){
        if(isEmptyDirt() || !lsEquipTypeEmpty().isEmpty()){return false;}
        else if(!isDigging){
            startTimeDigging = System.currentTimeMillis();
            curTimeDigging = startTimeDigging;
            if(curIndexDirt==-1){
                int i = 0;
                while(i<spotDirt.length && spotDirt[i]<=0){
                    i++;
                }
                curIndexDirt = i;
            }
            flagToDigging(true);
        }
        return true;
    }

    private void updateDigging(long curTimeUpdate) {
        if(!_isDigging()){return;}
        long delta = curTimeUpdate - curTimeDigging;
        long TIME_PER_YARD = StaticDataMng.i().getRepoGameObjectInfo().diggingInfo.getTimeOfYard();
        if (delta >= TIME_PER_YARD) {
            long dirt = delta / TIME_PER_YARD;
            curTimeDigging = curTimeUpdate - (delta % TIME_PER_YARD);
            // todo
            // add dirt in total, reduce in array dirt, save to DB
            int idx = getIndexDirtAvailable();
            if(idx!=-1){
                // reduce dirt in array dirt
                if(spotDirt[idx]<dirt){
                    dirt = spotDirt[idx];
                    spotDirt[idx] = 0;
                } else {
                    spotDirt[idx] -= dirt;
                }
                // add dirt in total
                this.dirt+=dirt;
                // save DB.
                saveData(); // todo Optimize: put to the thread that only using save DB

                System.out.println(String.format("%s - [Delta - %d] - Number of dirt is Digged -> %d", userID, delta, dirt));
                System.out.println(String.format("%s - [dirt - spotDirt] - [%d - %s]", userID, this.dirt, Func.json(spotDirt)));
            }
        }
    }

    private void rdResource(long n) {

        if(n>dirt||dirt<=0){return;}

        Map.Entry<ENUM_RESOURCE_TYPE, RatingInfo>[] temp;
        TreeMap<ENUM_RESOURCE_TYPE, RatingInfo> info = StaticDataMng.i().getRepoGameObjectInfo().washingDirtInfo.getInfo();
        synchronized (info) {
            temp = info.entrySet().toArray(WSGameDefine.EMPTY_ENTRY);
        }
        TreeMap<ENUM_RESOURCE_TYPE, Long> result = new TreeMap();
        for (Map.Entry<ENUM_RESOURCE_TYPE, RatingInfo> e : temp) {
            result.put(e.getKey(), 0L);
        }
        int i = 0;
        while (i++ < n) {
            for (Map.Entry<ENUM_RESOURCE_TYPE, RatingInfo> e : temp) {
                RatingInfo item = e.getValue();
                long val = result.get(e.getKey());
                int ratting = GlobalConfig.GI_RANDOM.nextInt(101);
                if(ratting>=0 && ratting<=item.getRating()){
                    val += GlobalConfig.rdInt(item.getAmountMin(), item.getAmountMax());
                }
                result.put(e.getKey(), val);
//                System.out.println(String.format("%s: %s - %d - %d", userID, Func.json(item), ratting, val));
            }
        }
        for(Map.Entry<ENUM_RESOURCE_TYPE, Long> e:result.entrySet()){
            if(e.getValue()>0){
                mResource.increaseValue(e.getKey(), e.getValue());
            }
        }
        dirt-=n;
        System.out.println(String.format("%s - Washing resource\n%s", userID, Func.pson(mResource)));
    }

    private boolean _isWashing(){
        if(dirt<=0||!lsEquipTypeEmpty().isEmpty()){return false;}
        else if(!isWashing){
            startTimeWashing = System.currentTimeMillis();
            curTimeWashing = startTimeWashing;
            flagToWashing(true);
        }
        return true;
    }

    private void updateWashing(long curTimeUpdate) {
        if(!_isWashing()){return;}
        long delta = curTimeUpdate - curTimeWashing;
        long TIME_WASHING = StaticDataMng.i().getRepoGameObjectInfo().washingDirtInfo.getTimeWashing();
        if (delta >= TIME_WASHING) {
            Long nTimes = delta / TIME_WASHING;
            curTimeWashing = curTimeUpdate - (delta % TIME_WASHING);
            rdResource(nTimes*10);
        }
    }

    private final StringBuilder mLogBuilder = new StringBuilder();
    private void _debugLog(){
        mLogBuilder.append(String.format("Date->%s\n", WSGameDefine.getFullCurrentTime()));
        mLogBuilder.append(String.format("ID->%d\n", id));
        mLogBuilder.append(String.format("SpotDirt->%s\n", Func.json(spotDirt)));
        mLogBuilder.append(String.format("Dirt->%d\n", dirt));
        mLogBuilder.append(String.format("Index spot dirt->%d\n", curIndexDirt));
        mLogBuilder.append(String.format("Current Digging time->%d\n", curTimeDigging));
        mLogBuilder.append(String.format("Resource->%s\n", Func.json(mResource)));
        mLogBuilder.append(String.format("Current Washing time->%d\n", curTimeWashing));
    }

    public void updateState(long time) {
        updateDigging(time);
        updateWashing(time);
//        _debugLog();
    }

    // </editor-fold>

}
