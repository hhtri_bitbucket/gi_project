/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ctmlab.game.data.model;

import com.ctmlab.game.goldincenum.ENUM_RESOURCE_TYPE;
import com.ctmlab.game.goldincenum.ENUM_UNIT_PROPERTY;
import com.ctmlab.game.goldincenum.ENUM_UNIT_TYPE;
import com.ctmlab.game.goldincenum.GI_SERVER_CMD;
import com.ctmlab.game.manager.GoldIncUserMng;
import com.ctmlab.game.manager.StaticDataMng;
import com.ctmlab.game.network.manager.WSServiceServer;
import com.ctmlab.util.Utils;
import com.dp.db.DataFieldType;
import com.google.common.io.LittleEndianDataInputStream;
import com.google.common.io.LittleEndianDataOutputStream;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

/**
 *
 * @author hhtri
 */
public class OilMine extends GameObjectMap{

    public Double curOil = 0d;
    public long curTimeRig = 0;
    public boolean isRigging = false;

    @DataFieldType(ignore = true)
    ByteArrayOutputStream baos = new ByteArrayOutputStream();
    @DataFieldType(ignore = true)
    GoldIncUser owner = null;

    public OilMine(){
        super();
    }

    public void startRig(){
        owner = GoldIncUserMng.i().getUser(userID);
        if(owner == null){return;}
        if(curTimeRig==0){curTimeRig = System.currentTimeMillis();}
        isRigging = !isRigging?true:false;
        curOil = (double) owner.getPlayerProfile().getPrivate().getGameResources().getValue(ENUM_RESOURCE_TYPE.OIL);
        saveData();
    }

    public boolean checkStateRig(){
        return isRigging;
    }

    @Override
    public void setBaseInfo(short id_r, short id_z, short x, short y, byte type, String userID){
        super.setBaseInfo(id_r, id_z, x, y, type, userID);
    }

    @Override
    public void refInfo(){
        if(id==-1){
            // decor
            objInfo = StaticDataMng.i().getRepoGameObjectInfo().lstObjectDecor.get(mType);
        } else{
            // object map
            objInfo = StaticDataMng.i().getRepoGameObjectInfo().lstObjectMap.get(mType);
        }
    }

    @Override
    public void writeData(LittleEndianDataOutputStream os) throws IOException{
        super.writeData(os);
    }

    public void writeDataEmptyMine(LittleEndianDataOutputStream os) throws IOException{
        super.writeData(os);
    }

    public void responseState(){
        baos.reset();
        try(LittleEndianDataOutputStream os = new LittleEndianDataOutputStream(baos)){
            os.writeShort(GI_SERVER_CMD.RES_OIL_RIG_INFO.getCode());
            os.writeLong(curOil.longValue());
            WSServiceServer.i().sendDataWithID(userID, baos.toByteArray());
            if(owner!=null){
                owner.setResourceValue(ENUM_RESOURCE_TYPE.OIL, curOil.longValue());
            }
        } catch (Exception ex){}
    }

    private double getAmountOilInHour(){
        if(objInfo==null){return 0;}
        ArrayList<HashMap<Byte, Long>> info = objInfo.getInfo();
        if(info==null || info.isEmpty()){return 0;}
        HashMap<Byte, Long> hash = info.get(0);
        if(hash.isEmpty()||!hash.containsKey(ENUM_UNIT_PROPERTY.OIL_PRODUCTION.getCode())){
            return 0;
        } else{
            double val = hash.get(ENUM_UNIT_PROPERTY.OIL_PRODUCTION.getCode())*1.0;
            return val/(60*60);
        }
    }

    @Override
    public void updateState(long time){
        if(isRigging) {
            long delta = time - curTimeRig;
            curTimeRig = time;

            long times = delta / 1000;
            curOil += getAmountOilInHour() * times;

            responseState();

            saveData();
        }
    }
}
