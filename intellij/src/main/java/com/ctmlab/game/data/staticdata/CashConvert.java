/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ctmlab.game.data.staticdata;

import com.ctmlab.game.data.ctmon.CTMON;
import com.ctmlab.game.data.ctmon.CTMONDataFieldType;
import com.ctmlab.game.goldincenum.ENUM_RESOURCE_TYPE;
import com.ctmlab.game.goldincenum.GI_SERVER_CMD;
import com.ctmlab.game.manager.GlobalConfig;
import com.ctmlab.game.network.manager.WSGameDefine;
import com.ctmlab.game.network.manager.WSServiceServer;
import com.ctmlab.util.Utils;
import com.dp.db.Func;
import com.google.common.io.LittleEndianDataOutputStream;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.HashMap;

/**
 *
 * @author hhtri
 */
public class CashConvert {

    public String version;
    public HashMap<Byte, CashConvertItem> lstConverter;

    @CTMONDataFieldType(ignore = true)
    public byte[] dataStream = WSGameDefine.EMPTY_BYTES;

    public CashConvert() {
        init();
    }

    private void init() {
        version = "v_2019_" + System.currentTimeMillis();
        byte[] types = ENUM_RESOURCE_TYPE.getKeys();
        String[] sTypes = ENUM_RESOURCE_TYPE.getStringKeys();
        lstConverter = new HashMap<>();
        for (int i = 0; i < types.length; i++) {
            lstConverter.put(types[i], new CashConvertItem(types[i], sTypes[i], 1000, 1100));
        }
        try {
            getDataStream();
        } catch (IOException ex) {
        }
    }

    public void saveData() {
        version = "v_2019_" + System.currentTimeMillis();
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        try (LittleEndianDataOutputStream ledos = new LittleEndianDataOutputStream(baos)) {
            CTMON.i().toStream(ledos, this);
            OutputStream os = new FileOutputStream(new File(GlobalConfig.pathBackup + File.separator + "cashconvert.bin"));
            os.write(baos.toByteArray());
            os.flush();
        } catch (Exception ex) {
        }
    }

    public String saveData(JsonArray arr) {
        version = "v_2019_" + System.currentTimeMillis();
        synchronized (this) {
            for (int i = 0; i < arr.size(); i++) {
                JsonObject o = arr.get(i).getAsJsonObject();
                byte k = o.get("type").getAsByte();
                lstConverter.put(k, new CashConvertItem(o));
            }
        }

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        try (LittleEndianDataOutputStream ledos = new LittleEndianDataOutputStream(baos)) {
            CTMON.i().toStream(ledos, this);
            OutputStream os = new FileOutputStream(new File(GlobalConfig.pathBackup + File.separator + "cashconvert.bin"));
            os.write(baos.toByteArray());
            os.flush();
        } catch (Exception ex) {
            return Func.toString(ex);
        }
        baos.reset();
        try (LittleEndianDataOutputStream os = new LittleEndianDataOutputStream(baos)) {
            os.writeShort(GI_SERVER_CMD.RES_TRADE_CASH_INFO.getCode());
            os.write(getDataStream());
            byte[] data = baos.toByteArray();
            WSServiceServer.i().sendAllClient(data);
        } catch (Exception ex) {
        }
        return "";
    }

    public JsonArray getJsonObject() {
        JsonArray obj = new JsonArray();
        lstConverter.entrySet().forEach((e) -> {
            obj.add(e.getValue().getObject());
        });
        return obj;
    }

    public CashConvertItem getPriceInfo(byte type) {
        return lstConverter.get(type);
    }

    public void syncCashConvert(CashConvert cashConvert) {
        if (lstConverter.size() >= cashConvert.lstConverter.size()) {
            for (CashConvertItem i : lstConverter.values()) {
                if (!cashConvert.lstConverter.containsKey(i.type)) {
                    cashConvert.lstConverter.put(i.type, i);
                }
            }
        }        
    }

    public byte[] getDataStream() throws IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        try (LittleEndianDataOutputStream os = new LittleEndianDataOutputStream(baos)) {
            Utils.i().writeBigString(os, version);
            os.writeInt(lstConverter.size());
            for (CashConvertItem item : lstConverter.values()) {
                item.writeData(os);
            }
        }
        dataStream = baos.toByteArray();
        return dataStream;
    }
}
