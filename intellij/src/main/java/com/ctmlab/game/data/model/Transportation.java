/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ctmlab.game.data.model;

import com.ctmlab.game.data.pojo.TradeTransaction;
import com.ctmlab.game.data.staticdata.PointShort;
import com.ctmlab.game.goldincenum.ENUM_UNIT_TYPE;
import com.dp.db.DB;
import com.dp.db.DataFieldType;
import com.dp.db.Func;
import com.google.common.io.LittleEndianDataInputStream;
import com.google.common.io.LittleEndianDataOutputStream;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Map;
import java.util.TreeMap;

/**
 *
 * @author hhtri
 */
public class Transportation extends GameObjectMap {

    private long startTime;
    private long endTime;
    private long srcID;
    private long destID;
    public int capacity;
    public int level;
    public String jsonTransaction;
   
    @DataFieldType(ignore = true)
    public TradeTransaction tradeTransaction = null;

    public Transportation() {
        setStartTime(-1);
        setEndTime(-1);
        setSrcID(-1);
        setDestID(-1);
        capacity = 1000;
        level = 1;
        jsonTransaction = "";
    }

    public void setInfo(GameObjectMap obj, long src, String user){
        id_r = obj.id_r;
        id_z = obj.id_z;
        xPos = obj.xPos;
        yPos = obj.yPos;
        mType = (byte) ENUM_UNIT_TYPE.TRANSPORTATION.getCode();
        setSrcID(src);
        setDestID(obj.id);
        userID = user;
    }
    
    public void setTransaction(TradeTransaction transacion){
        tradeTransaction = transacion;
    }
    
    @Override
    public ArrayList<PointShort> getPathFinding(){
        if(tradeTransaction!=null){
            return tradeTransaction.getPath();
        }
        return null;
    }
    
    @Override
    public void setPathFinding(ArrayList<PointShort> path){
        if(tradeTransaction!=null){
            tradeTransaction.setPath(path);
        }
    }
        
//    public void unloadResourceNoSave(){
//        if(tradeTransaction!=null){
//            tradeTransaction.unload();
//        }
//    }
    
//    public void unloadResource(){
//        if(tradeTransaction!=null){
//            tradeTransaction.unload();
//        }
//        saveData();
//    }
    
    public void setTimes(long start, long end){
        setStartTime(start);
        setEndTime(end);
    }
    
    public TradeTransaction getTransaction(){
        return tradeTransaction;
    }
    
    public void parseJson(){
        tradeTransaction = Func.gson.fromJson(jsonTransaction, TradeTransaction.class);
    }

    @Override
    public int saveData() {
        jsonTransaction = Func.json(tradeTransaction);
        try {
            return this.save(DB.i().con());
        } catch (Exception ex) {
            return -2;
        }
    }

    @Override
    public void writeData(LittleEndianDataOutputStream os) throws IOException{
        super.writeData(os);
        os.writeLong(getStartTime());
        os.writeLong(getEndTime());
        os.writeLong(getSrcID());
        os.writeLong(getDestID());
        os.writeInt(capacity);
        os.writeInt(level);
        if(tradeTransaction!=null){
            TreeMap<Byte, Long> container = tradeTransaction.getLstTrade();
            os.writeInt(container.size());
            for(Map.Entry<Byte, Long> e:container.entrySet()){
                os.writeByte(e.getKey());
                os.writeLong(e.getValue());
            }
            writePathFinding(os, tradeTransaction.getPath());
        } else{
            os.writeInt(0);
        }
    }
    
    @Override
    public void readData(LittleEndianDataInputStream is) throws IOException{
        super.readData(is);
        setStartTime(is.readLong());
        setEndTime(is.readLong());
        setSrcID(is.readLong());
        setDestID(is.readLong());
        capacity = is.readInt();
        level = is.readInt();
        int len = is.readInt();
        if(len>0){
            tradeTransaction = new TradeTransaction();
            TreeMap<Byte, Long> container = tradeTransaction.getLstTrade();
            int i = 0;
            while(i++<len){
                container.put(is.readByte(), is.readLong());
            }
            if(is.readBoolean()){
                len = is.readInt();
                i = 0;
                ArrayList<PointShort> path = new ArrayList<>();
                while(i++<len){
                    path.add(new PointShort(is.readShort(), is.readShort()));
                }
                tradeTransaction.setPath(path);
            }
        }

    }

    public long getStartTime() {
        return startTime;
    }

    public void setStartTime(long startTime) {
        this.startTime = startTime;
    }

    public long getEndTime() {
        return endTime;
    }

    public void setEndTime(long endTime) {
        this.endTime = endTime;
    }

    public long getSrcID() {
        return srcID;
    }

    public void setSrcID(long srcID) {
        this.srcID = srcID;
    }

    public long getDestID() {
        return destID;
    }

    public void setDestID(long destID) {
        this.destID = destID;
    }
}
