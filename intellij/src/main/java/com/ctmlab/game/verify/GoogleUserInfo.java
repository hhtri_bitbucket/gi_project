/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ctmlab.game.verify;

/**
 *
 * @author hhtri
 */
public class GoogleUserInfo {
    public String sub="";
    public String name="";
    public String email="";
    public String given_name="";
    public String family_name="";
    public String profile="";
    public String picture="";
    public String gender="";
    public String locale="";
    public boolean isSame(GoogleUserInfo info) {
        if(info==null) return false;
        if(!same(info.name,name)) return false;
        if(!same(info.given_name,given_name)) return false;
        if(!same(info.family_name,family_name)) return false;
        if(!same(info.profile,profile)) return false;
        if(!same(info.picture,picture)) return false;
        if(!same(info.gender,gender)) return false;
        if(!same(info.email,email)) return false;
        return same(info.locale,locale);
    }
    
    public static boolean same(String s1,String s2) {
        if(s1==null){return s2==null;}else{return s1.equals(s2);}
    } 
}
