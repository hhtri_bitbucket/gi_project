package com.ctmlab.game.goldincenum;

import java.util.HashMap;

public enum ENUM_UNIT_PROPERTY_AMMO {
    NULL(0),
    COPPER_AMMO(1),
    SILVER_AMMO(2),
    TUNGSTEN(3),
    TITANIUM(4),
    NEODYMIUM(5),
    IRIDIUM(6),
    PLATINUM(7),
    PALLADIUM(8),
    RHODIUM(9),
    URAINIUM(10),
    GOLD(11),
    PLUTONIUM(12),
    ;

    private final int code;
    private static final HashMap<Integer, ENUM_UNIT_PROPERTY_AMMO> CODE_FIELD_TYPE = new HashMap();

    ENUM_UNIT_PROPERTY_AMMO(int code) {
        this.code = code;
    }

    public byte getCode() {
        return (byte) code;
    }

    static {
        for (ENUM_UNIT_PROPERTY_AMMO type : ENUM_UNIT_PROPERTY_AMMO.values()) {
            CODE_FIELD_TYPE.put(type.code, type);
        }
    }

    public static String getString(int i) {
        ENUM_UNIT_PROPERTY_AMMO rs = CODE_FIELD_TYPE.get(i);
        return (rs == null ? NULL.toString().toLowerCase() : rs.toString().toLowerCase());
    }

    public static byte getCode(int i) {
        ENUM_UNIT_PROPERTY_AMMO rs = CODE_FIELD_TYPE.get(i);
        return (byte) (rs == null ? NULL.getCode() : rs.getCode());
    }

    public static byte getCode(String key) {
        ENUM_UNIT_PROPERTY_AMMO rs = ENUM_UNIT_PROPERTY_AMMO.NULL;
        try{
            rs = ENUM_UNIT_PROPERTY_AMMO.valueOf(key.toUpperCase());
        } catch(Exception ex){}
        return rs == null ? NULL.getCode() : rs.getCode();
    }
}