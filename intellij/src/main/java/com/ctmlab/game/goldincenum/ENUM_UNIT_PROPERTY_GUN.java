package com.ctmlab.game.goldincenum;

import java.util.HashMap;

public enum ENUM_UNIT_PROPERTY_GUN {
    NULL(0),
    PISTOL(1),
    RIFLE(2),
    SMG(3),
    ASSULT_RIFLE(4),
    SHOT_GUN(5),
    ROCKET_LAUNCHER(6),
    SNIPER(7),
    MACHINE_GUN(8),
    ADVANCE_PISTOL(9),
    ADVANCE_RIFLE(10),
    ADVANCE_SMG(11),
    TACTICAL_RIFLE(12),
    ADVANCE_SHOTGUN(14),
    ADVANCE_ROCKET_LAUNCHER(14),
    HEAVY_SNIPER(15),
    HEAVY_MACHINE_GUN(16),
    MISSILE_LAUNCHER(17),
    ;

    private final int code;
    private static final HashMap<Integer, ENUM_UNIT_PROPERTY_GUN> CODE_FIELD_TYPE = new HashMap();

    ENUM_UNIT_PROPERTY_GUN(int code) {
        this.code = code;
    }

    public byte getCode() {
        return (byte) code;
    }

    static {
        for (ENUM_UNIT_PROPERTY_GUN type : ENUM_UNIT_PROPERTY_GUN.values()) {
            CODE_FIELD_TYPE.put(type.code, type);
        }
    }

    public static String getString(int i) {
        ENUM_UNIT_PROPERTY_GUN rs = CODE_FIELD_TYPE.get(i);
        return (rs == null ? NULL.toString().toLowerCase() : rs.toString().toLowerCase());
    }

    public static byte getCode(int i) {
        ENUM_UNIT_PROPERTY_GUN rs = CODE_FIELD_TYPE.get(i);
        return (byte) (rs == null ? NULL.getCode() : rs.getCode());
    }

    public static byte getCode(String key) {
        ENUM_UNIT_PROPERTY_GUN rs = ENUM_UNIT_PROPERTY_GUN.NULL;
        try{
            rs = ENUM_UNIT_PROPERTY_GUN.valueOf(key.toUpperCase());
        } catch(Exception ex){}
        return rs == null ? NULL.getCode() : rs.getCode();
    }
}