/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ctmlab.game.goldincenum;

import com.google.gson.JsonArray;
import com.google.gson.JsonPrimitive;
import java.util.ArrayList;
import java.util.HashMap;

/**
 *
 * @author hhtri
 */
public enum ENUM_DECOR_MAP_TYPE {
    
    MOUNTAIN_1(0),
    MOUNTAIN_2(1),
    MOUNTAIN_3(2),
    MOUNTAIN_4(3),
    MOUNTAIN_5(4),    
    MOUNTAIN_6(5),
    MOUNTAIN_7(6),
    TREE_1(7),
    TREE_2(8),
    TREE_3(9),
    TREE_4(10),
    TREE_5(11),
    TREE_6(12),
    TREE_7(13),
    TREE_8(14),
    TREE_9(15),
    TREE_10(16),
    TREE_11(17),
    TREE_12(18),
    GROUND_DECOR_1(19),
    GROUND_DECOR_2(20),
    GROUND_DECOR_3(21),
    GROUND_DECOR_4(22),
    GROUND_DECOR_5(23),
    GROUND_DECOR_6(24),
    GROUND_DECOR_7(25),
    GROUND_DECOR_8(26),
    BUSH_1(27),
    BUSH_2(28),
    BUSH_3(29),
    TREES_1(30),
    TREES_2(31),
    TREES_3(32),
    TREES_4(33),
    TREES_5(34),
    TREES_6(35),  
    REVIER_1(36),
    REVIER_2(37),
    REVIER_3(38),
    REVIER_4(39),
    REVIER_5(40),
    REVIER_6(41),   
    ;
    
    private final int code;
    private static final HashMap<Integer, ENUM_DECOR_MAP_TYPE> CODE_FIELD_TYPE = new HashMap();
    private static final HashMap<String, ENUM_DECOR_MAP_TYPE> S_CODE_FIELD_TYPE = new HashMap();

    ENUM_DECOR_MAP_TYPE(int code) {
        this.code = code;
    }

    public byte getCode(){return (byte)code;}
    
    public static ENUM_DECOR_MAP_TYPE get(int code) {
        return CODE_FIELD_TYPE.get(code);
    }
    
    public static int get(String code) {
        ENUM_DECOR_MAP_TYPE rs = S_CODE_FIELD_TYPE.get(code);
        return (rs==null)?-1:rs.getCode();
    }
    
    public static byte beginTrees(){
        return TREES_1.getCode();
    }
    
    public static byte beginTree(){
        return TREE_1.getCode();
    }
    
    public static byte beginBush(){
        return BUSH_1.getCode();
    }
    
    public static JsonArray getListNames(){
        JsonArray rs = new JsonArray();
        for (ENUM_DECOR_MAP_TYPE type : ENUM_DECOR_MAP_TYPE.values()) {
            rs.add(new JsonPrimitive(type.toString()));
        }
        return rs;
    }

    static {
        for (ENUM_DECOR_MAP_TYPE type : ENUM_DECOR_MAP_TYPE.values()) {
            CODE_FIELD_TYPE.put(type.code, type);
            S_CODE_FIELD_TYPE.put(type.toString(), type);
        }
    }
}
