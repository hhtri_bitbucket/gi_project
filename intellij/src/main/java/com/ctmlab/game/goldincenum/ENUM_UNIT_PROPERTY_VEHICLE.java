package com.ctmlab.game.goldincenum;

import java.util.HashMap;

public enum ENUM_UNIT_PROPERTY_VEHICLE {
    NULL(0),
    TANK(1),
    ATTACK_HELICOPTER(2),
    ADVANCE_TANK(3),
    ADVANCE_HELICOPTER(4),
    MISSILE_LAUNCHER(5),
    ;

    private final int code;
    private static final HashMap<Integer, ENUM_UNIT_PROPERTY_VEHICLE> CODE_FIELD_TYPE = new HashMap();

    ENUM_UNIT_PROPERTY_VEHICLE(int code) {
        this.code = code;
    }

    public byte getCode() {
        return (byte) code;
    }

    static {
        for (ENUM_UNIT_PROPERTY_VEHICLE type : ENUM_UNIT_PROPERTY_VEHICLE.values()) {
            CODE_FIELD_TYPE.put(type.code, type);
        }
    }

    public static String getString(int i) {
        ENUM_UNIT_PROPERTY_VEHICLE rs = CODE_FIELD_TYPE.get(i);
        return (rs == null ? NULL.toString().toLowerCase() : rs.toString().toLowerCase());
    }

    public static byte getCode(int i) {
        ENUM_UNIT_PROPERTY_VEHICLE rs = CODE_FIELD_TYPE.get(i);
        return (byte) (rs == null ? NULL.getCode() : rs.getCode());
    }

    public static byte getCode(String key) {
        ENUM_UNIT_PROPERTY_VEHICLE rs = ENUM_UNIT_PROPERTY_VEHICLE.NULL;
        try{
            rs = ENUM_UNIT_PROPERTY_VEHICLE.valueOf(key.toUpperCase());
        } catch(Exception ex){}
        return rs == null ? NULL.getCode() : rs.getCode();
    }
}