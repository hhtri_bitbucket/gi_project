/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ctmlab.game.goldincenum;

import com.dp.db.DBDataPostgres;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.TreeMap;
import java.util.TreeSet;

/**
 *
 * @author hhtri
 */
public enum CTMON_FieldType {
    NULL(null, null, 0),
    OBJECT(null, null, 1),
    STRING(String.class, StringBuilder.class, 2),
    BYTE(byte.class, Byte.class, 3),
    SHORT(short.class, Short.class, 4),
    INTEGER(int.class, Integer.class, 5),
    LONG(long.class, Long.class, 6),
    FLOAT(float.class, Float.class, 7),
    DOUBLE(double.class, Double.class, 8),
    BOOLEAN(boolean.class, Boolean.class, 9),
    DATE(Date.class, java.sql.Timestamp.class, 10),
    BYTE_ARRAY(byte[].class, Byte[].class, 11),
    ARRAYLIST(null, ArrayList.class, 12),
    HASHMAP(null, HashMap.class, 13),
    ENUM(null, null, 14),
    BYTE_BUFFER(null, ByteBuffer.class, 15),
    ;

    public final Class clazz1;
    public final Class clazz2;
    public final int code;

    CTMON_FieldType(Class clazz1, Class clazz2, int code) {
        this.clazz1 = clazz1;
        this.clazz2 = clazz2;
        this.code = code;
    }
    private static final HashMap<Class, CTMON_FieldType> HASH_FIELD_TYPE = new HashMap();
    private static final HashMap<Integer, CTMON_FieldType> CODE_FIELD_TYPE = new HashMap();

    static {
        for (CTMON_FieldType type : CTMON_FieldType.values()) {
            CODE_FIELD_TYPE.put(type.code, type);
            if (type.clazz1 != null) {
                HASH_FIELD_TYPE.put(type.clazz1, type);
            }
            if (type.clazz2 != null) {
                HASH_FIELD_TYPE.put(type.clazz2, type);
            }
        }
    }

    public static CTMON_FieldType get(Class clazz) {
        if (clazz.isEnum()) {
            return ENUM;
        }
        if (clazz == TreeMap.class) {
            return HASHMAP;
        }
        if (clazz == HashSet.class || clazz == TreeSet.class) {
            return ARRAYLIST;
        }
        CTMON_FieldType type = HASH_FIELD_TYPE.get(clazz);
        return type == null ? OBJECT : type;
    }

    public static CTMON_FieldType get(int code) {
        return CODE_FIELD_TYPE.get(code);
    }
    
    public static CTMON_FieldType get(Object obj) {
        if(obj==null){
            return NULL;
        }
        CTMON_FieldType type = HASH_FIELD_TYPE.get(obj.getClass());
        return type == null ? OBJECT : type;
    }
    
    public CTMON_FieldType getFieldType(int code){
        return CODE_FIELD_TYPE.get(code);
    }
    
    public Class<?> getFieldTypeClass(){
        return clazz1!=null?clazz1:clazz2;
    }
}
