/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ctmlab.game.goldincenum;

import com.google.gson.JsonArray;
import com.google.gson.JsonPrimitive;
import java.util.ArrayList;
import java.util.HashMap;

/**
 *
 * @author hhtri
 */
public enum ENUM_UNIT_TYPE {
    
    NULL(0),
    HOME_BASE(1),
    MINE(2),
    OIL_RIG_BASIC(3),
    OIL_RIG_ADVANCE(4),
    OIL_RIG_ULTRA(5),
    TRADE_CENTER(6),
    EXPLORATION(7),
    TRANSPORTATION(8),
    
    HOME_OFFICE(10),
    HOME_REFINERY(11),
    HOME_BARRACK(12),
    HOME_DEFENSE(13),
    HOME_WEAPON(14),
    HOME_EXPLORATION(15),
    HOME_TRANSPORTATION(16),
    HOME_WALL(17),
    
    CLAIM_EXCAVATOR(20),
    CLAIM_DUMPTRUCK(21),
    CLAIM_WASH_PLANT(22),
    CLAIM_OIL_RIG(23),
    CLAIM_WORKER(24),
    ;
    
    private final int code;
    private static final HashMap<Integer, ENUM_UNIT_TYPE> CODE_FIELD_TYPE = new HashMap();

    ENUM_UNIT_TYPE(int code) {
        this.code = code;
    }

    public byte getCode(){return (byte)code;}
    
    public static ENUM_UNIT_TYPE get(int code) {
        return CODE_FIELD_TYPE.get(code);
    }
    
    public static int get(String code) {
        ENUM_UNIT_TYPE rs = NULL;
        try{
            rs = ENUM_UNIT_TYPE.valueOf(code.toUpperCase());
        } catch (Exception ex){}
        return (rs==NULL)?-1:rs.getCode();
    }

    public static String getString(int _code){
        ENUM_UNIT_TYPE rs = CODE_FIELD_TYPE.get(_code);
        return (rs==NULL)?NULL.toString().toLowerCase():rs.toString().toLowerCase();
    }

    public static ENUM_UNIT_TYPE[] listEnum(){
        return ENUM_UNIT_TYPE.values();
    }

    public static ENUM_UNIT_TYPE getEnum(String code){
        ENUM_UNIT_TYPE rs = NULL;
        try{
            rs = ENUM_UNIT_TYPE.valueOf(code.toUpperCase());
        } catch (Exception ex){}
        return rs;
    }

    public static JsonArray getListNames(){
        JsonArray rs = new JsonArray();
        for (ENUM_UNIT_TYPE type : ENUM_UNIT_TYPE.values()) {
            rs.add(new JsonPrimitive(type.toString()));
        }
        return rs;
    }

    static {
        for (ENUM_UNIT_TYPE type : ENUM_UNIT_TYPE.values()) {
            CODE_FIELD_TYPE.put(type.code, type);
        }
    }
}
