/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ctmlab.game.goldincenum;

import java.util.HashMap;

/**
 *
 * @author hhtri
 */
public enum ENUM_EXPLORATION {
    LAND(10),
    AIR(11),
    ;
    
    private final int code;
    private static final HashMap<Integer, ENUM_EXPLORATION> CODE_FIELD_TYPE = new HashMap();

    ENUM_EXPLORATION(int code) {
        this.code = code;
    }

    public byte getCode(){return (byte) code;}
    
    public static ENUM_EXPLORATION get(int code) {
        return CODE_FIELD_TYPE.get(code);
    }

    static {
        for (ENUM_EXPLORATION type : ENUM_EXPLORATION.values()) {
            CODE_FIELD_TYPE.put(type.code, type);
        }
    }
}
