/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ctmlab.util;

import java.io.IOException;
import java.util.HashMap;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.io.IOUtils;

/**
 *
 * @author ccthien
 */
public class FileUploadRequest {
    public String path = "";
    public String url = "";
    public String data = "";
    public HashMap<String,String>fields = new HashMap();
    public HashMap<String,FileItem>files = new HashMap();
    public String getParameter(String key) {return fields.get(key);}
    public FileItem getFile(String key) {return files.get(key);}
    public byte[] getData(String key) throws IOException {
		FileItem item = files.get(key);
		if(item!=null) {return IOUtils.toByteArray(item.getInputStream());}
		return null;
	}
}
