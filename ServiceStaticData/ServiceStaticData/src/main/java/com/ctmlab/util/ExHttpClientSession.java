package com.ctmlab.util;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.io.IOUtils;
import org.apache.http.*;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.*;
import org.apache.http.entity.AbstractHttpEntity;
import org.apache.http.impl.client.*;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.*;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author ccthien
 */
public class ExHttpClientSession {
    private static final ExHttpClientSession INST = new ExHttpClientSession();
    public static ExHttpClientSession i() {return INST;}
    
    //////////////////////////////////////////////////////////////
    
    private final CloseableHttpClient httpclient;
    public ExHttpClientSession() {
        httpclient = ExHttpClient.i().getHttpClient();
    }
    public ExHttpClientSession(int timeout) {
        httpclient = ExHttpClient.i().getHttpClient(timeout);
    }
    public void close() {
        try {
            httpclient.close();
        } catch (IOException ex) {
            Logger.getLogger(ExHttpClientSession.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    public ExHttpResult post(String URL, String []headers, byte[] data) {
        ExHttpResult result = new ExHttpResult();
        try {
            HttpPost httpPost = new HttpPost(URL);
            httpPost.setEntity(new AbstractHttpEntity() {
                @Override public boolean isRepeatable() { return false; }
                @Override public long getContentLength() { return data.length; }
                @Override public boolean isStreaming() { return false; }
                @Override public InputStream getContent() throws IOException { throw new UnsupportedOperationException(); }
                @Override public void writeTo(final OutputStream outstream) throws IOException {outstream.write(data);}
            });
            if(headers!=null && (headers.length&1)==0) {
                for(int i=0;i<headers.length;i+=2) {
                    httpPost.addHeader(headers[i], headers[i+1]);
                }
            }
            try (CloseableHttpResponse response = httpclient.execute(httpPost)) {
                HttpEntity entity = response.getEntity();
                result.responseCode = response.getStatusLine().getStatusCode();
                result.text = (IOUtils.toString(entity.getContent(), "UTF-8"));
                EntityUtils.consume(entity);
            }
        } catch (Exception ex) {
            Func.printlnLog(this.getClass().getSimpleName()+"_err.log",ex);
        }
        return result;
    }    
    public ExHttpResult post(String URL, String data) {
		return post(URL,null,data.getBytes(StandardCharsets.UTF_8));
	}
    public ExHttpResult post(String URL, HashMap<String,String>parameters) {
        ExHttpResult result = new ExHttpResult();
        try {
            HttpPost httpPost = new HttpPost(URL);
            List<NameValuePair> urlParameters = new ArrayList();
            for(Entry<String,String>e:parameters.entrySet()) {
                urlParameters.add(new BasicNameValuePair(e.getKey(), e.getValue()));
            }
            httpPost.setEntity(new UrlEncodedFormEntity(urlParameters));
            try (CloseableHttpResponse response = httpclient.execute(httpPost)) {
                HttpEntity entity = response.getEntity();
                result.responseCode = response.getStatusLine().getStatusCode();
                result.text = (IOUtils.toString(entity.getContent(), "UTF-8"));
                EntityUtils.consume(entity);
            }
        } catch (Exception ex) {
            Func.printlnLog(this.getClass().getSimpleName()+"_err.log",ex);
        }
        return result;
    }
    public ExHttpResult post(String URL, String[] params) {
        ExHttpResult result = new ExHttpResult();
        try {
            HttpPost httpPost = new HttpPost(URL);
            List<NameValuePair> urlParameters = new ArrayList();
            for(int i=0;i<params.length;i+=2) {
                urlParameters.add(new BasicNameValuePair(params[i], params[i+1]));
            }
            httpPost.setEntity(new UrlEncodedFormEntity(urlParameters));
            try (CloseableHttpResponse response = httpclient.execute(httpPost)) {
                HttpEntity entity = response.getEntity();
                result.responseCode = response.getStatusLine().getStatusCode();
                result.text = (IOUtils.toString(entity.getContent(), "UTF-8"));
                EntityUtils.consume(entity);
            }
        } catch (Exception ex) {
            result.error = ex.toString();
        }
        return result;
    }
    public ExHttpResult get(String URL) {return get(URL,"UTF-8");}
    public ExHttpResult get(String URL,String encode) {
        ExHttpResult result = new ExHttpResult();
        try {
            HttpGet httpGet = new HttpGet(URL);
            try (CloseableHttpResponse response = httpclient.execute(httpGet)) {
                HttpEntity entity = response.getEntity();
                result.responseCode = response.getStatusLine().getStatusCode();
                result.data = IOUtils.toByteArray(entity.getContent());
                result.text = (IOUtils.toString(result.data, encode));
                EntityUtils.consume(entity);
            }

        } catch (Exception ex) {
            result.error = ex.toString();
        }
        return result;
    }
    public ExHttpResult get(String URL,String[] params) {
        if(params.length<2 || (params.length&1)==1) return get(URL);
        StringBuilder sb = new StringBuilder();
        try {
            sb.append(params[0]).append("=").append(URLEncoder.encode(params[1],"UTF-8"));
            for(int i=2;i<params.length;i+=2) {
                sb.append("&").append(params[i]).append("=").append(URLEncoder.encode(params[i+1],"UTF-8"));
            }
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(ExHttpClientSession.class.getName()).log(Level.SEVERE, null, ex);
        }
        return get(URL+"?"+sb);
    }
    public ExHttpResult getWithHeaders(String URL,String[] headers) {
        ExHttpResult result = new ExHttpResult();
        try {
            HttpGet httpGet = new HttpGet(URL);
            if((headers.length&1)==0) {
                for(int i=0;i<headers.length;i+=2) {
                    httpGet.addHeader(headers[i], headers[i+1]);
                }
            }
            try (CloseableHttpResponse response = httpclient.execute(httpGet)) {
                HttpEntity entity = response.getEntity();
                result.responseCode = response.getStatusLine().getStatusCode();
                result.text = (IOUtils.toString(entity.getContent(), "UTF-8"));
                EntityUtils.consume(entity);
            }
        } catch (Exception ex) {
            result.error = ex.toString();
        }
        return result;
    }
    public ExHttpResult postWithHeaders(String URL,String[] headers) {
        ExHttpResult result = new ExHttpResult();
        try {
            HttpPost httpPost = new HttpPost(URL);
            if((headers.length&1)==0) {
                for(int i=0;i<headers.length;i+=2) {
                    httpPost.addHeader(headers[i], headers[i+1]);
                }
            }
            try (CloseableHttpResponse response = httpclient.execute(httpPost)) {
                HttpEntity entity = response.getEntity();
                result.responseCode = response.getStatusLine().getStatusCode();
                result.text = (IOUtils.toString(entity.getContent(), "UTF-8"));
                EntityUtils.consume(entity);
            }
        } catch (Exception ex) {
            result.error = ex.toString();
        }
        return result;
    }
    public ExHttpResult get(String URL,String[] headers,String[] params) {
        if(params.length<2 || (params.length&1)==1) return get(URL,headers);
        StringBuilder sb = new StringBuilder();
        try {
            sb.append(params[0]).append("=").append(URLEncoder.encode(params[1],"UTF-8"));
            for(int i=2;i<params.length;i+=2) {
                sb.append("&").append(params[i]).append("=").append(URLEncoder.encode(params[i+1],"UTF-8"));
            }
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(ExHttpClientSession.class.getName()).log(Level.SEVERE, null, ex);
        }
        return getWithHeaders(URL+"?"+sb,headers);
    }
}
