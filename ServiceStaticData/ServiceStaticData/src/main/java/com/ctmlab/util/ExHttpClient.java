package com.ctmlab.util;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.security.KeyStore;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;
import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import org.apache.commons.io.IOUtils;
import org.apache.http.*;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.*;
import org.apache.http.conn.ssl.*;
import org.apache.http.entity.AbstractHttpEntity;
import org.apache.http.impl.client.*;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.*;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author ccthien
 */
public class ExHttpClient {
    private static ExHttpClient _inst = null;
    public static ExHttpClient i() {if(_inst==null) _inst=new ExHttpClient();return _inst;}
    public static SSLConnectionSocketFactory SSL_SOCKET_FACTORY;
    static {
        try {
            char[] passphrase = "dd2008".toCharArray();
            KeyStore trustStore = KeyStore.getInstance(KeyStore.getDefaultType());
            KeyManagerFactory keyManagerFactory = KeyManagerFactory.getInstance("SunX509");
            try (InputStream fis = ExHttpClient.class.getResourceAsStream("keystore.jks")) {
                trustStore.load(fis, passphrase);
            }
            keyManagerFactory.init(trustStore,passphrase);

            SSLContext sslcontext = SSLContexts.custom().loadTrustMaterial(trustStore, new TrustSelfSignedStrategy()).build();
            sslcontext.init(keyManagerFactory.getKeyManagers(), getAllTrustingManager(), new SecureRandom());
            SSL_SOCKET_FACTORY = new SSLConnectionSocketFactory(sslcontext,new String[] { "SSLv3","TLSv1" },null,SSLConnectionSocketFactory.BROWSER_COMPATIBLE_HOSTNAME_VERIFIER);
        }catch(Exception ex) {
            Func.printlnLog(ExHttpClient.class.getSimpleName()+"_err.log",ex);
        }
    }
    
    public static TrustManager[] getAllTrustingManager() {
        TrustManager[] trustAllCerts = new TrustManager[] { new X509TrustManager() {
            public java.security.cert.X509Certificate[] getAcceptedIssuers() {return null;}
            public void checkClientTrusted(java.security.cert.X509Certificate[] certs, String authType) {}
            public void checkServerTrusted(java.security.cert.X509Certificate[] certs, String authType) {}
        }};
        return trustAllCerts;
    }
    public CloseableHttpClient getHttpClient() {
        return getHttpClient(0);
    }
    public CloseableHttpClient getHttpClient(int timeout) {
        
        if(timeout==0) return HttpClients.custom().setSSLSocketFactory(SSL_SOCKET_FACTORY).build();
        RequestConfig defaultRequestConfig = RequestConfig.custom()
            .setSocketTimeout(timeout)
            .setConnectTimeout(timeout)
            .setConnectionRequestTimeout(timeout)
            .setStaleConnectionCheckEnabled(true)
            .build();
        return HttpClients.custom().setSSLSocketFactory(SSL_SOCKET_FACTORY).setDefaultRequestConfig(defaultRequestConfig).build();
    }

    public String post(String URL, HashMap<String,String>parameters) {
        String result = null;
        try (CloseableHttpClient httpclient = getHttpClient()) {
            HttpPost httpPost = new HttpPost(URL);
            List<NameValuePair> urlParameters = new ArrayList<NameValuePair>();
            for(Entry<String,String>e:parameters.entrySet()) {
                urlParameters.add(new BasicNameValuePair(e.getKey(), e.getValue()));
            }
            httpPost.setEntity(new UrlEncodedFormEntity(urlParameters));            
            try (CloseableHttpResponse response = httpclient.execute(httpPost)) {
                HttpEntity entity = response.getEntity();
                if (response.getStatusLine().getStatusCode() == 200) {
                    result = (IOUtils.toString(entity.getContent(), "UTF-8"));
                }
                EntityUtils.consume(entity);
            }
        } catch (Exception ex) {
            Func.printlnLog(this.getClass().getSimpleName()+"_err.log",ex);
        }
        return result;
    }
    public String post(String URL, HashMap<String,String> parameters, int timeout) {
        String result = null;
        try (CloseableHttpClient httpclient = getHttpClient(timeout)) {
            HttpPost httpPost = new HttpPost(URL);
            List<NameValuePair> urlParameters = new ArrayList<NameValuePair>();
            for(Entry<String,String>e:parameters.entrySet()) {
                urlParameters.add(new BasicNameValuePair(e.getKey(), e.getValue()));
            }
            httpPost.setEntity(new UrlEncodedFormEntity(urlParameters));
            try (CloseableHttpResponse response = httpclient.execute(httpPost)) {
                HttpEntity entity = response.getEntity();
                if (response.getStatusLine().getStatusCode() == 200) {
                    result = (IOUtils.toString(entity.getContent(), "UTF-8"));
                }
                EntityUtils.consume(entity);
            }
        } catch (Exception ex) {
            Func.printlnLog(this.getClass().getSimpleName()+"_err.log",ex);
        }
        return result;
    }
    public String post(String surl,String data) {
        return post(surl,data.getBytes(StandardCharsets.UTF_8));
    }
    public String post(String URL, byte[] data) {
        String result = null;
        try (CloseableHttpClient httpclient = getHttpClient()) {
            HttpPost httpPost = new HttpPost(URL);
            httpPost.setEntity(new AbstractHttpEntity() {
                @Override public boolean isRepeatable() { return false; }
                @Override public long getContentLength() { return data.length; }
                @Override public boolean isStreaming() { return false; }
                @Override public InputStream getContent() throws IOException { throw new UnsupportedOperationException(); }
                @Override public void writeTo(final OutputStream outstream) throws IOException {outstream.write(data);}
            });
            try (CloseableHttpResponse response = httpclient.execute(httpPost)) {
                HttpEntity entity = response.getEntity();
                if (response.getStatusLine().getStatusCode() == 200) {
                    result = (IOUtils.toString(entity.getContent(), "UTF-8"));
                }
                EntityUtils.consume(entity);
            }
        } catch (Exception ex) {
            Func.printlnLog(this.getClass().getSimpleName()+"_err.log",ex);
        }
        return result;
    }
    
    // post json
    public String postJson(String surl,String data, int timeout) {
        return postJson(surl,data.getBytes(StandardCharsets.UTF_8), timeout);
    }
    public String postJson(String URL, byte[] data, int timeout) {
        String result = null;
        try (CloseableHttpClient httpclient = getHttpClient()) {
            HttpPost httpPost = new HttpPost(URL);
            httpPost.setEntity(new AbstractHttpEntity() {
                @Override public boolean isRepeatable() { return false; }
                @Override public long getContentLength() { return data.length; }
                @Override public boolean isStreaming() { return false; }
                @Override public InputStream getContent() throws IOException { throw new UnsupportedOperationException(); }
                @Override public void writeTo(final OutputStream outstream) throws IOException {outstream.write(data);}
            });
//            httpPost.addHeader("Content-Length", String.valueOf(data.length));
            httpPost.addHeader("Content-Type", "application/json");
            try (CloseableHttpResponse response = httpclient.execute(httpPost)) {
                HttpEntity entity = response.getEntity();
                if (response.getStatusLine().getStatusCode() == 200) {
                    result = (IOUtils.toString(entity.getContent(), "UTF-8"));
                }
                EntityUtils.consume(entity);
            }
        } catch (Exception ex) {
            Func.printlnLog(this.getClass().getSimpleName()+"_err.log",ex);
        }
        return result;
    }
    
    public ExHttpResult postEx(String URL, byte[] data) {
        ExHttpResult result = new ExHttpResult();
        try (CloseableHttpClient httpclient = getHttpClient()) {
            HttpPost httpPost = new HttpPost(URL);
            httpPost.setEntity(new AbstractHttpEntity() {
                @Override public boolean isRepeatable() { return false; }
                @Override public long getContentLength() { return data.length; }
                @Override public boolean isStreaming() { return false; }
                @Override public InputStream getContent() throws IOException { throw new UnsupportedOperationException(); }
                @Override public void writeTo(final OutputStream outstream) throws IOException {outstream.write(data);}
            });
            try (CloseableHttpResponse response = httpclient.execute(httpPost)) {
                HttpEntity entity = response.getEntity();
                result.responseCode = response.getStatusLine().getStatusCode();
                result.text = (IOUtils.toString(entity.getContent(), "UTF-8"));
                EntityUtils.consume(entity);
            }
        } catch (Exception ex) {
            Func.printlnLog(this.getClass().getSimpleName()+"_err.log",ex);
        }
        return result;
    }
    
    public static String queryString(HashMap<String,String[]> params) {
        ArrayList<String> list = new ArrayList();
        params.entrySet().stream().forEach((e) -> {
            for(String s:e.getValue()) {
                try {
                    list.add(e.getKey()+"="+URLEncoder.encode(s,"UTF-8"));
                } catch (UnsupportedEncodingException ex) {
					Func.printlnLog(ExHttpClient.class.getSimpleName()+"_err.log",ex);
                }
            }
        });
        return String.join("&", list);
    }
    
    public String getF(String URL) {String r;while((r=get(URL))==null) {try {Thread.sleep(2000);}catch(InterruptedException ex){}}return r;}
    public String get(String URL) {return get(URL,0);}
    public boolean download(String URL,File file) {return download(URL,file,0);}

    public byte[] getBinary(String URL,int timeout) {
        try (CloseableHttpClient httpclient = getHttpClient(timeout)) {
            HttpGet httpGet = new HttpGet(URL);
            try (CloseableHttpResponse response = httpclient.execute(httpGet)) {
                HttpEntity entity = response.getEntity();
                if (response.getStatusLine().getStatusCode() == 200) {
					return IOUtils.toByteArray(entity.getContent());
                }
                EntityUtils.consume(entity);
            }

        } catch (Exception ex) {}
        return null;
    }
    public String get(String URL,int timeout) {
        String result = null;
        try (CloseableHttpClient httpclient = getHttpClient(timeout)) {
            HttpGet httpGet = new HttpGet(URL);
            try (CloseableHttpResponse response = httpclient.execute(httpGet)) {
                HttpEntity entity = response.getEntity();
                if (response.getStatusLine().getStatusCode() == 200) {
                    result = (IOUtils.toString(entity.getContent(), "UTF-8"));
                }
                EntityUtils.consume(entity);
            }

        } catch (Exception ex) {}
        return result;
    }
    public ExHttpResult getEx(String URL,int timeout) {
        ExHttpResult result = new ExHttpResult();
        try (CloseableHttpClient httpclient = getHttpClient(timeout)) {
            HttpGet httpGet = new HttpGet(URL);
            try (CloseableHttpResponse response = httpclient.execute(httpGet)) {
                HttpEntity entity = response.getEntity();
                result.responseCode = response.getStatusLine().getStatusCode();
                result.text = (IOUtils.toString(entity.getContent(), "UTF-8"));
                EntityUtils.consume(entity);
            }

        } catch (Exception ex) {
            result.error = ex.toString();
        }
        return result;
    }
    public boolean download(String URL,File file, int timeout) {
        boolean res = false;
        try (CloseableHttpClient httpclient = getHttpClient(timeout)) {
            HttpGet httpGet = new HttpGet(URL);
            try (CloseableHttpResponse response = httpclient.execute(httpGet)) {
                HttpEntity entity = response.getEntity();
                if (response.getStatusLine().getStatusCode() == 200) {
                    try (FileOutputStream fos = new FileOutputStream(file)) {
                        IOUtils.copy(entity.getContent(), fos);
                        res = true;
                    }
                }
                EntityUtils.consume(entity);
            }
        } catch (Exception ex) {Func.printlnLog(this.getClass().getSimpleName()+"_err.log",ex);}
        return res;
    }
    public ExHttpResult getEx(String URL,int timeout,int retry) {
        ExHttpResult result = new ExHttpResult();
        do {
            try (CloseableHttpClient httpclient = getHttpClient(timeout)) {
                HttpGet httpGet = new HttpGet(URL);
                try (CloseableHttpResponse response = httpclient.execute(httpGet)) {
                    HttpEntity entity = response.getEntity();
                    result.responseCode = response.getStatusLine().getStatusCode();
                    result.text = (IOUtils.toString(entity.getContent(), "UTF-8"));
                    result.error = null;
                    EntityUtils.consume(entity);
                    return result;
                }
            } catch (Exception ex) {
                result.error = ex.toString();
                retry--;
            }
        }while(retry>0);
        return result;
    }
}
