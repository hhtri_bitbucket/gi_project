/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ctmlab.util;

import com.google.common.io.LittleEndianDataInputStream;
import com.google.common.io.LittleEndianDataOutputStream;
import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;
import java.util.Random;
import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

/**
 *
 * @author hhtri
 */
public class Utils {

    public static final Object LOCK = new Object();
    private static Utils inst;
    private final Random rd = new Random();
    public final Charset UTF_8 = Charset.forName("UTF-8");

    public static Utils i() {
        synchronized (LOCK) {
            if (inst == null) {
                inst = new Utils();
            }
            return inst;
        }
    }

    // <editor-fold defaultstate="collapsed" desc="LittleEndianDataInputStream read">
    public int readByte(LittleEndianDataInputStream is) throws IOException {
        return is.readByte();
    }

    // big string ctmlab
    public String readBigString(LittleEndianDataInputStream is) throws IOException {
        int n = is.readInt();
        if (n > 0) {
            byte[] bb = new byte[n];
            is.read(bb);
            return new String(bb, UTF_8);
        }
        return "";
    }

    // big string ctmlab
    public String readBigString(ByteBuffer is) throws IOException {
        int n = is.getInt();
        if (n > 0) {
            byte[] bb = new byte[n];
            is.get(bb);
            return new String(bb, UTF_8);
        }
        return "";
    }

    // unity
    public String readString(LittleEndianDataInputStream is) throws IOException {
        int n = is.read();
        if (n > 0) {
            byte[] bb = new byte[n];
            is.read(bb);
            return new String(bb, UTF_8);
        }
        return "";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="LittleEndianDataInputStream write">
    public void writeBigString(LittleEndianDataOutputStream os, String s) throws IOException {
        byte[] bb = s.getBytes(Func.UTF8);
        os.writeInt(bb.length);
        if (bb.length > 0) {
            os.write(bb);
        }
    }

    public void writeString(LittleEndianDataOutputStream os, String s) throws IOException {
        byte[] bb = s.getBytes(Func.UTF8);
        os.write(bb.length);
        if (bb.length > 0) {
            os.write(bb);
        }
    }
    
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="AES">
    public String sha256(String data) {
        try {
            MessageDigest digest = MessageDigest.getInstance("SHA-256");
            byte[] encodedhash = digest.digest(data.getBytes(StandardCharsets.UTF_8));
            return byteToHex(encodedhash);
        } catch (Exception ex) {
            return "";
        }
    }

    public String randomString(int len) {
        String AB = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
        StringBuilder sb = new StringBuilder(len);
        for (int i = 0; i < len; i++) {
            sb.append(AB.charAt(rd.nextInt(AB.length())));
        }
        return sb.toString();
    }

    private String byteToHex(byte[] data) {
        StringBuilder buf = new StringBuilder();
        for (int i = 0; i < data.length; i++) {
            int halfbyte = (data[i] >>> 4) & 0x0F;
            int two_halfs = 0;
            do {
                if ((0 <= halfbyte) && (halfbyte <= 9)) {
                    buf.append((char) ('0' + halfbyte));
                } else {
                    buf.append((char) ('a' + (halfbyte - 10)));
                }
                halfbyte = data[i] & 0x0F;
            } while (two_halfs++ < 1);
        }

        return buf.toString();
    }

    private byte[] hexToBytes(char[] hex) {
        int length = hex.length / 2;
        byte[] raw = new byte[length];
        for (int i = 0; i < length; i++) {
            int high = Character.digit(hex[i * 2], 16);
            int low = Character.digit(hex[i * 2 + 1], 16);
            int value = (high << 4) | low;
            if (value > 127) {
                value -= 256;
            }
            raw[i] = (byte) value;
        }
        return raw;
    }

    public String aesGenKey() throws NoSuchAlgorithmException {
        KeyGenerator keyGen = KeyGenerator.getInstance("AES");
        keyGen.init(256); // for example
        SecretKey secretKey = keyGen.generateKey();
        return Base64.getEncoder().encodeToString(secretKey.getEncoded());
    }

    public String encryptAES(String dataEncrypt, String secret) {
        try {
            byte[] key = Base64.getDecoder().decode(secret);
            SecretKeySpec secretKey = new SecretKeySpec(key, "AES");
            Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
            cipher.init(Cipher.ENCRYPT_MODE, secretKey);
            return byteToHex(cipher.doFinal(dataEncrypt.getBytes("UTF-8")));
        } catch (Exception e) {

        }
        return "";
    }

    public String decryptAES(String dataDecrypt, String secret) {
        try {
            byte[] key = Base64.getDecoder().decode(secret);
            SecretKeySpec secretKey = new SecretKeySpec(key, "AES");
            Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5PADDING");
            cipher.init(Cipher.DECRYPT_MODE, secretKey);
            return new String(cipher.doFinal(hexToBytes(dataDecrypt.toCharArray())));
        } catch (Exception e) {

        }
        return "";
    }

    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="Random">
    
    
    // </editor-fold>
    
    public byte[] readFile(File pathFile) {
        byte[] rs = new byte[0];
        try (InputStream input = new FileInputStream(pathFile)) {
            byte[] buff = new byte[1024];
            int len = 0;
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            while ((len = input.read(buff)) != -1) {
                baos.write(buff, 0, len);
            }
            rs = baos.toByteArray();
        } catch (IOException ex) {

        }
        return rs;
    }
    
    public byte[] readFile(String pathFile) {
        byte[] rs = new byte[0];
        try (InputStream input = new FileInputStream(pathFile)) {
            byte[] buff = new byte[1024];
            int len = 0;
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            while ((len = input.read(buff)) != -1) {
                baos.write(buff, 0, len);
            }
            rs = baos.toByteArray();
        } catch (IOException ex) {

        }
        return rs;
    }
    
    public String readTextFile(String pathFile) {
        String rs = null;
        try (InputStream input = new FileInputStream(pathFile)) {
            byte[] buff = new byte[1024];
            int len = 0;
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            while ((len = input.read(buff)) != -1) {
                baos.write(buff, 0, len);
            }
            rs = new String(baos.toByteArray(), UTF_8);
        } catch (IOException ex) {

        }
        return rs;
    }

    public void writeTextFile(String pathFile, String value) {
        try (PrintWriter writer = new PrintWriter(new BufferedWriter(new FileWriter(new File(pathFile), false)))) {
            writer.print(value);
        } catch (IOException ex) {

        }
    }
    
    public void writeFile(String pathFile, byte[] value) {
        try (OutputStream os = new FileOutputStream(pathFile)) {
            os.write(value);
            os.flush();
        } catch (IOException ex) {

        }
    }

}
