/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ctmlab.manager;

import com.ctmlab.game.data.ctmon.CTMON;
//import com.ctmlab.game.data.staticdata.gameobjectinfo.GameObjectInfoRepo;
import com.ctmlab.game.data.staticdata.Region;
import com.ctmlab.game.data.staticdata.WorldMap;
import com.ctmlab.util.Utils;
import com.google.common.io.LittleEndianDataInputStream;
import com.google.common.io.LittleEndianDataOutputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.HashMap;


/**
 *
 * @author hhtri
 */
public class DownLoadServiceMng {
    private static final Object LOCK = new Object();
    private static DownLoadServiceMng sInst = null;

    public static DownLoadServiceMng i() {
        synchronized (LOCK) {
            if (sInst == null) {
                sInst = new DownLoadServiceMng();
            }
            return sInst;
        }
    }
    private DownLoadServiceMng(){}
    
    public WorldMap worldMap = new WorldMap();
    
    public WorldMap worldMapStatic = new WorldMap();
    
//    public GameObjectInfoRepo GOIRepo = new GameObjectInfoRepo();
    
    public HashMap<String, String> hashVersion = new HashMap();
    
    public void mapConvertToStream() throws Exception{        
        String prefix = WSGameDefine.pathBackup;
        byte[] buff = new byte[1024];
        int len = 0;
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        try (InputStream is = new FileInputStream(prefix + File.separator + "map.bin")) {
            while ((len = is.read(buff)) != -1) {
                baos.write(buff, 0, len);
            }
        }

        ByteArrayInputStream bais = new ByteArrayInputStream(baos.toByteArray());
        baos.reset();
        LittleEndianDataInputStream lsDis = new LittleEndianDataInputStream(bais);
        WorldMap worldMapTemp = CTMON.i().fromStream(lsDis, WorldMap.class);
        try(LittleEndianDataOutputStream os = new LittleEndianDataOutputStream(baos)){            
            worldMapTemp.writeData(os);            
            byte[] data = baos.toByteArray();
            Utils.i().writeFile(prefix + File.separator + "map.bin", data);

        }
        baos.reset();
        try (InputStream is = new FileInputStream(new File(prefix + File.separator + "regions.txt"))) {
            while ((len = is.read(buff)) != -1) {baos.write(buff, 0, len);}
        }
        String sRegion = new String(baos.toByteArray(), WSGameDefine.UTF_8);
        baos.reset();
        
        String a[] = sRegion.split(",");
        for (String region : a) {
            baos.reset();
            try (InputStream is = new FileInputStream(new File(prefix + File.separator + region))) {
                while ((len = is.read(buff)) != -1) {baos.write(buff, 0, len);}
            }
            bais = new ByteArrayInputStream(baos.toByteArray());
            baos.reset();
            lsDis = new LittleEndianDataInputStream(bais);
            Region regionTemp = CTMON.i().fromStream(lsDis, Region.class);
            try(LittleEndianDataOutputStream os = new LittleEndianDataOutputStream(baos)){
                regionTemp.writeData(os);            
                byte[] data = baos.toByteArray();
                Utils.i().writeFile(prefix + File.separator + region, data);
            }
        }
        
    }
    
    public void readResource() throws Exception{
        
        readVersions();
        
//        readGameObjectInfoConfig();
        
//        readMap();
    }
    
    private void readMap() throws Exception{
        String prefix = WSGameDefine.pathBackup;
        byte[] buff = new byte[1024];
        int len = 0;
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        try (InputStream is = new FileInputStream(prefix + File.separator + "map.bin")) {
            while ((len = is.read(buff)) != -1) {
                baos.write(buff, 0, len);
            }
        }
        byte[] data = baos.toByteArray();
        
        baos.reset();
        ByteArrayInputStream bais = new ByteArrayInputStream(data);
        LittleEndianDataInputStream lsDis = new LittleEndianDataInputStream(bais);
        worldMap = new WorldMap();
        worldMap.lenFile = data.length;
        worldMap.readData(lsDis);

        try (InputStream is = new FileInputStream(new File(prefix + File.separator + "regions.txt"))) {
            while ((len = is.read(buff)) != -1) {
                baos.write(buff, 0, len);
            }
        }
        String sRegion = new String(baos.toByteArray(), WSGameDefine.UTF_8);
        for (String region : sRegion.split(",")) {
            baos.reset();
            try (InputStream is = new FileInputStream(new File(prefix + File.separator + region))) {
                while ((len = is.read(buff)) != -1) {
                    baos.write(buff, 0, len);
                }
            }
            data = baos.toByteArray();        
            baos.reset();
            bais = new ByteArrayInputStream(data);
            lsDis = new LittleEndianDataInputStream(bais);
            Region regionTemp = new Region();
            regionTemp.readData(lsDis);
            for(Region r:worldMap.regions.values()){
                if(r.id == regionTemp.id){
                    r.addZones(regionTemp.zones);
                    r.lenFile = data.length;
                    break;
                }
            }
        }        
        worldMapStatic = worldMap.copyStatic();
    }
    
    private void readGameObjectInfoConfig(){
//        String path = WSGameDefine.pathBackup + File.separator + "gameobjectinfo.bin";
//        GOIRepo.readGameObjectInfo(path);
    }
    
    private void readVersions(){
        String path = WSGameDefine.pathBackup+File.separator+"version.txt";
        String data = Utils.i().readTextFile(path);
        if(data!=null&&!data.isEmpty()){
            hashVersion = WSGameDefine.gson.fromJson(data, HashMap.class);
        }else{
            hashVersion = new HashMap<>();
        }
        
    }
    
    public void init(){
        try{
            WSGameDefine.initFolders();        
            readVersions();
        }catch(Exception ex){}
    }
}
