/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ctmlab.manager;
import com.ctmlab.websocket.client.MSClient;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;
/**
 *
 * @author hhtri
 */
@WebListener()
public class startup implements ServletContextListener {

    @Override
    public void contextInitialized(ServletContextEvent sce) {
        DownLoadServiceMng.i().init();
        MSClient.i().startWS();
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        MSClient.i().stopWS();
    }
    
}
