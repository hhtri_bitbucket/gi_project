/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ctmlab.manager;

import com.ctmlab.util.Func;
import com.ctmlab.util.Utils;
import com.ctmlab.websocket.client.WSServiceClient;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import java.io.File;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.nio.ByteOrder;
import java.nio.charset.Charset;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

/**
 *
 * @author hhtri
 */
public class WSGameDefine {
    public static final byte[] EMPTY_BYTES = new byte[0];
    public static final Map.Entry[] EMPTY_ENTRY = new Map.Entry[0];
    public static final String[] EMPTY_STRING = new String[0];
    public static final WSServiceClient[] EMPTY_WORKER = new WSServiceClient[0];
    
    public static final Charset UTF_8 = Charset.forName("UTF-8");
    public static final ByteOrder BYTE_ORDER = ByteOrder.LITTLE_ENDIAN;
    
    public static final long MILISEC_IN_DAY = 24*60*60*1000; 
    
    public static final int NUMBET_OF_FRAME = 512*1024;
    public static final int WS_BUFFER_SIZE = 65536;
    public static final int MAP_INITIAL_CAPACITY = 512;
    
    public static final Gson gson = new Gson();
    public static final Gson pson = new GsonBuilder().serializeNulls().setPrettyPrinting().create();
    
    public static SimpleDateFormat sSimpleDay = new SimpleDateFormat("yyyyMMdd");
    public static SimpleDateFormat sSimpleTimeDay = new SimpleDateFormat("yyyyMMddHHmmss");    
    
    public static String now(){
        return sSimpleTimeDay.format(new Date());
    }
    
    public static String toString(Throwable ex) {
        StringWriter w = new StringWriter();
        ex.printStackTrace(new PrintWriter(w));
        return w.toString();
    }
    
    // <editor-fold defaultstate="collapsed" desc="HOST">
    
    public static int PORT_SERVER = 8080;
//    public static String HOST = "http://192.168.9.22:8084";
    public static String HOST = WSGameDefine.isOnlineServer() ? "http://test.ctmlab.com:8080":("http://192.168.9.22:"+PORT_SERVER);
    public static String WS = WSGameDefine.isOnlineServer() ? "ws://test.ctmlab.com:8080/goldinc/microservice":("ws://localhost:"+PORT_SERVER+"/goldinc/microservice");
    
    // </editor-fold>
        
    // <editor-fold defaultstate="collapsed" desc="LOG FOLDER">
    public static final String prefix = isOnlineServer() ? ("/mnt/external/microservice/" + PORT_SERVER +"/") 
            : ("/home/hhtri/Desktop/workspace/netbean/external/microservice/" + PORT_SERVER +"/");
//    public static final String prefix = "/mnt/external/";
    // log text
    public static final String pathLogText = prefix + "log_text";
    // log bin
    public static final String pathLogBin = prefix + "log_bin";
    // bak
    public static final String pathBackup = prefix + "backup";
    // config
    public static final String pathConfig = prefix + "config";
    
    public static boolean isOnlineServer(){
        if(Func.isLinuxServer()){
            String cmd = "uname -n";
            String rs = Func.exec(cmd);
            return !rs.contains("hhtri");
        } else{
            return false;
        }
    }
    
    public static void initFolders(){
        File f = new File(pathLogText);
        if(!f.exists()){f.mkdirs();}
        f = new File(pathLogBin);
        if(!f.exists()){f.mkdirs();}
        f = new File(pathBackup);
        if(!f.exists()){f.mkdirs();}
        f = new File(pathConfig);
        if(!f.exists()){f.mkdirs();}
        
//        f = new File(prefix+File.separator+"config.txt");
//        if(!f.exists()){
//            PORT_SERVER = 8080;
//            HOST = (WSGameDefine.isOnlineServer() ? "http://test.ctmlab.com:":"http://localhost:" +PORT_SERVER);
//            WS = (WSGameDefine.isOnlineServer() ? "ws://test.ctmlab.com:":"ws://localhost:")+PORT_SERVER+"/goldinc/microservice";
//            String s = "";
//            s+=PORT_SERVER + "\n";
//            s+=HOST + "\n";
//            s+=WS;
//            Utils.i().writeTextFile(f.getPath(), s);
//        } else{
//            String[] a = Utils.i().readTextFile(f.getPath()).split("\n");
//            PORT_SERVER = Integer.parseInt(a[0]);
//            HOST = a[1];
//            WS = a[2];
//        }
    }
    
    public static void initConfigEmail(){
        JsonObject obj = new JsonObject();
        obj.addProperty("email", "servicectmlab2019@gmail.com");
        obj.addProperty("sender_name", "GoldInc Service");
        obj.addProperty("passwd", "t123456789!");
        obj.addProperty("port", 587);
        obj.addProperty("subject", "GoldInc registry account");
        obj.addProperty("content", "Hi #email,\n #link_verify \n Goodluck");
        String json = Func.pson(obj);
        String path = pathConfig + File.separator + "send_email.json";
        Utils.i().writeTextFile(path, json);
    }
    
    // </editor-fold>    
}
