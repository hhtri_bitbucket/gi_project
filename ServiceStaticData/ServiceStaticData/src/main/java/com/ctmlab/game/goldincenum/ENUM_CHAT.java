/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ctmlab.game.goldincenum;

import java.util.HashMap;

/**
 *
 * @author hhtri
 */
public enum ENUM_CHAT {
    
    CHAT_ALLIANCE(1),
    CHAT_REGION(2),
    ;
    
    private final int code;
    private static final HashMap<Integer, ENUM_CHAT> CODE_FIELD_TYPE = new HashMap();

    ENUM_CHAT(int code) {
        this.code = code;
    }

    public byte getCode(){return (byte) code;}
    
    public static ENUM_CHAT get(int code) {
        return CODE_FIELD_TYPE.get(code);
    }

    static {
        for (ENUM_CHAT type : ENUM_CHAT.values()) {
            CODE_FIELD_TYPE.put(type.code, type);
        }
    }
}
