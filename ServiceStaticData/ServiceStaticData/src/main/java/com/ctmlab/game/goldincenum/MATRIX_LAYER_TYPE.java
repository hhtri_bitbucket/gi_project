/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ctmlab.game.goldincenum;

import java.util.HashMap;

/**
 *
 * @author hhtri
 */
public enum MATRIX_LAYER_TYPE {
    NONE(0),
    TREE(1),
    MOUNTAIN(2),
    GROUND(3),
    WATER(4),
    SHORE(5),
    HOMEBASE(6),
    MINE(7),
    TRADE_CENTER(8),
    ;
    
    private final int code;
    private static final HashMap<Integer, MATRIX_LAYER_TYPE> CODE_FIELD_TYPE = new HashMap();

    MATRIX_LAYER_TYPE(int code) {
        this.code = code;
    }

    public byte getCode(){return (byte) code;}
    
    public static MATRIX_LAYER_TYPE get(int code) {
        return CODE_FIELD_TYPE.get(code);
    }

    static {
        for (MATRIX_LAYER_TYPE type : CODE_FIELD_TYPE.values()) {
            CODE_FIELD_TYPE.put(type.code, type);
        }
    }
}
