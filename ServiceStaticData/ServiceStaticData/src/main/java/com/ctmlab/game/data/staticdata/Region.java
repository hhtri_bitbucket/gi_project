/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ctmlab.game.data.staticdata;

import com.ctmlab.game.data.ctmon.CTMONDataFieldType;
import com.ctmlab.util.Utils;
import com.google.common.io.LittleEndianDataInputStream;
import com.google.common.io.LittleEndianDataOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;

/**
 *
 * @author hhtri
 */
public class Region implements DataStreamListener{
    
    @CTMONDataFieldType(ignore = true)
    public int lenFile;
    
    public short id;
    public String name;
    public ArrayList<Integer> pos;
    public HashMap<Short, Zone> zones;

    public Region(){
        id = 0;
        pos = new ArrayList();
        zones = new HashMap();
        name = "";
    }   
    
    public Region(short id, int posX, int posY, String name){
        this.id = id;
        pos = new ArrayList();
        pos.add(posX);
        pos.add(posY);
        zones = new HashMap<>();
        this.name = name;
    }
    
    public void addZones(HashMap<Short, Zone> hZone){
        for(Entry<Short, Zone> e:hZone.entrySet()){
            Zone z = e.getValue();
            synchronized(zones){
                zones.put(e.getKey(), z);
            }
        }
    }

    @Override
    public void writeData(LittleEndianDataOutputStream os) throws IOException {
        os.writeShort(id);
        Utils.i().writeBigString(os, name);
        os.writeInt(pos.size());
        for(int p:pos){
            os.writeInt(p);
        }
        os.writeInt(zones.size());
        for(Entry<Short, Zone> z:zones.entrySet()){
            os.writeShort(z.getKey());
            z.getValue().writeData(os);
        }
    }

    @Override
    public void readData(LittleEndianDataInputStream is) throws IOException {
        id = is.readShort();
        name = Utils.i().readBigString(is);
        int len = is.readInt();
        int i = 0;
        pos = new ArrayList<>();
        while(i++<len){
            pos.add(is.readInt());
        }
        len = is.readInt();
        i = 0;
        zones = new HashMap<>();
        while(i++<len){
            short key = is.readShort();
            Zone z = new Zone();
            z.readData(is);
            zones.put(key, z);
        }
    }
}
