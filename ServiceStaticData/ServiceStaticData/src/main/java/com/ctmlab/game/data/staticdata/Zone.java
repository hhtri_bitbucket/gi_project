/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ctmlab.game.data.staticdata;
import com.ctmlab.util.Utils;
import com.google.common.io.LittleEndianDataInputStream;
import com.google.common.io.LittleEndianDataOutputStream;
import java.io.IOException;
import java.util.ArrayList;
/**
 *
 * @author hhtri
 */
public class Zone implements DataStreamListener{
    
   
    public short id;
    public short id_r;
    public String name;
    public ArrayList<Integer> pos;
    public ArrayList<DecorLayer> layerDecor = new ArrayList<>();    

    public Zone(){        
        id = 0;
        pos = new ArrayList();
        layerDecor = new ArrayList<>();
    }

    @Override
    public void writeData(LittleEndianDataOutputStream os) throws IOException {
        os.writeShort(id);
        os.writeShort(id_r);
        Utils.i().writeBigString(os, name);
        os.writeInt(pos.size());
        for(int p:pos){
            os.writeInt(p);
        }
        os.writeInt(layerDecor.size());
        for(DecorLayer dl:layerDecor){
            dl.writeData(os);
        }
    }

    @Override
    public void readData(LittleEndianDataInputStream is) throws IOException {
        id = is.readShort();
        id_r = is.readShort();
        name = Utils.i().readBigString(is);
        int len = is.readInt();
        int i = 0;
        pos = new ArrayList<>();
        while(i++<len){
            pos.add(is.readInt());
        }
        len = is.readInt();
        i = 0;
        layerDecor = new ArrayList<>();
        while(i++<len){
            DecorLayer dl = new DecorLayer();
            dl.readData(is);
            layerDecor.add(dl);
        }
    }
}