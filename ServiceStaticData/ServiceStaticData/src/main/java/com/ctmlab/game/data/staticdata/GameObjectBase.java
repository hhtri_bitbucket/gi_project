/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ctmlab.game.data.staticdata;

import com.google.common.io.LittleEndianDataInputStream;
import com.google.common.io.LittleEndianDataOutputStream;
import java.io.IOException;

/**
 *
 * @author hhtri
 */
public class GameObjectBase implements DataStreamListener{
    public long id; 
    public short id_r;
    public short id_z;
    public short xPos;
    public short yPos;
    public GameObjectBase(){}

    @Override
    public void writeData(LittleEndianDataOutputStream os) throws IOException {
        os.writeLong(id);
        os.writeShort(id_r);
        os.writeShort(id_z);
        os.writeShort(xPos);
        os.writeShort(yPos);
    }

    @Override
    public void readData(LittleEndianDataInputStream is) throws IOException {
        id = is.readLong();
        id_r = is.readShort();
        id_z = is.readShort();
        xPos = is.readShort();
        yPos = is.readShort();
    }
    
    public void writeDecorData(LittleEndianDataOutputStream os) throws IOException {

        os.writeShort(id_r);
        os.writeShort(id_z);
        os.writeShort(xPos);
        os.writeShort(yPos);
    }

    public void readDecorData(LittleEndianDataInputStream is) throws IOException {
        id = -1;
        id_r = is.readShort();
        id_z = is.readShort();
        xPos = is.readShort();
        yPos = is.readShort();
    }
}
