/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ctmlab.game.data.staticdata;

import com.ctmlab.game.data.ctmon.CTMONDataFieldType;
import com.ctmlab.util.Utils;
import com.google.common.io.LittleEndianDataInputStream;
import com.google.common.io.LittleEndianDataOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

/**
 *
 * @author hhtri
 */
public class WorldMap implements DataStreamListener{    
    @CTMONDataFieldType(ignore = true)
    public int lenFile;
    
    public String version;
    public String name;
    public HashMap<String, ArrayList<Integer>> metadata;
    public HashMap<Short, Region> regions;
    
    @CTMONDataFieldType(ignore = true)
    public final HashMap<Short, GameObjectMap> lstTradeCenter = new HashMap();

    public WorldMap(){
        name = "";
        version = "0.0.0";
        metadata = new HashMap();
        regions = new HashMap<>();
    }
    
    public WorldMap copyStatic(){
        WorldMap m = new WorldMap();
        m.version = version;
        m.metadata = new HashMap();
        m.regions = new HashMap();
        
        for(Map.Entry<String, ArrayList<Integer>>e:metadata.entrySet()){
            ArrayList<Integer> ls = new ArrayList<>(e.getValue());
            m.metadata.put(e.getKey(), ls);
        }
        
        for(Region item:regions.values()){
            Region r = new Region(item.id, item.pos.get(0), item.pos.get(1), item.name);
            m.regions.put(item.id, r);
        }
        m.lenFile = this.lenFile;
        return m;
    }
    
    public Region getRegion(short id) {
        Region rs = regions.get(id);
        return rs;
    }

    public Zone getZone(short id) {
        Zone rs = null;
        for(Region r:regions.values()){
            if(r.zones.containsKey(id)){
                rs = r.zones.get(id);
            }
            if(rs!=null){
                break;
            }
        }
        return rs;
    }

    @Override
    public void writeData(LittleEndianDataOutputStream os) throws IOException {
        Utils.i().writeBigString(os, version);
        Utils.i().writeBigString(os, name);
        os.writeInt(metadata.size());
        for(Entry<String, ArrayList<Integer>>e:metadata.entrySet()){
            Utils.i().writeBigString(os, e.getKey());
            os.writeInt(e.getValue().size());
            for(int i:e.getValue()){
                os.writeInt(i);
            }
        }
        os.writeInt(regions.size());
        for(Entry<Short, Region> z:regions.entrySet()){
            os.writeShort(z.getKey());
            z.getValue().writeData(os);
        }
    }

    @Override
    public void readData(LittleEndianDataInputStream is) throws IOException {
        version = Utils.i().readBigString(is);
        name = Utils.i().readBigString(is);
        int len = is.readInt();
        int i =0;
        metadata = new HashMap<>();
        while(i++<len){
            String key = Utils.i().readBigString(is);
            ArrayList<Integer> value = new ArrayList<>();
            int n = is.readInt();
            int j = 0;
            while(j++<n){
                value.add(is.readInt());
            }
            metadata.put(key, value);
        }
        len = is.readInt();
        i = 0;
        regions = new HashMap<>();
        while(i++<len){
            short key = is.readShort();
            Region value = new Region();
            value.readData(is);
            regions.put(key, value);
        }
    }
}