/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ctmlab.game.goldincenum;

import java.util.HashMap;

/**
 *
 * @author hhtri
 */
public enum ENUM_BUILDING {
    
    OFFICE(1),
    BARRACK(2),
    REFINERY(3),
    TRADE_CENTER(4),
    DEFENSE(5),
    WEAPON_DEPOT(6),
    EXPLORATION_CENTER(7),
    WALL(8),
    TRANSPORT_DEPOT(9)
    ;
    
    private final int code;
    private static final HashMap<Integer, ENUM_BUILDING> CODE_ENUM_BUILDING = new HashMap();

    ENUM_BUILDING(int code) {
        this.code = code;
    }
    
    public int getCode(){
        return code;
    }

    public static ENUM_BUILDING get(int code) {
        return ENUM_BUILDING.get(code);
    }

    static {
        for (ENUM_BUILDING type : CODE_ENUM_BUILDING.values()) {
            CODE_ENUM_BUILDING.put(type.code, type);
        }
    }
}
