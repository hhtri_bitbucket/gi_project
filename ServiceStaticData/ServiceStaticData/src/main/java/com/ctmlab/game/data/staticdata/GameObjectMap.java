/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ctmlab.game.data.staticdata;

import com.ctmlab.util.Utils;
import com.google.common.io.LittleEndianDataInputStream;
import com.google.common.io.LittleEndianDataOutputStream;
import java.io.IOException;

/**
 *
 * @author hhtri
 */
public class GameObjectMap extends GameObjectBase {
    
    public String userID = "";
    public byte mType = 0;       
    public GameObjectMap(){super();}   
    
    @Override
    public void writeData(LittleEndianDataOutputStream os) throws IOException {
        super.writeData(os);
        Utils.i().writeBigString(os, userID);
        os.writeByte(mType);
    }

    @Override
    public void readData(LittleEndianDataInputStream is) throws IOException {
        super.readData(is);
        userID = Utils.i().readBigString(is);
        mType = is.readByte();
    }
    
    @Override
    public void writeDecorData(LittleEndianDataOutputStream os) throws IOException {
        super.writeDecorData(os);
        os.writeByte(mType);
    }
    
    @Override
    public void readDecorData(LittleEndianDataInputStream is) throws IOException {
        super.readDecorData(is);
        mType = is.readByte();
    }
    
}
