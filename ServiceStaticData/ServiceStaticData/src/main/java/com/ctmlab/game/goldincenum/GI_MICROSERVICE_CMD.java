/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ctmlab.game.goldincenum;

import java.util.HashMap;

/**
 *
 * @author thelight
 */
public enum GI_MICROSERVICE_CMD {
    
    Null(0), 
    REQ_CONNECT(1),
    REQ_GAMEOBJECT_INFO(2),
    REQ_MAP_INFO(3),
    REQ_MAP_INFO_EXTRA(4),
    REQ_REGION_DATA(5),
    REQ_HOMEBASE_CONFIG(6),
    
    //---
    RES_CONNECT(2021),
    RES_GAMEOBJECT_INFO(2022),
    RES_MAP_INFO(2023),
    RES_MAP_INFO_EXTRA(2024),
    RES_REGION_DATA(2025),
    RES_HOMEBASE_CONFIG(2026),
        
    RES_PING(9998),
    IDLE(9999),
    TEST_WS(10000)
    ;
    
    private static final HashMap<Integer, GI_MICROSERVICE_CMD> hashID = new HashMap();

    private final int code;  
    
    GI_MICROSERVICE_CMD(int _code) {code = _code;}
    
    public int getCode() {return code;}

    static {
        for (GI_MICROSERVICE_CMD type : values()) {
            hashID.put(type.code, type);
        }
    }

    public static GI_MICROSERVICE_CMD fromID(int _code) {
        return hashID.get(_code) == null ? Null : hashID.get(_code);
    }
}
