/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ctmlab.game.goldincenum;

import java.util.HashMap;

/**
 *
 * @author hhtri
 */
public enum ENUM_UNIT_PROPERTY {
    NULL(0),
    MAX_CLAIM(1),
    SPEED_LAND(2),
    SPEED_AIR(3)
    ;
    
    private final int code;
    private static final HashMap<Integer, ENUM_UNIT_PROPERTY> CODE_FIELD_TYPE = new HashMap();
    private static final HashMap<String, ENUM_UNIT_PROPERTY> S_CODE_FIELD_TYPE = new HashMap();

    ENUM_UNIT_PROPERTY(int code) {
        this.code = code;
    }

    public byte getCode(){return (byte)code;}
    
    static {
        for (ENUM_UNIT_PROPERTY type : ENUM_UNIT_PROPERTY.values()) {
            CODE_FIELD_TYPE.put(type.code, type);
            S_CODE_FIELD_TYPE.put(type.toString().toLowerCase(), type);
        }
    }
    
    public static byte getCode(int i){
        ENUM_UNIT_PROPERTY rs = CODE_FIELD_TYPE.get(i);
        return (byte)(rs==null?NULL.getCode():rs.getCode());
    }
    
    public static byte getCode(String key){
        ENUM_UNIT_PROPERTY rs = S_CODE_FIELD_TYPE.get(key);
        return rs==null?NULL.getCode():rs.getCode();
    }
}
