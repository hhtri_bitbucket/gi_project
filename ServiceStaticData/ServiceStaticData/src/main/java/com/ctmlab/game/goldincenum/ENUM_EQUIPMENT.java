/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ctmlab.game.goldincenum;

import java.util.HashMap;

/**
 *
 * @author hhtri
 */
public enum ENUM_EQUIPMENT {
    EXCAVATOR(1),
    DUMPTRUCK(2),
    WASH_PLANT(3),
    OIDRIG(4)
    ;
    
    public final int code;
    private static final HashMap<Integer, ENUM_EQUIPMENT> CODE_ENUM_EQUIPMENT = new HashMap();

    ENUM_EQUIPMENT(int code) {
        this.code = code;
    }

    public static ENUM_EQUIPMENT get(int code) {
        return ENUM_EQUIPMENT.get(code);
    }

    static {
        for (ENUM_EQUIPMENT type : CODE_ENUM_EQUIPMENT.values()) {
            CODE_ENUM_EQUIPMENT.put(type.code, type);
        }
    }
}
