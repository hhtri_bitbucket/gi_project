/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ctmlab.game.goldincenum;

import java.util.HashMap;

/**
 *
 * @author hhtri
 */
public enum ENUM_RESOURCE_TYPE {
    GOLD(1),
    SILVER(2),
    IRON(3),
    COPPER(4),
    PLATINUM(5),
    TITANIUM(6),
    DIAMOND(7),
    RUBY(8),
    EMERALD(9),
    SAPPHIRE(10),
    OIL(11),
    FUEL(12),
    CASH(13),
    CREDIT(14),
    ;
    private final int code;
    private static final HashMap<Integer, ENUM_RESOURCE_TYPE> CODE_FIELD_TYPE = new HashMap();

    ENUM_RESOURCE_TYPE(int code) {
        this.code = code;
    }

    public byte getCode() {
        return (byte) code;
    }

    public static ENUM_RESOURCE_TYPE get(int code) {
        return CODE_FIELD_TYPE.get(code);
    }
    
    public static byte[] getKeys(){        
        ENUM_RESOURCE_TYPE[] temp = ENUM_RESOURCE_TYPE.values();
        byte[] rs = new byte[temp.length];
        for(int i=0;i<temp.length; i++){
            rs[i] = temp[i].getCode();
        }
        return rs;
    }
    public static String[] getStringKeys(){        
        ENUM_RESOURCE_TYPE[] temp = ENUM_RESOURCE_TYPE.values();
        String[] rs = new String[temp.length];
        for(int i=0;i<temp.length; i++){
            rs[i] = temp[i].toString();
        }
        return rs;
    }

    static {
        for (ENUM_RESOURCE_TYPE type : ENUM_RESOURCE_TYPE.values()) {
            CODE_FIELD_TYPE.put(type.code, type);
        }
    }
}
