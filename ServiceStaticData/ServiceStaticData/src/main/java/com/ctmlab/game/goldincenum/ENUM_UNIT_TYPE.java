/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ctmlab.game.goldincenum;

import java.util.HashMap;

/**
 *
 * @author hhtri
 */
public enum ENUM_UNIT_TYPE {
    
    NULL(0),
    HOME_BASE(1),
    MINE(2),
    OIL_MINE(3),
    TRADE_CENTER(4),    
    EXPLRATION(5),
    TRANSPORTATION(6),
    
    HOME_OFFICE(10),
    HOME_REFINELY(11),
    HOME_BARRACK(12),
    HOME_DEFENSE(13),
    HOME_WEAPON_DEPOT(14),
    HOME_EXPLORATION_DEPOT(15),
    HOME_TRANSPORTATION_DEPOT(16),
    HOME_WALL(17),
    
    CLAIM_EXCAVATOR(20),
    CLAIM_DUMPTRUCK(21),
    CLAIM_WASH_PLANT(22),
    CLAIM_OIL_RIG(23),
    CLAIM_WORKER(24),
    ;
    
    private final int code;
    private static final HashMap<Integer, ENUM_UNIT_TYPE> CODE_FIELD_TYPE = new HashMap();

    ENUM_UNIT_TYPE(int code) {
        this.code = code;
    }

    public int getCode(){return code;}
    
    public static ENUM_UNIT_TYPE get(int code) {
        return CODE_FIELD_TYPE.get(code);
    }

    static {
        for (ENUM_UNIT_TYPE type : ENUM_UNIT_TYPE.values()) {
            CODE_FIELD_TYPE.put(type.code, type);
        }
    }
}
