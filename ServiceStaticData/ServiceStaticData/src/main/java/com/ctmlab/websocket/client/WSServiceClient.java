/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ctmlab.websocket.client;

import com.ctmlab.game.data.ctmon.CTMON;
import com.ctmlab.game.data.staticdata.Region;
import com.ctmlab.game.goldincenum.GI_MICROSERVICE_CMD;
import com.ctmlab.manager.DownLoadServiceMng;
import com.ctmlab.manager.WSGameDefine;
import com.ctmlab.util.Utils;
import com.ctmlab.websocket.manager.WSServiceServer;
import com.google.common.io.LittleEndianDataOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.Date;
import java.util.LinkedList;
import java.util.Queue;
import javax.websocket.SendHandler;
import javax.websocket.SendResult;
import javax.websocket.Session;

/**
 *
 * @author hhtri
 */
public class WSServiceClient implements SendHandler {
    
    // <editor-fold defaultstate="collapsed" desc="Variances in class">    
    
    // LOCK to synchronize
    private final Object SENDING_LOCK = new Object();
    // flag uses for synchronizing    
    private boolean mSending = false;
    // last time connect. Using for check timeout session
    private long mLastPing = 0;
    // keeping session when has a connection 
    private Session mSession = null;
    // id return after add worker to manager (WSServiceMng)
    private int mId = -1;
    // buffer data (bytebuffer) to support for sending data to client 
    private final Queue<byte[]> mDataQueue = new LinkedList(); 
    // id user signin (success)
    private String userID = "";
    
    // log name
    private String mLogName = this.getClass().getSimpleName().toLowerCase();
    // stringbuilder using for debug log
    private final StringBuilder mLogBuilder = new StringBuilder();
    
    
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="Public Methods">
    
    
    public void init(Session _session){        
        mSession = _session;
        WSServiceServer.i().addClient(this);
        mLastPing = System.currentTimeMillis();
    }    
    
    public void notifySend() {       
        ByteBuffer buffer;
        synchronized (mDataQueue) {
            if (mDataQueue.isEmpty()) {
                return;
            }
            synchronized (SENDING_LOCK) {
                if (mSending) {
                    return;
                }
                mSending = true;
            }
            buffer = ByteBuffer.wrap(mDataQueue.poll());
        }
        if (mSession.isOpen()) {
            try {
                synchronized (buffer) {
                    buffer.rewind();
                    mSession.getAsyncRemote().sendBinary(buffer, this);
                }
            } catch (java.lang.IllegalStateException ex) {
                try {mSession.close();}catch(IOException ex_close){}
            }
        }
    }
    
    private long mTestTime = System.currentTimeMillis() + 30*60*1000;
    // method send: 
    public void send(byte[] buffer) {     
        synchronized (mDataQueue) {
            mDataQueue.add(buffer);
        }
        notifySend();
    }

    // method disconnect: close connection and remove from manager RAM.
    // Just manager call this methods
    public void disconnect() {
        if (mSession != null) {
            try {
                mSession.close();
            } catch (IOException ex) {
            }
        }
    }
    
    // <editor-fold defaultstate="collapsed" desc="Recv Methods">
    public void onMessage(ByteBuffer data) throws IOException {
        GI_MICROSERVICE_CMD CMD = GI_MICROSERVICE_CMD.fromID((int)data.getShort());        
        mLogBuilder.append("Time: ")
                .append(WSGameDefine.sSimpleTimeDay.format(new Date(System.currentTimeMillis())))
                .append("\nCMD: ").append(CMD.toString())
                .append("\nData remain: ").append(data.remaining());

        long time = System.currentTimeMillis();
        switch (CMD) {
            case RES_PING:{
                break;
            }
            case REQ_CONNECT:{
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                try(LittleEndianDataOutputStream os = new LittleEndianDataOutputStream(baos)){
                    os.writeShort(GI_MICROSERVICE_CMD.RES_CONNECT.getCode());
                    Utils.i().writeBigString(os, DownLoadServiceMng.i().hashVersion.get("worldmap"));
                    Utils.i().writeBigString(os, DownLoadServiceMng.i().hashVersion.get("gameobjectinfo"));
                } catch(Exception ex){}
                send(baos.toByteArray());
                break;
            }
            case REQ_GAMEOBJECT_INFO:{
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                try(LittleEndianDataOutputStream os = new LittleEndianDataOutputStream(baos)){
                    os.writeShort(GI_MICROSERVICE_CMD.RES_GAMEOBJECT_INFO.getCode());
//                    os.writeInt(DownLoadServiceMng.i().GOIRepo.lenFile);
//                    os.write(DownLoadServiceMng.i().GOIRepo.getBytes());
                    String binPath = WSGameDefine.pathBackup + File.separator + "gameobjectinfo.bin";
                    byte[] dataGO = Utils.i().readFile(binPath);
                    os.writeInt(dataGO.length);
                    os.write(dataGO);
                } catch(Exception ex){}
                send(baos.toByteArray());
                break;
            }
            case REQ_MAP_INFO:{
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                try(LittleEndianDataOutputStream os = new LittleEndianDataOutputStream(baos)){                    
                    baos.reset();                    
                    os.writeShort(GI_MICROSERVICE_CMD.RES_MAP_INFO.getCode());
//                    os.writeInt(DownLoadServiceMng.i().worldMapStatic.lenFile);
//                    DownLoadServiceMng.i().worldMapStatic.writeData(os);
                    String binPath = WSGameDefine.pathBackup + File.separator + "map.bin";
                    byte[] dataGO = Utils.i().readFile(binPath);
                    os.writeInt(dataGO.length);
                    os.write(dataGO);
                } catch(Exception ex){}
                send(baos.toByteArray());
                break;
            }
            case REQ_MAP_INFO_EXTRA:{
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                try(LittleEndianDataOutputStream os = new LittleEndianDataOutputStream(baos)){                    
                    CTMON.i().toStream(os, DownLoadServiceMng.i().worldMap);                    
                    byte[] dataTemp = baos.toByteArray();
                    baos.reset();                    
                    os.writeShort(GI_MICROSERVICE_CMD.RES_MAP_INFO.getCode());
                    os.writeInt(dataTemp.length);
                    os.write(dataTemp);
                } catch(Exception ex){}
                send(baos.toByteArray());
                break;
            }
            case REQ_REGION_DATA:{
                short id_r = data.getShort();
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                try(LittleEndianDataOutputStream os = new LittleEndianDataOutputStream(baos)){                         
//                    Region r = DownLoadServiceMng.i().worldMap.getRegion(id_r);               
//                    os.writeShort(GI_MICROSERVICE_CMD.RES_REGION_DATA.getCode());
//                    os.writeInt(r.lenFile);
//                    r.writeData(os);
                    os.writeShort(GI_MICROSERVICE_CMD.RES_REGION_DATA.getCode());
                    String binPath = WSGameDefine.pathBackup + File.separator + id_r + ".bin";
                    byte[] dataGO = Utils.i().readFile(binPath);
                    os.writeInt(dataGO.length);
                    os.write(dataGO);
                } catch(Exception ex){}
                send(baos.toByteArray());
                break;
            }
            default:{
                data.rewind();
//                System.out.println(WSGameDefine.now() + ": " + WSGameDefine.gson.toJson(data.array()));
                break;
            }
        }
        System.out.println(mLogBuilder.toString());
        mLogBuilder.setLength(0);
        mLastPing = time;
    }
    
    public void onMessage(String data) throws IOException {
//        synchronized(mLogBuilder){
//            mLogBuilder.append(Func.now()).append(" - Recv String\n").append(data);
//            debug(mLogBuilder.toString());
//            mLogBuilder.setLength(0);
//        }
    }    
    // </editor-fold>
    
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="Private Methods">
    
    
//    private void debug(String data){
//        LogManager.i().writeLog(mLogName, data);
//    }
    
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="Refactor Methods">
    
    /**
     * @return the lastPing
     */
    public long getLastPing() {
        return mLastPing;
    }

    /**
     * @param lastPing the lastPing to set
     */
    public void setLastPing(long lastPing) {
        this.mLastPing = lastPing;
    }

    /**
     * @return the session
     */
    public Session getSession() {
        return mSession;
    }

    /**
     * @param session the session to set
     */
    public void setSession(Session session) {
        this.mSession = session;
    }

    /**
     * @return the id
     */
    public int getId() {
        return mId;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.mId = id;
    }
    
    public String getUserID() {
        return userID;
    }
    
    // </editor-fold>      
    
    // <editor-fold defaultstate="collapsed" desc="Override Methods">
    
    /**
    * Method insure that the data is sent completely to client-side
    * @author hhtri
    * @param result
    */
    @Override
    public void onResult(SendResult result) {
        synchronized (SENDING_LOCK) {
            mSending = false;
        }
        if (result.isOK()) {
        } else {
            Throwable ex = result.getException();
            if (ex != null) {
//                debug(Func.toString(ex));
            }
        }
        notifySend();
    }
    
    // </editor-fold>
    
}