/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ctmlab.websocket.client.frames;

import java.nio.ByteBuffer;
import java.util.ArrayList;

/**
 *
 * @author hhtri
 */
public class MicroServiceFrames {
    
    private int len;
    private final ArrayList<byte[]> lstFrameItem; 
    
    private FrameItem curFrame = null;
    private MicroServiceListener listener = null;
    
    public MicroServiceFrames(){
        len = 0;
        lstFrameItem = new ArrayList<>(512);
    }
    
    public void setListener(MicroServiceListener l){
        listener = l;
    }
    
    public void receiverData(ByteBuffer buff){
        if(len==0){
            len = buff.getInt();
            curFrame = new FrameItem();
        }
        while(buff.remaining()>0){
            boolean flag = curFrame.readData(buff);
            if(flag){
                lstFrameItem.add(curFrame.getData());
                curFrame.reset();
            }
        }        
        if(len==lstFrameItem.size()){
            listener.receiver(lstFrameItem);
            lstFrameItem.clear();
            len = 0;
        }
    }
}
