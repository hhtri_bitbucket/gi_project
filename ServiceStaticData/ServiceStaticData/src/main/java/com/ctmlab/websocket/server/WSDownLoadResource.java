/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ctmlab.websocket.server;

import com.ctmlab.manager.WSGameDefine;
import com.ctmlab.websocket.client.WSServiceClient;
import com.ctmlab.websocket.manager.WSServiceServer;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import javax.websocket.OnClose;
import javax.websocket.OnError;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;

/**
 *
 * @author hhtri
 */
@ServerEndpoint("/downloads")
public class WSDownLoadResource {
    private final String logName = "microservice";
    private final StringBuilder logData = new StringBuilder();
    private WSServiceClient client = null;
    
    private void debug(String log){
        System.out.println(log);
    }
    
    @OnOpen
    public void onOpen(Session peer) {        
        client = new WSServiceClient();
        client.init(peer);
        
        logData.append(WSGameDefine.now()).append("\n")
                    .append("[MicroSerive WebSocket] - ")
                    .append(client.getId())
                    .append(" - Opened (Playing): (").append(peer.getId())
                    .append(")");
            debug(logData.toString());
        logData.setLength(0);
    }

    @OnClose
    public void onClose(Session peer) {
        if (client != null) {        
            
            WSServiceServer.i().rmClient(client);  
            
            logData.append(WSGameDefine.now()).append("\n")
                    .append(client.getId())
                    .append("[MicroSerive WebSocket] - ")
                    .append(" - Closed (Playing): (").append(peer.getId())
                    .append(")");
            debug(logData.toString());
            logData.setLength(0);
            
            client = null;
        }
    }

    @OnError
    public void onError(Throwable t) {        
        logData.append(WSGameDefine.now()).append("\n")
                .append(client.getId()).append(" - onError\n")
                .append(WSGameDefine.toString(t));
        debug(logData.toString());
        logData.setLength(0);
    }

    @OnMessage
    public void onMessage(ByteBuffer data) {
        if (client != null) {
            data.order(ByteOrder.LITTLE_ENDIAN);
            try {
                client.onMessage(data);
            } catch (IOException ex) {
                logData.append(WSGameDefine.now()).append("\n")
                    .append(client.getId())
                    .append(" - onMessage by ByteBuffer has Exception\n")
                    .append(WSGameDefine.toString(ex));
                debug(logData.toString());
                logData.setLength(0);
            }
        }
    }

    @OnMessage
    public void onMessage(String message) {
        if (client != null) {
            try {
                client.onMessage(message);
            } catch (IOException ex) {
                logData.append(WSGameDefine.now())
                    .append("\n")
                    .append(client.getId())
                    .append(" - onMessage by String has Exception\n")
                    .append(WSGameDefine.toString(ex));
                debug(logData.toString());
                logData.setLength(0);
            }
        }
    }
}
