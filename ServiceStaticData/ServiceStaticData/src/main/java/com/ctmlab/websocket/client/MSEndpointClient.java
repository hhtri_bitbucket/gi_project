/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ctmlab.websocket.client;

import com.ctmlab.websocket.client.frames.MicroServiceFrames;
import com.ctmlab.websocket.client.frames.MicroServiceListener;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.URI;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Queue;
import javax.websocket.ClientEndpoint;
import javax.websocket.CloseReason;
import javax.websocket.ContainerProvider;
import javax.websocket.OnClose;
import javax.websocket.OnError;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.SendHandler;
import javax.websocket.SendResult;
import javax.websocket.Session;
import javax.websocket.WebSocketContainer;

/**
 *
 * @author hhtri
 */
@ClientEndpoint
public class MSEndpointClient implements SendHandler {

    private static final Object LOCK = new Object();
    private final Queue<byte[]> data_queue = new LinkedList();
    private final Object SENDING_LOCK = new Object();
    private boolean sending = false;
    private URI endpointURI=null;
    private boolean isConnect = false;    
    private Session session = null;
    private MSClientListener listener = null;
    private MicroServiceFrames frames = null;    
    private int bufferSize = 8192;
    
    public MSEndpointClient(URI _endpointURI){
        endpointURI = _endpointURI;
    }
    
    public void sendMessage(byte[] message) {
        addToBuffer(message);
    }
    
    public void addListener(MSClientListener l){
        listener = l;
    }
    
    private void addToBuffer(byte[] data) {
        if (session == null) {
            return;
        }
        synchronized (data_queue) {
            data_queue.add(data);
        }
        notifySend();
    }
    
    private void receiverServer(ArrayList<byte[]> data){
        if(listener!=null){
            for(byte[] b:data){
                ByteBuffer buff = ByteBuffer.wrap(b);
                buff.order(ByteOrder.LITTLE_ENDIAN);
                buff.rewind();
                listener.handleOnMessage(buff);
            }
        }
    }
    
    public void connect() {
        try {
            WebSocketContainer container = ContainerProvider.getWebSocketContainer();
            container.connectToServer(this, endpointURI);                        
            frames = new MicroServiceFrames();
            frames.setListener(new MicroServiceListener() {
                @Override
                public void receiver(ArrayList<byte[]> data) {
                    receiverServer(data);
                }
            });
            
            isConnect = true;
        } catch (Exception ex) {
            session = null;
            isConnect = false;
        }
    }
    
    public boolean isConnected(){return isConnect;}

    public void disconnect() {
        try {
            if (session != null) {
                session.close();
            }
        } catch (IOException ex) {
        }        
    }
    
    public void notifySend() {       
        ByteBuffer buffer;
        synchronized (data_queue) {
            if (data_queue.isEmpty()) {
                return;
            }
            synchronized (SENDING_LOCK) {
                if (sending) {
                    return;
                }
                sending = true;
            }
            buffer = ByteBuffer.wrap(data_queue.poll());
        }
        if (session.isOpen()) {
            try {
                synchronized (buffer) {
                    buffer.rewind();
                    buffer.order(ByteOrder.LITTLE_ENDIAN);
                    session.getAsyncRemote().sendBinary(buffer, this);
                }
            } catch (java.lang.IllegalStateException ex) {
                try {session.close();}catch(IOException ex_close){}
            }
        }
    }

    @OnOpen
    public void onOpen(Session userSession) {
        session = userSession;
        if(listener!=null){
            listener.handleOnOpen(bufferSize);
        }
    }

    @OnClose
    public void onClose(Session userSession, CloseReason reason) {
       if(listener!=null){
            listener.handleOnClosed();
        }
        isConnect = false;
        session = null;
    }
    
    @OnError
    public void onError(Throwable t) {
        StringWriter w = new StringWriter();
        t.printStackTrace(new PrintWriter(w));
        
        isConnect = false;
        session = null;
    }

    
    @OnMessage
    public void onMessage(ByteBuffer buff) {
        buff.order(ByteOrder.LITTLE_ENDIAN);
        buff.rewind();        
        frames.receiverData(buff);
    }       

    @Override
    public void onResult(SendResult result) {
        synchronized (SENDING_LOCK) {sending = false;}
        if (result.isOK()) {
            notifySend();           
        } else{
            disconnect();
        }
    }
    
}
