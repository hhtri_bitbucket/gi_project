/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ctmlab.websocket.client;

import com.ctmlab.game.goldincenum.GI_MICRO_SEVICE_CMD;
import com.ctmlab.manager.DownLoadServiceMng;
import com.ctmlab.manager.WSGameDefine;
import com.ctmlab.util.Utils;
import com.google.common.io.LittleEndianDataInputStream;
import com.google.common.io.LittleEndianDataOutputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.HashMap;

/**
 *
 * @author hhtri
 */
public class MSClient extends Thread {

    private static final Object LOCK = new Object();
    private boolean mRunning = false;
    private MSEndpointClient endpoint = null;
    private boolean mConnected = false;
    private int id = -1;
    private int portWS = WSGameDefine.PORT_SERVER;
    private String url = "";
    private ByteArrayOutputStream mArrOutput = new ByteArrayOutputStream();
    private LittleEndianDataOutputStream mOutput = new LittleEndianDataOutputStream(mArrOutput);

    public static MSClient _inst = null;

    public MSClient() {
//        if(portWS==8084){
//            url = "ws://localhost:8084/goldinc/microservice";
//        } else if(portWS==8080){
//            url = "ws://localhost:8080/goldinc/microservice";
//        } else{
//            url = "ws://localhost:8080/goldinc/microservice";
//        }
        url = WSGameDefine.WS;
    }

    public static MSClient i() {
        synchronized (LOCK) {
            if (_inst == null) {
                _inst = new MSClient();
            }
            return _inst;
        }
    }
    

    public void startWS() {
        connect();
        start();
    }

    public void stopWS() {
        mRunning = false;
        interrupt();
    }

    public void connect() {
        if (!mConnected) {
            endpoint = new MSEndpointClient(URI.create(url));
            //        endpoint = new WSClientEndpoint(URI.create("ws://localhost:8084/goldinc/giservice"));
            //        endpoint = new WSClientEndpoint(URI.create("ws://test.ctmlab.com/goldinc/giservice"));
            endpoint.addListener(new MSClientListener() {
                @Override
                public void handleOnOpen(int bufferSize) {
                    mConnected = true;
                    reqConnect(bufferSize);
                }

                @Override
                public void handleOnMessage(ByteBuffer data) {
                    try {
                        processData(data);
                    } catch (IOException ex) {
                    }
                }

                @Override
                public void handleOnClosed() {
                    mConnected = false;
                }
            });
            endpoint.connect();
        }
    }

    public void disconnect() {
        if (mConnected && endpoint != null) {
            endpoint.disconnect();
        }
        mArrOutput = null;
        mOutput = null;
    }

    public void ping() throws IOException {
        if((System.currentTimeMillis()-lastTime)>30000){
            synchronized (LOCK) {
                mArrOutput.reset();
                try {
                    mOutput.writeShort(GI_MICRO_SEVICE_CMD.REQ_PING.getCode());
                } catch (IOException ex) {
                }
                if (mConnected && endpoint != null) {
                    endpoint.sendMessage(mArrOutput.toByteArray());
                }
            }
        }
    }

    private void reqConnect(int bufferSize) {
        synchronized (LOCK) {
            mArrOutput.reset();
            try {
                mOutput.writeShort(GI_MICRO_SEVICE_CMD.REQ_CONNECT.getCode());         
                mOutput.writeInt(bufferSize);
            } catch (IOException ex) {
            }
            if (mConnected && endpoint != null) {
                endpoint.sendMessage(mArrOutput.toByteArray());
            }
        }
    }
    
    private void reqListStaticFiles() {
        synchronized (LOCK) {
            mArrOutput.reset();
            try {
                mOutput.writeShort(GI_MICRO_SEVICE_CMD.REQ_LIST_STATIC_DATA.getCode());
                Utils.i().writeBigString(mOutput, WSGameDefine.gson.toJson(DownLoadServiceMng.i().hashVersion));
            } catch (IOException ex) {
            }
            if (mConnected && endpoint != null) {
                endpoint.sendMessage(mArrOutput.toByteArray());
            }
        }
    }
    
    private void reqDownloadFiles(ArrayList<String> lstFile){
        synchronized (LOCK) {
            mArrOutput.reset();
            try {
                mOutput.writeShort(GI_MICRO_SEVICE_CMD.REQ_GET_STATIC_DATA.getCode());
                mOutput.writeInt(lstFile.size());
                for(String nf:lstFile){
                    Utils.i().writeBigString(mOutput, nf);
                }
            } catch (IOException ex) {
            }
            if (mConnected && endpoint != null) {
                endpoint.sendMessage(mArrOutput.toByteArray());
            }
        }
    }
    private long lastTime = System.currentTimeMillis();
    private void processData(ByteBuffer data) throws IOException {
        lastTime = System.currentTimeMillis();
        byte[] a = new byte[data.remaining()];
        data.get(a);
        try (LittleEndianDataInputStream is = new LittleEndianDataInputStream(new ByteArrayInputStream(a))) {
            GI_MICRO_SEVICE_CMD cmd = GI_MICRO_SEVICE_CMD.fromID(is.readShort());
            System.out.println(cmd.toString());
            switch (cmd) {
                case RES_CONNECT: {
                    id = is.readInt();
                    reqListStaticFiles();
                    break;
                }
                case RES_LIST_STATIC_DATA: {
                    if(is.readBoolean()){
                        String json = Utils.i().readBigString(is);
                        DownLoadServiceMng.i().hashVersion = WSGameDefine.gson.fromJson(json, HashMap.class);
                        int len = is.readInt();                    
                        int i = 0;
                        ArrayList<String> lstFile = new ArrayList<>();
                        while(i++<len){
                            String filename = Utils.i().readBigString(is);
                            lstFile.add(filename);
                        }
                        if(!lstFile.isEmpty()){
                            reqDownloadFiles(lstFile);
                        }
                    } else{
                        DownLoadServiceMng.i().readResource();
                    }
                    
                    break;
                }
                case RES_GET_STATIC_DATA: {
                    String preFix = WSGameDefine.pathBackup + File.separator;
                    String path = preFix + Utils.i().readBigString(is);
                    byte[] temp = new byte[is.readInt()];
                    is.read(temp);
                    Utils.i().writeFile(path, temp);
                    break;
                }
                case RES_UPDATE_STATIC_DATA: {
                    int len = is.readInt();                    
                    int i = 0;
                    ArrayList<String> lstFile = new ArrayList<>();
                    while(i++<len){
                        String filename = Utils.i().readBigString(is);
                        lstFile.add(filename);
                    }
                    reqDownloadFiles(lstFile);
                    break;
                }
                case RES_FINISH_SYNC_DATA: {
//                    DownLoadServiceMng.i().mapConvertToStream();
                    DownLoadServiceMng.i().readResource();
                    break;
                }
                case RES_PING: {
                    break;
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public void run() {
        mRunning = true;
        int count = 0;
        while (mRunning) {
            try {
                Thread.sleep(1000);
                count++;
                if (count == 3) {
                    count = 0;
                    try {
                        ping();
                        connect();
                    } catch (IOException ex) {

                    }
                }
            } catch (InterruptedException ex) {
            }
        }
        disconnect();
    }
}
