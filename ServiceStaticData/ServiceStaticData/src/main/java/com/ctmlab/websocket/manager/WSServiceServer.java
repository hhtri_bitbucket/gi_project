/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ctmlab.websocket.manager;


import com.ctmlab.manager.WSGameDefine;
import com.ctmlab.websocket.client.WSServiceClient;
import java.io.File;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.HashMap;

/**
 *
 * @author hhtri
 */
public class WSServiceServer {
    
    // <editor-fold defaultstate="collapsed" desc="Variances in class">    
    
    private final AtomicInteger aClientID = new AtomicInteger(1); 
    private static final Object LOCK = new Object(); 
//    private final String mLogName = this.getClass().getSimpleName().toLowerCase();
    
    public static WSServiceServer _inst = null;  
    public final HashMap<Integer, WSServiceClient> iClients = new HashMap();
    
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="Singleton Methods">
    
    private WSServiceServer() {}
    
    public static WSServiceServer i(){synchronized(LOCK){if(_inst==null){_inst = new WSServiceServer();}return _inst;}}

    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="Private Methods">
       
    
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="Public Methods">
    
    public WSServiceClient[] getListClient() {
        WSServiceClient[] temp;
        synchronized(iClients){temp = iClients.values().toArray(WSGameDefine.EMPTY_WORKER);}
        return temp;
    }
    
    public void addClient(WSServiceClient client){
        if(client!=null){
            int id = aClientID.getAndIncrement();
            client.setId(id);
            synchronized(iClients){iClients.put(id, client);}
        }
    }
    
    public void rmClient(int id){
        synchronized(iClients){iClients.remove(id);}
    }  
    
    public void rmClient(WSServiceClient client){
        if(client!=null){
            rmClient(client.getId());
        }
    }    
    
    public void disconnect(int id){
        WSServiceClient client = iClients.get(id);
        if(client!=null){
            client.disconnect();
        }
    }
    
    public void sendAllClient(byte[] data){
        for(WSServiceClient c:getListClient()){
            c.send(data);
        }
    }
        
    // </editor-fold>
    
}
