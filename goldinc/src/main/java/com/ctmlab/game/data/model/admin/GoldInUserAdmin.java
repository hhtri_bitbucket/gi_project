/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ctmlab.game.data.model.admin;

import com.dp.db.DBDataPostgres;
import com.dp.db.DataFieldType;

/**
 *
 * @author hhtri
 */
public class GoldInUserAdmin extends DBDataPostgres{
    @DataFieldType(primaryKey = true)
    public String username;
    public String password;
    public long created;
    public long lastLogin;
    public String createdBy;
    public GoldInUserAdmin(){}
}
