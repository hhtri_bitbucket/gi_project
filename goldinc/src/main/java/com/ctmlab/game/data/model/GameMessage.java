/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ctmlab.game.data.model;

import com.ctmlab.game.data.model.base.GameMessageBase;
import com.ctmlab.game.network.manager.WSGameDefine;
import com.google.common.io.LittleEndianDataOutputStream;
import java.io.IOException;

/**
 *
 * @author hhtri
 */
public class GameMessage extends GameMessageBase{
    
    public byte[] exData = WSGameDefine.EMPTY_BYTES;
        
    public GameMessage(){}
    
    public GameMessage(String sender, String receiver, 
            String sub, String content, byte type){
        super(sender, receiver, sub, content, type);
        exData = WSGameDefine.EMPTY_BYTES;
    }
       
    @Override
    public void writeData(LittleEndianDataOutputStream os) throws IOException{
        super.writeData(os);
    }
    
    public void saveInvitationData(){}
    
    public void saveJoinAllianceData(){}
    
    public void addMineIndexInfo(short id_z, long mine){
    }
    
}
