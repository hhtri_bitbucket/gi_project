/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ctmlab.game.data.staticdata.gameobjectinfo;

import com.ctmlab.game.goldincenum.ENUM_DECOR_MAP_TYPE;
import com.ctmlab.game.goldincenum.ENUM_UNIT_TYPE;
import com.ctmlab.game.manager.GlobalConfig;
import com.ctmlab.game.manager.StaticDataMng;
import com.ctmlab.util.Utils;
import com.dp.db.Func;
import com.google.common.io.LittleEndianDataInputStream;
import com.google.common.io.LittleEndianDataOutputStream;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSyntaxException;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.TreeMap;

/**
 *
 * @author hhtri
 */
public class GameObjectInfoRepo {

    public String version;
    public TreeMap<Byte, GameObjectInfo> lstObjectMap;
    public TreeMap<Byte, GameObjectInfo> lstObjectDecor;

    public GameObjectInfoRepo() {
        version = "";
        lstObjectMap = new TreeMap<>();
        lstObjectDecor = new TreeMap<>();
    }

    public GameObjectInfo getDecorInfo(byte t) {
        return lstObjectDecor.get(t);
    }

    public GameObjectInfo getGameOjectInfo(byte t) {
        return lstObjectMap.get(t);
    }

    public void init(String v) {
        try {
            String json = Utils.i().readTextFile(GlobalConfig.pathConfig + File.separator + "gameobjectinfo.json");
            JsonObject objAll = Func.gson.fromJson(json, JsonObject.class);
            if (objAll.has("version") && objAll.has("lstObjectMap") && objAll.has("lstObjectDecor")) {
                version = v;
                JsonObject obj = objAll.get("lstObjectMap").getAsJsonObject();
                for (Entry<String, JsonElement> e : obj.entrySet()) {
                    byte key_type = Byte.valueOf(e.getKey());
                    JsonObject temp = e.getValue().getAsJsonObject();
                    GameObjectInfo clazz = new GameObjectInfo();
                    clazz.readData(temp);
                    lstObjectMap.put(key_type, clazz);
                }
                // barrack
                json = Utils.i().readTextFile(GlobalConfig.pathConfig + File.separator + "barracks.json");
                lstObjectMap.put(ENUM_UNIT_TYPE.HOME_BARRACK.getCode(), GameObjectInfo.initFromJson(ENUM_UNIT_TYPE.HOME_BARRACK, json));
                // defense
                json = Utils.i().readTextFile(GlobalConfig.pathConfig + File.separator + "defense.json");
                lstObjectMap.put(ENUM_UNIT_TYPE.HOME_DEFENSE.getCode(), GameObjectInfo.initFromJson(ENUM_UNIT_TYPE.HOME_DEFENSE, json));
                // exploration
                json = Utils.i().readTextFile(GlobalConfig.pathConfig + File.separator + "exploration.json");
                lstObjectMap.put(ENUM_UNIT_TYPE.HOME_EXPLORATION.getCode(), GameObjectInfo.initFromJson(ENUM_UNIT_TYPE.HOME_EXPLORATION, json));
                // office
                json= Utils.i().readTextFile(GlobalConfig.pathConfig + File.separator + "office.json");
                lstObjectMap.put(ENUM_UNIT_TYPE.HOME_OFFICE.getCode(), GameObjectInfo.initFromJson(ENUM_UNIT_TYPE.HOME_OFFICE, json));
                // refinely
                json = Utils.i().readTextFile(GlobalConfig.pathConfig + File.separator + "refinery.json");
                lstObjectMap.put(ENUM_UNIT_TYPE.HOME_REFINERY.getCode(), GameObjectInfo.initFromJson(ENUM_UNIT_TYPE.HOME_REFINERY, json));
                // transportation
                json = Utils.i().readTextFile(GlobalConfig.pathConfig + File.separator + "transportation.json");
                lstObjectMap.put(ENUM_UNIT_TYPE.HOME_TRANSPORTATION.getCode(), GameObjectInfo.initFromJson(ENUM_UNIT_TYPE.HOME_TRANSPORTATION, json));
                // wall
                json = Utils.i().readTextFile(GlobalConfig.pathConfig + File.separator + "protective.json");
                lstObjectMap.put(ENUM_UNIT_TYPE.HOME_WALL.getCode(), GameObjectInfo.initFromJson(ENUM_UNIT_TYPE.HOME_WALL, json));
                // weapon
                json = Utils.i().readTextFile(GlobalConfig.pathConfig + File.separator + "weapon.json");
                lstObjectMap.put(ENUM_UNIT_TYPE.HOME_WEAPON.getCode(), GameObjectInfo.initFromJson(ENUM_UNIT_TYPE.HOME_WEAPON, json));

                obj = objAll.get("lstObjectDecor").getAsJsonObject();
                for (Entry<String, JsonElement> e : obj.entrySet()) {
                    byte key_type = Byte.valueOf(e.getKey());
                    JsonObject temp = e.getValue().getAsJsonObject();
                    GameObjectInfo clazz = new GameObjectInfo();
                    clazz.readData(temp);
                    clazz.mType = key_type;
                    lstObjectDecor.put(key_type, clazz);
                }
            }
            String path = GlobalConfig.pathBackup + File.separator + "gameobjectinfo.bin";
            writeGameObjectInfo(path);
            path = GlobalConfig.pathBackup + File.separator + "gameobjectinfo.json";
            Utils.i().writeTextFile(path, Func.json(this));
        } catch (Exception ex) {
            System.out.println(Func.toString(ex));
        }
    }

    public void readGameObjectInfo(String pathBin) {
        byte[] data = Utils.i().readFile(pathBin);
        ByteArrayInputStream bais = new ByteArrayInputStream(data);
        try (LittleEndianDataInputStream is = new LittleEndianDataInputStream(bais)) {
            version = Utils.i().readBigString(is);
            int len = is.readInt();
            int i = 0;

            lstObjectMap.clear();
            while (i++ < len) {
                byte key = is.readByte();
                GameObjectInfo clazz = GameObjectInfo.readObjectData(is);
                if (clazz != null) {
                    lstObjectMap.put(key, clazz);
                }
            }
            len = is.readInt();
            i = 0;
            lstObjectDecor.clear();
            while (i++ < len) {
                byte key = is.readByte();
                GameObjectInfo clazz = GameObjectInfo.readDecorData(is);
                if (clazz != null) {
                    lstObjectDecor.put(key, clazz);
                }
            }
        } catch (Exception ex) {
        }
    }

    public void writeGameObjectInfo(String pathBin) {
        version = StaticDataMng.i().setNewVersion("gameobjectinfo");
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        try (LittleEndianDataOutputStream os = new LittleEndianDataOutputStream(baos)) {
            Utils.i().writeBigString(os, version);
            os.writeInt(lstObjectMap.size());
            for (Entry<Byte, GameObjectInfo> e : lstObjectMap.entrySet()) {
                // order in enum ENUM_UNIT_TYPE
                os.writeByte(e.getKey());
                // type extend from GameObjectInfo
                e.getValue().writeData(os);
            }
            os.writeInt(lstObjectDecor.size());
            for (Entry<Byte, GameObjectInfo> e : lstObjectDecor.entrySet()) {
                // order in enum DECOR_MAP_TYPE
                os.writeByte(e.getKey());
                // type extend from GameObjectInfo
                e.getValue().writeData(os);
            }
        } catch (Exception ex) {
        }
        Utils.i().writeBinFile(pathBin, baos.toByteArray());
    }

    public long getSpeedExploration(int level) {

        if (level < 0) {
            return 0;
        }

        byte key = (byte) ENUM_UNIT_TYPE.HOME_EXPLORATION.getCode();
        GameObjectExplorationInfo item = (GameObjectExplorationInfo) lstObjectMap.get(key);
        int idx = level - 1;
        if (idx < item.info.size()) {
            return item.info.get((byte) idx).get("speed");
        }
        return 0;
    }

    public HashMap<Byte, Long> getSpeedTransportation(int level) {
        HashMap<Byte, Long> rs = new HashMap<>();
        if (level < 0) {
            return rs;
        }

        byte key = (byte) ENUM_UNIT_TYPE.HOME_TRANSPORTATION.getCode();

        GameObjectTransportationInfo item = (GameObjectTransportationInfo) lstObjectMap.get(key);
        int idx = level - 1;
        if (idx < item.info.size()) {
            return item.info.get((byte) idx);
        }
        return rs;
    }

    // using for editor
    // object map - building in homebase, claim
    public String getObjectUnit(String type){
        String rs = "";
        byte key = (byte)ENUM_UNIT_TYPE.get(type);
        if(lstObjectMap.containsKey(key)){
            rs = lstObjectMap.get(key).toJson();
        }
        return rs;
    }
    public String setObjectUnit(String type, String json){
        String rs = "";
        byte key = (byte)ENUM_UNIT_TYPE.get(type);
        if(lstObjectMap.containsKey(key)){
            rs = lstObjectMap.get(key).fromJson(json);
        }
        return rs;
    }
    // object map - decor unit (static)
    public String getObjectDecor(String type){
        String rs = "";
        byte key = (byte)ENUM_DECOR_MAP_TYPE.get(type);
        if(lstObjectDecor.containsKey(key)){
            rs = lstObjectDecor.get(key).toJson();
        }
        return rs;
    }
    public String setObjectDecor(String type, String json){
        String rs = "";
        byte key = (byte)ENUM_DECOR_MAP_TYPE.get(type);
        if(lstObjectDecor.containsKey(key)){
            rs = lstObjectDecor.get(key).fromJson(json);
        }
        return rs;
    }
    
    // using for testing

    
    public void apply(byte[] data) {
        ByteArrayInputStream bais = new ByteArrayInputStream(data);
        lstObjectMap.clear();
        try (LittleEndianDataInputStream is = new LittleEndianDataInputStream(bais)) {
            version = Utils.i().readBigString(is);
            int len = is.readInt();
            int i = 0;
            while (i++ < len) {
                byte key = is.readByte();
                GameObjectInfo clazz = GameObjectInfo.readObjectData(is);
                if (clazz != null) {
                    lstObjectMap.put(key, clazz);
                }
            }
            len = is.readInt();
            i = 0;
            while (i++ < len) {
                byte key = is.readByte();
                GameObjectInfo clazz = GameObjectInfo.readDecorData(is);
                if (clazz != null) {
                    lstObjectDecor.put(key, clazz);
                }
            }
        } catch (Exception ex) {
        }
    }

    public byte[] getBytes() {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        try (LittleEndianDataOutputStream os = new LittleEndianDataOutputStream(baos)) {
            Utils.i().writeBigString(os, version);
            os.writeInt(lstObjectMap.size());
            for (Entry<Byte, GameObjectInfo> e : lstObjectMap.entrySet()) {
                os.writeByte(e.getKey());
                e.getValue().writeData(os);
            }
            os.writeInt(lstObjectDecor.size());
            for (Entry<Byte, GameObjectInfo> e : lstObjectDecor.entrySet()) {
                os.writeByte(e.getKey());
                e.getValue().writeData(os);
            }
        } catch (Exception ex) {
        }
        return baos.toByteArray();
    }
}
