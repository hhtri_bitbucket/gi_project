/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ctmlab.game.data.pojo;

import com.ctmlab.game.data.model.SocialChat;

/**
 *
 * @author hhtri
 */
public class DataChat {
    public long time;
    public int mType;
    public long groupId;
    public byte[] data;
    
    public DataChat(){}
    
    public DataChat(SocialChat chatItem){
        this.time = chatItem.time;
        this.mType = chatItem.mType;
        this.groupId = chatItem.groupId;
        this.data = chatItem.data;
    }
}
