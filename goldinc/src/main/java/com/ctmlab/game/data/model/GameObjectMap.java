/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ctmlab.game.data.model;

import com.ctmlab.game.data.model.base.GameObjectBase;
import com.ctmlab.game.data.ctmon.CTMONDataFieldType;
import com.ctmlab.game.data.pojo.MineIndex;
import com.ctmlab.game.data.staticdata.PointInt;
import com.ctmlab.game.data.staticdata.PointShort;
import com.ctmlab.game.data.staticdata.Zone;
import com.ctmlab.game.manager.StaticDataMng;
import com.ctmlab.util.Utils;
import com.dp.db.DB;
import com.dp.db.DataFieldType;
import com.google.common.io.LittleEndianDataInputStream;
import com.google.common.io.LittleEndianDataOutputStream;
import com.google.gson.annotations.ExcluseJson;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author hhtri
 */
public class GameObjectMap extends GameObjectBase{
    
    public String userID = "";
    public byte mType = 0;
    
    @DataFieldType(ignore = true)
    @CTMONDataFieldType(ignore = true)
    @ExcluseJson(scope = ExcluseJson.ALL)
    private MineIndex mIndex = null;
    
    public GameObjectMap(){super();}
    
    public GameObjectMap(byte mType){super();this.mType = mType;}
    
    public MineIndex getIndex(){
        if(mIndex==null){mIndex=new MineIndex(id_r, id_z, id);}
        return mIndex;
    }
    
    public void refClaim(Claim claim){}
    
    public Claim getClaim(){return null;}

    protected void writePathFinding(LittleEndianDataOutputStream os,  ArrayList<PointShort> path) throws IOException{
        if(path!=null){
            os.writeBoolean(true);
            os.writeInt(path.size());
            for(PointShort p:path){
                os.writeShort(p.x);
                os.writeShort(p.y);
            }
        } else{
            os.writeBoolean(false);
        }
    }

    public void setPathFinding(ArrayList<PointShort> path){}

    public void getPathFinding(){}

    @Override
    public void refInfo(){
        if(id==-1){
            // decor
            objInfo = StaticDataMng.i().getRepoGameObjectInfo().lstObjectDecor.get(mType);
        } else{
            // object map
            objInfo = StaticDataMng.i().getRepoGameObjectInfo().lstObjectMap.get(mType);
        }
    }

    @Override
    public long capacityOnRAM(){
        long rs = userID.length() + 1 + super.capacityOnRAM();
        return rs;
    }
    
    public int saveData(){
        try {return this.save(DB.i().con());}catch(SQLException ex){return -2;}        
    }
    
    @Override
    public void writeData(LittleEndianDataOutputStream os) throws IOException{
        super.writeData(os);
        Utils.i().writeBigString(os, userID);
        os.writeByte(mType);
    }
    
    @Override
    public void readData(LittleEndianDataInputStream is) throws IOException{
        super.readData(is);
        userID = Utils.i().readBigString(is);
        mType = is.readByte();
    }
    
    @Override
    public void writeDecorData(LittleEndianDataOutputStream os) throws IOException {
        super.writeDecorData(os);
        os.writeByte(mType);
    }
    
    @Override
    public void readDecorData(LittleEndianDataInputStream is) throws IOException {
        super.readDecorData(is);
        mType = is.readByte();
    }
    
    public ArrayList<Integer> getRealLocation(){
        Zone z = StaticDataMng.i().worldMap.getZone(id_z);
        if (z == null) {
            return null;
        }
        ArrayList<Integer> rs = new ArrayList<>();
        rs.add(z.pos.get(0) + xPos);
        rs.add(z.pos.get(1) + yPos);
        return rs;
    }
}
