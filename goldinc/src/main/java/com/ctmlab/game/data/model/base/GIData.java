/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ctmlab.game.data.model.base;

import com.dp.db.DBDataPostgres;
import com.dp.db.DataFieldType;
import com.google.common.io.LittleEndianDataInputStream;
import java.io.IOException;
import java.util.Date;

/**
 *
 * @author hhtri
 */
public class GIData extends DBDataPostgres {

    @DataFieldType(primaryKey = true, size = 128)
    public String userID; 
    
    public java.util.Date dateServer;
    public long timeClient = 0;

    public GIData() {
        this.dateServer = new Date(System.currentTimeMillis());
    }

    public void load(LittleEndianDataInputStream is) throws IOException {
        timeClient = is.readLong();
    }
}
