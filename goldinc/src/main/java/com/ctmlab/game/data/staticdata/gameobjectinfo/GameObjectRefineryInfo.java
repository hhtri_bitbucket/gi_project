/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ctmlab.game.data.staticdata.gameobjectinfo;

import com.ctmlab.game.goldincenum.ENUM_UNIT_PROPERTY;
import com.ctmlab.game.goldincenum.ENUM_UNIT_TYPE;
import com.ctmlab.util.Utils;
import com.dp.db.Func;
import com.google.common.io.LittleEndianDataInputStream;
import com.google.common.io.LittleEndianDataOutputStream;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSyntaxException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author hhtri
 */
public class GameObjectRefineryInfo extends GameObjectInfo {

    public String version;
    public String name;
    public int maxLevel;
    public ArrayList<HashMap<Byte, Long>> info;
    public ArrayList<UpgradeInfo> upgrade_require;

    public GameObjectRefineryInfo() {
        super();
        mType = (byte) ENUM_UNIT_TYPE.HOME_REFINERY.getCode();
    }

    @Override
    public int getMaxLevel() {
        return maxLevel;
    }

    @Override
    public void initByJson(String json) {
        JsonObject obj = Func.gson.fromJson(json, JsonObject.class);
        version = obj.get("version").getAsString();
        name = obj.get("name").getAsString();
        maxLevel = obj.get("maxLevel").getAsInt();
        width = obj.get("width").getAsShort();
        height = obj.get("height").getAsShort();
        info = new ArrayList<>();
        JsonArray arr = obj.get("info").getAsJsonArray();
        for (int i = 0; i < arr.size(); i++) {
            HashMap<Byte, Long> hash = new HashMap<>();
            JsonObject temp = arr.get(i).getAsJsonObject();
            for (Map.Entry<String, JsonElement> e : temp.entrySet()) {
                hash.put(Byte.parseByte(e.getKey()), e.getValue().getAsLong());
            }
            info.add(hash);
        }
        upgrade_require = new ArrayList();
        arr = obj.get("upgrade_require").getAsJsonArray();
        for (int i = 0; i < arr.size(); i++) {
            JsonObject temp = arr.get(i).getAsJsonObject();
            upgrade_require.add(UpgradeInfo.parse(temp));
        }
    }

    // admin page
    @Override
    public String fromJson(String json) {
        try {
            JsonObject obj = Func.gson.fromJson(json, JsonObject.class);
            version = obj.get("version").getAsString();
            name = obj.get("name").getAsString();
            maxLevel = obj.get("maxLevel").getAsInt();
            width = obj.get("width").getAsShort();
            height = obj.get("height").getAsShort();
            info.clear();
            JsonArray arr = obj.get("info").getAsJsonArray();
            for (int i = 0; i < arr.size(); i++) {
                HashMap<Byte, Long> hash = new HashMap<>();
                JsonObject temp = arr.get(i).getAsJsonObject();
                for (Map.Entry<String, JsonElement> e : temp.entrySet()) {
                    hash.put(Byte.parseByte(e.getKey()), e.getValue().getAsLong());
                }
                info.add(hash);
            }
            upgrade_require.clear();
            arr = obj.get("upgrade_require").getAsJsonArray();
            for (int i = 0; i < arr.size(); i++) {
                JsonObject temp = arr.get(i).getAsJsonObject();
                upgrade_require.add(UpgradeInfo.parse(temp));
            }
        } catch (JsonSyntaxException | NumberFormatException ex) {
            return ex.getMessage();
        }
        return "";
    }

    // admin page
    @Override
    public String toJson() {
        String json = "{}";

        JsonObject obj = new JsonObject();
        JsonArray aInfo = new JsonArray();
        JsonArray aUpgrade = new JsonArray();
        obj.addProperty("name", name);
        obj.addProperty("maxLevel", maxLevel);
        obj.addProperty("width", width);
        obj.addProperty("height", height);
        obj.add("info", aInfo);
        obj.add("upgrade_require", aUpgrade);

        for (HashMap<Byte, Long> e : info) {
            JsonObject temp = new JsonObject();
            for (Map.Entry<Byte, Long> e1 : e.entrySet()) {
                String key = ENUM_UNIT_PROPERTY.getString(e1.getKey());
                temp.addProperty(key, e1.getValue());
            }
            aInfo.add(temp);
        }

        for (UpgradeInfo e : upgrade_require) {
            aUpgrade.add(e.getJsonObject());
        }
        json = Func.json(obj);
        return json;
    }

    @Override
    public void writeData(LittleEndianDataOutputStream os) throws IOException {
        super.writeData(os);
        Utils.i().writeBigString(os, version);
        Utils.i().writeBigString(os, name);
        os.writeInt(maxLevel);
        os.writeInt(info.size());
        for (HashMap<Byte, Long> h : info) {
            os.writeInt(h.size());
            for (Map.Entry<Byte, Long> e : h.entrySet()) {
                os.writeByte(e.getKey());
                os.writeLong(e.getValue());
            }
        }
        os.writeInt(upgrade_require.size());
        for (UpgradeInfo ui : upgrade_require) {
            ui.writeData(os);
        }
    }

    @Override
    public void readData(LittleEndianDataInputStream is) throws IOException {
        super.readData(is);
        version = Utils.i().readBigString(is);
        name = Utils.i().readBigString(is);
        maxLevel = is.readInt();
        int len = is.readInt();
        int i = 0;
        info = new ArrayList<>();
        while (i++ < len) {
            HashMap<Byte, Long> h = new HashMap<>();
            int n = is.readInt();
            int j = 0;
            while (j++ < n) {
                h.put(is.readByte(), is.readLong());
            }
            info.add(h);
        }
        len = is.readInt();
        i = 0;
        upgrade_require = new ArrayList<>();
        while (i++ < len) {
            UpgradeInfo ui = new UpgradeInfo();
            ui.readData(is);
            upgrade_require.add(ui);
        }
    }
}
