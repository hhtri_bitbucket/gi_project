/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ctmlab.game.data.staticdata;

import com.google.common.io.LittleEndianDataOutputStream;
import com.google.gson.JsonObject;
import java.io.IOException;

/**
 *
 * @author hhtri
 */
public class CashConvertItem {
    public byte type;
    public String name;
    public long selling;
    public long buying;
    
    public CashConvertItem(){}
    
    public CashConvertItem(byte type, String name, long priceSell, long priceBuy){
        this.type = type;
        this.name = name;
        this.selling = priceSell;
        this.buying = priceBuy;
    }
    
    public CashConvertItem(JsonObject obj){
        this.type = obj.get("type").getAsByte();
        this.name = obj.get("name").getAsString();
        this.selling = obj.get("selling").getAsLong();
        this.buying = obj.get("buying").getAsLong();
    }
    
    public JsonObject getObject(){
        JsonObject obj = new JsonObject();
        obj.addProperty("type", type);
        obj.addProperty("name", name);
        obj.addProperty("selling", selling);
        obj.addProperty("buying", buying);
        return obj;
    }
    
    public void writeData(LittleEndianDataOutputStream os) throws IOException{
        os.writeByte(type);
        os.writeLong(selling);
        os.writeLong(buying);
    }
}
