/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ctmlab.game.data.pojo;

import com.ctmlab.game.data.model.Claim;
import com.ctmlab.game.data.model.Exploration;
import com.ctmlab.game.data.model.GameObjectMap;
import com.ctmlab.game.data.model.GoldIncUser;
import com.ctmlab.game.data.model.HomeBase;
import com.ctmlab.game.data.model.Mine;
import com.ctmlab.game.data.model.Transportation;
import com.ctmlab.game.data.model.worker.Worker;
import com.ctmlab.game.data.model.building.Building;
import com.ctmlab.game.data.model.building.BuildingBarrack;
import com.ctmlab.game.data.model.building.BuildingDefense;
import com.ctmlab.game.data.model.building.BuildingExploration;
import com.ctmlab.game.data.model.building.BuildingOfficer;
import com.ctmlab.game.data.model.building.BuildingRefinery;
import com.ctmlab.game.data.model.building.BuildingTransportation;
import com.ctmlab.game.data.model.building.BuildingWall;
import com.ctmlab.game.data.model.building.BuildingWeaponDepot;
import com.ctmlab.game.data.model.equipment.Equipment;
import com.ctmlab.game.data.model.equipment.EquipmentDumptruck;
import com.ctmlab.game.data.model.equipment.EquipmentExcavator;
import com.ctmlab.game.data.model.equipment.EquipmentOilRig;
import com.ctmlab.game.data.model.equipment.EquipmentWashPlant;
import com.ctmlab.game.data.staticdata.CashConvertItem;
import com.ctmlab.game.data.staticdata.PointInt;
import com.ctmlab.game.data.staticdata.PointShort;
import com.ctmlab.game.data.staticdata.Region;
import com.ctmlab.game.data.staticdata.Zone;
import com.ctmlab.game.data.staticdata.gameobjectinfo.UpgradeInfo;
import com.ctmlab.game.goldincenum.ENUM_UNIT_TYPE;
import com.ctmlab.game.goldincenum.ENUM_REF_PROFILE;
import com.ctmlab.game.goldincenum.ENUM_RESOURCE_TYPE;
import com.ctmlab.game.goldincenum.ENUM_UNIT_PROPERTY;
import com.ctmlab.game.goldincenum.GI_SERVER_CMD;
import com.ctmlab.game.manager.StaticDataMng;
import com.ctmlab.game.network.manager.WSGameDefine;
import com.ctmlab.game.pathfinding.astartalgorithm.A_StarAlgorithm;
import com.ctmlab.game.threads.BuildingUpgradeThread;
import com.ctmlab.game.threads.GameStateThread;
import com.ctmlab.util.Utils;
import com.dp.db.DB;
import com.dp.db.Func;
import com.google.common.io.LittleEndianDataInputStream;
import com.google.common.io.LittleEndianDataOutputStream;
import java.awt.Point;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.TreeMap;

/**
 *
 * @author hhtri
 */
public class ProfilePrivate {

    private final Object LOCK = new Object();

    public String secret = "";
    public GameResources resource = new GameResources();
    public long lastEditTime = 0;
    public MineIndex homeBase = null;
    public ArrayList<MineIndex> lstIndexMine = new ArrayList();
    public ArrayList<MineIndex> lstIndexTestedMine = new ArrayList();
    public boolean wallet = false;
    public String walletID = "";
    public boolean pattern = false;

    public GameObjectMap refHomeBase = null;
    private ArrayList<Exploration> lstExploration = null;
    private ArrayList<Transportation> lstTransportation = null;
    private ArrayList<Building> lstBuilding = null;
    private ArrayList<Equipment> lstEquipment = null;
    private ArrayList<Worker> lstWorker = null;
    private ArrayList<Claim> lstClaim = null;

    private final TreeMap<Long, MineIndex> refTestMine = new TreeMap();
    private final TreeMap<Long, MineIndex> refPegMine = new TreeMap();
    private final HashMap<Integer, HashMap<Long, GameObjectMap>> refMines = new HashMap();

    private GoldIncUser mParent = null;
    private ArrayList<PointShort> pathToTradeCenter = null;

    public void resetMine() {
        HashMap<Long, GameObjectMap> hash = refMines.get(ENUM_REF_PROFILE.K_PEG_MINE.getCode());
        hash.clear();;
        for (MineIndex idx : refPegMine.values()) {
            Mine m = (Mine) StaticDataMng.i().worldMap.getItemMap(idx);
            m.userID = "";
            m.refClaim(null);
            m.saveData();
        }
        refPegMine.clear();
        if (lstClaim != null) {
            for (Claim c : lstClaim) {
                c.reset();
                c.delete(DB.i());
            }
            lstClaim.clear();
        }
    }

    public ProfilePrivate() {
        initRefMine();
    }

    public ProfilePrivate(GoldIncUser user) {
        mParent = user;
        initRefMine();
    }

    public void setListBuilding(ArrayList<Building> l) {
        lstBuilding = l;
    }

    public ArrayList<Building> getListBuilding() {
        return lstBuilding;
    }

    public void setListEquipment(ArrayList<Equipment> l) {
        lstEquipment = l;
    }

    public ArrayList<Equipment> getListEquipment() {
        return lstEquipment;
    }

    public void setListTransportation(ArrayList<Transportation> l) {
        lstTransportation = l;
//        if(lstExploration!=null&&!lstExploration.isEmpty()){
//            long curTime = System.currentTimeMillis();
//            for(int i=lstExploration.size()-1; i>=0; i--){
//                Exploration e = lstExploration.get(i);
//                if(e.endTime>curTime){
//                    e.getPathFinding();
//                } else{
//                    lstExploration.remove(i);
//                }
//            }
//        }
    }

    public ArrayList<Transportation> getListTransportation() {
        return lstTransportation;
    }

    public void setListExploration(ArrayList<Exploration> l) {
        lstExploration = l;
        if(lstExploration!=null&&!lstExploration.isEmpty()){
            long curTime = System.currentTimeMillis();
            for(int i=lstExploration.size()-1; i>=0; i--){
                Exploration e = lstExploration.get(i);
                if(e.endTime>curTime){
                    e.getPathFinding();
                } else{
                    lstExploration.remove(i);
                }
            }
        }
    }

    public ArrayList<Exploration> getListExploration() {
        return lstExploration;
    }

    public void setListWorker(ArrayList<Worker> l) {
        lstWorker = l;
    }

    public ArrayList<Worker> getListWorker() {
        return lstWorker;
    }

    public void setListClaim(ArrayList<Claim> l) {
        lstClaim = l;
    }

    public ArrayList<Claim> getListClaim() {
        return lstClaim;
    }

    private void initRefMine() {
        for (int k : ENUM_REF_PROFILE.getKeys()) {
            refMines.put(k, new HashMap<>());
        }
    }

    private HashMap<Long, GameObjectMap> getHashMap(int key) {
        HashMap<Long, GameObjectMap> rs = refMines.get(key);
        if (rs == null) {
            rs = new HashMap();
            synchronized (LOCK) {
                refMines.put(key, rs);
            }
        }
        return rs;
    }

    public void synReference() {
        synchronized (this) {
            HashMap<Long, GameObjectMap> hashTest = getHashMap(ENUM_REF_PROFILE.K_TEST_MINE.getCode());
            HashMap<Long, GameObjectMap> hashPeg = getHashMap(ENUM_REF_PROFILE.K_PEG_MINE.getCode());
            if (refTestMine.size() != hashTest.size()) {
                hashTest.clear();
                for (MineIndex m : refTestMine.values()) {
                    GameObjectMap o = StaticDataMng.i().worldMap.getItemMap(m);
                    hashTest.put(m.id, o);
                }
            }
            if (refPegMine.size() != hashPeg.size()) {
                hashPeg.clear();
                for (MineIndex m : refPegMine.values()) {
                    GameObjectMap o = StaticDataMng.i().worldMap.getItemMap(m);
                    if (lstClaim != null) {
                        for (Claim c : lstClaim) {
                            if (c.mineID == o.id) {
                                c.refData(lstEquipment, lstWorker);
                                o.refClaim(c);
                                break;
                            }
                        }
                    }
                    hashPeg.put(m.id, o);
                }
            } else {
                for (MineIndex m : refPegMine.values()) {
                    GameObjectMap o = StaticDataMng.i().worldMap.getItemMap(m);
                    if (lstClaim != null) {
                        for (Claim c : lstClaim) {
                            if (c.mineID == o.id) {
                                c.refData(lstEquipment, lstWorker);
                                o.refClaim(c);
                                break;
                            }
                        }
                    }
                    hashPeg.put(m.id, o);
                }
            }
        }
    }

    private HashMap<Byte, Long> getBuildingInfo(byte type) {
        if (lstBuilding == null) {
            return null;
        }
        for (GameObjectMap it : lstBuilding) {
            Building b = (Building) it;
            if (type == b.getType()) {
                ArrayList<HashMap<Byte, Long>> arr = b.getConfigGameObjectInfo();
                return arr.get(b.getLevel() - 1);
            }
        }
        return null;
    }

    public void initForTesting(String userID) {
        try {
            lstBuilding = new ArrayList<>();
            short id_r = refHomeBase.id_r;
            short id_z = refHomeBase.id_z;
            short xPos = 0;
            short yPos = 0;
            Building b = null;
            b = new BuildingBarrack(userID, id_r, id_z, xPos, yPos);b.writeData();lstBuilding.add(b);
            b = new BuildingDefense(userID, id_r, id_z, xPos, yPos);b.writeData();lstBuilding.add(b);
            b = new BuildingExploration(userID, id_r, id_z, xPos, yPos);b.writeData();lstBuilding.add(b);
            b = new BuildingOfficer(userID, id_r, id_z, xPos, yPos);b.writeData();lstBuilding.add(b);
            b = new BuildingRefinery(userID, id_r, id_z, xPos, yPos);b.writeData(); lstBuilding.add(b);
            b = new BuildingTransportation(userID, id_r, id_z, xPos, yPos);b.writeData();lstBuilding.add(b);
            b = new BuildingWall(userID, id_r, id_z, xPos, yPos);b.writeData();lstBuilding.add(b);
            b = new BuildingWeaponDepot(userID, id_r, id_z, xPos, yPos);b.writeData();lstBuilding.add(b);

            lstEquipment = new ArrayList<>();
            lstWorker = new ArrayList<>();
            Equipment eq = null;
            for (int i = 0; i < 10; i++) {
                eq = new EquipmentDumptruck(userID, id_r, id_z, xPos, yPos);eq.writeData();lstEquipment.add(eq);
                eq = new EquipmentExcavator(userID, id_r, id_z, xPos, yPos);eq.writeData();lstEquipment.add(eq);
                eq = new EquipmentOilRig(userID, id_r, id_z, xPos, yPos);eq.writeData();lstEquipment.add(eq);
                eq = new EquipmentWashPlant(userID, id_r, id_z, xPos, yPos);eq.writeData();lstEquipment.add(eq);

                Worker w = new Worker(userID);w.saveData();lstWorker.add(w);
                w = new Worker(userID);w.saveData();lstWorker.add(w);
            }
        } catch (IOException ex) {
        }
    }

    public void setHomeBase(GameObjectMap home) {
        homeBase = home.getIndex();
        refHomeBase = (HomeBase) home;
    }

    public void addMineFromDB(GameObjectMap claim) {
        MineIndex idxTmp = claim.getIndex();
        if (refTestMine.containsKey(idxTmp.id)) {
            HashMap<Long, GameObjectMap> hash = getHashMap(ENUM_REF_PROFILE.K_TEST_MINE.getCode());
            synchronized (LOCK) {
                hash.put(idxTmp.id, claim);
            }
        } else if (refPegMine.containsKey(idxTmp.id)) {
            HashMap<Long, GameObjectMap> hash = getHashMap(ENUM_REF_PROFILE.K_PEG_MINE.getCode());
            synchronized (LOCK) {
                hash.put(idxTmp.id, claim);
            }
        }
    }

    private boolean _IsMaxClaim() {
        if (lstBuilding == null) {
            return true;
        }
        for (Building b : lstBuilding) {
            if (ENUM_UNIT_TYPE.HOME_OFFICE.getCode() == b.getType() && b.objInfo != null) {
                HashMap<Byte, Long> info = b.getGOInfoByLevel();
                if (info.containsKey(ENUM_UNIT_PROPERTY.MAX_CLAIM.getCode())) {
                    long max = info.get(ENUM_UNIT_PROPERTY.MAX_CLAIM.getCode());
                    HashMap<Long, GameObjectMap> hashTest = refMines.get(ENUM_REF_PROFILE.K_PEG_MINE.getCode());
                    return max <= hashTest.size();
                }
            }
        }
        return false;
    }

    public int addClaim(GameObjectMap mine) {
        MineIndex idxTmp = mine.getIndex();
        if (refPegMine.containsKey(idxTmp.id) || (refHomeBase.id_z!=mine.id_z) || _IsMaxClaim()) {return 1;}
        HashMap<Long, GameObjectMap> hashMine = getHashMap(ENUM_REF_PROFILE.K_PEG_MINE.getCode());
        HashMap<Long, GameObjectMap> hashTest = getHashMap(ENUM_REF_PROFILE.K_TEST_MINE.getCode());
        synchronized (LOCK) {
            // add ref index to save
            refPegMine.put(idxTmp.id, idxTmp);
            // add ref object in world map
            Claim mCliam = new Claim();
            mCliam.mineID = idxTmp.id;
            mCliam.userID = mParent.userID;
            mCliam.saveData();
            // add claim to list claim in user
            lstClaim.add(mCliam);
            // ref claim in mine
            mine.refClaim(mCliam);

            hashMine.put(idxTmp.id, mine);
            // remove ref index to save
            refTestMine.remove(idxTmp.id);
            // remove ref object in world map
            hashTest.remove(idxTmp.id);
        }
        return 0;
    }

    public int addMineTest(GameObjectMap claim) {
        MineIndex idxTmp = claim.getIndex();
        if (refTestMine.containsKey(idxTmp.id)) {return 1;}
        HashMap<Long, GameObjectMap> hashTest = refMines.get(ENUM_REF_PROFILE.K_TEST_MINE.getCode());
        synchronized (LOCK) {
            // add ref index to save
            refTestMine.put(idxTmp.id, idxTmp);
            // add ref objectin world map
            hashTest.put(idxTmp.id, claim);
        }
        return 0;

    }

    public boolean isAvailableObject(short id_z, long id) {
        return refPegMine.containsKey(id) || refTestMine.containsKey(id);
    }

    public boolean checkResForUpgrade(TreeMap<Byte, Long> values) {
        GameResources resCopy = null;
        synchronized (LOCK) {resCopy = resource.copy();}
        return resCopy.checkResForUpgrade(values);
    }

    public int reduceResource(TreeMap<Byte, Long> values) {
        GameResources resCopy = null;
        synchronized (LOCK) {resCopy = resource.copy();}
        resCopy.decreaseResource(values);
        synchronized (LOCK) {resource.setData(resCopy);}
        return 0;
    }

    public GameResources getGameResources() {
        return resource;
    }

    public void setWallet(boolean wallet) {
        this.wallet = wallet;
    }

    public void setWalletID(String walletID) {
        if (this.walletID.isEmpty()) {
            this.walletID = String.format("%s-%s-%s-%s",
                    Utils.i().randomString(4), Utils.i().randomString(4),
                    Utils.i().randomString(4), Utils.i().randomString(4));
        } else {
            this.walletID = walletID;
        }
    }

    /* Methods for refinery*/
    // It used when refinery is finish (calling in refinery thread)
    public void setRefineryFuel(int fuel) {
        getGameResources().increaseValue(ENUM_RESOURCE_TYPE.FUEL, fuel);
        mParent.saveData();
    }

    public int refineFuel(LittleEndianDataOutputStream os, StringBuilder sb, int number) throws IOException {
        boolean flag = false;
        if (lstBuilding != null) {
            byte type = (byte) ENUM_UNIT_TYPE.HOME_REFINERY.getCode();
            for (Building it : lstBuilding) {
                if (type == it.getType()) {
                    BuildingRefinery br = (BuildingRefinery) it;
                    if (br.endTimeRefinery > 0 && br.endTimeRefinery > System.currentTimeMillis()) {
                        os.writeShort(GI_SERVER_CMD.RES_REFINE_OIL.getCode());
                        os.writeByte(1);
                        return 1;
                    }
                    byte speedRefine = ENUM_UNIT_PROPERTY.REFINE_SPEED.getCode();
                    HashMap<Byte, Long> hash = br.getGOInfoByLevel();
                    if (hash.containsKey(speedRefine)) {
                        long time = number * hash.get(speedRefine);
                        if (getGameResources().getValue(ENUM_RESOURCE_TYPE.OIL) >= number) {
                            // refine, reduce oil
                            long startTime = System.currentTimeMillis();
                            GameStateThread.i().addRefineryOilInfo(mParent.userID, br, startTime, (startTime + time), number);
                            final String format = String.format("{rs,start,end}-{%d,%d,%d}", 0, startTime, (startTime + time));
                            if(!getGameResources().decreaseValue(ENUM_RESOURCE_TYPE.OIL, number)){
                                // return start, end time to client
                                os.writeShort(GI_SERVER_CMD.RES_REFINE_OIL.getCode());
                                os.writeByte(0);
                                sb.append(format);
                            } else{
                                // return start, end time to client
                                os.writeShort(GI_SERVER_CMD.RES_REFINE_OIL.getCode());
                                os.writeByte(0);
                                sb.append(format);

                                // update time to building
                                br.startTimeRefinery = startTime;
                                br.endTimeRefinery = (startTime + time);

                                mParent.saveData();
                                br.writeData();
                            }
                        }
                    }
                    flag = true;
                    break;
                }
            }
        }
        if (!flag) {
            os.writeShort(GI_SERVER_CMD.RES_REFINE_OIL.getCode());
            os.writeByte(1);
            return 1;
        }
        return 0;
    }

    /* Methods for exploration*/
    public void removeExploration(Exploration obj) {
        if (lstExploration != null) {
            for (int i = lstExploration.size() - 1; i >= 0; i--) {
                Exploration o = lstExploration.get(i);
                if (o.id == obj.id) {lstExploration.remove(i);return;}
            }
        }
    }

    private ArrayList<PointShort> findingPath(GameObjectMap objDest, Point from, Point to){
        Zone z = StaticDataMng.i().worldMap.getZone(objDest.id_z);
        byte[][] matrix = null;
        if(objDest.id_z==refHomeBase.id_z) {
//            matrix = z.getRawMatrix(objDest.xPos, objDest.yPos, objDest);
            matrix = z.getRawMatrix();
            to = new Point(objDest.xPos, objDest.yPos);
        } else{
            Region r = StaticDataMng.i().worldMap.getRegion(refHomeBase.id_r);
            Zone z0 = StaticDataMng.i().worldMap.getZone(refHomeBase.id_z);
            matrix = r.getInnerMatrix(z0, z);
            ArrayList<Integer> pos = refHomeBase.getRealLocation();
            from = new Point(pos.get(0), pos.get(1));
            pos = objDest.getRealLocation();
            to = new Point(pos.get(0), pos.get(1));
        }
        ArrayList<PointShort> rs = A_StarAlgorithm.findPath(matrix, from, to);
        return rs;
    }

    public GameObjectMap exploratingToMine(GameObjectMap obj) {
        if (refHomeBase != null) {
            Exploration expl = new Exploration();
            expl.setInfo(obj, refHomeBase.userID);
            expl.setPathFinding(findingPath(obj, new Point(refHomeBase.xPos, refHomeBase.yPos), new Point(obj.xPos, obj.yPos)));
            long speedPerCell = 1; // get speed from
            if (mParent != null) {
                HashMap<Byte, Long> hashInfo = getBuildingInfo((byte) ENUM_UNIT_TYPE.HOME_EXPLORATION.getCode());
                if (hashInfo != null) {speedPerCell = hashInfo.get(ENUM_UNIT_PROPERTY.SPEED_LAND.getCode());}
            }
//            Double timeExplore = Math.sqrt(Math.pow(refHomeBase.xPos - obj.xPos, 2) + Math.pow(refHomeBase.yPos - obj.yPos, 2)) / speed;
            long startTime = System.currentTimeMillis();
            long endTime = startTime + (speedPerCell*expl.path.size());
            expl.setTimes(startTime, endTime);
            if (lstExploration == null) {return null;}
            synchronized (LOCK) {
                if (expl.saveData() > 0) {lstExploration.add(expl);}
                else {expl = null;}
            }
            return expl;
        }
        return null;
    }

    /* Methods for truck*/
    public Transportation getFreeTransportation(byte type) {
        Transportation truck = new Transportation();
        truck.id_r = -1;
        truck.id_z = -1;
        truck.xPos = -1;
        truck.yPos = -1;
        truck.mType = (byte) ENUM_UNIT_TYPE.TRANSPORTATION.getCode();
        truck.srcID = refHomeBase.id;
        truck.destID = -1;
        truck.userID = refHomeBase.userID;
        HashMap<Byte, Long> hashInfo = getBuildingInfo((byte) ENUM_UNIT_TYPE.HOME_TRANSPORTATION.getCode());
        if (hashInfo != null && hashInfo.containsKey(type)) {
            truck.capacity = hashInfo.get(type).intValue();
        }
        try {
            if (truck.saveData() > 0) {lstTransportation.add(truck);}
            else {truck = null;}
        } catch (Exception ex) {
            truck = null;
        }
        return truck;
    }

    public TradeTransaction buyResource(TreeMap<Byte, Long> lstTrade) {

        // reduce cash first
        // adding resources trade after finishing transport

        TradeTransaction rs = new TradeTransaction();
        Transportation truck = getFreeTransportation(ENUM_UNIT_PROPERTY.SPEED_LAND.getCode());
        if (truck == null) {
            rs.setLstTrade(lstTrade);
            rs.setResultTrans(false);
            rs.typeTranstport = TradeTransaction.ACTION_BUY;
            return rs;
        }
        rs.truckID = truck.id;
        rs.typeTranstport = TradeTransaction.ACTION_BUY;
        rs.setLstTrade(lstTrade);
        rs.setResultTrans(false);
        GameResources resCopy = null;
        synchronized (LOCK) {resCopy = resource.copy();}
        TreeMap<Byte, Long> lstResult = new TreeMap();
        CashConvertItem itemCash = null;
        long totalCash = 0;
        boolean flag = true;
        for (Entry<Byte, Long> e : lstTrade.entrySet()) {
            itemCash = StaticDataMng.i().cashConvert.getPriceInfo(e.getKey());
            if (itemCash == null) {
                lstResult.put(e.getKey(), (long) WSGameDefine.ADD_RES_MISSING_CONFIG);
                flag = false;
            } else {
                int rCode = resCopy.addResource(true, e.getKey(), e.getValue(), itemCash.selling);
                lstResult.put(e.getKey(), (rCode == WSGameDefine.ADD_RES_SUCCESS) ? (e.getValue() * itemCash.selling) : rCode);
                totalCash += (e.getValue() * itemCash.selling);
                if (rCode != WSGameDefine.ADD_RES_SUCCESS) {flag = false;}
            }
        }
        if (flag) {
            synchronized (LOCK) {
                getGameResources().decreaseValue(ENUM_RESOURCE_TYPE.CASH, totalCash);
                mParent.saveData();
            }
        }
        rs.setLstResult(lstResult);
        rs.setResultTrans(flag);
        return rs;
    }

    public TradeTransaction sellResource(TreeMap<Byte, Long> lstTrade) {

        // 1 copy user resource => resCopy
        // 2 get free transport and make new transaction
        // 3 make a flag for resulting and reduce resources selling to resCopy. If there are some wrong, the flag will be wrong (false)
        // 4 if the flag is true, to calculate cash for transportation.
        TradeTransaction rs = new TradeTransaction();
        Transportation truck = getFreeTransportation(ENUM_UNIT_PROPERTY.SPEED_LAND.getCode());
        if (truck == null) {
            rs.setLstTrade(lstTrade);
            rs.setResultTrans(false);
            rs.typeTranstport = TradeTransaction.ACTION_SELL;
            return rs;
        }
        rs.truckID = truck.id;
        rs.setLstTrade(lstTrade);
        rs.setResultTrans(false);
        rs.typeTranstport = TradeTransaction.ACTION_SELL;
        GameResources resCopy = null;
        synchronized (LOCK) {resCopy = resource.copy();}
        TreeMap<Byte, Long> lstResult = new TreeMap();
        CashConvertItem itemCash = null;
        boolean flag = true;
        boolean isBuying = false;
        for (Entry<Byte, Long> e : lstTrade.entrySet()) {
            itemCash = StaticDataMng.i().cashConvert.getPriceInfo(e.getKey());
            if (itemCash == null) {
                lstResult.put(e.getKey(), (long) WSGameDefine.ADD_RES_MISSING_CONFIG);
                flag = false;
            } else {
                int rCode = resCopy.addResource(isBuying, e.getKey(), e.getValue(), itemCash.buying);
                lstResult.put(e.getKey(), (rCode == WSGameDefine.ADD_RES_SUCCESS) ? (e.getValue() * itemCash.buying) : rCode);
                if (rCode != WSGameDefine.ADD_RES_SUCCESS) {flag = false;}
            }
        }
        if (flag) {
            synchronized (LOCK) {
                getGameResources().setData(resCopy);
                mParent.saveData();
            }
        }
        rs.setResultTrans(flag);
        rs.setLstResult(lstResult);
        return rs;
    }

    public GameObjectMap transportHB2TC(GameObjectMap obj, TradeTransaction transacion, TreeMap<Byte, Long> tree) {
        if (refHomeBase != null) {
            Transportation truck = null;
            if (lstTransportation == null) {
                return null;
            } else {
                for (Transportation t : lstTransportation) {
                    if (t.id == transacion.truckID) {
                        truck = t;
                        break;
                    }
                }
                if (truck == null) {return null;}
            }
            truck.setInfo(obj, refHomeBase.id, refHomeBase.userID);
            if(pathToTradeCenter==null){
                pathToTradeCenter = findingPath(obj, new Point(refHomeBase.xPos, refHomeBase.yPos), new Point(obj.xPos, obj.yPos));
            }
            truck.setPathFinding(pathToTradeCenter);
            long speedPerCell = 1; // get speed from
            if (mParent != null) {
                HashMap<Byte, Long> hashInfo = getBuildingInfo((byte) ENUM_UNIT_TYPE.HOME_TRANSPORTATION.getCode());
                if (hashInfo != null) {speedPerCell = hashInfo.get(ENUM_UNIT_PROPERTY.SPEED_LAND.getCode());}
            }
            long startTime = System.currentTimeMillis();
            long endTime = startTime + (speedPerCell*truck.path.size());

            truck.setTimes(startTime, endTime);
            truck.setTransaction(transacion);
            synchronized (LOCK) {if (truck.saveData() < 0) {return null;}}
            return truck;
        }
        return null;
    }

    private void refEquipmentToCliam(Transportation t, ArrayList<Long> equipments, ArrayList<Long> workers) {
        HashMap<Long, GameObjectMap> hashTest = refMines.get(ENUM_REF_PROFILE.K_PEG_MINE.getCode());
        GameObjectMap mine = hashTest.get(t.destID);
        if (mine.getClaim() != null) {mine.getClaim().setEquipAndWorkerData(lstEquipment, lstWorker, equipments, workers);}
    }

    private boolean isEquipmentAndWorkerAvailable(ArrayList<Long> lstEquip, ArrayList<Long> lstW) {
        for (Equipment e : lstEquipment) {if (lstEquip.contains(e.id) && e.inClaim) {return false;}}
        for (Worker w : this.lstWorker) {if (lstW.contains(w.id) && w.inClaim) {return false;}}
        return true;
    }

    private void setEquipmentAndWorkerFlag(ArrayList<Long> lstEquip, ArrayList<Long> lstW) {
        for (Equipment e : lstEquipment) {if (lstEquip.contains(e.id)) {e.inClaim = true;}}
        for (Worker w : this.lstWorker) {if (lstW.contains(w.id)) {w.inClaim = true;}}
    }

    public Transportation equipmentsToClaim(GameObjectMap mine_claim, ArrayList<Long> lstEquip, ArrayList<Long> lstWorker, byte type) {
        synchronized (LOCK) {if (!isEquipmentAndWorkerAvailable(lstEquip, lstWorker)) {return null;}}

        Transportation truck = getFreeTransportation(ENUM_UNIT_PROPERTY.SPEED_LAND.getCode());
        if (refHomeBase != null) {
            double speed = 1;
            if (mParent != null) {
                HashMap<Byte, Long> hashInfo = getBuildingInfo((byte) ENUM_UNIT_TYPE.HOME_TRANSPORTATION.getCode());
                if (hashInfo != null) {speed = hashInfo.get(type);}
            }
            Double disTime = Math.sqrt(Math.pow(refHomeBase.xPos - mine_claim.xPos, 2) + Math.pow(refHomeBase.yPos - mine_claim.yPos, 2)) / speed;
            long startTime = System.currentTimeMillis();
            long endTime = startTime + disTime.longValue();

            TradeTransaction transacion = new TradeTransaction();
            transacion.setEquipment(lstEquip);
            transacion.setWorker(lstWorker);
            transacion.truckID = truck.id;
            transacion.typeTranstport = TradeTransaction.ACTION_TRANS_TO_CLAIM;

            truck.setInfo(mine_claim, refHomeBase.id, refHomeBase.userID);
            truck.setTimes(startTime, endTime);
            truck.setTransaction(transacion);
            synchronized (LOCK) {
                if (truck.saveData() > 0) {setEquipmentAndWorkerFlag(lstEquip, lstWorker);}
                else {return null;}
            }
        }
        return truck;
    }

    private void setFuelToClaim(Transportation t) {
        HashMap<Long, GameObjectMap> hashPeg = getHashMap(ENUM_REF_PROFILE.K_PEG_MINE.getCode());
        Mine m = (Mine) hashPeg.get(t.destID);
        TreeMap<Byte, Long> lstResult = t.getTransaction().getLstResult();
        if (lstResult.containsKey(ENUM_RESOURCE_TYPE.FUEL.getCode())) {
            long fuel = lstResult.get(ENUM_RESOURCE_TYPE.FUEL.getCode());
            m.getClaim().fuel += fuel;
            m.getClaim().saveData();
        }
    }

    public Transportation fuelToClaim(GameObjectMap mine_claim, long fuel, byte typeTrans) {

        synchronized (LOCK) {if (getGameResources().getValue(ENUM_RESOURCE_TYPE.FUEL) < fuel) {return null;}}

        TradeTransaction transacion = new TradeTransaction();
        Transportation truck = getFreeTransportation(ENUM_UNIT_PROPERTY.SPEED_LAND.getCode());
        if (refHomeBase != null) {
            double speed = 1;
            if (mParent != null) {
                HashMap<Byte, Long> hashInfo = getBuildingInfo((byte) ENUM_UNIT_TYPE.HOME_TRANSPORTATION.getCode());
                if (hashInfo != null) {speed = hashInfo.get(typeTrans);}
            }
            Double disTime = Math.sqrt(Math.pow(refHomeBase.xPos - mine_claim.xPos, 2) + Math.pow(refHomeBase.yPos - mine_claim.yPos, 2)) / speed;
            long startTime = System.currentTimeMillis();
            long endTime = startTime + disTime.longValue();

            transacion.truckID = truck.id;
            transacion.typeTranstport = TradeTransaction.ACTION_TRANS_FUEL;

            truck.id_r = mine_claim.id_r;
            truck.id_z = mine_claim.id_z;
            truck.xPos = mine_claim.xPos;
            truck.yPos = mine_claim.yPos;
            truck.mType = (byte) ENUM_UNIT_TYPE.TRANSPORTATION.getCode();
            truck.srcID = refHomeBase.id;
            truck.destID = mine_claim.id;
            truck.userID = refHomeBase.userID;
            truck.setTimes(startTime, endTime);
            truck.setTransaction(transacion);
            synchronized (LOCK) {
                if (truck.saveData() > 0) {
                    getGameResources().decreaseValue(ENUM_RESOURCE_TYPE.FUEL, fuel);
                    TreeMap<Byte, Long> lstResult = new TreeMap<>();
                    lstResult.put(ENUM_RESOURCE_TYPE.FUEL.getCode(), fuel);
                    transacion.setLstResult(lstResult);
                    mParent.saveData();
                } else{
                    return null;
                }
            }
        }
        return truck;
    }

    public void transportFinish(long idTruck) {
        if (lstTransportation != null) {
            for (int i = lstTransportation.size() - 1; i >= 0; i--) {
                if (lstTransportation.get(i).id == idTruck) {
                    finishTransaction(lstTransportation.remove(i));
                    break;
                }
            }
        }
    }

    private void finishTransaction(Transportation t) {
        switch (t.tradeTransaction.typeTranstport) {
            case TradeTransaction.ACTION_SELL:{
                TreeMap<Byte, Long> listResult = t.getTransaction().getLstResult();
                long total = 0;
                for(Long i:listResult.values()){total+=i;}
                synchronized(LOCK){
                    getGameResources().increaseValue(ENUM_RESOURCE_TYPE.CASH, total);
                    mParent.saveData();
                }
                break;
            }
            case TradeTransaction.ACTION_BUY:
                TreeMap<Byte, Long> lstTrade = t.getTransaction().getLstTrade();
                GameResources resCopy = null;
                synchronized (LOCK) {resCopy = resource.copy();}
                resCopy.increaseResource(lstTrade);
                synchronized (LOCK) {
                    getGameResources().setData(resCopy);
                    mParent.saveData();
                }
                break;
            case TradeTransaction.ACTION_TRANS_TO_CLAIM:
                refEquipmentToCliam(t, t.getTransaction().lstEquipment, t.getTransaction().lstWorker);
                break;
            case TradeTransaction.ACTION_TRANS_FUEL:
                setFuelToClaim(t);
                break;
            default:
                break;
        }
    }

    /* Methods for building*/
    public boolean checkBuildingLevel(byte type, int level) {
        if (lstBuilding == null) {
            return false;
        }
        for (GameObjectMap i : lstBuilding) {
            Building it = (Building) i;
            if (type == it.getType() && level <= it.getLevel()) {
                return true;
            }
        }
        return false;
    }

    public int checkUpgreadeBuilding(byte type) {
        int rs = 0;
        if (lstBuilding == null) {
            return -1;
        }
        for (Building it : lstBuilding) {
            if (type == it.getType()) {
                if (it.objInfo == null || ((it.getLevel() + 1) > it.objInfo.getMaxLevel())) {
                    return 1;
                }
                ArrayList<UpgradeInfo> ls = it.getConfigGameObjectUpgradeInfo();
                if (ls == null) {
                    return 1;
                }
                int lvIndx = it.getLevel();
                if (ls.size() > lvIndx) {
                    UpgradeInfo upInfo = ls.get(lvIndx);
                    TreeMap<Byte, Integer> building_level = upInfo.building_level;
                    // check building level
                    for (Entry<Byte, Integer> e : building_level.entrySet()) {
                        if (!checkBuildingLevel(e.getKey(), e.getValue())) {
                            return 1;
                        }
                    }
                    // check resource
                    if (!checkResForUpgrade(upInfo.resources)) {
                        return 1;
                    }
                }
                break;
            }
        }
        return rs;
    }

    public int upgradeBuilding(byte type, LittleEndianDataOutputStream os, StringBuilder sb) throws IOException {
        int rs = 1;
        if (lstBuilding == null) {
            return -1;
        }
        for (Building it : lstBuilding) {
            if (type == it.getType()) {
                ArrayList<UpgradeInfo> ls = it.getConfigGameObjectUpgradeInfo();
                int lvIndx = it.getLevel();
                if (ls.size() >= lvIndx) {
                    UpgradeInfo upInfo = ls.get(lvIndx);
                    reduceResource(upInfo.resources);
                    long start = System.currentTimeMillis();
                    long end = start + upInfo.timeUpgrade;
                    it.setTimeUpgrade(start, end);
                    BuildingUpgradeThread.i().addToBuff(mParent.userID, type, it.getLevel() + 1, start, end);
                    os.writeByte(0);
                    os.writeByte(it.getType());
                    it.writeData(os);
                    sb.append(Func.json(it));
                    return 0;
                }
            }
        }
        os.writeByte(rs);
        return rs;
    }

    public Building setBuildingLevel(byte type, int lv) {
        if (lstBuilding == null) {
            return null;
        }
        for (GameObjectMap i : lstBuilding) {
            Building b = (Building) i;
            if (b.getType() == type) {
                b.setLevel(lv);
                return b;
            }
        }
        return null;
    }

    /* Methods for datastream*/
    private void writeBaseInfoPrivateProfile(LittleEndianDataOutputStream os) throws IOException {
        Utils.i().writeBigString(os, secret);
        getGameResources().writeData(os);
        os.writeLong(lastEditTime);
        if (refHomeBase != null) {
            os.writeBoolean(true);
            refHomeBase.writeData(os);
        } else {
            os.writeBoolean(false);
        }
    }

    public void writeResponseData(LittleEndianDataOutputStream os) throws IOException {
        synchronized (LOCK) {
            writeBaseInfoPrivateProfile(os);
            GameObjectMap[] temp;
            Building[] tempBuildings;
            Equipment[] tempEquipments;
            // Send peg mine
            temp = refMines.get(ENUM_REF_PROFILE.K_PEG_MINE.getCode()).values().toArray(WSGameDefine.EMPTY_GAMEOBJECT_MAP);
            os.writeInt(temp.length);
            for (GameObjectMap e : temp) {
                e.writeData(os);
            }
            // send tested mine
            temp = refMines.get(ENUM_REF_PROFILE.K_TEST_MINE.getCode()).values().toArray(WSGameDefine.EMPTY_GAMEOBJECT_MAP);
            if (lstExploration == null || lstExploration.isEmpty()) {
                // send test mine (not check)
                os.writeInt(temp.length);
                for (GameObjectMap e : temp) {
                    Mine m = (Mine) e;
                    m.writeData(os);
                }
                // send exploration is empty
                os.writeInt(0);
            } else {
                // get resource unlock from level of exploration_depot (containing gameobjectinfo exploration depot)
                int order = -1;
                for (GameObjectMap i : lstBuilding) {
                    Building b = (Building) i;
                    if (b.getType() == ENUM_UNIT_TYPE.HOME_EXPLORATION.getCode()) {
                        HashMap<Byte, Long> info = b.getGOInfoByLevel();
                        Long orderItem = info.get(ENUM_UNIT_PROPERTY.RESOURCE_UNLOCK.getCode());
                        if(orderItem!=null){
                            order = orderItem.intValue();
                        }
                    }
                }
                // send test mine
                os.writeInt(temp.length);
                for (GameObjectMap e : temp) {
                    Mine m = (Mine) e;
                    boolean flag = false;
                    boolean isExploring = false;
                    for (Exploration expl : lstExploration) {
                        if (expl.destID == m.id) {
                            flag = true;
                            isExploring = expl.endTime > System.currentTimeMillis();
                            break;
                        }
                    }
                    if (flag && isExploring) {
                        m.writeDataEmptyMine(os);
                    } else {
                        m.writeDataWithOrder(os, order);
                    }
                }
                // send exploration is empty
                os.writeInt(lstExploration.size());
                for (Exploration e : lstExploration) {
                    e.writeData(os);
                }
            }
            if (lstTransportation == null || lstTransportation.isEmpty()) {
                os.writeInt(0);
            } else {
                os.writeInt(lstTransportation.size());
                for (Transportation e : lstTransportation) {
                    e.writeData(os);
                }
            }
            // send list buildings
            if (lstBuilding == null || lstBuilding.isEmpty()) {
                os.writeInt(0);
            } else {
                tempBuildings = lstBuilding.toArray(WSGameDefine.EMPTY_BUILDING);
                os.writeInt(tempBuildings.length);
                for (Building e : tempBuildings) {
                    os.writeByte(e.getType());
                    e.writeData(os);
                }
            }
            // send list equipment
            if (lstEquipment == null || lstEquipment.isEmpty()) {
                os.writeInt(0);
            } else {
                tempEquipments = lstEquipment.toArray(WSGameDefine.EMPTY_EQUIPMENT);
                os.writeInt(tempEquipments.length);
                for (Equipment e : tempEquipments) {
                    os.writeByte(e.getType());
                    e.writeData(os);
                }
            }
            // send list worker
            if (lstWorker == null || lstWorker.isEmpty()) {
                os.writeInt(0);
            } else {
                Worker[] tempWorkers = lstWorker.toArray(WSGameDefine.EMPTY_WORKER);
                os.writeInt(tempWorkers.length);
                for (Worker e : tempWorkers) {
                    os.writeByte(e.getType());
                    e.writeData(os);
                }
            }
            os.writeBoolean(wallet);
            os.writeBoolean(pattern);
            Utils.i().writeBigString(os, walletID);
        }
    }

    public void readData(LittleEndianDataInputStream is) throws IOException {
        secret = Utils.i().readBigString(is);
        resource = new GameResources();
        resource.readData(is);
        lastEditTime = is.readLong();
        if (is.readBoolean()) {
            homeBase = new MineIndex();
            homeBase.readData(is);
        }
        int len = is.readInt();
        int i = 0;
        MineIndex t = null;
        while (i < len) {
            t = new MineIndex();
            t.readData(is);
            refPegMine.put(t.id, t);

            i++;
        }
        len = is.readInt();
        i = 0;
        while (i < len) {
            t = new MineIndex();
            t.readData(is);
            refTestMine.put(t.id, t);

            i++;
        }
        wallet = is.readBoolean();
        pattern = is.readBoolean();
        walletID = Utils.i().readBigString(is);
    }

    public void writeData(LittleEndianDataOutputStream os) throws IOException {
        synchronized (LOCK) {
            Utils.i().writeBigString(os, secret);
            getGameResources().writeData(os);
            os.writeLong(lastEditTime);
            if (homeBase != null) {
                os.writeBoolean(true);
                homeBase.writeData(os);
            } else {
                os.writeBoolean(false);
            }
            MineIndex[] emptyMine = new MineIndex[0];
            MineIndex[] temp;
            temp = refPegMine.values().toArray(emptyMine);
            os.writeInt(temp.length);
            for (MineIndex m : temp) {
                m.writeData(os);
            }
            temp = refTestMine.values().toArray(emptyMine);
            os.writeInt(temp.length);
            for (MineIndex m : temp) {
                m.writeData(os);
            }
            os.writeBoolean(wallet);
            os.writeBoolean(pattern);
            Utils.i().writeBigString(os, walletID);
        }
    }
}
