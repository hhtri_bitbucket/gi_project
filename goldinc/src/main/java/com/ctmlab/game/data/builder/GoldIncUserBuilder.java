/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ctmlab.game.data.builder;

import com.ctmlab.game.data.model.GoldIncUser;
import com.ctmlab.util.Utils;
import com.dp.db.DB;
import java.sql.SQLException;

/**
 *
 * @author hhtri
 */
public class GoldIncUserBuilder {
    private String userID;
    private String password;
    private String companyName;
    public GoldIncUserBuilder(){}

    /**
     * @param userID the userID to set
     * @return 
     */
    public GoldIncUserBuilder setUserID(String userID) {
        this.userID = userID;
        return this;
    }

    /**
     * @param password the password to set
     * @return 
     */
    public GoldIncUserBuilder setPassword(String password) {
        this.password = password;
        return this;
    }

    /**
     * @param companyName the companyName to set
     * @return 
     */
    public GoldIncUserBuilder setCompanyName(String companyName) {
        this.companyName = companyName;
        return this;
    }
    
    public GoldIncUser build(){
        GoldIncUser u = new GoldIncUser();
        u.setUserID(userID);
        u.setPassword(Utils.i().sha256(password));
        u.setCompanyName(companyName, false);
        u.setGameName(userID.split("@")[0], false);
        u.updateWallet(true, false);        
        u.setAllianceID(-1);
        u.setPlayerProfile();
        return u;
    }
    
    public GoldIncUser buildAndSave() throws SQLException{
        GoldIncUser u = build();
        if(u.save(DB.i().con())<0){return null;}
        return u;
    }
}
