/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ctmlab.game.data.model;

import com.ctmlab.game.data.model.worker.Worker;
import com.ctmlab.game.data.model.equipment.Equipment;
import com.ctmlab.game.goldincenum.ENUM_UNIT_TYPE;
import com.ctmlab.game.network.manager.WSGameDefine;
import com.dp.db.DB;
import com.dp.db.DBDataPostgres;
import com.dp.db.DataFieldType;
import com.google.common.io.LittleEndianDataInputStream;
import com.google.common.io.LittleEndianDataOutputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import sun.audio.AudioDataStream;

/**
 *
 * @author hhtri
 */
public class Claim  extends DBDataPostgres{
    @DataFieldType(primaryKey = true, notNull = true, autoIncrease = true, usingMapSeq = true)
    public long id; 
    
    public String userID;
    public long mineID;    
    public long dirt; // mining and store (having rating)
    public long fuel; // from homebase to claim    
    public byte[] dataRef;
    
    @DataFieldType(ignore = true)
    public Equipment[] lstEquipment;
    @DataFieldType(ignore = true)
    public Worker[] lstWorker;
    
    public Claim(){
        userID = "";
        mineID = -1;
        dirt = -1;
        fuel = -1;
        id = 0;
        lstEquipment = new Equipment[]{null, null, null, null};
        lstWorker = new Worker[]{null, null, null, null};
        dataRef = WSGameDefine.EMPTY_BYTES;
    }
    
    public void reset(){
        for(Equipment e:lstEquipment){
            if(e!=null){
                e.inClaim = false;
                e.saveData();
            }
        }
        for(Worker w:lstWorker){
            if(w!=null){
                w.inClaim = false;
                w.saveData();
            }
        }
    }
      
    public boolean isFullWorker(){
        if(lstWorker==null){
            lstWorker = new Worker[]{null, null, null, null};
            return false;
        }
        boolean rs = true;
        for(Worker w:lstWorker){
            if(w==null){
                rs = false;
                break;
            }
        }
        return rs;
    } 
    
    public ArrayList<Byte> isFullEquip(){
        ArrayList<Byte> rs = new ArrayList();
        rs.add((byte)ENUM_UNIT_TYPE.CLAIM_DUMPTRUCK.getCode());
        rs.add((byte)ENUM_UNIT_TYPE.CLAIM_EXCAVATOR.getCode());
        rs.add((byte)ENUM_UNIT_TYPE.CLAIM_OIL_RIG.getCode());
        rs.add((byte)ENUM_UNIT_TYPE.CLAIM_WASH_PLANT.getCode());
        if(lstEquipment==null){
            lstEquipment = new Equipment[]{null, null, null, null};
        }
        for(Equipment e:lstEquipment){
            if(e!=null){rs.remove(e.getType());}
        }
        
        return rs;
    }
    
    private void putEquipment(ArrayList<Equipment> lstEquipData, ArrayList<Long> lstEquipMoving) {
        int len = lstEquipment.length;
        ArrayList<Byte> rs = isFullEquip();
        if (!rs.isEmpty()) {
            for (int i = 0; i < len; i++) {
                if (lstEquipment[i] == null) {
                    for (Equipment e : lstEquipData) {
                        if (rs.contains(e.getType()) && lstEquipMoving.contains(e.id)) {
                            lstEquipment[i] = e;
                            e.saveData(); // save 'inClaim' var to Database
                            lstEquipMoving.remove(e.id);
                            break;
                        }
                    }
                }
            }
        }
    }
    
    private void putWorker(ArrayList<Worker> lstWorkerData, ArrayList<Long> lstWorkerMoving) {
        int len = lstWorker.length;
        if (!isFullWorker()) {
            for (int i = 0; i < len; i++) {
                if (lstWorker[i] == null) {
                    for (Worker w : lstWorkerData) {
                        if (lstWorkerMoving.contains(w.id)) {
                            lstWorker[i] = w;
                            w.saveData(); // save 'inClaim' var to Database
                            lstWorkerMoving.remove(w.id);
                            break;
                        }
                    }
                }
            }
        }
    }
    
    public void setEquipAndWorkerData(ArrayList<Equipment> lstEquipData, ArrayList<Worker> lstWorker, ArrayList<Long> lstEquipMoving, ArrayList<Long> lstWorkerMoving) {
        putEquipment(lstEquipData, lstEquipMoving);
        putWorker(lstWorker, lstWorkerMoving);
        saveData();
    }
    
    public void refData(ArrayList<Equipment> lstEquip, ArrayList<Worker> lstW){
        ByteArrayInputStream bais = new ByteArrayInputStream(dataRef);
        try(LittleEndianDataInputStream is = new LittleEndianDataInputStream(bais)){
            int len = is.readInt();
            if(len==lstEquipment.length){
                for(int i=0; i<len; i++){
                    long id = is.readLong();
                    if(id>0){
                        for(Equipment e:lstEquip){
                            if(e.id==id){
                                lstEquipment[i] = e;
                                break;
                            }
                        }
                    }
                }
            }
            len = is.readInt();
            if(len==lstWorker.length){
                for(int i=0; i<len; i++){
                    long id = is.readLong();
                    if(id>0){
                        for(Worker w:lstW){
                            if(w.id==id){
                                lstWorker[i] = w;
                                break;
                            }
                        }
                    }
                }
            }
        } catch(Exception ex){}
    }
    
    public void saveData(){
        try{            
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            try(LittleEndianDataOutputStream os = new LittleEndianDataOutputStream(baos)){
                if(lstEquipment!=null){
                    os.writeInt(lstEquipment.length);
                    for(Equipment e:lstEquipment){
                        os.writeLong(e==null?0L:e.id);
                    }
                }
                if(lstWorker!=null){
                    os.writeInt(lstWorker.length);
                    for(Worker w:lstWorker){
                        os.writeLong(w==null?0L:w.id);
                    }
                }
                dataRef = baos.toByteArray();
            }            
            this.save(DB.i().con());
        } catch(IOException | SQLException ex){}
    }
    
    public void writeData(LittleEndianDataOutputStream os) throws IOException{
        os.writeLong(id);
        os.writeLong(dirt);
        os.writeLong(fuel);
        // equip ref
        os.writeInt(lstEquipment.length);
        for(Equipment e:lstEquipment){
            os.writeLong(e==null?0L:e.id);
        }
        
        os.writeInt(lstWorker.length);
        for(Worker w:lstWorker){
            os.writeLong(w==null?0L:w.id);
        }
    }
}
