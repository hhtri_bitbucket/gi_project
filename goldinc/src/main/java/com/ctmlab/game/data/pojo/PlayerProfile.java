/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ctmlab.game.data.pojo;

import com.ctmlab.game.data.model.GoldIncUser;
import com.ctmlab.game.data.model.SocialAlliance;
import com.ctmlab.game.manager.GoldIncUserMng;
import com.ctmlab.util.Utils;
import com.google.common.io.LittleEndianDataInputStream;
import com.google.common.io.LittleEndianDataOutputStream;
import java.io.IOException;

/**
 *
 * @author hhtri
 */
public class PlayerProfile {    
    public String userID = "";
    public String name = "";
    public String companyName = "";
    public int userLevel = 1;    
    public long allianceID = -1;
    public ProfilePrivate privateInfo = new ProfilePrivate();
    
    public PlayerProfile(){}
    
    public ProfilePrivate getPrivate(){return privateInfo;}
    
    public void getProfileBin(LittleEndianDataOutputStream os, boolean isNullPrivateInfo) throws IOException{        
        // public info
        Utils.i().writeBigString(os, userID);
        Utils.i().writeBigString(os, name);
        Utils.i().writeBigString(os, companyName);
        os.writeInt(userLevel);
        if(allianceID==-1){
            // not have alliance => write flag true to describe object null
            os.writeBoolean(false);
        } else{
            SocialAlliance sa = GoldIncUserMng.i().getAlliance(allianceID);
            os.writeBoolean(true);
            sa.writeData(os);
        }
        
        // private info
        if(isNullPrivateInfo){
            os.writeBoolean(false);
        } else{
            if(privateInfo==null){
                os.writeBoolean(false);
            } else{
                os.writeBoolean(true);
                privateInfo.writeResponseData(os);
            }
        }
    }

    public void writeData(LittleEndianDataOutputStream os) throws IOException{
        Utils.i().writeBigString(os, userID);
        Utils.i().writeBigString(os, name);
        Utils.i().writeBigString(os, companyName);
        os.writeInt(userLevel);
        os.writeLong(allianceID);
        if(privateInfo==null){
            os.writeBoolean(false);
        } else{
            os.writeBoolean(true);
            privateInfo.writeData(os);
        }
    }
    
    public void readData(GoldIncUser user, LittleEndianDataInputStream is) throws IOException{
        userID = Utils.i().readBigString(is);
        name = Utils.i().readBigString(is);
        companyName = Utils.i().readBigString(is);
        userLevel = is.readInt();
        allianceID = is.readLong();
        if(is.readBoolean()){
            privateInfo = new ProfilePrivate(user);
            privateInfo.readData(is);
        }
        
    }
    
}
