/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ctmlab.game.data.model.base;

import com.ctmlab.game.network.manager.WSGameDefine;
import com.ctmlab.util.Utils;
import com.dp.db.DBDataPostgres;
import com.dp.db.DataFieldType;
import com.google.common.io.LittleEndianDataOutputStream;
import java.io.IOException;

/**
 *
 * @author hhtri
 */
public class GameMessageBase extends DBDataPostgres{
        
    @DataFieldType(primaryKey = true, autoIncrease = true)
    public long id;
    public String sender;
    public String receiver;
    public String subject;
    public String content;
    public byte mType;
    public byte mState;
    public long mTime;
    
    public GameMessageBase(){}
    
    public GameMessageBase(String sender, String receiver, 
            String sub, String content, byte type){
        this.sender = sender;
        this.receiver = receiver;
        this.subject = sub;
        this.content = content;
        mType = type;
        mState = (byte)WSGameDefine.UNREAD_STATE_MSG;
        mTime = System.currentTimeMillis();
        id = 0;
    }
       
    public void writeData(LittleEndianDataOutputStream os) throws IOException{
        os.writeByte(mType);
        os.writeLong(id);
        Utils.i().writeBigString(os, sender);
        Utils.i().writeBigString(os, receiver);
        Utils.i().writeBigString(os, subject);
        Utils.i().writeBigString(os, content);
        os.writeByte(mState);
        os.writeLong(mTime);
    }
}