/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ctmlab.game.data.builder;

import com.ctmlab.game.data.model.RegistryAccount;
import com.dp.db.DB;
import java.sql.SQLException;

/**
 *
 * @author hhtri
 */
public class RegistryAccountBuilder {
    private String id;
    private String email;
    private String password;
    private String companyName;
    public RegistryAccountBuilder(){}

    /**
     * @param id the id to set
     * @return 
     */
    public RegistryAccountBuilder setId(String id) {
        this.id = id;
        return this;
    }

    /**
     * @param email the email to set
     * @return 
     */
    public RegistryAccountBuilder setEmail(String email) {
        this.email = email;
        return this;
    }

    /**
     * @param password the password to set
     * @return 
     */
    public RegistryAccountBuilder setPassword(String password) {
        this.password = password;
        return this;
    }

    /**
     * @param companyName the companyName to set
     * @return 
     */
    public RegistryAccountBuilder setCompanyName(String companyName) {
        this.companyName = companyName;
        return this;
    }
    
    public RegistryAccount build(){
        RegistryAccount rs = new RegistryAccount(id, email, password, companyName);
        return rs;
    }
    
    public RegistryAccount buildAndSave() throws SQLException{
        RegistryAccount ra = build();
        if(ra.save(DB.i().con())<0){return null;}
        return ra;
    }
    
}
