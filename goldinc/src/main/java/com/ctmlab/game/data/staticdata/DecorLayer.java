/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ctmlab.game.data.staticdata;

import com.ctmlab.game.data.model.GameObjectMap;
import com.google.common.io.LittleEndianDataInputStream;
import com.google.common.io.LittleEndianDataOutputStream;
import java.io.IOException;
import java.util.ArrayList;

/**
 *
 * @author hhtri
 * not using
 */
public class DecorLayer implements DataStreamListener{
    public byte type;
    public byte minZoom;
    public byte maxZoom;
    public ArrayList<GameObjectMap> data;
    
    public DecorLayer(){
        type = 0;
        minZoom = 1;
        maxZoom = 10;
        data = new ArrayList<>();
    }
    
    public DecorLayer(byte type){
        this.type = type;
        minZoom = 1;
        maxZoom = 10;
        data = new ArrayList<>();
    }
    
    public void addItem(GameObjectMap item){
        data.add(item);
    }

    @Override
    public void writeData(LittleEndianDataOutputStream os) throws IOException {
        os.writeByte(type);
        os.writeByte(minZoom);
        os.writeByte(maxZoom);
        os.writeInt(data.size());
        for(GameObjectMap o:data){
            o.writeDecorData(os);
        }
    }

    @Override
    public void readData(LittleEndianDataInputStream is) throws IOException {
        type = is.readByte();
        minZoom = is.readByte();
        maxZoom = is.readByte();
        int len = is.readInt();
        int i = 0;
        data = new ArrayList<>();
        while(i<len){
            GameObjectMap obj = new GameObjectMap();
            obj.readDecorData(is);
            data.add(obj);
            i++;
        }
    }

    @Override
    public long calDataOnRAM() {
        long rs = 3;
        for(GameObjectMap o:data){
            rs+=o.capacityOnRAM();
        }
        return rs;
    }
}
