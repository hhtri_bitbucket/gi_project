/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ctmlab.game.data.model;

import com.ctmlab.game.manager.GoldIncUserMng;
import com.ctmlab.game.network.manager.WSGameDefine;
import com.ctmlab.util.Utils;
import com.dp.db.DB;
import com.dp.db.DBDataPostgres;
import com.dp.db.DataFieldType;
import com.google.common.io.LittleEndianDataOutputStream;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author hhtri
 */
public class SocialAlliance extends DBDataPostgres {

    @DataFieldType(primaryKey = true, autoIncrease = true)
    public long id;
    @DataFieldType(uniqueKey = true)
    public String name = "";
    public String leader = "";
    @DataFieldType(mapValueClass = String.class)
    public ArrayList<String> coLeaders;
    public int numberOfMember;
    public int rank;
    public long created;

    @DataFieldType(ignore = true)
    private ArrayList<String> mMembers = new ArrayList<>();

    public SocialAlliance() {
        this.leader = "";
        this.name = "";
        coLeaders = new ArrayList<>();
        numberOfMember = 1;
        rank = -1;
        created = System.currentTimeMillis();
        id= 0;
    }

    public SocialAlliance(String leader, String name) {
        this.leader = leader;
        this.name = name;
        coLeaders = new ArrayList<>();
        numberOfMember = 1;
        rank = 0;
        created = System.currentTimeMillis();
        id = 0;
    }

    public void saveData() throws SQLException {
        this.save(DB.i().con());
        getMemberInRam();
    }

    public int addCoLeader(String userID) throws SQLException {
        if (coLeaders.size() < WSGameDefine.ALLIANCE_COLEADER_LIMIT) {
            getMemberInRam();
            boolean flag = false;
            for (String member : mMembers) {
                if (member.equals(userID)) {
                    flag = true;
                    break;
                }
            }
            if (flag) {
                coLeaders.add(userID);
                saveData();
                return 0;
            }
        }
        return -1;
    }

    public void getMemberInRam() {
        mMembers = GoldIncUserMng.i().getUserInAlliance(this);
    }

    public ArrayList<String> getMembers() {
        return mMembers;
    }

    public void writeData(LittleEndianDataOutputStream os) throws IOException {
        getMemberInRam();
        os.writeLong(id);
        Utils.i().writeBigString(os, name);
        os.writeInt(rank);
        Utils.i().writeBigString(os, leader);
        os.writeInt(coLeaders.size());
        for (String l : coLeaders) {
            Utils.i().writeBigString(os, l);
        }
        os.writeInt(numberOfMember);
        if(!mMembers.isEmpty()) {
            os.writeBoolean(true);
            os.writeInt(mMembers.size());
            for (String member : mMembers) {
               Utils.i().writeBigString(os, member);
            }
        } else {
            os.writeBoolean(false);
        }
    }
}
