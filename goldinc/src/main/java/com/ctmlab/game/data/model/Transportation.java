/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ctmlab.game.data.model;

import com.ctmlab.game.data.pojo.TradeTransaction;
import com.ctmlab.game.data.staticdata.PointInt;
import com.ctmlab.game.data.staticdata.PointShort;
import com.ctmlab.game.goldincenum.ENUM_UNIT_TYPE;
import com.dp.db.DB;
import com.dp.db.DataFieldType;
import com.dp.db.Func;
import com.google.common.io.LittleEndianDataInputStream;
import com.google.common.io.LittleEndianDataOutputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Map;
import java.util.TreeMap;

/**
 *
 * @author hhtri
 */
public class Transportation extends GameObjectMap {

    public long startTime;
    public long endTime;
    public long srcID;
    public long destID;
    public int capacity;
    public int level;
    public String jsonTransaction;
   
    @DataFieldType(ignore = true)
    public TradeTransaction tradeTransaction = null;

    @DataFieldType(ignore = true)
    public ArrayList<PointShort> path = null;

    public Transportation() {
        startTime = endTime = srcID = destID = -1;
        capacity = 1000;
        level = 1;
        jsonTransaction = "";
    }

    public void setInfo(GameObjectMap obj, long src, String user){
        id_r = obj.id_r;
        id_z = obj.id_z;
        xPos = obj.xPos;
        yPos = obj.yPos;
        mType = (byte) ENUM_UNIT_TYPE.TRANSPORTATION.getCode();
        srcID = src;
        destID = obj.id;
        userID = user;
    }
    
    public void setTransaction(TradeTransaction transacion){
        tradeTransaction = transacion;
    }
    
    @Override
    public void getPathFinding(){
        path = new ArrayList<>();
    }
    
    @Override
    public void setPathFinding(ArrayList<PointShort> path){
        this.path = path;
    }
        
//    public void unloadResourceNoSave(){
//        if(tradeTransaction!=null){
//            tradeTransaction.unload();
//        }
//    }
    
//    public void unloadResource(){
//        if(tradeTransaction!=null){
//            tradeTransaction.unload();
//        }
//        saveData();
//    }
    
    public void setTimes(long start, long end){
        startTime = start;
        endTime = end;
    }
    
    public TradeTransaction getTransaction(){
        return tradeTransaction;
    }
    
    public void parseJson(){
        tradeTransaction = Func.gson.fromJson(jsonTransaction, TradeTransaction.class);
    }

    @Override
    public int saveData() {
        jsonTransaction = Func.json(tradeTransaction);
        try {
            return this.save(DB.i().con());
        } catch (Exception ex) {
            return -2;
        }
    }

    @Override
    public void writeData(LittleEndianDataOutputStream os) throws IOException{
        super.writeData(os);
        os.writeLong(startTime);
        os.writeLong(endTime);
        os.writeLong(srcID);
        os.writeLong(destID);
        os.writeInt(capacity);
        os.writeInt(level);
        if(tradeTransaction!=null){
            TreeMap<Byte, Long> container = tradeTransaction.getLstTrade();
            os.writeInt(container.size());
            for(Map.Entry<Byte, Long> e:container.entrySet()){
                os.writeByte(e.getKey());
                os.writeLong(e.getValue());
            }
        } else{
            os.writeInt(0);
        }
        writePathFinding(os, path);
    }
    
    @Override
    public void readData(LittleEndianDataInputStream is) throws IOException{
        super.readData(is);
        startTime = is.readLong();
        endTime = is.readLong();
        srcID = is.readLong();
        destID = is.readLong();
        capacity = is.readInt();
        level = is.readInt();
        int len = is.readInt();
        if(len>0){
            tradeTransaction = new TradeTransaction();
            TreeMap<Byte, Long> container = tradeTransaction.getLstTrade();
            int i = 0;
            while(i++<len){
                container.put(is.readByte(), is.readLong());
            }
        }
        if(is.readBoolean()){
            len = is.readInt();
            int i = 0;
            path = new ArrayList<>();
            while(i++<len){
                path.add(new PointShort(is.readShort(), is.readShort()));
            }
        }
    }
}
