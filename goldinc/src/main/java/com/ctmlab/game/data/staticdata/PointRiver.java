/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ctmlab.game.data.staticdata;

/**
 *
 * @author hhtri
 */
public class PointRiver {
    public PointInt startPos;
    public PointInt rootPos;
    public PointInt outPos;
    public PointInt worldPos;
    public PointRiver(){
        startPos = new PointInt();
        rootPos = new PointInt();
        outPos = new PointInt();
        worldPos = new PointInt();
    }
    public PointRiver(PointInt rootPos, PointInt startPos, PointInt outPos, PointInt worldPos){
        this.rootPos = rootPos;
        this.startPos = startPos;
        this.outPos = outPos;
        this.worldPos = worldPos;
    }
}