/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ctmlab.game.data.pojo;

import com.ctmlab.game.data.staticdata.PointInt;
import com.ctmlab.game.data.staticdata.PointShort;

import java.util.ArrayList;
import java.util.TreeMap;

/**
 *
 * @author hhtri
 */
public class TradeTransaction {
    
    public static final int ACTION_BUY = 1;
    public static final int ACTION_SELL = 2;
    public static final int ACTION_TRANS_TO_CLAIM = 3;
    public static final int ACTION_TRANS_FUEL = 4;
        
    public TreeMap<Byte, Long> lstTrade;
    public ArrayList<Long> lstEquipment;
    public ArrayList<Long> lstWorker;
    public TreeMap<Byte, Long> lstResult;
    public ArrayList<PointShort> path;
    public boolean canTransport;
    public int typeTranstport;
    public long truckID = -1;
    
    public TradeTransaction(){
        lstTrade = new TreeMap();
        lstResult = new TreeMap();
        lstEquipment = new ArrayList();
        lstWorker = new ArrayList();
        path = new ArrayList();
        canTransport = false;
        typeTranstport = -1;
        truckID = -1;
    }
    
    public void unload(){
       setResultTrans(false);
       lstTrade.clear();
       lstResult.clear();
       lstEquipment.clear();
       lstWorker.clear();
       path.clear();
    }
    
    public void setEquipment(ArrayList<Long> w){
        lstEquipment = w;
    }
    
    public ArrayList<Long> getlstEquipment(){
        return lstEquipment;
    }
    
    public void setWorker(ArrayList<Long> w){
        lstWorker = w;
    }
    
    public ArrayList<Long> getlstWorker(){
        return lstWorker;
    }

    public TreeMap<Byte, Long> getLstTrade() {
        return lstTrade;
    }

    public TradeTransaction setLstTrade(TreeMap<Byte, Long> lstTrade) {
        this.lstTrade = lstTrade;
        return this;
    }

    public TreeMap<Byte, Long> getLstResult() {
        return lstResult;
    }

    public TradeTransaction setLstResult(TreeMap<Byte, Long> lstResult) {
        this.lstResult = lstResult;
        return this;
    }

    public void setResultTrans(boolean resultTrans) {
        this.canTransport = resultTrans;
    }
}
