/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ctmlab.game.data.staticdata.gameobjectinfo;

import com.ctmlab.game.goldincenum.ENUM_UNIT_TYPE;
import com.google.common.io.LittleEndianDataInputStream;
import com.google.common.io.LittleEndianDataOutputStream;
import com.google.gson.JsonObject;
import java.io.IOException;

/**
 *
 * @author hhtri
 */
public class GameObjectInfo {
    public byte mType;
    public short width;
    public short height;
    
    public GameObjectInfo(){
        width = height = 0;
    }
    
    public GameObjectInfo(byte mType){
        this.mType = mType;
        width = height = 0;
    }
    
    public void writeData(LittleEndianDataOutputStream os) throws IOException{
        os.writeByte(mType);
        os.writeShort(width);
        os.writeShort(height);
    }
    public void readData(LittleEndianDataInputStream is) throws IOException{
        width = is.readShort();
        height = is.readShort();    
    }
    
    public void readData(JsonObject obj) throws IOException{
        mType = obj.get("mType").getAsByte();
        width = obj.get("width").getAsShort();
        height = obj.get("height").getAsShort();
    }
    
    public static GameObjectInfo readDecorData(LittleEndianDataInputStream is) throws IOException{
        byte t = is.readByte();
        GameObjectInfo clazz = new GameObjectInfo(t);
        clazz.readData(is);
        return clazz;
    }
    
    public int getMaxLevel(){return -1;}
    
    public void initByJson(String json){}
    
    public String fromJson(String json){return "";}
    
    public String toJson(){return "{}";}
    
    public static GameObjectInfo initFromJson(ENUM_UNIT_TYPE type, String json){
        GameObjectInfo rs = null;
        switch(type){
            case HOME_BARRACK:{
                rs = new GameObjectBarrackInfo();
                break;
            }
            case HOME_DEFENSE:{
                rs = new GameObjectDefenseInfo();
                break;
            }
            case HOME_EXPLORATION:{
                rs = new GameObjectExplorationInfo();
                break;
            }
            case HOME_OFFICE:{
                rs = new GameObjectOfficeInfo();
                break;
            }
            case HOME_REFINERY:{
                rs = new GameObjectRefineryInfo();
                break;
            }
            case HOME_TRANSPORTATION:{
                rs = new GameObjectTransportationInfo();
                break;
            }
            case HOME_WALL:{
                rs = new GameObjectWallInfo();
                break;
            }
            case HOME_WEAPON:{
                rs = new GameObjectWeaponDepotInfo();
                break;
            }
        }
        
        if(rs!=null){
            rs.initByJson(json);
        }
        return rs;
    }
    
    public static GameObjectInfo readObjectData(LittleEndianDataInputStream is) throws IOException{
        byte t = is.readByte();
        ENUM_UNIT_TYPE unit = ENUM_UNIT_TYPE.get(t);
        GameObjectInfo clazz = null;
        switch (unit) {
            case HOME_BARRACK:{
                clazz = new GameObjectBarrackInfo();
                break;
            }
            case HOME_DEFENSE:{
                clazz = new GameObjectDefenseInfo();
                break;
            }
            case HOME_EXPLORATION:{
                clazz = new GameObjectExplorationInfo();
                break;
            }
            case HOME_OFFICE:{
                clazz = new GameObjectOfficeInfo();
                break;
            }
            case HOME_REFINERY:{
                clazz = new GameObjectRefineryInfo();
                break;
            }
            case HOME_TRANSPORTATION:{
                clazz = new GameObjectTransportationInfo();
                break;
            }
            case HOME_WALL:{
                clazz = new GameObjectWallInfo();
                break;
            }
            case HOME_WEAPON:{
                clazz = new GameObjectWeaponDepotInfo();
                break;
            }
            default: GameObjectInfo: {
                clazz = new GameObjectInfo(t);
            }
        }
        if(clazz!=null){
            clazz.readData(is);
        }
        return clazz;
    } 
    
    
}
