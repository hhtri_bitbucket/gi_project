/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ctmlab.game.data.model.equipment;

import com.ctmlab.game.data.model.GameObjectMap;
import com.ctmlab.game.data.staticdata.gameobjectinfo.GameObjectInfo;
import com.ctmlab.game.data.staticdata.gameobjectinfo.UpgradeInfo;
import com.ctmlab.game.goldincenum.ENUM_UNIT_TYPE;
import com.dp.db.DB;
import com.google.common.io.LittleEndianDataOutputStream;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;

/**
 *
 * @author hhtri
 */
public class Equipment extends GameObjectMap{
    
    public byte[] data;  
    public boolean inClaim = false;
        
    public Equipment(){}
    
    public byte getType(){return mType;}
    
    public int getLevel(){return 0;}
    
    public void setLevel(int lv){}
    
    public String getUserID(){return userID;}
    
    public void setUserID(String u){userID=u;}
    
    public HashMap<Byte, Long> getGOInfo(){return null;}
    
    public GameObjectInfo getGameObjectInfo(){
        super.refInfo();
        return objInfo;
    }
    
    public ArrayList<HashMap<Byte, Long>> getConfigGameObjectInfo(){return null;}
        
    public ArrayList<UpgradeInfo> getConfigGameObjectUpgradeInfo(){return null;}
    
    public void readData() throws IOException{}
    
    public void writeData() throws IOException {}
    
    @Override
    public int saveData(){try{return saveEx(DB.i().con(), Equipment.class);} catch(SQLException ex){return -2;}}
    
    public void writeData(LittleEndianDataOutputStream os) throws IOException {super.writeData(os);}
            
    public Equipment parseData(){
        Equipment eq = null;
        ENUM_UNIT_TYPE type = ENUM_UNIT_TYPE.get(mType);
        switch(type){
            case CLAIM_DUMPTRUCK:{
                eq = new EquipmentDumptruck();                
                break;
            }
            case CLAIM_EXCAVATOR:{
                eq = new EquipmentExcavator();                
                break;
            }
            case CLAIM_OIL_RIG:{
                eq = new EquipmentOilRig();                
                break;
            }
            case CLAIM_WASH_PLANT:{
                eq = new EquipmentWashPlant();                
                break;
            }
        }
        if(eq!=null){
            eq.id = this.id;
            eq.id_r = this.id_r;
            eq.id_z = this.id_z;
            eq.xPos = this.xPos;
            eq.yPos = this.yPos;
            eq.mType = this.mType;
            eq.userID = this.userID;
            eq.data = this.data;
            try{eq.readData();}catch(IOException ex){}
        }
        return eq;
    }
}
