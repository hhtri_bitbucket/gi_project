/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ctmlab.game.data.model;

import com.ctmlab.game.data.staticdata.PointInt;
import com.ctmlab.game.data.staticdata.PointShort;
import com.ctmlab.game.goldincenum.ENUM_UNIT_TYPE;
import com.ctmlab.game.network.manager.WSGameDefine;
import com.dp.db.DB;
import com.dp.db.DataFieldType;
import com.google.common.io.LittleEndianDataInputStream;
import com.google.common.io.LittleEndianDataOutputStream;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author hhtri
 */
public class Exploration extends GameObjectMap{
    
    public long startTime;
    public long endTime;
    public long destID;
    public byte[] pathResult = WSGameDefine.EMPTY_BYTES;

    @DataFieldType(ignore = true)
    public ArrayList<PointShort> path = null;
    
    public Exploration(){}

    public void setTimes(long start, long end){
        startTime = start;
        endTime = end;
    }
    
    public void setInfo(GameObjectMap obj, String user){
        id_r = obj.id_r;
        id_z = obj.id_z;
        xPos = obj.xPos;
        yPos = obj.yPos;
        mType = (byte) ENUM_UNIT_TYPE.EXPLRATION.getCode();
        destID = obj.id;
        userID = user;
    }

    @Override
    public void setPathFinding(ArrayList<PointShort> path){
        this.path = path;
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        try(LittleEndianDataOutputStream os = new LittleEndianDataOutputStream(baos)){
            int len = path.size();
            os.writeInt(len);
            for(PointShort p:path){
                os.writeShort(p.x);
                os.writeShort(p.y);
            }
            pathResult = baos.toByteArray();
            baos.reset();
        } catch (Exception ex){}
    }

    @Override
    public void getPathFinding(){
        path = new ArrayList<>();
        ByteArrayInputStream bais = new ByteArrayInputStream(pathResult);
        try(LittleEndianDataInputStream is = new LittleEndianDataInputStream(bais)){
            int len = is.readInt();
            int i = 0;
            while(i++<len){path.add(new PointShort(is.readShort(), is.readShort()));}
        } catch (Exception ex){}
    }

    @Override
    public int saveData(){
        try {return this.save(DB.i().con());}catch(SQLException ex){return -2;}
    }

    @Override
    public void writeData(LittleEndianDataOutputStream os) throws IOException{
        super.writeData(os);
        os.writeLong(startTime);
        os.writeLong(endTime);
        os.writeLong(destID);
        writePathFinding(os, path);
    }

    @Override
    public void readData(LittleEndianDataInputStream is) throws IOException{
        super.readData(is);
        startTime = is.readLong();
        endTime = is.readLong();
        destID = is.readLong();
        if(is.readBoolean()){
            int len = is.readInt();
            int i = 0;
            path = new ArrayList<>();
            while(i++<len){
                path.add(new PointShort(is.readShort(), is.readShort()));
            }
        }
    }
}
