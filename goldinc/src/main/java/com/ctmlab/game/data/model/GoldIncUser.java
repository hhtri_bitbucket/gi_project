/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ctmlab.game.data.model;

import com.ctmlab.game.data.model.base.GIData;
import com.ctmlab.game.data.model.building.Building;
import com.ctmlab.game.data.pojo.PlayerProfile;
import com.ctmlab.game.goldincenum.ENUM_RESOURCE_TYPE;
import com.ctmlab.game.network.manager.WSGameDefine;
import com.ctmlab.util.Utils;
import com.dp.db.DB;
import com.dp.db.DataFieldType;
import com.google.common.io.LittleEndianDataInputStream;
import com.google.common.io.LittleEndianDataOutputStream;
import com.google.gson.JsonObject;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.sql.SQLException;

/**
 *
 * @author hhtri
 */
public class GoldIncUser extends GIData {
    
    private static final Object LOCK = new Object();

    public String deviceModel;
    public String deviceName;
    public String ip;
    public String agent;
    public String password;
    public String companyName;
    public int version;
    public long allianceID;

    @DataFieldType(ignore = true)
    public String authCode;

    public byte[] profile = WSGameDefine.EMPTY_BYTES;

    @DataFieldType(ignore = true)
    private PlayerProfile mPlayerProfile = new PlayerProfile();

    @DataFieldType(ignore = true)
    public boolean isLogin = false;
    @DataFieldType(ignore = true)
    public short curZoneID = -1;

    public GoldIncUser() {
        super();
        deviceModel = "";
        deviceName = "";
        ip = "";
        agent = "";
        password = "";
        companyName = "";
        version = 0;
        allianceID = -1;
        authCode = "";
        profile = WSGameDefine.EMPTY_BYTES;
    }
    
    public PlayerProfile parse() {
        mPlayerProfile = new PlayerProfile();
        if (profile != null && profile.length > 0) {
            try (LittleEndianDataInputStream is = new LittleEndianDataInputStream(new ByteArrayInputStream(profile))) {
                mPlayerProfile.readData(this, is);
            } catch (IOException ex) {

            }
            userID = mPlayerProfile.userID;           
        }
        mPlayerProfile.getPrivate().setWalletID("");
        return mPlayerProfile;
    }

    public PlayerProfile getPlayerProfile() {
        return mPlayerProfile;
    }
    
    public void initToTest(){
        mPlayerProfile.getPrivate().initForTesting(userID);
        // put to class manager
    }
    
    public Building setBuildingLevel(byte type, int level){
        return mPlayerProfile.getPrivate().setBuildingLevel(type, level);
    }
    
    public int checkUpgreadeBuilding(byte type){
        return mPlayerProfile.getPrivate().checkUpgreadeBuilding(type);
    }  
    
    public int upgradeBuilding(byte type, LittleEndianDataOutputStream os, StringBuilder sb) throws IOException{
        return mPlayerProfile.getPrivate().upgradeBuilding(type, os, sb);
    }    
    
    public void setCompanyName(String name, boolean flagSaveDB) {
        companyName = name;
        mPlayerProfile.companyName = name;
        setPlayerProfile();
        if (flagSaveDB) {
            try {
                save(DB.i().con());
            } catch (SQLException ex) {
            }
        }
    }

    public boolean setGameName(String name, boolean flagSaveDB) {
        mPlayerProfile.name = name;
        setPlayerProfile();
        if (flagSaveDB) {
            try {
                save(DB.i().con());
                return true;
            } catch (SQLException ex) {
                return false;
            }
        }
        return true;
    }

    public boolean updateWallet(boolean flag, boolean flagSaveDB) {
        mPlayerProfile.getPrivate().setWallet(flag);
        setPlayerProfile();
        if (flagSaveDB) {
            try {
                save(DB.i().con());
                return true;
            } catch (SQLException ex) {
                return false;
            }
        }
        return true;
    }

    public void setUserID(String id) {
        userID = id;
        mPlayerProfile.userID = id;
        setPlayerProfile();
    }

    public void setPassword(String pass) {
        password = pass;
    }

    public void setAllianceID(long id) {
        allianceID = id;
        mPlayerProfile.allianceID = id;
        setPlayerProfile();
    }

    public void setPlayerProfile() {
        setPlayerProfile(mPlayerProfile);
    }

    public void setPlayerProfileTest() {
        mPlayerProfile.getPrivate().getGameResources().genDataTest();
        setPlayerProfile(mPlayerProfile);
    }

    public void setLastEditTime() {
        mPlayerProfile.getPrivate().lastEditTime = System.currentTimeMillis();
    }

    public void setHomeBase(GameObjectMap homebase) {
        mPlayerProfile.getPrivate().setHomeBase(homebase);
    }

    public void addMineFromDB(GameObjectMap claim) {
        mPlayerProfile.getPrivate().addMineFromDB(claim);
    }

    public void addClaim(GameObjectMap claim) {
        mPlayerProfile.getPrivate().addClaim(claim);
    }

    public int addMineTest(GameObjectMap claim) {
        return mPlayerProfile.getPrivate().addMineTest(claim);
    }

    public void setPlayerProfile(PlayerProfile ures) {
        try {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            LittleEndianDataOutputStream os = new LittleEndianDataOutputStream(baos);
            this.mPlayerProfile = ures;
            this.mPlayerProfile.writeData(os);
            this.profile = baos.toByteArray();
            this.userID = ures.userID;
            this.allianceID = ures.allianceID;
            baos.reset();
        } catch (IOException ex) {
        }
    }

    public int payTestMine(Mine item) {
        if (mPlayerProfile.getPrivate().addMineTest(item) == 1) {
            return 1;
        }
        mPlayerProfile.getPrivate().getGameResources().decreaseValue(ENUM_RESOURCE_TYPE.CASH, item.test_price);
        setPlayerProfile();
        try {
            save(DB.i().con());
            return 0;
        } catch (SQLException ex) {
            return 1;
        }
    }
   
    public int payMine(Mine item) {        
        if (mPlayerProfile.getPrivate().addClaim(item) == 1) {
            return 1;
        }
        item.userID = userID;
        mPlayerProfile.getPrivate().getGameResources().decreaseValue(ENUM_RESOURCE_TYPE.CASH, item.price);
        setPlayerProfile();
        try {
            save(DB.i().con());
            item.save(DB.i().con());
        } catch (SQLException ex) {
            return 1;
        }
        return 0;
    }

    // exploration
    public void removeExploration(Exploration id){
        mPlayerProfile.getPrivate().removeExploration(id);
    }
    
    // truck
    public void transportFinish(long idTruck){
        mPlayerProfile.getPrivate().transportFinish(idTruck);
        setPlayerProfile();
        try {
            save(DB.i().con());
        } catch (SQLException ex) {
        }
    }
    
    public void saveData(){
        try {
            setPlayerProfile();
            save(DB.i().con());
        } catch (SQLException ex) {
        }
    }
    
    // using for admin page edit info
    public void getUserInfoAdmin(JsonObject obj) {
        obj.addProperty("userID", userID);
        obj.addProperty("name", mPlayerProfile.name);
        obj.addProperty("password", password);
        obj.addProperty("companyName", companyName);
        obj.addProperty("userLevel", mPlayerProfile.userLevel);
        obj.addProperty("isLogin", isLogin);
        obj.add("resource", mPlayerProfile.getPrivate().getGameResources().getJsonObjectAdmin());
    }

    public void setUserInfoAdmin(JsonObject obj) throws Exception {
        obj.addProperty("userID", userID);
        obj.addProperty("name", mPlayerProfile.name);
        obj.addProperty("password", password);
        obj.addProperty("companyName", companyName);
        obj.addProperty("userLevel", mPlayerProfile.userLevel);
        obj.addProperty("isLogin", isLogin);
        mPlayerProfile.name = obj.get("name").getAsString();
        companyName = obj.get("companyName").getAsString();
        mPlayerProfile.companyName = companyName;
        mPlayerProfile.userLevel = obj.get("userLevel").getAsInt();
        String pass = obj.get("password").getAsString();
        if (!password.equals(password)) {
            password = Utils.i().sha256(pass);
        }
        mPlayerProfile.getPrivate().getGameResources().setResourceAdmin(obj.getAsJsonObject("resource"));
        setPlayerProfile();
        if (save(DB.i().con()) < 0) {
            throw new Exception("Can not save to database.");
        }
    }
}
