/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ctmlab.game.data.staticdata;

import com.ctmlab.game.data.ctmon.CTMONDataFieldType;
import com.ctmlab.game.manager.StaticDataMng;
import com.ctmlab.util.Utils;
import com.google.common.io.LittleEndianDataInputStream;
import com.google.common.io.LittleEndianDataOutputStream;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map.Entry;
import java.util.TreeMap;

/**
 *
 * @author hhtri
 */
public class Region implements DataStreamListener{
    public short id;
    public String name;
    public ArrayList<Integer> pos;
    public TreeMap<Short, Zone> zones;

    private byte[][] matrix = null;

    public Region(){
        id = 0;
        pos = new ArrayList();
        zones = new TreeMap();
        name = "";
    }

    public Region(short id, int posX, int posY, String name){
        this.id = id;
        pos = new ArrayList();
        pos.add(posX);
        pos.add(posY);
        zones = new TreeMap<>();
        this.name = name;
    }
    
    public Region(byte[] data){
        _readData(data);
    }

    private Zone getZone(PointInt p) {
        for (Zone z : zones.values()) {
            if (p.x >= z.x() && p.x < (z.x() + z.getWidth()) && p.y >= z.y() && p.y < (z.y() + z.getHeight())) {
                return z;
            }
        }
        return null;
    }



    public byte[][] getInnerMatrix(Zone z1, Zone z2){
        ArrayList<Integer> zoneSize = StaticDataMng.i().getZoneSize();
        int startX = (z1.x()>z2.x())?z2.x():z1.x();
        int endX =  ((z1.x()>z2.x())?z1.x():z2.x()) + zoneSize.get(0);
        int startY = (z1.y()>z2.y())?z2.y():z1.y();
        int endY =  ((z1.y()>z2.y())?z1.y():z2.y()) + zoneSize.get(1);
        int w = Math.abs(startX-endX);
        int h = Math.abs(startY-endY);
        byte[][] rs = new byte[w][h];
        for(int x=startX, i=0; x<endX; x+=zoneSize.get(0), i+=zoneSize.get(0)){
            for(int y=startY, j=0; y<endY; y+=zoneSize.get(1), j+=zoneSize.get(1)){
                Zone zTemp = getZone(new PointInt(x, y));
                if(zTemp!=null){
                    byte[][] matrix = zTemp.getRawMatrix();
                    for(int m=0;m< zoneSize.get(0); m++){
                        for(int l=0;l< zoneSize.get(1); l++){
                            rs[i+m][j+l]=matrix[m][l];
                        }
                    }
                    matrix = null;
                }
            }
        }
        return rs;
    }

    public byte[][] getMatrix(){
        for (Zone z : zones.values()) {
            byte[][] temp = z.getRawMatrix();
            int x = z.x();
            int y = z.y();
            for(int i=0; i<z.getWidth(); i++){
                for(int j=0; j<z.getHeight(); j++){
                    matrix[x+i][y+j]=temp[i][j];
                }
            }
        }
        return matrix;
    }

    public Zone getRandomZone(short order){
        Short[] temp;
        synchronized(zones){
            temp = zones.keySet().toArray(new Short[0]);
        }
        return zones.get(temp[order]);
    }
    
    public Zone getZone(short id){
        return zones.get(id);
    }
    
    public void addZones(TreeMap<Short, Zone> hZone){
        if(hZone==null||hZone.isEmpty()){return;}
        ArrayList<Integer> zoneSize = StaticDataMng.i().getZoneSize();
        for(Entry<Short, Zone> e:hZone.entrySet()){
            Zone z = e.getValue();            
            z.syncListPosExisted(zoneSize.get(0), zoneSize.get(1));
            synchronized(zones){
                zones.put(e.getKey(), z);
            }
        }
    }

    
    public HashSet<String> lstUserID(){
        HashSet<String> hs = new HashSet<>();
        zones.values().forEach((z) -> {
            hs.addAll(z.getListUserID());
        });        
        return hs;
    }
    
    private void _readData(byte[] data){
        ByteArrayInputStream iss = new ByteArrayInputStream(data);
        try(LittleEndianDataInputStream is=new LittleEndianDataInputStream(iss)){
            readData(is);
        } catch(Exception ex){}
    }

    @Override
    public void writeData(LittleEndianDataOutputStream os) throws IOException {
        os.writeShort(id);
        Utils.i().writeBigString(os, name);
        os.writeInt(pos.size());
        for(int p:pos){
            os.writeInt(p);
        }
        os.writeInt(zones.size());
        for(Entry<Short, Zone> z:zones.entrySet()){
            os.writeShort(z.getKey());
            z.getValue().writeData(os);
        }
    }

    @Override
    public void readData(LittleEndianDataInputStream is) throws IOException {
        id = is.readShort();
        name = Utils.i().readBigString(is);
        int len = is.readInt();
        int i = 0;
        pos = new ArrayList<>();
        while(i++<len){
            pos.add(is.readInt());
        }
        len = is.readInt();
        i = 0;
        zones = new TreeMap<>();
        while(i++<len){
            short key = is.readShort();
            Zone z = new Zone();
            z.readData(is);
            zones.put(key, z);
        }
    }

    @Override
    public long calDataOnRAM() {
        long rs =  2+name.length()+(pos.size())*4;
        for(Zone z:zones.values()){
            rs += (2 + z.calDataOnRAM());
        }
        return rs;
    }
    
}
