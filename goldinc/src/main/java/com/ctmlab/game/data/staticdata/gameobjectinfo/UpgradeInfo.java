/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ctmlab.game.data.staticdata.gameobjectinfo;

import com.ctmlab.game.goldincenum.ENUM_RESOURCE_TYPE;
import com.ctmlab.game.goldincenum.ENUM_UNIT_TYPE;
import com.google.common.io.LittleEndianDataInputStream;
import com.google.common.io.LittleEndianDataOutputStream;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import java.io.IOException;
import java.util.Map.Entry;
import java.util.TreeMap;

/**
 *
 * @author hhtri
 */
public class UpgradeInfo {
    public TreeMap<Byte, Integer> building_level;
    public TreeMap<Byte, Long> resources;
    public long timeUpgrade;
    
    public UpgradeInfo(){
        building_level = new TreeMap<>();
        resources = new TreeMap<>();
    }
    
    public static UpgradeInfo parse(JsonObject obj){
        UpgradeInfo rs = new UpgradeInfo();
        rs.timeUpgrade = obj.get("timeUpgrade").getAsLong();
        JsonObject temp = obj.get("building_level").getAsJsonObject();
        for(Entry<String, JsonElement> e:temp.entrySet()){
            rs.building_level.put(Byte.valueOf(e.getKey()), e.getValue().getAsInt());
        }
        temp = obj.get("resources").getAsJsonObject();
        for(Entry<String, JsonElement> e:temp.entrySet()){
            rs.resources.put(Byte.valueOf(e.getKey()), e.getValue().getAsLong());
        }
        return rs;
    }
    
    public JsonObject getJsonObject(){
        JsonObject obj = new JsonObject();
        obj.addProperty("timeUpgrade", timeUpgrade);
        JsonObject oBuilding = new JsonObject();
        JsonObject oRes = new JsonObject();
        obj.add("building_level", oBuilding);
        obj.add("resources", oRes);
        for(Entry<Byte, Integer> e:building_level.entrySet()){
            oBuilding.addProperty(ENUM_UNIT_TYPE.getString(e.getKey()), e.getValue());
        }
        for(Entry<Byte, Long> e:resources.entrySet()){
            oRes.addProperty(ENUM_RESOURCE_TYPE.getString(e.getKey()), e.getValue());
        }
        return obj;
    }
    
    public void readData(LittleEndianDataInputStream is) throws IOException{
        int len = is.readInt();
        int i = 0;
        building_level = new TreeMap<>();
        while(i++<len){building_level.put(is.readByte(), is.readInt());}
        
        len = is.readInt();
        i = 0;
        resources = new TreeMap<>();
        while(i++<len){resources.put(is.readByte(), is.readLong());}
        timeUpgrade = is.readLong();
    }
    
    public void writeData(LittleEndianDataOutputStream os) throws IOException{
        os.writeInt(building_level.size());
        for(Entry<Byte, Integer> e:building_level.entrySet()){
            os.writeByte(e.getKey().intValue());
            os.writeInt(e.getValue());
        }
        os.writeInt(resources.size());
        for(Entry<Byte, Long> e:resources.entrySet()){
            os.writeByte(e.getKey().intValue());
            os.writeLong(e.getValue());
        }
        os.writeLong(timeUpgrade);
    }
    
}
