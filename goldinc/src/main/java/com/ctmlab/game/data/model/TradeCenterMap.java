/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ctmlab.game.data.model;

import com.ctmlab.game.goldincenum.ENUM_UNIT_TYPE;

/**
 *
 * @author hhtri
 */
public class TradeCenterMap extends GameObjectMap{
    public TradeCenterMap(){
        super();
        mType = (byte) ENUM_UNIT_TYPE.TRADE_CENTER.getCode();
    }
}
