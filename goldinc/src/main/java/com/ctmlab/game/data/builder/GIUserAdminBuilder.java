/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ctmlab.game.data.builder;

import com.ctmlab.game.data.model.GIUserAdmin;
import com.ctmlab.game.data.model.GoldIncUser;
import com.ctmlab.game.data.pojo.PlayerProfile;
import com.ctmlab.game.network.manager.WSGameDefine;
import com.ctmlab.util.Utils;
import com.dp.db.DB;
import com.dp.db.DataFieldType;
import java.sql.SQLException;

/**
 *
 * @author hhtri
 */
public class GIUserAdminBuilder {
    public String username;
    public String password;
    public long created;
    public long lastlogin;
    public String createdby;
    public GIUserAdminBuilder(){}

    public GIUserAdminBuilder setUsername(String username) {
        this.username = username;
        return this;
    }
    public GIUserAdminBuilder setPassword(String password) {
        this.password = password;
        return this;
    }
    public GIUserAdminBuilder setCreatedBy(String username) {
        this.createdby = username;
        return this;
    }
    public GIUserAdmin build(){
        GIUserAdmin u = new GIUserAdmin(username, password, createdby);
        return u;
    }
    
    public GIUserAdmin buildAndSave() throws SQLException{
        GIUserAdmin u = build();
        if(u.save(DB.i().con())<0){return null;}
        return u;
    }
}
