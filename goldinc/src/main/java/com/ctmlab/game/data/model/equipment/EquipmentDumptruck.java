/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ctmlab.game.data.model.equipment;

import com.ctmlab.game.data.staticdata.gameobjectinfo.GameObjectOfficeInfo;
import com.ctmlab.game.data.staticdata.gameobjectinfo.UpgradeInfo;
import com.ctmlab.game.goldincenum.ENUM_UNIT_TYPE;
import com.google.common.io.LittleEndianDataInputStream;
import com.google.common.io.LittleEndianDataOutputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

/**
 *
 * @author hhtri
 */
public class EquipmentDumptruck extends Equipment{
    
    public int level;
    public long startUpgradeTime;
    public long endUpgradeTime;
    
    public EquipmentDumptruck() {
        mType = (byte)ENUM_UNIT_TYPE.CLAIM_DUMPTRUCK.getCode();
        level = 1;
        startUpgradeTime = 0;
        endUpgradeTime = 0;
    }
    
    public EquipmentDumptruck(String user, short idRegion, short idZone, short x, short y) {
        
        id_r = idRegion;
        id_z = idZone;
        xPos = x;
        yPos = y;
        
        userID = user;        
        mType = (byte)ENUM_UNIT_TYPE.CLAIM_DUMPTRUCK.getCode();
        
        level = 1;
        startUpgradeTime = 0;
        endUpgradeTime = 0;
    }
    
    @Override
    public int getLevel(){        
        return level;
    }
    
    @Override
    public void setLevel(int lv){
        level = lv;
        startUpgradeTime = endUpgradeTime = 0;
        try {
            EquipmentDumptruck.this.writeData();
        } catch (IOException ex) {
        }
    }        
    
    @Override
    public HashMap<Byte, Long> getGOInfo(){
        super.getGameObjectInfo();
        GameObjectOfficeInfo oInfo = (GameObjectOfficeInfo)objInfo;
        if(oInfo.info.size()<level){return null;}
        return oInfo.info.get(level-1);
    }
    
    @Override
    public ArrayList<HashMap<Byte, Long>> getConfigGameObjectInfo(){
        super.getGameObjectInfo();
        GameObjectOfficeInfo oInfo = (GameObjectOfficeInfo)objInfo;
        return oInfo.info;
    }
        
    @Override
    public ArrayList<UpgradeInfo> getConfigGameObjectUpgradeInfo(){
        super.getGameObjectInfo();
        GameObjectOfficeInfo oInfo = (GameObjectOfficeInfo)objInfo;
        return oInfo.upgrade_require;
    }
        
    @Override
    public void readData() throws IOException{
        if(data!=null){
            try(LittleEndianDataInputStream is = new LittleEndianDataInputStream(new ByteArrayInputStream(data))){
                level = is.readInt();
                startUpgradeTime = is.readLong();
                endUpgradeTime = is.readLong();
            }
        }
            
    }
    
    @Override
    public void writeData() throws IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        try(LittleEndianDataOutputStream os = new LittleEndianDataOutputStream(baos)){
            os.writeInt(level);
            os.writeLong(startUpgradeTime);
            os.writeLong(endUpgradeTime);
        } catch(Exception ex){}
        data = baos.toByteArray();
        super.saveData();
    }
    
    // response to client
    @Override
    public void writeData(LittleEndianDataOutputStream os) throws IOException {
        super.writeData(os);
        os.writeInt(level);
        os.writeLong(startUpgradeTime);
        os.writeLong(endUpgradeTime);
    }
}