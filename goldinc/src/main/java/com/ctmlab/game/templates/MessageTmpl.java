/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ctmlab.game.templates;

/**
 *
 * @author hhtri
 */
public class MessageTmpl {
    public static String inviteSubject = "";
    public static String inviteContent = "";
    
    public static String explorationSubject = "Test mine finish";
    public static String explorationContent = "Below is your report from location #r:#z:[#p]";
    
    public static String tradeResourceSubject = "Transport resource finished";
    public static String tradeResourceContent = "Below is your report from location #r:#z:[#p]";
    
    public static String transportEquipSubject = "Transport equipment and worker finished";
    public static String transportEquipContent = "Below is your report from location #r:#z:[#p]";
    
    public static String transportFuelSubject = "Transport fuel to claim";
    public static String transportFuelContent = "Below is your report from location #r:#z:[#p]\n"
            + "Number of fuel to this claim is #fuel.";
}
