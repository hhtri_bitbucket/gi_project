/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ctmlab.game.threads;

import com.ctmlab.game.data.builder.GameMessageBuilder;
import com.ctmlab.game.data.model.GoldIncUser;
import com.ctmlab.game.data.model.base.GameMessageBase;
import com.ctmlab.game.data.model.building.Building;
import com.ctmlab.game.goldincenum.ENUM_MESSAGE;
import com.ctmlab.game.goldincenum.ENUM_UNIT_TYPE;
import com.ctmlab.game.goldincenum.GI_SERVER_CMD;
import com.ctmlab.game.hadoop.HadoopProvider;
import com.ctmlab.game.manager.GoldIncUserMng;
import com.ctmlab.game.network.manager.WSGameDefine;
import com.ctmlab.game.network.manager.WSServiceServer;
import com.dp.db.Func;
import com.google.common.io.LittleEndianDataOutputStream;
import java.io.ByteArrayOutputStream;
import java.util.Map.Entry;
import java.util.SortedMap;
import java.util.TreeMap;

/**
 *
 * @author hhtri
 */
public class BuildingUpgradeThread extends Thread {

    private static final Object LOCK = new Object();
    private static final Object LOCK_WAIT = new Object();
    private static BuildingUpgradeThread inst;

    public static BuildingUpgradeThread i() {
        synchronized (LOCK) {
            if (inst == null) {
                inst = new BuildingUpgradeThread();
            }
            return inst;
        }
    }    
    
    private class BuildingUpgrade{
        private final long startTime;
        private final long endTime;
        private final byte mType;
        private final int levelUp;
        private final String owner;
        public BuildingUpgrade(String userID, byte type, int lvl, long start, long end){
            owner = userID;
            mType = type;
            levelUp = lvl;
            startTime = start;
            endTime = end;
        }
        public String getOwner(){return owner;}
        public byte getType(){return mType;}
        public int getLevelUp(){return levelUp;}
        public long getStartTime(){return startTime;}
        public long getEndTime(){return endTime;}
    }
    
    private final TreeMap<Long, BuildingUpgrade> mItems = new TreeMap<>();

    private boolean mRunning = false;
    
    private BuildingUpgradeThread() {}

    private void debug(String data) {HadoopProvider.i().writeLog("explorationthread", data);}

    private void mSleep(int time) {
        try {synchronized (LOCK_WAIT){LOCK_WAIT.wait(time);}}catch(InterruptedException ex) {}
    }
    
    public void addToBuff(String userID, byte type, int levelUP, long start, long end){
        BuildingUpgrade item = new BuildingUpgrade(userID, type, levelUP, start, end);
        synchronized(mItems){mItems.put(item.getEndTime(), item);}
    }
    
    private void checkUpgrade(){
        if (mItems.isEmpty()) {return;}
        SortedMap<Long, BuildingUpgrade> temp;
        Entry<Long, BuildingUpgrade>[] eTemp;
        synchronized (mItems) {
            temp = mItems.headMap(System.currentTimeMillis());
            eTemp = temp.entrySet().toArray(WSGameDefine.EMPTY_ENTRY);
        }
        if (!temp.isEmpty()) {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            StringBuilder sb = new StringBuilder();
            sb.append(Func.now()).append("\n");
            try (LittleEndianDataOutputStream os = new LittleEndianDataOutputStream(baos)) {
                for (Entry<Long, BuildingUpgrade> e : eTemp) {
                    baos.reset();
                    BuildingUpgrade item = e.getValue();
                    // update level to owner
                    GoldIncUser user = GoldIncUserMng.i().getUser(item.getOwner());
                    if(user!=null){
                        Building b = user.setBuildingLevel(item.getType(), item.getLevelUp());
                        if(b!=null){
    //                        // send state to client//                        
                            os.writeShort(GI_SERVER_CMD.RES_UPDATE_BUILDING.getCode());
                            os.writeByte(b.getType());
                            b.writeData(os);  
                            
                            sb.append(String.format("Level info [%s]-[%s]\n", user.userID, Func.json(b)));
//                        
                            WSServiceServer.i().sendDataWithID(item.getOwner(), baos.toByteArray());
                            baos.reset();

                            // message to client
                            GameMessageBase gm = new GameMessageBuilder()
                                        .setSender("system")
                                        .setReceiver(item.getOwner())
                                        .setSubject("Building upgrade")
                                        .setContent("Your '" + ENUM_UNIT_TYPE.get(b.getType()).toString() + "' is upgraded to level " + b.getLevel())
                                        .setType(ENUM_MESSAGE.REPORT_TEST_MINE_MSG.getCode())
                                        .buildAndSave();
                            if (gm != null) {
                                // start to add to Thread sender
                                MessageThread.i().addData(user.userID, gm);
                                sb.append(String.format("Sending message to %s wiht all of content [%s]\n", user.userID, Func.json(gm)));
                            }   
                        }
                    }

                    synchronized (mItems) {
                        mItems.remove(e.getKey());
                    }
                }
            } catch (Exception ex) {
                sb.append(String.format("Exception detail:\n%s", Func.toString(ex)));
            }
            debug(sb.toString());
            sb.setLength(0);
        }
    }
    
    public void startMng() {
        start();
    }

    public void stopMng() {
        mRunning = false;
        interrupt();
    }

    @Override
    public void run() {
        mRunning = true;
        while (mRunning) {
            mSleep(1000);
            checkUpgrade();
        }
    }
}
