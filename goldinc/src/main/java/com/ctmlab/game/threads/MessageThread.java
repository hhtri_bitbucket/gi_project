/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ctmlab.game.threads;

import com.ctmlab.game.data.model.GameMessage;
import com.ctmlab.game.data.model.GoldIncUser;
import com.ctmlab.game.data.model.base.GameMessageBase;
import com.ctmlab.game.goldincenum.GI_SERVER_CMD;
import com.ctmlab.game.hadoop.HadoopProvider;
import com.ctmlab.game.manager.GoldIncUserMng;
import com.ctmlab.game.network.client.WSServiceClient;
import com.ctmlab.game.network.manager.WSGameDefine;
import com.ctmlab.game.network.manager.WSServiceServer;
import com.dp.db.Func;
import com.google.common.io.LittleEndianDataOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map.Entry;
import java.util.Queue;

/**
 *
 * @author hhtri
 */
public class MessageThread extends Thread{    
    private static final Object LOCK = new Object();
    private static final Object LOCK_WAIT = new Object();    
    private static MessageThread inst;
    public static MessageThread i() {
        synchronized (LOCK) {
            if (inst == null) {inst = new MessageThread();}
            return inst;
        }
    }        
    private final HashMap<String, Queue<GameMessageBase>> mData = new HashMap();
    private boolean mRunning = false;     
    private MessageThread(){}
    
    private void debug(String data){
        HadoopProvider.i().writeLog("messagesenderthread", data);
    }
    
    private void mSleep(int time){
        try{
            synchronized(LOCK_WAIT){
                LOCK_WAIT.wait(time);
            }
        } catch(InterruptedException ex){}
    }
    
    public void check(){
        
        Entry<String, Queue<GameMessageBase>>[] entries;
        synchronized(LOCK){entries = mData.entrySet().toArray(WSGameDefine.EMPTY_ENTRY);}
        
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        for(Entry<String, Queue<GameMessageBase>> e:entries){
            baos.reset();
            Queue<GameMessageBase> temp = e.getValue();
            if(!temp.isEmpty()){                    
                int len = temp.size();
                int i = 0;
                try (LittleEndianDataOutputStream os = new LittleEndianDataOutputStream(baos)) {
                    os.writeShort(GI_SERVER_CMD.RES_GET_MESSAGE.getCode()); 
                    os.writeInt(temp.size());
                    while(i++<len){
                        GameMessageBase gm = temp.poll();
                        gm.writeData(os);
                    }
                    GoldIncUser user = GoldIncUserMng.i().getUser(e.getKey());
                    byte[] data = baos.toByteArray();
                    debug(String.format("Time %s: send to client %s (%d) with %d messages - %d bytes", 
                            Func.now(), e.getKey(), (user!=null && user.isLogin)?1:0, len, data.length));
                    if(user!=null && user.isLogin){
                        WSServiceServer.i().sendDataWithID(e.getKey(), data);
                    }
                    user = null;
                } catch (IOException ex) {}
            }
        }
        synchronized(mData){
            for(Entry<String, Queue<GameMessageBase>> e:mData.entrySet()){
                e.getValue().clear();
            }
        }
    }
        
    public void addData(String userID, GameMessageBase data){
        Queue<GameMessageBase> q = mData.get(userID);
        if(q==null){
            q = new LinkedList<>();
            synchronized(mData){
                mData.put(userID, q);
            }
        }
        synchronized(q){
            q.add(data);
        }
    }
    
    public void startMsgSenderMng(){
        start();
    }
    
    public void stopMsgSenderMng(){
        mRunning = false;
        interrupt();
    }
    
    @Override
    public void run(){
        mRunning = true;
        while(mRunning){
            check();
            mSleep(3000);
        }
    }
    
}