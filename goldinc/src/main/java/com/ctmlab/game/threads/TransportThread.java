/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ctmlab.game.threads;

import com.ctmlab.game.data.builder.GameMessageBuilder;
import com.ctmlab.game.data.model.Exploration;
import com.ctmlab.game.data.model.GameObjectMap;
import com.ctmlab.game.data.model.GoldIncUser;
import com.ctmlab.game.data.model.Mine;
import com.ctmlab.game.data.model.TradeCenterMap;
import com.ctmlab.game.data.model.Transportation;
import com.ctmlab.game.data.model.base.GameMessageBase;
import com.ctmlab.game.data.pojo.PlayerProfile;
import com.ctmlab.game.data.pojo.TradeTransaction;
import com.ctmlab.game.data.staticdata.Zone;
import com.ctmlab.game.goldincenum.ENUM_MESSAGE;
import com.ctmlab.game.goldincenum.ENUM_RESOURCE_TYPE;
import com.ctmlab.game.goldincenum.GI_SERVER_CMD;
import com.ctmlab.game.hadoop.HadoopProvider;
import com.ctmlab.game.manager.GoldIncUserMng;
import com.ctmlab.game.manager.StaticDataMng;
import com.ctmlab.game.network.manager.WSGameDefine;
import com.ctmlab.game.network.manager.WSServiceServer;
import com.ctmlab.game.templates.MessageTmpl;
import com.dp.db.Func;
import com.google.common.io.LittleEndianDataOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Map.Entry;
import java.util.SortedMap;
import java.util.TreeMap;

/**
 *
 * @author hhtri
 */
public class TransportThread extends Thread {

    private static final Object LOCK = new Object();
    private static final Object LOCK_WAIT = new Object();
    private static TransportThread inst;

    public static TransportThread i() {
        synchronized (LOCK) {
            if (inst == null) {
                inst = new TransportThread();
            }
            return inst;
        }
    }
    private final TreeMap<Long, Exploration> mTreeExploration = new TreeMap<>();
    private final TreeMap<Long, Transportation> mTreeTruck = new TreeMap<>();

    private boolean mRunning = false;

    private TransportThread() {}

    private void debug(String data) {
        HadoopProvider.i().writeLog("explorationthread", data);
    }

    private void mSleep(int time) {
        try {synchronized(LOCK_WAIT){LOCK_WAIT.wait(time);}}
        catch (InterruptedException ex){}
    }

    private void checkExploration() {
        if (mTreeExploration.isEmpty()) {
            return;
        }
        SortedMap<Long, Exploration> temp;
        synchronized (mTreeExploration) {
            temp = mTreeExploration.headMap(System.currentTimeMillis());
        }
        if (!temp.isEmpty()) {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            StringBuilder sb = new StringBuilder();
            sb.append(Func.now()).append("\n");
            try (LittleEndianDataOutputStream os = new LittleEndianDataOutputStream(baos)) {
                for (Entry<Long, Exploration> e : temp.entrySet()) {
                    baos.reset();
                    Exploration expl = e.getValue();
                    String userID = expl.userID;
                    GoldIncUser user = GoldIncUserMng.i().getUser(userID);
                    PlayerProfile p = user.getPlayerProfile();

                    Zone z = StaticDataMng.i().worldMap.getZone(expl.id_z);
                    if (z != null && z.layerGameObject != null) {
                        Mine item = (Mine) z.getObjectMap(expl.destID);
                        // send res mine info
                        // <byte:type><[mine, oil_mine] data><profile public>
                        os.writeShort(GI_SERVER_CMD.RES_MINE_INFO.getCode());
                        os.writeByte(item.mType);
                        item.writeData(os);
                        p.getProfileBin(os, true);
                        user.removeExploration(e.getValue());
                        // start to send
                        byte[] data = baos.toByteArray();
                        WSServiceServer.i().sendDataWithID(userID, data);
                        sb.append(String.format("Sending mine info [%s] with %d bytes\n", userID, data.length));
                        // send message report
                        ArrayList<Integer> posReal = item.getRealLocation();
                        if (posReal != null) {
                            String ct = MessageTmpl.explorationContent;
                            ct = ct.replace("#r", String.valueOf(item.id_r))
                                    .replace("#z", String.valueOf(item.id_z))
                                    .replace("#p", posReal.get(0) + ":" + posReal.get(1));
                            GameMessageBase gm = new GameMessageBuilder()
                                    .setSender("system")
                                    .setReceiver(expl.userID)
                                    .setSubject(MessageTmpl.explorationSubject)
                                    .setContent(ct)
                                    .setType(ENUM_MESSAGE.REPORT_TEST_MINE_MSG.getCode())
                                    .buildAndSave();
                            if (gm != null) {
                                // start to add to Thread sender
                                MessageThread.i().addData(userID, gm);
                                sb.append(String.format("Sending message to %s wiht all of content [%s]\n", userID, Func.json(gm)));
                            }
                        }
                    }
                    p = null;
                    synchronized (mTreeExploration) {
                        mTreeExploration.remove(e.getKey());
                    }
                }
            } catch (Exception ex) {
                sb.append(String.format("Exception detail:\n%s", Func.toString(ex)));
            }
            debug(sb.toString());
            sb.setLength(0);
        }
    }

    private void finishTransResourceTrader(LittleEndianDataOutputStream os, ByteArrayOutputStream baos, StringBuilder sb, Entry<Long, Transportation> e) throws SQLException, IOException {
        Transportation truck = e.getValue();
        String userID = truck.userID;
        GoldIncUser user = GoldIncUserMng.i().getUser(userID);
        PlayerProfile p = user.getPlayerProfile();

        Zone z = StaticDataMng.i().worldMap.getZone(truck.id_z);
        if (z != null && z.layerGameObject != null) {
            TradeCenterMap item = (TradeCenterMap) z.getObjectMap(truck.destID);
            user.transportFinish(truck.id);
            // start to send
            byte[] data = null;
            // send message report
            ArrayList<Integer> posReal = item.getRealLocation();
            if (posReal != null) {
                TradeTransaction transaction = truck.getTransaction();
                String ct = MessageTmpl.tradeResourceContent;
                ct = ct.replace("#r", String.valueOf(item.id_r))
                        .replace("#z", String.valueOf(item.id_z))
                        .replace("#p", posReal.get(0) + ":" + posReal.get(1));

                String content = "\nList of resources " + (transaction.typeTranstport == 1 ? "bought" : "sold") + "\nName: Amount - total price\n";
                TreeMap<Byte, Long> treeTrade = transaction.getLstTrade();
                for (Entry<Byte, Long> i : transaction.getLstResult().entrySet()) {
                    String strTemp = "%s: %d - %d\n";
                    content += String.format(strTemp, ENUM_RESOURCE_TYPE.get((int) i.getKey()).toString(),
                            treeTrade.get(i.getKey()), i.getValue());
                }
                ct += content;
                GameMessageBase gm = new GameMessageBuilder()
                        .setSender("system")
                        .setReceiver(truck.userID)
                        .setSubject(MessageTmpl.tradeResourceSubject)
                        .setContent(ct)
                        .setType(ENUM_MESSAGE.REPORT_TEST_MINE_MSG.getCode())
                        .buildAndSave();
                if (gm != null) {
                    // start to add to Thread sender
                    MessageThread.i().addData(userID, gm);
                    sb.append(String.format("Sending message to %s wiht all of content [%s]\n", userID, Func.json(gm)));
                }

                // send data sell/buy
                baos.reset();
                os.writeShort(GI_SERVER_CMD.RES_UPDATE_PROFILE.getCode());
                user.getPlayerProfile().getProfileBin(os, false);
                data = baos.toByteArray();
                WSServiceServer.i().sendDataWithID(userID, data);
            }
        }
        p = null;
        synchronized(mTreeTruck){mTreeTruck.remove(e.getKey());}
    }
    
    private void finishTransEquip(LittleEndianDataOutputStream os, ByteArrayOutputStream baos, StringBuilder sb, Entry<Long, Transportation> e) throws SQLException, IOException {
        Transportation truck = e.getValue();
        String userID = truck.userID;
        GoldIncUser user = GoldIncUserMng.i().getUser(userID);
        PlayerProfile p = user.getPlayerProfile();
        Zone z = StaticDataMng.i().worldMap.getZone(truck.id_z);
        if (z != null && z.layerGameObject != null) {
            GameObjectMap item = z.getObjectMap(truck.destID);
            user.transportFinish(truck.id);
            // start to send
            byte[] data = null;
            // send message report
            ArrayList<Integer> posReal = item.getRealLocation();
            if (posReal != null) {
                String ct = MessageTmpl.transportEquipContent;
                ct = ct.replace("#r", String.valueOf(item.id_r))
                        .replace("#z", String.valueOf(item.id_z))
                        .replace("#p", posReal.get(0) + ":" + posReal.get(1));
                GameMessageBase gm = new GameMessageBuilder()
                        .setSender("system")
                        .setReceiver(truck.userID)
                        .setSubject(MessageTmpl.transportEquipSubject)
                        .setContent(ct)
                        .setType(ENUM_MESSAGE.REPORT_TEST_MINE_MSG.getCode())
                        .buildAndSave();
                if (gm != null) {
                    // start to add to Thread sender
                    MessageThread.i().addData(userID, gm);
                    sb.append(String.format("Sending message to %s wiht all of content [%s]\n", userID, Func.json(gm)));
                }
                // send data sell/buy
                baos.reset();
                os.writeShort(GI_SERVER_CMD.RES_UPDATE_PROFILE.getCode());
                user.getPlayerProfile().getProfileBin(os, false);
                data = baos.toByteArray();
                WSServiceServer.i().sendDataWithID(userID, data);
            }
        }
        p = null;
        synchronized(mTreeTruck){mTreeTruck.remove(e.getKey());}
    }

    private void finishTransFuel(LittleEndianDataOutputStream os, ByteArrayOutputStream baos, StringBuilder sb, Entry<Long, Transportation> e) throws SQLException, IOException {
        Transportation truck = e.getValue();
        String userID = truck.userID;
        GoldIncUser user = GoldIncUserMng.i().getUser(userID);
        Zone z = StaticDataMng.i().worldMap.getZone(truck.id_z);
        if (z != null && z.layerGameObject != null) {
            GameObjectMap item = z.getObjectMap(truck.destID);
            user.transportFinish(truck.id);            
            // start to send
            byte[] data = null;
            // send message report
            ArrayList<Integer> posReal = item.getRealLocation();
            if (posReal != null) {
                String ct = MessageTmpl.transportFuelContent;
                long fuel = 0;
                TreeMap<Byte, Long> lstResult = truck.getTransaction().getLstResult();
                if (lstResult.containsKey(ENUM_RESOURCE_TYPE.FUEL.getCode())) {
                    fuel = lstResult.get(ENUM_RESOURCE_TYPE.FUEL.getCode());
                }
                ct = ct.replace("#r", String.valueOf(item.id_r))
                        .replace("#z", String.valueOf(item.id_z))
                        .replace("#p", posReal.get(0) + ":" + posReal.get(1))
                        .replace("#fuel", String.valueOf(fuel));
                GameMessageBase gm = new GameMessageBuilder()
                        .setSender("system")
                        .setReceiver(truck.userID)
                        .setSubject(MessageTmpl.transportFuelSubject)
                        .setContent(ct)
                        .setType(ENUM_MESSAGE.REPORT_TEST_MINE_MSG.getCode())
                        .buildAndSave();
                if (gm != null) {
                    // start to add to Thread sender
                    MessageThread.i().addData(userID, gm);
                    sb.append(String.format("Sending message to %s wiht all of content [%s]\n", userID, Func.json(gm)));
                }
                // send data sell/buy
                baos.reset();
                os.writeShort(GI_SERVER_CMD.RES_USER_RESOURCE.getCode());
                user.getPlayerProfile().getPrivate().getGameResources().writeData(os);
                data = baos.toByteArray();
                WSServiceServer.i().sendDataWithID(userID, data);
            }
        }
        synchronized(mTreeTruck){mTreeTruck.remove(e.getKey());}
    }
    
    private void checkTruck() {
        if (mTreeTruck.isEmpty()) {
            return;
        }
        SortedMap<Long, Transportation> temp;
        Entry<Long, Transportation>[] eTemp;
        synchronized (mTreeTruck) {
            temp = mTreeTruck.headMap(System.currentTimeMillis());
            eTemp = temp.entrySet().toArray(WSGameDefine.EMPTY_ENTRY);
        }
        if (!temp.isEmpty()) {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            StringBuilder sb = new StringBuilder();
            sb.append(Func.now()).append("\n");
            try (LittleEndianDataOutputStream os = new LittleEndianDataOutputStream(baos)) {
                for (Entry<Long, Transportation> e : eTemp) {
                    baos.reset();
                    switch (e.getValue().tradeTransaction.typeTranstport) {
                        case TradeTransaction.ACTION_BUY:
                        case TradeTransaction.ACTION_SELL:
                            finishTransResourceTrader(os, baos, sb, e);
                            break;
                        case TradeTransaction.ACTION_TRANS_TO_CLAIM:
                            finishTransEquip(os, baos, sb, e);
                            break;
                        case TradeTransaction.ACTION_TRANS_FUEL:
                            finishTransFuel(os, baos, sb, e);
                            break;
                        default:
                            break;
                    }
                }
            } catch (Exception ex) {
                sb.append(String.format("Exception detail:\n%s", Func.toString(ex)));
            }
            debug(sb.toString());
            sb.setLength(0);
        }
    }

    public void addExploration(Exploration e) {
        synchronized (mTreeExploration) {
            mTreeExploration.put(e.endTime, e);
        }
    }

    public void addTruck(Transportation e) {
        synchronized (mTreeTruck) {
            mTreeTruck.put(e.endTime, e);
        }
    }

    public void startMng() {
        start();
    }

    public void stopMng() {
        mRunning = false;
        interrupt();
    }

    @Override
    public void run() {
        mRunning = true;
        while (mRunning) {
            checkExploration();
            checkTruck();
            mSleep(1000);
        }
    }

}
