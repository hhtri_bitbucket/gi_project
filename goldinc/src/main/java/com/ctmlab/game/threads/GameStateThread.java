/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ctmlab.game.threads;

import com.ctmlab.game.data.builder.GameMessageBuilder;
import com.ctmlab.game.data.model.GoldIncUser;
import com.ctmlab.game.data.model.base.GameMessageBase;
import com.ctmlab.game.data.model.building.BuildingRefinery;
import com.ctmlab.game.data.pojo.GameResources;
import com.ctmlab.game.goldincenum.ENUM_MESSAGE;
import com.ctmlab.game.goldincenum.ENUM_UNIT_TYPE;
import com.ctmlab.game.goldincenum.GI_SERVER_CMD;
import com.ctmlab.game.hadoop.HadoopProvider;
import com.ctmlab.game.manager.GoldIncMng;
import com.ctmlab.game.manager.GoldIncUserMng;
import com.ctmlab.game.network.manager.WSGameDefine;
import com.ctmlab.game.network.manager.WSServiceServer;
import com.dp.db.Func;
import com.google.common.io.LittleEndianDataOutputStream;
import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.SortedMap;
import java.util.TreeMap;

/**
 *
 * @author hhtri
 */
public class GameStateThread extends Thread{
    
    private static final Object LOCK = new Object();
    private static final Object LOCK_WAIT = new Object();
    private static GameStateThread inst;

    public static GameStateThread i() {
        synchronized (LOCK) {
            if (inst == null) {
                inst = new GameStateThread();
            }
            return inst;
        }
    }    
    
    private GameStateThread(){}
    
    private class RefineryOil{
        private String userID = null;
        private long startTime = 0;
        private long endTime = 0;
        private int numberOfFuel = 0;    
        private BuildingRefinery building = null;
        public RefineryOil(String id, long st, long et, int val, BuildingRefinery b){
            userID = id;
            startTime = st;
            endTime = et;
            numberOfFuel = val;
            building = b;
        }
        public String getUserID(){return userID;}
        public long getStart(){return startTime;}
        public long getEnd(){return endTime;}
        public int getNumberOilRefine(){return numberOfFuel;}        
        public BuildingRefinery getBuilding(){return building;}        
    }
    
    private final TreeMap<Long, RefineryOil> mListRefineryOil = new TreeMap();    
    private boolean mRunning = false;
    
    private void debug(String data) {HadoopProvider.i().writeLog("refinerythread", data);}
    
    public void addRefineryOilInfo(String id, BuildingRefinery b, long st, long et, int val){
        mListRefineryOil.put(et, new RefineryOil(id, st, et, val, b));
    }
    
    public void oilToFuel(){
        if (mListRefineryOil.isEmpty()) {return;}
        SortedMap<Long, RefineryOil> temp;
        Entry<Long, RefineryOil>[] eTemp;
        synchronized (mListRefineryOil) {
            temp = mListRefineryOil.headMap(System.currentTimeMillis());
            eTemp = temp.entrySet().toArray(WSGameDefine.EMPTY_ENTRY);
        }
        if (!temp.isEmpty()) {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            StringBuilder sb = new StringBuilder();
            sb.append(Func.now()).append("\n");
            try (LittleEndianDataOutputStream os = new LittleEndianDataOutputStream(baos)) {
                for (Entry<Long, RefineryOil> e : eTemp) {
                    baos.reset();
                    RefineryOil item = e.getValue();
                    // update level to owner
                    GoldIncUser user = GoldIncUserMng.i().getUser(item.getUserID());
                    if(user!=null){
                        user.getPlayerProfile().getPrivate().setRefineryFuel(item.getNumberOilRefine());
                        GameResources gRes = user.getPlayerProfile().getPrivate().getGameResources();
                        os.writeShort(GI_SERVER_CMD.RES_USER_RESOURCE.getCode());
                        gRes.writeData(os);
                        WSServiceServer.i().sendDataWithID(item.getUserID(), baos.toByteArray());
                        sb.append(String.format("oilToFuel - RES_USER_RESOURCE [%s]\n", user.userID, Func.json(gRes)));
                        baos.reset();
                        
                        os.writeShort(GI_SERVER_CMD.RES_UPDATE_BUILDING.getCode());
                        os.writeByte(item.getBuilding().getType());
                        item.getBuilding().writeData(os);
                        WSServiceServer.i().sendDataWithID(item.getUserID(), baos.toByteArray());
                        sb.append(String.format("oilToFuel - RES_UPDATE_BUILDING [%s]\n", user.userID, Func.json(item.getBuilding())));
                        baos.reset();
                        
                        // message to client
                        GameMessageBase gm = new GameMessageBuilder()
                                    .setSender("system")
                                    .setReceiver(item.getUserID())
                                    .setSubject("Refine oil")
                                    .setContent("Number of fuel is " + item.getNumberOilRefine())
                                    .setType(ENUM_MESSAGE.REPORT_TEST_MINE_MSG.getCode())
                                    .buildAndSave();
                        if (gm != null) {
                            // start to add to Thread sender
                            MessageThread.i().addData(user.userID, gm);
                            sb.append(String.format("oilToFuel - Sending message to %s wiht all of content [%s]\n", user.userID, Func.json(gm)));
                        }
                    }

                    synchronized (mListRefineryOil) {
                        mListRefineryOil.remove(e.getKey());
                    }
                }
            } catch (Exception ex) {
                sb.append(String.format("oilToFuel - Exception detail:\n%s", Func.toString(ex)));
            }
            debug(sb.toString());
            sb.setLength(0);
        }
    }
       
    public void startMng() {
        start();
    }

    public void stopMng() {
        mRunning = false;
        interrupt();
    }
    
    private void mSleep(int time) {
        try {
            synchronized (LOCK_WAIT) {
                LOCK_WAIT.wait(time);
            }
        } catch (InterruptedException ex) {
        }
    }

    @Override
    public void run() {
        mRunning = true;
        while (mRunning) {
            mSleep(1000);
            oilToFuel();
        }
    }
}
