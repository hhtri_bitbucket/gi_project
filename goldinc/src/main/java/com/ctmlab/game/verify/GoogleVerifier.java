/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ctmlab.game.verify;

import com.ctmlab.util.ExHttpClient;
import com.dp.db.Func;
import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeFlow;
import com.google.api.client.googleapis.auth.oauth2.GoogleClientSecrets;
import com.google.api.client.googleapis.auth.oauth2.GoogleIdTokenVerifier;
import com.google.api.client.googleapis.auth.oauth2.GoogleTokenResponse;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.jackson2.JacksonFactory;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.List;

/**
 *
 * @author hhtri
 */
public class GoogleVerifier {

    private static final Object LOCK = new Object();
    private static GoogleVerifier inst;

    public static GoogleVerifier i() {
        synchronized (LOCK) {
            if (inst == null) {
                inst = new GoogleVerifier();
            }
            return inst;
        }
    }

    public static final List<String> SCOPES = Arrays.asList("https://www.googleapis.com/auth/games");
    public static final NetHttpTransport httpTransport = new NetHttpTransport();
    public static final JacksonFactory jsonFactory = new JacksonFactory();

    public GoogleClientSecrets clientSecrets;
    public GoogleAuthorizationCodeFlow flow;
    public GoogleIdTokenVerifier verifier;

    public GoogleVerifier() {
        try {
//
//            ByteArrayOutputStream baos = new ByteArrayOutputStream();
//            byte[] b = new byte[1024];
//            int len;
//            InputStream is = GooglePlayGamesVerification.class.getResourceAsStream(secret);
//            while ((len = is.read(b)) != -1) {
//                baos.write(b, 0, len);
//            }
//
//            System.out.println(new String(baos.toByteArray()));

            clientSecrets = GoogleClientSecrets.load(jsonFactory, new InputStreamReader(GooglePlayGamesVerification.class.getResourceAsStream("secret")));
            flow = new GoogleAuthorizationCodeFlow.Builder(httpTransport, jsonFactory, clientSecrets, SCOPES)
                    //.setAccessType("offline")
                    .build();
            verifier = new GoogleIdTokenVerifier.Builder(httpTransport, jsonFactory)
                    .setAudience(Arrays.asList(clientSecrets.getDetails().getClientId()))
                    .setIssuer("accounts.google.com")
                    .build();
        } catch (Exception ex) {
        }
    }

    public GooglePlayGamesVerification verify(String token) {
        String error = null;
        if (!token.contains(".")) {
            try {
                synchronized (this) {
                    GoogleTokenResponse response = flow.newTokenRequest(token).execute();
                    Credential credential = flow.createAndStoreCredential(response, null);
                    String new_token = credential.getAccessToken();
                    Func.printlnLog("checkSocialAuth.log", "Exchange token: " + token + " > " + new_token);
                    if (!Func.isLinuxServer()) {
//                        System.out.println("Exchange token: " + token + " > " + new_token);
                    }
                    token = new_token;
                }
            } catch (Exception ex) {
                error = ex.toString();
            }
        }
        if (!Func.isLinuxServer()) {
            System.out.println("GooglePlayGamesVerification - Error: " + error);
        }
        if (error == null) {
            try {
                String url = "https://www.googleapis.com/oauth2/v2/tokeninfo?access_token=" + token;
                String result = ExHttpClient.i().get(url);
                if (result != null) {
                    GooglePlayGamesVerification v = Func.gson.fromJson(result, GooglePlayGamesVerification.class);
                    String json = ExHttpClient.i().get("https://www.googleapis.com/oauth2/v3/userinfo?access_token=" + token);
//                    if (!Func.isLinuxServer()) {
//                        System.out.println("json: \n" + json);
//                    }
                    if (json != null) {
                        v.user_info = Func.gson.fromJson(json, GoogleUserInfo.class);
                    }
//                    v.valid = true;
                    if (!Func.isLinuxServer()) {
//                        System.out.println("Info: \n" + Func.pson(v));
                    }
                    return v;
                }
            } catch (Exception ex) {
                error = ex.toString();
            }
        }
        return null;
    }

    public static void main(String[] args) {

        String authCode = "4/AADEg_lEigupYsdA6cU_NqEU6aGAE1xzkhQSkFXl728Fta7qupuhTjAXOncSPbwcdj3Ik1Cfj0nWSKJhZ4N5PT0";

        GoogleVerifier verifier = new GoogleVerifier();

        System.out.println(Func.pson(verifier.verify(authCode)));
    }
}
