/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ctmlab.game.verify;

import com.ctmlab.util.ExHttpResult;
import com.ctmlab.util.ExHttpClientSession;
import com.google.gson.JsonSyntaxException;
//import com.ctmlab.game.manager.MWManager;
import com.dp.db.Func;
import java.sql.SQLException;

/**
 *
 * @author hhtri
 */

public class GIIVerifyManager {
    private static final Object LOCK = new Object();
    private static GIIVerifyManager inst;
    public static GIIVerifyManager i() {synchronized (LOCK) {if (inst == null) {inst = new GIIVerifyManager();}return inst;}}
    private GIIVerifyManager() {}

	public static final long ONE_SECOND = 1000;
	public static final long ONE_MINUTE = 60 * ONE_SECOND;
	public static final long MARGIN_TIME = 5 * ONE_MINUTE;

    private long expireTime = 0;
    private String accessToken = "";
	
	public static class RefreshTokenResult {
		public String access_token;
		public int expires_in;
	}
	public static class AndroidReceipt {
		public String orderId;
		public String packageName;
		public String productId;
		public long purchaseTime;
		public int purchaseState;
		public String purchaseToken;
		public String developerPayload;

		public String getSellID() {
			String []ss = orderId.split("\\.");
			return ss.length==2?ss[1]:ss[0];
		}
		public boolean isRefund() {return purchaseState==1;}
	}
	public static class GoogleReturn {
		public String kind;
		public long purchaseTime;
		public int purchaseState;
		public int consumptionState;

		public long startTimeMillis;
		public long expiryTimeMillis;
		public boolean autoRenewing;
		public String priceCurrencyCode;
		public long priceAmountMicros;
		public String countryCode;
		public String developerPayload;
		public int cancelReason;
		public boolean isRefund() {return purchaseState==1;}
	}

	public static class GooglePlayReceiptPayload {
		public String json;
		public String signature;
	}
	public static class GooglePlayReceipt {
		public String Store;
		public String TransactionID;
		public String Payload;
		public GooglePlayReceiptPayload payload;
		public AndroidReceipt receipt;
	}
	
	public static GooglePlayReceipt receiptFromString(String receipt) {
		GooglePlayReceipt r = Func.gson.fromJson(receipt, GooglePlayReceipt.class);
		r.payload = Func.gson.fromJson(r.Payload, GooglePlayReceiptPayload.class);
		r.receipt = Func.gson.fromJson(r.payload.json, AndroidReceipt.class);
		return r;
	}
	
    public String getAccessToken() {
        long time = System.currentTimeMillis();
        if (expireTime < time) {
            ExHttpResult res;
            res = ExHttpClientSession.i().post("https://accounts.google.com/o/oauth2/token", new String[]{
                "grant_type", "refresh_token",
                "client_id", "999469359845-a9m2eicr3m0bdl2fr0n3nti90satlkor.apps.googleusercontent.com",
                "client_secret", "k5qq0Zm0b6JgMc2aq_AIhb6g",
                "refresh_token", "1/klG7N0ZxQFl_AhF8l7nGYx0zL3-_cArGuttLyaqWJrl7O4G66q88Kb8lVVLhdA_G"
            });
//            if(!Func.isLinuxServer()){
//                System.out.println("IAP");
//                System.out.println(res.responseCode);
//                System.out.println(res.text);
//            }
            try{
                RefreshTokenResult result = Func.gson.fromJson(res.text, RefreshTokenResult.class);
                accessToken = result.access_token;
	            expireTime = time + result.expires_in * ONE_SECOND - MARGIN_TIME;
                
//                if(!Func.isLinuxServer()){
//                    System.out.println(accessToken);
//                    System.out.println("----------------------------------------------");
//                }
            } catch(JsonSyntaxException ex){accessToken = "";}
        }
        return accessToken;
    }
    
    public ExHttpResult verify(String guid, GooglePlayReceipt r, String ip) {
		synchronized(this) {
			String token = getAccessToken();
			if(token!=null && !token.isEmpty()) {
				ExHttpResult res = ExHttpClientSession.i().get("https://www.googleapis.com/androidpublisher/v1.1/applications/" + r.receipt.packageName
						+ "/inapp/" + r.receipt.productId
						+ "/purchases/" + r.receipt.purchaseToken
						+ "?access_token=" + token);

//				if(!Func.isLinuxServer()){
//					System.out.println(res.text);
//				}
				return res;
			}else{
				return null;
			}
		}
    }
    
    public GoogleVerifier getGoogleClientVerifier(){
        return GoogleVerifier.i();
    }
    
    public GooglePlayGamesVerification getUserInfo(String authCode){
        GooglePlayGamesVerification obj = getGoogleClientVerifier().verify(authCode);
        return obj;
    }
	
//	public static void main(String []argv) throws SQLException {
//		
//		String receiptGood = "{\"Store\":\"GooglePlay\",\"TransactionID\":\"GPA.3384-0752-4671-06307\",\"Payload\":\"{\\\"json\\\":\\\"{\\\\\\\"orderId\\\\\\\":\\\\\\\"GPA.3384-0752-4671-06307\\\\\\\",\\\\\\\"packageName\\\\\\\":\\\\\\\"com.ctmlab.metalwarfare\\\\\\\",\\\\\\\"productId\\\\\\\":\\\\\\\"10k_coins\\\\\\\",\\\\\\\"purchaseTime\\\\\\\":1518618721385,\\\\\\\"purchaseState\\\\\\\":0,\\\\\\\"purchaseToken\\\\\\\":\\\\\\\"gfdclldhpopmnlmeijdcgmfl.AO-J1OzMq6QW5flrQsC6GZkIxy1blM4YpsYQHI9FfC2ZWvpXmzrF7UdWVGhhR8ji6y9q9f2FFc3BslJKT-stGwEO6X2jwSnD9dtXpU3YHpUHjAtoage2wAFI51yT6qucT1xsDY5avIt9\\\\\\\"}\\\",\\\"signature\\\":\\\"Kw1FIDfQvA6vmWzepDbuietIRJt1mI1XEU+5zKmUYH7tZx\\\\/b+S2THponGS4+5g2JG50yBo7\\\\/DkDN6Txfh2cmD\\\\/Zrna6\\\\/NIsJdfMTTNF+7H28AnsfszbjrWWNORX+y4kdkwIU6x\\\\/dItexqi8+uPIvzo661CghOLc2aOnYn0uwuy1GXvXRZfSPlbH5YACs0KPzuy30gLlDAM3W7I3D+N1hWRtXtM1JOgAh3qo1LpqaaMhNNR2QyV8\\\\/30t12vX3mswiHDYG3nW5lzuifZqnUo4ddrdMYSoXtFM\\\\/zrOOqS2E2Wzke9DsMXVlpuxemoBq+1MIVWddvGZbpQw8n7ryxDmBYA==\\\"}\"}";
//		String receiptFailed = "{\"Store\":\"GooglePlay\",\"TransactionID\":\"1657477742315060162.4949805230880596\",\"Payload\":\"{\\\"json\\\":\\\"{\\\\\\\"purchaseToken\\\\\\\":\\\\\\\"ozmhvrcavsityjasexmsloek\\\\\\\",\\\\\\\"developerPayload\\\\\\\":\\\\\\\"\\\\\\\",\\\\\\\"packageName\\\\\\\":\\\\\\\"com.ctmlab.metalwarfare\\\\\\\",\\\\\\\"purchaseState\\\\\\\":0,\\\\\\\"orderId\\\\\\\":\\\\\\\"1657477742315060162.4949805230880596\\\\\\\",\\\\\\\"purchaseTime\\\\\\\":1519009067453,\\\\\\\"productId\\\\\\\":\\\\\\\"400k_coins\\\\\\\"}\\\",\\\"signature\\\":\\\"k5QdZOYPgqvbaqsOVYPskJVoOoNupyeQ3PRC2B\\\\/OuOV\\\\/0g+U\\\\/ZYeVjPtvQu6fKjmwGjnegFjcsukzID+D5MIVrfwvy+JMlSF08PtHk0\\\\/6EuaM2+jX0z3zsL0FWcd7+rCmb0RGFvRDySYQIb9GgQKuULrlA72R9JspK3sXIDbWC4n6QkRHc0nmKCpn7siHdXNvYk2G0f+syc5rOAcoYV4boYVwwrJ8IY1zv1He9qfYCxyebKUaMombnimX96gITSegnfEoC\\\\/PQUBKyU85q980ma+9N2OD+BIH2e\\\\/edc+M6DkJB1w=\\\"}\"}";
//		MWManager.i().init();
//		MWVerifyManager m = MWVerifyManager.i();
//		m.getAccessToken();
//		System.out.println("Token: "+m.accessToken);
//		System.out.println("Expires: "+new java.sql.Timestamp(m.expireTime));
//		ExHttpResult res;
//		GooglePlayReceipt receipt;
//		receipt = receiptFromString(receiptGood);
//		System.out.println(Func.pson(receipt));
//		res = m.verify("1234",receipt,"172.0.0.1");
//		System.out.println(res.responseCode);
//		System.out.println(res.text);
//		
//		receipt = receiptFromString(receiptFailed);
//		System.out.println(Func.pson(receipt));
//		res = m.verify("1234",receipt,"172.0.0.1");
//		System.out.println(res.responseCode);
//		System.out.println(res.text);
//	}
}
