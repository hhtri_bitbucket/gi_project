/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ctmlab.game.hadoop;
import com.ctmlab.game.data.ctmon.CTMON;
import com.ctmlab.game.goldincenum.CTMON_FieldType;
import com.ctmlab.game.manager.GlobalConfig;
import com.ctmlab.game.network.manager.WSGameDefine;
import com.dp.db.DBData;
import com.dp.db.DBDataListener;
import com.dp.db.DBDataPostgres;
import com.dp.db.Func;
import com.google.common.io.LittleEndianDataInputStream;
import com.google.common.io.LittleEndianDataOutputStream;
import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
/**
 *
 * @author hhtri
 */
public class LogManager {
    
    private class StatePrintWriter extends PrintWriter {

        private boolean closed = false;

        public StatePrintWriter(BufferedWriter writer) {
            super(writer);
        }

        @Override
        public void close() {
            closed = true;
            super.close();
        }
    }

    private class StateOutputStream extends DataOutputStream {

        private boolean closed = false;

        public StateOutputStream(OutputStream out) {
            super(out);
        }

        @Override
        public void close() {
            closed = true;
            try {
                super.close();
            } catch (IOException ex) {
            }
        }
    }
    
    public static final Object LOCK = new Object();
    private static LogManager inst;
    public static LogManager i() {
        synchronized (LOCK) {
            if (inst == null) {inst = new LogManager();}
            return inst;
        }
    }

    private final HashMap<String, StatePrintWriter> handlerLogText = new HashMap(WSGameDefine.MAP_INITIAL_CAPACITY);// debug log
    private final HashMap<String, StateOutputStream> handlerLogBin = new HashMap(WSGameDefine.MAP_INITIAL_CAPACITY);// debug bin
    private final SimpleDateFormat sdfFull = new SimpleDateFormat("yyyyMMdd");
    private String currDate = sdfFull.format(new java.util.Date());
    

//    public boolean isDebug = true;
    public boolean isDebug = GlobalConfig.isOnlineServer();
    private final String mLogName = this.getClass().getSimpleName().toLowerCase();
    private final String FWD_SLASH = File.separator;
    private final String PJT_NAME = "goldinc";
    
    private final String prefixPath = GlobalConfig.prefix + PJT_NAME+"/debug_logs/";
    private final File folder_log = new File(GlobalConfig.pathLogText);
    private final File folder_bin = new File(GlobalConfig.pathLogBin);
    private final File folder_bak = new File(GlobalConfig.pathBackup);
    
    public void closeAll() {        
        StatePrintWriter[] t1;
        synchronized(handlerLogText){t1=handlerLogText.values().toArray(new StatePrintWriter[0]);}
        for(StatePrintWriter out:t1){synchronized (out){out.close();}}
        synchronized(handlerLogText){handlerLogText.clear();}
        //------------------------        
        StateOutputStream[] t2;
        synchronized(handlerLogBin){t2=handlerLogBin.values().toArray(new StateOutputStream[0]);}
        for(StateOutputStream out:t2){synchronized (out){out.close();}}
        synchronized(handlerLogBin){handlerLogBin.clear();}
        //------------------------        
    }
    
    public File getFolderLog(){
        return folder_log;
    }
    
    public File getFolderBak(){
        return folder_bak;
    }

    private LogManager() {
        if(!folder_log.exists()){folder_log.mkdirs();}
        if(!folder_bin.exists()){folder_bin.mkdirs();}
        if(!folder_bak.exists()){folder_bak.mkdirs();}
    }

    public String writeLog(File folder, String filename, String text) {
        if (!isDebug) {
            System.out.println(text);
            return "";
        }
        String key = "";
        try {
            int retry = 5;
            do {
                StatePrintWriter out;
                byte flag = 1;
                synchronized (LOCK) {
                    String strDate = sdfFull.format(new java.util.Date());
                    String year = strDate.substring(0, 4);
                    String month = strDate.substring(4, 6);
                    String day = strDate.substring(6, 8);
                    String path = folder.getPath() + FWD_SLASH + year + FWD_SLASH + month + FWD_SLASH + day;
                    //create new folder
                    File f = new File(path);
                    if(!f.exists()){f.mkdirs();}
                    if (!currDate.equals(strDate)) {
                        //close all handle last day
                        closeAll();
                        currDate = strDate;                        
                    }
                    key = path + FWD_SLASH + filename;
                    out = handlerLogText.get(key);
                    if (out == null) {
                        out = new StatePrintWriter(new BufferedWriter(new FileWriter(new File(key), true)));
                        handlerLogText.put(key, out);
                    }
                }
                synchronized (out) {
                    if (!out.closed && flag>0) {
                        out.println(text);
                        out.flush();
                        break;
                    }
                }
            } while (--retry > 0);
        } catch (IOException ex) {
            return ex.getMessage();
        }
        return key;
    }
    
    public String writeLog(String filename, String text) {
        return writeLog(folder_log, filename, text);
    }
    
    public String writeLogBin(File folder, String filename, byte[] data) {
        if(!isDebug){return "";}
        String key = "";
        try {
            int retry = 5;
            do {
                StateOutputStream out;
                byte flag = 1;
                synchronized (LOCK) {
                    String strDate = sdfFull.format(new java.util.Date());
                    String year = strDate.substring(0, 4);
                    String month = strDate.substring(4, 6);
                    String day = strDate.substring(6, 8);
                    String path = folder.getPath() + FWD_SLASH + year + FWD_SLASH + month + FWD_SLASH + day;
                    //create new folder
                    File f = new File(path);
                    if(!f.exists()){f.mkdirs();}
                    if (!currDate.equals(strDate)) {
                        //close all handle last day
                        closeAll();
                        currDate = strDate;                        
                    }
                    key = path + FWD_SLASH + filename;
                    out = handlerLogBin.get(key);
                    if (out == null) {
                        out = new StateOutputStream(new FileOutputStream(new File(key), true));
                        handlerLogBin.put(key, out);
                    }
                }
                synchronized (out) {
                    if (!out.closed && flag>0) {
                        out.writeInt(data.length);
                        out.write(data);
                        out.flush();
                        break;
                    }
                }
            } while (--retry > 0);
        } catch (IOException ex) {
            return ex.getMessage();
        }
        return key;
    }
    
    public String writeLog(String filename, byte[] data) {
        return writeLogBin(folder_bin, filename, data);
    }

    public String getLogArray(Object...data){
        return Func.json(data);
    }
    
    
    // <editor-fold defaultstate="collapsed" desc="Log instance of Hadoop when it is disconnected">
    
    private void writeObjectBase(StateOutputStream os, Object o) throws IOException {
        CTMON_FieldType type = CTMON_FieldType.get(o);
        os.write(type.code);
        switch (type) {
            case BYTE: {
                os.writeByte((byte) o);
                break;
            }
            case BOOLEAN: {
                os.writeBoolean((boolean) o);
                break;
            }
            case SHORT: {
                os.writeShort((short) o);
                break;
            }
            case INTEGER: {
                os.writeInt((int) o);
                break;
            }
            case LONG: {
                os.writeLong((long) o);
                break;
            }
            case STRING: {
                byte[] data = String.valueOf(o).getBytes(WSGameDefine.UTF_8);
                os.writeInt(data.length);
                os.write(data);
                break;
            }
            case FLOAT: {
                os.writeFloat((float) o);
                break;
            }
            case DOUBLE: {
                os.writeDouble((double) o);
                break;
            }
            case DATE: {
                os.writeLong(((java.util.Date) o).getTime());
                break;
            }
            case BYTE_ARRAY: {
                byte[] data = (byte[]) o;
                os.writeInt(data.length);
                os.write(data);
                break;
            }
            case OBJECT: {
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                LittleEndianDataOutputStream ledos = new LittleEndianDataOutputStream(baos);
                ledos.writeUTF(o.getClass().getName());
                CTMON.i().toStream(ledos, o);
                byte[] dataCTMON = baos.toByteArray();
                os.writeInt(dataCTMON.length);
                os.write(dataCTMON);
                break;
            }
        }
        os.flush();
    }
    
    private Object readObjectBase(DataInputStream is) throws Exception {
        CTMON_FieldType type = CTMON_FieldType.get(is.read());
        switch (type) {
            case BYTE: {
                return is.readByte();
            }
            case BOOLEAN: {
                return is.readBoolean();
            }
            case SHORT: {
                return is.readShort();
            }
            case INTEGER: {
                return is.readInt();
            }
            case LONG: {
                return is.readLong();
            }
            case STRING: {
                byte[] data = new byte[is.readInt()];
                is.read(data);
                return new String(data, WSGameDefine.UTF_8);
            }
            case FLOAT: {
                return is.readFloat();
            }
            case DOUBLE: {
                return is.readDouble();
            }
            case DATE: {
                return new Date(is.readLong());
            }
            case BYTE_ARRAY: {
                byte[] data = new byte[is.readInt()];
                is.read(data);
                return data;
            }
            case OBJECT: {
                byte[] data = new byte[is.readInt()];
                is.read(data);
                ByteArrayInputStream bais = new ByteArrayInputStream(data);
                LittleEndianDataInputStream ledis = new LittleEndianDataInputStream(bais);
                Class clz = Class.forName(ledis.readUTF());
                return CTMON.i().fromStream(ledis, clz);
            }
            default:{
                return null;
            }
        }
    }
    
    public void writeHDLog(String filename, Object data){
        if(data != null){
            try {
                int retry = 5;
                do {
                    StateOutputStream out;
                    synchronized (LOCK) {
                        String strDate = sdfFull.format(new java.util.Date());
                        String filePath = prefixPath + FWD_SLASH + PJT_NAME +
                            FWD_SLASH + (Func.todayServer() + "_" + filename);

                        //create new folder
                        File f = new File(prefixPath);
                        if (!f.exists()) {
                            f.mkdirs();
                        }
                        if (!currDate.equals(strDate)) {
                            //close all handle last day
                            closeAll();
                            currDate = strDate;
                        }
                        out = handlerLogBin.get(filePath);
                        if (out == null) {
                            out = new StateOutputStream(new FileOutputStream(
                                    new File(filePath), true));
                            handlerLogBin.put(filePath, out);
                        }

                    }
                    synchronized (out) {
                        if (!out.closed) {
                            writeObjectBase(out, data);
                            break;
                        }
                    }
                } while (--retry > 0);
            } catch (IOException | IllegalArgumentException ex) {
                LogManager.i().writeLog(mLogName, Func.now()
                        + "\nwriteLog(String filename, Object data)\n"
                        + Func.toString(ex));
            }
        }
    }
    
    public Object readHDLog(String filename){ 
        String filePath = prefixPath + FWD_SLASH + PJT_NAME +
            FWD_SLASH + (Func.todayServer() + "_" + filename);
        File path = new File(filePath);
        try (DataInputStream dis = new DataInputStream(new FileInputStream(path))){
            return readObjectBase(dis);            
        } catch (Exception ex) {
            LogManager.i().writeLog(mLogName, Func.now()
                    + "\nreadLog(String filename)\n"
                    + Func.toString(ex));
        }        
        return null;
    }
    
    public void saveHD(DBDataPostgres data){
        if(data != null){
            try {
                StateOutputStream out = null;              
                int retry = 5;
                do {
                    File hdfsPath = new File(prefixPath);
                    //create new folder main                        
                    if (!hdfsPath.exists()) {
                        hdfsPath.mkdirs();
                    }
                    synchronized (LOCK) {
                        String strDate = Func.todayServer();
                        String filePath = prefixPath + FWD_SLASH + PJT_NAME +
                            FWD_SLASH + Func.todayServer() + "_" + data.table();                        
                        if (!currDate.equals(strDate)) {
                            //close all handle last day
                            closeAll();
                            currDate = strDate;
                        }
                        out = handlerLogBin.get(filePath);
                        if (out == null) {
                            out = new StateOutputStream(new FileOutputStream(new File(filePath), true));
                            handlerLogBin.put(filePath, out);
                        }
                    }
                    synchronized (out) {
                        ByteArrayOutputStream baos = new ByteArrayOutputStream();
                        LittleEndianDataOutputStream os;
                        os = new LittleEndianDataOutputStream(baos);
                        CTMON.i().toStream(os, data);
                        byte[] dataCTMON = baos.toByteArray();
                        if(!out.closed){
                            out.writeInt(dataCTMON.length);
                            out.write(dataCTMON);
                            out.flush();
                            break;
                        }
                    }
                } while (--retry > 0);
                
            } catch (IOException | IllegalArgumentException ex) {
                LogManager.i().writeLog(mLogName, Func.now()
                        + "\n" + Func.toString(ex));
            }
        }
    }
    
    public void saveHDErr(DBDataPostgres data){
        if(data != null){
            try {
                StateOutputStream out = null;              
                int retry = 5;
                do {
                    File hdfsPath = new File(prefixPath);
                    //create new folder main                        
                    if (!hdfsPath.exists()) {
                        hdfsPath.mkdirs();
                    }
                    synchronized (LOCK) {
                        String strDate = Func.todayServer();
                        String filePath = prefixPath + FWD_SLASH +
                                Func.todayServer() + "_err_" + data.table();                        
                        if (!currDate.equals(strDate)) {
                            //close all handle last day
                            closeAll();
                            currDate = strDate;
                        }
                        out = handlerLogBin.get(filePath);
                        if (out == null) {
                            out = new StateOutputStream(new FileOutputStream(new File(filePath), true));
                            handlerLogBin.put(filePath, out);
                        }
                    }
                    synchronized (out) {
                        ByteArrayOutputStream baos = new ByteArrayOutputStream();
                        LittleEndianDataOutputStream os;
                        os = new LittleEndianDataOutputStream(baos);
                        CTMON.i().toStream(os, data);
                        byte[] dataCTMON = baos.toByteArray();
                        if(!out.closed){
                            out.writeInt(dataCTMON.length);
                            out.write(dataCTMON);
                            out.flush();
                            break;
                        }
                    }
                } while (--retry > 0);
                
            } catch (IOException | IllegalArgumentException ex) {
                LogManager.i().writeLog(mLogName, Func.now()
                        + "\n" + Func.toString(ex));
            }
        }
    }
    
    public <T extends DBData> void select(Class<T> classOfT, long start, long end, DBDataListener<T> listener) {
        if(end<start){return;}
        long timeOfDay = WSGameDefine.MILISEC_IN_DAY;
        long iTime = start;
        while(iTime<end){
            select(classOfT, iTime, listener);
            iTime+=timeOfDay;
        }
    }
    
    public <T extends DBData> void select(Class<T> classOfT, long time, DBDataListener<T> listener) {

        String nameFile = classOfT.getSimpleName().toLowerCase();
        File path = new File(prefixPath + FWD_SLASH + PJT_NAME +
                            FWD_SLASH + Func.getSDateUTC(time) + "_" + nameFile);
        try (DataInputStream dis = new DataInputStream(new FileInputStream(path))){
            while (true) {
                int len = dis.readInt();
                byte[] dataCTMON = new byte[len];
                dis.read(dataCTMON, 0, len);

                ByteArrayInputStream bais = new ByteArrayInputStream(dataCTMON);
                LittleEndianDataInputStream ledis = new LittleEndianDataInputStream(bais);
                T data = CTMON.i().fromStream(ledis, classOfT);
                if(listener!=null){
                    listener.onItem(data);
                }
            }
        } catch (Exception ex) {
            LogManager.i().writeLog(mLogName, Func.now()
                    + "\n" + Func.toString(ex));
        }    
    }    
}
