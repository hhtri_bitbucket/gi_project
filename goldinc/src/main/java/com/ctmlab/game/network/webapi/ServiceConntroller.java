/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ctmlab.game.network.webapi;

import com.ctmlab.game.data.model.GameObjectMap;
import com.ctmlab.game.manager.ServiceMng;
import com.ctmlab.game.data.pojo.ServiceResponse;
import com.ctmlab.game.data.staticdata.Zone;
import com.ctmlab.game.goldincenum.ZONE_LAYER;
import com.ctmlab.game.hadoop.HadoopProvider;
import com.ctmlab.game.manager.GoldIncUserMng;
import com.ctmlab.game.manager.StaticDataMng;
import com.ctmlab.util.FileUploadManager;
import com.ctmlab.util.FileUploadRequest;
import com.dp.db.Func;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author hhtri
 */
@WebServlet(name = "Service", urlPatterns = {"/service/*"})
public class ServiceConntroller extends HttpServlet {

    private enum Actions {
        region_data,        
        gameobject_info,  
        map_info,  
        map_info_extra,  
        cash_converter_info,  
        cash_converter_json_info,  
        cash_converter_info_save, 
        registry_account,
        verify_account,
        forgot_passwd,
        change_passwd,
        change_password, // jsp page
        test_ping,
        data_zone_test,
        data_zone_detail,
        
    }

    private String getUriAjax(String uri) {
        String[] urlPatterns = uri.split("/");
        if (urlPatterns.length > 1) {
            return urlPatterns[3];
        } else {
            return "";
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet get methods">   
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("UTF-8");
        response.setCharacterEncoding("UTF-8");
        String action = getUriAjax(request.getRequestURI());
        FileUploadRequest req = FileUploadManager.i().getRequest(request);
        ServiceResponse res = null;
        switch (Actions.valueOf(action)) {
            case data_zone_test:{
                String key = req.getParameter("data");
                Zone z = StaticDataMng.i().worldMap.getZone(Short.parseShort(key));
                String s = "";
                s+="Ground: " + z.getLayerDecorBy((byte) ZONE_LAYER.GROUND.getCode()).data.size();
                s+="\nWater: " + z.getLayerDecorBy((byte) ZONE_LAYER.WATER.getCode()).data.size();
                s+="\nShore: " + z.getLayerDecorBy((byte) ZONE_LAYER.SHORE.getCode()).data.size();
                s+="\nTree: " + z.getLayerDecorBy((byte) ZONE_LAYER.TREE.getCode()).data.size();
                s+="\nObject: " + z.layerGameObject.size();
                try(PrintWriter o = response.getWriter()){
                    o.print(s);
                }
                break;
            }
            case data_zone_detail:{
                String key = req.getParameter("data");
                Zone z = StaticDataMng.i().worldMap.getZone(Short.parseShort(key));
                GameObjectMap[] datas = z.getGameObjects();
                String s=""+datas.length;
                try(PrintWriter o = response.getWriter()){
                    o.print(s);
                }
                break;
            }
            case verify_account:{
                String key = req.getParameter("data");
                try(PrintWriter o = response.getWriter()){
                    o.print(ServiceMng.i().verifyEmail(key));
                }
                break;
            }
            case forgot_passwd:{
                String email = req.getParameter("email");
                String msg = "";
                if(email==null){
                    msg = "Param 'email' is empty";
                } else{
                    res = ServiceMng.i().forgotPasswd(email);
                    msg = res.message;
                }                
                try(PrintWriter o = response.getWriter()){
                    o.print(msg);
                }
                break;
            }
            case change_password:{
                String[] urlPatterns = request.getRequestURI().split("/");
                if (urlPatterns.length > 1) {
                    String key = urlPatterns[urlPatterns.length-1];
                    String email = GoldIncUserMng.i().getEmail(key);
                    
                    ServletContext servletContext = request.getServletContext().getContext("/goldinc");                    
                    RequestDispatcher requestDispatcher = servletContext.getRequestDispatcher("/pages/change_password.jsp?email=" + key);
                    if (requestDispatcher != null) {
                        requestDispatcher.forward(request, response);
                    }
                    
                }
                break;
            }
            case test_ping:{
                HadoopProvider.i().writeLog("goldincmng", request.getRemoteHost() + " ping time " + System.currentTimeMillis());
            }
//            case map_info_extra:{                
//                res = ServiceMng.i().getMapExtra();
//                try(PrintWriter o = response.getWriter()){
//                    o.print("Data length: " + res.data.length);
//                }
//                break;
//            }
//            case map_info:{                
//                res = ServiceMng.i().getMap();
//                try(PrintWriter o = response.getWriter()){
//                    o.print("Data length: " + res.data.length);
//                }
//                break;
//            }
            default:
                break;
        }
    }

    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="HttpServlet post methods">   
    
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("UTF-8");
        response.setCharacterEncoding("UTF-8");
        String action = getUriAjax(request.getRequestURI());
        ServiceResponse res = null;
        FileUploadRequest req = FileUploadManager.i().getRequest(request);
        switch (Actions.valueOf(action)) {
            case cash_converter_info:{                
                res = ServiceMng.i().getCashConverter();
                break;
            }
            case cash_converter_json_info:{                
                res = ServiceMng.i().getCashConverterJson();
                break;
            }
            case cash_converter_info_save:{
                String json = req.getParameter("json");
                res = ServiceMng.i().saveCashConverter(json);
                break;
            }
            case map_info_extra:{                
                res = ServiceMng.i().getMapExtra();
                break;
            }
            case gameobject_info:{                
                res = ServiceMng.i().getGameObjectInfos();
                break;
            }
            case map_info:{                
                res = ServiceMng.i().getMap();
                break;
            }
            case registry_account:{                
                String email = req.getParameter("email");
                String passwd = req.getParameter("passwd"); 
                String companyName = req.getParameter("companyName"); 
                res = ServiceMng.i().regAccount(email, passwd, companyName);
                break;
            }
            case region_data:{
                String id = req.getParameter("data");
                res = ServiceMng.i().getRegionStatic(Short.valueOf(id));
                break;
            }
            case forgot_passwd:{
                String email = req.getParameter("email");
                res = ServiceMng.i().forgotPasswd(email);
                break;
            }
            case change_passwd:{
                String key = req.getParameter("key");
                String new_passwd = req.getParameter("new_passwd");
                String cfr_passwd = req.getParameter("cfr_passwd");
                String msg = "";
                if(new_passwd.equals(cfr_passwd)){
                    res = ServiceMng.i().changePasswd(key, new_passwd);
                    msg = res.message;
                } else{
                    msg = "New password and confirm password is incorrect.";
                }                
                try(PrintWriter o = response.getWriter()){
                    o.print("<script>parent.showMsg('"+msg+"')</script>");
                }
                break;
            }
            default:
                res = ServiceMng.i().getDefaultResponse(action);
                break;
        }
        byte[] bytes = res.getBytes();
        response.setContentType(Func.MINE_FILE);
        response.setContentLength(bytes.length);
        HadoopProvider.i().writeLog("goldincmng", Func.now() + " === Data Lengh [" + action + "] - " + bytes.length);
        try (OutputStream out = response.getOutputStream()) {
            out.write(bytes);
            out.flush();
        }
    }
    
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="HttpServlet put methods">   
    
    @Override
    protected void doPut(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("UTF-8");
        response.setCharacterEncoding("UTF-8");
        String action = getUriAjax(request.getRequestURI());
        ServiceResponse mapRes = new ServiceResponse();
        FileUploadRequest req = FileUploadManager.i().getRequest(request);
        switch (Actions.valueOf(action)) {
            default:
                break;
        }
    }
    
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="HttpServlet delete methods">   
    
    @Override
    protected void doDelete(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("UTF-8");
        response.setCharacterEncoding("UTF-8");
        String action = getUriAjax(request.getRequestURI());
        ServiceResponse mapRes = new ServiceResponse();
        FileUploadRequest req = FileUploadManager.i().getRequest(request);
        switch (Actions.valueOf(action)) {
            default:
                break;
        }
    }
    
    // </editor-fold>

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
