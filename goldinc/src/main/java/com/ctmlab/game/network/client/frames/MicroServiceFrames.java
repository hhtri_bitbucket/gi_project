/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ctmlab.game.network.client.frames;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.ArrayList;

/**
 *
 * @author hhtri
 */
public class MicroServiceFrames {

    private int len;
    private final ArrayList<byte[]> lstFrameItem;

    private FrameItem curFrame = null;
    private MicroServiceListener listener = null;
    
    private int lenSend = 0;
    private ArrayList<byte[]> lstTemp = new ArrayList<>();

    public MicroServiceFrames() {
        len = 0;
        lstFrameItem = new ArrayList<>(512);
    }

    public void setListener(MicroServiceListener l) {
        listener = l;
    }

    public void receiverData(ByteBuffer buff) {
        if (len == 0) {
            len = buff.getInt();
            curFrame = new FrameItem();
        }
        while (buff.remaining() > 0) {
            boolean flag = curFrame.readData(buff);
            if (flag) {
                lstFrameItem.add(curFrame.getData());
                curFrame = new FrameItem();
            }
        }

        if (len == lstFrameItem.size()) {
            // finsish big frame and throw listeners
        }
    }

    public byte[] singleData(byte[] data, int maxSize) {
        byte[] dataTemp = new byte[4+data.length];
        ByteBuffer buff = ByteBuffer.wrap(dataTemp);
        buff.order(ByteOrder.LITTLE_ENDIAN);
        buff.rewind();
        buff.putInt(data.length);
        buff.put(data);
        return dataTemp;
    }

    public ArrayList<byte[]> seperateBigData(byte[] data, int maxSize) {
        int lenCheck = 4+data.length;
        byte[] arr = new byte[lenCheck];
        ByteBuffer buffData = ByteBuffer.wrap(arr);
        buffData.order(ByteOrder.LITTLE_ENDIAN);
        buffData.rewind();
        buffData.putInt(data.length); // size package
        buffData.put(data);
        
        // <NumberOfFrame><len><bytes>
        ArrayList<byte[]> rs = new ArrayList<>(512);
        int n = lenCheck / maxSize;
        if (lenCheck % maxSize != 0) {n++;}

        byte[] temp = new byte[maxSize-4];
        buffData.rewind();
        buffData.get(temp);
        rs.add(temp);
        while (true) {
            if (buffData.remaining()>maxSize){temp = new byte[maxSize];}
            else {temp = new byte[buffData.remaining()];}
            buffData.get(temp);
            rs.add(temp);
            if (rs.size()==n){break;}
        }
        listener.sendFrame(rs);
        return rs;
    }
}
