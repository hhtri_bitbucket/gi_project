/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ctmlab.game.network.buffer;

import com.ctmlab.game.data.model.SocialChat;
import com.ctmlab.game.hadoop.HadoopProvider;
import com.ctmlab.game.network.manager.WSGameDefine;
import com.dp.db.DB;
import com.dp.db.DBData;
import com.dp.db.DBDataListener;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.Date;
import java.util.TreeMap;
import java.util.concurrent.atomic.AtomicLong;

/**
 *
 * @author hhtri
 * MsgBuffer used to save data chat from ' current day ' to 'last ' 30 days '
 * Buffer will be loaded onStartup
 * Arrangement Loading Options follows as :
 * - First priority is : Loading from Postgres SQL
 * - Second priority is: Loading from HadoopProvider
 * - Finally priority is: Loading from file "/home/hhtri/Desktop/workspace/netbean/debug_logs/goldinc/"
 * 
 */
public class WSGameMsgBuffer {   
    
    public static WSGameMsgBuffer sInst = null;
    private static final Object LOCK = new Object();
    public final AtomicLong mAtomicIDBuffer = new AtomicLong(1L);
    public final MessageObjectBase mMsgGlobalBuff = new MessageObjectBase();
    public final TreeMap<Long, MessageObjectBase> mMsgBuffer = new TreeMap<>();
    
    public static WSGameMsgBuffer i(){synchronized(LOCK){if(sInst==null){sInst=new WSGameMsgBuffer();}return sInst;}}

    private WSGameMsgBuffer() {}
    
    private final DBDataListener dbItemListener = (DBDataListener) (DBData t) -> {
        SocialChat data = (SocialChat) t;
        addToBuffer(data.groupId, data);
    };    
    
    private int getState(){
        int rs = -1;
        Connection cnn = null;
        
        try{cnn = DB.i().con();} catch(SQLException ex){}
        
        if(cnn!=null){rs=1;} 
        else if(HadoopProvider.i().isConnected()){rs=2;}
        else {rs=3;}
        
        cnn = null;
        return rs;
    }
    
    private long getStartOfDay(long time){
        try{
            String dayTime = WSGameDefine.sSimpleDay.format(new Date(time)) + "000000";
            return WSGameDefine.sSimpleTimeDay.parse(dayTime).getTime();
        } catch(ParseException ex){return time;}
    }    
    
    public void loadData(){
        int state = getState();
        try{            
            long tEnd = System.currentTimeMillis();
            long tStart = getStartOfDay(tEnd - (30*WSGameDefine.MILISEC_IN_DAY));
            switch (state) {
                case 1:                    
                    String where = " where #1 <= time and time <= #2";
                    where = where.replace("#1", String.valueOf(tStart))
                            .replace("#2", String.valueOf(tEnd));
                    DB.i().select(SocialChat.class, where, dbItemListener);
                    break;
                case 2:
                    HadoopProvider.i().select(SocialChat.class, tStart, tEnd, dbItemListener);
                    break;
                default:
                    break;
            }
        } catch(Exception ex){}
    }
    
    public int addToBuffer(long idGroup, SocialChat data) {
        int returnCode = 0;
//        if (idGroup == WSGameDefine.CHATTING_GLOBAL_TYPE) {
//            mMsgGlobalBuff.addToBuffer(data);
//        }
//        MessageObjectBase gameObject = mMsgBuffer.get(idGroup);
//        
//        if (gameObject == null) {
//            returnCode = 1;
//            gameObject = new MessageObjectBase();
//            synchronized (mMsgBuffer) {
//                mMsgBuffer.put(idGroup, gameObject);
//            }
//        }
//        synchronized (gameObject) {
//            gameObject.addToBuffer(data);
//        }
        return returnCode;
    }
//    
//    public void getBuffer(WSGameMsgClient out){
//        MessageObjectBase gameFrame = mMsgBuffer.get(out.getmGameObjectID());
//        gameFrame.getBuffer(out.getmLastIdx(), out);
//    }
}
