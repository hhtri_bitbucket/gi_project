/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ctmlab.game.network.client;

import com.ctmlab.game.goldincenum.GI_MICRO_SEVICE_CMD;
import com.ctmlab.game.manager.GlobalConfig;
import com.ctmlab.game.manager.ServiceMng;
import com.ctmlab.game.manager.StaticDataMng;
import com.ctmlab.game.network.client.frames.MicroServiceFrames;
import com.ctmlab.game.network.client.frames.MicroServiceListener;
import com.ctmlab.game.network.manager.WSGameDefine;
import com.ctmlab.game.network.manager.WSServiceServer;
import com.ctmlab.util.Utils;
import com.dp.db.Func;
import com.google.common.io.LittleEndianDataOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;
import javax.websocket.SendHandler;
import javax.websocket.SendResult;
import javax.websocket.Session;

/**
 *
 * @author hhtri
 */
public class WSMicroServiceClient implements SendHandler {

    // <editor-fold defaultstate="collapsed" desc="Variances in class">    

    private final Object LOCK = new Object();
    private final Object SENDING_LOCK = new Object();
    private boolean mSending = false;
    private long mLastPing = 0;
    private Session mSession = null;
    private int mId = -1;
    private final Queue<byte[]> mDataQueue = new LinkedList();

    // log name
    private String mLogName = this.getClass().getSimpleName().toLowerCase();
    private final StringBuilder mLogBuilder = new StringBuilder();

    private int mMaxBufferSize = 8192;
    private MicroServiceFrames frames = null;
    private byte[] sendArr = null;
    private ByteBuffer bufferSend = null;

    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="Public Methods">
    public void init(Session _session) {
        mSession = _session;
        mLastPing = System.currentTimeMillis();

        frames = new MicroServiceFrames();
        frames.setListener(new MicroServiceListener() {
            @Override
            public void recvFrame(ArrayList<byte[]> data) {
            }

            @Override
            public void sendFrame(ArrayList<byte[]> data) {
                for(byte[] t:data){
                    addToBuff(t);
                }
                notifySend();
            }
        });
        
        sendArr = new byte[8192];
        bufferSend = ByteBuffer.wrap(sendArr);
        bufferSend.order(ByteOrder.LITTLE_ENDIAN);
    }

    public void notifySend() {
        notifySendEx();
//        ByteBuffer buffer;
//        synchronized (mDataQueue) {
//            if (mDataQueue.isEmpty()) {
//                return;
//            }
//            synchronized (SENDING_LOCK) {
//                if (mSending) {
//                    return;
//                }
//                mSending = true;
//            }
//            buffer = ByteBuffer.wrap(mDataQueue.poll());
//        }
//        if (mSession.isOpen()) {
//            try {
//                synchronized (buffer) {
//                    buffer.rewind();
//                    mSession.getAsyncRemote().sendBinary(buffer, this);
//                }
//            } catch (java.lang.IllegalStateException ex) {
//                try {
//                    mSession.close();
//                } catch (IOException ex_close) {
//                }
//            }
//        }
    }
    
    private int lenBigFrame = 0;
    private int lastLen = 0;
    
    private int isBigData(byte[] data) {
        if (lenBigFrame > 0) {
            return 0;
        }
        ByteBuffer b = ByteBuffer.wrap(data);
        b.order(ByteOrder.LITTLE_ENDIAN);
        b.rewind();
        int len = b.getInt();
        return (len > mMaxBufferSize) ? len : 0;
    }

    public void notifySendEx() {
        synchronized (mDataQueue) {
            if (mDataQueue.isEmpty()) {
                return;
            }
        }
        synchronized (SENDING_LOCK) {
            if (mSending) {
                return;
            }
            mSending = true;
        }
        byte[] t;
        int lenFrameSend = 0;
        synchronized (mDataQueue) {
            t = mDataQueue.poll();
        }
        int len = isBigData(t);
        if (len > 0) {
            lenBigFrame = len + 4;
        }
        if (lenBigFrame > 0 && lastLen == 0) {
            // put cái pack đầu tiên có size và len
            bufferSend.rewind();
            bufferSend.putInt(1);// size pack
            lenFrameSend += 4;
            bufferSend.put(t);
            lastLen += (4 + t.length);
            lenFrameSend += t.length;
        } else if (lenBigFrame > 0 && lastLen > 0) {
            // put cái pack kế tiếp ko cần size, len
            bufferSend.rewind();
            bufferSend.put(t);
            lastLen += t.length;
            lenFrameSend += t.length;
            // kết thúc gói tin tràn max size
            if (lastLen >= lenBigFrame) {
                lenBigFrame = 0;
                lastLen = 0;
            }
        } else if (lenBigFrame == 0) {
            // gói tin lấy ra chắc chắn nhỏ hơn max size
            // tiếp tục ghép frame
            int size = 1;
            bufferSend.rewind();
            bufferSend.putInt(size);
            lenFrameSend += 4;
            bufferSend.put(t);
            lenFrameSend += t.length;
            synchronized (mDataQueue) {
                if (!mDataQueue.isEmpty()) {
                    byte[] bTemp = mDataQueue.peek();
                    while (bTemp.length < bufferSend.remaining()) {
                        bufferSend.put(bTemp);
                        lenFrameSend += bTemp.length;
                        size++;
                        mDataQueue.poll();
                        bTemp = mDataQueue.peek();
                        if (bTemp == null) {
                            break;
                        }
                    }
                    bufferSend.rewind();
                    bufferSend.putInt(size);
                }
            }
        } else {
            return;
        }
        if (mSession != null && mSession.isOpen()) {
            try {
                byte[] data = new byte[lenFrameSend];
                synchronized (bufferSend) {
                    bufferSend.rewind();
                    bufferSend.get(data);
                }
                synchronized (data) {
                    ByteBuffer buf = ByteBuffer.wrap(data);
                    buf.order(ByteOrder.LITTLE_ENDIAN);
                    buf.rewind();
                    mSession.getAsyncRemote().sendBinary(buf, this);
                }
            } catch (java.lang.IllegalStateException ex) {
                disconnect();
            }
        }
    }

    private void addToBuff(byte[] data){
        synchronized (mDataQueue) {
            mDataQueue.add(data);
        }
    }
    
    public void send(byte[] buffer) {
        if ((8 + buffer.length) > mMaxBufferSize) {
            ArrayList<byte[]> bytes = frames.seperateBigData(buffer, mMaxBufferSize);
            for (byte[] t : bytes) {
                synchronized (mDataQueue) {
                    mDataQueue.add(t);
                }
            }
        } else {
            synchronized (mDataQueue) {
                mDataQueue.add(frames.singleData(buffer, mMaxBufferSize));
            }
        }
        notifySend();
    }

    // method disconnect: close connection and remove from manager RAM.
    // Just manager call this methods
    public void disconnect() {
        if (mSession != null) {
            try {
                mSession.close();
            } catch (IOException ex) {
            }
        }
        mArrOutput = null;
        mOutput = null;
    }

    // <editor-fold defaultstate="collapsed" desc="Recv Methods">
    private ByteArrayOutputStream mArrOutput = new ByteArrayOutputStream();
    private LittleEndianDataOutputStream mOutput = new LittleEndianDataOutputStream(mArrOutput);

    public void onMessage(ByteBuffer data) throws IOException {
        GI_MICRO_SEVICE_CMD CMD = GI_MICRO_SEVICE_CMD.fromID((int) data.getShort());
        mLogBuilder.append("Time: ")
                .append(WSGameDefine.sSimpleTimeDay.format(new Date(System.currentTimeMillis())))
                .append("\nCMD: ").append(CMD.toString())
                .append("\nData remain: ").append(data.remaining());

        ServiceMng mng = ServiceMng.i();
        long time = System.currentTimeMillis();
        switch (CMD) {
            case REQ_PING: {
                mLastPing = time;
                break;
            }
            case REQ_CONNECT: {
                mMaxBufferSize = data.getInt();
                sendArr = new byte[mMaxBufferSize];
                bufferSend = ByteBuffer.wrap(sendArr);
                bufferSend.order(ByteOrder.LITTLE_ENDIAN);
                WSServiceServer.i().addMicroSClient(this);
                synchronized (LOCK) {
                    mArrOutput.reset();
                    try {
                        mOutput.writeShort(GI_MICRO_SEVICE_CMD.RES_CONNECT.getCode());
                        mOutput.writeInt(mId);
                    } catch (IOException ex) {
                    }
                    send(mArrOutput.toByteArray());
                }
                break;
            }            
            case REQ_LIST_STATIC_DATA: {
                String version = Utils.i().readBigString(data);
                HashMap<String, String> hash = Func.gson.fromJson(version, HashMap.class);
                if (StaticDataMng.i().isNewConfig(hash)) {
                    File[] lstFile = new File(GlobalConfig.pathBackup).listFiles();
                    synchronized (LOCK) {
                        mArrOutput.reset();
                        try {
                            mOutput.writeShort(GI_MICRO_SEVICE_CMD.RES_LIST_STATIC_DATA.getCode());
                            mOutput.writeBoolean(true);
                            Utils.i().writeBigString(mOutput, Func.json(StaticDataMng.i().hashVersion));
                            if (lstFile != null) {
                                mOutput.writeInt(lstFile.length);
                                for (File f : lstFile) {
                                    Utils.i().writeBigString(mOutput, f.getName());
                                }
                            } else {
                                mOutput.writeInt(0);
                            }
                        } catch (IOException ex) {
                        }
                        send(mArrOutput.toByteArray());
                    }
                } else {
                    synchronized (LOCK) {
                        mArrOutput.reset();
                        try {
                            mOutput.writeShort(GI_MICRO_SEVICE_CMD.RES_LIST_STATIC_DATA.getCode());
                            mOutput.writeBoolean(false);
                            mOutput.writeInt(0);
                        } catch (IOException ex) {
                        }
                        send(mArrOutput.toByteArray());
                    }
                }
                break;
            }
            case REQ_GET_STATIC_DATA: {
                int len = data.getInt();
                int i = 0;
                HashMap<String, byte[]> hashData = new HashMap<>();
                while (i++ < len) {
                    String fileName = Utils.i().readBigString(data);
                    byte[] dataFile = Utils.i().readFile(GlobalConfig.pathBackup + File.separator + fileName);
                    hashData.put(fileName, dataFile);
                }
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                LittleEndianDataOutputStream os = new LittleEndianDataOutputStream(baos);
                for (Map.Entry<String, byte[]> e : hashData.entrySet()) {
                    baos.reset();
                    try {
                        os.writeShort(GI_MICRO_SEVICE_CMD.RES_GET_STATIC_DATA.getCode());
                        Utils.i().writeBigString(os, e.getKey());
                        byte[] temp = e.getValue();
                        os.writeInt(temp.length);
                        os.write(e.getValue());
                    } catch (IOException ex) {
                    }
                    send(baos.toByteArray());
                }
                baos.reset();
                try {os.writeShort(GI_MICRO_SEVICE_CMD.RES_FINISH_SYNC_DATA.getCode());} catch (IOException ex) {}
                send(baos.toByteArray());                
                break;
            }
            default: {
                data.rewind();
                System.out.println(Func.now() + ": " + Func.json(data.array()));
                break;
            }
        }
//        System.out.println(mLogBuilder.toString());
        mLogBuilder.setLength(0);
        mLastPing = time;
    }

    public void onMessage(String data) throws IOException {

    }
    // </editor-fold>

    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="Refactor Methods">
    /**
     * @return the lastPing
     */
    public long getLastPing() {
        return mLastPing;
    }

    /**
     * @param lastPing the lastPing to set
     */
    public void setLastPing(long lastPing) {
        this.mLastPing = lastPing;
    }

    /**
     * @return the session
     */
    public Session getSession() {
        return mSession;
    }

    /**
     * @param session the session to set
     */
    public void setSession(Session session) {
        this.mSession = session;
    }

    /**
     * @return the id
     */
    public int getId() {
        return mId;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.mId = id;
    }

    // </editor-fold>   
    
    // <editor-fold defaultstate="collapsed" desc="Override Methods">
    /**
     * Method insure that the data is sent completely to client-side
     *
     * @author hhtri
     * @param result
     */
    @Override
    public void onResult(SendResult result) {
        synchronized (SENDING_LOCK) {
            mSending = false;
        }
        if (result.isOK()) {
        } else {
            Throwable ex = result.getException();
            if (ex != null) {
                System.out.println(Func.toString(ex));
            }
        }
        notifySend();
    }

    // </editor-fold>
}
