/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ctmlab.game.network.manager;

import com.ctmlab.game.data.model.SocialChat;
import com.ctmlab.game.hadoop.HadoopProvider;
import com.ctmlab.game.network.buffer.WSGameMsgBuffer;
import com.ctmlab.game.network.client.WSServiceClient;
import com.ctmlab.game.network.client.WSServiceClientChatting;
import com.ctmlab.game.network.client.WSGroupChatting;
import com.dp.db.DB;
import java.nio.ByteBuffer;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.concurrent.atomic.AtomicInteger;

/**
 *
 * @author hhtri
 */
public class WSServiceChattingMng {
//
//    // <editor-fold defaultstate="collapsed" desc="Variances in class">    
//    private final AtomicInteger aClientID = new AtomicInteger(1);
//    public static WSServiceChattingMng sInst = null;
//    private static final Object LOCK = new Object();
//    public final HashMap<Integer, WSServiceClientChatting> clients
//            = new HashMap(WSGameDefine.MAP_INITIAL_CAPACITY);
//    public final HashMap<Long, WSGroupChatting> groups
//            = new HashMap(WSGameDefine.MAP_INITIAL_CAPACITY);
//
//    // </editor-fold>
//    
//    
//    // <editor-fold defaultstate="collapsed" desc="Singleton Methods">
//    private WSServiceChattingMng() {
//    }
//
//    public static WSServiceChattingMng i() {
//        synchronized (LOCK) {
//            if (sInst == null) {
//                sInst = new WSServiceChattingMng();
//            }
//            return sInst;
//        }
//    }
//
//    // </editor-fold>
//    
//    
//    // <editor-fold defaultstate="collapsed" desc="Public Methods">
//    public WSServiceClientChatting[] getListClient() {
//        WSServiceClientChatting[] temp;
//        synchronized (clients) {
//            temp = clients.values().toArray(new WSServiceClientChatting[0]);
//        }
//        return temp;
//    }
//
//    public void addClient(WSServiceClientChatting client) {
//        if (client != null) {
//            int id = aClientID.getAndIncrement();
//            client.setId(id);
//            synchronized (clients) {
//                clients.put(id, client);
//            }
//        }
//    }
//
//    public void rmClient(int id) {
//        synchronized (clients) {
//            clients.remove(id);
//        }
//    }
//
//    public void rmClient(WSServiceClient client) {
//        if (client != null) {
//            rmClient(client.getId());
//        }
//    }
//
//    private WSGroupChatting getGroup(long groupID) {
//        WSGroupChatting t = groups.get(groupID);
//        if (t == null) {
//            t = new WSGroupChatting(groupID);
//            synchronized (groups) {
//                groups.put(groupID, t);
//            }
//        }
//        return t;
//    }
//
//    public void addClientToTeam(long groupID, WSServiceClientChatting client) {
//        WSGroupChatting t = getGroup(groupID);
//        int id = t.add(client);
//        if (id != 0) {
//            client.idInHost = id;
//        }
//    }
//
//    public void rmTeam(long teamID) {
//        synchronized (groups) {
//            groups.remove(teamID);
//        }
//    }
//
//    // for global
//    public void nodifyGlobal(ByteBuffer data) {
//        WSServiceClientChatting[] list = getListClient();
//        for (WSServiceClientChatting c : list) {
//            c.send(data);
//        }
//
//        // save data to buffer
//        SocialChat mData = new SocialChat(WSGameDefine.CHATTING_GLOBAL_TYPE, -1);
//        mData.data = data.array();
//
//        try {
//            // write log to hadoop
//            HadoopProvider.i().save(mData);
//            // save DB
//            mData.save(DB.i().con());
//            // save Buffer
//            WSGameMsgBuffer.i().addToBuffer(WSGameDefine.CHATTING_GLOBAL_TYPE, mData);
//        } catch (SQLException ex) {
//
//        }
//    }
//
//    // for team
//    public void nodifyTeam(long teamID, ByteBuffer data) {
//        WSGroupChatting t = getGroup(teamID);
//        if (t != null) {
//            t.notifyClient(data);
//        }
//
//        // save data to buffer
//        SocialChat mData = new SocialChat(WSGameDefine.CHATTING_GROUP_TYPE, teamID);
//        mData.data = data.array();
//
//        try {
//            // write log to hadoop
//            HadoopProvider.i().save(mData);
//            // save DB
//            mData.save(DB.i().con());
//            // save Buffer
//            WSGameMsgBuffer.i().addToBuffer(teamID, mData);
//        } catch (SQLException ex) {
//
//        }
//    }
//
//    // </editor-fold>
}
