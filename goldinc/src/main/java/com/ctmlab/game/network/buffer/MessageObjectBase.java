/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ctmlab.game.network.buffer;

import com.ctmlab.game.data.model.SocialChat;
import com.ctmlab.game.network.manager.WSGameDefine;

/**
 *
 * @author hhtri
 */
public class MessageObjectBase {
    
    private final int STATUS_NEW = -1;
    private final int STATUS_SAME_DAY = 1;
    private final int STATUS_NEXT_DAY = 0;
    
    private final MessageInDay[] data = new MessageInDay[30];
    private int mCurIdx;
    private long mLastDate;
    public MessageObjectBase(){
        mCurIdx = -1;
        mLastDate = -1;
    }
    
    private void init(long time){
        for(int i=0; i<data.length; i++){
            data[i] = new MessageInDay();
        }
        mCurIdx = 0;
        mLastDate = time;
    }
    
    private void reset(){
        for(int i=0; i<data.length; i++){
            data[i].reset();
        }
        mCurIdx = -1;
        mLastDate = -1;
    }
    
    private int stateOfDay(long time){
        if(mLastDate==-1 || mLastDate > time){
            return -1;
        }
        String strLastDate = WSGameDefine.sSimpleDay.format(new java.util.Date(mLastDate));
        String strDate = WSGameDefine.sSimpleDay.format(new java.util.Date(time));
        if(!strDate.equals(strLastDate)){
            return 0;
        }
        return 1;
    }
    
    private boolean isOutofBound(){
        return mCurIdx>=(data.length-1);
    }
    
    private void nextIdx(){
        if(isOutofBound()){
            reset();
        }
        mCurIdx++;
        data[mCurIdx].reset();
    }
    
    public void addToBuffer(SocialChat data){
        long time = data.time;
        int rs = stateOfDay(time);
        MessageInDay dataDay = null;
        switch (rs) {
            case STATUS_NEW:{
                init(time);
                break;
            }
            case STATUS_NEXT_DAY:{
                // get (mCurIndex+1) and reset data in (mCurIndex+1)
                // if out of bound => reset()
                nextIdx();
                break;   
            }
            default:{
                // time in day => get mCurIndex
                break;
            }
        }
        mLastDate = data.time;
        dataDay = this.data[mCurIdx];
        synchronized(dataDay){
            dataDay.addItem(data);
        }
    }
    
    
//    public int addBuffer(ByteBuffer b){
////        if(mLastIdx<0){return -1;}
////        else if(mLastIdx>=mBuffer.length){return -2;}
////        else {
////            mBuffer[mLastIdx] = b;
////            mLastIdx++;
////            return mLastIdx;
////        }
//    }
    

}
    
