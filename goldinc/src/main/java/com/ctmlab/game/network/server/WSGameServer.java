/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ctmlab.game.network.server;

import com.ctmlab.game.network.buffer.WSGameMsgBuffer;
import com.ctmlab.game.network.client.__Ref_WSGameClient;
import com.ctmlab.util.Utils;
import com.google.common.io.LittleEndianDataOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.atomic.AtomicInteger;

/**
 *
 * @author TheLight
 */
public class WSGameServer {
    
//    public static final Charset UTF_8 = Charset.forName("UTF-8");
//    public static final ByteOrder BYTE_ORDER = ByteOrder.LITTLE_ENDIAN;
//    
//    private WSGameServer() {}
//    public static WSGameServer _inst = null;
//    private static final Object LOCK = new Object();
//    public static WSGameServer i(){synchronized(LOCK){if(_inst==null){_inst = new WSGameServer();}return _inst;}}
//    public final HashMap<Integer,WSGameClient> clients = new HashMap();// list client connect to endpoint
//    public final HashMap<Integer,WSTeamGroupChatting> hosts = new HashMap();
//    public final WSTeamGroupChatting[] EMPTY_HOST = new WSTeamGroupChatting[0];
//    public final WSGameClient[] EMPTY_CLIENTS = new WSGameClient[0];
//    
//    public WSGameClient[] getListClient() {
//        WSGameClient[] temp;
//        synchronized(clients){temp = clients.values().toArray(EMPTY_CLIENTS);}
//        return temp;
//    }
//    
//    public ArrayList<WSGameClient> getSingleClient() {
//        ArrayList<WSGameClient> rs = new ArrayList<>();
//        WSGameClient[] temp;
//        synchronized(clients){temp = clients.values().toArray(EMPTY_CLIENTS);}
//        for(WSGameClient c:temp){if(c!=null&&c.getHost()==null){rs.add(c);}}
//        return rs;
//    }
//    
//    public void listHostToSingleClient() throws IOException {
//        WSTeamGroupChatting[] rs = WSGameServer.i().getListHost();
//        // send list host back to client
//        ByteArrayOutputStream baos = new ByteArrayOutputStream();
//        LittleEndianDataOutputStream outStream = new LittleEndianDataOutputStream(baos);
//        MWNetworkClientCMD cmd = MWNetworkClientCMD.RES_LIST_HOST;
//        outStream.writeShort(cmd.getID());
//        outStream.writeInt(rs.length);
//        for (WSTeamGroupChatting host : rs) {
//            outStream.writeInt(host.id);
//            outStream.writeInt(host.Map_ID());
//            Utils.writeBigString(outStream, host.getName());
//            outStream.writeInt(host.playerCount());
//            outStream.writeInt(host.Max_Player());
//        }        
//        ByteBuffer buffer = ByteBuffer.wrap(baos.toByteArray());
//        baos.reset();
//        getSingleClient().forEach((c) -> {c.send(buffer);});
//    }
//    
//    public WSTeamGroupChatting createHost(int map_id, int max_player) {
//        synchronized(hosts) {
//            int hostID = getHostID();
//            WSTeamGroupChatting host = new WSTeamGroupChatting(hostID);
//            host.setMap_ID(map_id);
//            host.setMax_Player(max_player);
//            host.mGameObjectID = WSGameFrameBuffer.i().createFrameBuffer();
//            hosts.put(hostID, host);
//            return host;
//        }
//    }
//    public void removeHost(int host_id) throws IOException {
//        WSTeamGroupChatting removedHost = null;
//        synchronized(hosts) {
//            removedHost = hosts.remove(host_id);
//        }
//        if(removedHost != null) {
//            removedHost.destroySelf();
//        }
//    }
//    public WSTeamGroupChatting getHost(int id) {
//        synchronized(hosts) {
//            WSTeamGroupChatting host = hosts.get(id);
//            return host;
//        }
//    }
//    public WSTeamGroupChatting[] getListHost() {
//        WSTeamGroupChatting[] rs;
//        synchronized(hosts) {
//            rs = hosts.values().toArray(EMPTY_HOST);
//        }
//        return rs;
//    }
//    
//    public void addClient(WSGameClient client){
//        synchronized(clients){clients.put(client.getSessionID(), client);}
//    }
//    
//    public void rmClient(int id){
//        synchronized(clients){clients.remove(id);}
//    }
//    
//    public void rmClient(WSGameClient client){
//        rmClient(client.getSessionID());
//    }
//    
//    private final AtomicInteger atomicClientID = new AtomicInteger(1);
//    public int genClientID() {
//        return atomicClientID.getAndIncrement();
//    }
//    
//    private final AtomicInteger atomicHostID = new AtomicInteger(1);
//    public int getHostID() {
//        return atomicHostID.getAndIncrement();
//    }
}
