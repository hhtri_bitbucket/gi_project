/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ctmlab.game.pathfinding.astartalgorithm;

import java.awt.Point;

/**
 *
 * @author hhtri
 */
public class RectangleGraph extends SquareGraph{

    private int dimensionX;
    private int dimensionY;

    public RectangleGraph(int mapDimension) {
        super(mapDimension);
    }

    public RectangleGraph(int mapDimensionX, int mapDimensionY) {
        super(mapDimensionX, mapDimensionY);
        dimensionX = mapDimensionX;
        dimensionY = mapDimensionY;
    }

    public int getDimensionX(){
        return dimensionX;
    }

    public int getDimensionY(){
        return dimensionY;
    }

    @Override
    public boolean isInsideMap(Point p) {
        return ((p.getX() >= 0) && (p.getX() < getDimensionX()) && (p.getY() >= 0) && (p.getY() < getDimensionY()));
    }

}
