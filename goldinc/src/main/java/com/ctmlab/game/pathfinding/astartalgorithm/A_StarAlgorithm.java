package com.ctmlab.game.pathfinding.astartalgorithm;


/** ************************************************
 * Copyright (C) 2014  Raptis Dimos <raptis.dimos@yahoo.gr>
 *
 *
    This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
	*************************************************
 */
import com.ctmlab.game.data.staticdata.PointInt;
import com.ctmlab.game.data.staticdata.PointShort;
import java.awt.Point;
import java.io.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

public class A_StarAlgorithm {
  
    public static void test() throws InvalidLetterException, FileNotFoundException, IOException, HeapException {

        String filename = "/home/hhtri/Downloads/Pathfinding-master/Java/input_files/large.txt";

        InputHandler handler = new InputHandler();
        SquareGraph graph = handler.readMap(filename);
        long start = System.currentTimeMillis();
        ArrayList<Node> path = graph.executeAStar();
        System.out.println(System.currentTimeMillis()-start);
        if (path == null) {
            System.out.println("There is no path to target");
        } else {
            System.out.println("The total number of moves from distance to the target are : " + path.size());
            graph.printPath(path);
        }
    }
    
    public static ArrayList<PointShort> findPath(byte[][] matrixZone, Point start, Point end){
        byte[][] matrix = matrixZone;
        ArrayList<PointShort> rs = new ArrayList<>();
        try{
            InputHandler handler = new InputHandler();
            SquareGraph graph = handler.readMap(matrix, start, end);
//            System.out.println(String.format("start - {%d,%d}", start.x, start.y));
//            System.out.println(String.format("end - {%d,%d}", end.x, end.y));
            ArrayList<Node> path = graph.executeAStar();
            if (path != null) {
                int x = start.x;
                int y = start.y;
                for (int i = 0; i<path.size(); i++) {
                    Node node = path.get(i);
//                    System.out.println(String.format("{%d,%d}", node.getX(), node.getY()));
                    rs.add(new PointShort(node.getX()-x, node.getY()-y));
                    x = node.getX();
                    y = node.getY();
                }
                path.remove(0);
                Collections.reverse(path);
            }
            graph.reset();
            path = null;
            matrix = null;
            graph = null;
            handler = null;
        } catch(Exception ex){}
        return rs;
    }

    public static ArrayList<PointInt> findPathEx(byte[][] matrixZone, Point start, Point end){
        byte[][] matrix = matrixZone;
        ArrayList<PointInt> rs = new ArrayList<>();
        try{
            InputHandler handler = new InputHandler();
            SquareGraph graph = handler.readMap(matrix, start, end);
            ArrayList<Node> path = graph.executeAStar();
            if (path != null) {

                for (int i = 0; i<path.size(); i++) {
                    Node node = path.get(i);
                    rs.add(new PointInt(node.getX(), node.getY()));
                }
                path.remove(0);
                Collections.reverse(path);
            }
            graph.reset();
            path = null;
            matrix = null;
            graph = null;
            handler = null;
        } catch(Exception ex){}
        return rs;
    }
}
