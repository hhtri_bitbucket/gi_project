/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ctmlab.game.goldincenum;

import java.util.HashMap;

/**
 *
 * @author hhtri
 */
public enum ENUM_UNIT_PROPERTY {

    NULL(0),
    MAX_CLAIM(1),
    MAX_OIL_CLAIM(2),
    NETWORTH_VALUE(3),
    SPEED_LAND(4),
    SPEED_AIR(5),
    MAX_POWER(6),
    OIL_CAP(7),
    REFINE_SPEED(8),
    MAX_SPECIAL_SOLDIER(9),
    MAX_MERCENARY_SOLDIER(10),
    DPS(11),
    HEALTH(12),
    MAX_VEHICLE(13),
    AMMO_UNLOCK(14),
    VEHICLE_UNLOCK(15),
    RESOURCE_UNLOCK(16),
    OIL_PRODUCTION(17),
    ;

    private final int code;
    private static final HashMap<Integer, ENUM_UNIT_PROPERTY> CODE_FIELD_TYPE = new HashMap();
    private static final HashMap<String, ENUM_UNIT_PROPERTY> S_CODE_FIELD_TYPE = new HashMap();

    ENUM_UNIT_PROPERTY(int code) {
        this.code = code;
    }

    public byte getCode() {
        return (byte) code;
    }

    static {
        for (ENUM_UNIT_PROPERTY type : ENUM_UNIT_PROPERTY.values()) {
            CODE_FIELD_TYPE.put(type.code, type);
            S_CODE_FIELD_TYPE.put(type.toString().toLowerCase(), type);
        }
    }

    public static String getString(int i) {
        ENUM_UNIT_PROPERTY rs = CODE_FIELD_TYPE.get(i);
        return (rs == null ? NULL.toString().toLowerCase() : rs.toString().toLowerCase());
    }
    
    public static byte getCode(int i) {
        ENUM_UNIT_PROPERTY rs = CODE_FIELD_TYPE.get(i);
        return (byte) (rs == null ? NULL.getCode() : rs.getCode());
    }

    public static byte getCode(String key) {
        ENUM_UNIT_PROPERTY rs = S_CODE_FIELD_TYPE.get(key);
        return rs == null ? NULL.getCode() : rs.getCode();
    }
}
