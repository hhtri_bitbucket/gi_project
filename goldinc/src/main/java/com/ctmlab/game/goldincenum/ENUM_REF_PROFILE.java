/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ctmlab.game.goldincenum;

import java.util.HashMap;

/**
 *
 * @author hhtri
 */
public enum ENUM_REF_PROFILE {
    K_TEST_MINE(1),
    K_PEG_MINE(2),
//    K_EXPLORATION(3),
//    K_TRANSPORTATION(4),
    ;
    private final int code;
    private static final HashMap<Integer, ENUM_REF_PROFILE> CODE_FIELD_TYPE = new HashMap();

    ENUM_REF_PROFILE(int code) {
        this.code = code;
    }

    public int getCode() {
        return  code;
    }

    public static ENUM_REF_PROFILE get(int code) {
        return CODE_FIELD_TYPE.get(code);
    }
    
    public static int[] getKeys(){        
        ENUM_REF_PROFILE[] temp = ENUM_REF_PROFILE.values();
        int[] rs = new int[temp.length];
        for(int i=0;i<temp.length; i++){
            rs[i] = temp[i].getCode();
        }
        return rs;
    }
    public static String[] getStringKeys(){        
        ENUM_REF_PROFILE[] temp = ENUM_REF_PROFILE.values();
        String[] rs = new String[temp.length];
        for(int i=0;i<temp.length; i++){
            rs[i] = temp[i].toString();
        }
        return rs;
    }

    static {
        for (ENUM_REF_PROFILE type : ENUM_REF_PROFILE.values()) {
            CODE_FIELD_TYPE.put(type.code, type);
        }
    }
}
