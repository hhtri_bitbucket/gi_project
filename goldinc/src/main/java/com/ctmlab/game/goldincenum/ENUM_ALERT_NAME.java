/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ctmlab.game.goldincenum;

import java.util.HashMap;

/**
 *
 * @author hhtri
 */
public enum ENUM_ALERT_NAME {
    
    SET_NAME_FAIL(0),
    SET_NAME_SUCCESS(1),
    SET_NAME_TOO_SHORT(2),
    SET_NAME_TOO_LONG(3),
    SET_NAME_SPECIAL_KEY(4)
    ;
    
    private final int code;
    private static final HashMap<Integer, ENUM_ALERT_NAME> CODE_FIELD_TYPE = new HashMap();

    ENUM_ALERT_NAME(int code) {
        this.code = code;
    }

    public byte getCode(){return (byte) code;}
    
    public static ENUM_ALERT_NAME get(int code) {
        return CODE_FIELD_TYPE.get(code);
    }

    static {
        for (ENUM_ALERT_NAME type : ENUM_ALERT_NAME.values()) {
            CODE_FIELD_TYPE.put(type.code, type);
        }
    }
}
