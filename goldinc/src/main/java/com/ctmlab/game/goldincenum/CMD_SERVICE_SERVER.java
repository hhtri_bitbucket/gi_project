/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ctmlab.game.goldincenum;

import java.util.HashMap;

/**
 *
 * @author hhtri
 */
public enum CMD_SERVICE_SERVER {
    Null(0),   
    // world map 1-20  
    REQ_CONNECT(1),    
    REQ_MAP_INFO(2), 
    REQ_REGION_INFO(3), 
    REQ_CASH_CONVERTER_INFO(4),
    //---
    RES_CONNECT(1001),
    RES_MAP_INFO(1002),
    RES_REGION_INFOE(1003),
    RES_CASH_CONVERTER_INFO(1004),
        
    RES_PING(9998),
    IDLE(9999)
    ;
    
    private static final HashMap<Integer, CMD_SERVICE_SERVER> hashID = new HashMap();

    private final int code;  
    
    CMD_SERVICE_SERVER(int _code) {code = _code;}
    
    public int getCode() {return code;}

    static {
        for (CMD_SERVICE_SERVER type : values()) {
            hashID.put(type.code, type);
        }
    }

    public static CMD_SERVICE_SERVER fromID(int _code) {
        return hashID.get(_code) == null ? Null : hashID.get(_code);
    }
}
