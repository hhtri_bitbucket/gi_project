/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ctmlab.game.goldincenum;

import java.util.HashMap;

/**
 *
 * @author thelight
 */
public enum GI_MICRO_SEVICE_CMD {
    
    Null(0),   

    REQ_CONNECT(1),   
    REQ_GET_STATIC_DATA(2), 
//    REQ_UPDATE_STATIC_DATA(3), //server edit and auto send service
    REQ_LIST_STATIC_DATA(4),
    //---
    RES_CONNECT(2001),
    RES_GET_STATIC_DATA(2002),
    RES_UPDATE_STATIC_DATA(2003),
    RES_LIST_STATIC_DATA(2004),
    RES_FINISH_SYNC_DATA(2005),
    
    REQ_PING(10000),
    RES_PING(10001),
    
    ;
    
    private static final HashMap<Integer, GI_MICRO_SEVICE_CMD> hashID = new HashMap();

    private final int code;  
    
    GI_MICRO_SEVICE_CMD(int _code) {code = _code;}
    
    public int getCode() {return code;}

    static {
        for (GI_MICRO_SEVICE_CMD type : values()) {
            hashID.put(type.code, type);
        }
    }

    public static GI_MICRO_SEVICE_CMD fromID(int _code) {
        return hashID.get(_code) == null ? Null : hashID.get(_code);
    }
}
