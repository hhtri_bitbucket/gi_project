/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ctmlab.game.goldincenum;

import java.util.HashMap;

/**
 *
 * @author thelight
 */
public enum GI_SERVER_CMD {
    
    Null(0),   
    // world map 1-20  
    REQ_DATA_ZONE(1),    
    REQ_MINE_INFO(2), 
    REQ_TEST_MINE(3), 
    REQ_PEG_MINE(4),  
    REQ_UPDATE_SPOT(5),
    //---
    RES_DATA_ZONE(2001),
    RES_MINE_INFO(2002),
    RES_TEST_MINE(2003),
    RES_PEG_MINE(2004),
    RES_UPDATE_SPOT(2005),
    
    // player 21-40
    REQ_UPDATE_WALLET(21),
    // non REQ_UPDATE_PROFILE(22)
    REQ_BUY_RESOURCE(23),
    REQ_SELL_RESOURCE(24),
    REQ_SIGN_IN(25),
    REQ_USER_RESOURCE(26),
    REQ_SET_NAME(27),
    REQ_TRADE_CASH_INFO(28),
    
    //---
    RES_UPDATE_WALLET(2021),
    RES_UPDATE_PROFILE(2022),
    RES_BUY_RESOURCE(2023),
    RES_SELL_RESOURCE(2024),    
    RES_SIGN_IN(2025),
    RES_USER_RESOURCE(2026),
    RES_SET_NAME(2027),
    RES_TRADE_CASH_INFO(2028),
    
    // message 41-60
    REQ_SEND_MESSAGE(41),
    REQ_GET_MESSAGE(42),
    REQ_SET_STATE_MESSAGE(43),
    REQ_REMOVE_MESSAGE(44),
    //---
    RES_SEND_MESSAGE(2041),
    RES_GET_MESSAGE(2042),
    RES_SET_STATE_MESSAGE(2043),
    RES_REMOVE_MESSAGE(2044),
    
    // alliance chatting 61-80
    REQ_CREATE_ALLIANCE(61),    
    REQ_INFO_ALLIANCE(62),    
    REQ_INVITE_ALLIANCE(63),
    REQ_JOIN_ALLIANCE(64),
    REQ_ADD_MEMBER(65),
    REQ_REMOVE_MEMBER(66),
    REQ_SET_COLEADER(67),
    REQ_CHAT_ALLIANCE(68),
    REQ_CHAT_REGION(69),    
    //--- 
    RES_CREATE_ALLIANCE(2061),
    RES_INFO_ALLIANCE(2062),
    RES_INVITE_ALLIANCE(2063),
    RES_JOIN_ALLIANCE(2064),
    RES_ADD_MEMBER(2065),
    RES_REMOVE_MEMBER(2066),
    RES_SET_COLEADER(2067),
    RES_CHAT_ALLIANCE(2068),
    RES_CHAT_REGION(2069),
    
    // homebased info 81-100
    REQ_PLAYER_BUILDING(81),
    REQ_UPGRADE_BUILDING(82),
    REQ_CLAIMS_INFO(83),
//    REQ_UPDATE_HOMEBASE(84),
    REQ_TRANSPORT_TO_CLAIM(85),
    REQ_REFINE_OIL(86),
    REQ_TRANSPORT_FUEL(87),
    
    RES_PLAYER_BUILDING(2081),
    RES_UPGRADE_BUILDING(2082),
    RES_CLAIMS_INFO(2083),
    RES_UPDATE_BUILDING(2084),
    RES_TRANSPORT_TO_CLAIM(2085),
    RES_REFINE_OIL(2086),
    RES_TRANSPORT_FUEL(2087),
    
    RES_PING(9998),
    IDLE(9999),
    TEST_WS(10000)
    ;
    
    private static final HashMap<Integer, GI_SERVER_CMD> hashID = new HashMap();

    private final int code;  
    
    GI_SERVER_CMD(int _code) {code = _code;}
    
    public int getCode() {return code;}

    static {
        for (GI_SERVER_CMD type : values()) {
            hashID.put(type.code, type);
        }
    }

    public static GI_SERVER_CMD fromID(int _code) {
        return hashID.get(_code) == null ? Null : hashID.get(_code);
    }
}
