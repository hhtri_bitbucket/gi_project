/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ctmlab.game.goldincenum;

import java.util.HashMap;

/**
 *
 * @author hhtri
 */
public enum ENUM_MATRIX_TYPE {
    NONE(-1),
    TREE(1),
    MOUNTAIN(2),
    GROUND(3),
    WATER(4),
    SHORE(5),
    HOMEBASE(6),
    MINE(7),
    TRADE_CENTER(8),
    BUSH(9),
    ;
    
    private final int code;
    private static final HashMap<Integer, ENUM_MATRIX_TYPE> CODE_FIELD_TYPE = new HashMap();

    ENUM_MATRIX_TYPE(int code) {
        this.code = code;
    }

    public byte getCode(){return (byte) code;}
    
    public static ENUM_MATRIX_TYPE get(int code) {
        return CODE_FIELD_TYPE.get(code);
    }

    static {
        for (ENUM_MATRIX_TYPE type : CODE_FIELD_TYPE.values()) {
            CODE_FIELD_TYPE.put(type.code, type);
        }
    }
}
