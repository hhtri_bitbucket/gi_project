/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ctmlab.game.manager;

import com.ctmlab.game.data.builder.GoldIncUserBuilder;
import com.ctmlab.game.data.builder.RegistryAccountBuilder;
import com.ctmlab.game.data.model.Claim;
import com.ctmlab.game.data.model.Exploration;
import com.ctmlab.game.data.model.RegistryAccount;
import com.ctmlab.game.data.model.SocialAlliance;
import com.ctmlab.game.data.model.GoldIncUser;
import com.ctmlab.game.data.model.Transportation;
import com.ctmlab.game.data.model.worker.Worker;
import com.ctmlab.game.data.model.building.Building;
import com.ctmlab.game.data.model.equipment.Equipment;
import com.ctmlab.game.data.pojo.PlayerProfile;
import com.ctmlab.game.data.pojo.ProfilePrivate;
import com.ctmlab.game.hadoop.HadoopProvider;
import com.ctmlab.game.network.manager.WSGameDefine;
import com.ctmlab.game.threads.LoadingDataThread;
import com.ctmlab.util.SendEmailUtil;
import com.ctmlab.util.Utils;
import com.dp.db.DB;
import com.dp.db.Func;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.UUID;

/**
 *
 * @author hhtri
 */
public class GoldIncUserMng {
    
    public static final Object LOCK = new Object();
    private static GoldIncUserMng inst;

    public static GoldIncUserMng i() {
        synchronized (LOCK) {
            if (inst == null) {
                inst = new GoldIncUserMng();
            }
            return inst;
        }
    }
    
    private class UserInfo{    
        String userID;
        ArrayList<Building> lstBuilding;
        ArrayList<Equipment> lstEquipment;
        ArrayList<Transportation> lstTransportation;
        ArrayList<Exploration> lstExploration;
        ArrayList<Worker> lstWorker;        
        ArrayList<Claim> lstClaim;        
        public UserInfo(String user){
            userID = user;
            lstBuilding = new ArrayList<>();
            lstEquipment = new ArrayList<>();
            lstTransportation = new ArrayList<>();
            lstExploration = new ArrayList<>();
            lstWorker = new ArrayList<>();
            lstClaim = new ArrayList<>();
        }
        public String getUserID(){return userID;}
        public ArrayList<Building> getBuildings(){return lstBuilding;}
        public ArrayList<Equipment> getEquipments(){return lstEquipment;}
        public ArrayList<Transportation> getTransportations(){return lstTransportation;}
        public ArrayList<Exploration> getExplorations(){return lstExploration;}
        public ArrayList<Worker> getWorkers(){return lstWorker;}
        public ArrayList<Claim> getClaims(){return lstClaim;}
        public void addBuilding(Building o){lstBuilding.add(o);}
        public void addEquipment(Equipment o){lstEquipment.add(o);}
        public void addTransportation(Transportation o){lstTransportation.add(o);}
        public void addExploration(Exploration o){lstExploration.add(o);}
        public void addWorker(Worker o){lstWorker.add(o);}        
        public void addClaim(Claim o){lstClaim.add(o);}        
    }
    
    private final HashMap<String, GoldIncUser> hUsers = new HashMap(WSGameDefine.MAP_INITIAL_CAPACITY);
    
    private final HashMap<String, UserInfo> hUserInfo = new HashMap(WSGameDefine.MAP_INITIAL_CAPACITY);
    
//    private final HashMap<String, ArrayList<GameObjectMap>> hBuildings = new HashMap<>();
//    private final HashMap<String, ArrayList<GameObjectMap>> hEquipment = new HashMap<>();
//    private final HashMap<String, ArrayList<GameObjectMap>> hTransportation = new HashMap<>();
//    private final HashMap<String, ArrayList<GameObjectMap>> hExploration = new HashMap<>();
//    private final HashMap<String, ArrayList<Worker>> hWorker = new HashMap<>();
    
    private final HashMap<Long, SocialAlliance> hAlliances = new HashMap(WSGameDefine.MAP_INITIAL_CAPACITY); 
    
    private final HashMap<String, String> hashForgotPasswd = new HashMap<>();
    
    private final String logName = this.getClass().getSimpleName().toLowerCase();   
    
    private GoldIncUserMng(){}
    
    private void debug(String data){
        HadoopProvider.i().writeLog(logName, Func.now() + "\n" + data);
    }
    
    public void initDefault(){
        try {
            short regionId = 0;
            short zoneID = 0;
            int id = 1;
            GoldIncUser u = null;
            while(id<=3){               
                u = new GoldIncUserBuilder()
                    .setUserID(String.format("snipertest00%d@gmail.com", id))
                    .setPassword("123")
                    .setCompanyName(String.format("company %d", id))
                    .build(); 
                u.setPlayerProfileTest();
                if(u.save(DB.i().con())>0){
                    synchronized (hUsers) {hUsers.put(u.userID, u);}       
                    u.getPlayerProfile().getPrivate().setHomeBase(StaticDataMng.i().getSpot(u.userID, regionId, zoneID));                     
                    u.setPlayerProfile();
                    u.save(DB.i().con());
                    u.initToTest();
                }                
                id ++;
            }
            for(int i=0;i<200;i++){
                u = new GoldIncUserBuilder()
                    .setUserID(String.format("test%06d@gmail.com", i))
                    .setPassword("123")
                    .setCompanyName(String.format("company %06d", i))
                    .build(); 
                u.setPlayerProfileTest();
                if(u.save(DB.i().con())>0){
                    synchronized (hUsers) {hUsers.put(u.userID, u);}       
                    u.getPlayerProfile().getPrivate().setHomeBase(StaticDataMng.i().getSpotRandom(u.userID)); 
                    u.setPlayerProfile();
                    u.save(DB.i().con());
                    u.initToTest();
                } 
            }
        } catch (Exception ex) {
            debug(Func.toString(ex));
        }
    }
     
    public void loadAll() {
        try {
            ArrayList<GoldIncUser> lstUser = DB.i().select(GoldIncUser.class);   
            hUsers.clear();         
            synchronized(LOCK){
                lstUser.forEach((u) -> {
                    u.parse();                    
                    if(!"".equals(u.userID)){
                        hUsers.put(u.userID, u);
                    }
                });
            }
            ArrayList<SocialAlliance> lstAlliance = DB.i().select(SocialAlliance.class);     
            hAlliances.clear();       
            synchronized(LOCK){
                lstAlliance.forEach((g) -> {
                    hAlliances.put(g.id, g);
                });
            }
            hUserInfo.clear();
            //building
            DB.i().select(Building.class, (Building t) -> {
                LoadingDataThread.i().setTimeLoading(this.getClass().getSimpleName(), System.currentTimeMillis());                
                UserInfo uif = hUserInfo.get(t.userID);
                if(uif==null){
                    uif = new UserInfo(t.userID);
                    hUserInfo.put(t.userID, uif);
                }
                uif.addBuilding(t.parseData());
            });
            // euipment
            DB.i().select(Equipment.class, (Equipment t) -> {
                LoadingDataThread.i().setTimeLoading(this.getClass().getSimpleName(), System.currentTimeMillis());
                UserInfo uif = hUserInfo.get(t.userID);
                if(uif==null){
                    uif = new UserInfo(t.userID);
                    hUserInfo.put(t.userID, uif);
                }
                uif.addEquipment(t.parseData());
            });
            // exploration
            DB.i().select(Exploration.class, (Exploration t) -> {
                LoadingDataThread.i().setTimeLoading(this.getClass().getSimpleName(), System.currentTimeMillis());                
                if (t.endTime > System.currentTimeMillis()) {                    
                    UserInfo uif = hUserInfo.get(t.userID);
                    if(uif==null){
                        uif = new UserInfo(t.userID);
                        hUserInfo.put(t.userID, uif);
                    }
                    uif.addExploration(t);
                }
            });
            // transportation
            DB.i().select(Transportation.class, (Transportation t) -> {
                LoadingDataThread.i().setTimeLoading(this.getClass().getSimpleName(), System.currentTimeMillis());
                t.parseJson();                   
                UserInfo uif = hUserInfo.get(t.userID);
                if(uif==null){
                    uif = new UserInfo(t.userID);
                    hUserInfo.put(t.userID, uif);
                }
                uif.addTransportation(t);
            });
            // worker
            DB.i().select(Worker.class, (Worker t) -> {
                LoadingDataThread.i().setTimeLoading(this.getClass().getSimpleName(), System.currentTimeMillis());                   
                UserInfo uif = hUserInfo.get(t.userID);
                if(uif==null){
                    uif = new UserInfo(t.userID);
                    hUserInfo.put(t.userID, uif);
                }
                uif.addWorker(t);
            });
            // claim
            DB.i().select(Claim.class, (Claim t) -> {
                LoadingDataThread.i().setTimeLoading(this.getClass().getSimpleName(), System.currentTimeMillis());                   
                UserInfo uif = hUserInfo.get(t.userID);
                if(uif==null){
                    uif = new UserInfo(t.userID);
                    hUserInfo.put(t.userID, uif);
                }
                uif.addClaim(t);
            });
        } catch (SQLException ex) {
            debug(Func.toString(ex));
        }
    }
    
    public void syncUserInfo(){        
        Entry<String, UserInfo>[] tUserInfo;
        synchronized(LOCK){tUserInfo = hUserInfo.entrySet().toArray(WSGameDefine.EMPTY_ENTRY);}
        for(Entry<String, UserInfo> e:tUserInfo){
            GoldIncUser user = hUsers.get(e.getKey());
            UserInfo uif = e.getValue();
            if(user!=null){
                ProfilePrivate pp = user.getPlayerProfile().getPrivate();
                pp.setListBuilding(uif.getBuildings());
                pp.setListEquipment(uif.getEquipments());
                pp.setListTransportation(uif.getTransportations());
                pp.setListExploration(uif.getExplorations());
                pp.setListWorker(uif.getWorkers());
                pp.setListClaim(uif.getClaims());
            }
        }
    }
    
    public void refClaimToMine(){               
        synchronized(LOCK){
            for(GoldIncUser user:hUsers.values()){
                user.getPlayerProfile().getPrivate().synReference();
            }
        }
    }
    
    public String[] getUserIDs(){
        String[] temp;
        synchronized(hUsers){temp = hUsers.keySet().toArray(WSGameDefine.EMPTY_STRING);}
        return temp;
    }
    
    public GoldIncUser getUser(String key){return hUsers.get(key);}
    
    public GoldIncUser[] getUsers(){
        GoldIncUser[] temp;
        synchronized(hUsers){temp = hUsers.values().toArray(WSGameDefine.EMPTY_USER);}
        return temp;
    }
    
    public boolean isAvailableAllianceName(String name){
        SocialAlliance[] temp;
        synchronized(hAlliances){temp = hAlliances.values().toArray(WSGameDefine.EMPTY_USER_GROUP);}
        for(SocialAlliance sa:temp){if(sa.name.equals(name)){return false;}}
        return true;
    }
    
    public int addAlliance(SocialAlliance sa){
        if(hAlliances.containsKey(sa.id)){return -1;}
        synchronized(hAlliances){hAlliances.put(sa.id, sa);}
        return 0;
    }
    
    public SocialAlliance getAlliance(long key){return hAlliances.get(key);}
    
    public ArrayList<String> getUserInAlliance(SocialAlliance social){
        GoldIncUser[] temp;
        synchronized(hUsers){temp = hUsers.values().toArray(WSGameDefine.EMPTY_USER);}
        ArrayList<String> rs = new ArrayList();
        if(hUsers.containsKey(social.leader)){
            rs.add(social.leader);
            social.coLeaders.stream().filter((mem) -> (hUsers.containsKey(mem))).forEachOrdered((mem) -> {
                rs.add(mem);
            });
            for(GoldIncUser u:temp){
                String key = u.userID;
                if(u.allianceID==social.id && !key.equals(social.leader) && !social.coLeaders.contains(key)){
                    rs.add(u.userID);
                }
            }
        }
        return rs;
    }
    
    public PlayerProfile getUResourceObj(String userID){
        if(userID == null || !userID.isEmpty()){return null;}
        GoldIncUser u = null;
        String key = userID;
        u = hUsers.get(key);
        if (u == null) {
            // chưa có guid trên ram
            u = new GoldIncUser();
            u.getPlayerProfile().userID = userID;
            u.setPlayerProfile();
            synchronized (hUsers) {
                hUsers.put(key, u);
            }            
        }
        return u.getPlayerProfile();
    }
    
    public byte[] getUResourceBytes(String userID){
        if(userID == null || !userID.isEmpty()){return null;}
        GoldIncUser u = null;
        String key = userID;
        u = hUsers.get(key);
        if (u == null) {
            // chưa có guid trên ram
            u = new GoldIncUser();
            u.getPlayerProfile().userID = userID;
            u.setPlayerProfile();
            synchronized (hUsers) {
                hUsers.put(key, u);
            }            
        }
        return u.profile;
    }
    
    public boolean isUserInAlliance(GoldIncUser user, long id){
        SocialAlliance g = hAlliances.get(id);
        if(g==null){return false;}
        else{
            GoldIncUser[] ls;            
            synchronized(hUsers){ls = hUsers.values().toArray(WSGameDefine.EMPTY_USER);}
            synchronized(hUsers){
                for(GoldIncUser u:ls){
                    if(u.allianceID==id && u.userID.equals(user.userID)){
                        return true;
                    }
                }
            }            
            return false;
        }        
    }
    
    public void saveAll() {
        GoldIncUser[] ls;
        SocialAlliance[] lsGroup;
        synchronized(hUsers){
            ls = hUsers.values().toArray(WSGameDefine.EMPTY_USER);
        }
        synchronized(hAlliances){
            lsGroup = hAlliances.values().toArray(WSGameDefine.EMPTY_USER_GROUP);
        }
        synchronized(LOCK){
            for(GoldIncUser u:ls){
                try {
                    u.save(DB.i().con());
                } catch (SQLException ex) {
                    
                }
            }
            for(SocialAlliance g:lsGroup){
                try {
                    g.save(DB.i().con());
                } catch (SQLException ex) {
                    
                }
            }
        }
    }
    
    public void saveUser(GoldIncUser user){
        
        String key = user.userID;
        
        GoldIncUser u = hUsers.get(key);
        if(u!=null){
            // save
//            u.updateInfo(logName, user);
        } else{
            // add
            u = user;
            synchronized(hUsers){
                hUsers.put(key, u);
            }           
        }
        try{
            u.save(DB.i().con());            
        } catch(SQLException ex){}
        
        if(u.allianceID>0){
            // save
            if(!isUserInAlliance(u, u.allianceID)){
                // update numberOfMember
                SocialAlliance g = hAlliances.get(u.allianceID);
                synchronized(g){g.numberOfMember++;}
                try{g.save(DB.i().con());} catch(SQLException ex){}
            }
        }
    }
    
    ////// place for registry
    public String addVerifyAndEmail(String email, String passwd, String companyName){
        String msg = "";
        if(hUsers.containsKey(email)){
            msg = "Email exists in system.";
        } else{                
            String id = UUID.randomUUID().toString().replace("-", "");
            // add DB            
            try{
//                RegistryAccount acc = new RegistryAccount(id, email, Utils.i().sha256(passwd), companyName);
//                acc.save(DB.i().con());
                RegistryAccount acc = new RegistryAccountBuilder()
                        .setId(id)
                        .setEmail(email)
                        .setPassword(email)
                        .setCompanyName(email)
                        .buildAndSave();                
                if(acc==null){
                    msg = "This system has some error ON Database";
                } else{
                    String url = WSGameDefine.HOST +"/goldinc/service/verify_account/?data=" + id;
                    String content = SendEmailUtil.i().getContentTemplate();
                    content = content.replace("#email", email).replace("#link_verify", url);
                    SendEmailUtil.i().sendEmail(email, content);  
                    msg = "Registry account is successful. To check your email to verify this account, please.";
                }
            } catch(SQLException ex){
                msg = "Sorry, This system has some error - " + ex.getMessage();
            }
        }
        
        return msg;
    }
    
    public String verifyEmail(String key){
        String msg = "";
        try {
            ArrayList<RegistryAccount> ls = DB.i().select(RegistryAccount.class, "where id=?", new Object[]{key});            
            if(ls.size()>0){
                RegistryAccount acc = ls.get(0);
//                GoldIncUser u = new GoldIncUser();
//                u.getPlayerProfile().userID = acc.email;
//                u.password = acc.password;
//                u.companyName = acc.companyName;
//                u.setPlayerProfile();
                GoldIncUser u = new GoldIncUserBuilder()
                    .setUserID(acc.email)
                    .setPassword(acc.password)
                    .setCompanyName(acc.companyName)
                    .build();                
                if(u.save(DB.i().con())>0){
                    synchronized (hUsers) {
                        hUsers.put(acc.email, u);
                    }
                    acc.delete(DB.i());
                    u.setHomeBase(StaticDataMng.i().getSpotRandom(acc.email));                    
                }
                msg = "This account verified successful, so you can play game."
                        + "\nTo login with this email [" + acc.email + "] "
                        + "and your password to play the game. Enjoying!" ;
                
            } else{
                msg = "This account is verified.";
            }
            
        } catch (Exception ex) {
            msg = "Sorry, This system has some error.\n" + Func.toString(ex);
        }
        
        return msg;
    } 
    
    private String getKey(String email){
        String rs = "";
        for(Entry<String, String> e:hashForgotPasswd.entrySet()){
            if(e.getValue().equals(email)){
                rs = e.getKey();
            }
        }
        return rs;
    }
    
    public String getEmail(String key){
        String rs = hashForgotPasswd.get(key);
        return rs;
    }
    
    public String forgotPasswd(String email){
        String msg = "";
        if(!hUsers.containsKey(email)){
            msg = "Email does not exist in system.";
        } else{                
            GoldIncUser u = hUsers.get(email);            
            try{
                if(u!=null){
                    String tempKey = getKey(email);
                    if(tempKey.isEmpty()){
                        tempKey = UUID.randomUUID().toString().replaceAll("-", "");
                        synchronized(hashForgotPasswd){
                            hashForgotPasswd.put(tempKey, email);
                        }
                    }
                    
                    String temp = "";
                    temp+= "\nPlease to click this page to change password: " 
                    + WSGameDefine.HOST + "/goldinc/service/change_password/" + tempKey;
                    
                    String content = SendEmailUtil.i().getContentTemplate();
                    content = content.replace("#email", email).replace("#link_verify", temp);
                    String rs = SendEmailUtil.i().sendEmail(email, content);  
                    
                    if(rs.isEmpty()){
                        msg = "New your password is sent to your email [" + email + "]. To check your email, please.";
                    } else{
                        msg = rs;
                    }
                    
                } else{
                    msg = "Sorry, " + email + " is not available in system.";
                }
            } catch(Exception ex){
                msg = "Sorry, This system has some error - " + ex.getMessage();
            }
        }
        
        return msg;
    }
        
    public String changePasswd(String key, String newPasswd){
        String msg = "";
        if(!hashForgotPasswd.containsKey(key)){
            msg = "Email does not exist in system.";
        } else{   
            String email = hashForgotPasswd.get(key);
            GoldIncUser u = hUsers.get(email);            
            try{
                String temp = Utils.i().sha256(newPasswd);
                u.setPassword(temp);
                if (u.save(DB.i().con()) > 0) {
                    msg = "Changing password is successful.";
                    hashForgotPasswd.remove(key);
                } else {
                    msg = "Sorry, This system has some error - Can not insert data";
                }               
            } catch(SQLException ex){
                msg = "Sorry, This system has some error - " + ex.getMessage();
            }
        }        
        return msg;
    }
}
