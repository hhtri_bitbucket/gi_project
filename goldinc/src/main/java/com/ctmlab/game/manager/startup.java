/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ctmlab.game.manager;
import com.ctmlab.game.threads.ZoneManager;
import com.ctmlab.game.threads.TransportThread;
import com.ctmlab.game.threads.BuildingUpgradeThread;
import com.ctmlab.game.threads.LoadingDataThread;
import com.ctmlab.game.threads.MessageThread;
import com.ctmlab.game.threads.GameStateThread;
import com.ctmlab.game.threads.WSCheckPing;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;
/**
 *
 * @author hhtri
 */
@WebListener()
public class startup implements ServletContextListener {

    final Object LOCK = new Object();
    
    private void mSleep(int time) {
        try {
            synchronized (LOCK) {
                LOCK.wait(time);
            }
        } catch (InterruptedException ex) {
        }
    }
    
    @Override
    public void contextInitialized(ServletContextEvent sce) {  
        // loading
        LoadingDataThread.i().startMng();        
        mSleep(3000);
        
        // starting all threads in game
        WSCheckPing.i().startCheck();
        ZoneManager.i().startZoneMng();
        MessageThread.i().startMsgSenderMng();
        TransportThread.i().startMng();
        BuildingUpgradeThread.i().startMng();
        GameStateThread.i().startMng();
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        LoadingDataThread.i().stopMng();
        WSCheckPing.i().stopCheck();
        ZoneManager.i().stopZoneMng();
        MessageThread.i().stopMsgSenderMng();
        BuildingUpgradeThread.i().stopMng();
        GameStateThread.i().stopMng();
        GoldIncMng.i().close();
    }
    
}
