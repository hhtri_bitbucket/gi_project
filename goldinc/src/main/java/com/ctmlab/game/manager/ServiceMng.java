/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ctmlab.game.manager;

import com.ctmlab.game.threads.ZoneManager;
import com.ctmlab.game.threads.MessageThread;
import com.ctmlab.game.data.builder.GameMessageBuilder;
import com.ctmlab.game.data.builder.SocialChatBuilder;
import com.ctmlab.game.data.ctmon.CTMON;
import com.ctmlab.game.data.model.Exploration;
import com.ctmlab.game.data.model.GameMessage;
import com.ctmlab.game.data.model.GameObjectMap;
import com.ctmlab.game.data.model.GoldIncUser;
import com.ctmlab.game.data.model.Mine;
import com.ctmlab.game.data.model.OilMine;
import com.ctmlab.game.data.model.SocialAlliance;
import com.ctmlab.game.data.model.SocialChat;
import com.ctmlab.game.data.model.Transportation;
import com.ctmlab.game.data.model.base.GameMessageBase;
import com.ctmlab.game.data.model.building.Building;
import com.ctmlab.game.data.pojo.GameResources;
import com.ctmlab.game.data.pojo.PlayerProfile;
import com.ctmlab.game.data.pojo.ServiceResponse;
import com.ctmlab.game.data.pojo.TradeTransaction;
import com.ctmlab.game.data.staticdata.Region;
import com.ctmlab.game.data.staticdata.Zone;
import com.ctmlab.game.goldincenum.ENUM_CHAT;
import com.ctmlab.game.goldincenum.ENUM_MESSAGE;
import com.ctmlab.game.goldincenum.ENUM_UNIT_TYPE;
import com.ctmlab.game.goldincenum.GI_SERVER_CMD;
import com.ctmlab.game.hadoop.HadoopProvider;
import com.ctmlab.game.hadoop.logs.TradeCenterLogMng;
import com.ctmlab.game.network.client.WSMicroServiceClient;
import com.ctmlab.game.network.client.WSServiceClient;
import com.ctmlab.game.network.manager.WSGameDefine;
import com.ctmlab.game.network.manager.WSServiceServer;
import com.ctmlab.game.templates.MessageTmpl;
import com.ctmlab.game.threads.TransportThread;
import com.ctmlab.util.SendEmailUtil;
import com.ctmlab.util.Utils;
import com.dp.db.DB;
import com.dp.db.Func;
import com.google.common.io.LittleEndianDataOutputStream;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonSyntaxException;
import oracle.net.aso.e;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.TreeMap;

/**
 *
 * @author hhtri
 */
public class ServiceMng {

    public static final Object LOCK = new Object();
    public final int SUCCESS_RES = 0;
    public final int FAIL_RES = 1;
    public final int EMAIL_NA = 1003;
    public final int EXCEPTION_RES = 1001;
    public final int INVALID_ACTION = 1002;

    private static ServiceMng inst;

    public static ServiceMng i() {
        synchronized (LOCK) {
            if (inst == null) {
                inst = new ServiceMng();
            }
            return inst;
        }
    }

    private final String mLogName = this.getClass().getSimpleName().toLowerCase();
    private final StringBuilder mBuffLog = new StringBuilder();
    private final HashMap<String, JsonObject> hTemplateMsg = new HashMap();

    // <editor-fold defaultstate="collapsed" desc="Private Methods">
    private ServiceMng() {
        initTemplateMsg();
    }

    private void initTemplateMsg() {
        JsonObject obj = null;
        obj = new JsonObject();
        obj.addProperty("subject", "Inviting to alliance");
        obj.addProperty("content", "Hi #receiver, #sender in alliance want to invite you to join their alliance.");
        hTemplateMsg.put("invite", obj);

        obj = new JsonObject();
        obj.addProperty("subject", "Join alliance");
        obj.addProperty("content", "Hi #receiver, #sender want to join to your alliance.");
        hTemplateMsg.put("join", obj);
    }

    private void debug(String data) {
        HadoopProvider.i().writeLog(mLogName, Func.now() + "\n" + data);
    }

    private boolean isSignin(GoldIncUser user) {
        if (user == null || !user.isLogin) {
            return false;
        } else {
            WSServiceClient client = WSServiceServer.i().sClients.get(user.userID);
            if (client == null) {
                user.isLogin = false;
                return false;
            } else {
                client = WSServiceServer.i().iClients.get(client.getId());
                if (client == null) {
                    user.isLogin = false;
                    return false;
                }
                return true;
            }
        }
    }

    private void debug(String method, Throwable ex, String describe, Object... params) {
        synchronized (mBuffLog) {
            mBuffLog.setLength(0);
            String sTemp = "";
            for (Object o : params) {
                if (!sTemp.isEmpty()) {
                    sTemp += ",";
                }
                sTemp += String.valueOf(o);
            }
            mBuffLog.append(String.format("\n**********\n[%s]\n[%s]\n[%s]\n%s\n",
                    method, describe, sTemp, ex == null ? "" : Func.toString(ex)));
            debug(mBuffLog.toString());
        }
    }

    private void debug(String method, Throwable ex, StringBuilder sb, String describe, Object... params) {
        synchronized (sb) {
            String sTemp = "";
            for (Object o : params) {
                if (!sTemp.isEmpty()) {
                    sTemp += ",";
                }
                sTemp += String.valueOf(o);
            }
            sb.append(String.format("\n**********\n[%s]\n[%s]\n[%s]\n%s\n",
                    method, describe, sTemp, ex == null ? "" : Func.toString(ex)));
        }
    }

    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="HTTP API Methods">
    public ServiceResponse getDefaultResponse(String action) {

        ServiceResponse res = new ServiceResponse();

        res.setReturnCode(INVALID_ACTION);
        res.setMessage(action + " is not available.");
        res.setData(WSGameDefine.EMPTY_BYTES);

        return res;
    }

    public ServiceResponse getCashConverter() {
        ServiceResponse res = new ServiceResponse();
        LittleEndianDataOutputStream os;
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        os = new LittleEndianDataOutputStream(baos);
        CTMON.i().toStream(os, StaticDataMng.i().cashConvert);
        res.setReturnCode(SUCCESS_RES);
        res.setMessage("");
        res.setData(baos.toByteArray());
        baos.reset();
        return res;
    }

    public ServiceResponse getGameObjectInfos() {
        ServiceResponse res = new ServiceResponse();
        res.setReturnCode(SUCCESS_RES);
        res.setMessage("");
        res.setData(StaticDataMng.i().getRepoGameObjectInfo().getBytes());
        return res;
    }

    public ServiceResponse getCashConverterJson() {
        ServiceResponse res = new ServiceResponse();
        res.setReturnCode(SUCCESS_RES);
        res.setMessage(Func.json(StaticDataMng.i().cashConvert.getJsonObject()));
        res.setData(WSGameDefine.EMPTY_BYTES);
        return res;
    }

    public ServiceResponse saveCashConverter(String json) {
        ServiceResponse res = new ServiceResponse();
        String msg = "";
        try {
            msg = StaticDataMng.i().cashConvert.saveData(Func.gson.fromJson(json, JsonArray.class));
        } catch (JsonSyntaxException ex) {
            msg = Func.toString(ex);
        }
        res.setReturnCode(SUCCESS_RES);
        res.setMessage(msg);
        res.setData(WSGameDefine.EMPTY_BYTES);
        return res;
    }

    public ServiceResponse getMapExtra() {

        ServiceResponse res = new ServiceResponse();

        LittleEndianDataOutputStream os;
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        os = new LittleEndianDataOutputStream(baos);
        CTMON.i().toStream(os, StaticDataMng.i().worldMap);
        res.setReturnCode(SUCCESS_RES);
        res.setMessage("");
        res.setData(baos.toByteArray());
        baos.reset();

        return res;
    }

    public ServiceResponse getMap() {

        ServiceResponse res = new ServiceResponse();

        LittleEndianDataOutputStream os;
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        os = new LittleEndianDataOutputStream(baos);
        CTMON.i().toStream(os, StaticDataMng.i().worldMapStatic);
        res.setReturnCode(SUCCESS_RES);
        res.setMessage("");
        res.setData(baos.toByteArray());
        baos.reset();

        return res;
    }

    public ServiceResponse getRegionStatic(short id) {

        ServiceResponse res = new ServiceResponse();

        LittleEndianDataOutputStream os;
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        os = new LittleEndianDataOutputStream(baos);

        Region r = StaticDataMng.i().worldMap.getRegion(id);

        CTMON.i().toStream(os, r);

        res.setReturnCode(SUCCESS_RES);
        res.setMessage("");
        res.setData(baos.toByteArray());
        baos.reset();

        return res;
    }

    public ServiceResponse regAccount(String email, String passwd, String companyName) {

        ServiceResponse res = new ServiceResponse();

        String msg = "";
        int returnCode = SUCCESS_RES;
        res.setData(WSGameDefine.EMPTY_BYTES);

        if (!SendEmailUtil.i().isValid(email)) {
            returnCode = ServiceMng.i().EMAIL_NA;
            msg = "Email is not available";
        } else if (passwd == null || passwd.isEmpty() || companyName == null || companyName.isEmpty()) {
            returnCode = ServiceMng.i().FAIL_RES;
            msg = "password and company name must not be empty.";
        } else {
            msg = GoldIncUserMng.i().addVerifyAndEmail(email, passwd, companyName);
        }
        res.setReturnCode(returnCode);
        res.setMessage(msg);

        return res;
    }

    public String verifyEmail(String key) {
        return GoldIncUserMng.i().verifyEmail(key);
    }

    public ServiceResponse forgotPasswd(String email) {
        ServiceResponse res = new ServiceResponse();

        String msg = "";
        int returnCode = SUCCESS_RES;
        res.setData(WSGameDefine.EMPTY_BYTES);

        if (!SendEmailUtil.i().isValid(email)) {
            returnCode = ServiceMng.i().EMAIL_NA;
            msg = "Email is not available";
        } else {
            msg = GoldIncUserMng.i().forgotPasswd(email);
        }
        res.setReturnCode(returnCode);
        res.setMessage(msg);

        return res;
    }

    public ServiceResponse changePasswd(String key, String newPasswd) {
        ServiceResponse res = new ServiceResponse();
        int returnCode = SUCCESS_RES;
        res.setData(WSGameDefine.EMPTY_BYTES);
        String msg = GoldIncUserMng.i().changePasswd(key, newPasswd);
        res.setReturnCode(returnCode);
        res.setMessage(msg);

        return res;
    }

    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="Websocket Methods">
    public void resPing(WSServiceClient client, long time) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        try (LittleEndianDataOutputStream os = new LittleEndianDataOutputStream(baos)) {
            os.writeShort(GI_SERVER_CMD.RES_PING.getCode());
            os.writeLong(time);
            client.send(baos.toByteArray());
        } catch (Exception ex) {
        }

    }

    public int signIn(WSServiceClient client, StringBuilder sb, String accID, String passwd) {

        int returnCode = SUCCESS_RES;
        GoldIncUser user = GoldIncUserMng.i().getUser(accID);

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        byte[] data = WSGameDefine.EMPTY_BYTES;
        try (LittleEndianDataOutputStream os = new LittleEndianDataOutputStream(baos)) {
            os.writeShort(GI_SERVER_CMD.RES_SIGN_IN.getCode());
            if (user == null) {
                // not exist
                os.writeByte(3);
                returnCode = FAIL_RES;
            } else if (isSignin(user)) {
                // already signin
                os.writeByte(2);
                returnCode = FAIL_RES;
            } else if (!user.password.equals(Utils.i().sha256(passwd))) {
                // wrong password
                os.writeByte(1);
                returnCode = FAIL_RES;
            } else {
                // success
                os.writeByte(0);
                user.getPlayerProfile().getProfileBin(os, false);
                user.isLogin = true;
                user.save(DB.i().con());
            }
            data = baos.toByteArray();
            debug("signIn", null, sb, "userid, returnCode, Bytes", accID, returnCode, data.length);
        } catch (Exception ex) {
            debug("signIn", ex, sb, "userid, Exception", accID);
        }
        client.send(data);

        return returnCode;
    }

    public void getUserResource(WSServiceClient client, StringBuilder sb, String otherUserID) {
        GoldIncUser user = GoldIncUserMng.i().getUser(otherUserID);
        byte[] data = WSGameDefine.EMPTY_BYTES;
        if (user != null && user.getPlayerProfile().getPrivate().getGameResources() != null) {
            GameResources gRes = user.getPlayerProfile().getPrivate().getGameResources();
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            try (LittleEndianDataOutputStream os = new LittleEndianDataOutputStream(baos)) {
                os.writeShort(GI_SERVER_CMD.RES_USER_RESOURCE.getCode());
                gRes.writeData(os);
                data = baos.toByteArray();
                debug("getUserResource", null, sb, "UserID, Other UserID, JSON Resource", client.getUserID(), otherUserID, Func.json(gRes));
            } catch (Exception ex) {
                debug("getUserResource", ex, sb, "UserID, Other UserID, Exception", client.getUserID(), otherUserID);
            }
        }
        if (data.length > 0) {
            client.send(data);
        }
    }

    public void setNameClient(WSServiceClient client, StringBuilder sb, String name) {

        GoldIncUser user = GoldIncUserMng.i().getUser(client.getUserID());
        boolean flag = false;
        if (user != null) {
            GoldIncUser[] lst = GoldIncUserMng.i().getUsers();
            flag = true;
            for (GoldIncUser u : lst) {
                if (u.getPlayerProfile().name.equals(name)) {
                    flag = false;
                    break;
                }
            }
            if (flag) {
                flag = user.setGameName(name, true);
            }
        }
        byte[] data = WSGameDefine.EMPTY_BYTES;
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        try (LittleEndianDataOutputStream os = new LittleEndianDataOutputStream(baos)) {
            os.writeShort(GI_SERVER_CMD.RES_SET_NAME.getCode());
            os.writeBoolean(flag);
            data = baos.toByteArray();
            client.send(data);
            debug("setNameClient", null, sb, "UserID, name, result", client.getUserID(), name, flag);
        } catch (Exception ex) {
            debug("setNameClient", ex, sb, "UserID, name, exception", client.getUserID(), name);
        }

    }

    public void updateWalletClient(WSServiceClient client, StringBuilder sb) {

        GoldIncUser user = GoldIncUserMng.i().getUser(client.getUserID());
        boolean flag = false;
        if (user != null) {
            flag = user.updateWallet(true, true);
        }
        byte[] data = WSGameDefine.EMPTY_BYTES;
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        try (LittleEndianDataOutputStream os = new LittleEndianDataOutputStream(baos)) {
            os.writeShort(GI_SERVER_CMD.RES_UPDATE_WALLET.getCode());
            os.writeBoolean(flag);
            data = baos.toByteArray();
            client.send(data);
            debug("updateWalletClient", null, sb, "UserID, result", client.getUserID(), flag);
        } catch (Exception ex) {
            debug("updateWalletClient", ex, sb, "UserID, exception", client.getUserID());
        }
    }

    // <editor-fold defaultstate="collapsed" desc="Chat - Message Methods">
    // <editor-fold defaultstate="collapsed" desc="Message Methods">
    public void sendMsgGame(WSServiceClient client, StringBuilder sb, String sender, String receiver, String subject, String content, byte type) {

        GoldIncUser user = GoldIncUserMng.i().getUser(receiver);
        int rs = 1;
        GameMessageBase gm = null;
        if (user != null && !sender.equals(receiver)) {
            // save DB
            gm = new GameMessageBuilder()
                    .setSender(sender)
                    .setReceiver(receiver)
                    .setSubject(subject)
                    .setContent(content)
                    .setType(type)
                    .build();
            try {
                rs = gm.save(DB.i().con()) > 0 ? 0 : 1;
            } catch (SQLException ex) {
            }
        }
        try {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            try (LittleEndianDataOutputStream os = new LittleEndianDataOutputStream(baos)) {
                if (type == ENUM_MESSAGE.GAME_MSG.getCode()) {
                    os.writeShort(GI_SERVER_CMD.RES_SEND_MESSAGE.getCode());
                } else if (type == ENUM_MESSAGE.INVITATION_MSG.getCode()) {
                    os.writeShort(GI_SERVER_CMD.RES_INVITE_ALLIANCE.getCode());
                }
                os.writeByte(rs);
                byte[] data = baos.toByteArray();
                client.send(data);
                baos.reset();
                debug("sendMsgGame", null, sb, "sender, receiver, subject, content, type, result",
                        sender, receiver, subject, content, ENUM_MESSAGE.get(type).toString(), rs);
                ////////////////////////////////////////////////////////////////
                if (gm != null && rs == 0) {
                    gm.writeData(os);
                    MessageThread.i().addData(receiver, gm);
                    debug("sendMsgGame", null, sb, "Message", "Add to message sender for " + receiver);
                }
            }
        } catch (IOException ex) {
            debug("sendMsgGame", ex, sb, "sender, receiver, subject, content, type, exception",
                    sender, receiver, subject, content, ENUM_MESSAGE.GAME_MSG.toString());
        }
    }

    public void getMsgGame(WSServiceClient client, StringBuilder sb) {
        try {

            ArrayList<GameMessage> lstGM = DB.i().select(GameMessage.class,
                    "where receiver=? order by mtime DESC",
                    new Object[]{client.getUserID()});
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            try (LittleEndianDataOutputStream os = new LittleEndianDataOutputStream(baos)) {
                os.writeShort(GI_SERVER_CMD.RES_GET_MESSAGE.getCode());
                os.writeInt(lstGM.size());
                for (GameMessage gm : lstGM) {
                    gm.writeData(os);
                }
            }
            byte[] data = baos.toByteArray();
            baos.reset();
            client.send(data);
            debug("getMsgGame", null, sb, "userid, size, length", client.getUserID(), lstGM.size(), data.length);

            ///////////////////////////////////////
//            for (GameMessage gm : lstGM) {
//                gm.mState = (byte) WSGameDefine.READ_STATE_MSG;
//                if(gm.exData==null){
//                    gm.exData = WSGameDefine.EMPTY_BYTES;
//                }
//                gm.save(DB.i().con());
//            }
        } catch (IOException | SQLException ex) {
            debug("getMsgGame", ex, sb, "userid, exception", client.getUserID());
        }
    }

    public void setReadStateMsg(WSServiceClient client, StringBuilder sb, long id) {
        try {
            byte state = WSGameDefine.READ_STATE_MSG;
            String sql = "UPDATE GameMessage SET mState=? WHERE id=?";
            int rs = DB.i().executeUpdate(sql, state, id);
            debug("setReadStateMsg", null, sb, "userid, id, state, result", client.getUserID(), id, state, rs);
        } catch (SQLException ex) {
            debug("setReadStateMsg", ex, sb, "userid, id, exception", client.getUserID(), id);
        }
    }

    public void rmMsgGame(WSServiceClient client, StringBuilder sb, long id) {
        try {
            String sql = "DELETE FROM GameMessage WHERE id=?";
            int rs = DB.i().executeUpdate(sql, id);
            debug("rmMsgGame", null, sb, "userid, id, state, result", client.getUserID(), id, rs);
        } catch (SQLException ex) {
            debug("rmMsgGame", ex, sb, "userid, id, exception", client.getUserID(), id);
        }
    }

    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="Chat Methods">
    public void inviteAlliance(WSServiceClient client, StringBuilder sb, String userID, String subject, String content) {
        String sender = client.getUserID();
        String receiver = userID;
        sendMsgGame(client, sb, sender, receiver, subject, content, ENUM_MESSAGE.INVITATION_MSG.getCode());
    }

    public void createAlliance(WSServiceClient client, String name) {
        try {
            GoldIncUser curUser = GoldIncUserMng.i().getUser(client.getUserID());
            int rs = 0;
            SocialAlliance sa = null;
            if (!GoldIncUserMng.i().isAvailableAllianceName(name)) {
                rs = -1;
            } else if (curUser.allianceID > 0) {
                rs = 1;
            } else if (curUser.allianceID < 0) {
                sa = new SocialAlliance(client.getUserID(), name);
                sa.saveData();
                curUser.setAllianceID(sa.id);
                curUser.save(DB.i().con());
                GoldIncUserMng.i().addAlliance(sa);
            }

            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            try (LittleEndianDataOutputStream os = new LittleEndianDataOutputStream(baos)) {
                os.writeShort(GI_SERVER_CMD.RES_CREATE_ALLIANCE.getCode());
                os.writeByte(rs);
                if (sa != null && rs == 0) {
                    sa.writeData(os);
                }
            }
            byte[] data = baos.toByteArray();
            client.send(data);
            debug("createAlliance", null, "UserID, name, result, bytes", client.getUserID(), name, rs, data.length);
        } catch (IOException | SQLException ex) {
            debug("createAlliance", ex, "UserID, name, exception", client.getUserID(), name);
        }

    }

    public void infoAlliance(WSServiceClient client, long id) {

        try {
            SocialAlliance sa = GoldIncUserMng.i().getAlliance(id);
            if (sa != null) {
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                try (LittleEndianDataOutputStream os = new LittleEndianDataOutputStream(baos)) {
                    os.writeShort(GI_SERVER_CMD.RES_INFO_ALLIANCE.getCode());
                    sa.writeData(os);
                }
                byte[] data = baos.toByteArray();
                debug("infoAlliance", null, client.getUserID(), id, data.length);
            }
        } catch (IOException ex) {
            debug("infoAlliance", ex, client.getUserID(), id);
        }
    }

    public void joinAlliance(WSServiceClient client, StringBuilder sb, long allianceID) {
        String sender = client.getUserID();
        SocialAlliance sa = GoldIncUserMng.i().getAlliance(allianceID);
        int rs = (sa != null) ? 0 : 1;
        if (sa != null) {
            // send msg to leader in alliance
            String receiver = sa.leader;
            String subject = MessageTmpl.inviteSubject;
            String content = MessageTmpl.inviteContent;
            sendMsgGame(client, sb, sender, receiver, subject, content, ENUM_MESSAGE.JOIN_ALLIANCE_MSG.getCode());
        }
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        try (LittleEndianDataOutputStream os = new LittleEndianDataOutputStream(baos)) {
            // for user added, <RES_JOIN_ALLIANCE><byte:rs><socialalliance data>.
            // rs == 0 client must read social alliance info.
            os.writeShort(GI_SERVER_CMD.RES_JOIN_ALLIANCE.getCode());
            os.writeByte(rs);
            client.send(baos.toByteArray());
            debug("joinAlliance", null, sb, "userid, id, result", client.getUserID(), allianceID, rs);
        } catch (Exception ex) {
            debug("joinAlliance", ex, sb, "userid, id, exception", client.getUserID(), allianceID);
        }
    }

    public void addMemberAlliance(WSServiceClient client, StringBuilder sb, String otherUserID, long msgID) {
        try {
            GoldIncUser curUser = GoldIncUserMng.i().getUser(client.getUserID());
            GoldIncUser addedUser = GoldIncUserMng.i().getUser(otherUserID);
            if (curUser != null && addedUser != null) {
                SocialAlliance sa = GoldIncUserMng.i().getAlliance(curUser.allianceID);
                if (sa != null && (curUser.userID.equals(sa.leader) || sa.coLeaders.contains(curUser.userID))) {

                    // set allianceID and save DB to user who is added
                    addedUser.setAllianceID(sa.id);
                    addedUser.save(DB.i().con());

                    ByteArrayOutputStream baos = new ByteArrayOutputStream();
                    try (LittleEndianDataOutputStream os = new LittleEndianDataOutputStream(baos)) {
                        // for user added, <RES_JOIN_ALLIANCE><byte:rs><socialalliance data>.
                        // rs == 0 client must read social alliance info.
                        os.writeShort(GI_SERVER_CMD.RES_JOIN_ALLIANCE.getCode());
                        os.writeByte(1);
                        sa.writeData(os);
                        WSServiceServer.i().sendDataWithID(otherUserID, baos.toByteArray());

                        baos.reset();
                        // for user in alliance, <RES_INFO_ALLIANCE><socialalliance data>.
                        os.writeShort(GI_SERVER_CMD.RES_INFO_ALLIANCE.getCode());
                        sa.writeData(os);
                        final byte[] data = baos.toByteArray();
                        debug("addMemberAlliance", null, "UserID, other UserID, Bytes", client.getUserID(), otherUserID, data.length);
                        sa.getMembers().stream()
                                .filter((member) -> (!member.equals(otherUserID)))
                                .forEachOrdered((member) -> {
                                    WSServiceServer.i().sendDataWithID(member, data);
                                });

                    }
                }
            }
        } catch (Exception ex) {
            debug("addMemberAlliance", ex, "UserID, other UserID, exception", client.getUserID(), otherUserID);
        }
    }

    public void rmMemberAlliance(WSServiceClient client, String otherUserID) {
        try {
            GoldIncUser curUser = GoldIncUserMng.i().getUser(client.getUserID());
            GoldIncUser removedUser = GoldIncUserMng.i().getUser(otherUserID);
            if (curUser != null && removedUser != null) {
                SocialAlliance sa = GoldIncUserMng.i().getAlliance(curUser.allianceID);
                if (sa != null && (curUser.userID.equals(sa.leader) || sa.coLeaders.contains(curUser.userID))) {

                    removedUser.allianceID = -1;
                    removedUser.save(DB.i().con());

                    ByteArrayOutputStream baos = new ByteArrayOutputStream();
                    try (LittleEndianDataOutputStream os = new LittleEndianDataOutputStream(baos)) {
                        os.writeShort(GI_SERVER_CMD.RES_INFO_ALLIANCE.getCode());
                        sa.writeData(os);
                    }
                    byte[] data = baos.toByteArray();
                    debug("rmMemberAlliance", null, client.getUserID(), otherUserID, data.length);

                    client.send(data);

                    sa.getMembers().stream()
                            .filter((member) -> (!member.equals(client.getUserID()) && WSServiceServer.i().containsClient(member)))
                            .forEachOrdered((member) -> {
                                WSServiceServer.i().sendDataWithID(member, data);
                            });
                }
            }
        } catch (Exception ex) {
            debug("rmMemberAlliance", ex, client.getUserID(), otherUserID);
        }
    }

    public void setCoLeaderAlliance(WSServiceClient client, String otherUserID) {
        try {
            GoldIncUser curUser = GoldIncUserMng.i().getUser(client.getUserID());
            GoldIncUser coleader = GoldIncUserMng.i().getUser(otherUserID);
            if (curUser != null && coleader != null) {
                SocialAlliance sa = GoldIncUserMng.i().getAlliance(curUser.allianceID);
                if (sa != null && (curUser.userID.equals(sa.leader))) {

                    if (sa.addCoLeader(otherUserID) == 0) {
                        ByteArrayOutputStream baos = new ByteArrayOutputStream();
                        try (LittleEndianDataOutputStream os = new LittleEndianDataOutputStream(baos)) {
                            os.writeShort(GI_SERVER_CMD.RES_INFO_ALLIANCE.getCode());
                            sa.writeData(os);
                        }
                        byte[] data = baos.toByteArray();
                        debug("rmMemberAlliance", null, client.getUserID(), otherUserID, data.length);

                        client.send(data);

                        sa.getMembers().stream()
                                .filter((member) -> (!member.equals(client.getUserID()) && WSServiceServer.i().containsClient(member)))
                                .forEachOrdered((member) -> {
                                    WSServiceServer.i().sendDataWithID(member, data);
                                });
                    }
                }
            }
        } catch (Exception ex) {
            debug("rmMemberAlliance", ex, client.getUserID(), otherUserID);
        }
    }

    public void chatAlliance(WSServiceClient client, String msg) {
        try {
            GoldIncUser curUser = GoldIncUserMng.i().getUser(client.getUserID());
            if (curUser != null) {
                SocialAlliance sa = GoldIncUserMng.i().getAlliance(curUser.allianceID);
                if (sa != null) {
                    // save chat to db
//                    SocialChat sc = new SocialChat(ENUM_CHAT.CHAT_ALLIANCE.getCode(), curUser.allianceID, curUser.userID, msg);
//                    sc.save(DB.i().con());
                    SocialChat sc = new SocialChatBuilder()
                            .setType(ENUM_CHAT.CHAT_ALLIANCE.getCode())
                            .setGroupID(curUser.allianceID)
                            .setSender(curUser.userID)
                            .setData(msg.getBytes(WSGameDefine.UTF_8))
                            .buildAndSave();
                    // add chat to buffer (buffer save limit 30 days chatting)
                    // send all alliance
                    ByteArrayOutputStream baos = new ByteArrayOutputStream();
                    try (LittleEndianDataOutputStream os = new LittleEndianDataOutputStream(baos)) {
                        os.writeShort(GI_SERVER_CMD.RES_CHAT_ALLIANCE.getCode());
                        sc.writeData(os);
                    }
                    byte[] data = baos.toByteArray();
                    debug("chatAlliance", null, "ID, name, UserID, msg, bytes", sa.id, sa.name, client.getUserID(), msg, data.length);
//
                    client.send(data);

                    sa.getMembers().stream()
                            .filter((member) -> (!member.equals(client.getUserID()) && WSServiceServer.i().containsClient(member)))
                            .forEachOrdered((member) -> {
                                WSServiceServer.i().sendDataWithID(member, data);
                            });
                }
            }
        } catch (Exception ex) {
            debug("chatAlliance", ex, client.getUserID(), msg);
        }
    }

    public void chatRegion(WSServiceClient client, String msg, short id_r) {
        try {
            GoldIncUser curUser = GoldIncUserMng.i().getUser(client.getUserID());
            Region region = StaticDataMng.i().worldMap.getRegion(id_r);
            HashSet<String> lstUser = region.lstUserID();

            if (curUser != null) {
//                SocialChat sc = new SocialChat(ENUM_CHAT.CHAT_REGION.getCode(), id_r, curUser.userID, msg);
                SocialChat sc = new SocialChatBuilder()
                        .setType(ENUM_CHAT.CHAT_REGION.getCode())
                        .setGroupID(id_r)
                        .setSender(curUser.userID)
                        .setData(msg.getBytes(WSGameDefine.UTF_8))
                        .buildAndSave();
                if (sc == null) {
                    debug("chatRegion", null, "ID, UserID, msg", id_r, client.getUserID(), msg);
                    return;
                }
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                try (LittleEndianDataOutputStream os = new LittleEndianDataOutputStream(baos)) {
                    os.writeShort(GI_SERVER_CMD.RES_CHAT_REGION.getCode());
                    sc.writeData(os);
                }
                byte[] data = baos.toByteArray();
                debug("chatRegion", null, "ID, UserID, msg, bytes", id_r, client.getUserID(), msg, data.length);

                for (String user : lstUser) {
                    WSServiceServer.i().sendDataWithID(user, data);
                }
            }
        } catch (Exception ex) {
            debug("rmMemberAlliance", ex, client.getUserID(), msg, id_r);
        }
    }

    // </editor-fold>
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="WorldMap Methods">    
    public void getZoneDetail(WSServiceClient client, StringBuilder sb, short zoneID) {

        byte[] data = WSGameDefine.EMPTY_BYTES;
        Zone z = StaticDataMng.i().worldMap.getZone(zoneID);
        if (z != null && z.layerGameObject != null) {
            GoldIncUser user = GoldIncUserMng.i().getUser(client.getUserID());
            user.curZoneID = zoneID;
            PlayerProfile p = user.getPlayerProfile();
            GameObjectMap[] temp = z.getGameObjects();
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            ArrayList<PlayerProfile> playerProfiles = new ArrayList<>();
            try (LittleEndianDataOutputStream os = new LittleEndianDataOutputStream(baos)) {
                os.writeShort(GI_SERVER_CMD.RES_DATA_ZONE.getCode());
                os.writeInt(temp.length);
                for (GameObjectMap o : temp) {
                    os.writeByte(o.mType);
                    if (o instanceof Mine || o instanceof OilMine) {
                        if (p.getPrivate().isAvailableObject(zoneID, o.id)) {
                            o.writeData(os);
                        } else {
                            ((Mine) o).writeDataEmptyMine(os);
                        }
                    } else {
                        o.writeData(os);
                    }
                    if (o.userID != null && !o.userID.isEmpty()) {
                        playerProfiles.add(GoldIncUserMng.i().getUser(o.userID).getPlayerProfile());
                    }
                }
                os.writeInt(playerProfiles.size());
                for (PlayerProfile i : playerProfiles) {
                    i.getProfileBin(os, true);
                }
                data = baos.toByteArray();
                debug("getZoneDetail", null, sb, "userid, zoneid, bytes", client.getUserID(), zoneID, data.length);
            } catch (Exception ex) {
                debug("getZoneDetail", ex, sb, "userid, zoneid, exception", client.getUserID(), zoneID);
            }
        }
        if (data.length > 0) {
            client.send(data);
        }
    }

    public void getMineInfo(WSServiceClient client, StringBuilder sb, short zoneID, long mineID) {
        byte[] data = WSGameDefine.EMPTY_BYTES;
        Zone z = StaticDataMng.i().worldMap.getZone(zoneID);
        if (z != null && z.layerGameObject != null) {
            GameObjectMap item = z.getObjectMap(mineID);
            if (item != null) {
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                try (LittleEndianDataOutputStream os = new LittleEndianDataOutputStream(baos)) {
                    os.writeShort(GI_SERVER_CMD.RES_MINE_INFO.getCode());
                    os.writeByte(item.mType);
                    PlayerProfile p = GoldIncUserMng.i().getUser(client.getUserID()).getPlayerProfile();
                    if (p.getPrivate().isAvailableObject(zoneID, mineID)) {
                        item.writeData(os);
                    } else {
                        ((Mine) item).writeDataEmptyMine(os);
                    }
                    p.getProfileBin(os, true);
                    data = baos.toByteArray();
                    debug("getMineInfo", null, sb, "userid, zoneid, mineid, bytes", client.getUserID(), zoneID, mineID, data.length);
                } catch (Exception ex) {
                    debug("getMineInfo", ex, sb, "userid, zoneid, mineid, exception", client.getUserID(), zoneID, mineID);
                }
            }
        }
        if (data.length > 0) {
            client.send(data);
        }
    }

    private void updateProfile(LittleEndianDataOutputStream os, GoldIncUser user) throws IOException {
        os.writeShort(GI_SERVER_CMD.RES_UPDATE_PROFILE.getCode());
        user.getPlayerProfile().getProfileBin(os, false);
    }

    public void payTestMine(WSServiceClient client, StringBuilder sb, short zoneID, long mineID) {
        ArrayList<byte[]> data = new ArrayList();
        Zone z = StaticDataMng.i().worldMap.getZone(zoneID);
        if (z != null && z.layerGameObject != null) {
            Mine item = (Mine) z.getObjectMap(mineID);
            if (item != null) {
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                try (LittleEndianDataOutputStream os = new LittleEndianDataOutputStream(baos)) {
                    GoldIncUser user = GoldIncUserMng.i().getUser(client.getUserID());
                    // abstract value cash with price test
                    int rs = user.payTestMine(item);
                    os.writeShort(GI_SERVER_CMD.RES_TEST_MINE.getCode());
                    os.writeByte(rs);
                    if (rs == 0) {
                        byte[] temp;
                        Exploration expl = (Exploration) user.getPlayerProfile().getPrivate().exploratingToMine(item);
                        if (expl != null) {
                            // add thread to check
                            TransportThread.i().addExploration(expl);
                            debug("payTestMine - add Thread countdown time", null, sb, "userid, time", client.getUserID(), String.valueOf((expl.endTime - expl.startTime)));

                            expl.writeData(os);
                            temp = baos.toByteArray();
                            data.add(temp);
                            debug("payTestMine - result", null, sb, "userid, result, time", client.getUserID(), rs, (expl.endTime-expl.startTime));
//                            debug("payTestMine - result pathfinding", null, sb, "userid, result", client.getUserID(), Func.json(expl.path));

                            baos.reset();
                            updateProfile(os, user);
                            temp = baos.toByteArray();
                            data.add(temp);
                            debug("payTestMine - updateProfile", null, sb, "userid, bytes", client.getUserID(), temp.length);
                        } else {

                        }
                    } else {
                        byte[] temp = baos.toByteArray();
                        data.add(temp);
                        debug("payTestMine", null, sb, "userid, zoneid, mineid, result, bytes", client.getUserID(), zoneID, mineID, rs, temp.length);
                    }
                } catch (Exception ex) {
                    debug("payTestMine", ex, sb, "userid, zoneid, mineid, exception", client.getUserID(), zoneID, mineID);
                }
            }
        }
        if (!data.isEmpty()) {
            for (byte[] bytes : data) {
                client.send(bytes);
            }
        }
    }

    public void payMine(WSServiceClient client, StringBuilder sb, short zoneID, long mineID) {
        ArrayList<byte[]> data = new ArrayList();
        Zone z = StaticDataMng.i().worldMap.getZone(zoneID);
        if (z != null && z.hasGameObjects()) {
            Mine item = (Mine) z.getObjectMap(mineID);
            if (item != null) {
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                try (LittleEndianDataOutputStream os = new LittleEndianDataOutputStream(baos)) {
                    GoldIncUser user = GoldIncUserMng.i().getUser(client.getUserID());
                    // abstract value cash with price test
                    byte rs = 1;
                    synchronized (LOCK) {
                        if (!z.checkAvailableMine(mineID)) {
                            rs = (byte) user.payMine(item);
                        }
                    }
                    if (rs == 0) {
                        byte[] temp = null;
                        byte[] mineData = null;
                        ////////////////////////////////////////////////////////
                        os.writeByte(item.mType);
                        item.writeData(os);
                        mineData = baos.toByteArray();
                        ////////////////////////////////////////////////////////
                        baos.reset();
                        os.writeShort(GI_SERVER_CMD.RES_PEG_MINE.getCode());
                        os.writeByte(rs);
                        os.write(mineData);
                        user.getPlayerProfile().getProfileBin(os, true);
                        temp = baos.toByteArray();
                        data.add(temp);
                        debug("payMine_" + GI_SERVER_CMD.RES_PEG_MINE.toString(), null,
                                sb, "userid, zoneid, mineid, result, bytes", client.getUserID(), zoneID, mineID, rs, temp.length);
                        ////////////////////////////////
                        baos.reset();
                        updateProfile(os, user);
                        temp = baos.toByteArray();
                        data.add(temp);
                        debug("updateProfile", null, sb, "userid, bytes", client.getUserID(), temp.length);
                        ////////////////////////////////                        
//                        resUpdateSpot(user.curZoneID, client.getUserID(), temp);
                        ZoneManager.i().updateSpot(item);
                    } else {
                        os.writeShort(GI_SERVER_CMD.RES_PEG_MINE.getCode());
                        os.writeByte(rs);
                        byte[] temp = baos.toByteArray();
                        data.add(temp);
                        debug("payMine", null, sb, "userid, zoneid, mineid, result,  bytes", client.getUserID(), zoneID, mineID, rs, temp.length);
                    }
                } catch (Exception ex) {
                    debug("payMine", ex, sb, "userid, zoneid, mineid, exception", client.getUserID(), zoneID, mineID);
                }
            }
        }
        if (!data.isEmpty()) {
            for (byte[] bytes : data) {
                client.send(bytes);
            }
        }
    }

    public void getTradeCashInfo(WSServiceClient client, StringBuilder sb) {

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        try (LittleEndianDataOutputStream os = new LittleEndianDataOutputStream(baos)) {
            os.writeShort(GI_SERVER_CMD.RES_TRADE_CASH_INFO.getCode());
            os.write(StaticDataMng.i().cashConvert.dataStream);
            byte[] data = baos.toByteArray();
            client.send(data);
            debug("getTradeCashInfo", null, sb, "userID, data length", client.getUserID(), data.length);
        } catch (Exception ex) {
            debug("getTradeCashInfo", ex, sb, "UserID, exception", client.getUserID());
        }
    }

    public void buyResource(WSServiceClient client, StringBuilder sb, TreeMap<Byte, Long> tree) {
        GoldIncUser user = GoldIncUserMng.i().getUser(client.getUserID());
        if (user != null) {
            GameObjectMap tradeCenter = StaticDataMng.i().worldMap.getTradeCenter(user.getPlayerProfile().privateInfo.homeBase.id_z);
            TradeTransaction transaction = user.getPlayerProfile().getPrivate().buyResource(tree);
            GameObjectMap truck = null;
            if (transaction.canTransport) {
                truck = user.getPlayerProfile().getPrivate().transportHB2TC(tradeCenter, transaction, tree);
                TransportThread.i().addTruck((Transportation) truck);
            }

            ArrayList<byte[]> lstData = new ArrayList<>();
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            try (LittleEndianDataOutputStream os = new LittleEndianDataOutputStream(baos)) {
                byte[] data = null;
                os.writeShort(GI_SERVER_CMD.RES_BUY_RESOURCE.getCode());
                if (transaction.canTransport && truck != null) {
                    os.writeByte(WSGameDefine.ADD_RES_SUCCESS);
                    ((Transportation) truck).writeData(os);

                    data = baos.toByteArray();
                    lstData.add(data);
                } else {
                    os.writeByte(WSGameDefine.ADD_RES_FAIL);
                    data = baos.toByteArray();
                    lstData.add(data);
                }
                baos.reset();
                updateProfile(os, user);
                data = baos.toByteArray();
                baos.reset();
                lstData.add(data);

                lstData.forEach((d) -> {
                    client.send(d);
                });

                debug("buyResource", null, sb, "UserID, resource_type, amount, result", client.getUserID(), Func.json(tree), transaction.canTransport);

                TradeCenterLogMng.i().writeLogBuy(user.userID, user.curZoneID, transaction);

            } catch (Exception ex) {
                debug("buyResource", ex, sb, "UserID, resource_type, amount, exception", client.getUserID(), Func.json(tree));
            }
        }
    }

    public void sellResource(WSServiceClient client, StringBuilder sb, TreeMap<Byte, Long> tree) {
        GoldIncUser user = GoldIncUserMng.i().getUser(client.getUserID());
        if (user != null) {
            GameObjectMap tradeCenter = StaticDataMng.i().worldMap.getTradeCenter(user.getPlayerProfile().privateInfo.homeBase.id_z);
            TradeTransaction transaction = user.getPlayerProfile().getPrivate().sellResource(tree);
            Transportation truck = null;
            if (transaction.canTransport) {
                truck = (Transportation) user.getPlayerProfile().getPrivate().transportHB2TC(tradeCenter, transaction, tree);
                TransportThread.i().addTruck((Transportation) truck);
            }
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            ArrayList<byte[]> lstData = new ArrayList<>();
            try (LittleEndianDataOutputStream os = new LittleEndianDataOutputStream(baos)) {
                os.writeShort(GI_SERVER_CMD.RES_SELL_RESOURCE.getCode());
                byte[] data = null;
                if (transaction.canTransport && truck != null) {
                    os.writeByte(WSGameDefine.ADD_RES_SUCCESS);
                    truck.writeData(os);
                    data = baos.toByteArray();
                    lstData.add(data);
                } else {
                    os.writeByte(WSGameDefine.ADD_RES_FAIL);
                    data = baos.toByteArray();
                    lstData.add(data);
                }
                baos.reset();
                updateProfile(os, user);
                data = baos.toByteArray();
                baos.reset();
                lstData.add(data);

                lstData.forEach((d) -> {
                    client.send(d);
                });

                debug("sellResource", null, sb, "UserID, resource_type, amount, result", client.getUserID(), Func.json(tree), transaction.canTransport);

                TradeCenterLogMng.i().writeLogSell(user.userID, user.curZoneID, transaction);

            } catch (Exception ex) {
                debug("sellResource", ex, sb, "UserID, resource_type, amount, exception", client.getUserID(), Func.json(tree), transaction.canTransport);
            }
        }
    }

    public void transportToClaim(WSServiceClient client, StringBuilder sb, short zoneID, long mineID, ArrayList<Long> lstEquip, ArrayList<Long> lstWorker, byte type_speed /*land or air*/) {
        GoldIncUser user = GoldIncUserMng.i().getUser(client.getUserID());
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        if (user != null) {
            try (LittleEndianDataOutputStream os = new LittleEndianDataOutputStream(baos)) {
                os.writeShort((short) GI_SERVER_CMD.RES_TRANSPORT_TO_CLAIM.getCode());
                Zone z = StaticDataMng.i().worldMap.getZone(zoneID);

                if (z != null && z.hasGameObjects()) {
                    Mine item = (Mine) z.getObjectMap(mineID);
                    if (item != null) {
                        Transportation truck = user.getPlayerProfile().getPrivate().equipmentsToClaim(item, lstEquip, lstWorker, type_speed);
                        if (truck != null) {
                            //success
                            TransportThread.i().addTruck(truck);
                            os.writeByte(0);
                            truck.writeData(os);
                            byte[] data = baos.toByteArray();
                            client.send(data);
                            baos.reset();
                        } else {
                            // fail
                            os.writeByte(1);
                            client.send(baos.toByteArray());
                            baos.reset();
                        }
                    } else {
                        //fail
                        os.writeByte(1);
                        client.send(baos.toByteArray());
                        baos.reset();
                    }
                } else {
                    //fail
                    os.writeByte(1);
                    client.send(baos.toByteArray());
                    baos.reset();
                }

                updateProfile(os, user);
                client.send(baos.toByteArray());
                baos.reset();

            } catch (Exception ex) {
                debug("transportToClaim", ex, sb, "UserID, zoneID, mineID, lsEquip, lsWorker, land or air, exception",
                        client.getUserID(), zoneID, mineID, Func.json(lstEquip), Func.json(lstWorker), type_speed, Func.toString(ex));
                byte[] a = new byte[3];
                ByteBuffer buff = ByteBuffer.wrap(a);
                buff.order(ByteOrder.LITTLE_ENDIAN);
                buff.rewind();
                buff.putShort((short) GI_SERVER_CMD.RES_TRANSPORT_TO_CLAIM.getCode());
                buff.put((byte) 2);
                client.send(a);
            }
        }
    }

    public void resUpdateSpot(GameObjectMap obj, String owner) {
        ZoneManager.i().updateSpot(obj);
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="HomeBase Item">   
    public void playerBuilding(WSServiceClient client, StringBuilder sb, String otherUser) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        try (LittleEndianDataOutputStream os = new LittleEndianDataOutputStream(baos)) {
            GoldIncUser user = GoldIncUserMng.i().getUser(otherUser);
            ArrayList<Building> listBuildingHB = (user != null) ? (user.getPlayerProfile().getPrivate().getListBuilding()) : (new ArrayList<>());
            int size = listBuildingHB.size();
            os.writeShort(GI_SERVER_CMD.RES_PLAYER_BUILDING.getCode());
            os.writeInt(size);
            for (Building it : listBuildingHB) {
                os.writeByte(it.getType());
                it.writeDataOther(os);
            }
            if (user != null) {
                user.getPlayerProfile().getProfileBin(os, true);
            }
            byte[] data = baos.toByteArray();
            client.send(data);
            debug("hbGetAllBaseItem", null, sb, "UserID, OtherUserID, size, bytes", client.getUserID(), otherUser, size, data.length);
        } catch (IOException ex) {
            debug("hbGetAllBaseItem", ex, sb, "UserID, OtherUserID, exception", client.getUserID(), otherUser);
            byte[] a = new byte[6];
            ByteBuffer buff = ByteBuffer.wrap(a);
            buff.order(ByteOrder.LITTLE_ENDIAN);
            buff.rewind();
            buff.putShort((short) GI_SERVER_CMD.RES_PLAYER_BUILDING.getCode());
            buff.putInt(0);
            client.send(a);
        }
    }

    public void hbGetClaimInfo(WSServiceClient client, StringBuilder sb) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        try (LittleEndianDataOutputStream os = new LittleEndianDataOutputStream(baos)) {
            os.writeShort(GI_SERVER_CMD.RES_CLAIMS_INFO.getCode());
        } catch (IOException ex) {
            debug("hbGetConfig", ex, sb, "UserID, exception", client.getUserID());
            byte[] a = new byte[6];
            ByteBuffer buff = ByteBuffer.wrap(a);
            buff.order(ByteOrder.LITTLE_ENDIAN);
            buff.rewind();
            buff.putShort((short) GI_SERVER_CMD.RES_PLAYER_BUILDING.getCode());
            buff.putInt(0);
            client.send(a);
        }
    }

    public void upgradeBuilding(WSServiceClient client, StringBuilder sb, byte type) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        try (LittleEndianDataOutputStream os = new LittleEndianDataOutputStream(baos)) {
            GoldIncUser user = GoldIncUserMng.i().getUser(client.getUserID());
            os.writeShort(GI_SERVER_CMD.RES_UPGRADE_BUILDING.getCode());
            int code = user.checkUpgreadeBuilding(type);
            if (code == 0) {
//                 upgrade follow type
//                 return 1 is fail
//                 return 0 is successful + starttime + endtime
//                 after endtime, user get RES_UPDATE_HOMEBASE + type homebaseitem + leveup
                StringBuilder sbBuilding = new StringBuilder();
                user.upgradeBuilding(type, os, sbBuilding);
                client.send(baos.toByteArray());
                debug("upgradeBuilding", null, sb, "UserID, building data", client.getUserID(), sbBuilding.toString());

                baos.reset();
                os.writeShort(GI_SERVER_CMD.RES_USER_RESOURCE.getCode());
                user.getPlayerProfile().getPrivate().getGameResources().writeData(os);
                client.send(baos.toByteArray());
                debug("upgradeBuilding", null, sb, "UserID, resource data", client.getUserID(), Func.json(user.getPlayerProfile().getPrivate().getGameResources()));
            } else {
//                 send return code to client
                os.writeByte(code);
                client.send(baos.toByteArray());
            }

        } catch (Exception ex) {
            debug("buildingUpgrade", ex, sb, "UserID, exception", client.getUserID());
            byte[] a = new byte[6];
            ByteBuffer buff = ByteBuffer.wrap(a);
            buff.order(ByteOrder.LITTLE_ENDIAN);
            buff.rewind();
            buff.putShort((short) GI_SERVER_CMD.RES_UPGRADE_BUILDING.getCode());
            buff.put((byte) 2);
            client.send(a);
        }
    }

    public void refineFuel(WSServiceClient client, StringBuilder sb, int numberFuel) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        try (LittleEndianDataOutputStream os = new LittleEndianDataOutputStream(baos)) {
            GoldIncUser user = GoldIncUserMng.i().getUser(client.getUserID());
            StringBuilder log = new StringBuilder();
            int rs = user.getPlayerProfile().getPrivate().refineFuel(os, log, numberFuel);
            debug("refineFuel", null, sb, "UserID, numberFuel, result", client.getUserID(), numberFuel, log.toString());
            client.send(baos.toByteArray());
            baos.reset();

            if(rs==0){
                ArrayList<Building> listBuildingHB = (user.getPlayerProfile().getPrivate().getListBuilding());
                for (Building it : listBuildingHB) {
                    if (ENUM_UNIT_TYPE.HOME_REFINERY.getCode() == it.getType()) {
                        os.writeShort(GI_SERVER_CMD.RES_UPDATE_BUILDING.getCode());
                        os.writeByte(it.getType());
                        it.writeData(os);
                        client.send(baos.toByteArray());
                        debug("refineFuel", null, sb, "UserID, building", client.getUserID(), Func.json(it));
                        baos.reset();
                        break;
                    }
                }

                GameResources gRes = user.getPlayerProfile().getPrivate().getGameResources();
                os.writeShort(GI_SERVER_CMD.RES_USER_RESOURCE.getCode());
                gRes.writeData(os);
                client.send(baos.toByteArray());
                baos.reset();
                debug("refineFuel", null, sb, "UserID, game resource", client.getUserID(), Func.json(gRes));
            }


        } catch (Exception ex) {
            debug("refineFuel", ex, sb, "UserID, exception", client.getUserID());
            byte[] a = new byte[6];
            ByteBuffer buff = ByteBuffer.wrap(a);
            buff.order(ByteOrder.LITTLE_ENDIAN);
            buff.rewind();
            buff.putShort((short) GI_SERVER_CMD.RES_REFINE_OIL.getCode());
            buff.put((byte) 2);
            client.send(a);
        }
    }

    public void transportFuelToClaim(WSServiceClient client, StringBuilder sb, short zoneID, long mineID, int numberFuel, byte type_speed) {
        GoldIncUser user = GoldIncUserMng.i().getUser(client.getUserID());

        if (user != null) {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            try (LittleEndianDataOutputStream os = new LittleEndianDataOutputStream(baos)) {

                os.writeShort((short) GI_SERVER_CMD.RES_TRANSPORT_FUEL.getCode());
                Zone z = StaticDataMng.i().worldMap.getZone(zoneID);
                int rs = 0;
                if (z != null && z.hasGameObjects()) {
                    Mine item = (Mine) z.getObjectMap(mineID);
                    if (item != null) {
                        Transportation truck = user.getPlayerProfile().getPrivate().fuelToClaim(item, numberFuel, type_speed);
                        if (truck != null) {
                            //success
                            TransportThread.i().addTruck(truck);
                            os.writeByte(0);
                            truck.writeData(os);
                            byte[] data = baos.toByteArray();
                            client.send(data);                            
                            baos.reset();
                        } else {
                            // fail
                            rs = 1;
                            os.writeByte(1);
                            client.send(baos.toByteArray());
                            baos.reset();
                        }
                    } else {
                        //fail
                        rs = 1;
                        os.writeByte(1);
                        client.send(baos.toByteArray());
                        baos.reset();
                    }
                } else {
                    //fail
                    rs = 1;
                    os.writeByte(1);
                    client.send(baos.toByteArray());
                    baos.reset();
                }
                debug("transportFuelToClaim", null, sb, "UserID, result", client.getUserID(), rs);
                
                GameResources gRes = user.getPlayerProfile().getPrivate().getGameResources();
                os.writeShort(GI_SERVER_CMD.RES_USER_RESOURCE.getCode());
                gRes.writeData(os);
                client.send(baos.toByteArray());
                baos.reset();
                debug("transportFuelToClaim", null, sb, "UserID, game resource", client.getUserID(), Func.json(gRes));
                
            } catch (Exception ex) {
                debug("transportFuelToClaim", ex, sb, "UserID, exception", client.getUserID());
                byte[] a = new byte[6];
                ByteBuffer buff = ByteBuffer.wrap(a);
                buff.order(ByteOrder.LITTLE_ENDIAN);
                buff.rewind();
                buff.putShort((short) GI_SERVER_CMD.RES_REFINE_OIL.getCode());
                buff.put((byte) 2);
                client.send(a);
            }
        }
    }

    // </editor-fold>
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="Micro service Methods">
    public void msResPing(WSMicroServiceClient client, long time) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        try (LittleEndianDataOutputStream os = new LittleEndianDataOutputStream(baos)) {
            os.writeShort(GI_SERVER_CMD.RES_PING.getCode());
            os.writeLong(time);
            client.send(baos.toByteArray());
        } catch (Exception ex) {
        }
    }

    public void msResConnect(WSMicroServiceClient client, long time) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        try (LittleEndianDataOutputStream os = new LittleEndianDataOutputStream(baos)) {
            os.writeShort(GI_SERVER_CMD.RES_PING.getCode());
            os.writeLong(time);
            client.send(baos.toByteArray());
        } catch (Exception ex) {
        }
    }

    public void msResGetStaticResource(WSMicroServiceClient client, long time) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        try (LittleEndianDataOutputStream os = new LittleEndianDataOutputStream(baos)) {
            os.writeShort(GI_SERVER_CMD.RES_PING.getCode());
            os.writeLong(time);
            client.send(baos.toByteArray());
        } catch (Exception ex) {
        }
    }

    // </editor-fold>
}
