/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ctmlab.game.manager;

import com.ctmlab.game.data.model.worker.Worker;
import com.ctmlab.game.data.model.*;
import com.ctmlab.game.data.model.building.Building;
import com.ctmlab.game.data.model.equipment.Equipment;
import com.ctmlab.game.hadoop.*;
import com.dp.db.*;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Enumeration;

/**
 *
 * @author hhtri
 */
public class GoldIncMng {

    public static final Object LOCK = new Object();
    private static GoldIncMng inst;

    public static GoldIncMng i() {
        synchronized (LOCK) {
            if (inst == null) {
                inst = new GoldIncMng();
            }
            return inst;
        }
    }

    private final String mLogName = this.getClass().getSimpleName().toLowerCase();
    private final int PORT_TO_BUILD = GlobalConfig.PORT;

    private GoldIncMng() {
    }

    private void debug(String data) {
        HadoopProvider.i().writeLog(mLogName, Func.now() + "\n" + data);
    }

    private void initTableDB() throws Exception {  

        GlobalConfig.initFolders();

        String sql_map_id_seq = "CREATE SEQUENCE public.map_id_seq\n" +
                "    INCREMENT 1\n" +
                "    START 1\n" +
                "    MINVALUE 1\n" +
                "    MAXVALUE 9223372036854775807\n" +
                "    CACHE 1;\n" +
                "\n" +
                "ALTER SEQUENCE public.map_id_seq\n" +
                "    OWNER TO admin;";

        try{
            DB.i().exec(sql_map_id_seq);
        } catch (Exception ex){}

        new Claim().validateTable(DB.i().con());
        new Mine().validateTable(DB.i().con());
        new OilMine().validateTable(DB.i().con());
        new TradeCenterMap().validateTable(DB.i().con());
        new HomeBase().validateTable(DB.i().con());
        new Exploration().validateTable(DB.i().con());
        new Transportation().validateTable(DB.i().con());        

        new Building().validateTable(DB.i().con());
        new Equipment().validateTable(DB.i().con());
        new Worker().validateTable(DB.i().con());

        new GoldIncUser().validateTable(DB.i().con());
        new GIUserAdmin().validateTable(DB.i().con());
        new SocialAlliance().validateTable(DB.i().con());
        new SocialChat().validateTable(DB.i().con());
        new GameMessage().validateTable(DB.i().con());
        new RegistryAccount().validateTable(DB.i().con());
    }

    public void init(){
        try {
            if(GlobalConfig.isOnlineServer()){
                DB.i(new DBPostgreSQL("localhost", "goldinc", "admin", "123postgres456!"));
            } else if(PORT_TO_BUILD==8084){
                DB.i(new DBPostgreSQL("localhost", "goldinctemp", "admin", "123postgres456!"));
            } else if (PORT_TO_BUILD==8080){
                DB.i(new DBPostgreSQL("localhost", "goldinc", "admin", "123postgres456!"));
            } else{
                DB.i(new DBPostgreSQL("localhost", "goldinc", "admin", "123postgres456!"));
            }

            initTableDB();    
            GoldIncUserMng.i().loadAll();   
            StaticDataMng.i().init();
            debug("Finishing init function!!!");
        } catch (Exception ex) {
            debug(Func.toString(ex));
        }
    }

    public void initDefault(boolean isDropTable) {
        try {
            long start = System.currentTimeMillis();
            if(GlobalConfig.isOnlineServer()){
                DB.i(new DBPostgreSQL("localhost", "goldinc", "admin", "123postgres456!"));
            } else if(PORT_TO_BUILD==8084){
                DB.i(new DBPostgreSQL("localhost", "goldinctemp", "admin", "123postgres456!"));
            } else if (PORT_TO_BUILD==8080){
                DB.i(new DBPostgreSQL("localhost", "goldinc", "admin", "123postgres456!"));
            } else{
                DB.i(new DBPostgreSQL("localhost", "goldinc", "admin", "123postgres456!"));
            }
            initTableDB();
            String sql = "" 
                    + "SELECT tablename "
                    + "FROM pg_catalog.pg_tables\n"
                    + "WHERE schemaname != 'pg_catalog' AND schemaname != 'information_schema';";
            PreparedStatement st = DB.i().con().prepareStatement(sql);
            try (ResultSet rs = st.executeQuery()) {
                while (rs.next()) {
                    String table = rs.getString(1);                    
                    if(isDropTable){
                        System.out.println(String.format("Drop table [%s]", table));
                        DB.i().exec(String.format("Drop table %s;", table));                        
                    } else{
                        System.out.println(String.format("Deleting table [%s]", table));
                        DB.i().exec(String.format("Delete from %s;", table));
                    }
                }
                if(isDropTable){
                    initTableDB();
                }
            }
            debug(String.format("init DB: %d (ms)", System.currentTimeMillis()-start));            
            start = System.currentTimeMillis();
            StaticDataMng.i().initDefault();
            debug(String.format("initDefault: %d (ms)", System.currentTimeMillis()-start));
            start = System.currentTimeMillis();
            StaticDataMng.i().genInitMap(16);
            debug(String.format("genInitMap(15): %d (ms)", System.currentTimeMillis()-start));
            start = System.currentTimeMillis();
            GoldIncUserMng.i().initDefault();            
            debug(String.format("gen User: %d (ms)", System.currentTimeMillis()-start));
        } catch (Exception ex) {
            debug(Func.toString(ex));
        }
    }

    public void close() {
        DB.i().close();
        ClassLoader cl = Thread.currentThread().getContextClassLoader();
        // Loop through all drivers
        Enumeration<Driver> drivers = DriverManager.getDrivers();
        while (drivers.hasMoreElements()) {
            Driver driver = drivers.nextElement();
            if (driver.getClass().getClassLoader() == cl) {
                // This driver was registered by the webapp's ClassLoader, so deregister it:
                try {
//                    log.info("Deregistering JDBC driver {}", driver);
                    debug(String.format("Deregistering JDBC driver %s", driver.getClass().getSimpleName()));
                    DriverManager.deregisterDriver(driver);
                } catch (SQLException ex) {
//                    log.error("Error deregistering JDBC driver {}", driver, ex);
                    debug(Func.toString(ex));
                }
            } else {
                // driver was not registered by the webapp's ClassLoader and may be in use elsewhere
//                log.trace("Not deregistering JDBC driver {} as it does not belong to this webapp's ClassLoader", driver);
                debug("Not deregistering JDBC driver {} as it does not belong to this webapp's ClassLoader");
            }
        }
    }
    
    public long calDataOnRAM(){
        long rs = 0;
        
        return rs;
    }

}
