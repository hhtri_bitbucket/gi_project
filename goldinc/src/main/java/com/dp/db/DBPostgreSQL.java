/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dp.db;

import com.ctmlab.game.hadoop.LogManager;
import static com.dp.db.DB.getResultSetValue;
import java.lang.reflect.Type;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author hhtri
 */
public class DBPostgreSQL extends DB {
    int port;
    public DBPostgreSQL(String _host,String _database,String _user,String _password) {
        try {
            Class.forName("org.postgresql.Driver");
        }catch(ClassNotFoundException ex) {
            DB.i().onLog(this.getClass().getSimpleName().toLowerCase(), "Where is your POSTGRESQL JDBC Driver?");
        }
        port = 5432;
        host = _host;
        database = _database;
        user = _user;
        password = _password;
    }
    public DBPostgreSQL(String _host, int _port, String _database,String _user,String _password) {
        try {
            Class.forName("org.postgresql.Driver");
        }catch(ClassNotFoundException ex) {
            DB.i().onLog(this.getClass().getSimpleName().toLowerCase(), "Where is your POSTGRESQL JDBC Driver?");
        }
        port = _port;
        host = _host;
        database = _database;
        user = _user;
        password = _password;
    }       
    
    //hhtri
    /////////////////////////////////////////////////
    private boolean isDebugLog = false;
    @Override
    public void onLog(String name, Object data) {
        if(isDebugLog){
            LogManager.i().writeLog(name, String.valueOf(data));
        }
    }
        
    @Override
    public int delete(DBData data) throws SQLException {
        try {
            DBDataSQLInfo uSQL = data.getSQLInfo();
            try (PreparedStatement st = con().prepareStatement("DELETE FROM " + tableSQL(data.table()) + " WHERE " + uSQL.whereKey)) {
                data.setKeys(st, 1, uSQL);
                return st.executeUpdate();
            }
        } catch (IllegalArgumentException ex) {
            Logger.getLogger(DBData.class.getName()).log(Level.SEVERE, null, ex);
        }
        return -1;
    }
    /////////////////////////////////////////////////
    
    @Override
    public String url() {
        
        String urlTemp = "jdbc:postgresql://#host#:#port#/#db#"
                + "?user=#u#&password=#p#&charSet=UNICODE";

        String url = urlTemp.replace("#host#", host)
                .replace("#port#", String.valueOf(port))
                .replace("#db#", database)
                .replace("#u#", user)
                .replace("#p#", password);
        return url;
    }

    @Override
    public <T extends DBData> ArrayList<T> select(Class<T> classOfT, String where, HashSet<String> cols, Map ref, DBDataListener<T> listener, Object[] params) throws SQLException {
        ArrayList<T> list = null;
        if (listener == null) {
            list = new ArrayList();
        }

        String sql;
        if (cols == null) {
            sql = ("SELECT * FROM " + tableSQL(classOfT.getSimpleName().toLowerCase()) + " " + where);
        } else {
            StringBuilder sb = new StringBuilder();
            cols.stream().forEach((s) -> {
                if (sb.length() > 0) {
                    sb.append(",");
                }
                sb.append(quote(s));
            });
            sql = ("SELECT " + sb + " FROM " + tableSQL(classOfT.getSimpleName()) + " " + where);
        }
        try (PreparedStatement st = con().prepareStatement(sql)) {
            if (params != null) {
                int idx = 1;
                for (Object param : params) {
                    setValue(st, idx++, param);
                }
            }
            try (ResultSet rs = st.executeQuery()) {
                ResultSetMetaData metaData = rs.getMetaData();

                HashMap<String, Integer> columns = new HashMap();
                HashMap<Integer, java.lang.reflect.Field> fields = new HashMap();
                int count = metaData.getColumnCount(); //number of column
                for (int i = 1; i <= count; i++) {
                    columns.put(metaData.getColumnLabel(i), i);
                }

                java.lang.reflect.Field mapField = null;
                DataFieldType mapType = null;
                Class clazz = classOfT;
                while (clazz != DBDataPostgres.class) {
                    for (java.lang.reflect.Field f : clazz.getDeclaredFields()) {
                        f.setAccessible(true);
                        if (java.lang.reflect.Modifier.isStatic(f.getModifiers())) {
                            continue;
                        }
                        DataFieldType type = f.getAnnotation(DataFieldType.class);
                        if (type != null) {
                            if (type.isMap()) {
                                mapField = f;
                                mapType = type;
                                continue;
                            }
                        }
                        Integer i = columns.get(f.getName().toLowerCase());
                        if (i != null) {
                            fields.put(i, f);
                            columns.remove(f.getName().toLowerCase());
                        }
                    }
                    clazz = clazz.getSuperclass();
                }

                while (rs.next()) {
                    try {
                        T t = classOfT.newInstance();
                        for (Integer i : fields.keySet()) {
                            java.lang.reflect.Field f = fields.get(i);
                            DataFieldType type = f.getAnnotation(DataFieldType.class);
                            if (type != null && !type.childField().isEmpty() && ref != null) {
                                try {
                                    f.set(t, ref.get(getResultSetValue(rs, i, f.getType().getField(type.childField()).getType())));
                                } catch (NoSuchFieldException ex) {
                                    Func.printlnLog(this.getClass().getSimpleName() + "_err.log", ex);
                                }
                            } else {
                                if (f.getType() == ArrayList.class) {
                                    Type the_type = new DB.ListParameterizedType(type.mapValueClass());
                                    f.set(t, getResultSetValue(rs, i, the_type));
                                } else {
                                    f.set(t, getResultSetValue(rs, i, f.getType()));
                                }
                            }
                        }
                        if (mapField != null) {
                            if (mapField.getType() == TreeMap.class) {
                                TreeMap map = (TreeMap) mapField.get(t);

                                for (String key : columns.keySet()) {
                                    if (mapType.mapKeyClass().isEnum()) {
                                        map.put(Enum.valueOf(mapType.mapKeyClass(), key), getResultSetValue(rs, columns.get(key), mapType.mapValueClass()));
                                    } else {
                                        map.put(key, getResultSetValue(rs, columns.get(key), mapType.mapValueClass()));
                                    }
                                }
                            }
                        }
                        if (listener == null) {
                            list.add(t);
                        } else {
                            listener.onItem(t);
                        }
                    } catch (InstantiationException ex) {
                        Logger.getLogger(DBData.class.getName()).log(Level.SEVERE, null, ex);
                    } catch (IllegalAccessException ex) {
                        Logger.getLogger(DBData.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
        }

        return list;
    }
}
