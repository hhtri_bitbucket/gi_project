/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dp.db;

import com.ctmlab.util.FileUploadRequest;
import com.dp.db.data.js.JSData;
import com.dp.db.data.js.JSDataDeserializer;
import com.google.gson.*;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.Reader;
import java.io.StringReader;
import java.io.StringWriter;
import java.net.InetAddress;
import java.net.URLEncoder;
import java.net.UnknownHostException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Properties;
import java.util.Random;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.apache.commons.io.IOUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

/**
 *
 * @author ccthien
 */
public class Func {

    public static final SimpleDateFormat SDF_DATE = new SimpleDateFormat("yyyy-MM-dd");
    public static final SimpleDateFormat SDF_TIME = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    public static Timestamp parseDate(String str) {
        if (str == null || str.isEmpty()) {
            return new Timestamp(0);
        }
        Date date = null;
        if (date == null) {
            try {
                date = SDF_TIME.parse(str);
            } catch (Exception ex1) {
            }
        }
        if (date == null) {
            try {
                date = SDF_DATE.parse(str);
            } catch (Exception ex2) {
            }
        }
        long time = date == null ? 0 : date.getTime();
        return new Timestamp(time);
    }

    public static final String MINE_FILE = "application/octet-stream";
    public static final String MINE_JSON = "application/json";
    public static final String MINE_XML = "application/xml";
    public static final String MINE_TEXT = "text/plain";
    public static final String MINE_HTML = "text/html";

    public static final String UTF8_ENCODE = "UTF-8";
    public static final Charset UTF8 = Charset.forName(UTF8_ENCODE);

    public static final long ONE_YEAR = 31536000000L;
    public static final long ONE_DAY = 86400000L;
    public static final long ONE_WEEK = ONE_DAY * 7;

    public static final Gson gson;
    private static final Gson _pson;
    private static final Gson _lson;
    private static final Gson _plson;
    private static final Gson _kson;

    static {
        gson = new GsonBuilder()
                .registerTypeAdapter(JSData.class, new JSDataDeserializer())
                .serializeNulls()
                .create();
        _kson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
        _pson = new GsonBuilder().serializeNulls().setPrettyPrinting().create();

        _lson = new GsonBuilder()
                .serializeNulls()
                .setExcluseJsonScope("Unity")
                .registerTypeAdapter(Long.class, (JsonSerializer<Long>) (Long obj, java.lang.reflect.Type type, JsonSerializationContext jsc) -> new JsonPrimitive(String.valueOf(obj)))
                .create();
        _plson = new GsonBuilder()
                .serializeNulls()
                .setPrettyPrinting()
                .setExcluseJsonScope("Unity")
                .registerTypeAdapter(Long.class, (JsonSerializer<Long>) (Long obj, java.lang.reflect.Type type, JsonSerializationContext jsc) -> new JsonPrimitive(String.valueOf(obj)))
                .create();
    }

    public static String json(Object o) {
        return gson.toJson(o);
    }

    public static String lson(Object o) {
        return _lson.toJson(o);
    }

    public static String pson(Object o) {
        return _pson.toJson(o);
    }

    public static String plson(Object o) {
        return _plson.toJson(o);
    }

    public static String kson(Object o) {
        return _kson.toJson(o);
    }

    public static boolean isNull(JsonElement e) {
        return e == null || e.isJsonNull();
    }

    //////////////////////////////////////////////////////
    public static SimpleDateFormat sdfSimple = new SimpleDateFormat("yyyyMMdd_HHmmss.SSS");

    public static String now() {
        return sdfSimple.format(new java.util.Date());
    }
    public static final long START_WEEK_MARGIN = 3 * ONE_DAY;

    public static long startWeek(long time) {
        return (((time - START_WEEK_MARGIN) / ONE_WEEK) * ONE_WEEK) + START_WEEK_MARGIN;
    }

    public static void main(String[] argv) {
        System.out.println(Arrays.toString(randomInts(11, 10)));
    }

    public static int[] randomInts(int n) {
        return randomInts(n, n);
    }

    public static int[] randomInts(int max, int n) {
        if (max < n) {
            throw new RuntimeException("Random 'max' should not less than 'n'");
        }
        Random r = new Random();
        ArrayList<Integer> arr = new ArrayList(max);
        for (int i = 0; i < max; i++) {
            arr.add(i);
        }

        int[] res = new int[n];
        for (int i = 0; i < n; i++) {
            int idx = r.nextInt(arr.size());
            res[i] = arr.get(idx);
            arr.remove(idx);
        }
        return res;
    }

    public static String getExtension(String fileName) {
        char ch;
        int len;
        if (fileName == null
                || (len = fileName.length()) == 0
                || (ch = fileName.charAt(len - 1)) == '/' || ch == '\\'
                || //in the case of a directory
                ch == '.') //in the case of . or ..
        {
            return "";
        }
        int dotInd = fileName.lastIndexOf('.'),
                sepInd = Math.max(fileName.lastIndexOf('/'), fileName.lastIndexOf('\\'));
        if (dotInd <= sepInd) {
            return "";
        } else {
            return fileName.substring(dotInd + 1).toLowerCase();
        }
    }
    public static final String UTF8_BOM = "\uFEFF";

    public static String removeUTF8BOM(String s) {
        if (s.startsWith(UTF8_BOM)) {
            return s.substring(1);
        }
        return s;
    }

    public static String getFileName(String path) {
        if (path == null) {
            return null;
        }
        int idx = Math.max(path.lastIndexOf('/'), path.lastIndexOf('\\'));
        return (idx == -1) ? path : path.substring(idx + 1);
    }

    public static String urlEncode(String s) {
        try {
            return URLEncoder.encode(s, "UTF8");
        } catch (Exception ex) {
        }
        return "";
    }

    public static String urlEncodeObj(Object o) {
        String s = json(o);
        return urlEncode(s);
    }

    public static String ISOtoUTF8(String s) {
        if (s == null) {
            return null;
        }
        return new String(s.getBytes(java.nio.charset.Charset.forName("ISO-8859-1")), java.nio.charset.Charset.forName("UTF-8"));
    }

    public static String file(String id) {
        return id.replaceAll("[^a-zA-Z0-9]+", "_");
    }

    public static String md5(String s) {
        if (s == null) {
            return null;
        }
        try {
            return java.util.Base64.getEncoder().encodeToString(MessageDigest.getInstance("MD5").digest(s.getBytes()));
        } catch (NoSuchAlgorithmException ex) {
        }
        return s;
    }

    public static String stacktrace(Exception e) {
        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        e.printStackTrace(pw);
        return sw.toString();
    }

    public static void debug(Object o) {
        System.out.println(json(o));
    }

    //////////////////////////////////////////////////////
    public static void sendEmail(String from, String pass, String to, String subject, String msg) {
        sendEmail(from, pass, to, subject, msg, false);
    }

    public static void sendEmailHtml(String from, String pass, String to, String subject, String msg) {
        sendEmail(from, pass, to, subject, msg, true);
    }

    private static void sendEmail(String from, String pass, String to, String subject, String msg, boolean isHtml) {
        Properties props = new Properties();
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.port", "587");

        Session session = Session.getInstance(props,
                new javax.mail.Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(from, pass);
            }
        });

        try {
            MimeMessage message = new MimeMessage(session);
            message.setFrom(new InternetAddress(from));
            message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(to));
            message.setSubject(subject);
            if (isHtml) {
                message.setContent(msg, "text/html; charset=utf-8");
            } else {
                message.setText(msg);
            }
            Transport.send(message);
        } catch (MessagingException e) {
            throw new RuntimeException(e);
        }
    }

    private static File tomcatRoot = null;
    private static File tomcatWebApps = null;
    private static final HashMap<String, File> cacheImageFolders = new HashMap<String, File>();
    private static Boolean __is_tomcat = null;

    public static boolean isTomcat() {
        if (__is_tomcat != null) {
            return __is_tomcat;
        }
        __is_tomcat = System.getProperty("catalina.home") != null;
        return __is_tomcat;
    }

    public static File getTomcatWebapps() {
        if (tomcatWebApps == null) {
            String home = System.getProperty("catalina.home");
            if (home == null) {
                return null;
            }
            tomcatWebApps = new File(home, "webapps");
        }
        return tomcatWebApps;
    }

    public static File getTomcatRoot() {
        if (tomcatRoot == null) {
            File apps = getTomcatWebapps();
            if (apps == null) {
                return null;
            }
            tomcatRoot = new File(apps, "ROOT");
        }
        return tomcatRoot;
    }

    public static File getTomcatImageFolder(String name) {
        File folder = cacheImageFolders.get(name);
        if (folder == null) {
            //? File root = getTomcatRoot();
            File root = getTomcatWebapps();
            if (root == null) {
                return null;
            }
            folder = new File(new File(root, "images"), name);
            if (!folder.exists()) {
                folder.mkdirs();
            }
            cacheImageFolders.put(name, folder);
        }
        return folder;
    }
    private static HashMap<String, Boolean> image_extentions = null;

    public static HashMap<String, Boolean> getImageExtentions() {
        if (image_extentions == null) {
            image_extentions = new HashMap<String, Boolean>();
            image_extentions.put("jpg", true);
            image_extentions.put("jpeg", true);
            image_extentions.put("gif", true);
            image_extentions.put("png", true);
            image_extentions.put("bmp", true);
            image_extentions.put("dat", true);
        }
        return image_extentions;
    }

    public static boolean isImageExtension(String ext) {
        return getImageExtentions().get(ext.toLowerCase()) != null;
    }

    public static String getName(HttpServletRequest request) {
        String uri = request.getRequestURI();
        uri = uri.substring(uri.lastIndexOf("/") + 1);
        uri = uri.substring(0, uri.lastIndexOf("."));
        return uri;
    }

    public static String getTomcatImageURL(HttpServletRequest request, String folder) {
        return "http://" + request.getServerName() + "/images/" + folder + "/";
    }

    public static PrintWriter openWriterUTF8(File file, boolean append) throws FileNotFoundException {
        return new PrintWriter(new OutputStreamWriter(new FileOutputStream(file, append), StandardCharsets.UTF_8));
    }

    public static void saveJson(File folder, String file, Object o) {
        try (PrintWriter out = openWriterUTF8(new File(folder, file), false)) {
            out.println(gson.toJson(o));
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    public static void printJson(File folder, String file, Object o) {
        try (PrintWriter out = openWriterUTF8(new File(folder, file), true)) {
            out.println(gson.toJson(o));
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    public static void println(File folder, String file, Object o) {
        try (PrintWriter out = openWriterUTF8(new File(folder, file), true)) {
            out.println(o);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    public static void write(File file, Object o) {
        try (PrintWriter out = openWriterUTF8(file, false)) {
            out.print(o);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    public static String toString(Throwable ex) {
        StringWriter w = new StringWriter();
        ex.printStackTrace(new PrintWriter(w));
        return w.toString();
    }

    public static void clear(File folder, String file) {
        new File(folder, file).delete();
    }

    public static boolean isIn(java.util.ArrayList<String> array, String s) {
        for (String item : array) {
            if (item.equals(s)) {
                return true;
            }
        }
        return false;
    }

    public static int parseInt(String s, int _default) {
        if (s == null) {
            return _default;
        }
        try {
            return Integer.parseInt(s);
        } catch (Exception ex) {
        }
        return _default;
    }

    public static long parseLong(String s, long _default) {
        if (s == null) {
            return _default;
        }
        try {
            return Long.parseLong(s);
        } catch (Exception ex) {
        }
        return _default;
    }

    public static boolean isLinuxServer() {
        return System.getProperties().get("os.name").equals("Linux");
    }

    private static DocumentBuilder xmlDocBuilder = null;

    public static DocumentBuilder getXmlDocBuilder()
            throws ParserConfigurationException {
        if (xmlDocBuilder == null) {
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            xmlDocBuilder = dbFactory.newDocumentBuilder();
        }
        return xmlDocBuilder;
    }

    private static TransformerFactory xmlTransformFactory = null;

    public static TransformerFactory getXmlTransformFactory() {
        if (xmlTransformFactory == null) {
            xmlTransformFactory = TransformerFactory.newInstance();
        }
        return xmlTransformFactory;
    }

    public static Document parseXml(String txt)
            throws ParserConfigurationException, SAXException, IOException {
        Document doc;
        try (Reader reader = new StringReader(txt)) {
            doc = parseXml(reader);
        }
        return doc;
    }

    public static Document parseXml(Reader reader)
            throws ParserConfigurationException, SAXException, IOException {
        DocumentBuilder docBuilder = getXmlDocBuilder();
        InputSource source = new InputSource(reader);
        Document doc = docBuilder.parse(source);
        return doc;
    }

    public static Document createXmlDoc() throws ParserConfigurationException {
        DocumentBuilder docBuilder = getXmlDocBuilder();
        return docBuilder.newDocument();
    }

    public static String getXmlString(Document doc) throws TransformerException {
        DOMSource domSource = new DOMSource(doc);
        StringWriter writer = new StringWriter();
        StreamResult result = new StreamResult(writer);
        TransformerFactory tf = getXmlTransformFactory();
        Transformer transformer = tf.newTransformer();
        transformer.transform(domSource, result);
        return writer.toString();
    }

    public static Element createXmlTextElement(Document doc, String ename, String txt) {
        Element e = doc.createElement(ename);
        e.appendChild(doc.createTextNode(txt));
        return e;
    }

    public static Element createSubXmlElement(Document doc, Element parent, String ename) {
        Element e = doc.createElement(ename);
        parent.appendChild(e);
        return e;
    }

    public static Element createSubXmlTextElement(Document doc, Element parent, String ename, String txt) {
        Element e = createXmlTextElement(doc, ename, txt);
        parent.appendChild(e);
        return e;
    }

    private static File _dataFolder = null;

    public static File dataFolder() {
        if (_dataFolder == null) {
            String path = System.getProperty("user.home") + File.separatorChar + "GEL";
            _dataFolder = new File(path);
            _dataFolder.mkdirs();
        }
        return _dataFolder;
    }

    public static File getDataFile(String file) {
        return new File(dataFolder(), file);
    }

    public static void saveJson(String file, Object o) {
        try (PrintWriter out = openWriterUTF8(getDataFile(file), false)) {
            out.println(gson.toJson(o));
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    public static void saveJson(File file, Object o) {
        try (PrintWriter out = openWriterUTF8(file, false)) {
            out.println(gson.toJson(o));
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    public static <T> T loadJson(String file, Class<T> clazz) {
        return loadPson(getDataFile(file), clazz);
    }

    public static <T> T loadPson(File file, Class<T> clazz) {
        if (file.exists()) {
            try (FileInputStream fis = new FileInputStream(file)) {
                byte[] data = new byte[(int) file.length()];
                fis.read(data);
                return gson.fromJson(new String(data, "UTF-8"), clazz);
            } catch (IOException ex) {
                Logger.getLogger(Func.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return null;
    }
    public static final String DEFAULT_LANGUAGE = "ar";

    public static String getLanguage(HttpServletRequest request, HttpSession session) {
        String lang = request.getParameter("lang");
        /*	if (lang == null || lang.isEmpty()) {
			lang = (String)session.getAttribute("lang");
			if (lang == null || lang.isEmpty()) {
				lang = DEFAULT_LANGUAGE;
			}
		}
		else {
			session.setAttribute("lang", lang);
		}	*/
        // don't store lang in the session
        if (lang == null || lang.isEmpty()) {
            lang = DEFAULT_LANGUAGE;
        }
        return lang;
    }

    public static String getLanguage(FileUploadRequest request, HttpSession session) {
        String lang = request.getParameter("lang");
        if (lang == null || lang.isEmpty()) {
            lang = DEFAULT_LANGUAGE;
        }
        return lang;
    }

    public static String getLayoutDirection(String lang) {
        if (lang == null || lang.isEmpty()) {
            return "ltr";
        }
        if (lang.equals("ar")) {
            return "rtl";
        }
        return "ltr";
    }

    public static String getTextAlign(String lang) {
        if (lang == null || lang.isEmpty()) {
            return "left";
        }
        if (lang.equals("ar")) {
            return "right";
        }
        return "left";
    }

    public static String getHostname() {
        try {
            return InetAddress.getLocalHost().getHostName();
        } catch (UnknownHostException ex) {
            return "";
        }
    }

    public static String exec(String cmd) {
        ByteArrayOutputStream os = new ByteArrayOutputStream();
        exec(cmd, os);
        return new String(os.toByteArray());
    }

    public static int exec(String cmd, OutputStream os) {
        try {
            Process p = isLinuxServer() ? Runtime.getRuntime().exec(new String[]{"bash", "-c", cmd}) : Runtime.getRuntime().exec(cmd);
            p.waitFor();
            InputStream is = p.getInputStream();
            InputStream err = p.getErrorStream();
            int len = 0;
            byte[] bb = new byte[1024];
            while ((len = is.read(bb)) > 0) {
                os.write(bb, 0, len);
            }
            while ((len = err.read(bb)) > 0) {
                os.write(bb, 0, len);
            }
            return p.exitValue();
        } catch (Exception e) {
            DB.onException(e, cmd);
        }
        return -1;
    }

    ////////////////////////////////////////////////////////////////////////
    // log handling
    private static File _logFolder = null;
    private static SimpleDateFormat sdfUTC = null;

    public static File logFolder() {
        if (_logFolder == null) {
            _logFolder = new File("/mnt/mw/logs");
            _logFolder.mkdirs();
        }
        return _logFolder;
    }

    public static String todayServer() {
        if (sdfUTC == null) {
            sdfUTC = new SimpleDateFormat("yyyy-MM-dd");
            sdfUTC.setTimeZone(TimeZone.getTimeZone("UTC"));
        }
        return sdfUTC.format(new Date());
    }
    
    public static String getSDateUTC(long time){
        if (sdfUTC == null) {
            sdfUTC = new SimpleDateFormat("yyyy-MM-dd");
            sdfUTC.setTimeZone(TimeZone.getTimeZone("UTC"));
        }
        return sdfUTC.format(new Date(time));
    }

    public static File logFile(String file) {
        return new File(logFolder(), todayServer() + "." + file);
    }

    public static void printlnLog(String file, Object text) {
        try (PrintWriter out = openWriterUTF8(logFile(file), true)) {
            out.println(now() + ": " + text);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    public static void printlnLog(String file, Exception ex) {
        printlnLog(file, toString(ex));
    }

    public static void printlnLog(String file, HttpServletRequest request) throws IOException {
        String data = IOUtils.toString(request.getInputStream());
        StringBuilder log = new StringBuilder();
        log.append(Func.pson(Func.getHeaders(request))).append(System.lineSeparator());
        log.append(Func.pson(request.getParameterMap())).append(System.lineSeparator());
        if (data != null && !data.isEmpty()) {
            log.append(data);
        }
        printlnLog(file, request.getRequestURI() + ": " + log.toString());
    }

    public static HashMap<String, String> getHeaders(HttpServletRequest request) {
        HashMap<String, String> headers = new HashMap();
        Enumeration<String> eh = request.getHeaderNames();
        while (eh.hasMoreElements()) {
            String name = eh.nextElement();
            headers.put(name, request.getHeader(name));
        }
        return headers;
    }
}
