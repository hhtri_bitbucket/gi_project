/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.dp.db;

/**
 *
 * @author ccthien
 */
public class BaseDataScoreInt extends BaseDataInt implements BaseDataScoreInterface {
    public long score;
    
    @Override
    public long getScore() {
        return score;
    }

    @Override
    public long getTime() {
        return LastUpdatedAt;
    }

    @Override
    public void increaseTime() {
        LastUpdatedAt++;
    }
}
