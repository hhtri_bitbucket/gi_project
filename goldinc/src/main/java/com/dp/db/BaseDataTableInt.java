/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.dp.db;

import java.util.*;

/**
 *
 * @author ccthien
 */
public class BaseDataTableInt<T extends BaseDataInt> {
    public final TreeMap<Integer,T>data = new TreeMap<Integer,T>();
    public final TreeMap<Long,T>time = new TreeMap<Long,T>();
//    public final HashMap<String,T>unique = new HashMap<String,T>();
    public int nextID() {
        if(data.isEmpty()) return 1;
        else return data.lastKey() + 1;
    }
    public boolean add(T t) {
        synchronized(this) {
            if(t.ID == 0) {
                t.ID = nextID();
            }else if(data.get(t.ID)!=null) {
                return false;
            }
            data.put(t.ID, t);
            afterUpdate(t);
        }
        return true;
    }
    public void beforeUpdate(T t) {
        time.remove(t.LastUpdatedAt);
//        unique.remove(Func.kson(t));
    }
    public void afterUpdate(T t) {
        time.put(t.LastUpdatedAt,t);
//        unique.put(Func.kson(t),t);
    }
}
