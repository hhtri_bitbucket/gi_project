/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.dp.db;

import com.google.gson.annotations.ExcluseJson;

/**
 *
 * @author ccthien
 */
public class BaseData extends BaseFunc {
    @ExcluseJson(scope = "Unity") public boolean Changed = true;
    public long CreatedAt=0;
    public long LastUpdatedAt=0;//System.currentTimeMillis();
    public final void updateTime() {
        LastUpdatedAt=System.currentTimeMillis();
        if(CreatedAt==0) CreatedAt = LastUpdatedAt;
    }
    public String table() {return this.getClass().getSimpleName();}
    public void clearAll() {DBOracle.i().delete(table());}
    public boolean save() {Changed = true;return saveOnChanged();}
    public boolean saveOnChanged() {return false;}
}
