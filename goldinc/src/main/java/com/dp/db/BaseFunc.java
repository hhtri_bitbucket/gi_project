/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.dp.db;

import com.ctmlab.util.FileUploadRequest;
import java.io.File;
import java.nio.charset.Charset;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringEscapeUtils;

/**
 *
 * @author ccthien
 */
public class BaseFunc {
    public void set(javax.servlet.http.HttpServletRequest request) {
        for (java.lang.reflect.Field f : this.getClass().getDeclaredFields()) {
            if(java.lang.reflect.Modifier.isStatic(f.getModifiers())) continue;
            try {
                String value = request.getParameter(f.getName());
                if(value!=null) {
                    f.setAccessible(true);
                    if(f.getType()==int.class) {
                        f.set(this, Integer.parseInt(value));
                    }else if(f.getType()==float.class) {
                        f.set(this, Float.parseFloat(value));
                    }else if(f.getType()==long.class) {
                        f.set(this, Long.parseLong(value));
                    }else if(f.getType()==String.class) {
                        f.set(this, new String(value.getBytes(Charset.forName("ISO-8859-1")),Charset.forName("UTF-8")));
                    }else if(f.getType()==String[].class) {
                        f.set(this, value.split(","));
                    }else if(f.getType()==boolean.class) {
                        f.set(this, "on".equals(value));
                    }
                }
            }catch(IllegalAccessException | IllegalArgumentException | SecurityException ex) {Func.printlnLog(this.getClass().getSimpleName()+"_err.log",ex);}
        }        
    }
    public void setFormField(java.lang.reflect.Field f,String item) throws IllegalArgumentException, IllegalAccessException {
        f.setAccessible(true);
        if(f.getType()==int.class) {
            f.set(this, Integer.parseInt(item));
        }else if(f.getType()==float.class) {
            f.set(this, Float.parseFloat(item));
        }else if(f.getType()==long.class) {
            f.set(this, Long.parseLong(item));
        }else if(f.getType()==boolean.class) {
            f.set(this, Boolean.parseBoolean(item));
        }else if(f.getType()==String.class) {
            f.set(this, item);
            //f.set(this, new String(item.getString().getBytes(Charset.forName("ISO-8859-1")),Charset.forName("UTF-8")));
        }else if(f.getType()==String[].class) {
            f.set(this, item.split(","));
        }
    }
    public void setFormFile(java.lang.reflect.Field f,FileItem item,String folder) throws Exception {
        if(f.getType()==String.class) {
            String ext = FilenameUtils.getExtension(item.getName()).toLowerCase();
            if(Func.isImageExtension(ext) && item.getSize()>0) {
//                String filename = item.getName();
//                java.lang.reflect.Field id_field = this.getClass().getField("ID");
//                if(id_field!=null) {filename = id_field.get(this).toString()+"."+ext;}
                
                String old = (String)f.get(this);
                if(old!=null) {
                    File f_old = new File(Func.getTomcatImageFolder(folder),old);
                    if(f_old.exists()) f_old.delete();
                }
                
                String filename = System.currentTimeMillis()+"_"+System.nanoTime()+"."+ext;
                item.write(new File(Func.getTomcatImageFolder(folder),filename));
                f.set(this, filename);
            }
        }
    }
    public void set(FileUploadRequest req,String folder) {
        for(java.lang.reflect.Field f:getFields()) {
            if(java.lang.reflect.Modifier.isStatic(f.getModifiers())) continue;
            try {
                FileItem item = req.getFile(f.getName());
                if(item!=null) {
					setFormFile(f,item,folder);
                }else{
					String fitem = req.getParameter(f.getName());
					if(fitem!=null) {setFormField(f,fitem);}
				}
            }catch(Exception ex) {Func.printlnLog(this.getClass().getSimpleName()+"_err.log",ex);}
        }        
    }
    public void setData(FileUploadRequest req) {
        for(java.lang.reflect.Field f:getFields()) {
            if(java.lang.reflect.Modifier.isStatic(f.getModifiers())) continue;
            try {
                String data = req.getParameter(f.getName());
                if(data!=null) {
					setFormField(f,data);
                }
            }catch(Exception ex) {Func.printlnLog(this.getClass().getSimpleName()+"_err.log",ex);}
        }        
    }
    public void setFile(FileUploadRequest req,String folder) {
        for(java.lang.reflect.Field f:getFields()) {
            if(java.lang.reflect.Modifier.isStatic(f.getModifiers())) continue;
            try {
                FileItem item = req.getFile(f.getName());
                if(item!=null) {
					setFormFile(f,item,folder);
                }
            }catch(Exception ex) {Func.printlnLog(this.getClass().getSimpleName()+"_err.log",ex);}
        }        
    }
    public java.util.ArrayList<java.lang.reflect.Field>getFields() {
        java.util.ArrayList<java.lang.reflect.Field>fields = new java.util.ArrayList<java.lang.reflect.Field>();
        Class clazz = this.getClass();
        while(clazz!=Object.class) {
            for (java.lang.reflect.Field f : clazz.getDeclaredFields()) {fields.add(f);}
            clazz = clazz.getSuperclass();
        }
        return fields;
    }
    public boolean hasFileUpload() {
        for(java.lang.reflect.Field f:getFields()) {
            if(java.lang.reflect.Modifier.isStatic(f.getModifiers())) continue;
            FieldDataType ftype = f.getAnnotation(FieldDataType.class);
            if(ftype!=null && ftype.type()==FieldDataTypeEnum.FILE) return true;
        }
        return false;
    }
    public String htmlTable() {
        StringBuilder sb = new StringBuilder();
        sb.append("<table>\n");
        for(java.lang.reflect.Field f:getFields()) {
            try {
                if(java.lang.reflect.Modifier.isStatic(f.getModifiers())) continue;
                Object data = f.get(this);
                if(f.getType() == boolean.class) {
                    sb.append("<tr><td>").append(f.getName()).append("</td><td><input type='hidden' name='"+f.getName()+"' value='"+data+"'><input type='checkbox' "+(((boolean)data)?"checked":"")+" onchange='this.form[\""+f.getName()+"\"].value=this.checked' /></td></tr>\n");
                }else{
                    String sdata = data==null?"":data.toString();
                    FieldDataType ftype = f.getAnnotation(FieldDataType.class);
                    FieldDataTypeEnum type = (ftype==null)?FieldDataTypeEnum.TEXT:ftype.type();
                    sb.append("<tr><td>").append(f.getName()).append("</td><td><input type='").append(type).append("' name='").append(f.getName()).append("' value='").append(StringEscapeUtils.escapeHtml4(sdata)).append("'/></td></tr>\n");
                }
            } catch (IllegalArgumentException ex) {
            } catch (IllegalAccessException ex) {
            }
        }
        sb.append("</table>\n");
        return sb.toString();
    }
    public String htmlForm(){
        StringBuilder sb = new StringBuilder();
        sb.append("<form method=\"post\""+(hasFileUpload()?" enctype=\"multipart/form-data\"":"")+">\n");
        sb.append(htmlTable());
        sb.append("<input type=\"submit\"/>\n</form>\n");
        return sb.toString();
    }
    public final void copy(Object p) {
        for (java.lang.reflect.Field f : getFields()) {
            if(java.lang.reflect.Modifier.isStatic(f.getModifiers())) continue;
            try {
                FieldDataType ftype = f.getAnnotation(FieldDataType.class);
                if(ftype!=null && ftype.skip_copy()) continue;
                Object val = f.get(p);
                if(val!=null) {
                    f.setAccessible(true);
                    f.set(this, val);
                }
            }catch(Exception ex) {Func.printlnLog(this.getClass().getSimpleName()+"_err.log",ex);}
        }
    }
}
