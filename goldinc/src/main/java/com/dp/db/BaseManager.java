/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.dp.db;

/**
 *
 * @author ccthien
 */
public class BaseManager {
    private static final DataStatus status = new DataStatus();
    
    public void doLoad() {}
    public final void load() {
        if(status.isNew()) {
            status.loading();
            doLoad();
            status.loaded();
        }
        
    }
}
