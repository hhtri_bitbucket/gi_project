/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.dp.db.data;

import java.util.HashMap;

/**
 *
 * @author ccthien
 */
public class TwoSideHashMap<Key,Value> {
    private final HashMap<Key,Value> keyHash = new HashMap();
    private final HashMap<Value,Key> valueHash = new HashMap();
    public void put(Key key,Value value) {
        synchronized(this) {
            Value old_value = keyHash.get(key);if(old_value!=null) valueHash.remove(old_value);
            Key old_key = valueHash.get(value);if(old_key!=null) keyHash.remove(old_key);
            keyHash.put(key, value);
            valueHash.put(value, key);
        }
    }
    public void remove(Key key) {
        synchronized(this) {
            Value value = keyHash.get(key);
            if(value!=null) {
                valueHash.remove(value);
                keyHash.remove(key);
            }
        }
    }
    public Value get(Key key) {return keyHash.get(key);}
    public void removeValue(Value value) {
        synchronized(this) {
            Key key = valueHash.get(value);
            if(key!=null) {
                valueHash.remove(value);
                keyHash.remove(key);
            }
        }
    }
    public Key getKey(Value value) {return valueHash.get(value);}
}
