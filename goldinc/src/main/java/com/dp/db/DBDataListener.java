/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.dp.db;

/**
 *
 * @author ccthien
 * @param <T>
 */
public interface DBDataListener<T extends DBData> {
    void onItem(T t);
}
