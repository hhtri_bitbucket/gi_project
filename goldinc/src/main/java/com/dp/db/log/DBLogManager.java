/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dp.db.log;

import com.dp.db.DB;
import com.dp.db.DBData;
import com.dp.db.Func;
import java.sql.SQLException;
import java.util.Collection;
import java.util.HashSet;
import java.util.TreeMap;

/**
 *
 * @author ccthien
 * @param <T>
 */
public class DBLogManager<T extends DBLog> {

    public static final String toSQL(Collection<String> ss) {
        StringBuilder sb = new StringBuilder();
        ss.forEach((s) -> {
            if (sb.length() != 0) {
                sb.append(",");
            }
            sb.append("'").append(s).append("'");
        });
        return sb.toString();
    }

    private final Class<T> logClass;

    public DBLogManager(Class<T> clazz) {
        logClass = clazz;
    }

    private T newLog() {
        try {
            return logClass.newInstance();
        } catch (IllegalAccessException | InstantiationException ex) {
            throw new RuntimeException(ex);
        }
    }

    public DBLogManager validate() {
        try {
            DB.i().validateTable(newLog());
        } catch (SQLException ex) {
            DB.onException(ex, "validate");
        }
        return this;
    }

    public long lastID() {
        return DB.i().lastAutoID(logClass);
    }

    public void add(DBData data, int action) {
        if (ignoreClasses.contains(data.getClass())) {
            return;
        }
        DBLog log = newLog();
        log.table = data.table();
        log.action = action;
        log.data = Func.json(data);
        log.save();
    }
    private final HashSet<Class> ignoreClasses = new HashSet();

    public void addIgnoreClass(Class clazz) {
        ignoreClasses.add(clazz);
    }

    public TreeMap<Long, DBLogRecord> getLog(long fromID) {
        final TreeMap<Long, DBLogRecord> map = new TreeMap();
        try {
            DB.i().select(logClass, "WHERE " + DB.i().tableSQL("idx") + " > " + fromID, (t) -> {
                if (t.data == null) {
                    map.put(t.idx, new DBLogRecord(t.action, null));
                } else {
                    Class clazz = DB.i().getTableClass(t.table);
                    if (clazz != null) {
                        map.put(t.idx, new DBLogRecord(t.action, (DBData) Func.gson.fromJson(t.data, clazz)));
                    }
                }
            });
        } catch (SQLException ex) {
            DB.onException(ex, fromID);
        }
        return map;
    }
}
