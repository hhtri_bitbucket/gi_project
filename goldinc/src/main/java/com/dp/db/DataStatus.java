/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.dp.db;

/**
 *
 * @author ccthien
 */
public class DataStatus {
    public static final int STATUS_NEW = 0;
    public static final int STATUS_LOADING = 1;
    public static final int STATUS_LOADED = 2;
    
    private int status = STATUS_NEW;
    public void reset() {status = STATUS_NEW;}
    public void loading() {status = STATUS_LOADING;}
    public void loaded() {status = STATUS_LOADED;}
    public boolean isNew() {return status == STATUS_NEW;}
    public boolean isLoading() {return status == STATUS_LOADING;}
    public boolean isLoaded() {return status == STATUS_LOADED;}
}
