/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.dp.db;

import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author ccthien
 */
public class BaseDataString extends BaseData {
    public String ID;
	@Override
    public boolean saveOnChanged() {
        if(ID==null || ID.isEmpty()) return false;
        if(Changed) {
            Changed = false;
            DBOracle.i().save(table(), ID, Func.json(this));
        }
        return true;
    }
    public void delete() {DBOracle.i().delete(table(),ID);}
}
