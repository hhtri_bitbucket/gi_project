/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dp.db;

import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.EOFException;
import java.io.IOException;
import java.lang.reflect.Field;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author hhtri
 */
public class DBDataPostgres extends DBData {

    private static final DBCacheSQL updateSQLs = new DBCacheSQL();

    private static final HashMap<Class, Field> autoFields = new HashMap<Class, Field>();

    private static final ArrayList<String> primaryFields = new ArrayList();

    private static final ArrayList<ByteArrayInputStream> mBufferStream = new ArrayList();

    public static Field getAutoField(Class c) {
        return autoFields.get(c);
    }

    private static String dbType(DataFieldType type, Class clazz, int size) {
//        java.lang.reflect.Field field = null;
        if (type!=null&&!type.childField().isEmpty()) {
            try {
                java.lang.reflect.Field field = clazz.getField(type.childField());
                clazz = field.getType();
                type = field.getAnnotation(DataFieldType.class);
                size = (type == null) ? 0 : type.size();
            } catch (NoSuchFieldException e) {
                DB.i().onLog("dbdata_dbType", Func.now() + ":\n" + Func.toString(e));
            }
        }
        String sNotNull = (type!=null&&type.notNull()) ? " NOT NULL" : "";
        if (clazz == long.class || clazz == Long.class) {
            String rs = "bigint";
            if(type!=null){
                if(type.autoIncrease()&&type.usingMapSeq()){
                    rs = "bigint DEFAULT nextval('map_id_seq'::regclass)";
                } else{
                    rs = "bigserial";
                }                
            }
            return rs;
        } else if (clazz == int.class || clazz == Integer.class) {
            return ((type!=null&&type.autoIncrease()) ? "serial" : "integer") + sNotNull;
        } else if (clazz == short.class || clazz == Short.class) {
            return ((type!=null&&type.autoIncrease()) ? "smallserial" : "smallint") + sNotNull;
        } else if (clazz == boolean.class || clazz == Boolean.class) {
            return ("boolean") + sNotNull;
        } else if (clazz == double.class || clazz == Double.class) {
            return ("double precision") + sNotNull;
        } else if (clazz == float.class || clazz == Float.class) {
            return ("real") + sNotNull;
        } else if (clazz == Date.class) {
            return ((type!=null&&type.isTimestamp()) ? "timestamp DEFAULT CURRENT_TIMESTAMP" : "timestamp") + sNotNull;
        } else if (clazz == byte[].class) {
            return ("bytea") + sNotNull;
        } else if (clazz == String.class && size > 0) {
            return ("character(" + size + ")") + sNotNull;
        } else {
            return ("text") + sNotNull;
        }
    }

    private static String toSqlTableType(DataFieldType type, Class clazz, String name, int size) {
        StringBuilder sb = new StringBuilder();
        sb.append("\t").append(name).append(" ").append(dbType(type, clazz, size)).append(",\n");
        return sb.toString();
    }

    @Override
    public String table() {
        return this.getClass().getSimpleName().toLowerCase();
    }

    @Override
    public ArrayList<DBDataField> getDataFields() {
        ArrayList<DBDataField> fields = new ArrayList<>();
        primaryFields.clear();
        Class clazz = this.getClass();
        while (clazz != DBDataPostgres.class) {
            for (java.lang.reflect.Field f : clazz.getDeclaredFields()) {
                if (java.lang.reflect.Modifier.isStatic(f.getModifiers())) {
                    continue;
                }
                try {
                    int size = 0;
                    DataFieldType type = f.getAnnotation(DataFieldType.class);
                    if (type != null) {
                        if (type.ignore()) {
                            continue;
                        }
                        size = type.size();
                        if (type.isMap()) {
                            f.setAccessible(true);
                            Map map = (Map) f.get(this);
                            for (Object key : map.keySet()) {
                                Object value = map.get(key);
                                fields.add(new DBDataField(type, value.getClass(), key.toString(), size, type.primaryKey()));
                            }
                            continue;
                        }
                        if (type.autoIncrease()) {
                            autoFields.put(this.getClass(), f);
                        }
                        if (type.primaryKey()) {
                            primaryFields.add(f.getName().toLowerCase());
                            continue;
                        }
                    }
                    fields.add(new DBDataField(type, f.getType(), f.getName().toLowerCase(), size, false));
                } catch (IllegalAccessException | IllegalArgumentException | SecurityException e) {
                    DB.i().onLog(table(), Func.now() + "\nMethod - getDataFields\n" + Func.json(this) + "\n" + Func.toString(e));
                }
            }
            clazz = clazz.getSuperclass();
        }
        return fields;
    }
    
    public ArrayList<DBDataField> getPrimaryFields() {
        ArrayList<DBDataField> fields = new ArrayList<>();
        Class clazz = this.getClass();
        while (clazz != DBDataPostgres.class) {
            for (java.lang.reflect.Field f : clazz.getDeclaredFields()) {
                if (java.lang.reflect.Modifier.isStatic(f.getModifiers())) {
                    continue;
                }
                try {
                    int size = 0;
                    DataFieldType type = f.getAnnotation(DataFieldType.class);
                    if (type != null) {
                        if (type.ignore()) {
                            continue;
                        }
                        size = type.size();
                        if (type.isMap()) {
                            f.setAccessible(true);
                            Map map = (Map) f.get(this);
                            for (Object key : map.keySet()) {
                                Object value = map.get(key);
                                fields.add(new DBDataField(type, value.getClass(), 
                                        key.toString(), size, type.primaryKey()));
                            }
                            continue;
                        }
                        if (type.autoIncrease()) {
                            autoFields.put(this.getClass(), f);
                        }
                        if (type.primaryKey()) {
                            primaryFields.add(f.getName().toLowerCase());
                        }

                    }
                    fields.add(new DBDataField(type, f.getType(), f.getName().toLowerCase(), size, false));
                } catch (IllegalAccessException | IllegalArgumentException | SecurityException e) {
                    DB.i().onLog(table(), Func.now() + "\nMethod - getDataFields\n" + Func.json(this) + "\n" + Func.toString(e));
                }
            }
            clazz = clazz.getSuperclass();
        }
        return fields;
    }

    @Override
    public String getSqlTable() {
        StringBuilder sb = new StringBuilder();
        StringBuilder primary_key = new StringBuilder();
        StringBuilder unique_key = new StringBuilder();
        sb.append("CREATE TABLE IF NOT EXISTS ").append(table()).append(" (\n");
        Class clazz = this.getClass();
        while (clazz != DBDataPostgres.class) {
            for (java.lang.reflect.Field f : clazz.getDeclaredFields()) {

                if (java.lang.reflect.Modifier.isStatic(f.getModifiers())) {
                    continue;
                }
                try {
                    int size = 0;
                    DataFieldType type = f.getAnnotation(DataFieldType.class);
                    if (type != null) {
                        if (type.ignore()) {
                            continue;
                        }
                        size = type.size();
                        if (type.primaryKey()) {
                            if (primary_key.length() > 0) {
                                primary_key.append(",");
                            }
                            primary_key.append(f.getName().toLowerCase());
                        }
                        if (type.uniqueKey()) {
                            if (unique_key.length() > 0) {
                                unique_key.append(",");
                            }
                            unique_key.append(f.getName().toLowerCase());
                        }
                        if (type.isMap()) {
                            f.setAccessible(true);
                            Map map = (Map) f.get(this);
                            for (Object key : map.keySet()) {
                                Object value = map.get(key);
                                sb.append(toSqlTableType(type, value.getClass(), key.toString(), size));
                            }
                            continue;
                        }
                    }
                    sb.append(toSqlTableType(type, f.getType(), f.getName().toLowerCase(), size));
                } catch (IllegalAccessException | IllegalArgumentException | SecurityException e) {
                    DB.i().onLog(table(), Func.now() + "\nMethod - getSqlTable\n" + Func.json(this) + "\n" + Func.toString(e));
                }
            }
            clazz = clazz.getSuperclass();
        }
        if(unique_key.length()>0){        
            String uniqueCode = String.format("\tCONSTRAINT unique_%s_key UNIQUE (%s)\n)", table(), unique_key);
            String primaryCode = String.format("\tCONSTRAINT %s_key PRIMARY KEY (%s),\n%s", table(), primary_key, uniqueCode);
//            sb.append("\tCONSTRAINT ").append(table()).append("_key PRIMARY KEY (") .append(primary_key).append(")\n").append(")");
            sb.append(primaryCode);
        } else{
            String primaryCode = String.format("\tCONSTRAINT %s_key PRIMARY KEY (%s)\n)", table(), primary_key);
//            sb.append("\tCONSTRAINT ").append(table()).append("_key PRIMARY KEY (") .append(primary_key).append(")\n").append(")");
            sb.append(primaryCode);
        }
//        String primaryCode = String.format("\tCONSTRAINT %s_key PRIMARY KEY (%s)\n", table(), primary_key);
//        
//        String uniqueCode = String.format("\tCONSTRAINT unique_%s_key UNIQUE (%s)\n", table(), unique_key);
//        sb.append("\tCONSTRAINT ").append(table()).append("_key PRIMARY KEY (") .append(primary_key).append(")\n").append(")");
//        System.out.println(sb.toString());
        return sb.toString();
    }

    @Override
    public int createTable(Connection connection) throws SQLException {
        int result;
        String sql = getSqlTable();
//        System.out.println(sql);
        try (PreparedStatement st = connection.prepareStatement(sql)) {
            result = st.executeUpdate();
        }
        return result;
    }

    @Override
    public void validateTable(Connection connection) throws SQLException {
        createTable(connection);
        HashMap<String, String> db_fields = new HashMap();
        HashMap<String, String> fields = new HashMap();

        // get info class
        getDataFields().forEach((f) -> {
            fields.put(f.name, dbType(f.type, f.clazz, f.size));
        });
        String sql = "select column_name, data_type, character_maximum_length, column_default from INFORMATION_SCHEMA.COLUMNS where table_name ='" + table() + "'";
        try (PreparedStatement st = connection.prepareStatement(sql)) {
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                String sName = rs.getString(1);
                String sType = rs.getString(2);
                int len = rs.getInt(3);
                String sSeq = rs.getString(4);
                String mSeq = table() + "_" + sName + "_seq";
                if ("character".equals(sType) && len > 0) {
                    sType = sType + "(" + len + ")";
                } else if (sSeq != null && ("bigint".equals(sType) || "smallint".equals(sType) || "integer".equals(sType)) && sSeq.contains(mSeq)) {
                    sType = sType.replace("bigint", "bigserial").replace("smallint", "smallserial").replace("integer", "serial");
                }
                db_fields.put(sName, sType);
            }
            rs.close();
            st.close();
        } catch (Exception ex) {
            DB.i().onLog(table(), Func.now() + "\nMethod - validateTable\nINFORMATION_SCHEMA.COLUMNS\n" + Func.toString(ex));
            return;
        }

        for (String f : fields.keySet()) {
            sql = null;
            String df = db_fields.get(f);
            if (df == null) {
                sql = "ALTER TABLE " + table() + " ADD COLUMN " + f + " " + fields.get(f);
            } else if (!fields.get(f).equals(df)) {
                sql = "ALTER TABLE " + table() + " ALTER COLUMN " + f + " TYPE " + fields.get(f);
            }
            if (sql != null) {
//                System.out.println(sql);
                try (PreparedStatement statement = connection.prepareStatement(sql)) {
                    statement.executeUpdate();
                    statement.close();
                } catch(Exception ex){
                    DB.i().onLog(table(), Func.now() + "\nMethod - validateTable\n" + sql + "\n" + Func.toString(ex));
                }
            }
        }

        if (!primaryFields.isEmpty()) {
            String sPrimary = "";
            for (String f : primaryFields) {
                if (!sPrimary.isEmpty()) { sPrimary += ","; }
                sPrimary += f;
            }
            String sqlPri = "";
            sqlPri += "ALTER TABLE #tbl# DROP CONSTRAINT #tbl#_key;\n";
            sqlPri += "ALTER TABLE #tbl# ADD CONSTRAINT #tbl#_key PRIMARY KEY(#keys#);";

            sqlPri = sqlPri.replaceAll("#tbl#", table()).replace("#keys#", sPrimary);
            try (PreparedStatement statement = connection.prepareStatement(sqlPri)) {
                statement.executeUpdate();
                statement.close();
            } catch(Exception ex){
                DB.i().onLog(table(), Func.now() + "\nMethod - validateTable\n" + sqlPri + "\n" + Func.toString(ex));
            }
        }
    }

    //////////////////////////////////////////////////////////////////////////
    @Override
    public void set(HttpServletRequest request) {
        Class clazz = this.getClass();
        while (clazz != DBDataPostgres.class) {
            for (java.lang.reflect.Field f : clazz.getDeclaredFields()) {
                if (java.lang.reflect.Modifier.isStatic(f.getModifiers())) {
                    continue;
                }
                try {
                    String value = request.getParameter(f.getName().toLowerCase());
                    if (value != null) {
                        Class type = f.getType();
                        if (type == long.class || type == Long.class) {
                            f.setLong(this, Func.parseLong(value, 0));
                        } else if (type == int.class || type == Integer.class) {
                            f.setInt(this, Func.parseInt(value, 0));
                        } else if (type == boolean.class || type == Boolean.class) {
                            f.setBoolean(this, "true".equals(value));
                        } else if (type == double.class || type == Double.class) {
                            f.setDouble(this, Double.parseDouble(value));
                        } else if (type == String.class || type == StringBuilder.class) {
                            f.set(this, value);
                        } else if (type == Date.class) {
                            try {
                                f.set(this, Func.SDF_DATE.parse(value));
                            } catch (ParseException e) {
                                DB.onException(e, this);
                            }
                        }
                    }
                } catch (IllegalAccessException e) {
                    DB.onException(e, this);
                }
            }
            clazz = clazz.getSuperclass();
        }
    }

    @Override
    public int getFieldCount() {
        int fieldCount = 0;
        Class clazz = this.getClass();
        while (clazz != DBDataPostgres.class) {
            for (java.lang.reflect.Field f : clazz.getDeclaredFields()) {
                if (java.lang.reflect.Modifier.isStatic(f.getModifiers())) {
                    continue;
                }
                f.setAccessible(true);
                try {
                    DataFieldType type = f.getAnnotation(DataFieldType.class);
                    if (type != null) {
                        if (type.isMap()) {
                            Map map = (Map) f.get(this);
                            fieldCount += map.size();
                            continue;
                        }
                        if (type.isTimestamp() || type.autoIncrease()) {
                            continue;
                        }
                        if (type.ignore()) {
                            continue;
                        }
                    }
                    fieldCount++;
                } catch (IllegalAccessException ex) {
                    DB.i().onLog(table(), Func.now() + "\nMethod - getFieldCount\n" + Func.toString(ex));
//                    Func.printlnLog(this.getClass().getSimpleName() + "_err.log", ex);
                }
            }
            clazz = clazz.getSuperclass();
        }
        return fieldCount;
    }

    @Override
    public void setValueEx(PreparedStatement st, int idx, Class clazz, Object value) throws SQLException {
        if (clazz == long.class || clazz == Long.class) {
            st.setLong(idx, (Long) value);
        } else if (clazz == int.class || clazz == Integer.class) {
            st.setInt(idx, (Integer) value);
        } else if (clazz == short.class || clazz == Short.class) {
            st.setShort(idx, (Short) value);
        } else if (clazz == boolean.class || clazz == Boolean.class) {
            st.setBoolean(idx, (Boolean) value);
        } else if (clazz == double.class || clazz == Double.class) {
            st.setDouble(idx, (Double) value);
        } else if (clazz == float.class || clazz == Float.class) {
            st.setFloat(idx, (Float) value);
        } else if (clazz == String.class || clazz == StringBuilder.class) {
            st.setString(idx, value == null ? "" : value.toString());
        } else if (clazz == Date.class) {
            st.setDate(idx, new java.sql.Date(((Date) value).getTime()));
        } else if (clazz == byte[].class) {
            byte[] data = (byte[]) value;
            ByteArrayInputStream bais = new ByteArrayInputStream(data);
            st.setBinaryStream(idx, bais, data.length);
            // save on RAM, It will be close and clear after statement executed.
            mBufferStream.add(bais);
        } else {
            st.setString(idx, Func.json(value));
        }
    }

    @Override
    public int saveEx(Connection connection) throws SQLException {
        int fieldCount = getFieldCount();
        StringBuilder sbFields = new StringBuilder();
        StringBuilder sbFieldCount = new StringBuilder();
        StringBuilder sbFieldAndValue = new StringBuilder();

        Class[] classList = new Class[fieldCount];
        String[] nameList = new String[fieldCount];
        Object[] valueList = new Object[fieldCount];

        int idx = 0;
        Class clazz = this.getClass();
        while (clazz != DBDataPostgres.class) {
            for (java.lang.reflect.Field f : clazz.getDeclaredFields()) {
                f.setAccessible(true);
                if (java.lang.reflect.Modifier.isStatic(f.getModifiers())) {
                    continue;
                }
                try {
                    DataFieldType type = f.getAnnotation(DataFieldType.class);
                    if (type != null) {
                        if (type.isMap()) {
                            Map map = (Map) f.get(this);
                            for (Object key : map.keySet()) {
                                Object value = map.get(key);
                                classList[idx] = value.getClass();
                                nameList[idx] = key.toString();
                                valueList[idx] = value;
                                idx++;
                            }
                            continue;
                        }
                        if (type.isTimestamp() || type.autoIncrease()) {
                            continue;
                        }
                        if (type.ignore()) {
                            continue;
                        }
                    }
                    classList[idx] = f.getType();
                    nameList[idx] = f.getName().toLowerCase();
                    valueList[idx] = f.get(this);
                    idx++;
                } catch (IllegalAccessException ex) {
                    DB.i().onLog(table(), Func.now() + "\nMethod - saveEx\n" + Func.json(this) + "\n" + Func.toString(ex));
//                    Func.printlnLog(this.getClass().getSimpleName() + "_err.log", ex);
                }
            }
            clazz = clazz.getSuperclass();
        }

        for (int i = 0; i < fieldCount; i++) {
            if (i > 0) {
                sbFields.append(",");
                sbFieldCount.append(",");
                sbFieldAndValue.append(",");
            }
            sbFields.append(nameList[i]);
            sbFieldCount.append("?");
            sbFieldAndValue.append(nameList[i]).append("=?");
        }

        String sql = "INSERT INTO " + table() + "(" + sbFields + ") values(" + sbFieldCount + ") ON DUPLICATE KEY UPDATE " + sbFieldAndValue + ";";
        int result;
        try (PreparedStatement st = connection.prepareStatement(sql)) {
            for (int i = 0; i < fieldCount; i++) {
                setValueEx(st, i + 1, classList[i], valueList[i]);
            }
            for (int i = 0; i < fieldCount; i++) {
                setValueEx(st, i + fieldCount + 1, classList[i], valueList[i]);
            }
            result = st.executeUpdate();
        }
        return result;
    }

    ////////////////////////////////////////////////////////////////////////
    @Override
    public HashMap<String, Field> allFields() {
        HashMap<String, Field> fields = new HashMap();
        Class clazz = this.getClass();
        while (clazz != DBDataPostgres.class) {
            for (java.lang.reflect.Field f : clazz.getDeclaredFields()) {
                if (java.lang.reflect.Modifier.isStatic(f.getModifiers())) {
                    continue;
                }
                fields.put(f.getName().toLowerCase(), f);
            }
            clazz = clazz.getSuperclass();
        }
        return fields;
    }

    @Override
    public void setValue(PreparedStatement st, int idx, java.lang.reflect.Field f, Object value) throws SQLException {
        DataFieldType type = f.getAnnotation(DataFieldType.class);
        if (type != null && !type.childField().isEmpty()) {
            try {
                f = f.getType().getField(type.childField());
                value = f.get(value);
            } catch (NoSuchFieldException | IllegalArgumentException | IllegalAccessException ex) {
//                DB.onException(ex, this);
                DB.i().onLog(table(), Func.now() + "\nMethod - setValue\n" + Func.json(this) + "\n" + Func.toString(ex));
            }
        }
        Class clazz = f.getType();
        if (clazz == long.class || clazz == Long.class) {
            st.setLong(idx, (Long) value);
        } else if (clazz == int.class || clazz == Integer.class) {
            st.setInt(idx, (Integer) value);
        } else if (clazz == short.class || clazz == Short.class) {
            st.setShort(idx, (Short) value);
        } else if (clazz == boolean.class || clazz == Boolean.class) {
            st.setBoolean(idx, (Boolean) value);
        } else if (clazz == double.class || clazz == Double.class) {
            st.setDouble(idx, (Double) value);
        } else if (clazz == float.class || clazz == Float.class) {
            st.setFloat(idx, (Float) value);
        } else if (clazz == String.class || clazz == StringBuilder.class) {
            st.setString(idx, value == null ? "" : value.toString());
        } else if (clazz == Date.class) {
            st.setDate(idx, new java.sql.Date(((Date) value).getTime()));
        } else if (clazz == byte[].class) {
            byte[] data = (byte[]) value;
            ByteArrayInputStream bais = new ByteArrayInputStream(data);
            st.setBinaryStream(idx, bais, data.length);
            // save on RAM, It will be close and clear after statement executed.
            mBufferStream.add(bais);
        } else {
            st.setString(idx, Func.json(value));
        }
    }

    @Override
    public DBDataSQLInfo getSQLInfo() {
        return getSQLInfo(DBPostgreSQL.class);
    }

    @Override
    public DBDataSQLInfo getSQLInfo(Class dbClazz) {
        DBDataSQLInfo uSQL = updateSQLs.get(dbClazz, this.getClass());
        if (uSQL == null) {
            StringBuilder sbFields = new StringBuilder();
            StringBuilder sbFieldsNoAuto = new StringBuilder();
            StringBuilder sbFieldCountNoAuto = new StringBuilder();
            StringBuilder sbFieldCount = new StringBuilder();
            StringBuilder sbFieldAndValue = new StringBuilder();
            StringBuilder sbFieldAndValueNoKey = new StringBuilder();

            uSQL = new DBDataSQLInfo();
            int idx = 0;
            Class clazz = this.getClass();
            while (clazz != DBDataPostgres.class) {
                for (java.lang.reflect.Field f : clazz.getDeclaredFields()) {
                    f.setAccessible(true);
                    if (java.lang.reflect.Modifier.isStatic(f.getModifiers())) {
                        continue;
                    }
                    DataFieldType type = f.getAnnotation(DataFieldType.class);
                    Class f_clazz = f.getType();
                    if (type != null) {
                        if (type.autoIncrease()) {
                            uSQL.autoField = f;
                        }

                        if (type.ignore()) {
                            continue;
                        }
                        if (type.primaryKey()) {
                            uSQL.keys.add(f);
                            uSQL.hkeys.add(f.getName().toLowerCase());
                        } else {
                            uSQL.updateFields.add(f);
                        }
                        uSQL.insertFields.add(f);
                        uSQL.fields.put(f.getName().toLowerCase(), f);

                        if (!type.childField().isEmpty()) {
                            try {
                                f_clazz = clazz.getField(type.childField()).getType();
                            } catch (NoSuchFieldException ex) {
                                Func.printlnLog(table() + "_err.log", ex);
                            }
                        }
                    } else {
                        uSQL.updateFields.add(f);
                        uSQL.insertFields.add(f);
                        uSQL.fields.put(f.getName().toLowerCase(), f);
                    }
                    uSQL.nameList.add(f.getName().toLowerCase());
                    idx++;
                }
                clazz = clazz.getSuperclass();
            }

            uSQL.fieldCount = uSQL.nameList.size();
            for (String name : uSQL.nameList) {
                boolean isNotAuto = uSQL.autoField == null || !name.equals(uSQL.autoField.getName().toLowerCase());
                if (sbFields.length() > 0) {
                    sbFields.append(",");
                    sbFieldCount.append(",");
                    sbFieldAndValue.append(",");
                }
                if (isNotAuto) {
                    if (sbFieldsNoAuto.length() > 0) {
                        sbFieldsNoAuto.append(",");
                        sbFieldCountNoAuto.append(",");
                    }
                }

                String fieldName = name;
                sbFields.append(fieldName);
                if (isNotAuto) {
                    sbFieldsNoAuto.append(fieldName);
                }

                sbFieldCount.append("?");
                if (isNotAuto) {
                    sbFieldCountNoAuto.append("?");
                }

                sbFieldAndValue.append(name).append("=?");
                if (!uSQL.hkeys.contains(name)) {
                    if (sbFieldAndValueNoKey.length() > 0) {
                        sbFieldAndValueNoKey.append(",");
                    }
                    sbFieldAndValueNoKey.append(name).append("=?");
                }
            }
            String tbl = table();
            StringBuilder sbKey = new StringBuilder();
            for (java.lang.reflect.Field f : uSQL.keys) {
                if (sbKey.length() > 0) {
                    sbKey.append(",");
                }
                sbKey.append(f.getName().toLowerCase());
            }
            uSQL.saveSQL = ""
                    + "INSERT INTO " + tbl + "(" + sbFields + ") "
                    + "values(" + sbFieldCount + ") "
                    + "ON CONFLICT (" + sbKey.toString() + ") "
                    + "DO UPDATE SET " + sbFieldAndValue.toString() + ";";

            uSQL.insertSQL = "INSERT INTO " + tbl + ""
                    + "(" + sbFieldsNoAuto.toString() + ") "
                    + "values(" + sbFieldCountNoAuto + ")";

            sbKey.setLength(0);
            for (java.lang.reflect.Field f : uSQL.keys) {
                if (sbKey.length() > 0) {
                    sbKey.append(" AND ");
                }
                sbKey.append(f.getName()).append("=?");
            }

            uSQL.whereKey = sbKey.toString();
            uSQL.updateSQL = "UPDATE " + tbl + " "
                    + "SET " + sbFieldAndValueNoKey.toString() + " "
                    + "WHERE " + uSQL.whereKey;
            updateSQLs.put(dbClazz, this.getClass(), uSQL);
        }
        return uSQL;
    }

    // method setKeys using for saveField and saveField methods
    @Override
    public void setKeys(PreparedStatement st, int idx) throws SQLException {
        DBDataSQLInfo uSQL = getSQLInfo();
        try {
            for (java.lang.reflect.Field f : uSQL.keys) {
                setValue(st, idx, f, f.get(this));
                idx++;
            }
        } catch (IllegalAccessException ex) {
//            DB.onException(ex, this);
            DB.i().onLog(table(), Func.now() + "\nMethod - setKeys\n" + Func.json(this) + "\n" + Func.toString(ex));
        }
    }

    @Override
    public int saveField(Connection connection, String fieldName) throws SQLException {
        try {
            DBDataSQLInfo uSQL = getSQLInfo();
            int result;
            try (PreparedStatement st = connection.prepareStatement("UPDATE " + table() + " SET " + fieldName + "=? WHERE " + uSQL.whereKey)) {
                java.lang.reflect.Field f = this.getClass().getField(fieldName);
                setValue(st, 1, f, f.get(this));
                setKeys(st, 2);
                result = st.executeUpdate();
            }
            return result;
        } catch (NoSuchFieldException | SecurityException | IllegalArgumentException | IllegalAccessException ex) {
//            DB.onException(ex, this);
            DB.i().onLog(table(), Func.now() + "\nMethod - saveField\n" + fieldName + "\n" + Func.json(this) + "\n" + Func.toString(ex));
        }
        return -1;
    }

    @Override
    public int saveFields(Connection connection, String[] fieldNames) throws SQLException {
        try {
            DBDataSQLInfo uSQL = getSQLInfo();
            StringBuilder sbf = new StringBuilder();
            for (String fn : fieldNames) {
                if (sbf.length() > 0) {
                    sbf.append(",");
                }
                sbf.append(fn).append("=?");
            }
            int result;
            try (PreparedStatement st = connection.prepareStatement("UPDATE " + table() + " SET " + sbf + " WHERE " + uSQL.whereKey)) {
                int idx = 1;
                for (String fn : fieldNames) {
                    java.lang.reflect.Field f = this.getClass().getField(fn);
                    setValue(st, idx, f, f.get(this));
                    idx++;
                }
                setKeys(st, 1 + fieldNames.length);
                result = st.executeUpdate();
            }
            return result;
        } catch (NoSuchFieldException | SecurityException | IllegalArgumentException | IllegalAccessException ex) {
//            DB.onException(ex, this);
            DB.i().onLog(table(), Func.now() + "\nMethod - saveField\n" + Func.json(fieldNames) + "\n" + Func.json(this) + "\n" + Func.toString(ex));
        }
        return -1;
    }

    @Override
    public long save() {
        int count = 10;
        while (count > 0) {
            long r = save(DB.i());
            if (r != -1) {
                return r;
            }
            count--;
        }
        return -1;
    }

    @Override
    public int delete() {
        int count = 10;
        while (count > 0) {
            try {
                return delete(DB.i());
            } catch (Exception ex) {
//                DB.onException(ex, this);
                DB.i().onLog(table(), Func.now() + "\nMethod - delete\n" + Func.json(this) + "\n" + Func.toString(ex));
                count--;
            }
        }
        return -1;
    }

    @Override
    public long save(DB db) {
        try {
            long res = db.save(this);
            db.logSave(this);
            return res;
        } catch (SQLException ex) {
            DB.i().onLog(table(), Func.now() + "\nMethod - save(DB db)\n" + Func.json(this) + "\n" + Func.toString(ex));
//            DB.onException(ex, this);
        }
        return -1;
    }

    @Override
    public int delete(DB db) {
        try {
            int res = db.delete(this);
            db.logDelete(this);
            return res;
        } catch (SQLException ex) {
            DB.i().onLog(table(), Func.now() + "\nMethod - delete(DB db)\n" + Func.json(this) + "\n" + Func.toString(ex));
//            DB.onException(ex, this);
        }
        return -1;
    }

    public static boolean isZero(Object o) {
        if (o == null) {
            return false;
        }
        if (o instanceof Integer) {
            return 0 == (Integer) o;
        }
        if (o instanceof Long) {
            return 0L == (Long) o;
        }
        return false;
    }

    public enum FieldType {
        NULL(null, null, 0),
        OBJECT(null, null, 1),
        STRING(String.class, StringBuilder.class, 2),
        BYTE(byte.class, Byte.class, 3),
        SHORT(short.class, Short.class, 4),
        INTEGER(int.class, Integer.class, 5),
        LONG(long.class, Long.class, 6),
        FLOAT(float.class, Float.class, 7),
        DOUBLE(double.class, Double.class, 8),
        BOOLEAN(boolean.class, Boolean.class, 9),
        DATE(Date.class, java.sql.Timestamp.class, 10),
        BYTE_ARRAY(byte[].class, Byte[].class, 11);

        public final Class clazz1;
        public final Class clazz2;
        public final int code;

        FieldType(Class clazz1, Class clazz2, int code) {
            this.clazz1 = clazz1;
            this.clazz2 = clazz2;
            this.code = code;
        }
        private static final HashMap<Class, FieldType> HASH_FIELD_TYPE = new HashMap();
        private static final HashMap<Integer, FieldType> CODE_FIELD_TYPE = new HashMap();

        static {
            for (FieldType type : FieldType.values()) {
                CODE_FIELD_TYPE.put(type.code, type);
                if (type.clazz1 != null) {
                    HASH_FIELD_TYPE.put(type.clazz1, type);
                }
                if (type.clazz2 != null) {
                    HASH_FIELD_TYPE.put(type.clazz2, type);
                }
            }
        }

        public static FieldType get(Class clazz) {
            FieldType type = HASH_FIELD_TYPE.get(clazz);
            return type == null ? OBJECT : type;
        }

        public static FieldType get(int code) {
            return CODE_FIELD_TYPE.get(code);
        }
    }

    @Override
    public void save(DataOutputStream os) throws IOException {
        DBDataSQLInfo uSQL = getSQLInfo();
        for (Field f : uSQL.fields.values()) {
            os.writeUTF(f.getName().toLowerCase());
            Class clazz = f.getType();
            Object value;
            try {
                value = f.get(this);
            } catch (IllegalArgumentException | IllegalAccessException ex) {
                value = null;
            }
            if (value == null) {
                os.write(FieldType.NULL.code);
            } else {
                FieldType type = FieldType.get(clazz);
                os.write(type.code);
                switch (type) {
                    case OBJECT:
                        os.writeUTF(Func.json(value));
                        break;
                    case STRING:
                        os.writeUTF((String) value);
                        break;
                    case BYTE:
                        os.writeByte((byte) value);
                        break;
                    case SHORT:
                        os.writeShort((short) value);
                        break;
                    case INTEGER:
                        os.writeInt((int) value);
                        break;
                    case LONG:
                        os.writeLong((long) value);
                        break;
                    case FLOAT:
                        os.writeFloat((float) value);
                        break;
                    case DOUBLE:
                        os.writeDouble((double) value);
                        break;
                    case BOOLEAN:
                        os.writeBoolean((boolean) value);
                        break;
                    case DATE:
                        os.writeLong(((Date) value).getTime());
                        break;
                    case BYTE_ARRAY:
                        byte[] b = (byte[]) value;
                        os.writeInt(b.length);
                        os.write(b);
                        break;
                }
            }
        }
        os.writeUTF("");
    }

    @Override
    public boolean load(DataInputStream is) {
        DBDataSQLInfo uSQL = getSQLInfo();
        try {
            while (true) {
                String field = is.readUTF();
                if (field.isEmpty()) {
                    break;
                }
                FieldType type = FieldType.get(is.read());
                if (type == null) {
                    return false;
                }
                Field f = uSQL.fields.get(field);
                Object value = null;
                switch (type) {
                    case NULL:
                        break;
                    case OBJECT:
                        value = Func.gson.fromJson(is.readUTF(), f.getType());
                        break;
                    case STRING:
                        value = is.readUTF();
                        break;
                    case BYTE:
                        value = is.readByte();
                        break;
                    case SHORT:
                        value = is.readShort();
                        break;
                    case INTEGER:
                        value = is.readInt();
                        break;
                    case LONG:
                        value = is.readLong();
                        break;
                    case FLOAT:
                        value = is.readFloat();
                        break;
                    case DOUBLE:
                        value = is.readDouble();
                        break;
                    case BOOLEAN:
                        value = is.readBoolean();
                        break;
                    case DATE:
                        value = new Date(is.readLong());
                        break;
                    case BYTE_ARRAY:
                        int n = is.readInt();
                        if (n < 0) {
                            return false;
                        }
                        byte[] b = new byte[n];
                        is.read(b);
                        value = b;
                        break;
                }
                f.set(this, value);
            }
            return true;
        } catch (EOFException eof) {
        } catch (IOException | IllegalAccessException | IllegalArgumentException ex) {
            DB.onException(ex, this);
        }
        return false;
    }

    @Override
    public int save(Connection connection) throws SQLException {
        DBDataSQLInfo uSQL = getSQLInfo();
        Field autoField = autoFields.get(this.getClass());
        Object autoValue = null;
        if (autoField != null) {
            try {
                autoValue = autoField.get(this);
            } catch (IllegalArgumentException | IllegalAccessException ex) {
                DB.i().onLog(table(), Func.now() + "\nMethod - save(Connection connection)\n" + Func.json(this) + "\n" + Func.toString(ex));
            }
        }
        boolean zeroAutoValue = isZero(autoValue);

        int result;
        try (PreparedStatement st = connection.prepareStatement(zeroAutoValue ? uSQL.insertSQL : uSQL.saveSQL, PreparedStatement.RETURN_GENERATED_KEYS)) {
            int idx = 1;

            Class clazz = this.getClass();
            while (clazz != DBDataPostgres.class) {
                for (java.lang.reflect.Field f : clazz.getDeclaredFields()) {
                    f.setAccessible(true);
                    if (java.lang.reflect.Modifier.isStatic(f.getModifiers())) {
                        continue;
                    }
                    try {
                        DataFieldType type = f.getAnnotation(DataFieldType.class);
                        if (type != null) {
                            if ((type.autoIncrease() && isZero(autoValue)) || type.ignore()) {
                                continue;
                            }
                        }
                        Object value = f.get(this);
                        if (value != null && value.getClass() == String.class) {
                            value = ((String) value).replaceAll("[^\\u0000-\\uFFFF]", "");
                        }
                        setValue(st, idx, f, value);
                        if (!zeroAutoValue) {
                            setValue(st, idx + uSQL.fieldCount, f, value);
                        }
                        idx++;
                    } catch (IllegalAccessException ex) {
                        DB.onException(ex, this);
                    }
                }
                clazz = clazz.getSuperclass();
            }
            result = st.executeUpdate();
            if (autoField != null) {
                try (ResultSet rs = st.getGeneratedKeys()) {
                    if (rs != null && rs.next()) {
                        try {
                            if (autoField.getType() == int.class) {
                                autoField.set(this, rs.getInt("id"));
                            } else if (autoField.getType() == long.class) {
                                long id = rs.getLong("id");
                                autoField.set(this, id);
                            }
                        } catch (IllegalArgumentException | IllegalAccessException ex) {
                            DB.i().onLog(table(), Func.now() + "\nMethod - save(Connection connection)\n" + Func.json(this) + "\n" + Func.toString(ex));
                        }
                    }
                }
            }
        }

        try {
            for (ByteArrayInputStream is : mBufferStream) {
                is.close();
            }
        } catch (IOException ex) {
        }

        return result;
    }
    
    public DBDataSQLInfo getSQLInfoEx(Class cls) {
        DBDataSQLInfo uSQL = updateSQLs.get(DBPostgreSQL.class, cls);
        if (uSQL == null) {
            StringBuilder sbFields = new StringBuilder();
            StringBuilder sbFieldsNoAuto = new StringBuilder();
            StringBuilder sbFieldCountNoAuto = new StringBuilder();
            StringBuilder sbFieldCount = new StringBuilder();
            StringBuilder sbFieldAndValue = new StringBuilder();
            StringBuilder sbFieldAndValueNoKey = new StringBuilder();

            uSQL = new DBDataSQLInfo();
            int idx = 0;
            Class clazz = cls;
            while (clazz != DBDataPostgres.class) {
                for (java.lang.reflect.Field f : clazz.getDeclaredFields()) {
                    f.setAccessible(true);
                    if (java.lang.reflect.Modifier.isStatic(f.getModifiers())) {
                        continue;
                    }
                    DataFieldType type = f.getAnnotation(DataFieldType.class);
                    Class f_clazz = f.getType();
                    if (type != null) {
                        if (type.autoIncrease()) {
                            uSQL.autoField = f;
                        }

                        if (type.ignore()) {
                            continue;
                        }
                        if (type.primaryKey()) {
                            uSQL.keys.add(f);
                            uSQL.hkeys.add(f.getName().toLowerCase());
                        } else {
                            uSQL.updateFields.add(f);
                        }
                        uSQL.insertFields.add(f);
                        uSQL.fields.put(f.getName().toLowerCase(), f);

                        if (!type.childField().isEmpty()) {
                            try {
                                f_clazz = clazz.getField(type.childField()).getType();
                            } catch (NoSuchFieldException ex) {
                                Func.printlnLog(table() + "_err.log", ex);
                            }
                        }
                    } else {
                        uSQL.updateFields.add(f);
                        uSQL.insertFields.add(f);
                        uSQL.fields.put(f.getName().toLowerCase(), f);
                    }
                    uSQL.nameList.add(f.getName().toLowerCase());
                    idx++;
                }
                clazz = clazz.getSuperclass();
            }

            uSQL.fieldCount = uSQL.nameList.size();
            for (String name : uSQL.nameList) {
                boolean isNotAuto = uSQL.autoField == null || !name.equals(uSQL.autoField.getName().toLowerCase());
                if (sbFields.length() > 0) {
                    sbFields.append(",");
                    sbFieldCount.append(",");
                    sbFieldAndValue.append(",");
                }
                if (isNotAuto) {
                    if (sbFieldsNoAuto.length() > 0) {
                        sbFieldsNoAuto.append(",");
                        sbFieldCountNoAuto.append(",");
                    }
                }

                String fieldName = name;
                sbFields.append(fieldName);
                if (isNotAuto) {
                    sbFieldsNoAuto.append(fieldName);
                }

                sbFieldCount.append("?");
                if (isNotAuto) {
                    sbFieldCountNoAuto.append("?");
                }

                sbFieldAndValue.append(name).append("=?");
                if (!uSQL.hkeys.contains(name)) {
                    if (sbFieldAndValueNoKey.length() > 0) {
                        sbFieldAndValueNoKey.append(",");
                    }
                    sbFieldAndValueNoKey.append(name).append("=?");
                }
            }
            String tbl = cls.getSimpleName().toLowerCase();
            StringBuilder sbKey = new StringBuilder();
            for (java.lang.reflect.Field f : uSQL.keys) {
                if (sbKey.length() > 0) {
                    sbKey.append(",");
                }
                sbKey.append(f.getName().toLowerCase());
            }
            uSQL.saveSQL = ""
                    + "INSERT INTO " + tbl + "(" + sbFields + ") "
                    + "values(" + sbFieldCount + ") "
                    + "ON CONFLICT (" + sbKey.toString() + ") "
                    + "DO UPDATE SET " + sbFieldAndValue.toString() + ";";

            uSQL.insertSQL = "INSERT INTO " + tbl + ""
                    + "(" + sbFieldsNoAuto.toString() + ") "
                    + "values(" + sbFieldCountNoAuto + ")";

            sbKey.setLength(0);
            for (java.lang.reflect.Field f : uSQL.keys) {
                if (sbKey.length() > 0) {
                    sbKey.append(" AND ");
                }
                sbKey.append(f.getName()).append("=?");
            }

            uSQL.whereKey = sbKey.toString();
            uSQL.updateSQL = "UPDATE " + tbl + " "
                    + "SET " + sbFieldAndValueNoKey.toString() + " "
                    + "WHERE " + uSQL.whereKey;
            updateSQLs.put(DBPostgreSQL.class, this.getClass(), uSQL);
        }
        return uSQL;
    }
    
    public int saveEx(Connection connection, Class infoClazz) throws SQLException {
        DBDataSQLInfo uSQL = getSQLInfoEx(infoClazz);
        Field autoField = autoFields.get(infoClazz);
        Object autoValue = null;
        if (autoField != null) {
            try {
                autoValue = autoField.get(this);
            } catch (IllegalArgumentException | IllegalAccessException ex) {
                DB.i().onLog(table(), Func.now() + "\nMethod - save(Connection connection)\n" + Func.json(this) + "\n" + Func.toString(ex));
            }
        }
        boolean zeroAutoValue = isZero(autoValue);

        int result;
        try (PreparedStatement st = connection.prepareStatement(zeroAutoValue ? uSQL.insertSQL : uSQL.saveSQL, PreparedStatement.RETURN_GENERATED_KEYS)) {
            int idx = 1;

            Class clazz = infoClazz;
            while (clazz != DBDataPostgres.class) {
                for (java.lang.reflect.Field f : clazz.getDeclaredFields()) {
                    f.setAccessible(true);
                    if (java.lang.reflect.Modifier.isStatic(f.getModifiers())) {
                        continue;
                    }
                    try {
                        DataFieldType type = f.getAnnotation(DataFieldType.class);
                        if (type != null) {
                            if ((type.autoIncrease() && isZero(autoValue)) || type.ignore()) {
                                continue;
                            }
                        }
                        Object value = f.get(this);
                        if (value != null && value.getClass() == String.class) {
                            value = ((String) value).replaceAll("[^\\u0000-\\uFFFF]", "");
                        }
                        setValue(st, idx, f, value);
                        if (!zeroAutoValue) {
                            setValue(st, idx + uSQL.fieldCount, f, value);
                        }
                        idx++;
                    } catch (IllegalAccessException ex) {
                        DB.onException(ex, this);
                    }
                }
                clazz = clazz.getSuperclass();
            }
            result = st.executeUpdate();
            if (autoField != null) {
                try (ResultSet rs = st.getGeneratedKeys()) {
                    if (rs != null && rs.next()) {
                        try {
                            if (autoField.getType() == int.class) {
                                autoField.set(this, rs.getInt("id"));
                            } else if (autoField.getType() == long.class) {
                                long id = rs.getLong("id");
                                autoField.set(this, id);
                            }
                        } catch (IllegalArgumentException | IllegalAccessException ex) {
                            DB.i().onLog(table(), Func.now() + "\nMethod - save(Connection connection)\n" + Func.json(this) + "\n" + Func.toString(ex));
                        }
                    }
                }
            }
        }

        try {
            for (ByteArrayInputStream is : mBufferStream) {
                is.close();
            }
        } catch (IOException ex) {
        }

        return result;
    }
}
