/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dp.db;

import java.util.HashMap;

/**
 *
 * @author ccthien
 */
public class DBCacheSQL {

    private HashMap<Class, HashMap<Class, DBDataSQLInfo>> hash = new HashMap();

    public DBCacheSQL() {
        hash.put(DBMySQL.class, new HashMap());
        hash.put(DBPostgreSQL.class, new HashMap());
        hash.put(DBOracle.class, new HashMap());
    }

    public DBDataSQLInfo get(Class dbClazz, Class clazz) {
        return hash.get(dbClazz).get(clazz);
    }

    public DBDataSQLInfo put(Class dbClazz, Class clazz, DBDataSQLInfo info) {
        return hash.get(dbClazz).put(clazz, info);
    }
}
