/* global var */

var uri = "/energy/EnergyAPI/";
var CTMLAB_GLOBAL = {};

function getParamVal(str) {
    var v = window.location.search.match(new RegExp('(?:[\?\&]' + str
            + '=)([^&]+)'));
    return v ? v[1] : null;
}

window.AL = {
    defaultCM: "Write comment..."
};

function sCurrentDate() {
    var d = new Date();
    var s = d.getFullYear() + "-" + ("0" + (d.getMonth() + 1)).slice(-2) + "-" + ("0" + d.getDate()).slice(-2);
    s += " " + ("0" + d.getHours()).slice(-2) + ":" + ("0" + d.getMinutes()).slice(-2) + ":" + ("0" + d.getSeconds()).slice(-2);
    return s;
}

/* create user infor html */
function generateUserInloadHomeContentfor(obj) {
    var cont = $("<div>").addClass("above-infor");

    var a1 = $("<a>").addClass("cls-img");
    var img = $("<img>").attr('src',
            'resources/timeline/images/dark-cubes.png');
    $(img).attr('width', '55');
    $(img).attr('height', '55');
    $(img).attr('alt', 'DarkCubes photo avatar');
    $(a1).append(img);

    var a2 = $("<a>").addClass("cls-name").text(obj.userName);
    var a3 = $("<a>").addClass("cls-date").text(obj.commentDate);

    $(cont).append(a1);
    $(cont).append(a2);
    $(cont).append(a3);
    return cont;
}
/* end */

function generateUserInfor(obj) {
    var cont = $("<div>").addClass("above-infor");

    var a1 = $("<a>").addClass("cls-img");
    var img = $("<img>").attr('src', 'resources/timeline/images/dark-cubes.png');
    $(img).attr('width', '55');
    $(img).attr('height', '55');
    $(img).attr('alt', 'DarkCubes photo avatar');
    $(a1).append(img);

    var a2 = $("<a>").addClass("cls-name").text(obj.userName);
    var a3 = $("<a>").addClass("cls-date").text(obj.commentDate);

    $(cont).append(a1);
    $(cont).append(a2);
    $(cont).append(a3);
    return cont;
}

/* create comment html */
function generateUserComment(obj) {
    var cont = $("<div>").addClass("cmmnt-content");
    var p = $("<p>").text(obj.content);
    $(cont).append(p);
    return cont;
}
/* end */

/* create social tool */
function generateSocialTool(obj) {
    var cont = $("<div>").addClass("grp-like-share-ctr");
    var a1 = $("<a>").addClass("ctr cmn-like").text("like");
    var a2 = obj.totalLiked == 0 ? $("<a>").addClass("ctr cmm-count-like") : $("<a>").addClass("ctr cmm-count-like").text("(" + obj.totalLiked + ")");
    var a3 = $("<a>").text(" - ");
    var a4 = $("<a>").addClass("ctr cmn-cmnent").text("Comment");
    var a5 = $("<a>").text(" - ");
    var a6 = $("<a>").addClass("ctr cmn-share").text("Share");

    $(cont).append(a1);
    $(cont).append(a2);
    $(cont).append(a3);
    $(cont).append(a4);
    $(cont).append(a5);
    $(cont).append(a6);

    $(a1).click(likeClick);
    $(a4).click(commentClick);

    return cont;
}
/* end */

function showChildGUI(target, pid, lst) {
    var arrFilter = $.grep(lst, function (e) {
        return (e != null && e.parentId == pid);
    });

    for (var i = 0; i < arrFilter.length; i++) {
        var cmm = arrFilter[i];
        var li = $("<li>").addClass("cmmnt");
        $(li).attr('cmm_id', cmm.commentId)
        var ui = generateUserInfor(cmm);
        var u_comment = generateUserComment(cmm);
        var socialTool = generateSocialTool(cmm);
        $(li).append(ui);
        $(li).append(u_comment);
        $(li).append(socialTool);

        var ul_rep = $("<ul>").addClass("replies");
        $(li).append(ul_rep);

        $(target).append(li);

        cmm.isAppend = true;
        showChildGUI(ul_rep, cmm.commentId, lst);
    }
}

function loadGUIComments(cate_code, n_code, _lang) {
    loadComments(cate_code, n_code, _lang, function (listComment) {
        $("#comments").empty();
//        console.log(listComment);
        showChildGUI($("#comments"), 0, listComment);
    })
}

/* click event */
function likeClick() {
    if (!CTMLAB_GLOBAL.params.isLogin) {
        alert("Login to like, please!");
        return;
    }
    var pr = $(this).closest("li.cmmnt");
    var cmm_id = $(pr).attr('cmm_id');

    return $.ajax({
        url: uri + 'updateLike',
        data: {
            newsCode: CTMLAB_GLOBAL.params.code,
            lang: CTMLAB_GLOBAL.params.lang,
            userId: "",
            commentId: cmm_id,
            urlNews: window.location.href
        },
        type: 'POST',
        success: function (res) {
            if (res)
                loadGUIComments('ctg_01', AL.blog.newsCode, AL.blog.lang);
        }
    })
}

function writeComment(event, prid, cmm_cnt, _lang, callback) {
    var area = $(this).siblings();
    var cmm = $(area).val();
    var pr = $(this).closest("li.cmmnt");
    var cmm_id = $(pr).attr('cmm_id');

    return $.ajax({
        url: uri + 'writeComment',
        data: {
            newsCode: CTMLAB_GLOBAL.params.code,
            userId: "",
            lang: CTMLAB_GLOBAL.params.lang,
            parentId: (prid || prid == 0) ? prid : cmm_id,
            content: cmm_cnt ? cmm_cnt : cmm,
            urlNews: window.location.href
        },
        type: 'POST',
        success: function (res) {
            if (res) {
                loadGUIComments('ctg_01', AL.blog.newsCode, AL.blog.lang);
                if (callback) {
                    callback();
                }
            }

        }
    })
}

function writeCommentEx(prid, code, cmm_cnt, _lang, callback) {
    return $.ajax({
        url: uri + 'writeComment',
        data: {
            newsCode: code,
            userId: "",
            lang: _lang,
            parentId: prid,
            content: cmm_cnt,
            urlNews: window.location.href
        },
        type: 'POST',
        success: function (res) {
            if (callback) {
                callback(res);
            }
        }
    })
}

function commentClick() {
    if (!CTMLAB_GLOBAL.params.isLogin) {
        alert("Login to comment, please!");
        return;
    }
    $(".cmm_teporary").remove();

    var pr = $(this).closest("li.cmmnt");
    var cmm_tool = $("<div>").addClass("textarea-container cmm_teporary");
    var area = $("<textarea>").addClass("area-write-comment").text(AL.defaultCM);
    var a = $("<a>").addClass("btn btn-red-base").text("Comment");
    $(cmm_tool).append(area);
    $(cmm_tool).append(a);

    $(pr).children(':eq(2)').after($(cmm_tool));
    $(a).click(writeComment);
    $(area).focus(areaFocus);
    $(area).focusout(areaFocusOut);
}

function areaFocus() {
    if ($(this).val().trim() == AL.defaultCM) {
        $(this).empty();
        $(this).val("");
    }
}

function areaFocusOut() {
    var txt = $(this).val();
    if (txt.trim() == "")
        $(this).val(AL.defaultCM);
}

function loadComments(_cate_code, _n_code, _lang, callback) {
    return $.ajax({
        url: uri + 'loadComments',
        data: {
            cate_code: _cate_code,
            n_code: _n_code,
            lang: _lang
        },
        type: 'POST',
        success: function (json) {
            var listComment = JSON.parse(json);
            callback(listComment);
        }
    })
}

function checkUser(uid, uname, sn_id, email, callback) {
    return $.ajax({
        url: uri + 'checkUser',
        data: {
            uid: uid,
            uname: uname,
            sn_id: sn_id,
            email: email
        },
        type: 'POST',
        success: function (json) {
            callback(json);
        }
    })
}

function checkLoginState(callback) {    
//    var func = function () {
//        if (!parent.FB) {
//            checkLoginState(callback);
//            return;
//        }
//        FB.getLoginStatus(function (response) {
//            if (response.status == "unknown" || response.status == "not_authorized") {
//                callback();
//                return;
//            }
//            var access_token = FB.getAuthResponse()[response.authResponse.accessToken];
//            FB.api('/me', function (response) {
//                checkUserFB(response.id, response.name, 1,
//                        function (res) {
//                            callback(res);
//                        })
//            });
//        });
//    };
//    setTimeout(func,3000);
}

// Blog
function linkToLogin(_lang, url, callback) {
    return $.ajax({
        url: uri + 'linkToLogin',
        data: {
            lang: _lang,
            url: encodeURI(url)
        },
        type: 'POST',
        success: function (json) {
            var res = JSON.parse(json);
            if(res.text && res.text!==""){
                location.href = res.text;
            } else{
                callback();
            }
        }
    });
}

function loadBlogNews(_lang, callback) {
    return $.ajax({
        url: uri + 'loadBlogNews',
        data: {
            lang: _lang
        },
        type: 'POST',
        success: function (json) {
            var listNews = JSON.parse(json);
            if (callback)
                callback(listNews);
        }
    })
}

function loadBlogDetail(_code, _lang, callback) {
    return $.ajax({
        url: uri + 'loadBlogDetail',
        data: {
            code: _code,
            lang: _lang
        },
        type: 'POST',
        success: function (json) {
            var res = JSON.parse(json);
            if (callback)
                callback(res);
        }
    })
}

// end blog

function getDictionaryValue(dics, group, key) {
    var item = $.grep(dics, function (e) {
        return e.groupCode == group && e.keyCode == key
    })[0];
    return item;
}

function loadContactContent(_lang, callback) {
    return $.ajax({
        url: uri + 'loadContactContent',
        data: {
            lang: _lang
        },
        type: 'POST',
        success: function (json) {
            var res = JSON.parse(json);
            if (callback)
                callback(res);
        }
    })
}

function loadDownloadContent(_lang, callback) {
    return $.ajax({
        url: uri + 'loadDownloadContent',
        data: {
            lang: _lang
        },
        type: 'POST',
        success: function (json) {
            var res = JSON.parse(json);
            if (callback)
                callback(res);
        }
    });
}

function loadAboutContent(_lang, callback) {
    return $.ajax({
        url: uri + 'loadAboutContent',
        data: {
            lang: _lang
        },
        type: 'POST',
        success: function (json) {
            var res = JSON.parse(json);
            if (callback)
                callback(res);
        }
    })
}

function loadHomeContent(_lang, callback) {
    return $.ajax({
        url: uri + 'loadHomeContent',
        data: {
            lang: _lang
        },
        type: 'POST',
        success: function (json) {
            var res = JSON.parse(json);
            if (callback)
                callback(res);
        }
    })
}

function loadProducts(_lang, _lineCode, _purposeCode, callback) {
    return $.ajax({
        url: uri + 'loadProducts',
        data: {
            lang: _lang,
            lineCode: _lineCode,
            purposeCode: _purposeCode
        },
        type: 'POST',
        success: function (json) {
            var res = JSON.parse(json);
            if (callback)
                callback(res);
        }
    })
}

function loadProductDetail(_lang, _productCode, callback) {
    return $.ajax({
        url: uri + 'loadProductDetail',
        data: {
            lang: _lang,
            productCode: _productCode
        },
        type: 'POST',
        success: function (json) {
            var res = JSON.parse(json);
            if (callback)
                callback(res);
        }
    })
}

function getProductDetail(_lang, _productCode, callback) {
    return $.ajax({
        url: uri + 'getProductDetail',
        data: {
            lang: _lang,
            productCode: _productCode
        },
        type: 'POST',
        success: function (json) {
            var res = JSON.parse(json);
            if (callback)
                callback(res);
        }
    })
}

function loadNews(_lang, callback) {
    return $.ajax({
        url: uri + 'loadNews',
        data: {
            lang: _lang
        },
        type: 'POST',
        success: function (json) {
            var res = JSON.parse(json);
            if (callback)
                callback(res);
        }
    })
}
function loadNews2(_lang, callback) {
    return $.ajax({
        url: uri + 'loadNews2',
        data: {
            lang: _lang
        },
        type: 'POST',
        success: function (json) {
            var res = JSON.parse(json);
            if (callback)
                callback(res);
        }
    })
}

function loadNewsDetail(_code, _lang, callback) {
    return $.ajax({
        url: uri + 'loadNewsDetail',
        data: {
            code: _code,
            lang: _lang
        },
        type: 'POST',
        success: function (json) {
            var res = JSON.parse(json);
            if (callback)
                callback(res);
        }
    })
}