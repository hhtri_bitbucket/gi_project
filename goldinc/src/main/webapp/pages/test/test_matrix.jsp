<%@page import="com.dp.db.Func" %>
<%@page import="com.ctmlab.game.manager.StaticDataMng" %>
<%@page import="com.ctmlab.game.data.staticdata.Zone" %>
<%
    String id = request.getParameter("id");
    short zoneID = Short.parseShort(id == null ? "0" : id);
    Zone z = StaticDataMng.i().worldMap.getZone(zoneID);
    String s = z.getMatrix();
%>
<html lang="en">
<head>
    <title>Zone Test</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <jsp:include page="../../management/_header_scripts.jsp"/>
</head>
<body>
<div id="content" style="overflow:scroll; height:100%; width: 4050px;">
    <%=s%>
</div>
</body>
</html>