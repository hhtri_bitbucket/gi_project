<%@page import="java.util.ArrayList"
        %><%@page import="com.ctmlab.game.network.client.WSServiceClient"
        %><%@page import="com.ctmlab.game.network.manager.WSServiceServer"
        %><%@page import="com.dp.db.Func"
        %><%
    StringBuilder sb = new StringBuilder();
    
    ArrayList<WSServiceClient> ls = new ArrayList();
    for(WSServiceClient c:WSServiceServer.i().getListClient()){
        sb.append(c.getInfoSocket()).append("\n");
        if(c.getUserID().isEmpty()){
            ls.add(c);
        }
    }
    
    for(WSServiceClient c:ls){
        WSServiceServer.i().iClients.remove(c.getId());
    }
    
    response.setContentType(Func.MINE_TEXT);
    out.print(sb.toString());
    sb.setLength(0);
%>