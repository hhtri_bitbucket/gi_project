<%@page import="com.ctmlab.game.data.staticdata.gameobjectinfo.*"
%><%@page import="com.ctmlab.game.goldincenum.ENUM_UNIT_TYPE"
%><%@page import="java.io.File"
%><%@page import="com.ctmlab.util.Utils"
%><%@page import="com.ctmlab.game.network.manager.WSGameDefine"
%><%@page import="com.ctmlab.game.manager.StaticDataMng"%>
<%
    GameObjectInfoRepo repo = StaticDataMng.i().getRepoGameObjectInfo();
    // office
    String jsonOffice = Utils.i().readTextFile(WSGameDefine.pathConfig + File.separator + "homeoffice.json");
    repo.lstObjectMap.put((byte) ENUM_UNIT_TYPE.HOME_OFFICE.getCode(), GameObjectInfo.initFromJson(ENUM_UNIT_TYPE.HOME_OFFICE,jsonOffice));    
    // exploration
    String jsonExploration = Utils.i().readTextFile(WSGameDefine.pathConfig + File.separator + "exploration_center.json");
    repo.lstObjectMap.put((byte) ENUM_UNIT_TYPE.HOME_EXPLORATION.getCode(), GameObjectInfo.initFromJson(ENUM_UNIT_TYPE.HOME_EXPLORATION,jsonExploration));
    // transportation
    String jsonTransportation = Utils.i().readTextFile(WSGameDefine.pathConfig + File.separator + "transportation_center.json");
    repo.lstObjectMap.put((byte) ENUM_UNIT_TYPE.HOME_TRANSPORTATION.getCode(), GameObjectInfo.initFromJson(ENUM_UNIT_TYPE.HOME_TRANSPORTATION,jsonTransportation));
    
    out.print("done");
%>