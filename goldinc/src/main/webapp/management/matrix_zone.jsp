<%@page import="com.dp.db.Func"%><%@page import="com.ctmlab.game.manager.StaticDataMng"%><%@page import="com.ctmlab.game.data.staticdata.Zone"%><%
//    if (session.getAttribute("user") == null) {
//        response.sendRedirect("ad_login.jsp");
//        return;
//    }
    String id = request.getParameter("id");
    short zoneID = Short.parseShort(id == null ? "0" : id);
    Zone z = StaticDataMng.i().worldMap.getZone(zoneID);
    String s = z.getMatrix();
%>
<html lang="en">
    <head>
        <title>Zone Management</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <jsp:include page="_header_scripts.jsp" />
        <script>
            var __url_api__ = "ad_api.jsp";
            var zoneID = '<%=zoneID%>';
            function onSuccessEvent(type, result) {
                $('#content').html('');
                $('#content').html(result);
                $('#loading').hide();
            }
            function onErrorEvent(type, result) {
                console.log("[matri_zone] -Error: " + type);
                alert(result);
            }

            function onClickRegenStaticMap() {
                var json = {
                    action: "regen_static_map",
                    id: zoneID,
                    mounttain: 50,
                    tree: 100,
                    mine: 30,
                    oil_mine: 30
                };
                $('#loading').show();
                var errorFunc = function (res) {
                    onErrorEvent("login", res);
                };
                var successFunc = function (res) {
                    onSuccessEvent("login", res);
                };
                doPost(__url_api__, json, successFunc, errorFunc);
            }



            var oSelected = null;
            var lstSelected = null;
            var lstChanged = null;
            
            var lstIDChanged = [];
            var curSelected = null;
            var curObjSelected = null;

            var mWidth = 100;
            var mHeight = 100;

            var isMoving = false;
            var countObject = 0;

            function getText(x, y) {
                return Number($('#' + x + '_' + y).text());
            }

            function checkGameObject(x, y) {
                if (getText(x, y) === 3) {
                    var rs = [];
                    var row = x + 1;
                    var col = y + 1;
                    for (var i = x - 1; i <= row; i++) {
                        for (var j = y - 1; j <= col; j++) {
                            if (i >= 0 && i < mWidth && j >= 0 && j < mHeight) {
                                var t = getText(i, j);
                                if (t === 3 || t === 6 || t === 7) {
                                    rs.push({
                                        x: i,
                                        y: j,
                                        text: $('#' + i + "_" + j).text(),
                                        bg: $('#' + i + "_" + j).css("background-color")});
                                }
                            }
                        }
                    }
                    if (rs.length === 9) {
                        oSelected = rs;
                        // check double click
                        var key = x + "_" + y;
                        if (lstSelected !== null && lstSelected.hasOwnProperty(key)) {
                            var temp = lstSelected[key];
                            for (var i = 0; i < temp.length; i++) {
                                var o = temp[i];
                                $('#' + o.x + "_" + o.y).css("background-color", o.bg);
                            }
                            return false;
                        }
                        return true;
                    }
                }
                return false;
            }

            function checkGameObjectAvaibale(x, y) {

                var rs = [];
                var row = x + 1;
                var col = y + 1;
                for (var i = x - 1; i <= row; i++) {
                    for (var j = y - 1; j <= col; j++) {
                        if (i >= 0 && i < mWidth && j >= 0 && j < mHeight) {
                            var t = getText(i, j);
                            if (t !== 0) {
                                return null;
                            } else {
                                rs.push({x: i, y: j, bg: $('#' + i + "_" + j).css("background-color")});
                            }
                        }
                    }
                }
                return rs;
            }
            
            function getGameObject(x, y) {

                var rs = [];
                var row = x + 1;
                var col = y + 1;
                for (var i = x - 1; i <= row; i++) {
                    for (var j = y - 1; j <= col; j++) {
                        if (i >= 0 && i < mWidth && j >= 0 && j < mHeight) {
                            var t = getText(i, j);
                            if (t !== -1) {
                                return null;
                            } else {
                                rs.push({x: i, y: j, text:"0", bg:"#000000"});
                            }
                        }
                    }
                }
                return rs;
            }

            function btnOnClick(id) {
                var a = id.split("_");
                if (!isMoving) {
                    if (checkGameObject(Number(a[0]), Number(a[1]))) {
                        var item = {x: a[0], y: a[1], text: $('#' + id).text(), bg: $('#' + id).css("background-color")};
                        $('#' + item.x + "_" + item.y).css("background-color", "yellow");
                        if (lstSelected === null) {
                            lstSelected = {};
                        }
                        lstSelected[id] = oSelected;
                    }
                } else {                    
                    var item = checkGameObjectAvaibale(Number(a[0]), Number(a[1]));
                    if (item !== null) {
                        for (var i = 0; i < item.length; i++) {
                            var o = item[i];
                            $('#' + o.x + "_" + o.y).css("background-color", "yellow");
                            $('#' + o.x + "_" + o.y).text("-1");
                        }                        
                        curSelected = id;
                        curObjSelected = item;                        
                    } else if($('#' + id).text()=="-1"){
                        curSelected = id;
                        curObjSelected = getGameObject(Number(a[0]), Number(a[1])); 
                    }
                }
            }

            function btnBeginMoveOnClick() {
                if (lstSelected !== null) {
                    isMoving = true;
                    var keys = Object.keys(lstSelected);
                    for (var i = 0; i < keys.length; i++) {
                        var oGame = lstSelected[keys[i]];
                        for (var j = 0; j < oGame.length; j++) {
                            var o = oGame[j];
                            $('#' + o.x + "_" + o.y).css("background-color", "#000000");
                            $('#' + o.x + "_" + o.y).text("0");
                        }
                    }
                    countObject = keys.length;
                    $('#lbCount').text(countObject);
                    $('#lbCount').show();
                }
            }

            function btnCancelMoveOnClick() {
                isMoving = false;
                if (lstSelected != null) {
                    var keys = Object.keys(lstSelected);
                    for (var i = 0; i < keys.length; i++) {
                        var oGame = lstSelected[keys[i]];
                        for (var j = 0; j < oGame.length; j++) {
                            var o = oGame[j];
                            $('#' + o.x + "_" + o.y).css("background-color", o.bg);
                            $('#' + o.x + "_" + o.y).text(o.text);
                        }
                    }
                }
                lstSelected = null;
            }
            
            function btnSetUndoOnClick(){
                if(lstChanged!=null && lstChanged.hasOwnProperty(curSelected)){
                    delete lstChanged[curSelected];
                    var i = 0;
                    for(i=0; i<lstIDChanged.length;i++){
                        if(curSelected==lstIDChanged[i]){
                            break;
                        }
                    }
                    lstIDChanged.splice(i, 1);
                    countObject++;
                    if(Object.keys(lstSelected).length<=countObject){
                        countObject = Object.keys(lstSelected).length;
                    }
                    $('#lbCount').text(countObject);
                }
                for (var j = 0; j < curObjSelected.length; j++) {
                    var o = curObjSelected[j];
                    $('#' + o.x + "_" + o.y).css("background-color", o.bg);
                    $('#' + o.x + "_" + o.y).text(0);
                }
            }

            function btnSetPosOnClick() {
                if (countObject <= 0) {
                    alert('Changing object position is up!');
                    return;
                }
                if(curSelected!=null){
                    $('#' + curSelected).css("background-color", "green");
                    lstIDChanged.push(curSelected);
                    if (lstChanged === null) {
                        lstChanged = {};
                    }

                    if (!lstChanged.hasOwnProperty(curSelected)) {
                        lstChanged[curSelected] = curObjSelected;
                        countObject--;
                        $('#lbCount').text(countObject);
                    }
                }
            }

            function btnSetDataOnClick() {
                // save data to server

            }

        </script>
    </head>
    <body>
        <jsp:include page="_header.jsp" />
        <br/>
        <div>
            <input type="button" id="btnRegen" name="btnRegen" value="Regen" onclick="onClickRegenStaticMap()" />

            <input type="button" id="btnBeginMove" name="btnBeginMove" value="Click to move" onclick="btnBeginMoveOnClick()" />
            <input type="button" id="btnCancelMove" name="btnCancelMove" value="Cancelling move" onclick="btnCancelMoveOnClick()" />
            <input type="button" id="btnSetData" name="btnSetData" value="Save" onclick="btnSetDataOnClick()" /> 
            <input type="button" id="btnSaveData" name="btnSaveData" value="Regen" onclick="btnSetDataOnClick()" />
            <img id="loading" style="display: none" src="../resources/js/spinningred.gif" width="25" height="25"/>
        </div>        
        <br/>
        <div>
            <input type="button" id="btnSetPos" name="btnSetPos" value="Set Pos" onclick="btnSetPosOnClick()" />
            <input type="button" id="btnUndoPos" name="btnUndoPos" value="Undo Pos" onclick="btnSetUndoOnClick()" />
            <label id="lbCount" name="lbCount" value="0" style="display: none;"></label> 
        </div>
        <br/><br/>
        <div id="content" style="overflow:scroll; height:100%; width: 4050px;">
            <%=s%>     
        </div>
    </body>
</html>