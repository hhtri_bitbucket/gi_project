/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ctmlab.map;

import com.ctmlab.game.data.model.building.Building;
import com.ctmlab.game.data.model.building.BuildingExploration;
import com.ctmlab.game.data.model.building.BuildingOfficer;
import com.ctmlab.game.data.model.building.BuildingTransportation;
import com.ctmlab.game.data.model.worker.Worker;
import com.ctmlab.game.data.pojo.ProfilePrivate;
import com.ctmlab.game.data.staticdata.Region;
import com.ctmlab.game.data.staticdata.Zone;
import com.ctmlab.game.data.staticdata.gameobjectinfo.GameObjectInfoRepo;
import com.ctmlab.game.data.staticdata.gameobjectinfo.GameObjectOfficeInfo;
import com.ctmlab.game.data.staticdata.gameobjectinfo.GameObjectWeaponDepotInfo;
import com.ctmlab.game.manager.GoldIncMng;
import com.ctmlab.game.manager.StaticDataMng;
import com.dp.db.DB;
import com.dp.db.DBPostgreSQL;
import com.dp.db.Func;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.*;

/**
 *
 * @author hhtri
 */
public class TestMap {

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {

    }

    @After
    public void tearDown() {
    }

    public void testMatrix(){
        int i = 0;
        int n = 10;
        while(i++<n){
            System.out.println(String.format("Measure %d", i));
            long start = System.currentTimeMillis();
            long end = 0;
            Region r = StaticDataMng.i().worldMap.getRegion((short)0);
            end = System.currentTimeMillis();
            System.out.println(String.format("Time get region is %d", (end-start)));

            Zone z1 = null;
            Zone z2 = null;
            start = System.currentTimeMillis();
            for(Zone z:r.zones.values()){
                if(z.x()==100 &&z.y()==300){
                    z1 = z;
                } else if(z.x()==700 &&z.y()==700){
                    z2 = z;
                } else if(z1!=null && z2!=null){
                    break;
                }
            }
            end = System.currentTimeMillis();
            System.out.println(String.format("Time get 2 zones random is %d", (end-start)));

            start = System.currentTimeMillis();
            byte[][] rs = r.getInnerMatrix(z1, z2);
            end = System.currentTimeMillis();
            System.out.println(String.format("Size of inner matrix is %d:%d", rs.length, rs[0].length));
            System.out.println(String.format("Time get inner matrix is %d", (end-start)));
            rs = null;
        }
    }
        
    @org.junit.Test
    public void testSomeMethod() throws Exception {
        GoldIncMng.i().initDefault(true);
//        testMatrix();
    }
}
