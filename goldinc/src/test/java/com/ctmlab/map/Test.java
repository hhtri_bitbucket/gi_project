/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ctmlab.map;

import com.ctmlab.game.data.ctmon.CTMON;
import com.ctmlab.game.data.model.GoldIncUser;
import com.ctmlab.game.data.staticdata.gameobjectinfo.GameObjectInfoRepo;
import com.ctmlab.game.data.staticdata.WorldMap;
import com.ctmlab.game.data.staticdata.gameobjectinfo.GameObjectInfo;
import com.ctmlab.game.manager.StaticDataMng;
import com.ctmlab.util.Utils;
import com.dp.db.Func;
import com.google.common.io.LittleEndianDataInputStream;
import com.google.common.io.LittleEndianDataOutputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.util.Properties;
import java.util.TreeMap;
import java.util.UUID;
import javax.mail.Message;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;

/**
 *
 * @author hhtri
 */
public class Test {
    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
        TreeMap<Integer, Integer> tree  = new TreeMap<>();
        tree.put(1, 1);
        tree.put(2, 2);
        tree.put(3, 3);
        tree.put(4, 4);
        tree.put(5, 5);

        System.out.println(Func.pson(tree.headMap(0).entrySet()));
        
    }

    @After
    public void tearDown() {
    }
    
    private void test01(){
        LittleEndianDataOutputStream os;
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        os = new LittleEndianDataOutputStream(baos);
        CTMON.i().toStream(os, StaticDataMng.i().worldMap);
        byte[] data = baos.toByteArray();        
        System.out.println(data.length);
                
        WorldMap m1 = StaticDataMng.i().worldMap;
        
        ByteArrayInputStream bais = new ByteArrayInputStream(data);
        LittleEndianDataInputStream lsDis = new LittleEndianDataInputStream(bais);
        WorldMap m2 = CTMON.i().fromStream(lsDis, WorldMap.class);
        System.out.println("End!!!");
    }
    
    private void test02(){
        GoldIncUser u = new GoldIncUser();
        u.timeClient = System.currentTimeMillis();
        u.userID = "1111222333444";
        
        LittleEndianDataOutputStream os;
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        os = new LittleEndianDataOutputStream(baos);
        CTMON.i().toStream(os, u);
        byte[] data = baos.toByteArray();        
        System.out.println(data.length);
                
        GoldIncUser m1 = u;
        
        ByteArrayInputStream bais = new ByteArrayInputStream(data);
        LittleEndianDataInputStream lsDis = new LittleEndianDataInputStream(bais);
        GoldIncUser m2 = CTMON.i().fromStream(lsDis, GoldIncUser.class);
        
        System.out.println("U1");
        System.out.println(Func.pson(m1));
        
        System.out.println("E2");
        System.out.println(Func.pson(m2));
        
        System.out.println("End!!!");
        
    }
    
    public void email(){

        final String username = "servicectmlab2019@gmail.com";
        final String password = "t123456789!";

        Properties props = new Properties();
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.port", "587");

        Session session = Session.getInstance(props, new javax.mail.Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(username, password);
            }
        });

        try {

            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress(username, "test"));
            message.setRecipients(Message.RecipientType.TO,InternetAddress.parse("hhtris7e@gmail.com"));
            message.setSubject("Test");
            message.setContent("Test", "text/html");

            Transport.send(message);

        } catch (Exception e) {
            e.printStackTrace();
        }
//        
//    }
    }
    
    public String getMatrix(byte[][] matrix, int w, int h){
        String s="";
        String line = "";
        for(int i=0;i<w;i++){
            line = "";
            for(int j=0;j<h;j++){
                if(!line.isEmpty()){line+=" ";}
                line+=matrix[i][j];
            }
            s+=line+"\n";
        }
        return s;
    }
    
    public void setMatrix(byte[][] matrix, int w, int h, int x, int y){       
        int nw = x+2;
        int nh = y+2;
        for(int i=x-2; i<=nw; i++){
            for(int j=y-2; j<=nh; j++){
//                System.out.println(i+":"+j);
                boolean flag = i>=0 && i<w && j>=0 && y<h;
                if(flag){matrix[i][j] = 1;}                
                
            }
        }
    }
    
    public void testMatrix(){
//        WorldMapMng.i().testMap();
        int w = 10;
        int h = 10;
        byte[][] matrix = new byte[10][10];
        for(int i=0;i<w;i++){
            for(int j=0;j<h;j++){
                matrix[i][j]=0;
            }
        }
                ;
        int x = 1;
        int y = 1;
        
        setMatrix(matrix, w, h, 1, 1);
        setMatrix(matrix, w, h, 2, 3);
        
        System.out.println(getMatrix(matrix, w, h));
        
    }
    
    private void testGameObjectInfo(){
//        String path = "/home/hhtri/Desktop/workspace/netbean/external/backup/gameobjectinfo.json";
        GameObjectInfoRepo repo = new GameObjectInfoRepo();
//        repo.convertGameObjectInfo(path);
//        System.out.println(Func.json(repo)); 
//        
//        path = "/home/hhtri/Desktop/workspace/netbean/external/backup/gameobjectinfo.bin";
//        repo.writeGameObjectInfo(path);
//        
//        repo = new GameObjectInfoRepo();
//        repo.readGameObjectInfo(path);
//        
//        System.out.println(Func.json(repo)); 

//        String path = WSGameDefine.pathBackup + File.separator + "gameobjectinfo.bin";
//        if(!(new File(path)).exists()){
//            repo.init();
//        } else {
//            repo.readGameObjectInfo(path);
//        }

//        String path = "http://test.ctmlab.com/goldinc/service/gameobject_info";
////        String path = "http://localhost:8084/goldinc/service/gameobject_info";
//        byte[] rs = ExHttpClient.i().postEx(path);
//        if(rs!=null){        
//            ByteArrayInputStream bais = new ByteArrayInputStream(rs);
//            LittleEndianDataInputStream is = new LittleEndianDataInputStream(bais);
//            ServiceResponse sr = CTMON.i().fromStream(is, ServiceResponse.class);
//            
////            GameObjectInfoRepo repo = new GameObjectInfoRepo();
//            repo.apply(sr.getData());
//
//            System.out.println(Func.json(repo)); 
//        }
    }
    
    @org.junit.Test
    public void testSomeMethod() throws Exception {
//        test02();
//        GIUser u = new GIUser();
//        for(Field f: u.getClass().getDeclaredFields()){
//            System.out.println(f.getName());
//        }
//        CTConfig.initFolders();
//        CTConfig.initConfigEmail();
//
//        String email = "hhtris7e@gmail.com";
//        String content = SendEmailUtil.i().getContentTemplate();
//        content = content.replace("#email", email).replace("#link_verify", "123123");
//        System.out.println(SendEmailUtil.i().sendEmail(email, content));      
//
//        email();

//        File folder = new File("/home/hhtri/Downloads/Scripts");
//        ArrayList<File> lst = new ArrayList();
//        lst.add(folder);
//        while(!lst.isEmpty()){
//            File f = lst.get(0);
//            lst.remove(0);
//            
//            for(File fTmp: f.listFiles()){
//                if(fTmp.isDirectory()){
//                    lst.add(fTmp);
//                } else{
//                    if(fTmp.getName().contains(".meta")){
//                        fTmp.delete();
//                    }
//                }
//            }
//            
//        }
//             testMatrix();
//        new HomeBasedConfig().init("1.0.1");
//        testGameObjectInfo();

//        System.out.println(String.format("%s-%s-%s-%s", 
//                Utils.i().randomString(4), Utils.i().randomString(4), 
//                Utils.i().randomString(4), Utils.i().randomString(4)));
    }
}
